/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('RegisterController', function ($scope, $location, $http, $rootScope, $filter, $uibModal, $timeout) {
    $scope.formdata = {};
    $scope.formElements = $rootScope.formElements;
    $scope.typetitle = 'School/Group/Club';
    $scope.typemembernoun = 'members/students';
    $scope.formdata.orgmembercount = 1;
    $scope.formdata.orgcountry = '';

    $scope.validateRegForm = function(){
        console.log('run');
        angular.forEach($scope.registerForm, function(value, key) {
            if (typeof value === 'object' && value.hasOwnProperty('$modelValue')){
                if(value.$modelValue){
                    //console.log( value.$modelValue);
                    value.$setDirty();
                }
            }
        });
    };

    $timeout(function() {
        $scope.validateRegForm();
    },2000);

    $scope.changedValue = function (val) {
        if (angular.isDefined(val)) {
            var foundItem = $filter('filter')($scope.formElements.types, { id: val }, true)[0];
            $scope.typetitle = foundItem.title;
            $scope.typemembernoun = foundItem.membernoun;

        } else {
            $scope.typetitle = 'School/Group/Club';
            $scope.typemembernoun = 'members/students';
        }
    };

    $scope.lookupSchool = function () {
        // $scope.formdata = {};

        $scope.formdata.schoolDataId = $location.search().id;
        $scope.formdata.orgname = '';
        $scope.formdata.orgaddress1 = '';
        $scope.formdata.orgaddress2 = '';
        $scope.formdata.orgaddress3 = '';
        $scope.formdata.orgcounty = '';
        $scope.formdata.orgtown = '';
        $scope.formdata.orgpostcode = '';
        $scope.formdata.orgcountry = '222';
        $scope.formdata.orgurl = '';
        $scope.formdata.orgurn = '';
        $scope.formdata.orgmembercount = 1;
        $scope.formdata.orglea = '';
        $scope.typetitle = 'Establishment';

        $scope.schoolDataLoaded = false;
        $http.get($scope.serverstring + 'api/v1.0/organization/getschooldata?sdid=' + $scope.formdata.schoolDataId).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    // Fill out the form
                    $scope.formdata.orgname = data.data.Name;
                    $scope.formdata.orgaddress1 = data.data.Address1;
                    $scope.formdata.orgaddress2 = data.data.Address2;
                    $scope.formdata.orgaddress3 = data.data.Address3;
                    $scope.formdata.orgcounty = data.data.County;
                    $scope.formdata.orgtown = data.data.Town;
                    $scope.formdata.orgpostcode = data.data.Postcode;
                    $scope.formdata.orgcountry = '222';
                    $scope.formdata.orgurl = data.data.URL;
                    $scope.formdata.orgurn = data.data.URN;
                    $scope.formdata.orgtype = 1;
                    $scope.formdata.orglea = 1;
                    $scope.formdata.orgmembercount = parseInt(data.data.membercount, 10);
                    $scope.changedValue('1');
                    $scope.schoolDataLoaded = true;
                    //$scope.loadSchoolsByPostcode = false;

                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

    /*--- Postcode Validation ---*/
    $scope.$watch('formdata.orgpostcode', function (newValue, oldValue) {
        if (!$scope.schoolDataLoaded) {
            if ($scope.formdata.orgpostcode) {
                $http.post($rootScope.serverstring + 'api/v1.0/organization/checkpostcode', {
                    Postcode: $scope.formdata.orgpostcode
                }).success(function (data, status, headers, config) {
                    // school exists - have to choose
                    if (data) {
                        $scope.schoolsFound = data;
                        $scope.registerForm.orgpostcode.$setValidity('postcodeInUse', false);

                        $timeout(function() {
                            $scope.validateRegForm();
                        },2000);
                    }
                }).error(function (data, status, headers, config) {
                    // does not exists - can pick it!
                    $scope.registerForm.orgpostcode.$setValidity('postcodeInUse', true);
                    $scope.schoolsFound = null;
                });
            }
            else {
                $scope.registerForm.orgpostcode.$setValidity('postcodeInUse', true);
            }
        }
    });


    $scope.selectSchool = function (school) {
        if (school.school_status_name == 'Not Activated' || !school.school_status_name) {
            // Delete suggestions
            $scope.schoolsFound = null;
            // Prefill form 
            $scope.formdata.orgphone = school.Phone;
            $scope.formdata.orgurl = school.URL;
            $scope.formdata.orgmembercount = parseInt(school.Pupils);
            $scope.formdata.orgurn = school.URN;
            // Set validity to valid
            $scope.registerForm.orgpostcode.$setValidity('postcodeInUse', true);
        }
        else {
            // School exists - show popup
        }
    }

    // Close schools preview if correct
    $('#register-form').on('click', function (e) {
        if ($scope.schoolsFound) {
            if ((e.target.nodeName == 'INPUT' || e.target.nodeName == 'SELECT') && e.target.id != 'orgpostcode' && $scope.registerForm.orgpostcode.postcodeInUse) {
                $scope.schoolsFound = null;
                $scope.schoolChosen = false;
                $scope.$apply();
            }
        }
    });

    /*--- Postcode Validation END ---*/


    if ($location.search().id) {
        // Look up school detail if there is an id for it.
        $scope.lookupSchool();
    }

    $scope.registerSchool = function () {
        $http.post($scope.serverstring + 'api/v1.0/organization/registerOrganization', {
            formdata: $scope.formdata
        }).
            success(function (data, status, headers, config) {
                // message about email.  Modal?
                if (angular.isObject(data)) {
                    $scope.response = data;
                    $scope.result = data.data;
                    $scope.messages = data.messages;

                    var template, controller;

                    template = 'pages/modals/registered.html';
                    controller = 'registerMessageController';

                    $scope.selected = null;

                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: template,
                        controller: controller,
                        size: 'lg',
                        resolve: {
                            param: function () {
                                return { response: $scope.response };
                            }
                        }
                    });

                    modalInstance.result.then(function (selectedItem) {
                        // $scope.selected = selectedItem;
                    }, function () {
                        // Dismissed.
                    });

                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });

    };

});

uesApp.controllerProvider.register('registerMessageController', function ($scope, $uibModalInstance, $location, param) {
    $scope.data = param;

    $scope.ok = function () {
        $uibModalInstance.close();
        $location.path('/login');
    };

});