/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('SuController', function ($scope,$http,$uibModal,$log) {

    $scope.modalResult = null;
    $scope.test = {};
    $scope.test.searchString = null;
    $scope.test.orgID = null;
    $scope.test.limit = 7;
    $scope.searchresult = null;
    $scope.productresult = null;

    //initiate POST method everytime user type in and changes the searchString variable
    $scope.$watch('test.searchString', function(){
        $http.get('api/v1.0/organization/lookupregistered?query='+ $scope.test.searchString+'&limit='+$scope.test.limit).success(function(data, status, headers, config) {
            $scope.searchresult = data;
            //error message
        }).error(function(data, status, headers, config) {
            console.log("No data found..");
            $scope.searchresult = null;
        });
    });

    $scope.$watch('test.orgID', function(){
        $http.get('api/v1.0/organization/listproducts?orgid='+ $scope.test.orgID)
        .success(function(data, status, headers, config) {
            $scope.productresult = data;
            //error message
        }).error(function(data, status, headers, config) {
            console.log("No data found..");
            $scope.productresult = null;
        });
    });

});
