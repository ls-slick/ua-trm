/**
 * Created by andyl on 16/12/2015.
 */
var saveAs = saveAs || (function(view) {
        "use strict";
        // IE <10 is explicitly unsupported
        if (typeof view === "undefined" || typeof navigator !== "undefined" && /MSIE [1-9]\./.test(navigator.userAgent)) {
            return;
        }
        var
            doc = view.document
        // only get URL when necessary in case Blob.js hasn't overridden it yet
            , get_URL = function() {
                return view.URL || view.webkitURL || view;
            }
            , save_link = doc.createElementNS("http://www.w3.org/1999/xhtml", "a")
            , can_use_save_link = "download" in save_link
            , click = function(node) {
                var event = new MouseEvent("click");
                node.dispatchEvent(event);
            }
            , is_safari = /constructor/i.test(view.HTMLElement) || view.safari
            , is_chrome_ios =/CriOS\/[\d]+/.test(navigator.userAgent)
            , setImmediate = view.setImmediate || view.setTimeout
            , throw_outside = function(ex) {
                setImmediate(function() {
                    throw ex;
                }, 0);
            }
            , force_saveable_type = "application/octet-stream"
        // the Blob API is fundamentally broken as there is no "downloadfinished" event to subscribe to
            , arbitrary_revoke_timeout = 1000 * 40 // in ms
            , revoke = function(file) {
                var revoker = function() {
                    if (typeof file === "string") { // file is an object URL
                        get_URL().revokeObjectURL(file);
                    } else { // file is a File
                        file.remove();
                    }
                };
                setTimeout(revoker, arbitrary_revoke_timeout);
            }
            , dispatch = function(filesaver, event_types, event) {
                event_types = [].concat(event_types);
                var i = event_types.length;
                while (i--) {
                    var listener = filesaver["on" + event_types[i]];
                    if (typeof listener === "function") {
                        try {
                            listener.call(filesaver, event || filesaver);
                        } catch (ex) {
                            throw_outside(ex);
                        }
                    }
                }
            }
            , auto_bom = function(blob) {
                // prepend BOM for UTF-8 XML and text/* types (including HTML)
                // note: your browser will automatically convert UTF-16 U+FEFF to EF BB BF
                if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(blob.type)) {
                    return new Blob([String.fromCharCode(0xFEFF), blob], {type: blob.type});
                }
                return blob;
            }
            , FileSaver = function(blob, name, no_auto_bom) {
                if (!no_auto_bom) {
                    blob = auto_bom(blob);
                }
                // First try a.download, then web filesystem, then object URLs
                var
                    filesaver = this
                    , type = blob.type
                    , force = type === force_saveable_type
                    , object_url
                    , dispatch_all = function() {
                        dispatch(filesaver, "writestart progress write writeend".split(" "));
                    }
                // on any filesys errors revert to saving with object URLs
                    , fs_error = function() {
                        if ((is_chrome_ios || (force && is_safari)) && view.FileReader) {
                            // Safari doesn't allow downloading of blob urls
                            var reader = new FileReader();
                            reader.onloadend = function() {
                                var url = is_chrome_ios ? reader.result : reader.result.replace(/^data:[^;]*;/, 'data:attachment/file;');
                                var popup = view.open(url, '_blank');
                                if(!popup) view.location.href = url;
                                url=undefined; // release reference before dispatching
                                filesaver.readyState = filesaver.DONE;
                                dispatch_all();
                            };
                            reader.readAsDataURL(blob);
                            filesaver.readyState = filesaver.INIT;
                            return;
                        }
                        // don't create more object URLs than needed
                        if (!object_url) {
                            object_url = get_URL().createObjectURL(blob);
                        }
                        if (force) {
                            view.location.href = object_url;
                        } else {
                            var opened = view.open(object_url, "_blank");
                            if (!opened) {
                                // Apple does not allow window.open, see https://developer.apple.com/library/safari/documentation/Tools/Conceptual/SafariExtensionGuide/WorkingwithWindowsandTabs/WorkingwithWindowsandTabs.html
                                view.location.href = object_url;
                            }
                        }
                        filesaver.readyState = filesaver.DONE;
                        dispatch_all();
                        revoke(object_url);
                    }
                    ;
                filesaver.readyState = filesaver.INIT;

                if (can_use_save_link) {
                    object_url = get_URL().createObjectURL(blob);
                    setImmediate(function() {
                        save_link.href = object_url;
                        save_link.download = name;
                        click(save_link);
                        dispatch_all();
                        revoke(object_url);
                        filesaver.readyState = filesaver.DONE;
                    }, 0);
                    return;
                }

                fs_error();
            }
            , FS_proto = FileSaver.prototype
            , saveAs = function(blob, name, no_auto_bom) {
                return new FileSaver(blob, name || blob.name || "download", no_auto_bom);
            }
            ;

        // IE 10+ (native saveAs)
        if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob) {
            return function(blob, name, no_auto_bom) {
                name = name || blob.name || "download";

                if (!no_auto_bom) {
                    blob = auto_bom(blob);
                }
                return navigator.msSaveOrOpenBlob(blob, name);
            };
        }

        // todo: detect chrome extensions & packaged apps
        //save_link.target = "_blank";

        FS_proto.abort = function(){};
        FS_proto.readyState = FS_proto.INIT = 0;
        FS_proto.WRITING = 1;
        FS_proto.DONE = 2;

        FS_proto.error =
            FS_proto.onwritestart =
                FS_proto.onprogress =
                    FS_proto.onwrite =
                        FS_proto.onabort =
                            FS_proto.onerror =
                                FS_proto.onwriteend =
                                    null;

        return saveAs;
    }(
        typeof self !== "undefined" && self
        || typeof window !== "undefined" && window
        || this
    ));


var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('DonationProcessingController', function ($scope, $http, $timeout, $window, $filter) {

    $scope.dateTimeOptions = {minView: 'day'};

    $scope.isEndCollapsed = true;
    $scope.isStartCollapsed = true;
    $scope.loading = false;
    $scope.messages = [];
    $scope.maxSize = 9;

    $scope.sales = {
        page: 1,
        total: 0,
        file: null
    };

    $scope.donations = {
        date: null,
        dateView: 'Pick a date',
        page: 1,
        total: 0,
        file: null
    };

    $scope.failed = {
        page: 1,
        file: null,
        rows: {}
    };

    $scope.returned = {
        page: 1,
        file: null,
        rows: {}
    };

    $scope.users = {
        paymentDate: null,
        paymentDateView: 'Pick a date',
        periodFromDate: null,
        periodFromDateView: 'Pick a date',
        periodToDate: null,
        periodToDateView: 'Pick a date',
        page: 1,
        total: 0,
        lastPeriodEnd: null
    };

    $scope.$watch('donations.date', function (newValue) {
        $scope.donations.dateView = $filter('date')(newValue, 'MMM d, y');
    });

    $scope.$watch('users.paymentDate', function (newValue) {
        $scope.users.paymentDateView = $filter('date')(newValue, 'MMM d, y');
    });

    $scope.$watch('users.periodFromDate', function (newValue) {
        $scope.users.periodFromDateView = $filter('date')(newValue, 'MMM d, y');
    });

    $scope.$watch('users.periodToDate', function (newValue) {
        $scope.users.periodToDateView = $filter('date')(newValue, 'MMM d, y');
    });

    $scope.clearSales = function(){
        if(confirm('Are You sure you want to mark remaining sales as processed?')){
            $http.get($scope.serverstring+'api/v1.0/donations/clearremainingsales').
                success(function (data, status, headers, config) {
                    if (angular.isObject(data)) {
                        // save CSV here.
                        $timeout(function () {
                            $scope.messages = data.messages;
                            $timeout(function () {
                                // Make sure they see the success message before it closes.
                                $scope.messages = [];
                            }, 3000);
                        });
                        $scope.updateSales();
                    }
                }).
                error(function (data, status, headers, config) {
                    // log error
                    if (angular.isObject(data)) {
                        $timeout(function () {
                            $scope.messages = data.messages;
                            $timeout(function () {
                                // Make sure they see the success message before it closes.
                                $scope.messages = [];
                            }, 3000);
                        });
                    }
                });
        }

    };


    $scope.startProcess = function(){
        $http.get($scope.serverstring+'api/v1.0/donations/processsales').
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                    $scope.sales.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                if (angular.isObject(data)) {
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                }
            });
    };

    $scope.removeFailedPayments = function(){
        $http.post($scope.serverstring+'api/v1.0/donations/removefailed',{
            removerows: $scope.failed.rows
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    // save CSV here.
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                if (angular.isObject(data)) {
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                }
            });
    };


    $scope.downloadFixedFile = function(){
        $http.get($scope.serverstring+'api/v1.0/donations/downloadfixed').
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                  // save CSV here.
                    var file = new Blob([data.data.filecontent], {type: 'text/csv'});
                   // var fileURL = URL.createObjectURL(file);
                  //  window.open(fileURL);
                    saveAs(file, data.data.filename);
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                if (angular.isObject(data)) {
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                }
            });
    };


    $scope.applyReturns = function(){
        $http.get($scope.serverstring+'api/v1.0/donations/applyreturns').
            success(function (data, status, headers, config) {
                $timeout(function () {
                    $scope.messages = data.messages;
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        $scope.messages = [];
                    }, 3000);
                });
                $scope.updateReturnedPayments();
            }).
            error(function (data, status, headers, config) {
                // log error
                if (angular.isObject(data)) {
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                    $scope.updateReturnedPayments();
                }
            });
    };

    $scope.startPaymentProcess = function(){
        $http.post($scope.serverstring+'api/v1.0/donations/processpayments',{
            date:  $scope.donations.date
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                    $scope.sales.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                if (angular.isObject(data)) {
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                }
            });
    };

    $scope.startEmailProcess = function(){
        $http.post($scope.serverstring+'api/v1.0/donations/processemails',
            $scope.users
        ).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                    $scope.sales.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                if (angular.isObject(data)) {
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                }
            });
    };

    $scope.uploadSales = function() {
        $scope.loading = true;
        // Upload this file to the temp tables.
        $http.post($scope.serverstring+'api/v1.0/donations/uploadsales', {
            "file": $scope.sales.file
        }).
        success(function (data, status, headers, config) {
            if (angular.isObject(data)) {
                $scope.sales.file = null;
                $scope.sales.page = 1;
                $scope.loading = false;
                $scope.updateSales();
            }
        }).
            error(function (data, status, headers, config) {
                // log error
                $scope.loading = false;
            });
    };

    $scope.uploadDonationPayments = function() {
        $scope.loading = true;
        // Upload this file to the temp tables.
        $http.post($scope.serverstring+'api/v1.0/donations/uploadpayments', {
            "file": $scope.donations.file,
            "date": $scope.donations.date
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.donations.file = null;
                    $scope.donations.page = 1;
                    $scope.loading = false;
                    $scope.updatePayments();

                    //$scope.donations.payments = data.data;
                    //$scope.donations.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.loading = false;
            });
    };

    $scope.uploadFailedPayments = function() {
        $scope.loading = true;
        // Upload this file to the temp tables.
        $http.post($scope.serverstring+'api/v1.0/donations/uploadfailedpayments', {
            "file": $scope.failed.file
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.failed.file = null;
                    $scope.failed.page = 1;
                    $scope.loading = false;
                    $scope.updateFailedPayments();
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                    //$scope.donations.payments = data.data;
                    //$scope.donations.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.loading = false;
                $timeout(function () {
                    $scope.messages = data.messages;
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        $scope.messages = [];
                    }, 3000);
                });
            });
    };

    $scope.uploadReturnedPayments = function() {
        $scope.loading = true;
        // Upload this file to the temp tables.
        $http.post($scope.serverstring+'api/v1.0/donations/uploadreturnedpayments', {
            "file": $scope.returned.file
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.returned.file = null;
                    $scope.returned.page = 1;
                    $scope.loading = false;
                    $scope.updateReturnedPayments();
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                    //$scope.donations.payments = data.data;
                    //$scope.donations.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.loading = false;
                $timeout(function () {
                    $scope.messages = data.messages;
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        $scope.messages = [];
                    }, 3000);
                });
            });
    };

    $scope.updatePayments = function(){
        $http.post($scope.serverstring+'api/v1.0/donations/getpaymentswaiting', {
            "p": $scope.donations.page
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.donations.payments = data.data;
                    $scope.donations.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

    $scope.updateFailedPayments = function(){
        $http.post($scope.serverstring+'api/v1.0/donations/getfailedpayments').
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.failed.payments = data.data;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

    $scope.updateReturnedPayments = function(){
        $http.post($scope.serverstring+'api/v1.0/donations/getreturnedpayments').
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.returned.payments = data.data;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

    $scope.updateSales = function(){
        $http.post($scope.serverstring+'api/v1.0/donations/getsales', {
            "p": $scope.sales.page
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.sales.sales = data.data;
                    $scope.sales.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

    $scope.updateEmails = function(){
        $http.post($scope.serverstring+'api/v1.0/donations/getuseremails', {
            "p": $scope.users.page
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.users.emails = data.data;
                    $scope.users.total = data.totalrows;
                    $scope.users.lastPeriodEnd = data.lastPeriodEnd;
                }
            }).
            error(function (data, status, headers, config) {
                // log error

            });
    };


    $scope.downloadReport = function(type) {
        $scope.loadingON();
        var parameters = {};
        var endpoint;
        switch (type) {
            default:
                endpoint = 'donationpaymentlists';
                break;
        }

        $http.post($scope.serverstring + 'api/v1.0/reports/' + endpoint,
            parameters
        ).success(function(data, status, headers, config) {
                if (angular.isObject(data)) {
                    // just open a new window on that?
                    parameters = {};
                    $window.open(data.data.filename);
                    $scope.loadingOFF();
                }
            }).
            error(function(data, status, headers, config) {
                // log error
                console.log(new Error(data));
                alert('There was a problem loading this file - please try again');
                $scope.loadingOFF();
            });
    };

    $scope.resetUserStatus = function(type) {
        $scope.loadingON();

        $http.post($scope.serverstring + 'api/v1.0/donations/resetuseremail',
                $scope.users
        )

            .success(function(data, status, headers, config) {
                if (angular.isObject(data)) {
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                }
                $scope.loadingOFF();
                $scope.updateEmails();
            }).
            error(function(data, status, headers, config) {
                if (angular.isObject(data)) {
                    $timeout(function () {
                        $scope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            $scope.messages = [];
                        }, 3000);
                    });
                }
                $scope.loadingOFF();
            });
    };

    /* Pagination */
    $scope.pageChanged = function(currentPage) {
        //$scope.apage = currentPage;
        //console.log('Page changed to: ' + currentPage);
        $scope.updateSales();
        $scope.updatePayments();
        $scope.updateEmails();
    };

    $scope.loadingON = function() {
        $timeout(function() {
            $scope.loading = true;
            return;
        })
    };

    $scope.loadingOFF = function() {
        $timeout(function() {
            $scope.loading = false;
            return;
        })
    }
});
