/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('NominateController', function ($scope, $uibModalInstance) {

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});
