/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('AdController2', function($scope,$http,$rootScope,$uibModal,$location,$route) {
    $scope.test = 'sdfsdf';
    $scope.$parent.test = 'Testing';
    //-------MODAL------/
    $scope.$parent.cancel = function() {
        $scope.$parent.uibModalInstance.dismiss();
    };

    //initiate POST method everytime user type in and changes the searchString variable
    $scope.$parent.$watch('searchString', function(){
        $http.post($rootScope.serverstring+'api/v1.0/organization/adminlookup', {
            q: $scope.$parent.searchString
        }).success(function(data, status, headers, config) {

            $scope.$parent.data = data.data;

            //error message
        }).error(function(data, status, headers, config) {
            console.log("No data found..");
            $scope.$parent.data = null;
        });
    });

    $scope.$parent.viewOrg = function (orgid){
        if(orgid){
            $location.path('/orginfo/'+orgid);
        }
    };

    $scope.$parent.showModal = function(type, size, options) {
        var template, controller;
        switch (type) {
            case 'adminTasksEdit':
                template = 'pages/modals/admin-tasks-edit.html';
                controller = 'AdminTasksEditController';
                break;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: template,
            controller: controller,
            size: size,
            resolve: {
                options: function() {
                    return options;
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.$parent.selected = selectedItem;
        }, function() {
           // $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.$parent.showProspect = function(prospectData) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/show-prospect.html',
            controller: 'ProspectController',
            size: 'lg',
            resolve: {
                options: function() {
                    return prospectData;
                }
            }
        });
    };

});


//---------------------------------------------------------------------//
//------------------------ViewEmblemController-------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('ProspectController', function($scope, $uibModalInstance,options,$sce) {

    //------DATA------/
    $scope.prospectData = options;

    //------MODAL-----/
    $scope.ok = function() {
        $uibModalInstance.close(true);
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

});