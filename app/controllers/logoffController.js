/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('LogoffControllerB', function ($scope,$interval, $route, $rootScope, routeFactory, $http, $location, cssInjector) {
    $scope.doLogoff = function () {
        $http.get($scope.serverstring+'api/v1.0/login/destroy').
            success(function(data, status, headers, config) {
                $rootScope.session = null;

                routeFactory.clearRoutes();

                routeFactory.addRoute('/session',{
                    templateUrl: 'pages/session.html',
                    title: ''
                });

                $route.routes['null'] = angular.extend({
                    redirectTo: '/session',
                    reloadOnSearch: true,
                    caseInsensitiveMatch: false
                });

                cssInjector.removeAll();

                $interval.cancel($rootScope.sessionTimer);


                $location.path('/session'); //reload routes from session.
            }).
            error(function(data, status, headers, config) {
                $rootScope.session = null;

                routeFactory.clearRoutes();

                routeFactory.addRoute('/session',{
                    templateUrl: 'pages/session.html',
                    title: ''
                });
                $route.routes['null'] = angular.extend({
                    redirectTo: '/session',
                    reloadOnSearch: true,
                    caseInsensitiveMatch: false
                });

                cssInjector.removeAll();

                $location.path('/session'); //reload routes from session.
            })
    };


    $scope.ok = function () {
        $scope.doLogoff();
    };

    $scope.cancel = function () {
        //reload session
        $location.path('/session');
    };

});

uesApp.controllerProvider.register('LogoffController', function ($scope, $http, $location, $route, $rootScope, routeFactory, $uibModal) {

        $scope.showModal = function () {
            var template, controller;

            template = 'pages/modals/logoff.html';
            controller = 'LogoffControllerB';

            $scope.selected = null;

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: template,
                controller: controller,
                size: 'md'
            });

            modalInstance.result.then(function (selectedItem) {
                // $scope.selected = selectedItem;
            }, function () {
                // Dismissed.
            });

        };


    $scope.showModal('lg');
});