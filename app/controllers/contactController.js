/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('ContactController', function ($scope,$http) {

    //contactData
    $scope.contactData = {};
    $scope.contactData.firstName = '';
    $scope.contactData.lastName = '';
    $scope.contactData.email = '';
    $scope.contactData.message = '';

    //apply user data to contact form if logged
    if ($scope.session) {
        $scope.contactData.firstName = $scope.session.user.user_first_name;
        $scope.contactData.lastName = $scope.session.user.user_last_name;
        $scope.contactData.email = $scope.session.user.user_email;
        if ($scope.session.organizations[$scope.session.showingorg]) {
            if ($scope.session.organizations[$scope.session.showingorg].organization_parent_id) {
                $scope.contactData.urn = $scope.session.organizations[$scope.session.showingorg].organization_parent_id;
            }
            else {
                $scope.contactData.urn = $scope.session.organizations[$scope.session.showingorg].school_URN;
            }
        }
        else {
            $scope.contactData.urn = '';
        }
    }
    
    //messages from API
    $scope.messages = null;
    $scope.formResponse = {};

    $scope.sendForm = function () {
        $http.post($scope.serverstring+'api/v1.0/forms/sendcontact', {
            formdata: $scope.contactData
        }).
        success(function (data, status, headers, config) {

            if(angular.isObject(data)) {
                $scope.messages = data.messages;
                $scope.formResponse = data;
                // clear the form
                $scope.contactData.firstName = '';
                $scope.contactData.lastName = '';
                $scope.contactData.email = '';
                $scope.contactData.message = '';
                $scope.contactForm.$setPristine();
            }
        }).
        error(function (data, status, headers, config) {
            // log error
            $scope.messages = data.messages;
        });
    };

    //cancel
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

});
