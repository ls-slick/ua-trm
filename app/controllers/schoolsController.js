/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('SchoolsController', function ($scope) {

    $scope.schools =
        [
            {
                test: 'abc'
            },{
            test: 'def'
        },{
            test: 'ghi'
        }
        ]; // stub object for development

    /* Pagination */
    $scope.totalItems = 64;
    $scope.currentPage = 4;

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function() {
        $log.log('Page changed to: ' + $scope.currentPage);
    };

    $scope.maxSize = 9;
    $scope.bigTotalItems = 175;
    $scope.bigCurrentPage = 1;

});
