/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('AdminOrgController', function ($scope, $rootScope, $http, $location, $route, $uibModal, $timeout, $filter, $log, getOrgNotesFactory) {
    var getVar = $route.current.params;

    $scope.notes = {};
    $scope.form = {};
    $scope.schoolBankData = {};

    $scope.selectedColourStyle = '';
    $scope.selectedGMOStyle = '';
    $scope.selectedColourImage = '';
    $scope.selectedPlainColourStyle = '';
    $scope.selectedPlainColourImage = '';
    $scope.selectedLogoImage = '';
    $scope.selectedLogoBackground = '';
    $scope.newproducts = [];
    $scope.newcolours = [];
    $scope.newlogos = [];

    $scope.newproductData = {
        orgid: getVar.orgid
    };

    $scope.newplainProductData = {
        orgid: getVar.orgid
    };

    $scope.schoolName = '';

    if (!getVar.orgid) {
        // not allowed here without an orgid
        $location.path('/uniformschools');
    }

    // Alert messages function
    var messageTimeout = null;
    $scope.showAlertMessage = function (message, time) {
        // clear timer if already on
        if (messageTimeout != null) {
            $timeout.cancel(messageTimeout);
        }
        // assign message
        $scope.messages = message;
        // clear message
        messageTimeout = $timeout(function () {
            $scope.messages = [];
        }, time);
        return messageTimeout;
    };

    $scope.registerBank = function () {
        var orgid = getVar.orgid;
        //  var orgid = 5432;
        $http.post($scope.serverstring + 'api/v1.0/admin/updateorgbank', {
            updateFormDetails: $scope.orgdetail
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    // updated.
                    $scope.showAlertMessage(data.messages, 3000);
                    $scope.form.bankDetailsForm.$setPristine();
                    $scope.getOrgDetail();
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.showAlertMessage(data.messages, 3000);
            });
    };

    $scope.sendupdate = function () {
        var orgid = getVar.orgid;
        //  var orgid = 5432;
        $http.post($scope.serverstring + 'api/v1.0/admin/updateorg', {
            updateFormDetails: $scope.orgdetail
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    // updated.
                    $scope.showAlertMessage(data.messages, 3000);
                    $scope.form.updateOrgForm.$setPristine();
                    $scope.getOrgDetail();

                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.showAlertMessage(data.messages, 3000);
            });
    };


    $scope.getOrgNotes = function () {
        var orgid = getVar.orgid;
        getOrgNotesFactory.updateNotes(orgid)
            .success(function (data) {
                $scope.orgnotes = data.data;
                $scope.orgnotespriv = data.datapriv;
            })
            .error(function (data) {
                $scope.orgnotes = {};
                $scope.orgnotespriv = {};
            });
    }

    $scope.deleteNoteAlert = function (note) {
        if (note) {
            var modalInstanceShow = $uibModal.open({
                animation: true,
                templateUrl: 'pages/modals/confirm.html',
                controller: 'AdminDeleteNoteAlertController',
                size: 'md'
            });
            modalInstanceShow.result.then(function (deleteNoteAlert) {
                if (deleteNoteAlert) {
                    $http.post($scope.serverstring + 'api/v1.0/admin/deletenote', {
                        ticket_id: note.ticket_id
                    }).success(function (data, status, headers, config) {
                        // on success
                        $scope.loading = 0;
                        $scope.getOrgNotes();
                        // show alert message
                        $scope.showAlertMessage(data.messages, 3000);
                    }).error(function (data, status, headers, config) {
                        // log error
                        $scope.loading = 0;
                        // show alert message
                        $scope.showAlertMessage(data.messages, 3000);
                    });
                }
            },
                function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
        } else {
            return;
        }
    }

    // $scope.getOrgNotes = function() {

    //     var orgid = getVar.orgid;
    //     //  var orgid = 5432;
    //     $http.get($scope.serverstring + 'api/v1.0/admin/getorgnotes?orgid=' + orgid).
    //     success(function(data, status, headers, config) {
    //         if (angular.isObject(data)) {
    //             $scope.orgnotes = data.data;
    //         }
    //     }).
    //     error(function(data, status, headers, config) {
    //         // log error
    //         $scope.orgnotes = {};
    //     });
    // };

    $scope.getOrgAudit = function () {
        var orgid = getVar.orgid;
        //   var orgid = 5432;
        $http.get($scope.serverstring + 'api/v1.0/admin/getorgaudit?orgid=' + orgid).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.auditlogs = data.data;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.auditlogs = {};
                $scope.showAlertMessage(data.messages, 3000);
            });
    };

    $scope.getOrgDetail = function () {

        $scope.linktime = Date.now();

        var orgid = getVar.orgid;

        console.log("Firefly: Checking posted id:" +orgid );
        //  var orgid = 5432;
        $http.get($scope.serverstring + 'api/v1.0/admin/getorgdetail?orgid=' + orgid).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.orgdetail = data.data.orgdetail;
                    $scope.usersdetail = data.data.users;
                    $scope.housedetail = data.data.houses;
                    $scope.subhousedetail = data.data.subhouses;


                    $scope.orgadminsdetail = data.data.orgadmins;

                    $scope.emblemdetail = data.data.emblems;
                    $scope.emblemsApproved = $filter('filter')($scope.emblemdetail, { logo_approved: 1 });
                    $scope.donationdetail = data.data.donations;

                    //amount of donations to show
                    $scope.donationsAmount = ($scope.donationdetail).length;
                    $scope.initialDonationsToShow = 5; //how many donations to show?
                    if ($scope.initialDonationsToShow < $scope.donationsAmount) {
                        $scope.hideAdditionalDonations = true;
                    }

                    $scope.orgdetail.organization_member_count = parseInt($scope.orgdetail.organization_member_count);
                    $scope.orgdetail.organization_welcome_pack = parseInt($scope.orgdetail.organization_welcome_pack);

                    if ($scope.emblemsApproved) {
                        $scope.usableEmblems = $scope.emblemsApproved;
                    }
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.orgdetail = {};
            });
    };

    $scope.getOrgProducts = function () {

        var orgid = getVar.orgid;
        //  var orgid = 5432;
        $http.get($scope.serverstring + 'api/v1.0/admin/getorgproducts?orgid=' + orgid).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.orgproducts = data.products;
                    $scope.orgplainproducts = data.plainproducts;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.orgdetail = {};
            });

        $http.get($scope.serverstring + 'api/v1.0/admin/getembstyles').
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.newproducts = data.data;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.newproducts = {};
            });


        $http.get($scope.serverstring + 'api/v1.0/admin/getplnstyles').
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.newplainproducts = data.data;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.newplainproducts = {};
            });

    };


    $scope.getProductColours = function (productID, object) {
        $http.get($scope.serverstring + 'api/v1.0/admin/getproductcolours?pid=' + productID).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope[object] = data.data;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                return null;
            });
    };


    //maximum emblem size for small products
    var smallProductEmblemMaxSize = 61;
    $scope.$watch('newproductData.productid', function (newVal, oldVal) {
        if (newVal !== undefined) {
            $scope.selectedColourStyle = '';
            $scope.selectedGMOStyle = '';
            $scope.selectedColourImage = '';
            $scope.newproductData.colour = '';
            $scope.selectedLogoImage = '';
            $scope.newproductData.logo = '';

            //pick selected product object from list
            for (var key in $scope.newproducts) {
                if ($scope.newproducts[key].product_id == newVal) {
                    var selectedproduct = $scope.newproducts[key];
                }
            }
            //create emblems array based on a chosen product
            $scope.usableEmblems = $scope.emblemsApproved;

            if (selectedproduct['product_emblem_small'] == '1') {
                $scope.usableEmblems = $filter('smallLogo')($scope.usableEmblems, { maxSize: 61 });
            }

            $scope.getProductColours(newVal, 'newcolours');

        }
    });

    $scope.$watch('newproductData.colour', function (newVal, oldVal) {
        if (newVal !== undefined && newVal !== '') {
            $scope.selectedColourStyle = $scope.newcolours[newVal].product_colour_style_ref;
            $scope.selectedGMOStyle = $scope.newcolours[newVal].product_colour_gmo_ref;
            $scope.selectedColourImage = $scope.newcolours[newVal].product_colour_image_url;
            $scope.selectedLogoBackground = $scope.newcolours[newVal].colour_rgb;
        }
    });

    $scope.$watch('newproductData.logo', function (newVal, oldVal) {
        if (newVal !== undefined && newVal !== '') {
            var foundItem = $filter('filter')($scope.emblemdetail, { organization_logo_id: newVal }, true)[0];
            $scope.selectedLogoImage = foundItem.logo_url;
        }
    });

    $scope.$watch('newplainProductData.productid', function (newVal, oldVal) {
        if (newVal !== undefined && newVal !== '') {
            //alert('changed'+newVal);
            $scope.newplainProductData.colour = '';
            $scope.selectedPlainColourStyle = '';
            $scope.selectedPlainColourImage = '';
            $scope.getProductColours(newVal, 'newplaincolours');

        }
    });

    $scope.$watch('newplainProductData.colour', function (newVal, oldVal) {
        if (newVal !== undefined) {
            $scope.selectedPlainColourStyle = $scope.newplaincolours[newVal].product_colour_style_ref;
            $scope.selectedPlainColourImage = $scope.newplaincolours[newVal].product_colour_image_url;
        }
    });


    $scope.getOrgDetail();

    $scope.user = null;
    $scope.messages = null;

    $scope.doReset = function (user) {
        $http.post($scope.serverstring + 'api/v1.0/admin/resetpassword', {
            "user": user
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    // Make sure they see the success message before it closes.
                    $scope.showAlertMessage(data.messages, 3000);
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.showAlertMessage(data.messages, 3000);
            });
    };

    $scope.resetPassword = function (user) {
        // modal.
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/resetpass.html',
            controller: 'ResetPasswordController',
            size: 'md',
            resolve: {
                options: function () {
                    return user;
                }
            }
        });
        modalInstance.result.then(function () {
            // success?
            // Clear any error messages.
            $scope.messages = [];
            $scope.doReset(user);
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
            //alert('NO');
        });

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.modalResult = null;
    $scope.showModal = function (type, size, options) {
        var template, controller;
        switch (type) {
            case 'userAdd':
                template = 'pages/modals/admin-user-modal.html';
                controller = 'UserAddController';
                break;
            case 'userEdit':
                template = 'pages/modals/admin-user-modal.html';
                controller = 'UserEditController';
                break;
            case 'makeUserPrimary':
                template = 'pages/modals/admin-user-modal.html';
                controller = 'MakeUserPrimaryController';
                break;
            case 'deleteUser':
                template = 'pages/modals/admin-user-modal.html';
                controller = 'DeleteUserController';
                break;
            case 'addHouse':
                template = 'pages/modals/admin-house-modal.html';
                controller = 'AddHouseController';
                break;
            case 'houseEdit':
                template = 'pages/modals/admin-house-modal.html';
                controller = 'HouseEditController';
                break;
            case 'addNote':
                template = 'pages/modals/admin-add-note.html';
                controller = 'AddNoteController';
                break;
            case 'viewEmblem':
                template = 'pages/modals/viewAdminEmblem.html';
                controller = 'ViewEmblemController';
                break;
            case 'sendSample':
                template = 'pages/modals/sample-confirm.html';
                controller = 'SendSampleController';
                break;
            case 'addEmblem':
                backdrop = 'static';
                template = 'pages/modals/admin-add-emblem.html';
                controller = 'AdminAddEmblemController';
                break;
            case 'deleteEmblem':
                backdrop = 'static';
                template = 'pages/modals/viewAdminEmblem.html';
                controller = 'AdminDeleteEmblemController';
                break;
            case 'uploadDesign':
                template = 'pages/modals/admin-upload-design.html';
                controller = 'AdminUploadDesignConfirmController';
                break;
            case 'formProcess':
                template = 'pages/modals/processFormReceipt.html';
                controller = 'FormReceiptController';
                break;
            case 'suspendConfirm':
                template = 'pages/modals/admin-confirm.html';
                controller = 'SuspendConfirmController';
                break;
            case 'withdrawConfirm':
                template = 'pages/modals/admin-withdraw-org.html';
                controller = 'WithdrawConfirmController';
                break;
            case 'deleteHouse':
                template = 'pages/modals/admin-confirm.html';
                controller = 'DeleteHouseController';
                break;
            case 'deleteNoteAlert':
                template = 'pages/modals/admin-confirm.html';
                controller = 'AdminDeleteNoteAlertController';
                break;
        }

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: template,
            controller: controller,
            size: size,
            backdrop: 'static',
            resolve: {
                options: function () {
                    return options;
                },
                colorParameters: function () {
                    return $scope.activeColor;
                },
                originalscope: function () {
                    return $scope;
                }
            }
        });

        modalInstance.result.then(function (data) {
            $timeout(function () {
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    $scope.messages = [];
                }, 3000);
                $scope.messages = data;
                $scope.getOrgDetail();
                $scope.getOrgNotes();
            });

        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });

    };


    $scope.deleteProduct = function (product) {
        // do the delete here.
        $http.post($scope.serverstring + 'api/v1.0/admin/removeproduct', {
            deleteProdData: {
                "productColourOrgID": product.colour.products_colours_to_organizations_id,
                "organizationID": getVar.orgid
            }
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.showAlertMessage(data.messages, 3000);
                    //console.log($scope.messages);
                    $scope.getOrgProducts();
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.showAlertMessage(data.messages, 3000);
            });
    };

    $scope.addProduct = function (embroidered) {
        // do the add here.
        if (embroidered) {
            var passData = $scope.newproductData
        } else {
            var passData = $scope.newplainProductData
        }
        passData.embroidered = embroidered;

        $http.post($scope.serverstring + 'api/v1.0/admin/addproduct', {
            addProdData: passData
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    //$scope.response = data;
                    $scope.showAlertMessage(data.messages, 3000);
                    //console.log($scope.messages);
                    $scope.getOrgProducts();
                    $scope.getOrgDetail();
                    $scope.selectedColourStyle = '';
                    $scope.selectedGMOStyle = '';
                    $scope.selectedColourImage = '';
                    $scope.selectedLogoImage = '';
                    $scope.selectedLogoBackground = '';
                    $scope.newproductData = {
                        orgid: getVar.orgid
                    };
                    $scope.form.addEmbForm.$setPristine();

                    $scope.selectedPlainColourStyle = '';
                    $scope.selectedPlainColourImage = '';
                    $scope.newplainProductData = {
                        orgid: getVar.orgid
                    };
                    $scope.form.addPlnForm.$setPristine();
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.showAlertMessage(data.messages, 3000);
            });
    };

    //--------MODAL--------//
    $scope.modalResult = null;
    $scope.deleteProductConfirm = function (product) {

        if (product.product_decorated == "1") {
            var message = 'Are you sure you wish to remove <br /><br />' + product.product_name + '<br />' +
                product.colour.colour_name + '<br />' +
                product.colour.tesco_style_ref + '?<br /><br />';
        } else {
            var message = 'Are you sure you wish to remove <br /><br />' + product.product_name + '<br />' +
                product.colour.colour_name + '?<br /><br />';
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/confirm.html',
            controller: 'ConfirmController',
            size: 'md',
            resolve: {
                options: function () {
                    return {
                        title: 'Delete Product',
                        message: message
                    };
                }
            }
        });
        modalInstance.result.then(function (confirmStatus) {
            // close.
            if (confirmStatus) {
                // YES.
                $scope.deleteProduct(product);
            }
        }, function () {
            // dismiss
        });
    };

    $scope.suspendConfirm = function (orgdetail) {

        var description = '';
        var message = 'Are you sure you wish to suspend <br /><br />' + orgdetail.organization_name + '?<br /><br />';

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/confirm.html',
            controller: 'ConfirmController',
            size: 'md',
            resolve: {
                options: function () {
                    return {
                        title: 'Suspend Organization',
                        message: message,
                        description: description
                    };
                }
            }
        });
        modalInstance.result.then(function (confirmStatus) {
            // close.
            if (confirmStatus) {
                // YES.
                $http.post($scope.serverstring + 'api/v1.0/admin/suspendorg', {
                    orgData: orgdetail
                }).
                    success(function (data, status, headers, config) {
                        if (angular.isObject(data)) {
                            //$scope.response = data;
                            $timeout(function () {
                                $scope.showAlertMessage(data.messages, 3000);
                                $scope.getOrgDetail();
                            });
                            //console.log($scope.messages);
                        }
                    }).
                    error(function (data, status, headers, config) {
                        // log error
                        $timeout(function () {
                            $scope.messages = data.messages;
                            $timeout(function () {
                                // Make sure they see the success message before it closes.
                                $scope.messages = [];
                            }, 3000);
                        });
                    });
            }
        }, function () {
            // dismiss
        });
    };

    $scope.withdrawConfirm = function (orgdetail) {

        var message = 'Are you sure you wish to withdraw <br /><br />' + orgdetail.organization_name + '?<br /><br />';

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/confirm.html',
            controller: 'ConfirmController',
            size: 'md',
            resolve: {
                options: function () {
                    return {
                        title: 'Withdraw Organization',
                        message: message
                    };
                }
            }
        });
        modalInstance.result.then(function (confirmStatus) {
            // close.
            if (confirmStatus) {
                // YES.
                $http.post($scope.serverstring + 'api/v1.0/admin/withdraworg', {
                    orgData: orgdetail
                }).
                    success(function (data, status, headers, config) {
                        if (angular.isObject(data)) {
                            //$scope.response = data;
                            $timeout(function () {
                                $scope.showAlertMessage(data.messages, 3000);
                                $scope.getOrgDetail();
                            });
                            //console.log($scope.messages);
                        }
                    }).
                    error(function (data, status, headers, config) {
                        // log error
                        $timeout(function () {
                            $scope.showAlertMessage(data.messages, 3000);
                        });
                    });
            }
        }, function () {
            // dismiss
        });
    };

    $scope.reinstateConfirm = function (orgdetail) {

        var message = 'Are you sure you wish to re-instate <br /><br />' + orgdetail.organization_name + '?<br /><br />';

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/confirm.html',
            controller: 'ConfirmController',
            size: 'md',
            resolve: {
                options: function () {
                    return {
                        title: 'Re-Instate Organization',
                        message: message
                    };
                }
            }
        });
        modalInstance.result.then(function (confirmStatus) {
            // close.
            if (confirmStatus) {
                // YES.
                $http.post($scope.serverstring + 'api/v1.0/admin/reinstateorg', {
                    orgData: orgdetail
                }).
                    success(function (data, status, headers, config) {
                        if (angular.isObject(data)) {
                            //$scope.response = data;
                            $timeout(function () {
                                $scope.showAlertMessage(data.messages, 3000);
                                $scope.getOrgDetail();
                            });
                            //console.log($scope.messages);
                        }
                    }).
                    error(function (data, status, headers, config) {
                        // log error
                        $timeout(function () {
                            $scope.showAlertMessage(data.messages, 3000);
                        });
                    });
            }
        }, function () {
            // dismiss
        });
    };

    $scope.today = new Date();
    $scope.todaytime = $scope.today.getTime();

    // handle loading straight to a specific tab
    switch (getVar.tab) {
        case 'product':
            $scope.active = 1;
            $scope.getOrgProducts();
            break;
        case 'notes':
            $scope.active = 2;
            $scope.getOrgNotes();
            break;
        case 'audit':
            $scope.active = 3;
            $scope.getOrgAudit();
            break;
    }


    // Quick notes
    $scope.getQuickNote = function () {
        var orgid = getVar.orgid;
        $http.post('api/v1.0/admin/getquicknote', { organization_id: orgid }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.organization_note = data.quick_note;
                }
            }).error(function (data, status, headers, config) {
                // log error
                $scope.showAlertMessage(data.messages, 3000);
            });
    };
    $scope.getQuickNote();

    $scope.saveQuickNote = function (note) {
        $http.post($scope.serverstring + 'api/v1.0/admin/setquicknote', { orgData: orgdetail, }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.showAlertMessage(data.messages, 3000);
                    $scope.getOrgDetail();
                }
            }).error(function (data, status, headers, config) {
                // log error
                $scope.showAlertMessage(data.messages, 3000);
            });
    }

    // Update quickNote on event from admin buttons
    $scope.$on('updateNote', function () {
        $scope.getQuickNote();
    });

});

//---------------------------------------------------------------------//
//-----------------------Admin-AddEmblemController---------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('AdminAddEmblemController', function ($scope, $uibModal, $uibModalInstance, $uibModalStack, $timeout, $log, options, $http, originalscope, $sce, $filter, $route) {
    //------DATA------/
    var getVar = $route.current.params;
    $scope.emblems = originalscope.emblemdetail;
    $scope.requestData = {
        createOrder: true
    };
    $scope.messages = {};
    //width/height pattern
    $scope.onlyNumbers = /([0-9])+(.)/;
    //default logoType value - new emblem
    $scope.requestData.logoType = 'new';
    $scope.loading = 0;

    $scope.logoTypeButton = 'first';
    $scope.logoTypeChosen = null;
    //let user choose type of the logo
    $scope.chooseLogoType = function (choice) {
        $scope.requestData.logoType = '';
        $scope.requestData.logoType = choice;
    };

    //set flag of logo to reveal the form
    $scope.acceptLogoType = function () {
        $scope.logoTypeChosen = true;
    };

    // No emblems? Open new emblem option, hide others
    if ($scope.emblems.length < 1) {
        $scope.logoTypeChosen = true;
        $scope.requestData.logoType = 'new';
    }


    //-----MODAL------/
    $scope.modalResult = null;
    $scope.showModal = function (type, size, options) {
        var template, controller;
        switch (type) {
            case 'addExistingEmblem':
                template = 'pages/modals/add-existing-emblem.html';
                controller = 'AdminAddExistingEmblemController';
                break;
            case 'addEmblemConfirm':
                template = 'pages/modals/add-emblem-confirm.html';
                controller = 'AdminAddEmblemConfirmController';
                break;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: template,
            controller: controller,
            size: size,
            backdrop: 'static',
            resolve: {
                options: function () {
                    return options;
                },
                originalscope: function () {
                    return $scope;
                }
            }
        });

        //returns existing logo data
        $scope.chosenLogo = {};
        modalInstance.result.then(function (selectedLogo) {
            $scope.loading = 1;
            $scope.selected = selectedLogo;
            $scope.chosenLogo.backgroundColour = $scope.selected.primary_background_colour;
            $scope.chosenLogo.type = $scope.selected.type;
            $scope.chosenLogo.url = $scope.serverstring + $scope.selected.logo_url;
            $scope.chosenLogo.name = $scope.selected.tesco_style_ref;

            var foundItem = $filter('filter')($scope.allLogos, $scope.chosenLogo, true)[0];

            if (foundItem) {
                $scope.displayError('This emblem is already attached');
            } else {
                // not found.
                $scope.loading = 0;
                $scope.allLogos.push($scope.chosenLogo);
            }
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


    // new logo init variables
    $scope.allLogos = [];
    $scope.chosenLogo = {};
    $scope.thumbnail = {
        url: 0
    };
    $scope.fileReaderSupported = window.FileReader != null;
    $scope.maxsize = 2000000;

    //error message function
    $scope.displayError = function (message) {

        $scope.loading = 0;
        $timeout(function () {
            // Make sure they see the success message before it closes.
            originalscope.messages.push({
                type: 'danger',
                message: message
            });
            $timeout(function () {
                // Make sure they see the success message before it closes.
                originalscope.messages = [];
            }, 3000);
        });
        $scope.messages = {};
        originalscope.messages = [];
    };

    function formatSizeUnits(bytes) {
        if (bytes >= 1000000000) { bytes = (bytes / 1000000000).toFixed(2) + ' GB'; } else if (bytes >= 1000000) { bytes = (bytes / 1000000).toFixed(2) + ' MB'; } else if (bytes >= 1000) { bytes = (bytes / 1000).toFixed(2) + ' KB'; } else if (bytes > 1) { bytes = bytes + ' bytes'; } else if (bytes == 1) { bytes = bytes + ' byte'; } else { bytes = '0 byte'; }
        return bytes;
    }

    $scope.filePattern = new RegExp("(bat|exe|cmd|sh|php|pl|cgi|386|dll|com|torrent|js|app|jar|pif|vb|vbscript|wsf|asp|cer|csr|jsp|drv|sys|ade|adp|bas|chm|cpl|crt|csh|fxp|hlp|hta|inf|ins|isp|jse|htaccess|htpasswd|ksh|lnk|mdb|mde|mdt|mdw|msc|msi|msp|mst|ops|pcd|prg|reg|scr|sct|shb|shs|url|vbe|vbs|wsc|wsf|wsh)$");
    $scope.photoChanged = function (event) {
        $timeout(function () {
            var files = event.target.files;
            $scope.chosenLogo = {};
            $scope.loading = 1;
            if (files != null) {
                var file = files[0];
                var extension = file.name.split('.').pop().toLowerCase();
                //check file size(2mb)
                if (file.size > $scope.maxsize) {
                    $scope.displayError('Uploaded file size[' + formatSizeUnits(file.size) + '] exceeds maximum file size[' + formatSizeUnits($scope.maxsize) + ']');
                    file = null;
                } else {
                    //check for file extension
                    if ($scope.filePattern.test(extension)) {
                        $scope.displayError('Uploaded file has wrong extension.');
                    } else {
                        if ($scope.fileReaderSupported) {
                            $timeout(function () {
                                var fileReader = new FileReader();
                                fileReader.readAsDataURL(file);
                                fileReader.onload = function (e) {
                                    $scope.thumbnail.url = e.target.result;
                                    $scope.filetype = file.type;
                                    file.url = e.target.result;
                                    $scope.chosenLogo.isImage = file.type.indexOf('image') > -1;
                                    $scope.chosenLogo.url = e.target.result;
                                    $scope.chosenLogo.name = file.name;
                                    $scope.chosenLogo.type = file.type;
                                    var foundItem = $filter('filter')($scope.allLogos, $scope.chosenLogo, true)[0];

                                    //file already added?
                                    if (foundItem) {
                                        $scope.displayError('This file is already attached');
                                    } else {
                                        // file not found -> send file
                                        $timeout(function () {
                                            $scope.allLogos.push($scope.chosenLogo);
                                            $('#uploadfile').val(null);
                                            $scope.loading = 0;
                                        });
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    };

    //remove emblem from allLogos
    $scope.removeEmblem = function (i) {
        $scope.loading = 1;
        $timeout(function () {
            $scope.allLogos.splice(i, 1);
        });
        $scope.loading = 0;
        return $scope.allLogos;
    };

    //send request to API -> show confirm modal first
    $scope.sendRequest = function (i) {
        var modalInstanceShow = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/add-emblem-confirm.html',
            controller: 'AdminAddEmblemConfirmController',
            size: 'md'
        });
        //returns existing logo data
        $scope.chosenLogo = {};
        modalInstanceShow.result.then(function (addEmblem) {
            if (addEmblem) {
                $scope.loading = 1;
                $scope.requestData.attachments = $scope.allLogos;

                var getVar = $route.current.params;
                //CHANGE THIS <- valid admin id/not org(?)
                $scope.requestData.orgid = getVar.orgid;

                console.log($scope.requestData);
                $http.post($scope.serverstring + 'api/v1.0/admin/addemblemrequest', {
                    requestData: $scope.requestData
                }).success(function (data, status, headers, config) {
                    // on success
                    $scope.loading = 0;
                    originalscope.getOrgDetail();
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        originalscope.messages = [];
                    }, data.metrics.messagetime);

                    $uibModalStack.dismissAll();
                }).error(function (data, status, headers, config) {
                    // log error
                    $scope.loading = 0;
                    originalscope.messages = data.messages;

                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        originalscope.messages = [];
                    }, data.metrics.messagetime);
                });
            }
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };


    /* -------------------
     ADD EXISTING EMBLEM 
     -------------------*/
    $scope.selectedEmblem = {};
    $scope.requestExistingData = {};
    $scope.selectedExistingLogo = null;
    $scope.threadEditorOpen = false;
    $scope.hideNoteField = true;
    $scope.loadSVGError = null;

    $scope.editedThreads = [];


    // Get emblem threads as array 
    $scope.getEmblemThreads = function (emblemObj) {
        if (emblemObj.thread_array) {
            emblemObj.emblemThreads = emblemObj.thread_array.split(",");
            $scope.editedThreads = angular.copy(emblemObj.emblemThreads);
        }
    };


    $scope.selectExistingEmblem = function (emblem, index) {
        // Reset values
        $scope.selectedEmblem = {};
        $scope.selectedEmblem.description = '';
        $scope.hideNoteField = true; // note if not digitized yet
        $scope.selectedEmblemDigitized = null;
        $scope.selectedEmblemIndex = null;
        // Set values

        if ($scope.selectedEmblem) {
            $scope.selectedEmblem = emblem;
            $scope.selectedExistingLogo = emblem;
            $scope.selectedEmblemIndex = index;
            $scope.selectedEmblemDigitized = $scope.selectedEmblem.logo_digitized;
        }

        // Show note field if not digitized
        if ($scope.selectedEmblem != 'undefined' && $scope.selectedEmblemDigitized == "0") {
            $scope.hideNoteField = false;
            $timeout(function () {
                $(".modal").find('#selected-emblem-desc').focus();
            })
        }

        $scope.getEmblemThreads($scope.selectedEmblem);
        //console.log($scope.selectedEmblem);
        //console.log('Digitized? : ' + $scope.selectedEmblem.logo_digitized);
        //console.log('Index : ' + $scope.selectedEmblemIndex);
    };

    $scope.toggleEditor = function () {
        $scope.unselectThreads();
        if ($scope.threadEditorOpen) {
            $scope.threadEditorOpen = false;
            $scope.editLogo = false;
            hideEditLogoContent();
            console.log('Hiding editor');

        } else {
            $scope.threadEditorOpen = true;
            $scope.editLogo = true;
            showEditLogoContent();
            console.log('Showing editor');

        }
    }


    //-------------------//
    //---EDIT THREADS ---//
    //-------------------//

    $scope.getEmblemSVG = function () {
        $scope.emblemSVG = null;
        $scope.loadSVGError = false;
        $scope.loadingSVG = true;
        $scope.emblemSVGresized = false;
        $scope.emblemSVGloaded = false;
        $http.get($scope.serverstring + $scope.selectedEmblem.logo_svg_url + '?' + $scope.selectedEmblem.logo_last_updated).success(function (data) {
            $scope.emblemSVG = $sce.trustAsHtml(data);
            SVGloadedOnce = true;
            $scope.emblemSVGloaded = true;
            $scope.loadingSVG = false;
            $timeout(function () {
                $scope.resizeEmblem();
            })
        }).error(function (data) {
            $scope.loadSVGError = true;
            console.log('Unable to load design..');
        })
    };

    $scope.resizeEmblem = function () {
        var $svg = $(document).find("svg");
        $scope.svgAnimate = false;

        var $svg_height_prop = $svg.prop("height");
        var $svg_width_prop = $svg.prop("width");

        var svg_height = $svg_height_prop['baseVal']['value'];
        var svg_width = $svg_width_prop['baseVal']['value'];

        var SetSvgHeight = $svg.attr('height', '100%');
        var SetSvgWidth = $svg.attr('width', '100%');
        //var SetSvgViewBox = $svg.attr("viewBox", "0 0 " + svg_width + " " + svg_height);
        $svg.each(function () { $(this)[0].setAttribute('viewBox', "0 0 " + svg_width + " " + svg_height) });

        $scope.loading = false;
        $scope.loadingSVG = false;
        $scope.emblemSVGresized = true;
    }

    $scope.higlightThread = function (index) {
        for (var i = 0; i < $scope.editedThreads.length; i++) {
            //Highlight chosen thread
            if (i === index) {
                $('.colour' + i).css({ 'stroke-opacity': '1' });
                if ($scope.svgAnimate) {
                    $('.colour' + i).addClass('flashing');
                }
            } else {
                $('.colour' + i).css({ 'stroke-opacity': '.07' });
                if ($scope.svgAnimate) {
                    $('.colour' + i).removeClass('flashing');
                }
            }
        }
    }

    $scope.dontHighlightThreads = function () {
        for (var i = 0; i < $scope.editedThreads.length; i++) {
            $('.colour' + i).css({ 'stroke-opacity': '1' });
            $('.colour' + i).removeClass('flashing');

        }
    }

    // Choose thread to change
    $scope.chooseThread = function (selThreadIndex, event) {
        if (!$scope.loading && !$scope.loadingSVG) {
            if ($scope.editLogo) {
                if ($scope.selThreadIndex === selThreadIndex) {
                    $scope.selThreadIndex = null;
                    $scope.dontHighlightThreads();

                } else {
                    $scope.selThreadIndex = selThreadIndex;
                    $scope.higlightThread($scope.selThreadIndex);
                }
            }
        }
    }

    // Apply thread colour to svg stroke
    $scope.applyThread = function (threadIndex, threadRGB) {
        $('.colour' + threadIndex).css({ 'stroke': 'rgb(' + threadRGB + ')' });
    }

    // Select thread to apply on chosen thread
    $scope.selectThread = function (thread) {
        if (!$scope.loading) {
            // Assign variables
            var selectedThreadRGB = thread.rgb;
            $scope.editedThreads[$scope.selThreadIndex] = thread.title.split(' ')[0]; // get color number only (ex. 2148)
            $scope.thread_search_name = '';

            //Change the thread on svg
            $scope.applyThread($scope.selThreadIndex, selectedThreadRGB);
        }
    };


    $scope.unselectThreads = function () {
        $scope.dontHighlightThreads();
        $scope.selThreadIndex = null;
    }

    var showEditLogoContent = function () {
        // Reload SVG each time as they might change the logo!
        $scope.getEmblemSVG();
        // Dont check for now (smooths loading)
        // if (SVGloadedOnce) {
        //     $scope.showSVG = true;
        // }
        $scope.showIMG = false;
        $scope.showSVG = true;
        $('#emblem-svg').show();


        $('#emblem-img').hide();
        $('.modal-dialog').animate({
            width: "+=200"
        }, 300, function () {
            $('#view-emblem-img-container').addClass(' view-img-fixed');
            //$('.emblem-details').hide(300, function() {

            $('#view-emblem-img-container').addClass(' expand-img');
            $scope.showSVG = true;
            //});


        });
    };

    var hideEditLogoContent = function () {
        $('#view-emblem-img-container').removeClass('view-img-fixed');

        $scope.showSVG = false;
        $scope.showIMG = true;
        $('#emblem-svg').hide();
        $('#emblem-img').show();
        $('#emblem-threads').hide();
        $('.modal-dialog').animate({
            width: "-=200"
        }, 300, function () {
            //$('.emblem-details').show(300, function() {
            $('#emblem-threads').show();
            // });
        });

        $timeout(function () {
            $('#view-emblem-img-container').removeClass('expand-img');
        })
    };

    // Edit Logo button (switch)
    $scope.openLogoEdit = function () {
        $scope.unselectThreads(); // Close selected threads if any
        if ($scope.editLogo) {
            showEditLogoContent(); // Show edit logo 
        } else {
            hideEditLogoContent(); // Hide edit logo 
        }
    };


    // Accept selected threads and close modal
    $scope.acceptThreads = function () {
        $scope.loading = true;
        $http.post('api/v1.0/admin/thread_update', {
            organization_logo_id: $scope.emblem.organization_logo_id,
            organization_id: $scope.emblem.organization_id,
            tesco_style_ref: $scope.emblem.tesco_style_ref,
            threads: $scope.editedThreads
        }).success(function (data) {
            $scope.loading = false;
            if (angular.isObject(data)) {
                $timeout(function () {
                    originalscope.messages = data.messages;
                    originalscope.getOrgDetail();
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        originalscope.messages = [];
                    }, 3000);
                    $uibModalStack.dismissAll();
                });
            }
        }).error(function (data) {
            $scope.loading = false;
            $timeout(function () {
                originalscope.messages = data.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
            });
        })
    }


    //init with first emblem selected
    if ($scope.emblems.length > 0) {
        $scope.selectExistingEmblem($scope.emblems[0], 0);
    }


    $scope.requestExistingLogo = function () {
        var modalInstanceShow = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/add-emblem-confirm.html',
            controller: 'AdminAddEmblemConfirmController',
            size: 'md'
        });
        //returns existing logo data
        $scope.chosenLogo = {};
        modalInstanceShow.result.then(function (addEmblem) {
            if (addEmblem) {
                // Assign selected emblem
                $scope.requestExistingData = $scope.selectedExistingLogo;
                $scope.requestExistingData['emblemThreads'] = $scope.editedThreads;
                if ($scope.selectedEmblem.description) {
                    $scope.requestExistingData['description'] = $scope.selectedEmblem.description;
                } else {
                    $scope.requestExistingData['description'] = null;
                }
                $http.post($scope.serverstring + 'api/v1.0/admin/copyexistingemblem',
                    $scope.requestExistingData
                ).success(function (data, status, headers, config) {
                    // on success
                    $scope.loading = 0;
                    originalscope.getOrgDetail();
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        originalscope.messages = [];
                    }, 3000);

                    $uibModalStack.dismissAll();
                }).error(function (data, status, headers, config) {
                    // log error
                    $scope.loading = 0;
                    originalscope.messages = data.messages;

                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        originalscope.messages = [];
                    }, 3000);
                    $uibModalStack.dismissAll();
                });
            }
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

});

//---------------------------------------------------------------------//
//-----------------------AdminAddExistingEmblemController--------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('AdminAddExistingEmblemController', function ($scope, $uibModal, $uibModalInstance, $timeout, $log, options) {

    //------DATA------/
    $scope.emblems = options.emblems;
    $scope.selectedLogo = {};

    $scope.selectEmblem = function (i) {
        $scope.selectedEmblem = i;
        //parameter for next modal
        $scope.selectedLogo = i;
    };

    //init with first emblem selected
    $scope.selectEmblem($scope.emblems[0]);

    //save logo
    $scope.saveSelectedLogo = function () {
        //save logo data on modal close
        $scope.selectedLogo.type = 'emblem';
        $uibModalInstance.close($scope.selectedLogo);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

//------------------------------------------------------------------------//
//-----------------------AdminDeleteEmblemController----------------------//
//------------------------------------------------------------------------//
uesApp.controllerProvider.register('AdminDeleteEmblemController', function ($scope, $http, $uibModal, $uibModalInstance, $timeout, originalscope, $log, options) {

    $scope.emblem = options.emblem;
    $scope.deleteButton = 1;
    $scope.loading = 0;



    $scope.deleteEmblemConfirm = function () {
        $scope.loading = 1;
        $http.post($scope.serverstring + 'api/v1.0/admin/removeemblem', {
            embData: $scope.emblem
        }).success(function (data, status, headers, config) {
            $scope.loading = 0;
            $timeout(function () {
                originalscope.messages = data.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
            });
            $uibModalInstance.dismiss('cancel');
            originalscope.getOrgDetail();
        }).error(function (data, status, headers, config) {
            $scope.loading = 0;
            // log error
            $timeout(function () {
                originalscope.messages = data.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
            });
            originalscope.getOrgDetail();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        originalscope.getOrgDetail();
    };

});


//---------------------------------------------------------------------//
//-----------------------AdminAddEmblemConfirmController---------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('AdminAddEmblemConfirmController', function ($scope, $http, $uibModal, $uibModalInstance, $log) {

    $scope.addEmblem = false;

    $scope.ok = function () {
        $scope.addEmblem = true;
        $uibModalInstance.close($scope.addEmblem);
    };

    $scope.cancel = function () {
        $scope.addEmblem = false;
        $uibModalInstance.dismiss('cancel');
    };

});

//---------------------------------------------------------------------//
//-----------------------AdminUploadDesignConfirmController------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('AdminUploadDesignConfirmController', function ($scope, $http, $uibModal, $uibModalInstance, $uibModalStack, $timeout, $filter, $route, originalscope, options) {
    var getVar = $route.current.params;
    //------DATA------/
    $scope.requestData = {};
    $scope.messages = {};
    $scope.emblem = options.emblem;
    //width/height pattern
    $scope.onlyNumbers = /([0-9])+(.)/;
    //default logoType value - new emblem
    $scope.requestData.logoType = 'new';
    $scope.loading = 0;

    $scope.sampleSent = false;
    $scope.logoTypeButton = 'first';
    $scope.logoTypeChosen = null;
    //let user choose type of the logo
    $scope.chooseLogoType = function (choice) {
        $scope.requestData.logoType = choice;
    };

    //set flag of logo to reveal the form
    $scope.acceptLogoType = function () {
        $scope.logoTypeChosen = true;
    };


    //-----MODAL------/
    $scope.modalResult = null;
    $scope.showModal = function (type, size, options) {
        var template, controller;

        switch (type) {
            case 'addExistingEmblem':
                template = 'pages/modals/add-existing-emblem.html';
                controller = 'AdminAddExistingEmblemController';
                break;
            case 'addEmblemConfirm':
                template = 'pages/modals/add-emblem-confirm.html';
                controller = 'AdminAddEmblemConfirmController';
                break;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: template,
            controller: controller,
            backdrop: 'static',
            size: size,
            resolve: {
                options: function () {
                    return options;
                },
                originalscope: function () {
                    return $scope;
                }
            }
        });

        //returns existing logo data
        $scope.chosenLogo = {};
        modalInstance.result.then(function (selectedLogo) {
            $scope.loading = 1;
            $scope.selected = selectedLogo;
            $scope.chosenLogo.backgroundColour = $scope.selected.primary_background_colour;
            $scope.chosenLogo.type = $scope.selected.type;
            $scope.chosenLogo.url = $scope.serverstring + $scope.selected.logo_url;
            $scope.chosenLogo.name = $scope.selected.tesco_style_ref;

            var foundItem = $filter('filter')($scope.allLogos, $scope.chosenLogo, true)[0];

            if (foundItem) {
                $scope.displayError('This emblem is already attached');
            } else {
                // not found.
                $scope.loading = 0;
                $scope.allLogos.push($scope.chosenLogo);
            }
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


    // new logo init variables
    $scope.allLogos = [];
    $scope.chosenLogo = {};
    $scope.thumbnail = {
        url: 0
    };
    $scope.fileReaderSupported = window.FileReader != null;
    $scope.maxsize = 2000000; //2mb
    $scope.minsize = 1024; //1kb

    //error message function
    $scope.displayError = function (message) {

        $scope.loading = 0;
        $timeout(function () {
            // Make sure they see the success message before it closes.
            originalscope.messages.push({
                type: 'danger',
                message: message
            });
            $timeout(function () {
                // Make sure they see the success message before it closes.
                originalscope.messages = [];
            }, 3000);
        });
        $scope.messages = {};
        originalscope.messages = [];
    };

    function formatSizeUnits(bytes) {
        if (bytes >= 1000000000) { bytes = (bytes / 1000000000).toFixed(2) + ' GB'; } else if (bytes >= 1000000) { bytes = (bytes / 1000000).toFixed(2) + ' MB'; } else if (bytes >= 1000) { bytes = (bytes / 1000).toFixed(2) + ' KB'; } else if (bytes > 1) { bytes = bytes + ' bytes'; } else if (bytes == 1) { bytes = bytes + ' byte'; } else { bytes = '0 byte'; }
        return bytes;
    }

    //$scope.filePattern = new RegExp("(bat|exe|cmd|sh|php|pl|cgi|386|dll|com|torrent|js|app|jar|pif|vb|vbscript|wsf|asp|cer|csr|jsp|drv|sys|ade|adp|bas|chm|cpl|crt|csh|fxp|hlp|hta|inf|ins|isp|jse|htaccess|htpasswd|ksh|lnk|mdb|mde|mdt|mdw|msc|msi|msp|mst|ops|pcd|prg|reg|scr|sct|shb|shs|url|vbe|vbs|wsc|wsf|wsh)$");
    //$scope.dstFilePattern = new RegExp("(dst|pdf|emb)$");
    //$scope.pdfFilePattern = new RegExp("(pdf)$");
    //$scope.embFilePattern = new RegExp("(emb)$");

    $scope.uploadDesignPattern = new RegExp("(dst|pdf|emb)$");
    $scope.uploadedExtensions = {
        dst: false,
        emb: false,
        pdf: false
    };

    $scope.photoChanged = function (event) {
        $timeout(function () {
            var files = event.target.files;
            $scope.chosenLogo = {};
            $scope.loading = 1;
            if (files != null) {
                var file = files[0];
                console.log(file);
                var extension = file.name.split('.').pop().toLowerCase();
                //check file size(2mb)
                if (file.size > $scope.maxsize) {
                    $scope.displayError('Uploaded file size[' + formatSizeUnits(file.size) + '] exceeds maximum file size[' + formatSizeUnits($scope.maxsize) + ']');
                    file = null;
                } else if (file.size <= $scope.minsize) {
                    $scope.displayError('Uploaded file size[' + formatSizeUnits(file.size) + '] is too small - file may be corrupted');
                    file = null;
                } else {
                    //check for file extension
                    if (!$scope.uploadDesignPattern.test(extension)) {
                        $scope.displayError('Uploaded file has wrong extension [.' + extension + ']');
                    } else if ($scope.uploadedExtensions[extension] == true) {
                        $scope.displayError('File with such extension already exists [.' + extension + ']');
                    } else {
                        $scope.uploadedExtensions[extension] = true;
                        if ($scope.fileReaderSupported) {
                            $timeout(function () {
                                var fileReader = new FileReader();
                                fileReader.readAsDataURL(file);
                                fileReader.onload = function (e) {
                                    $scope.thumbnail.url = e.target.result;
                                    $scope.filetype = file.type;
                                    file.url = e.target.result;
                                    $scope.chosenLogo.isImage = file.type.indexOf('image') > -1;
                                    $scope.chosenLogo.url = e.target.result;
                                    $scope.chosenLogo.name = file.name;
                                    $scope.chosenLogo.type = file.type;
                                    var foundItem = $filter('filter')($scope.allLogos, $scope.chosenLogo, true)[0];

                                    //file already added?
                                    if (foundItem) {
                                        $scope.displayError('This file is already attached');
                                    } else {
                                        // file not found -> send file
                                        $timeout(function () {
                                            $scope.allLogos.push($scope.chosenLogo);
                                            $('#uploadfile').val(null);
                                            $scope.loading = 0;
                                        });
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    };

    //remove emblem from allLogos and it's extension
    $scope.removeEmblem = function (i) {
        $scope.extension = $scope.allLogos[i].name.split('.').pop().toLowerCase();
        $scope.loading = 1;
        $timeout(function () {
            $scope.uploadedExtensions[$scope.extension] = false;
            $scope.allLogos.splice(i, 1);
        });
        $scope.loading = 0;
        return $scope.allLogos;
    };

    //send request to API -> show confirm modal first
    $scope.sendRequest = function (i) {
        var modalInstanceShow = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/add-emblem-confirm.html',
            controller: 'AdminAddEmblemConfirmController',
            size: 'md'
        });
        //returns existing logo data
        $scope.chosenLogo = {};
        modalInstanceShow.result.then(function (addEmblem) {
            if (addEmblem) {
                $scope.loading = 1;
                $scope.requestData.sampleSent = $scope.sampleSent;
                $scope.requestData.attachments = $scope.allLogos;
                $scope.requestData.emblem = $scope.emblem;
                $scope.requestData.orgid = originalscope.orgdetail.organization_id;

                $http.post($scope.serverstring + 'api/v1.0/admin/addlogofiles', {
                    requestData: $scope.requestData
                }).success(function (data, status, headers, config) {
                    // on success
                    $scope.loading = 0;
                    originalscope.getOrgDetail();
                    $timeout(function () {
                        originalscope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            originalscope.messages = [];
                        }, 3000);
                    });
                    $uibModalStack.dismissAll();
                }).error(function (data, status, headers, config) {
                    // log error
                    $scope.loading = 0;
                    $timeout(function () {
                        originalscope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            originalscope.messages = [];
                        }, 3000);
                    });
                });
            }
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.cancel = function () {
        $scope.addEmblem = false;
        originalscope.getOrgDetail();
        $uibModalInstance.dismiss('cancel');
    };

});



//---------------------------------------------------------------------//
//------------------------ViewEmblemController-------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('ViewEmblemController', function ($scope, $uibModal, $sce, $http, $uibModalInstance, $uibModalStack, $timeout, $log, options, originalscope) {

    //------DATA------/
    $scope.emblem = angular.copy(options.emblem);
    $scope.editedThreads = $scope.emblem.emblemThreads;
    $scope.svg_url = $scope.emblem['logo_svg_url'];
    $scope.org_default_logo_id = originalscope.orgdetail.organization_default_logo_id;
    $scope.isDefaultLogoAlready = $scope.emblem.organization_logo_id === $scope.org_default_logo_id;
    $scope.messages = originalscope.messages; //holds reference to original messages
    $scope.messagesReference = originalscope.messages; //holds reference to original messages
    $scope.reloadOrgDetails = originalscope.getOrgDetail;

    // User did not click select background options
    $scope.showEditBackground = false;
    $scope.editBGBtn = false;

    $scope.editBackground = function () {
        $scope.showEditBackground = true;
        $timeout(function () {
            $scope.editBGBtn = true;
        }, 510);
    }

    $scope.updateEmblemBackground = function (emblem) {
        if (emblem) {
            var modalInstanceShow = $uibModal.open({
                animation: true,
                templateUrl: 'pages/modals/confirm.html',
                controller: 'AdminEditEmblemBackgroundController',
                size: 'md'
            });
            modalInstanceShow.result.then(function (updateEmblem) {
                if (updateEmblem) {
                    $http.post($scope.serverstring + 'api/v1.0/admin/emblembgupdate', {
                        emblem: $scope.emblem
                    }).success(function (data, status, headers, config) {
                        // on success
                        $scope.loading = 0;
                        originalscope.getOrgDetail();
                        $uibModalStack.dismissAll();
                        // show alert message

                        originalscope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            originalscope.messages = [];
                        }, 3000);

                    }).error(function (data, status, headers, config) {
                        // log error
                        $scope.loading = 0;
                        // show alert message
                        originalscope.messages = data.messages;
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            originalscope.messages = [];
                        }, 3000);

                    });
                }
            },
                function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
        } else {
            return;
        }
    }


    $scope.updateEmblemBG = function () {
        if ($scope.emblem.primary_background_colour != undefined || $scope.emblem.primary_background_colour != null) {

        } else {
            return false;
            console.log("Emblem was empty!");
        }
    }


    if ($scope.emblem.thread_array) {
        $scope.emblem.emblemThreads = $scope.emblem.thread_array.split(",");
        $scope.editedThreads = angular.copy($scope.emblem.emblemThreads);
    }

    //----------footer buttons----------//
    $scope.setAsDefaultLogoButton = ($scope.emblem.logo_approved == '1');
    $scope.approveButton = ($scope.emblem.needs_ebos_update == '0' && $scope.emblem.needs_to_update_ebos == '0' && $scope.emblem.logo_digitized == '1' && $scope.emblem.logo_approved == '0');
    $scope.rejectButton = ($scope.emblem.needs_ebos_update == '0' && $scope.emblem.needs_to_update_ebos == '0' && $scope.emblem.logo_digitized == '1' && $scope.emblem.logo_approved == '0');
    $scope.sampleButton = ($scope.emblem.needs_ebos_update == '0' &&
        $scope.emblem.needs_to_update_ebos == '0' &&
        $scope.emblem.logo_digitized == '1' &&
        $scope.emblem.threads_updated == '1');
    $scope.editLogoButton = $scope.emblem.logo_approved == '0' && $scope.emblem.needs_to_update_ebos == '0' && $scope.emblem.needs_ebos_update == '0';



    $scope.setLogoAsDefault = function () {
        $scope.loading = 1;
        $http.post($scope.serverstring + 'api/v1.0/admin/updatedefaultlogo', {
            organization_id: originalscope.orgdetail.organization_id,
            organization_logo_id: $scope.emblem.organization_logo_id
        }).success(function (data, status, headers, config) {
            $scope.loading = 0;
            originalscope.getOrgDetail();
            if (angular.isObject(data)) {
                $timeout(function () {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        originalscope.messages = [];
                    }, 3000);
                    $uibModalStack.dismissAll();
                });
            }
        }).error(function (data, status, headers, config) {
            // log error
            $scope.loading = 0;
            originalscope.getOrgDetail();
            $timeout(function () {
                originalscope.messages = data.messages;
                $timeout(function () {
                    // Make sure they see the error message before it closes.
                    originalscope.messages = [];
                }, 3000);
            });
        });
    };



    //$scope.emblem.organization_logo_id
    $scope.sendForm = function (endpoint, data) {
        $scope.loading = 1;
        $http.post($scope.serverstring + 'api/v1.0/admin/' + endpoint, {
            emblem: $scope.emblem,
            description: data
        }).success(function (data, status, headers, config) {
            // message about email.  Modal?
            $scope.loading = 0;
            if (angular.isObject(data)) {
                $timeout(function () {
                    originalscope.messages = data.messages;
                    originalscope.getOrgDetail();
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        originalscope.messages = [];
                    }, 3000);
                    $uibModalStack.dismissAll();
                });
            }
        }).error(function (data, status, headers, config) {
            // log error
            $scope.loading = 0;
            $timeout(function () {
                originalscope.messages = data.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
            });
        });
    };


    //------MODAL-----/
    $scope.showModal = function (type, size, options) {
        var template, controller;
        switch (type) {
            case 'requestSample':
                template = 'pages/modals/admin-request-sample.html';
                controller = 'RequestSampleController';
        }

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: template,
            controller: controller,
            size: size,
            backdrop: 'static',
            resolve: {
                options: function () {
                    return options;
                },
                colorParameters: function () {
                    return $scope.activeColor;
                },
                originalscope: function () {
                    return $scope;
                }
            }
        });

        modalInstance.result.then(function (data) {
            originalscope.messages = data;
            $scope.cancel(); //cancel modal
            originalscope.getOrgDetail();
            originalscope.getOrgNotes();
            $timeout(function () {
                // Make sure they see the success message before it closes.
                originalscope.messages = [];
            }, 3000);
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.scrollToElement = function (element) {
        $(document).ready(function () {
            var $container = $('.modal-body');
            var scrollTo = $(element);


            $container.scrollTop(scrollTo.offset().top - $container.offset().top + $container.scrollTop(), 300);

        });
    }


    /*--------------------------*/
    /*------ EDIT THREADS ------*/
    /*--------------------------*/

    $scope.selThreadIndex = null;
    $scope.selectedThread = { value: 0 };
    $scope.emblemSVGloaded = false; // is SVG loaded
    $scope.svgTotalNodesAmount = 0; // not in use for now
    $scope.svgNodesLimitPerG = 2500; // dont flash svg if too many nodes
    $scope.svgAnimate = true;
    $scope.showSVG = false;
    $scope.showIMG = true;
    var SVGloadedOnce = false; // is SVG loaded once?
    // Disable flashing for specific browsers (slow)
    var disableFirefox = true;
    var disableIE = true;
    var disableEdge = true;

    $scope.getEmblemSVG = function () {
        $scope.loadingSVG = true;
        $scope.emblemSVGresized = false;
        $scope.emblemSVGloaded = false;
        $http.get($scope.serverstring + $scope.emblem.logo_svg_url + '?' + $scope.emblem.logo_last_updated).success(function (data) {
            $scope.emblemSVG = $sce.trustAsHtml(data);
            SVGloadedOnce = true;
            $scope.emblemSVGloaded = true;
            $scope.loadingSVG = false;
            $timeout(function () {
                $scope.resizeEmblem();
            })
        })
    };

    // Disable flashing on IE/Edge/Firefox totally -> slow
    function isIEorEDGEorFIREFOX() {
        return ((navigator.appName == 'Microsoft Internet Explorer') && disableIE) ||
            ((navigator.appName == "Netscape" && navigator.appVersion.indexOf('Edge') > -1) && disableEdge) ||
            ((navigator.userAgent.toLowerCase().indexOf('firefox') > -1) && disableFirefox);
    }

    $scope.resizeEmblem = function () {
        var $svg = $(document).find("svg");
        $scope.svgAnimate = false;

        // // check browser
        // if (!isIEorEDGEorFIREFOX()) {
        //     var gNodes = document.getElementsByTagName('g');
        //     $scope.svgNodesAmount = 0;
        //     for (var i = 0; i < gNodes.length; i++) {
        //         $scope.svgNodesAmount += parseFloat(gNodes[i].childNodes.length);
        //         if (parseFloat(gNodes[i].childNodes.length) > $scope.svgNodesLimitPerG) {
        //             $scope.svgAnimate = false;
        //         }
        //     }
        // } else {
        //     $scope.svgAnimate = false;
        // }

        var $svg_height_prop = $svg.prop("height");
        var $svg_width_prop = $svg.prop("width");

        var svg_height = $svg_height_prop['baseVal']['value'];
        var svg_width = $svg_width_prop['baseVal']['value'];

        var SetSvgHeight = $svg.attr('height', '100%');
        var SetSvgWidth = $svg.attr('width', '100%');
        //var SetSvgViewBox = $svg.attr("viewBox", "0 0 " + svg_width + " " + svg_height);
        $svg.each(function () { $(this)[0].setAttribute('viewBox', "0 0 " + svg_width + " " + svg_height) });

        $scope.loadingSVG = false;
        $scope.emblemSVGresized = true;
    }

    $scope.higlightThread = function (index) {
        for (var i = 0; i < $scope.editedThreads.length; i++) {
            //Highlight chosen thread
            if (i === index) {
                $('.colour' + i).css({ 'stroke-opacity': '1' });
                if ($scope.svgAnimate) {
                    $('.colour' + i).addClass('flashing');
                }
            } else {
                $('.colour' + i).css({ 'stroke-opacity': '.07' });
                if ($scope.svgAnimate) {
                    $('.colour' + i).removeClass('flashing');
                }
            }
        }
    }

    $scope.dontHighlightThreads = function () {
        for (var i = 0; i < $scope.editedThreads.length; i++) {
            $('.colour' + i).css({ 'stroke-opacity': '1' });
            $('.colour' + i).removeClass('flashing');

        }
    }


    // Choose thread to change
    $scope.chooseThread = function (selThreadIndex, event) {
        if (!$scope.loading && !$scope.loadingSVG) {
            if ($scope.editLogo) {
                if ($scope.selThreadIndex === selThreadIndex) {
                    $scope.selThreadIndex = null;
                    $scope.dontHighlightThreads();

                } else {
                    $scope.selThreadIndex = selThreadIndex;
                    $scope.higlightThread($scope.selThreadIndex);
                }
            }
        }
    }

    // Apply thread colour to svg stroke
    $scope.applyThread = function (threadIndex, threadRGB) {
        $('.colour' + threadIndex).css({ 'stroke': 'rgb(' + threadRGB + ')' });
    }

    // Select thread to apply on chosen thread
    $scope.selectThread = function (thread) {
        if (!$scope.loading) {
            // Assign variables
            var selectedThreadRGB = thread.rgb;
            $scope.editedThreads[$scope.selThreadIndex] = thread.title.split(' ')[0]; // get color number only (ex. 2148)
            $scope.thread_search_name = '';

            //Change the thread on svg
            $scope.applyThread($scope.selThreadIndex, selectedThreadRGB);
        }
    };


    $scope.unselectThreads = function () {
        $scope.dontHighlightThreads();
        $scope.selThreadIndex = null;
    }

    var showEditLogoContent = function () {
        // Load SVG logo if it's not loaded yet
        if (!$scope.emblemSVG) {
            $scope.getEmblemSVG();
        }

        // Dont check for now (smooths loading)
        // if (SVGloadedOnce) {
        //     $scope.showSVG = true;
        // }
        $scope.showIMG = false;
        $scope.showSVG = true;

        $('#emblem-img').hide();
        $('.modal-dialog').animate({
            width: "+=200"
        }, 300, function () {
            $('#view-emblem-img-container').addClass(' view-img-fixed');
            $('.emblem-details').hide(300, function () {
                $('#emblem-svg').show();
                $('#view-emblem-img-container').addClass(' expand-img');
                $scope.showSVG = true;
            });


        });
    };

    var hideEditLogoContent = function () {
        $('#view-emblem-img-container').removeClass('view-img-fixed');

        $scope.showSVG = false;
        $scope.showIMG = true;
        $('#emblem-svg').hide();
        $('#emblem-img').show();
        $('#emblem-threads').hide();
        $('.modal-dialog').animate({
            width: "-=200"
        }, 300, function () {
            $('.emblem-details').show(300, function () {
                $('#emblem-threads').show();
            });
        });

        $timeout(function () {
            $('#view-emblem-img-container').removeClass('expand-img');
        })
    };


    // Edit Logo button (switch)
    $scope.openLogoEdit = function () {
        $scope.unselectThreads(); // Close selected threads if any
        if ($scope.editLogo) {
            showEditLogoContent(); // Show edit logo 
        } else {
            hideEditLogoContent(); // Hide edit logo 
        }
    };


    // Accept selected threads and close modal
    $scope.acceptThreads = function () {
        $scope.loading = true;
        $http.post('api/v1.0/admin/thread_update', {
            organization_logo_id: $scope.emblem.organization_logo_id,
            organization_id: $scope.emblem.organization_id,
            tesco_style_ref: $scope.emblem.tesco_style_ref,
            alert_ticket_id: $scope.emblem.alert_ticket_id,
            threads: $scope.editedThreads
        }).success(function (data) {
            $scope.loading = false;
            if (angular.isObject(data)) {
                $timeout(function () {
                    originalscope.messages = data.messages;
                    originalscope.getOrgDetail();
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        originalscope.messages = [];
                    }, 3000);
                    $uibModalStack.dismissAll();
                });
            }
        }).error(function (data) {
            $scope.loading = false;
            $timeout(function () {
                originalscope.messages = data.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
            });
        })
    }


});

//---------------------------------------------------------------------//
//------------------------FormReceiptController------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('FormReceiptController', function ($scope, $uibModal, $uibModalInstance, $http, originalscope, $timeout, $filter) {

    //------DATA------/

    $scope.formForm = {};

    $scope.formForm.orgid = originalscope.orgdetail.organization_id;
    $scope.formForm.userid = originalscope.session.user.user_id;

    $scope.beforeRender = function ($view, $dates) {
        var today = new Date();
        //console.log(today);
        angular.forEach($dates, function (options, key) {
            // console.log(options);
            if (options.utcDateValue >= today.getTime()) {
                $dates[key].selectable = false;
            }
        });
    };


    $scope.$watch('formForm.date', function (newValue) {
        $scope.formForm.viewdate = $filter('date')(newValue, 'longDate');
    });
    $scope.formForm.date = new Date();

    $scope.onTimeSet = function (newDate, oldDate) {
        $scope.isCollapsed = !$scope.isCollapsed;
    };

    $scope.dateTimeOptions = {
        minView: 'day'
    };


    //------MODAL-----/
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.addFormToOrg = function () {

        $http.post($scope.serverstring + 'api/v1.0/admin/addformtoorg', {
            formData: $scope.formForm
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    //$scope.response = data;
                    originalscope.messages = data.messages;
                    originalscope.getOrgDetail();
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        originalscope.messages = [];
                    }, 3000);
                }
                $uibModalInstance.close();
            }).
            error(function (data, status, headers, config) {
                // log error
                originalscope.messages = data.messages;
                originalscope.getOrgDetail();
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
                $uibModalInstance.close();
            });

    }

});


//---------------------------------------------------------------------//
//-----------------------ResetPasswordController-----------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('ResetPasswordController', function ($scope, $uibModal, $uibModalInstance, options) {

    //------DATA------/
    $scope.user = options;

    //------MODAL-----/
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.ok = function () {
        $uibModalInstance.close(true);
    };


});

//---------------------------------------------------------------------//
//-------------------------UserEditController--------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('UserEditController', function ($scope, $http, $location, $route, $uibModal, $uibModalInstance, options, $timeout, $rootScope) {
    //set modal to choose proper content
    $scope.isUserEditModal = true;

    //variables
    $scope.user = options.user;

    $scope.editForm = {};

    $scope.editForm.userID = $scope.user.user_id;
    $scope.editForm.firstname = $scope.user.user_first_name;
    $scope.editForm.lastname = $scope.user.user_last_name;
    $scope.editForm.email = $scope.user.user_email;

    $scope.updateUser = function () {
        $http.post($scope.serverstring + 'api/v1.0/admin/updateuser', {
            userData: $scope.editForm
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    //things to update
                    $uibModalInstance.close(data.messages);
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $uibModalInstance.close(data.messages);
            });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});


//---------------------------------------------------------------------//
//-------------------------UserAddController---------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('UserAddController', function ($scope, $http, $location, $route, $uibModal, $uibModalInstance, options, $timeout, originalscope) {
    //set modal to choose proper content
    $scope.isUserAddModal = true;

    //variables
    $scope.newUserForm = {};
    $scope.house = options.org;
    $scope.houseID = $scope.house.organization_id;

    console.log("this user is part of the:" + $scope.houseID + " house");

    //--------New User---------//
    $scope.addUserAdmin = function () {
        $scope.loading = 1;
        $scope.newUserForm.organizationID = $scope.houseID;
        $http.post($scope.serverstring + 'api/v1.0/admin/adduser', {
            formData: $scope.newUserForm
        }).success(function (data, status, headers, config) {
            $scope.loading = 0;
            $scope.cancel();
            originalscope.getOrgDetail();
            $timeout(function () {
                originalscope.messages = data.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
            });
        }).error(function (data, status, headers, config) {
            // log error
            $scope.loading = 0;
            $timeout(function () {
                originalscope.messages = data.messages;
                $timeout(function () {
                    originalscope.messages = [];
                }, 3000);
            });
        });
    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

});

//---------------------------------------------------------------------//
//---------------------MakeUserPrimaryController-----------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('MakeUserPrimaryController', function ($scope, $http, $location, $route, $uibModal, $uibModalInstance, options, $timeout, originalscope) {
    //set modal to choose proper content
    $scope.isUserMakePrimaryModal = true;

    //variables
    $scope.user = options.user;
    $scope.house = options.org;

    //variables for API
    $scope.formData = {};
    $scope.formData.user = $scope.user;
    $scope.houseID = $scope.house.organization_id;

    console.log($scope.formData);

    //--------change primary User---------//
    $scope.makeUserPrimary = function (user) {
        $scope.loading = 1;
        $http.post($scope.serverstring + 'api/v1.0/admin/changeprimaryuser', {
            formData: {
                user: $scope.user,
                organizationID: $scope.houseID
            }
        }).
            success(function (data, status, headers, config) {
                $scope.loading = 0;
                originalscope.getOrgDetail();
                $scope.cancel();
                $timeout(function () {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        originalscope.messages = [];
                    }, 3000);
                });
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.loading = 0;
                originalscope.getOrgDetail();
                $timeout(function () {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        originalscope.messages = [];
                    }, 3000);
                });
            });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

});

//---------------------------------------------------------------------//
//-------------------------DeleteUserController------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('DeleteUserController', function ($scope, $http, $location, $route, $uibModal, $uibModalInstance, options, $timeout, originalscope) {
    //set modal to choose proper content
    $scope.isUserDeleteModal = true;

    //variables
    $scope.user = options.user;
    $scope.house = options.org;

    //variables for API
    $scope.formData = {};
    $scope.formData.user = $scope.user;
    $scope.houseID = $scope.house.organization_id;

    //--------delete User---------//
    $scope.deleteUser = function (user) {

        $scope.loading = 1;
        $http.post($scope.serverstring + 'api/v1.0/admin/deleteuser', {
            formData: {
                user: $scope.user,
                organizationID: $scope.houseID
            }
        }).success(function (data, status, headers, config) {
            $scope.loading = 0;
            originalscope.getOrgDetail();
            $scope.cancel();
            $timeout(function () {
                originalscope.messages = data.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
            });
        }).error(function (data, status, headers, config) {
            // log error
            $scope.loading = 0;
            originalscope.getOrgDetail();
            $timeout(function () {
                originalscope.messages = data.messages;
                $timeout(function () {
                    originalscope.messages = [];
                }, 3000);
            });
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };


});


//---------------------------------------------------------------------//
//-------------------------HouseEditController-------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('HouseEditController', function ($scope, $http, $location, $route, $uibModal, $uibModalInstance, options, $timeout, $rootScope, originalscope) {
    //Set form and variables
    $scope.isHouseEditModal = true;
    $scope.houseEdit = true;
    $scope.house = options.house;
    $scope.orgName = originalscope.orgdetail.organization_name;

    //Update house variables
    $scope.editForm = {};
    $scope.editForm.houseName = $scope.house.organization_name;
    $scope.editForm.houseID = $scope.house.organization_id;

    //Update hose
    $scope.updateHouse = function () {
        $http.post($scope.serverstring + 'api/v1.0/admin/updatehouse', {
            houseData: $scope.editForm
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    if (data.messages[0].type != 'danger') {
                        $uibModalInstance.close(data.messages);
                        $timeout(function () {
                            originalscope.messages = data.messages;
                            $timeout(function () {
                                originalscope.messages = [];
                            }, 3000);
                        });
                    }
                    $timeout(function () {
                        originalscope.messages = data.messages;
                        $timeout(function () {
                            originalscope.messages = [];
                        }, 3000);
                    });
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $uibModalInstance.close(data.messages);
            });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});
//---------------------------------------------------------------------//
//--------------------------AddNoteController--------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('AddNoteController', function ($scope, $http, $location, $route, $uibModal, $uibModalInstance, options, $filter, $rootScope, originalscope) {

    $scope.isCollapsed = true;

    $scope.org = options.org;


    $scope.newNote = {};
    $scope.newNote.noteauthor = $scope.session.user.user_first_name;
    $scope.newNote.userid = $scope.session.user.user_id;
    $scope.newNote.orgid = $scope.org.organization_id;

//firefly
    $scope.newNote.adminusers =  originalscope.orgadminsdetail;



    $scope.dateTimeOptions = {
        minView: 'day'
    };

    $scope.beforeRender = function ($view, $dates) {
        var today = new Date();
        //console.log(today);
        angular.forEach($dates, function (options, key) {
            // console.log(options);
            if (options.utcDateValue < today.getTime()) {
                //$dates[key].selectable = false;
            }
        });
    };

    $scope.$watch('newNote.myDate', function (newValue) {
        $scope.newNote.myDateView = $filter('date')(newValue, 'MMM d, y');
    });

    // $scope.newNote.myDate = new Date();

    $scope.onTimeSet = function (newDate, oldDate) {
        $scope.isCollapsed = !$scope.isCollapsed;
    };

    $scope.isOpen = false;

    $scope.openCalendar = function (e) {
        e.preventDefault();
        e.stopPropagation();
        $scope.isOpen = true;
    };

    // $scope.newNote.myDate = new Date();

    $scope.addNote = function () {
        $http.post($scope.serverstring + 'api/v1.0/admin/addnote', { //change to proper API
            newNote: $scope.newNote
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    //things to update#
                    $uibModalInstance.close(data.messages);
                }

            }).
            error(function (data, status, headers, config) {
                // log error
                $uibModalInstance.close(data.messages);
            });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

//---------------------------------------------------------------------//
//-----------------------SuspendConfirmController----------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('SuspendConfirmController', function ($scope, $http, $timeout, $uibModal, $uibModalInstance, options, originalscope, $sce) {

    //-------------DATA-------------//
    $scope.confirmButtonShow = true;
    $scope.user = options;
    $scope.requestData = {};
    $scope.requestData.orgId = originalscope.orgdetail.organization_id;
    $scope.requestData.orgName = originalscope.orgdetail.organization_name;
    $scope.requestData.suspend = false;

    //------------CONTENT-----------//
    $scope.title = 'Suspend organization';
    $scope.message = '<h5><b>Are you sure you want to <em style="color:orange">suspend</em> ' + originalscope.orgdetail.organization_name + ' ?</b></h5><br>';
    $scope.trustAsHtml = $sce.trustAsHtml($scope.message);

    //------------FOOTER------------//
    $scope.buttonClass = 'btn btn-warning btn-sm'
    $scope.glyphClass = 'glyphicon glyphicon-pushpin';
    $scope.buttonMessage = 'Suspend';


    //-------------SEND-------------//
    $scope.ok = function () {
        $scope.requestData.suspend = true;
        $http.post($scope.serverstring + 'api/v1.0/admin/suspendorg', {
            requestData: $scope.requestData
        }).success(function (data, status, headers, config) {
            if (angular.isObject(data)) {
                // updated.
                $timeout(function () {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        originalscope.messages = [];
                    }, 3000);
                });
            }
        }).
            error(function (data, status, headers, config) {
                // log error
                $timeout(function () {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        originalscope.messages = [];
                    }, 3000);
                });
            });
        $uibModalInstance.close($scope.requestData);
    };

    //-------------CLOSE-------------//
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

//---------------------------------------------------------------------//
//-----------------------WithdrawConfirmController---------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('WithdrawConfirmController', function ($scope, $http, $timeout, $uibModal, $uibModalInstance, options, originalscope, $sce) {

    //------------DATA--------------//
    $scope.confirmButtonShow = true;
    $scope.user = options;
    $scope.requestData = {};
    $scope.requestData.orgId = originalscope.orgdetail.organization_id;
    $scope.requestData.orgName = originalscope.orgdetail.organization_name;
    $scope.requestData.withdraw = false;
    $scope.showOtherReasonArea = false;

    //------message on modal--------//
    $scope.title = 'Withdraw organization';
    $scope.message = '<h5><b>Are you sure you want to <em style="color:red">withdraw</em>  ' + originalscope.orgdetail.organization_name + '?</b></h5><br>';
    $scope.trustAsHtml = $sce.trustAsHtml($scope.message);

    //------footer style------------//
    $scope.buttonClass = 'btn btn-danger btn-sm';
    $scope.glyphClass = 'glyphicon glyphicon-fire';
    $scope.buttonMessage = 'Withdraw';

    $scope.$watch('requestData.reasonID', function (newVal, oldVal) {
        for (let reason in $scope.formElements.withdraw_reasons) {
            if ($scope.formElements.withdraw_reasons[reason].id === $scope.requestData.reasonID) {
                $scope.requestData.description = $scope.formElements.withdraw_reasons[reason].title;
            }
        }

        if ($scope.requestData.description == 'Other' && $scope.requestData.reasonID == 99) {
            $scope.showOtherReasonArea = true;
            $timeout(function(){
                $scope.requestData.description = '';
                $('#withdrawReasonText').focus();
            })
        } else {
            $scope.showOtherReasonArea = false;
        }
    });


    //-------------SEND-------------//
    $scope.ok = function () {
        $scope.requestData.withdraw = true;
        $http.post($scope.serverstring + 'api/v1.0/admin/withdraworg', {
            requestData: $scope.requestData
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    // updated.

                    $timeout(function () {
                        originalscope.messages = data.messages;
                        $timeout(function () {
                            originalscope.messages = [];
                        }, 3000);
                    });
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $timeout(function () {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        originalscope.messages = [];
                    }, 3000);
                });
            });
        $uibModalInstance.close($scope.requestData);
    };

    //-------------CLOSE-------------//
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

//---------------------------------------------------------------------//
//-----------------------AddHouseController----------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('AddHouseController', function ($scope, $http, $timeout, $uibModal, $uibModalInstance, options, originalscope, $sce) {

    //------------DATA--------------//
    $scope.isAddHouseModal = true;
    $scope.user = options;
    $scope.requestData = {};
    $scope.requestData.orgId = originalscope.orgdetail.organization_id;
    $scope.orgName = originalscope.orgdetail.organization_name;

    //-----display proper content on modal------//
    $scope.newHouseModal = true;
    $scope.addNewHouseShow = true;

    //------message on modal--------//
    $scope.title = 'Add new house';
    $scope.trustAsHtml = $sce.trustAsHtml($scope.message);

    //------footer style------------//
    $scope.buttonClass = 'btn btn-success btn-sm';
    $scope.glyphClass = 'glyphicon glyphicon-plus';
    $scope.buttonMessage = 'Add new house';


    //-------------SEND-------------//
    $scope.ok = function () {
        $http.post($scope.serverstring + 'api/v1.0/admin/addnewhouse', { //TODO change addNewHouse API(?)
            requestData: $scope.requestData
        }).
            success(function (data, status, headers, config) {
                originalscope.getOrgDetail();
                if (angular.isObject(data)) {
                    if (data.messages[0].type != 'danger') {
                        $uibModalInstance.close($scope.requestData);
                        $timeout(function () {
                            originalscope.messages = data.messages;
                            $timeout(function () {
                                originalscope.messages = [];
                            }, 3000);
                        });
                    } else {
                        $timeout(function () {
                            originalscope.messages = data.messages;
                            $timeout(function () {
                                originalscope.messages = [];
                            }, 3000);
                        });
                    }

                }
            }).
            error(function (data, status, headers, config) {
                // log error
                originalscope.getOrgDetail();
                $timeout(function () {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        originalscope.messages = [];
                    }, 3000);
                });
            });

    };

    //-------------CLOSE-------------//
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

//---------------------------------------------------------------------//
//-----------------------SendSampleController-------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('SendSampleController', function ($scope, $http, $timeout, $uibModal, $uibModalInstance, options, originalscope, $sce) {

    //------------DATA--------------//
    $scope.emblem = options.emblem;

    //------message on modal--------//
    $scope.title = 'Send Sample ' + $scope.emblem.tesco_style_ref;
    $scope.hideForm = true;
    $scope.message = '<h5><b>Are you sure you want to mark logo ' + $scope.emblem.tesco_style_ref + ' as sample sent?</b></h5>';
    $scope.trustAsHtml = $sce.trustAsHtml($scope.message);
    //------footer style------------//
    console.log($scope.emblem);

    //------------- REMOVE -------------//
    $scope.sendSample = function () {
        $http.post($scope.serverstring + 'api/v1.0/admin/sendsample', { //TODO change addNewHouse API(?)
            emblemid: $scope.emblem.organization_logo_id,
            orgid: $scope.emblem.organization_id
        }).success(function (data, status, headers, config) {
            originalscope.getOrgDetail();
            $scope.cancel();
            if (angular.isObject(data)) {
                $timeout(function () {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        originalscope.messages = [];
                    }, 3000);
                });
            }
        }).
            error(function (data, status, headers, config) {
                // log error
                originalscope.getOrgDetail();
                $scope.cancel();
                $timeout(function () {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        originalscope.messages = [];
                    }, 3000);
                });
            });
    };

    //-------------CLOSE-------------//
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});


//---------------------------------------------------------------------//
//-----------------------DeleteHouseController-------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('DeleteHouseController', function ($scope, $http, $timeout, $uibModal, $uibModalInstance, options, originalscope, $sce) {

    //------------DATA--------------//
    $scope.house = options.house;

    //------message on modal--------//
    $scope.title = 'Delete house (' + $scope.house.school_URN + ')';
    $scope.hideForm = true;
    $scope.message = '<h5><b>Are you sure you want to <em style="color:red">delete  </em> ' + $scope.house.organization_name + ' ?</b></h5>' +
        '<div class="alert alert-danger">All house products will be removed unless this is the only house where products will be assigned back to the main school.</div>';
    $scope.trustAsHtml = $sce.trustAsHtml($scope.message);
    //------footer style------------//
    $scope.removeHouseButton = true;
    $scope.buttonClass = 'btn btn-danger btn-sm';
    $scope.glyphClass = 'glyphicon glyphicon-fire';
    $scope.buttonMessage = 'Delete';


    console.info($scope.house);
    //------------- REMOVE -------------//
    $scope.deleteHouse = function () {
        $http.post($scope.serverstring + 'api/v1.0/admin/removehouse', { //TODO change addNewHouse API(?)
            houseid: $scope.house.organization_id,
            orgid: $scope.house.organization_parent_organization_id
        }).success(function (data, status, headers, config) {
            originalscope.getOrgDetail();
            $scope.cancel();
            if (angular.isObject(data)) {
                $timeout(function () {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        originalscope.messages = [];
                    }, 3000);
                });
            }
        }).
            error(function (data, status, headers, config) {
                // log error
                originalscope.getOrgDetail();
                $scope.cancel();
                $timeout(function () {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        originalscope.messages = [];
                    }, 3000);
                });
            });
    };

    //-------------CLOSE-------------//
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

//---------------------------------------------------------------------//
//-----------------------RequestSampleController-----------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('RequestSampleController', function ($scope, $http, $timeout, $uibModal, $uibModalInstance, $uibModalStack, options, originalscope, $filter) {

    $scope.products = [];
    $scope.colours = [];
    $scope.sizes = [];

    $scope.emblem = angular.copy(options.emblem);
    $scope.emblem_style_ref = $scope.emblem['tesco_style_ref'];
    $scope.emblem_threads = $scope.emblem['thread_array'];

    const EMBLEM_MAX_SIZE = 61; //max mm size for caps etc
    $scope.emblemTooBig = false;

    $scope.sample = {};
    $scope.requestData = {};

    // used to check similar colours of emblem&threads
    $scope.emblem.threadsCopy = [];



    $scope.fetchProducts = function () {
        $http.get($scope.serverstring + 'api/v1.0/admin/getembstyles').
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.products = data.data;
                    // Remove small products if emblem size is too big
                    if (parseFloat($scope.emblem['size__mm_'].split('x')[1]) >= EMBLEM_MAX_SIZE) {
                        $scope.emblemTooBig = true;
                        for (var product in $scope.products) {
                            if ($scope.products.hasOwnProperty(product)) {
                                if ($scope.products[product]['product_emblem_small'] == 1) {
                                    delete $scope.products[product]; // leaves null values..
                                }
                            }
                        }
                    }
                }

                // Remove null values to show properly
                $scope.products = $scope.products.filter(function (product) {
                    return $scope.products[product] !== null;
                });

            }).error(function (data, status, headers, config) {
                // log error
                $scope.products = {};
            });
    };

    $scope.fetchProducts();

    $scope.getProductSizes = function (productColorID) {
        $scope.sizes = [];
        $http.post('api/v1.0/admin/getproductsizes', {
            product_colour_id: productColorID
        }).
            success(function (data, status, headers, config) {
                $scope.sizes = data;
            }).
            error(function (data, status, headers, config) {
                // log error
                return null;
            });
    }

    $scope.getProductColours = function (productID, object) {
        $http.get($scope.serverstring + 'api/v1.0/admin/getproductcolours?pid=' + productID).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope[object] = data.data;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                return null;
            });
    };

    $scope.$watch('sample.product', function (newVal, oldVal) {
        if (!$scope.sample.product) {
            $scope.sample.colour = '';
            $scope.sample.colourRGB = '';
            $scope.sample.size = '';
            $scope.sizes = [];
            $scope.colour = [];
            $scope.selectedColourImage = '';
        }
        if (newVal !== undefined) {
            $scope.selectedGMOStyle = '';
            $scope.selectedColourStyle = '';
            $scope.selectedColourImage = '';
            $scope.sample.colour = '';
            $scope.sample.colourRGB = '';
            $scope.selectedLogoImage = '';
            //$scope.sample.logo = '';

            //pick selected product object from list
            for (var key in $scope.products) {
                if ($scope.products[key].product_id == newVal) {
                    var selectedproduct = $scope.products[key];
                }
            }
            //create emblems array based on a chosen product
            //$scope.usableEmblems = $scope.emblemsApproved;
            $scope.usableEmblems = $scope.emblemsApproved;

            if (selectedproduct['product_emblem_small'] == '1') {
                $scope.usableEmblems = $filter('smallLogo')($scope.usableEmblems, { maxSize: 61 });
            }

            $scope.getProductColours(newVal, 'colours');

        }
    });

    // Colour tolerance
    $scope.tolerance = 84; // around 25~30% tolerance
    $scope.$watch('tolerance', function (newVal, oldVal) {
        if (newVal !== undefined && newVal !== '') {
            $scope.emblem.threadsCopy.length = 0;
            for (var thread in $scope.emblem.emblemThreads) {
                if (($scope.emblem.emblemThreads[thread]).length) {
                    $scope.emblem.threadsCopy.push({ title: $scope.emblem.emblemThreads[thread], rgb_diffrence: $scope.isSimilarColour($scope.sample.colourRGB, $scope.formElements.threads[$scope.emblem.emblemThreads[thread]].rgb, $scope.tolerance) });
                }
            }
        }
    })


    $scope.$watch('sample.colour', function (newVal, oldVal) {
        if (!$scope.sample.colour) {
            $scope.sample.size = '';
            $scope.sizes = [];
        }
        if (newVal !== undefined && newVal !== '') {
            $scope.getProductSizes($scope.sample.colour);

            // set new colour values
            for (var colour in $scope.colours) {
                if ($scope.sample.colour == $scope.colours[colour]['product_colour_id']) {
                    $scope.sample.colourRGB = $scope.colours[colour]['colour_rgb'];
                }
            }

            // recalculate colours similarities
            $scope.emblem.threadsCopy.length = 0;
            for (var thread in $scope.emblem.emblemThreads) {
                if (($scope.emblem.emblemThreads[thread]).length) {
                    $scope.emblem.threadsCopy.push({ title: $scope.emblem.emblemThreads[thread], rgb_diffrence: $scope.isSimilarColour($scope.sample.colourRGB, $scope.formElements.threads[$scope.emblem.emblemThreads[thread]].rgb, $scope.tolerance) });
                }
            }

            $scope.selectedColourStyle = $scope.colours[newVal].product_colour_style_ref;
            $scope.selectedGMOStyle = $scope.colours[newVal].product_colour_gmo_ref;
            $scope.selectedColourImage = $scope.colours[newVal].product_colour_image_url;
            $scope.selectedLogoBackground = $scope.colours[newVal].colour_rgb;
        }
    });

    // compare rgb1 and rgb2, return is they are similar
    // tolerance is the "distance" of colors in range 0-255
    $scope.isSimilarColour = function (rgb1, rgb2, tolerance) {
        if (tolerance == undefined) {
            tolerance = 32;
        }
        if (rgb1 && rgb2) {
            var rgb1 = rgb1.split(',');
            var rgb2 = rgb2.split(',');

            return Math.abs(rgb1[0] - rgb2[0]) <= tolerance &&
                Math.abs(rgb1[1] - rgb2[1]) <= tolerance &&
                Math.abs(rgb1[2] - rgb2[2]) <= tolerance;
        } else {
            return false;
        }
    }

    $scope.requestSample = function () {
        $scope.loading = true;
        $scope.requestData = {
            organization_id: $scope.emblem['organization_id'],
            organization_logo_id: $scope.emblem['organization_logo_id'],
            emblem_style_ref: $scope.emblem['tesco_style_ref'],
            //emblem_threads: $scope.emblem['thread_array'],
            product_sku: $scope.sizes['product_sku'],
            product_size: $scope.sample.size
        };

        // Get product data by id
        for (var product in $scope.products) {
            if ($scope.sample.product == $scope.products[product]['product_id']) {
                $scope.requestData.product = $scope.products[product];
                break;
            }
        }

        // Get colour name by id
        for (var colour in $scope.colours) {
            if ($scope.sample.colour == $scope.colours[colour]['product_colour_id']) {
                $scope.requestData.background_rgb = $scope.colours[colour]['colour_rgb'];
                $scope.requestData.colour = $scope.colours[colour]['colour_name'];
                break;
            }
        }

        // Get product sku by size name
        for (var size in $scope.sizes) {
            if ($scope.sample.size == $scope.sizes[size]['product_size_name']) {
                $scope.requestData.product_sku = $scope.sizes[size]['product_sku']
                break;
            }
        }

        //there is an old function of requestsample( by andy )
        $http.post('api/v1.0/admin/samplerequest', {
            sample_request: $scope.requestData
        }).
            success(function (data, status, headers, config) {
                $scope.loading = false;
                originalscope.messagesReference = data.messages;
                $uibModalInstance.close(data.messages);
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.loading = false;
                $uibModalStack.dismissAll(); //close all
                originalscope.messagesReference = data.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
                return null;
            });
    }
    //-------------CLOSE-------------//
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});


//---------------------------------------------------------------------//
//--------------------AdminDeleteNoteAlertController-------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('AdminDeleteNoteAlertController', function ($scope, $http, $route, $rootScope, $uibModalInstance, $sce, $location) {

    $scope.question = {};

    $scope.question['title'] = $sce.trustAsHtml('Remove alert');
    $scope.question['message'] = $sce.trustAsHtml('<h4 class="text text-danger">Are you sure you want to remove this alert?</h4>');

    // Delete
    $scope.ok = function () {
        $scope.deleteAlertNote = true;
        $uibModalInstance.close($scope.deleteAlertNote);
    };

    // Close modal
    $scope.cancel = function () {
        $scope.deleteAlertNote = false;
        $uibModalInstance.dismiss('cancel');
    };

});


//---------------------------------------------------------------------//
//--------------- AdminEditEmblemBackgroundController -----------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('AdminEditEmblemBackgroundController', function ($scope, $http, $route, $rootScope, $uibModalInstance, $sce, $location) {

    $scope.question = {};

    $scope.question['title'] = $sce.trustAsHtml('Update emblem background');
    $scope.question['message'] = $sce.trustAsHtml('<h4 class="text text-info">Are you sure you want apply this background?</h4>');

    // Delete
    $scope.ok = function () {
        $scope.updateEmblem = true;
        $uibModalInstance.close($scope.updateEmblem);
    };

    // Close modal
    $scope.cancel = function () {
        $scope.updateEmblem = false;
        $uibModalInstance.dismiss('cancel');
    };

});