/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('PasswordController', function ($scope,$http) {
    $scope.user = null;
    $scope.messages = null;

    $scope.doSubmit = function(){
        $http.post($scope.serverstring+'api/v1.0/login/reset',{
            "user":$scope.user
        }).
            success(function(data, status, headers, config) {
                if(angular.isObject(data)){
                    $scope.response = data;
                    $scope.messages = data.messages;
                    //console.log($scope.messages);
                }
            }).
            error(function(data, status, headers, config) {
                // log error
                $scope.messages = data.messages;

            });
    };

    $scope.submitForm = function(){
        // Clear any error messages.
        $scope.messages = [];
        $scope.doSubmit();
    };
});
