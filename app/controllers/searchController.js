/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('SchoolsController', function ($scope, $http) {
    $scope.showingclass = '';

    $scope.exampleText = 'exampleText';

    $scope.$watch('searchText', function (val) {
        $scope.searchTerm = val;
        $scope.placeholder = "";
        // only fire if something there at least 3 characters.
        if (angular.isDefined(val) && val.length >= 3) {

            $http.post($scope.serverstring+'api/v1.0/organization/lookup', {
                "q": val
            }).
                success(function (data, status, headers, config) {
                    if (angular.isObject(data)) {
                        $scope.schools = data.data;
                        $scope.totalRows = data.metrics.totalRows;
                        $scope.resultRows = data.metrics.resultRows;
                        $scope.showingRows = 30;
                        $scope.showingclass = 'resultsshowing';
                        $scope.searchedFor = val;
                    }
                }).
                error(function (data, status, headers, config) {
                    // log error
                });

        } else {
            // clear the results.
            $scope.schools = '';
            $scope.showingRows = 0;
            $scope.totalRows = 0;
            $scope.resultRows = 0;
            $scope.showingclass = '';
            $scope.searchedFor = null;
        }
    });

    $scope.testFunc = function () {
        // get the next lot and append it to $scope.schools
        $http.post($scope.serverstring+'api/v1.0/organization/lookup', {
            "q": $scope.searchTerm,
            "s": $scope.showingRows
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.schools = $scope.schools.concat(data.data);
                    $scope.totalRows = data.metrics.totalRows;
                    $scope.showingRows += data.metrics.resultRows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
        // More button?
    };

    $scope.processEvent = function (event) {
        $scope.placeholder = angular.element(event.target).text();
    };

    $scope.updatePlaceholder = function () {
        $scope.placeholder = $('#schoolTyper .typer').html();
    };

});

uesApp.controllerProvider.register('CarouselDemoCtrl', function ($scope, $http) {
    $http.get($scope.serverstring+'api/v1.0/products/ribbon').
        success(function (data, status, headers, config) {
            if (angular.isObject(data)) {
                $scope.productPanes = data.products;
            }
        }).
        error(function (data, status, headers, config) {
            // log error
        });
});

uesApp.controllerProvider.register('StatisticsController', function ($scope, $http) {
    $http.get($scope.serverstring+'api/v1.0/stats/top').
        success(function (data, status, headers, config) {
            if (angular.isObject(data)) {
                $scope.statistics = data.statistics;
            }
        }).
        error(function (data, status, headers, config) {
            // log error
        });
});
