/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('SupportController', function($scope, $http, $filter, $timeout, $log, $window, $uibModal) {

    //initial tesco charts values
    $scope.tescoChart1 = {};
    $scope.tescoChart2 = {};
    $scope.tescoChart3 = {};
    $scope.tescoChart4 = {};

    $scope.feedbackCount = 328;

    $scope.schools = [{
        test: 'abc'
    }, {
        test: 'def'
    }, {
        test: 'ghi'
    }]; // stub object for development

    /* Pagination */
    $scope.totalItems = 64;
    $scope.currentPage = 4;

    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function() {
        $log.log('Page changed to: ' + $scope.currentPage);
    };

    $scope.maxSize = 9;
    $scope.bigTotalItems = 175;
    $scope.bigCurrentPage = 1;

    $scope.loadResources = function() {
        $scope.resources = []; // stub object for development

        // get them from database.
        $http.get($scope.serverstring + 'api/v1.0/faqs/getresources').
        success(function(data, status, headers, config) {
            if (angular.isObject(data)) {
                $scope.resources = data;
            }
        }).
        error(function(data, status, headers, config) {
            // log error
        });
    };

    $scope.loadFAQ = function() {
        // FAQ stuff.
        $scope.FAQuestions = [];
        $scope.UPQs = [];

        // get them from database.
        $http.get($scope.serverstring + 'api/v1.0/faqs/getfaqs').
        success(function(data, status, headers, config) {
            if (angular.isObject(data)) {
                $scope.FAQuestions = data;

                angular.forEach(data, function(section, key) {
                    angular.forEach(section.sectionFAQs, function(sectionFAQ) {
                        if (sectionFAQ.uniformactive == 1) {
                            $scope.UPQs.push(sectionFAQ);
                        }
                    });

                });
            }
        }).
        error(function(data, status, headers, config) {
            // log error
        });

        $scope.loadResources();
    }();


    $scope.rf = {};
    $scope.ss = {};
    $scope.wp = {};
    $scope.nl = {};
    $scope.downloadReport = function(type) {
        $scope.loadingON();
        var parameters = {};
        var endpoint;
        switch (type) {
            case 'emb':
                endpoint = 'embroideredallocation';
                break;
            case 'embg':
                endpoint = 'embroideredallocationgmo';
                break;
            case 'plng':
                endpoint = 'plainallocationgmo';
                break;
            case 'don':
                endpoint = 'consolodateddonations';
                break;
            case 'lu':
                endpoint = 'createlusheet';
                break;
            case 'ls':
                endpoint = 'liveorganizations';
                break;
            case 'ao':
                endpoint = 'allorganizations';
                break;
            case 'as':
                endpoint = 'activatingorganizations';
                break;
            case 'ws':
                endpoint = 'withdrawnorganizations';
                break;
            case 'dp':
                endpoint = 'donationpaymentlist';
                break;
            case 'sd':
                endpoint = 'stilldigitizing';
                break;
            case 'wp':
                endpoint = 'welcomepackreport';
                parameters = {
                    fromdate: $scope.wp.orderdate.from,
                    todate: $scope.wp.orderdate.to
                };
                break;
            case 'ss':
                endpoint = 'samplessentreport';
                parameters = {
                    fromdate: $scope.ss.orderdate.from,
                    todate: $scope.ss.orderdate.to
                };
                break;
            case 'rf':
                endpoint = 'referafriendlist';
                parameters = {
                    fromdate: $scope.rf.orderdate.from,
                    todate: $scope.rf.orderdate.to
                };
                break;
            case 'nl':
                endpoint = 'nominationlist';
                parameters = {
                    fromdate: $scope.nl.orderdate.from,
                    todate: $scope.nl.orderdate.to
                };
                break;
            default:
                endpoint = 'plainallocation';
                break;
        }

        $http.post($scope.serverstring + 'api/v1.0/reports/' + endpoint,
            parameters
        ).success(function(data, status, headers, config) {
            if (angular.isObject(data)) {
                // just open a new window on that?
                parameters = {};
                $window.open(data.data.filename);
                $scope.loadingOFF();
            }
        }).
        error(function(data, status, headers, config) {
            // log error
            console.log(new Error(data));
            alert('There was a problem loading this file - please try again');
            $scope.loadingOFF();
        });
    };

    // Loading
    $scope.loading = false;
    $scope.loadingON = function() {
        $timeout(function() {
            $scope.loading = true;
            return;
        })
    }

    $scope.loadingOFF = function() {
        $timeout(function() {
            $scope.loading = false;
            return;
        })
    }

    $scope.isFileStillDownloading = function() {
        if ($http.pendingRequests.length !== 0) {
            $scope.loadingON();
        } else {
            $scope.loadingOFF();
        }
    }



    $scope.updateDispatches = function() {
        $http.post('//www.slick-tools.com/sapphiredata/api/v1.0/ues/listorders', {
            p: $scope.dispatches.page,
            filter: $scope.searchFilter
        }).
        success(function(data, status, headers, config) {
            $scope.dispatchesData = data.data;
            $scope.dispatches.total = data.metrics.totalrows.totalRows;
        }).
        error(function(data, status, headers, config) {});
    };

    $scope.uploadMasterDonations = function(type) {

    };



    //---------------------------------//
    //----------- DISPATCHES ----------//
    //---------------------------------//

    $scope.getDispatchStatuses = function() {
        $http.get('//www.slick-tools.com/sapphiredata/api/v1.0/ues/getstatuses', {}).
        success(function(data, status, headers, config) {
            $scope.dispatchStatuses = data;
            console.log($scope.dispatchStatuses);
        }).
        error(function(data, status, headers, config) {});
    };
    $scope.getDispatchStatuses();

    $scope.searchFilter = {
        da_ref: '',
        da_cust_ref: '',
        da_delivery_name: '',
        trackingref: '',
        carrier: '',
        da_delivery_email: '',
        orderdate: { from: '', to: '' },
        createddate: {
            from: '',
            to: ''
        },
        pickeddate: {
            from: '',
            to: ''
        },
        requireddate: {
            from: '',
            to: ''
        },
        dispatcheddate: {
            from: '',
            to: ''
        },
        da_status: ''
    };

    $scope.maxSize = 9;
    $scope.dispatches = {
        page: 1,
        total: 0
    };

    $scope.updateDispatches = function() {
        $http.post('//www.slick-tools.com/sapphiredata/api/v1.0/ues/listorders', {
            p: $scope.dispatches.page,
            filter: $scope.searchFilter
        }).
        success(function(data, status, headers, config) {
            $scope.dispatchesData = data.data;
            $scope.dispatches.total = data.metrics.totalrows.totalRows;
        }).
        error(function(data, status, headers, config) {});
    };
    $scope.updateDispatches();

    /* Pagination */
    $scope.activePageChanged = function(currentPage) {
        $scope.dispatches.page = currentPage;
        $scope.updateDispatches();
    };

    /*  Clear Data */
    $scope.clearFilter = function() {
        $scope.searchFilter = {
            da_ref: '',
            da_cust_ref: '',
            da_delivery_name: '',
            trackingref: '',
            carrier: '',
            da_delivery_email: '',
            orderdate: { from: '', to: '' },
            createddate: { from: '', to: '' },
            pickeddate: { from: '', to: '' },
            requireddate: { from: '', to: '' },
            dispatcheddate: { from: '', to: '' },
            da_status: ''
        };
        $scope.updateDispatches();
    };



    $scope.showModal = function(type, size, options) {
        var template, controller;
        switch (type) {
            case 'dispatchInfo':
                template = 'pages/modals/admin-dispatch-info.html';
                controller = 'SupportDispatchController';
                break;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: template,
            controller: controller,
            size: size,
            resolve: {
                options: function() {
                    return options;
                },
                originalscope: function() {
                    return $scope;
                }
            }
        });

        modalInstance.result.then(function() {}, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };


    //---------------------------------//
    //------------ DEMANDS ------------//
    //---------------------------------//
    $scope.loadDemandScope = (function() {
        $scope.tescoChartData = null;
        $scope.loading = true;
        $scope.updateDispatches = function() {
            $http.get('http://www.ff-ues.com/api/v1.0/stats/demand', { //TODO change to serverstring
            }).success(function(data, status, headers, config) {
                //loaded data
                $scope.tescoChartData = data;


                $scope.tescoChart1 = {
                    "title": {
                        "text": "Forecast Inbound",
                        'background-color': '#337ab7',
                        'color': '#fff'

                    },

                    legend: {
                        overflow: 'page',
                        "highlight-plot": true,
                        'border-radius': '2px'

                    },
                    gui: {
                        behaviors: [ //default contextMenu behaviors
                            {
                                id: "Reload", //built-in id
                                text: "Reload", //default text
                                enabled: "none" //sets visibility to show
                            },
                            {
                                id: "SaveAsImage",
                                text: "View as PNG",
                                enabled: "all"
                            },
                            {
                                id: "DownloadPDF", //built-in id
                                text: "Export PDF", //modified text
                                enabled: "all" //sets visibility to show
                            },
                            {
                                id: "DownloadSVG",
                                enabled: "all"
                            },
                            {
                                id: "Print",
                                enabled: "all"
                            },
                            {
                                id: "ViewSource", //built-in id
                                enabled: "none" //sets visibility to hide
                            },
                        ]
                    },
                    globals: {
                        shadow: false,
                        fontFamily: "Helvetica",
                        'box-shadow': '0 0px 5px 0px rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12)'
                    },
                    type: "area",

                    scaleX: {
                        maxItems: ($scope.tescoChartData.xScaleTicks.length / 2),
                        transform: {
                            type: 'date',
                            all: "%d %M %y"
                        },
                        zooming: true,
                        values: $scope.tescoChartData.xScaleTicks,
                        lineColor: "black",
                        lineWidth: "1px",
                        tick: {
                            lineColor: "black",
                            lineWidth: "1px"
                        },
                        item: {
                            fontColor: "black"
                        },

                        guide: {
                            visible: true,
                            rules: [{
                                "rule": "%v ==" + $scope.tescoChartData.todayTime,
                                "line-color": "#f93",
                                "line-width": "2",
                                "line-style": "solid",
                                'visible': 'true'
                            }]
                        }
                    },
                    scaleY: {
                        label: {
                            "text": "scale-y"
                        },
                        lineColor: "#949494",
                        lineWidth: "1px",
                        tick: {
                            lineColor: "black",
                            lineWidth: "1px"
                        },
                        guide: {
                            lineStyle: "dashed",
                            lineColor: "#949494",
                        },
                        item: {
                            fontColor: "black"
                        }
                    },
                    crosshairX: {
                        scaleLabel: {
                            backgroundColor: "#ddd",
                            fontColor: "black"
                        },
                        plotLabel: {
                            backgroundColor: "#ddd",
                            fontColor: "black",
                            _text: "Number of hits : %v"
                        },
                        "marker": {
                            "type": "circle",
                            "size": 5, //Make sure to specify size.
                            "background-color": "yellow",
                            "border-width": 1,
                            "border-color": "gray"
                        }
                    },
                    plot: {
                        lineWidth: "2px",
                        aspect: "spline",
                        marker: {
                            visible: false
                        }
                    },
                    series: [{
                            text: "Actual demand",
                            values: $scope.tescoChartData.demandActual,
                            backgroundColor1: "#711414",
                            backgroundColor2: "#8e1919",
                            lineColor: "#711414"

                        },
                        {
                            text: "Forecast demand",
                            values: $scope.tescoChartData.demandTarget,
                            backgroundColor1: "	#2F4F4F",
                            backgroundColor2: "#568f8f",
                            lineColor: '	#2F4F4F'
                        }

                    ]
                };
                $scope.tescoChart2 = {
                    "title": {
                        "text": "Forecast outbound",
                        'background-color': '#337ab7',
                        'color': '#fff',
                        'text-align': 'center',
                    },
                    gui: {
                        behaviors: [ //default contextMenu behaviors
                            {
                                id: "Reload", //built-in id
                                text: "Reload", //default text
                                enabled: "none" //sets visibility to show
                            },
                            {
                                id: "SaveAsImage",
                                text: "View as PNG",
                                enabled: "all"
                            },
                            {
                                id: "DownloadPDF", //built-in id
                                text: "Export PDF", //modified text
                                enabled: "all" //sets visibility to show
                            },
                            {
                                id: "DownloadSVG",
                                enabled: "all"
                            },
                            {
                                id: "Print",
                                enabled: "all"
                            },
                            {
                                id: "ViewSource", //built-in id
                                enabled: "none" //sets visibility to hide
                            },
                        ]
                    },
                    legend: {
                        overflow: 'page',
                        "highlight-plot": true,
                        'border-radius': '2px'

                    },
                    globals: {
                        shadow: false,
                        fontFamily: "Helvetica"
                    },
                    type: "area",
                    scaleX: {
                        maxItems: ($scope.tescoChartData.xScaleTicks.length / 2),
                        transform: {
                            type: 'date',
                            all: "%d %M %y"
                        },
                        zooming: true,
                        values: $scope.tescoChartData.xScaleTicks,
                        lineColor: "black",
                        lineWidth: "1px",
                        tick: {
                            lineColor: "black",
                            lineWidth: "1px"
                        },
                        item: {
                            fontColor: "black"
                        },

                        guide: {
                            visible: true,
                            rules: [

                                {
                                    "rule": "%v ==" + $scope.tescoChartData.todayTime,
                                    "line-color": "#f93",
                                    "line-width": "2",
                                    "line-style": "solid"
                                }
                            ]
                        }
                    },
                    scaleY: {
                        lineColor: "#949494",
                        lineWidth: "1px",
                        tick: {
                            lineColor: "black",
                            lineWidth: "1px"
                        },
                        guide: {
                            lineStyle: "dashed",
                            lineColor: "#949494",
                        },
                        item: {
                            fontColor: "black"
                        }
                    },
                    crosshairX: {
                        scaleLabel: {
                            backgroundColor: "#ddd",
                            fontColor: "black"
                        },
                        plotLabel: {
                            backgroundColor: "#ddd",
                            fontColor: "black",
                            _text: "Number of hits : %v"
                        },
                        "marker": {
                            "type": "circle",
                            "size": 5, //Make sure to specify size.
                            "background-color": "yellow",
                            "border-width": 1,
                            "border-color": "gray"
                        }
                    },
                    plot: {
                        lineWidth: "2px",
                        aspect: "spline",
                        marker: {
                            visible: false,
                        }
                    },
                    series: [

                        {
                            text: "Actual outbound",
                            values: $scope.tescoChartData.outputActual,
                            // backgroundColor1: "#77d9f8",
                            lineColor: "#86CADE"
                        },

                        {
                            text: "Forecast outbound",
                            values: $scope.tescoChartData.outputTarget,
                            backgroundColor1: "#8e1919",
                            backgroundColor2: "#cb2424",
                            lineColor: "#cb2424"

                        }
                    ]
                };
                $scope.tescoChart3 = {
                    "title": {
                        "text": "Pot",
                        'background-color': '#337ab7',
                        'color': '#fff',
                        'text-align': 'center',
                    },
                    gui: {
                        behaviors: [ //default contextMenu behaviors
                            {
                                id: "Reload", //built-in id
                                text: "Reload", //default text
                                enabled: "none" //sets visibility to show
                            },
                            {
                                id: "SaveAsImage",
                                text: "View as PNG",
                                enabled: "all"
                            },
                            {
                                id: "DownloadPDF", //built-in id
                                text: "Export PDF", //modified text
                                enabled: "all" //sets visibility to show
                            },
                            {
                                id: "DownloadSVG",
                                enabled: "all"
                            },
                            {
                                id: "Print",
                                enabled: "all"
                            },
                            {
                                id: "ViewSource", //built-in id
                                enabled: "none" //sets visibility to hide
                            },
                        ]
                    },
                    legend: {
                        overflow: 'page',
                        "highlight-plot": true,
                        'border-radius': '2px'

                    },
                    globals: {
                        shadow: false,
                        fontFamily: "Helvetica"
                    },
                    type: "area",
                    scaleX: {
                        maxItems: ($scope.tescoChartData.xScaleTicks.length / 2),
                        transform: {
                            type: 'date',
                            all: "%d %M %y"
                        },
                        zooming: true,
                        values: $scope.tescoChartData.xScaleTicks,
                        lineColor: "black",
                        lineWidth: "1px",
                        tick: {
                            lineColor: "black",
                            lineWidth: "1px"
                        },
                        item: {
                            fontColor: "black"
                        },
                        guide: {
                            visible: true,
                            rules: [{
                                "rule": "%v ==" + $scope.tescoChartData.todayTime,
                                "line-color": "#f93",
                                "line-width": "2",
                                "line-style": "solid"
                            }]
                        }
                    },
                    scaleY: {
                        lineColor: "#949494",
                        lineWidth: "1px",
                        tick: {
                            lineColor: "black",
                            lineWidth: "1px"
                        },
                        guide: {
                            lineStyle: "dashed",
                            lineColor: "#949494"
                        },
                        item: {
                            fontColor: "black"
                        }
                    },

                    tooltip: {
                        visible: false
                    },
                    crosshairX: {
                        scaleLabel: {
                            backgroundColor: "#ddd",
                            fontColor: "black"
                        },
                        plotLabel: {
                            backgroundColor: "#ddd",
                            fontColor: "black",
                            _text: "Number of hits : %v"
                        },
                        "marker": {
                            "type": "circle",
                            "size": 5, //Make sure to specify size.
                            "background-color": "yellow",
                            "border-width": 1,
                            "border-color": "gray"
                        }
                    },
                    plot: {
                        lineWidth: "2px",
                        aspect: "spline",
                        marker: {
                            visible: false,
                        },
                    },
                    series: [

                        // {
                        //     text: "Forecast demand",
                        //     values: $scope.tescoChartData.demandTarget,
                        //     // backgroundColor1: "#77d9f8",
                        //     // backgroundColor2: "#272822",
                        //     //lineColor: "#86CADE"
                        // },

                        {
                            text: "Pot",
                            values: $scope.tescoChartData.potActual,
                            //backgroundColor1: "#22B14C",
                            //backgroundColor2: "#22B14C",
                            lineColor: '#794884',
                            backgroundColor1: '#794884',
                            backgroundColor2: '#9e66ab'
                        }
                    ]
                };
                $scope.tescoChart4 = {
                    title: {
                        "text": "Lead time",
                        'background-color': '#337ab7',
                        'color': '#fff',
                        'text-align': 'center',
                    },
                    gui: {
                        behaviors: [ //default contextMenu behaviors
                            {
                                id: "Reload", //built-in id
                                text: "Reload", //default text
                                enabled: "none" //sets visibility to show
                            },
                            {
                                id: "SaveAsImage",
                                text: "View as PNG",
                                enabled: "all"
                            },
                            {
                                id: "DownloadPDF", //built-in id
                                text: "Export PDF", //modified text
                                enabled: "all" //sets visibility to show
                            },
                            {
                                id: "DownloadSVG",
                                enabled: "all"
                            },
                            {
                                id: "Print",
                                enabled: "all"
                            },
                            {
                                id: "ViewSource", //built-in id
                                enabled: "none" //sets visibility to hide
                            },
                        ]
                    },
                    globals: {
                        shadow: false,
                        fontFamily: "Helvetica"
                    },

                    legend: {
                        overflow: 'page',
                        "highlight-plot": true,
                        'border-radius': '2px'

                    },
                    type: "area",
                    scaleX: {
                        maxItems: ($scope.tescoChartData.xScaleTicks.length / 2),
                        transform: {
                            type: 'date',
                            all: "%d %M %y"
                        },
                        zooming: true,
                        values: $scope.tescoChartData.xScaleTicks,
                        lineColor: "black",
                        lineWidth: "1px",
                        tick: {
                            lineColor: "black",
                            lineWidth: "1px"
                        },
                        item: {
                            fontColor: "black"
                        },
                        guide: {
                            visible: true,
                            rules: [{
                                "rule": "%v ==" + $scope.tescoChartData.todayTime,
                                "line-color": "#f93",
                                "line-width": "2",
                                "line-style": "solid"
                            }]
                        }
                    },
                    scaleY2: {
                        lineColor: "black",
                        lineWidth: "1px",
                        tick: {
                            lineColor: "black",
                            lineWidth: "1px"
                        },
                        guide: {
                            lineStyle: "dashed",
                            lineColor: "black"
                        },
                        item: {
                            fontColor: "black"
                        }
                    },

                    tooltip: {
                        visible: false
                    },
                    crosshairX: {
                        scaleLabel: {
                            backgroundColor: "#ddd",
                            fontColor: "black"
                        },
                        plotLabel: {
                            backgroundColor: "#ddd",
                            fontColor: "black",
                            _text: "Number of hits : %v"
                        },
                        "marker": {
                            "type": "circle",
                            "size": 5, //Make sure to specify size.
                            "background-color": "yellow",
                            "border-width": 1,
                            "border-color": "gray"
                        }
                    },
                    plot: {
                        lineWidth: "2px",
                        aspect: "spline",
                        marker: {
                            visible: false

                        }
                    },
                    series: [

                        {
                            text: "propActual",
                            values: $scope.tescoChartData.propActual,
                            // backgroundColor1: "#77d9f8",
                            // backgroundColor2: "#272822",
                            //lineColor: "#86CADE"
                        },

                        {
                            text: "prop Target",
                            values: $scope.tescoChartData.propTarget,
                            //backgroundColor1: "#22B14C",
                            //backgroundColor2: "#22B14C",
                            //lineColor: "#22B14C"
                        }
                    ]
                };
                $scope.loading = 0;

            }).error(function(data, status, headers, config) {

            });
        }();

        var a = true;

        $scope.columnSize = 'col-xs-6';
        $scope.switchView = function() {

            a = !a;
            if (a) {
                $scope.columnSize = 'col-xs-6';
                delete $scope.tescoChart1.legend, $scope.tescoChart2.legend, $scope.tescoChart3.legend, $scope.tescoChart4.legend;
            } else if (!a) {
                $scope.columnSize = 'col-xs-12';
                $scope.tescoChart1.legend = {
                    overflow: 'page',
                    "highlight-plot": true,
                    'border-radius': '2px'

                };
                $scope.tescoChart2.legend = {
                    overflow: 'page',
                    "highlight-plot": true,
                    'border-radius': '2px'

                };
                $scope.tescoChart3.legend = {
                    overflow: 'page',
                    "highlight-plot": true,
                    'border-radius': '2px'

                };
                $scope.tescoChart4.legend = {
                    overflow: 'page',
                    "highlight-plot": true,
                    'border-radius': '2px'

                };
            }
        };


        // Save for later, if it needs to be manipulated
        let tescoChart = {
            windowHeight: $(window).height(),
            windowWidth: $(window).width(),
            width: 0,
            height: 0,
            getChartSizes: function() {
                loading = true;
                $scope.$apply();
                console.log('in here.. loading');
                let frame = angular.element('#demands-chart');
                frame.on('load', function() {
                    loading = false;
                    $(frame).css('height', '100%'); // apply to fix -moz- flexbox issue
                    this.height = frame.height();
                    this.width = frame.width();
                    $scope.$apply();
                });


            },

            setChartSizes: function() {
                $('.loading-center').show();
                loading = true;
                let frame = angular.element('#demands-chart');
                frame.on('load', function() {
                    $(frame).css('height', '100%'); // apply to fix -moz- flexbox issue
                    this.height = frame.height();
                    this.width = frame.width();

                    setTimeout(function() {
                        loading = false;
                        console.log('chart ready!');

                    })
                });
            },
        };

        window.addEventListener('resize', function() {
            // try {
            //     tescoChart.resizeChart();

            // } catch (error) {
            //     console.log('Could not resize chart.. ' + error);
            // }
        });

        $(function() {
            tescoChart.getChartSizes();
        })

    });
});

//--------------------------------------------------------//
//------------- SUPPORT DISPATCH MODAL CTRL --------------//
//--------------------------------------------------------//
uesApp.controllerProvider.register('SupportDispatchController', function($scope, $http, $filter, $log, $window, $uibModal, $uibModalInstance, $timeout, options) {

    $scope.selectedDispatch = options.options;
    $scope.selectedDispatchID = $scope.selectedDispatch.da_id;


    //get dispatch info
    $scope.getDispatchInfo = function() {
        $http.post('//www.slick-tools.com/sapphiredata/api/v1.0/ues/loadorder', {
            da_id: $scope.selectedDispatchID
        }).
        success(function(data, status, headers, config) {
            $scope.dispatchInfoById = data.data;
            $scope.dispatchInfo = $scope.dispatchInfoById[$scope.selectedDispatchID];
        }).
        error(function(data, status, headers, config) {});
    };
    $scope.getDispatchInfo();







    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

});
