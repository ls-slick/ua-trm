/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('ProductsController', function ($scope, $uibModal,$log) {



    //fake product objects for development
    $scope.products =
        [
            {
                test: 'abc'
            },{
            test: 'def'
        },{
            test: 'ghi'
        }
        ];

    /* Pagination -- testing */
    $scope.totalItems = 64;
    $scope.currentPage = 4;
    $scope.maxSize = 9;
    $scope.bigTotalItems = 175;
    $scope.bigCurrentPage = 1;


    //ng-click="showModal('delete','lg',{ 'urn': product.test })"
    //Modals
    $scope.items = ['item1', 'item2', 'item3'];
    $scope.modalResult = null;
    $scope.showModal = function (type,size,options) {
        var template, controller;
        switch (type) {
            case 'delete':
                template = 'pages/modals/delete.html';
                controller = 'DeleteController';
                break;
            case 'edit':
                template = 'pages/modals/edit.html';
                controller = 'EditController';
                break;
            case 'add':
                template = 'pages/modals/add.html';
                controller = 'AddController';
                break;

            //no need for modals(?)
            /*case 'import':
                template = 'pages/modals/edit.html';
                controller = 'ImportController';
                break;
            case 'export':
                template = 'pages/modals/add.html';
                controller = 'ExportController';
                break;
             */
        }

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: template,
            controller: controller,
            size: size,
            resolve: {
                options:  function () {
                    return options;
                },
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });

    };

});


//--DELETE modal Controller-------------->
uesApp.controllerProvider.register('DeleteController', function ($scope, $uibModal,$uibModalInstance,$log){

    $scope.ok = function(){
        console.log('You have clicked ok button');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

//--EDIT modal Controller----------------->
uesApp.controllerProvider.register('EditController', function ($scope, $uibModal,$uibModalInstance,$log){

    $scope.ok = function(){
        console.log('You have clicked ok button');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

//--ADD-Embroided modal Controller------------------>
uesApp.controllerProvider.register('AddEmbroidedController', function ($scope, $uibModal,$uibModalInstance,$log){


    $scope.ok = function(){
        console.log('You have clicked ok button');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

//--ADD-Plain modal Controller------------------>
uesApp.controllerProvider.register('AddPlainController', function ($scope, $uibModal,$uibModalInstance,$log){



    $scope.ok = function(){
        console.log('You have clicked ok button');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

//--IMPORT CSV modal Controller------------------>
uesApp.controllerProvider.register('ImportController', function ($scope, $uibModal,$uibModalInstance,$log){

    $scope.ok = function(){
        console.log('You have clicked ok button');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    app.directive('fileReader', function() {
        return {
            scope: {
                fileReader:"="
            },
            link: function(scope, element) {
                $(element).on('change', function(changeEvent) {
                    var files = changeEvent.target.files;
                    if (files.length) {
                        var r = new FileReader();
                        r.onload = function(e) {
                            var contents = e.target.result;
                            scope.$apply(function () {
                                scope.fileReader = contents;
                            });
                        };

                        r.readAsText(files[0]);
                    }
                });
            }
        };
    });





});

//--EXPORT CSV modal Controller------------------>
uesApp.controllerProvider.register('ExportController', function ($scope, $uibModal,$uibModalInstance,$log){


    $scope.ok = function(){
        console.log('You have clicked ok button');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});