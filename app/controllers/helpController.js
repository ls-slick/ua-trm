/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('LiveHelpController', function ($scope,$uibModal,$http,$rootScope,$timeout,$location ) {
    $scope.isCollapsed = true;

    $rootScope.screenshot = function(){
        // hide the help box.
        $('#livehelp').hide();
        html2canvas($('body'), {
            onrendered: function(canvas) {
                // canvas is the final rendered <canvas> element
                // upload canvas?
                $('#livehelp').show();
                var imgData = canvas.toDataURL();
                // console.log(imgData);
                // upload it.
                $http.post($rootScope.serverstring+'api/v1.0/livehelp/screenshot',{
                    chatID: 1234,
                    imageData: imgData
                }).
                    success(function (data, status, headers, config) {
                        // save elements
                        $rootScope.formElements = data;
                    }).
                    error(function (data, status, headers, config) {
                        // log error
                    });
            }
        });
    };

});