/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('DonationsController', function ($scope, $http, $timeout, $rootScope) {

    $scope.form = {};
    //validation
    $scope.onlyNumbers = /^\d+$/;
    $scope.schoolBankData = {
        orgid : $scope.session.showingorg,
        SortCode: []
    };

    $scope.$watch('bankDetailsForm', function() {

    });
    //-------Donations data-------//
    $scope.hideAdditionalDonations = true;
    $scope.updateDonationsOrg = function () {
        $http.get($scope.serverstring+'api/v1.0/organization/getorgdonations?orgid='+$scope.session.organizations[$scope.session.showingorg].organization_id, {}).success(function (data, status, headers, config) {
            $scope.donations = data.donations;
            $scope.donationsAmount = ($scope.donations).length;
            $scope.initialDonationsToShow = 5;   //how many donations to show?
            if($scope.initialDonationsToShow < $scope.donationsAmount ){
                $scope.hideAdditionalDonations = true;
            }

            $scope.totalDonations = 0;
            $scope.totalPaid = 0;

            //counters
            for (var i = 0; i < $scope.donations.length; i++) {
                if ($scope.donations[i].donation_paid > 0) {
                    $scope.totalPaid += Math.abs($scope.donations[i].donation_amount);
                }
            }

            for (var j = 0; j < $scope.donations.length; j++) {
                if ($scope.donations[j].donation_paid == 0) {
                    $scope.totalDonations += Math.abs($scope.donations[j].donation_amount);
                }
            }

            //error message
        }).error(function (data, status, headers, config) {
            console.log("No data found.." + status);
            $scope.donations = null;
        });
    };

    $scope.updateDonationsOrg();

    $scope.getBankDetails = function () {
        $http.get($scope.serverstring+'api/v1.0/organization/getorgdonationbank?orgid='+$scope.session.organizations[$scope.session.showingorg].organization_id, {})
            .success(function (data, status, headers, config) {
                // fill bank details.
                $scope.schoolBankData = data.data;
                $scope.schoolBankData.orgid = $scope.session.showingorg;
            })
            .error(function (data, status, headers, config) {
                $scope.schoolBankData.accountName = '';
                $scope.schoolBankData.accountNumber = '';
                $scope.schoolBankData.SortCode[0] = '';
                $scope.schoolBankData.SortCode[1] = '';
                $scope.schoolBankData.SortCode[2] = '';
            });
    };

    $scope.getBankDetails();

    $scope.registerBank = function () {
        $http.post($scope.serverstring + 'api/v1.0/organization/updateorgbank', {
            formData: $scope.schoolBankData
        }).success(function (data, status, headers, config) {
            // message about success ?
            $scope.schoolBankData.saved = true;
            // Set the session var here for refresh.
            $rootScope.session.organizations[$rootScope.session.showingorg].organization_bank_verified = '1';

            $scope.messages = data.messages;
            $timeout(function () {
                // Make sure they see the success message before it closes.
                $scope.messages = [];
            }, 3000);
            $scope.form.bankDetailsForm.$setPristine();
        }).error(function (data, status, headers, config) {
            console.log("No data found.." + status);
            $scope.messages = data.messages;
            $timeout(function () {
                // Make sure they see the success message before it closes.
                $scope.messages = [];
            }, 3000);
        });
    };

});
