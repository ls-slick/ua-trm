/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('AccountController', function ($scope,$http,$uibModal,$timeout) {
    $scope.form = {};
    $scope.formdata = {};
    $scope.newUserForm = {};
    $scope.loading = false;

    $scope.typetitle = 'School/Group/Club';
    $scope.typemembernoun = 'members/students';
    $scope.formdata.orgmembercount = 1;
    $scope.formdata.orgcountry = '222';

    // first organization should be master.  Could switch this possibly in future.

    //----organization data----//
    $scope.formdata.orgid = $scope.session.organizations[$scope.session.showingorg].organization_id;
    $scope.formdata.orgurn = $scope.session.organizations[$scope.session.showingorg].school_URN;
    $scope.formdata.orgtype = $scope.session.organizations[$scope.session.showingorg].organization_type_id;
    $scope.formdata.orgname = $scope.session.organizations[$scope.session.showingorg].organization_name;
    $scope.formdata.orgmembercount = parseInt($scope.session.organizations[$scope.session.showingorg].organization_member_count);
    $scope.formdata.orgdefaultaddress = $scope.session.organizations[$scope.session.showingorg].organization_default_address_id;
    $scope.formdata.orgtelephone = $scope.session.organizations[$scope.session.showingorg].organization_telephone;
    $scope.formdata.orgfax = $scope.session.organizations[$scope.session.showingorg].organization_fax;
    $scope.formdata.orgemail = $scope.session.organizations[$scope.session.showingorg].organization_email;
    $scope.formdata.orgurl = $scope.session.organizations[$scope.session.showingorg].organization_url;
    $scope.formdata.orgschooldataid = $scope.session.organizations[$scope.session.showingorg].school_data_id;
    $scope.formdata.orglea = $scope.session.organizations[$scope.session.showingorg].school_local_authority_id;
    $scope.formdata.orgphase = $scope.session.organizations[$scope.session.showingorg].school_education_phase_id;

    $scope.formdata.orgdeflogoid = $scope.session.organizations[$scope.session.showingorg].organization_default_logo_id;
    $scope.formdata.orgfunds = $scope.session.organizations[$scope.session.showingorg].organization_funds;
    $scope.formdata.orgstatus = $scope.session.organizations[$scope.session.showingorg].organization_status_id;
    $scope.formdata.orgsubunits = $scope.session.organizations[$scope.session.showingorg].organization_has_subunits;
    $scope.formdata.orgparentid = $scope.session.organizations[$scope.session.showingorg].organization_parent_id;
    $scope.formdata.orglogosimported = $scope.session.organizations[$scope.session.showingorg].logos_imported;

    //---ORG address data ----//
    $scope.formdata.addressid = $scope.session.organizations[$scope.session.showingorg].addresses[0].address_id;
    $scope.formdata.orgaddress1 = $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_street_address;
    $scope.formdata.orgaddress2 = $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_street_address_2;
    $scope.formdata.orgaddress3 = $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_suburb;
    $scope.formdata.orgtown = $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_city;
    $scope.formdata.orgcounty = $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_state;
    $scope.formdata.orgpostcode = $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_postcode;
    $scope.formdata.orgcountry = $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_country_id;

    //--- Your Profile Data ---//
    $scope.formdata.usertype = $scope.session.user.user_type_id;
    $scope.formdata.userid = $scope.session.user.user_id;
    $scope.formdata.userdefaultaddress = $scope.session.user.user_default_address_id;
    $scope.formdata.username = $scope.session.user.user_name;
    $scope.formdata.userfirstname = $scope.session.user.user_first_name;
    $scope.formdata.userlastname = $scope.session.user.user_last_name;
    $scope.formdata.useremail = $scope.session.user.user_email;
    $scope.formdata.useravatar = $scope.session.user.user_avatar;
    $scope.formdata.userworkphone = $scope.session.user.user_work_phone;
    $scope.formdata.usermobilephone = $scope.session.user.user_mobile_phone;
    $scope.formdata.userhomephone = $scope.session.user.user_home_phone;
    $scope.formdata.userhomeemail = $scope.session.user.user_home_email;
    $scope.formdata.usertitle = $scope.session.user.user_title_id;
    $scope.formdata.usersalutation = $scope.session.user.user_salutation;
    $scope.formdata.usertypename = $scope.session.user.user_type_name;
    $scope.formdata.usertypedefaultroute = $scope.session.user.user_type_default_route;
    $scope.formdata.userpassword = '';
    $scope.formdata.usernewpassword = '';
    $scope.formdata.userconfirmpassword = '';

    console.log($scope.formdata.usertitle);

    //--------New User---------//
    $scope.confirmAddUser = function(){

        var message = 'Are you sure you wish to add this user?<br /><br />' +
            '<b>' + $scope.newUserForm.email + '</b>' +
            '<br /><br />';

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/confirm.html',
            controller: 'ConfirmController',
            size: 'md',
            resolve: {
                options: function () {
                    return {
                        title: 'Add User',
                        message: message
                    };
                }
            }
        });
        modalInstance.result.then(function(confirmStatus) {
            // close.
            if(confirmStatus){
                // YES.
                $scope.loading = true;
                $scope.newUserForm.organizationID = $scope.session.showingorg;

                $http.post($scope.serverstring+'api/v1.0/organization/adduser', {
                    formData: $scope.newUserForm
                }).
                    success(function (data, status, headers, config) {
                        // message about email.  Modal?
                        if(angular.isObject(data)){
                            $timeout(function() {
                                $scope.messages = data.messages;
                                $scope.session = data.session;
                                $scope.newUserForm = {};
                                // reload users.
                                $scope.form.addUserForm.$setPristine();
                                $timeout(function(){
                                    $scope.messages = [];
                                },3000);
                                $scope.loading = false;
                            });
                        }
                    }).
                    error(function (data, status, headers, config) {
                        // log error
                        if(angular.isObject(data)) {
                            $timeout(function() {
                                $scope.messages = data.messages;
                                $timeout(function(){
                                    $scope.messages = [];
                                },3000);
                            });
                        } else {
                            $scope.messages = [];
                        }
                        $scope.loading = false;
                    });
            }
        }, function () {
            // dismiss
        });
    };

    //--------delete User---------//
    $scope.confirmDeleteUser = function(user){

        var message = 'Are you sure you wish to delete this user?<br /><br />' +
            '<b>' + user.user_email + '</b>' +
            '<br /><br />';

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/confirm.html',
            controller: 'ConfirmController',
            size: 'md',
            resolve: {
                options: function () {
                    return {
                        title: 'Delete User',
                        message: message
                    };
                }
            }
        });
        modalInstance.result.then(function(confirmStatus) {
            // close.
            if(confirmStatus){
                // YES.
                $http.post($scope.serverstring+'api/v1.0/organization/deleteuser', {
                    formData: {
                        user: user,
                        organizationID: $scope.session.showingorg
                    }
                }).
                    success(function (data, status, headers, config) {
                        // message about email.  Modal?
                        if(angular.isObject(data)){
                            $timeout(function() {
                                $scope.messages = data.messages;
                                $scope.session = data.session;
                                $timeout(function(){
                                    $scope.messages = [];
                                },3000);
                            });
                        }
                    }).
                    error(function (data, status, headers, config) {
                        // log error
                        if(angular.isObject(data)) {
                            $timeout(function() {
                                $scope.messages = data.messages;
                                $timeout(function(){
                                    $scope.messages = [];
                                },3000);
                            });
                        } else {
                            $scope.messages = [];
                        }
                    });
            }
        }, function () {
            // dismiss
        });
    };

    //--------change primary User---------//
    $scope.setPrimaryUser = function(user){

        var message = 'Are you sure you wish to change the primary user to<br /><br />' +
            '<b>' + user.user_email + '</b>?' +
            '<br /><br />';

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/confirm.html',
            controller: 'ConfirmController',
            size: 'md',
            resolve: {
                options: function () {
                    return {
                        title: 'Change Primary User',
                        message: message
                    };
                }
            }
        });
        modalInstance.result.then(function(confirmStatus) {
            // close.
            if(confirmStatus){
                // YES.
                $http.post($scope.serverstring+'api/v1.0/organization/changeprimaryuser', {
                    formData: {
                        user: user,
                        organizationID: $scope.session.showingorg
                    }
                }).
                    success(function (data, status, headers, config) {
                        // message about email.  Modal?
                        if(angular.isObject(data)){
                            $timeout(function() {
                                $scope.messages = data.messages;
                                $scope.session = data.session;
                                $timeout(function(){
                                   $scope.messages = [];
                                },3000);
                            });
                        }
                    }).
                    error(function (data, status, headers, config) {
                        // log error
                        if(angular.isObject(data)) {
                            $timeout(function() {
                                $scope.messages = data.messages;
                                $timeout(function(){
                                    $scope.messages = [];
                                },3000);
                            });
                        } else {
                            $scope.messages = [];
                        }
                    });
            }
        }, function () {
            // dismiss
        });
    };

    //-------------SEND FORM--------------//

    $scope.sendupdate = function (type) {
        $timeout(function() {
            $scope.messages = [];
            $scope.loading = true;
        });
        $scope.update(type);
    };

    $scope.update = function (type) {

        $http.post($scope.serverstring+'api/v1.0/organization/updateorganization', {
            formdata: $scope.formdata,
            calltype: type
        }).
        success(function (data, status, headers, config) {
            // message about email.  Modal?
            //console.log('sent')
            if(angular.isObject(data)){
                $scope.response = data;
                $scope.result = data.data;
                $timeout(function() {
                    $scope.messages = data.messages;
                    $scope.session = data.session;
                    // clear password fields.
                    $scope.formdata.currentpassword = '';
                    $scope.formdata.pw1 = '';
                    $scope.formdata.pw2 = '';

                    $timeout(function(){
                        $scope.messages = [];
                    },3000);
                    $scope.loading = false;
                    $scope.form.userForm.$setPristine();
                });

            }

        }).
        error(function (data, status, headers, config) {
            // log error
                if(angular.isObject(data)) {
                    $timeout(function() {
                        $scope.messages = data.messages;
                        $timeout(function(){
                            $scope.messages = [];
                        },3000);
                    });
                } else {
                    $scope.messages = [];
                }
                $scope.loading = false;
        });

    };


    $scope.updateUser = function()
    {
        console.log('User saved');
    };

    $scope.updateOrganization = function(){

    };

   // $scope.formdata.orgaddress2 = $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_street_address_2;
   // $scope.formdata.orgaddress3 = $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_suburb;

});
