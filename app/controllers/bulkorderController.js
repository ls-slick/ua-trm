/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('BulkorderController', function ($scope, $http, $timeout, $uibModal,$uibModalStack, $rootScope) {

    //$scope.session.organizations[$scope.session.showingorg].organization_can_bulk = 1;
    $scope.isCollapsed = false;
    $scope.input_size = null;
    $scope.formdata = {
        orgId: $scope.session.showingorg
    };

    $scope.form = {};

    if($scope.session.organizations[$scope.session.showingorg].organization_parent_name){

    }
    $scope.bulkData = {
        orgId: $scope.session.showingorg,
        orgname: $scope.session.organizations[$scope.session.showingorg].organization_name,
        orgcontactname: $scope.session.user.user_first_name+" "+$scope.session.user.user_last_name,
        orgtelephone: $scope.session.organizations[$scope.session.showingorg].organization_telephone,
        orgemailaddress: $scope.session.organizations[$scope.session.showingorg].organization_email,
        orgaddress1: $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_street_address,
        orgaddress2: $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_street_address_2,
        orgaddress3: $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_suburb,
        orgtown: $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_city,
        orgcounty: $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_state,
        orgpostcode: $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_postcode,
        orgcountry: $scope.session.organizations[$scope.session.showingorg].addresses[0].entry_country_id,
        amount: []
    };

    if($scope.session.organizations[$scope.session.showingorg].organization_parent_name){
        $scope.bulkData.orgname = $scope.session.organizations[$scope.session.showingorg].organization_parent_name + ' ('+$scope.session.organizations[$scope.session.showingorg].organization_name+')';
    }
    //$scope.inputs.input_size;

    $scope.$watch('input_size', function (val) {
        return $scope.input_size;
    });

    $scope.newscopeobj = {};

    $scope.orderValue = 0.00;

    $scope.$watch('bulkData.amount', function (newVal, oldVal) {

        $scope.orderValue = 0.00;
        // needed to update the validity of other cells that have to add up to 50.
        for (var prop in $scope.form.bulkForm) {
            if($scope.bulkData.amount){ // allow for emptying array.

                if ($scope.form.bulkForm.hasOwnProperty(prop) && prop.indexOf('$') < 0) {
                    // Does this have standard prooerties?
                    if($scope.form.bulkForm[prop].$name.toLowerCase()
                            .indexOf(('[')
                                .toLowerCase()) > 0){
                        // is this one of the amount[X] fields?


                        var realVal = $scope.form.bulkForm[prop].$viewValue;
                        if ($scope.form.bulkForm[prop].$viewValue) {

                            $scope.form.bulkForm[prop].$setViewValue(parseInt(realVal) - 1, 'your event name', true);
                            $scope.form.bulkForm[prop].$setViewValue(parseInt(realVal), 'your event name', true);
                            $scope.orderValue += realVal * parseFloat($('[name="'+$scope.form.bulkForm[prop].$name+'"]').data('priceeach'));

                        }
                    }
                }
            }

        }

        // update prices?
    }, true);

    $scope.confirmBulkOrder = function () {
        var message = 'Are you sure you wish to place this order?<br /><br />' +
            '<br /><br />';

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/confirm.html',
            controller: 'ConfirmBulkOrderController',
            size: 'md',
            backdrop: 'static',
            resolve: {
                options: function () {
                    return {
                        title: 'Place Bulk Order',
                        message: message
                    }
                },
                originalscope: function () {
                    return $scope;
                }
            }
        });
        modalInstance.result.then(function(confirmStatus) {
            // close.
            if(confirmStatus){
                // YES.
                $scope.sendForm();
            }
        }, function () {
            // dismiss
        });
    };

    $scope.sendForm = function () {
        $scope.loading = 1;
        $http.post($scope.serverstring + 'api/v1.0/organization/placebulkorder', {
            requestData: $scope.bulkData
        }).success(function (data, status, headers, config) {
            // message about email.  Modal?
            $scope.loading = 0;
            if (angular.isObject(data)) {
                delete $scope.bulkData.amount;
                console.log($scope.bulkData);
                $scope.form.bulkForm.$setPristine();
                $uibModalStack.dismissAll();
                $timeout(function() {
                    $scope.messages = data.messages;
                   // $uibModalStack.dismissAll();
                    $timeout(function(){
                        $scope.messages = [];
                    },3000);
                });
            }
        }).error(function (data, status, headers, config) {
            // log error
            $scope.loading = 0;
            $uibModalStack.dismissAll();
            $timeout(function() {
                $scope.messages = data.messages;
                $timeout(function(){
                    $scope.messages = [];
                },3000);
            });
        });

    };


    $scope.confirmBilling = function () {
        var message = 'Are you sure you wish to submit these billing details?<br /><br />' +
            '<br /><br />';

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/confirm.html',
            controller: 'ConfirmController',
            size: 'md',
            resolve: {
                options: function () {
                    return {
                        title: 'Submit Billing Details',
                        message: message
                    };
                }
            }
        });
        modalInstance.result.then(function(confirmStatus) {
            // close.
            if(confirmStatus){
                // YES.
                $scope.sendBilling();
            }
        }, function () {
            // dismiss
        });
    };

    $scope.sendBilling = function(){
        $http.post($scope.serverstring + 'api/v1.0/organization/submitbilling', {
            requestData: $scope.formdata
        }).success(function (data, status, headers, config) {
            // message about email.  Modal?
            if (angular.isObject(data)) {
                $timeout(function() {
                    $scope.messages = data.messages;
                    $rootScope.session = data.session;
                    // $uibModalStack.dismissAll();
                    $timeout(function(){
                        $scope.messages = [];
                    },3000);
                });
            }
        }).error(function (data, status, headers, config) {
            // log error
            $timeout(function() {
                $scope.messages = data.messages;
                $timeout(function(){
                    $scope.messages = [];
                },3000);
            });
        });
    };

    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.bulkData.length; i++){
            var product = $scope.bulkData[i];
            total += product;
        }
        return total;
    };

    //-------EMBROIDED_PRODUCT_CREATED_DATA------/
    $http.get($scope.serverstring+'api/v1.0/organization/getorgproductsandsizes?orgid='+$scope.session.organizations[$scope.session.showingorg].organization_id, {
    }).success(function(data, status, headers, config) {
        $scope.created = data.products;
        //Get sizes.
        //error message
    }).error(function(data, status, headers, config) {
        console.log("No data found.." + status);
        $scope.created = null;
    });
});

uesApp.controllerProvider.register('ConfirmBulkOrderController', function ($scope, $http, $timeout, $uibModal,$uibModalInstance, $rootScope,options,$sce,originalscope) {

    $scope.question = options;
    $scope.question.message = $sce.trustAsHtml($scope.question.message);

    //------MODAL-----/
    $scope.ok = function () {
        originalscope.sendForm();
        $scope.loading = 1;
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});