/**
 * Created by andyl on 16/12/2015.
 */
(function () {
    var uesApp = angular.module('uesApp');
    //---------------------------------------------------------------------//
    //--------------------------UniformController--------------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('UniformController', function ($scope, $http, $uibModal, $log, $filter, $route) {

        $scope.refreshSession = function(){
            $scope.updateSelectedOrg();
            $scope.updatePlainOrg();
            $scope.updateEmblemsOrg();
            $scope.houses = $filter('filter')($filter('toArray')($scope.session.organizations), {type: 'H'}, true);
            $scope.linkedorgs = $filter('filter')($filter('toArray')($scope.session.organizations), {type: 'O'}, true);
        };


        $scope.updateLinkedOrg = function (orgid) {
            $scope.session.showingorg = orgid;
            $scope.refreshSession();
        };

        $scope.updateSelectedOrg = function () {
            //-------EMBROIDED_PRODUCT_CREATED_DATA------/
            $http.get($scope.serverstring + 'api/v1.0/organization/getorgproducts?orgid=' + $scope.session.organizations[$scope.session.showingorg].organization_id, {}).success(function (data, status, headers, config) {
                $scope.created = data.products;
                //error message
            }).error(function (data, status, headers, config) {
                console.log("No data found.." + status);
                $scope.created = null;
            });
            //------EMBROIDED_PRODUCT_DATA------/
            $http.get($scope.serverstring + 'api/v1.0/products/getusableproducts', {}).success(function (data, status, headers, config) {
                $scope.products = data.products;
                //error message
            }).error(function (data, status, headers, config) {
                console.log("No data found.." + status);
                $scope.products = null;
            });
        };

        $scope.updatePlainOrg = function () {
            //-------PLAIN_PRODUCT_CREATED_DATA------/
            $http.get($scope.serverstring + 'api/v1.0/organization/getorgplainproducts?orgid=' + $scope.session.organizations[$scope.session.showingorg].organization_id, {}).success(function (data, status, headers, config) {
                $scope.plain_created = data.products;
                //error message
            }).error(function (data, status, headers, config) {
                console.log("No data found.." + status);
                $scope.plain_created = null;
            });
            //------PLAIN_PRODUCT_DATA------/
            $http.get($scope.serverstring + 'api/v1.0/products/getusableplainproducts', {}).success(function (data, status, headers, config) {
                $scope.plain_products = data.products;
                //error message
            }).error(function (data, status, headers, config) {
                console.log("No data found.." + status);
                $scope.plain_products = null;
            });
        };

        $scope.updateEmblemsOrg = function (type, size, options) {
            //--------EMBLEMS_DATA-----/
            $http.get($scope.serverstring + 'api/v1.0/organization/getorgemblems?orgid=' + $scope.session.organizations[$scope.session.showingorg].organization_id, {}).success(function (data, status, headers, config) {
                $scope.emblems = data.emblems;
                $scope.approvedemblems = $filter('filter')($filter('toArray')($scope.emblems), {logo_approved: '1'}, true);
                //---error message
            }).error(function (data, status, headers, config) {
                console.log("No data found.." + status);
                $scope.emblems = null;
            });
        };

        var backdrop = true;
        //-----MODAL------/
        $scope.modalResult = null;
        $scope.showModal = function (type, size, options) {
            var template, controller;
            switch (type) {
                case 'addEmb':
                    template = 'pages/modals/addemb.html';
                    controller = 'ProductAddEmbController';
                    break;
                case 'addPlain':
                    template = 'pages/modals/addplain.html';
                    controller = 'ProductAddPlainController';
                    break;
                case 'reviewEmb':
                    template = 'pages/modals/reviewEmb.html';
                    controller = 'ExistingProductReviewController';
                    break;
                case 'reviewPlain':
                    template = 'pages/modals/reviewPlain.html';
                    controller = 'PlainProductReviewController';
                    break;
                case 'color-confirmation':
                    template = 'pages/modals/color-confirmation.html';
                    controller = 'ColorConfirmationController';
                    break;
                case 'viewEmblem':
                    template = 'pages/modals/viewEmblem.html';
                    controller = 'ViewEmblemController';
                    break;
                case 'addEmblem':
                    backdrop = 'static';
                    template = 'pages/modals/add-emblem.html';
                    controller = 'AddEmblemController';
                    break;
                case 'addHouse':
                    template = 'pages/modals/add-house.html';
                    controller = 'AddHouseController';
                    break;
                case 'viewEmblemInProgress':
                    template = 'pages/modals/viewEmblem.html';
                    controller = 'ViewEmblemInProgressController';
                    backdrop = 'static';
                    break;
                case 'contact':
                    template = 'pages/modals/contact.html';
                    controller = 'ContactController';
                    break;
                case 'deletePlain':
                    template = 'pages/modals/contact.html';
                    controller = 'DeletePlainController';
                    break;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: template,
                controller: controller,
                size: size,
                backdrop: backdrop,
                resolve: {
                    options: function () {
                        return options;
                    },
                    originalscope: function () {
                        return $scope;
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };


        //fetch initial tab
        /*$scope.updateSelectedOrg();
        $scope.updateEmblemsOrg();*/
        $scope.refreshSession();

        var getVar = $route.current.params;
        // handle loading straight to a specific tab

         switch(getVar.tab){
            case 'houses' :
                $scope.active = 4;
                break;
        }

    });

    //---------------------------------------------------------------------//
    //--------------------ExistingProductReviewController------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('ExistingProductReviewController', function ($scope, $http, $uibModal, $uibModalInstance, $log,$filter, options, originalscope) {

        //-----Initialization-----//
        $scope.emblems = options.emblems;
        $scope.product = options.product;
        $scope.products = Object.keys(options.products).length;

        $scope.activeColor = {};
        $scope.activeColor.embroided = $scope.product.product_decorated;
        $scope.availableEmblems = $filter('filter')($scope.emblems, { logo_approved: 1 });


        $scope.activeColor.emblemID = $scope.product.colour.organization_logo_id;
        //------SET IMAGE/COLOR/EMBLEM-----//
        $scope.selectColor = function (i) {
            $scope.selectedColor = i;
            $scope.backgroundColor = $scope.product.colour.colour_rgb;
            $scope.currentImage = $scope.product.colour.product_colour_image_url;

            //parameters for next modal
            $scope.activeColor.color = $scope.backgroundColor;
            $scope.activeColor.colorName = $scope.product.colour.colour_name;
            $scope.activeColor.colourID = $scope.product.colour.colour_id;
            $scope.activeColor.image = $scope.currentImage;
        };

        //------SET ACTIVE EMBLEM------//
        $scope.selectEmblem = function (i) {
            $scope.selectedEmblem = i;

            //parameter for next modal
            $scope.activeColor.emblem = $scope.emblems[i].logo_url;
            $scope.activeColor.emblemName = $scope.emblems[i].tesco_style_ref;

        };


        //--------MODAL--------//
        $scope.modalResult = null;
        $scope.showModal = function (type, size, options) {
            var template, controller;
            switch (type) {
                case 'color-confirmation':
                    template = 'pages/modals/color-confirmation.html';
                    controller = 'ColorConfirmationController';
                    break;
                case 'deleteEmb':
                    template = 'pages/modals/deleteEmb.html';
                    controller = 'DeleteProdController';
                    break;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: template,
                controller: controller,
                size: size,
                resolve: {
                    options: function () {
                        return options;
                    },
                    colorParameters: function () {
                        return $scope.activeColor;
                    },
                    originalscope: function () {
                        return originalscope;
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        //-----INIT COLOR/EMBLEM------/
        $scope.selectColor(0);
        $scope.selectColor($scope.colorChosen);
        $scope.selectEmblem(0);
    });

    //---------------------------------------------------------------------//
    //--------------------ProductAddEmbController--------------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('ProductAddEmbController', function ($scope, $http, $uibModal, $uibModalInstance,$timeout, $log, options, originalscope, $filter) {


        //-----Initialization-----//
        $scope.colorChosen = options.color;
        $scope.emblems = options.emblems;
        $scope.product = options.product;
        $scope.product.emblemSizeSmall = $scope.product.product_emblem_small;
        $scope.productName = $scope.product.product_name;


        //created products array[id][colours]
        $scope.createdProducts = [];
        angular.forEach(originalscope.created, function (options, key) {
            if (!$scope.createdProducts[options.product_id]) {
                $scope.createdProducts[options.product_id] = [];
            }
            $scope.createdProducts[options.product_id].push(options.colour.colour_id);
        });

        //filter to check whether product was already created - make new availableColours variable to pick from
        $scope.availableColours = $filter('colorExists')($scope.product.colours, $scope.createdProducts[$scope.product.product_id]);
        $scope.availableEmblems = $filter('filter')($scope.emblems, { logo_approved: 1 });

        //parameters for confirmation modal
        $scope.activeColor = {};
        $scope.activeColor.embroided = $scope.product.product_decorated;
        $scope.testColour = 'background-color:rgb(' + $scope.product.colours[0].colour_rgb + ')';

        //------SET IMAGE/COLOR/EMBLEM-----//
        $scope.selectColor = function (i) {
            $scope.selectedColor = i;
            $scope.backgroundColor = $scope.availableColours[i].colour_rgb;
            $scope.currentImage = $scope.availableColours[i].product_colour_image_url;
            //parameters for next modal
            $scope.activeColor.colourID = $scope.availableColours[i].colour_id;
            $scope.activeColor.pColourID = $scope.availableColours[i].product_colour_id;
            $scope.activeColor.color = $scope.backgroundColor;
            $scope.activeColor.image = $scope.currentImage;
        };


        //------SET ACTIVE EMBLEM------//
        /* Select emblems by $index, select emblems to request
         using it's id to not override with each other */
        $scope.selectedEmblemObject = null;
        $scope.selectedEmblem = null;
        $scope.selectedEmblemToRequest = null;

        //reset functions
        var resetSelectedEmblem = function(){
            $scope.selectedEmblemObject = null;
            $scope.selectedEmblem = null;
        };
        var resetSelectedEmblemToRequest = function(){
            $scope.selectedEmblemToRequestID = null;
            $scope.selectedEmblemObject = null;
        };
        $scope.selectedEmblemSmall = true;
        $scope.selectEmblem = function (i) {
            $scope.selectedEmblemObject = null;
            $scope.selectedEmblemSmall = false;
            resetSelectedEmblemToRequest();
            $scope.selectedEmblem = i;
            $scope.selectedEmblemObject = $scope.availableEmblems[i];
            //parameter for next modal
            $scope.activeColor.emblem = $scope.emblems[i].logo_url;
            $scope.activeColor.emblemID = $scope.emblems[i].organization_logo_id;
        };

        /* Select too big emblems(hats) by id */
        $scope.selectEmblemToRequest = function(id, object){
            $scope.selectedEmblemObject = null;
            $scope.selectedEmblemSmall = true;
            resetSelectedEmblem();
            $scope.selectedEmblemObject = object;
            $scope.selectedEmblemToRequestID = id;
        };


        //---- SMALL PRODUCTS ----//
        /** filter emblems - check if product is small -> allow emblems of maxSize only
        allows only emblems of 60mm for small products */
        var smallProductEmblemMaxSize = 61;
        if ($scope.product.emblemSizeSmall == '1') {
            $scope.allEmblems = angular.copy($scope.availableEmblems);
            $scope.emblemsToRequest = [];
            $scope.availableEmblems = $filter('smallLogo')($scope.availableEmblems, {maxSize: smallProductEmblemMaxSize});

            $scope.availableEmblemsID = [];
            if($scope.availableEmblems.length >= 0){

                var requestEmblemsIndex = 0; //temp var to keep track of pushed emblemsToRequest
                //grab IDs of small logos
                for(var i = 0; i < $scope.availableEmblems.length; i ++ ){
                    $scope.availableEmblemsID.push($scope.availableEmblems[i]['organization_logo_id']);
                }

                //loop through all logos to find big ones
                for (var j = 0; j < $scope.allEmblems.length; j++) {
                    // found small logo? -> dont do anything
                    if (($scope.availableEmblemsID.indexOf($scope.allEmblems[j]['organization_logo_id'])) > -1) {
                    }
                    else {
                        //not a small logo? push to emblemsTooBig
                        if (($scope.emblemsToRequest.indexOf($scope.allEmblems[j]['organization_logo_id'])) <= 0) {
                            $scope.emblemsToRequest.push($scope.allEmblems[j]);
                            $scope.emblemsToRequest[requestEmblemsIndex]['logo_to_request'] = true;
                            requestEmblemsIndex++;
                        }
                    }
                }
                //Pick first logo out of multiple ones
                $timeout(function(){ $scope.selectEmblemToRequest($scope.emblemsToRequest[0]['organization_logo_id'], $scope.emblemsToRequest[0]); })
            }
            //there's only one logo -> dont loop, assign it and choose it on init
            else {
               $timeout(function(){
                   $scope.showOtherEmblems = true;
                   $scope.emblemsToRequest = $scope.allEmblems;
                   $scope.emblemsToRequest[0]['logo_to_request'] = true;
                   $scope.selectEmblemToRequest($scope.emblemsToRequest[0]['organization_logo_id'], $scope.emblemsToRequest[0]);
               });
            }

            //switch to normal emblems on hide Request Emblems
            $scope.switchEmblemOnHide = function(){
                if(!$scope.showOtherEmblems && $scope.availableEmblems.length > 0){
                    if($scope.selectedEmblemObject['logo_to_request'] === true)
                    {
                        $scope.selectEmblem(0);
                    }
                }
            }
        }


        //-------MODAL------/
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.modalResult = null;
        $scope.showModal = function (type, size, options) {
            var template, controller;
            switch (type) {
                case 'color-confirmation':
                    template = 'pages/modals/color-confirmation.html';
                    controller = 'ColorConfirmationController';
                    break;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: template,
                controller: controller,
                size: size,
                resolve: {
                    options: function () {
                        return options;
                    },
                    colorParameters: function () {
                        return $scope.activeColor;
                    },
                    originalscope: function () {
                        return originalscope;
                    },
                    selectedEmblem: function(){
                        return $scope.selectedEmblemObject;
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };


        if($scope.availableColours.length < 1)
        {
            $timeout(function () {
                $scope.messages = [
                    {
                        type: 'danger',
                        message: 'No more colours available for this product.'
                    }
                ];
                originalscope.messages = $scope.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
                $uibModalInstance.dismiss('cancel');
            });
        }
        else {
            //-----INIT COLOR/EMBLEM------/
            $scope.selectColor(0);
            $scope.selectColor($scope.colorChosen);
            $scope.selectEmblem(0);
        }

    });

    //---------------------------------------------------------------------//
    //--------------------PlainProductReviewController---------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('PlainProductReviewController', function ($scope, $http, $uibModal, $uibModalInstance, $log, options,originalscope) {

        //-----Initialization-----//

        $scope.emblems = options.emblems;
        $scope.product = options.product;

        //parameters for confirmation modal
        $scope.activeColor = {};
        $scope.activeColor.embroided = $scope.product.product_decorated;

        //------SET IMAGE/COLOR/EMBLEM-----//
        $scope.selectColor = function (i) {
            $scope.selectedColor = i;
            $scope.backgroundColor = $scope.product.colour.colour_rgb;
            $scope.currentImage = $scope.product.colour.product_colour_image_url;
            //parameters for next modal
            $scope.activeColor.colourID = $scope.product.colour.colour_id;
            $scope.activeColor.color = $scope.backgroundColor;
            $scope.activeColor.image = $scope.currentImage;

        };

        //------SET ACTIVE EMBLEM------/
        $scope.selectEmblem = function (i) {
            $scope.selectedEmblem = i;
            //parameter for next modal
            $scope.activeColor.emblem = $scope.emblems[i].logo_url;
        };

        //-------MODAL------/
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.modalResult = null;
        $scope.showModal = function (type, size, options) {
            var template, controller;
            switch (type) {
                case 'color-confirmation':
                    template = 'pages/modals/color-confirmation.html';
                    controller = 'ColorConfirmationController';
                    break;
                case 'deletePlain':
                    template = 'pages/modals/deletePlain.html';
                    controller = 'DeleteProdController';
                    break;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: template,
                controller: controller,
                size: size,
                resolve: {
                    options: function () {
                        return options;
                    },
                    colorParameters: function () {
                        return $scope.activeColor;
                    },
                    originalscope: function () {
                        return originalscope;
                    },
                    selectedEmblem: function(){
                        return $scope.selectedEmblemObject;
                    }
                }
            });

            modalInstance.result.then(function(data) {

            }, function () {
               // $log.info('Modal dismissed at: ' + new Date());
            });

        };

        //-----INIT COLOR/EMBLEM------/
        $scope.selectColor(0);
        $scope.selectColor($scope.colorChosen);
        $scope.selectEmblem(0);

    });

    //---------------------------------------------------------------------//
    //--------------------ProductAddPlainController------------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('ProductAddPlainController', function ($scope, $http, $uibModal, $uibModalInstance, $log,$filter,$timeout, options, originalscope) {

        //-----Initialization-----//
        $scope.colorChosen = options.color;
        $scope.emblems = options.emblems;
        $scope.product = options.product;

        $scope.productName = $scope.product.product_name;


        //created products array[id][colours]
        $scope.createdProducts = [];
        angular.forEach(originalscope.plain_created, function (options, key) {
            if (!$scope.createdProducts[options.product_id]) {
                $scope.createdProducts[options.product_id] = [];
            }
            $scope.createdProducts[options.product_id].push(options.colour.colour_id);
        });

        //filter to check whether product was already created - make new availableColours variable to pick from
        $scope.availableColours = $filter('colorExists')($scope.product.colours, $scope.createdProducts[$scope.product.product_id]);
        //console.log($scope.availableColours);



        //parameters for confirmation modal
        $scope.activeColor = {};
        $scope.activeColor.embroided = $scope.product.product_decorated;
        $scope.testColour = 'background-color:rgb(' + $scope.product.colours[0].colour_rgb + ')';

        //------SET IMAGE/COLOR/EMBLEM-----//
        $scope.selectColor = function (i) {
            $scope.selectedColor = i;
            $scope.backgroundColor =  $scope.availableColours[i].colour_rgb;
            $scope.currentImage =  $scope.availableColours[i].product_colour_image_url;
            //parameters for next modal
            $scope.activeColor.colourID =  $scope.availableColours[i].colour_id;
            $scope.activeColor.pColourID =  $scope.availableColours[i].product_colour_id;
            $scope.activeColor.color = $scope.backgroundColor;
            $scope.activeColor.image = $scope.currentImage;

        };

        //------SET ACTIVE EMBLEM------/
        $scope.selectEmblem = function (i) {
            $scope.selectedEmblem = i;
            //parameter for next modal
            $scope.activeColor.emblem = $scope.emblems[i].logo_url;
            $scope.activeColor.emblemID = $scope.emblems[i].organization_logo_id;
        };

        //-------MODAL------/
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.modalResult = null;
        $scope.showModal = function (type, size, options) {
            var template, controller;
            switch (type) {
                case 'color-confirmation':
                    template = 'pages/modals/color-confirmation.html';
                    controller = 'ColorConfirmationController';
                    break;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: template,
                controller: controller,
                size: size,
                resolve: {
                    options: function () {
                        return options;
                    },
                    colorParameters: function () {
                        return $scope.activeColor;
                    },
                    originalscope: function () {
                        return originalscope;
                    },
                    selectedEmblem: function(){
                        return $scope.selectedEmblemObject;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        };


        //-----INIT COLOR/EMBLEM------/
        if($scope.availableColours.length < 1)
        {
            $timeout(function () {
                $scope.messages = [
                    {
                        type: 'danger',
                        message: 'No more colours available for this product.'
                    }
                ];
                originalscope.messages = $scope.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
                $uibModalInstance.dismiss('cancel');
            });
        }
        else {
        $scope.selectColor(0);
        $scope.selectColor($scope.colorChosen);
        $scope.selectEmblem(0);
        }

    });

    //---------------------------------------------------------------------//
    //------------------------DeleteProdController-------------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('DeleteProdController', function ($scope, $http, $uibModal, $uibModalStack, $uibModalInstance, colorParameters, options, originalscope, $timeout) {

        $scope.product = options;
        $scope.deleteEmbData = {
            organizationID: $scope.session.organizations[$scope.session.showingorg].organization_id,
            productColourOrgID: $scope.product.urn.colour.products_colours_to_organizations_id
    };

        $scope.deleteProduct = function () {
            $http.post($scope.serverstring + 'api/v1.0/organization/removeproduct', {
                deleteEmbData: $scope.deleteEmbData
            }).success(function (data, status, headers, config) {
                // on success

                originalscope.updateSelectedOrg();
                originalscope.updatePlainOrg();
                originalscope.messages = data.messages;

                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);

                $uibModalStack.dismissAll();

            }).error(function (data, status, headers, config) {
                // log error
                //console.log('Error status: ' + status);
                originalscope.messages = data.messages;

                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);

                $uibModalStack.dismissAll();
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    });

    //---------------------------------------------------------------------//
    //--------------------ColorConfirmationController----------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('ColorConfirmationController', function ($scope, $uibModal, $uibModalInstance, $uibModalStack, $log, $http, colorParameters, options, originalscope, selectedEmblem, $timeout) {

        //---------DATA---------/
        $scope.color = colorParameters;

        $scope.selectedEmblem = {};

        if(selectedEmblem){
            $scope.selectedEmblem = selectedEmblem;
        }

        $scope.product = options;

        $scope.colourAdd = {
            organizationID: $scope.session.organizations[$scope.session.showingorg].organization_id,
            logoID: $scope.selectedEmblem.organization_logo_id,
            colourID: $scope.color.colourID,
            pColourID: $scope.color.pColourID,
            embroided: $scope.color.embroided
        };

        $scope.colourConfirmation = function () {
            $http.post($scope.serverstring + 'api/v1.0/organization/addproduct', {
                colourAdd: $scope.colourAdd
            }).success(function (data, status, headers, config) {
                // on success

                originalscope.updateSelectedOrg();
                originalscope.updatePlainOrg();
                originalscope.messages = data.messages;

                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);

                $uibModalStack.dismissAll();
            }).error(function (data, status, headers, config) {
                // log error
                originalscope.messages = data.messages;

                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);

                $uibModalStack.dismissAll();
            });
        };

        $scope.requestSmallEmblemData = {
            orgid : $scope.selectedEmblem['organization_id'],
            logoType : 'new',
            description : 'Please create a logo suitable for hats',
            attachments : [{
                name : $scope.selectedEmblem['tesco_style_ref'],
                url : $scope.selectedEmblem['logo_url'],
                type: 'emblem'
            }]
        };

        $scope.requestSmallEmblem = function () {

            $http.post($scope.serverstring + 'api/v1.0/organization/addemblemrequest', {
                requestData:  $scope.requestSmallEmblemData
            }).success(function (data, status, headers, config) {
                // on success
                originalscope.updateSelectedOrg();
                originalscope.updatePlainOrg();
                originalscope.messages = data.messages;

                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 6000);

                $uibModalStack.dismissAll();
            }).error(function (data, status, headers, config) {
                // log error
                originalscope.messages = data.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    originalscope.messages = [];
                }, 3000);
                $uibModalStack.dismissAll();
            });
        };


        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });

    //---------------------------------------------------------------------//
    //------------------------ViewEmblemController-------------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('ViewEmblemController', function ($scope, $uibModal, $uibModalInstance, options) {


        //------DATA------/
        $scope.emblem = options.emblem;

        //------MODAL-----/
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    });

    //---------------------------------------------------------------------//
    //------------------------AddEmblemController--------------------------//
    //---------------------------------------------------------------------//
   uesApp.controllerProvider.register('AddEmblemController', function($scope, $uibModal, $uibModalInstance, $uibModalStack, $timeout, $log, options, $http, originalscope, $filter) {
       //------DATA------/
       $scope.emblems = options.emblems;
       $scope.requestData = {};
       $scope.messages = {};
       //width/height pattern
       $scope.onlyNumbers = /([0-9])+(.)/;
       //default logoType value - new emblem
       $scope.requestData.logoType = 'new';
       $scope.loading = 0;

      $scope.logoTypeButton = 'first';
      $scope.logoTypeChosen = null;
       //let user choose type of the logo
      $scope.chooseLogoType = function(choice)
      {
          $scope.requestData.logoType = choice;
      };

       //set flag of logo to reveal the form
       $scope.acceptLogoType = function()
       {
           $scope.logoTypeChosen= true;
       };


      //-----MODAL------/
      $scope.modalResult = null;
      $scope.showModal = function(type, size, options) {
          var template, controller;
          switch (type) {
              case 'addExistingEmblem':
                  template = 'pages/modals/add-existing-emblem.html';
                  controller = 'AddExistingEmblemController';
                  break;
              case 'addEmblemConfirm':
                  template = 'pages/modals/add-emblem-confirm.html';
                  controller = 'AddEmblemConfirmController';
                  break;
          }
          var modalInstance = $uibModal.open({
              animation: true,
              templateUrl: template,
              controller: controller,
              size: size,
              resolve: {
                  options: function() {
                      return options;
                  },
                  originalscope: function() {
                      return $scope;
                  }
              }
          });

          //returns existing logo data
          $scope.chosenLogo = {};
          modalInstance.result.then(function(selectedLogo) {
              $scope.loading = 1;
              $scope.selected = selectedLogo;
              $scope.chosenLogo.backgroundColour = $scope.selected.primary_background_colour;
              $scope.chosenLogo.type = $scope.selected.type;
              $scope.chosenLogo.url = $scope.serverstring + $scope.selected.logo_url;
              $scope.chosenLogo.name = $scope.selected.tesco_style_ref;

              var foundItem = $filter('filter')($scope.allLogos, $scope.chosenLogo, true)[0];

              if (foundItem) {
           $scope.displayError('This emblem is already attached');
              } else {
                  // not found.
                  $scope.loading = 0;
                  $scope.allLogos.push($scope.chosenLogo);
              }
          }, function() {
              $log.info('Modal dismissed at: ' + new Date());
          });
      };
      $scope.cancel = function() {
          $uibModalInstance.dismiss('cancel');
      };


      // new logo init variables
      $scope.allLogos = [];
      $scope.chosenLogo = {};
      $scope.thumbnail = {
          url: 0
      };
      $scope.fileReaderSupported = window.FileReader != null;
      $scope.maxsize = 2000000;

      //error message function
      $scope.displayError = function(message) {

          $scope.loading = 0;
          $timeout(function() {
              // Make sure they see the success message before it closes.
              originalscope.messages.push({
                  type: 'danger',
                  message: message
              });
              $timeout(function() {
                  // Make sure they see the success message before it closes.
                  originalscope.messages = [];
              }, 3000);
          });
          $scope.messages = {};
          originalscope.messages = [];
      };

       function formatSizeUnits(bytes){
           if      (bytes>=1000000000) {bytes=(bytes/1000000000).toFixed(2)+' GB';}
           else if (bytes>=1000000)    {bytes=(bytes/1000000).toFixed(2)+' MB';}
           else if (bytes>=1000)       {bytes=(bytes/1000).toFixed(2)+' KB';}
           else if (bytes>1)           {bytes=bytes+' bytes';}
           else if (bytes==1)          {bytes=bytes+' byte';}
           else                        {bytes='0 byte';}
           return bytes;
       }

       $scope.filePattern = new RegExp("(bat|exe|cmd|sh|php|pl|cgi|386|dll|com|torrent|js|app|jar|pif|vb|vbscript|wsf|asp|cer|csr|jsp|drv|sys|ade|adp|bas|chm|cpl|crt|csh|fxp|hlp|hta|inf|ins|isp|jse|htaccess|htpasswd|ksh|lnk|mdb|mde|mdt|mdw|msc|msi|msp|mst|ops|pcd|prg|reg|scr|sct|shb|shs|url|vbe|vbs|wsc|wsf|wsh)$");
       $scope.photoChanged = function (event) {
           $timeout(function(){
           var files = event.target.files;
           $scope.chosenLogo = {};
           $scope.loading = 1;
           if (files != null) {
               var file = files[0];
               var extension = file.name.split('.').pop().toLowerCase();
               //check file size(2mb)
               if (file.size > $scope.maxsize) {
                   $scope.displayError('Uploaded file size[' + formatSizeUnits(file.size) + '] exceeds maximum file size[' + formatSizeUnits($scope.maxsize) + ']');
                   file = null;
               } else {
                   //check for file extension
                   if ($scope.filePattern.test(extension)) {
                       $scope.displayError('Uploaded file has wrong extension.');
                   }
                   else {
                       if ($scope.fileReaderSupported) {
                           $timeout(function () {
                               var fileReader = new FileReader();
                               fileReader.readAsDataURL(file);
                               fileReader.onload = function (e) {
                                   $scope.thumbnail.url = e.target.result;
                                   $scope.filetype = file.type;
                                   file.url = e.target.result;
                                   $scope.chosenLogo.isImage = file.type.indexOf('image') > -1;
                                   $scope.chosenLogo.url = e.target.result;
                                   $scope.chosenLogo.name = file.name;
                                   $scope.chosenLogo.type = file.type;
                                   var foundItem = $filter('filter')($scope.allLogos, $scope.chosenLogo, true)[0];

                                   //file already added?
                                   if (foundItem) {
                                       $scope.displayError('This file is already attached');
                                   }
                                   else {
                                       // file not found -> send file
                                       $timeout(function () {
                                           $scope.allLogos.push($scope.chosenLogo);
                                           $('#uploadfile').val(null);
                                           $scope.loading = 0;
                                       });
                                   }
                               }
                           });
                       }
                   }
               }
           }
           });
       };

       //remove emblem from allLogos
      $scope.removeEmblem = function(i) {
          $scope.loading = 1;
          $timeout(function() {
              $scope.allLogos.splice(i, 1);
          });
          $scope.loading = 0;
          return $scope.allLogos;
      };

      //send request to API -> show confirm modal first
      $scope.sendRequest = function(i) {
          var modalInstanceShow = $uibModal.open({
              animation: true,
              templateUrl: 'pages/modals/add-emblem-confirm.html',
              controller: 'AddEmblemConfirmController',
              size: 'md'
          });
          //returns existing logo data
          $scope.chosenLogo = {};
          modalInstanceShow.result.then(function(addEmblem) {
              if (addEmblem) {
                  $scope.loading = 1;
                  $scope.requestData.attachments = $scope.allLogos;
                  $scope.requestData.orgid = $scope.session.organizations[$scope.session.showingorg].organization_id;

                  $http.post($scope.serverstring + 'api/v1.0/organization/addemblemrequest', {
                      requestData: $scope.requestData
                  }).success(function(data, status, headers, config) {
                      // on success
                      $scope.loading = 0;
                      $timeout(function() {
                          originalscope.updateEmblemsOrg();
                          originalscope.messages = data.messages;
                          $timeout(function () {
                              // Make sure they see the success message before it closes.
                              originalscope.messages = [];
                          }, 3000);
                      });

                      $uibModalStack.dismissAll();
                  }).error(function(data, status, headers, config) {
                      // log error
                      $scope.loading = 0;
                      $timeout(function() {
                      originalscope.messages = data.messages;
                      $timeout(function() {
                          // Make sure they see the success message before it closes.
                          originalscope.messages = [];
                      }, 3000);
                      });
                  });
              }
          }, function() {
              $log.info('Modal dismissed at: ' + new Date());
          });
      };
  });

    //---------------------------------------------------------------------//
    //-----------------------AddExistingEmblemController-------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('AddExistingEmblemController', function ($scope, $uibModal, $uibModalInstance,$timeout, $log,options) {

        //------DATA------/
        $scope.emblems = options.emblems;
        $scope.selectedLogo = {};

        $scope.selectEmblem = function (i) {
            $scope.selectedEmblem = i;
            //parameter for next modal
            $scope.selectedLogo = i;
        };

        //init with first emblem selected
        $scope.selectEmblem($scope.emblems[0]);

        //save logo
        $scope.saveSelectedLogo = function()
        {
            //save logo data on modal close
            $scope.selectedLogo.type = 'emblem';
            $uibModalInstance.close($scope.selectedLogo);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    });

    //---------------------------------------------------------------------//
    //-----------------------AddEmblemConfirmController--------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('AddEmblemConfirmController', function ($scope, $http, $uibModal, $uibModalInstance) {

        $scope.addEmblem = false;

        $scope.ok = function(){
            $scope.addEmblem = true;
            $uibModalInstance.close($scope.addEmblem);
        };

        $scope.cancel = function () {
            $scope.addEmblem = false;
            $uibModalInstance.dismiss('cancel');
        };

    });

    //---------------------------------------------------------------------//
    //---------------------ViewEmblemInProgressController------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('ViewEmblemInProgressController', function ($scope, $http, $uibModal, $uibModalInstance, $uibModalStack, options,originalscope,$timeout) {

        //--------------DATA---------------//
        $scope.emblem = options.emblem;
        $scope.addEmblem = false;
        $scope.loading = 0;


        //----------footer buttons----------//
        $scope.approveButton = ($scope.emblem.needs_ebos_update == '0' && $scope.emblem.needs_to_update_ebos == '0' && $scope.emblem.logo_digitized == '1' && $scope.emblem.logo_approved == '0');
        $scope.rejectButton = ($scope.emblem.needs_ebos_update == '0' && $scope.emblem.needs_to_update_ebos == '0' && $scope.emblem.logo_digitized == '1' && $scope.emblem.logo_approved == '0');
        $scope.sampleButton = ($scope.emblem.needs_ebos_update == '0' &&
                                $scope.emblem.needs_to_update_ebos == '0' &&
                                $scope.emblem.logo_digitized == '1' &&
                                $scope.emblem.logo_sampled == '0' &&
                                $scope.emblem.logo_approved == '0');

        $scope.sendForm = function(endpoint,data)
        {
            //disable buttons until everything is sent
            $scope.loading = 1;
            $http.post($scope.serverstring + 'api/v1.0/organization/' + endpoint, {
                emblem: $scope.emblem,
                description : data
            }).success(function (data, status, headers, config) {
                $scope.loading = 0;
                if (angular.isObject(data)) {
                    $scope.response = data;
                    $scope.result = data.data;
                    $timeout(function() {
                        originalscope.messages = data.messages;
                        originalscope.updateEmblemsOrg();
                        $timeout(function () {
                            // Make sure they see the success message before it closes.
                            originalscope.messages = [];
                        }, 3000);
                        $uibModalStack.dismissAll();
                    });
                }
            }).error(function (data, status, headers, config) {
                // log error
                $scope.loading = 0;
                $timeout(function() {
                    originalscope.messages = data.messages;
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        originalscope.messages = [];
                    }, 3000);
                });
            });
        };

        $scope.cancel = function () {
            $scope.addEmblem = false;
            $uibModalInstance.dismiss('cancel');
        };

    });

    //---------------------------------------------------------------------//
    //------------------------ContactController----------------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('ContactController', function ($scope, $http, $uibModal, $uibModalInstance) {

        //-----MODAL------/
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.message = '';

        $scope.sendForm = function () {
            $http.post($scope.serverstring + 'api/v1.0/test/posttome', {
                formdata: $scope.message
            }).success(function (data, status, headers, config) {
                // message about email.  Modal?
                if (angular.isObject(data)) {
                    $scope.response = data;
                    $scope.result = data.data;
                    $scope.messages = data.messages;

                    var template, controller;

                    template = 'pages/modals/registered.html';
                    controller = 'registerMessageController';

                    $scope.selected = null;

                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: template,
                        controller: controller,
                        size: 'lg',
                        resolve: {
                            param: function () {
                                return {response: $scope.response};
                            }
                        }
                    });
                    modalInstance.result.then(function (selectedItem) {
                        // $scope.selected = selectedItem;
                    }, function () {
                        // Dismissed.
                    });
                }
            }).error(function (data, status, headers, config) {
                // log error
            });

        };


    });

    //---------------------------------------------------------------------//
    //------------------------AddHouseController----------------------------//
    //---------------------------------------------------------------------//
    uesApp.controllerProvider.register('AddHouseController', function ($scope, $http, $rootScope, $uibModal, $uibModalInstance, $uibModalStack, options, originalscope, $timeout) {

        $scope.houses = options.houses;
        $scope.created = options.created;
        $scope.plain_created = options.plain_created;
        $scope.formData = {
            orgId: $scope.session.showingorg
        };

        $scope.form = {};

        //-----MODAL------/
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.sendForm = function () {
            $http.post($scope.serverstring + 'api/v1.0/organization/addnewhouse', {
                requestData: $scope.formData
            }).success(function (data, status, headers, config) {
                // message about email.  Modal?
                if (angular.isObject(data)) {
                    $timeout(function() {
                        $rootScope.session = data.session;
                        $scope.messages = data.messages;
                        $uibModalStack.dismissAll();
                        originalscope.refreshSession();
                        $timeout(function(){
                            $scope.messages = [];
                        },3000);
                    });
                }
            }).error(function (data, status, headers, config) {
                // log error
                $timeout(function() {
                    $scope.messages = data.messages;
                    $timeout(function(){
                        $scope.messages = [];
                    },3000);
                });
            });

        };


    });

    //--------------------------------END----------------------------------//
}());