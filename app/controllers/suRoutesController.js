/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('SuRoutesController', function ($scope,$http,$uibModal,$log) {
    $scope.adminusers = {};
    $scope.newuser = {};

    $scope.search = {};

    $scope.getAdminUsers = function(){
        $http.get($scope.serverstring+'api/v1.0/admin/getadminusers').
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.adminusers = data.data;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.adminusers = {};
            });
    };


    $scope.$watch('search.usertype', function (newValue) {
        if(newValue = ''){
           delete $scope.search.usertype;
        }
    }, true);

    $scope.createAdminUser = function() {
        $http.post($scope.serverstring+'api/v1.0/admin/createuser',
            {
                userdata:  $scope.newuser
            }
        ).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    // done.
                    $scope.newuser = {}; // empty the form
                    $scope.userform.$setPristine();
                    $scope.getAdminUsers();
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

});
