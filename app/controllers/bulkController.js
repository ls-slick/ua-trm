/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('BulkController', function ($scope,$http,$uibModal,$timeout) {

    $scope.test = {};
    $scope.bulks = {
        page: 1,
        total: 0
    };

    $scope.loadBulks = function () {
        $http.post($scope.serverstring+'api/v1.0/bulk/getbulklist', {
            "q": $scope.bulks.query,
            "p": $scope.bulks.page
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.bulks.orders = data.orders;
                    $scope.bulks.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

    $scope.loadBulks();

    /* Pagination */
    $scope.bulkPageChanged = function(currentPage) {
        //$scope.apage = currentPage;
        //console.log('Page changed to: ' + currentPage);
        $scope.loadBulks();
    };

    $scope.$watch('bulks.query', function (newValue) {
        $scope.bulks.page = 1;
        $scope.loadBulks();
    }, true);

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.modalResult = null;
    $scope.showModal = function (type, size, options) {
        var template, controller;
        switch (type) {
            case 'bulkEdit':
                template = 'pages/modals/admin-bulk-modal.html';
                controller = 'BulkEditController';
                break;
        }

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: template,
            controller: controller,
            size: size,
            //backdrop: 'static',
            resolve: {
                options: function () {
                    return options;
                },
                originalscope: function () {
                    return $scope;
                }
            }
        });

        modalInstance.result.then(function (data) {

           $timeout(function () {
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    $scope.messages = [];
                }, 3000);
               // alert(data);
                $scope.messages = data;

                $scope.loadBulks();

            });

        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });

    };

});

//---------------------------------------------------------------------//
//-------------------------BulkEditController--------------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('BulkEditController', function ($scope, $http, $location, $route, $uibModal, $uibModalInstance, options, $timeout, $rootScope, $filter) {
    //set modal to choose proper content
    $scope.isBulkEditModal = true;
    $scope.oneHourOld = false;
    $scope.bulkEditForm = {};
    //variables
    $scope.bulkorderid = options.boid;

    $scope.bulkForm = {};

    $scope.bulkForm.bulkID = $scope.bulkorderid;

    // New Item object.
    $scope.newproducts = [];
    $scope.colours = [];
    $scope.sizes = [];

    $scope.newproductData = {
        productid: null,
        colour: null,
        colourRGB: null,
        size: null,
        quantity: 1,
        logo: null,
        added: null
    };

    $scope.selectedColourStyle = null;
    $scope.selectedColourImage = null;
    $scope.selectedEAN = null;
    $scope.selectedSizeQty = null;
    $scope.emblemdetail = null;
    $scope.usableEmblems = false;

    $scope.updateBulk = function () {
        $http.post($scope.serverstring + 'api/v1.0/bulk/updatebulk', {
            bulkData: $scope.bulkForm.bulkData
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    //things to update
                    $uibModalInstance.close(data.messages);
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $uibModalInstance.close(data.messages);
            });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.test = function () {
        $uibModalInstance.dismiss();
    };

    $scope.getOrgEmblems = function (schoolURN) {

        $http.get($scope.serverstring + 'api/v1.0/admin/getorgemblems?urn=' + schoolURN).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {

                    $scope.emblemdetail = data.data.emblems;
                    $scope.emblemsApproved = $filter('filter')($scope.emblemdetail, { logo_approved: 1 });
                    if ($scope.emblemsApproved) {
                        $scope.usableEmblems = $scope.emblemsApproved;
                    }
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.emblemdetail = {};
                $scope.emblemsApproved = {};
            });
    };

    $scope.viewBulk = function () {
        $rootScope.loading = true;
        $http.post($scope.serverstring+'api/v1.0/bulk/getbulkorder', {
            "bid": $scope.bulkorderid
        }).
            success(function (data, status, headers, config) {
                $scope.bulkForm.bulkData = data;

                if($scope.bulkForm.bulkData.header.organization_bulk_order_gmo_sent_datetime){
                    var lastSent = new Date($scope.bulkForm.bulkData.header.organization_bulk_order_gmo_sent_datetime);
                    var diffMs = (new Date() - lastSent);
                    var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours

                    if(diffHrs >= 1){
                        $scope.oneHourOld = true;
                    }
                } else {
                    $scope.oneHourOld = true;
                }

                //$timeout(function() {
                    //alert($scope.bulkForm.bulkData.header.school_URN);
                    $scope.getOrgEmblems($scope.bulkForm.bulkData.header.school_URN);
               // });

                $scope.bulkForm.bulkData.removed = [];
                $scope.bulkForm.bulkData.added = [];
                $rootScope.loading = false;
                angular.forEach($scope.bulkEditForm.$error.required, function(field) {
                    $timeout(function() {
                        field.$setDirty();
                    });
                });
            }).
            error(function (data, status, headers, config) {
                // log error
                // set error message.
                $timeout(function() {
                    $scope.messages = data.messages;
                    $timeout(function(){
                        $scope.messages = [];
                    },3000);
                });
                $rootScope.loading = false;
            });
    }();

    $scope.fetchProducts = function () {
        $http.get($scope.serverstring + 'api/v1.0/admin/getembstyles').
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.newproducts = data.data;
                    // Remove small products if emblem size is too big
                    if (parseFloat($scope.emblem['size__mm_'].split('x')[1]) >= EMBLEM_MAX_SIZE) {
                        $scope.emblemTooBig = true;
                        for (var product in $scope.newproducts) {
                            if ($scope.newproducts.hasOwnProperty(product)) {
                                if ($scope.newproducts[product]['product_emblem_small'] == 1) {
                                    delete $scope.newproducts[product]; // leaves null values..
                                }
                            }
                        }
                    }
                }

                // Remove null values to show properly
                $scope.products = $scope.products.filter(function (product) {
                    return $scope.products[product] !== null;
                });

            }).error(function (data, status, headers, config) {
                // log error
                $scope.products = {};
            });
    }();

    $scope.getProductSizes = function (productColorID) {
        $scope.sizes = [];
        $http.post('api/v1.0/admin/getproductsizes', {
            product_colour_id: productColorID
        }).
            success(function (data, status, headers, config) {
                $scope.sizes = data;
            }).
            error(function (data, status, headers, config) {
                // log error
                return null;
            });
    };


    $scope.getProductColours = function (productID, object) {
        $http.get($scope.serverstring + 'api/v1.0/admin/getproductcolours?pid=' + productID).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope[object] = data.data;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                return null;
            });
    };

    //maximum emblem size for small products
    var smallProductEmblemMaxSize = 61;
    $scope.$watch('newproductData.productid', function (newVal, oldVal) {
        if (newVal !== undefined) {

            $scope.selectedColourStyle = '';
            $scope.selectedEAN = '';
            $scope.selectedGMOStyle = '';
            $scope.selectedColourImage = '';
            $scope.newproductData.colour = '';
            $scope.selectedLogoImage = '';
            $scope.newproductData.logo = '';
            $scope.selectedSizeQty = null;

            //pick selected product object from list
            var selectedproduct = $filter('filter')($scope.newproducts, { product_id: newVal }, true)[0];

            //create emblems array based on a chosen product
            $scope.usableEmblems = $scope.emblemsApproved;

            if (selectedproduct['product_emblem_small'] == '1') {
                $scope.usableEmblems = $filter('smallLogo')($scope.usableEmblems, { maxSize: 61 });
            }

            $scope.getProductColours(newVal, 'newcolours');

        }
    });

    $scope.$watch('newproductData.colour', function (newVal, oldVal) {
        if (!$scope.newproductData.colour) {
            $scope.newproductData.size = '';
            $scope.sizes = [];
        }
        if (newVal !== undefined && newVal !== '') {

            // set new colour values
            for (var colour in $scope.newcolours) {
                if ($scope.newproductData.colour == $scope.newcolours[colour]['product_colour_id']) {
                    $scope.newproductData.colourRGB = $scope.newcolours[colour]['colour_rgb'];
                }
            }

            // recalculate colours similarities
            /*$scope.emblem.threadsCopy.length = 0;
            for (var thread in $scope.emblem.emblemThreads) {
                if (($scope.emblem.emblemThreads[thread]).length) {
                    $scope.emblem.threadsCopy.push({ title: $scope.emblem.emblemThreads[thread], rgb_diffrence: $scope.isSimilarColour($scope.newproductData.colourRGB, $scope.formElements.threads[$scope.emblem.emblemThreads[thread]].rgb, $scope.tolerance) });
                }
            }*/

            $scope.selectedColourStyle = $scope.newcolours[newVal].product_colour_style_ref;

            $scope.selectedColourImage = $scope.newcolours[newVal].product_colour_image_url;
            $scope.selectedLogoBackground = $scope.newcolours[newVal].colour_rgb;

            $scope.getProductSizes($scope.newproductData.colour);
        }
    });

    $scope.$watch('newproductData.size', function (newVal, oldVal) {
        if (newVal !== undefined && newVal !== '') {
            var foundSize = $filter('filter')($scope.sizes, { product_size_id: newVal }, true)[0];
            $scope.selectedSizeQty = foundSize.product_qty;
            $scope.selectedEAN = foundSize.product_sku;
            $scope.selectedGMOStyle = foundSize.product_gmo_sku;
        }
    });

    $scope.$watch('newproductData.logo', function (newVal, oldVal) {
        if (newVal !== undefined && newVal !== '') {
            var foundItem = $filter('filter')($scope.emblemdetail, { organization_logo_id: newVal }, true)[0];
            $scope.selectedLogoImage = foundItem.logo_url;
        }
    });

    $scope.addItem = function () {


        var foundProduct = $filter('filter')($scope.newproducts, { product_id: $scope.newproductData.productid }, true)[0];
        var foundColour = $scope.newcolours[$scope.newproductData.colour];
        var foundSize = $filter('filter')($scope.sizes, { product_size_id: $scope.newproductData.size }, true)[0];
        var foundLogo = $filter('filter')($scope.emblemdetail, { organization_logo_id: $scope.newproductData.logo }, true)[0];
        var addTime = new Date().getTime();

        // Add as item.
        $scope.newproductData.added = addTime;
        $scope.bulkForm.bulkData.added.push($scope.newproductData);

        $scope.bulkForm.bulkData.lines.push({
            GMOERROR: "ADDED PRODUCT",
            organization_bulk_order_id: $scope.bulkForm.bulkData.header.organization_bulk_order_id,
            organization_bulk_order_item_id: addTime,
            organization_urn: $scope.bulkForm.bulkData.header.school_URN,
            product_colour_image_url: $scope.selectedColourImage,
            product_colour_name: foundColour.colour_name,
            product_colour_style_ref: $scope.selectedColourStyle,
            product_ean: $scope.selectedEAN,
            product_gmo_cat_id: $scope.selectedGMOStyle,
            product_line_price: $scope.newproductData.quantity * foundSize.product_price,
            product_name: foundProduct.product_name,
            product_price: foundSize.product_price,
            product_quantity: $scope.newproductData.quantity,
            product_size_name: foundSize.product_size_name,
            product_variation_stock: foundSize.product_qty,
            products_logo_ref: foundLogo.tesco_style_ref,
            products_sizes_to_colours_id: foundSize.products_sizes_to_colours_id
        });

        $scope.newproductData = {
            productid: null,
            colour: null,
            colourRGB: null,
            size: null,
            quantity: 1,
            logo: null,
            added: null
        };

        $scope.selectedColourStyle = '';
        $scope.selectedEAN = '';
        $scope.selectedGMOStyle = '';
        $scope.selectedColourImage = '';
        $scope.newproductData.colour = '';
        $scope.selectedLogoImage = '';
        $scope.newproductData.logo = '';
        $scope.selectedSizeQty = null;

    };

    $scope.removeItem = function (item) {
        if(confirm('Remove Item?','Yes','No')){
            // was this an added one? if so just remove it from item and added.
            if(item.GMOERROR == 'ADDED PRODUCT'){
                var foundAdded = $filter('filter')($scope.bulkForm.bulkData.added, { added: item.organization_bulk_order_item_id }, true)[0];
                var index = $scope.bulkForm.bulkData.added.indexOf(foundAdded);
                $scope.bulkForm.bulkData.added.splice(index, 1);
            } else {
                $scope.bulkForm.bulkData.removed.push(item);
            }

            // remove from main list.
            var index = $scope.bulkForm.bulkData.lines.indexOf(item);
            $scope.bulkForm.bulkData.lines.splice(index, 1);

            $scope.viewBulk();
        }

    };

});
