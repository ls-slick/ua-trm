/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('SuUsersController', function ($scope,$http,$uibModal,$log,$timeout) {
    $scope.adminusers = {};
    $scope.newuser = {};

    $scope.search = {};

    $scope.getAdminUsers = function(){
        $http.get($scope.serverstring+'api/v1.0/admin/getadminusers').
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.adminusers = data.data;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
                $scope.adminusers = {};
            });
    };


    $scope.$watch('search.usertype', function (newValue) {
        if(newValue = ''){
           delete $scope.search.usertype;
        }
    }, true);

    $scope.createAdminUser = function() {
        $http.post($scope.serverstring+'api/v1.0/admin/createuser',
            {
                userdata:  $scope.newuser
            }
        ).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    // done.
                    $scope.newuser = {}; // empty the form
                    $scope.userform.$setPristine();
                    $scope.getAdminUsers();
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

    $scope.getAdminUsers();

	 $scope.doReset = function(user){
		$http.post($scope.serverstring+'api/v1.0/admin/resetpassword',{
            "user":user
        }).

            success(function(data, status, headers, config) {
                if(angular.isObject(data)){
                    //$scope.response = data;
                    $scope.messages = data.messages;
                    $timeout(function () {
                        // Make sure they see the success message before it closes.
                        $scope.messages = [];
                    }, 3000);
                    //console.log($scope.messages);
                }
            }).
            error(function(data, status, headers, config) {
                // log error
                $scope.messages = data.messages;
                $timeout(function () {
                    // Make sure they see the success message before it closes.
                    $scope.messages = [];
                }, 3000);
            });
    };

    $scope.resetPassword = function(user){
        // modal.
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/resetpass.html',
            controller: 'ResetPasswordController',
            size: 'md',
            resolve: {
                options: function() {
                    return user;
                }
            }
        });

        modalInstance.result.then(function() {
            // success?
            // Clear any error messages.
            $scope.messages = [];
            $scope.doReset(user);
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
            //alert('NO');
        });
    };
	
});

//---------------------------------------------------------------------//
//---------------------------------------------------------------------//
//-----------------------ResetPasswordController-----------------------//
//---------------------------------------------------------------------//
uesApp.controllerProvider.register('ResetPasswordController', function($scope, $uibModal, $uibModalInstance,options) {
    //------DATA------/
    $scope.user = options;
	
    //------MODAL-----/
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.ok = function () {
        $uibModalInstance.close(true);
    };
});


   
