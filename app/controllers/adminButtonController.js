/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('AdminButtonController', function($scope, $rootScope, $uibModal, $http, $timeout, $location, $filter, $route) {

    $scope.alerts = [];
    $scope.actions = [];

    // Quick note button
    $scope.quickNoteBtn = false;
    toggleQuickNoteBtn();
    $scope.$on('$routeChangeStart', function(next, current) {
        toggleQuickNoteBtn();
    });

    $scope.showModal = function(type, size, options) {
        var template, controller;
        switch (type) {
            case 'adminSearch':
                template = 'pages/modals/admin-search.html';
                controller = 'AdminSearchInstanceController';
                break;
            case 'adminTasks':
                template = 'pages/modals/admin-tasks.html';
                controller = 'AdminTasksController';
                break;
            case 'adminActions':
                template = 'pages/modals/admin-actions.html';
                controller = 'AdminActionsController';
                break;
            case 'adminTasksEdit':
                template = 'pages/modals/admin-tasks-edit.html';
                // controller = 'AdminTasksEditController';
                break;
            case 'adminQuickNote':
                template = 'pages/modals/admin-quick-note.html';
                controller = 'AdminQuickNoteController';
                break;
        }

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: template,
            controller: controller,
            size: size,
            resolve: {
                options: function() {
                    return options;
                },
                originalscope: function() {
                    return $scope;
                }
            }
        });

        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem;
        }, function() {

        });
    };


    $scope.getUserAlerts = function() {
        //  var orgid = 5432;
        $http.get($scope.serverstring + 'api/v1.0/admin/getuseralerts').
        success(function(data, status, headers, config) {
            if (angular.isObject(data)) {
                $scope.alerts = $filter('filter')(data.data, { ticket_action: "0" }, true);
                $scope.actions = $filter('filter')(data.data, { ticket_action: "1" }, true);
            }
        }).
        error(function(data, status, headers, config) {
            // log error
            $scope.alerts = [];
        });
        $timeout(function() {
            $scope.getUserAlerts();
        }, 30000); // reload each 30 seconds.
    };
    $scope.getUserAlerts();

    function toggleQuickNoteBtn() {
        var organization_path = "/orginfo/";
        var currentPath = $location.path(); //                  ->  /orginfo/29384
        var currentPathOnly = currentPath.substring(0, 9); //   ->  /orginfo/
        if (currentPathOnly === organization_path && $route.current.params) {
            $scope.quickNoteBtn = true;
        } else {
            $scope.quickNoteBtn = false;
        }
    }

});

uesApp.controllerProvider.register('AdminSearchInstanceController', function($scope, $http, $rootScope, $uibModalInstance, options) {

    //initialize starting value of search to null
    $scope.uibModalInstance = $uibModalInstance; // Allows usage from child scopes.

    $scope.options = options;
    $scope.searchString = null;
    $scope.quantity = 7;

});

uesApp.controllerProvider.register('AdminTasksController', function($scope, $http, $route, $rootScope, $uibModalInstance, options, $location) {

    //initialize starting value of search to null
    $scope.uibModalInstance = $uibModalInstance; // Allows usage from child scopes.

    var currentUrl = $location.absUrl();
    $scope.alerts = options.alerts;

    //go to note URL, close modal if note site is valid already.
    $scope.viewOrg = function(orgid) {
        if (orgid) {
            if (currentUrl == $location.absUrl('/orginfo/' + orgid + '/notes')) {
                $scope.cancel();
            }
            $location.path('/orginfo/' + orgid + '/notes');
        }
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

});

uesApp.controllerProvider.register('AdminActionsController', function($scope, $http, $route, $rootScope, $uibModal, originalscope, $uibModalInstance, options, $timeout, $location, $log, $filter, getOrgNotesFactory) {

    //initialize starting value of search to null
    $scope.uibModalInstance = $uibModalInstance; // Allows usage from child scopes.

    var currentUrl = $location.absUrl();
    $scope.actions = options.actions;

    //go to note URL, close modal if note site is valid already.
    $scope.viewOrg = function(orgid) {
        if (orgid) {
            if (currentUrl == $location.absUrl('/orginfo/' + orgid + '/notes')) {
                $scope.cancel();
            }
            $location.path('/orginfo/' + orgid + '/notes');
        }
    };


    // Delete task -> show confirmation + reload tasks here + at background
    $scope.deleteTask = function(task) {
        var modalInstanceShow = $uibModal.open({
            animation: true,
            templateUrl: 'pages/modals/confirm.html',
            controller: 'AdminDeleteTaskController',
            size: 'md'
        });
        modalInstanceShow.result.then(function(removeTask) {
                if (removeTask) {
                    $http.post($scope.serverstring + 'api/v1.0/admin/deletenote', {
                        ticket_id: task.ticket_id
                    }).success(function(data, status, headers, config) {
                        // on success
                        $scope.loading = 0;
                        $http.get($scope.serverstring + 'api/v1.0/admin/getuseralerts').
                        success(function(data, status, headers, config) {
                            if (angular.isObject(data)) {
                                // Delete on modal
                                $scope.alerts = $filter('filter')(data.data, { ticket_action: "0" }, true);
                                $scope.actions = $filter('filter')(data.data, { ticket_action: "1" }, true);
                                // Delete in the background
                                originalscope.alerts = $filter('filter')(data.data, { ticket_action: "0" }, true);
                                originalscope.actions = $filter('filter')(data.data, { ticket_action: "1" }, true);
                            }
                        }).
                        error(function(data, status, headers, config) {
                            // log error
                            originalscope.alerts = [];
                            $scope.alerts = [];
                        });
                    }).error(function(data, status, headers, config) {
                        // log error
                        $scope.loading = 0;
                    });
                }
            },
            function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

});


uesApp.controllerProvider.register('AdminDeleteTaskController', function($scope, $http, $route, $rootScope, $uibModalInstance, $sce, $location) {

    $scope.question = {};

    $scope.question['title'] = $sce.trustAsHtml('Remove task');
    $scope.question['message'] = $sce.trustAsHtml('<h4 class="text text-danger">Are you sure you want to remove this task?</h4>');

    // Delete
    $scope.ok = function() {
        $scope.removeTask = true;
        $uibModalInstance.close($scope.removeTask);
    };

    // Close modal
    $scope.cancel = function() {
        $scope.removeTask = false;
        $uibModalInstance.dismiss('cancel');
    };

});

uesApp.controllerProvider.register('AdminQuickNoteController', function($scope, $route, $http, options, $route, $rootScope, $uibModalInstance, $sce, $location) {

    var organization_id = $route.current.params.orgid;
    $scope.organization_note = '';

    $scope.getQuickNote = function() {
        $http.post('api/v1.0/admin/getquicknote', {
            organization_id: organization_id
        }).
        success(function(data, status, headers, config) {
            if (angular.isObject(data)) {
                // Delete on modal
                $scope.organization_note = data.quick_note;
            }
        }).
        error(function(data, status, headers, config) {
            // log error
            console.log(data);
        });
    };
    $scope.getQuickNote();

    $scope.saveQuickNote = function() {
        $http.post('api/v1.0/admin/setquicknote', {
            organization_id: organization_id,
            text: $scope.organization_note
        }).
        success(function(data, status, headers, config) {
            $rootScope.$broadcast('updateNote', { note: $scope.organization_note });
        }).
        error(function(data, status, headers, config) {
            // log error
            console.log(data);
        });
    }

    // Accept
    $scope.ok = function() {
        $uibModalInstance.close();
    };

    // Close modal
    $scope.cancel = function() {
        $scope.saveQuickNote();
        $uibModalInstance.dismiss('cancel');
    };

    // Handle debounce on quick notes
    $(function() {
        var typingTimer; //timer identifier
        var doneTypingInterval = 500; //time in ms, 1 seconds default
        var $input = $('#quick-note-comment');

        // updated events 
        $input.on('input propertychange paste', function() {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });

        //user is "finished typing," do something
        function doneTyping() {
            $scope.saveQuickNote();
        }
    });
});