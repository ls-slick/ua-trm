/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('LoginController', function($scope,$interval, $http, $location, $route, $rootScope,routeFactory,cssInjector,scriptLoader){
        $scope.user = null;
        $scope.pass = null;
        $scope.messages = null;

        $scope.doSubmit = function(){
            $http.post($scope.serverstring+'api/v1.0/login/process',{
                "user":$scope.user,
                "pass":$scope.pass
            }).
                success(function(data, status, headers, config) {
                    if(angular.isObject(data)){

                        $scope.messages = data.messages;

                        $rootScope.session = data.data;

                        // apply additional CSS
                        if(data.metrics.hasOwnProperty('css')){
                            cssInjector.add(data.metrics.css);
                        }

                        // apply additional scripts
                        if(data.metrics.hasOwnProperty('script')){
                            scriptLoader.loadScript(data.metrics.script);
                        }

                        if(data.metrics.hasOwnProperty('include')){
                            $rootScope.adminurl = data.metrics.include;
                        }
                        if(data.metrics.hasOwnProperty('includehelp')){
                            $rootScope.helpurl = data.metrics.includehelp;
                        }

                        // ad extra routes.
                        routeFactory.clearRoutes();
                        angular.forEach(data.metrics.user_routes, function(options, key) {
                            routeFactory.addRoute(key,options);
                        });

                        // set default route.
                        $route.routes['null'] = angular.extend({
                            redirectTo: data.metrics.default_route,
                            reloadOnSearch: true,
                            caseInsensitiveMatch: false
                        });

                        // start keep alive timer
                        $interval($rootScope.sessionTimer);
                        // update menu items
                        $rootScope.menuItems = data.metrics.menu_items;

                        $rootScope.updateLists();

                        $location.path($rootScope.session['redirect']);
                    }
                }).
                error(function(data, status, headers, config) {
                    // log error
                    $scope.messages = data.messages;

                    // update menu items
                    $rootScope.menuItems = data.metrics.menu_items;

                });
        };

        $scope.submitForm = function(){
            // Clear any error messages.
            $scope.messages = [];
            $scope.doSubmit();
        };

});
