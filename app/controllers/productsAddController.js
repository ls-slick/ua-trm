/**
 * Created by Kamil on 05/02/2016.
 */
var uesApp = angular.module('uesApp');


uesApp.controllerProvider.register('ProductsAddController', function ($scope,$uibModal,$log) {


    //form variables
    $scope.uniform_name = null;
    $scope.uniform_uniform_group_id = null;
    $scope.uniform_most_popular = null;
    $scope.uniform_price_text = null;
    $scope.uniform_link = null;
    $scope.uniform_description = null;


    //badges states
    $scope.addNewBadge = function(){

    };
    $scope.addSoonBadge = function(){

    };
    $scope.addAwaitingBadge = function(){

    };


    //Modals
    $scope.items = ['item1', 'item2', 'item3'];
    $scope.modalResult = null;
    $scope.showModal = function (type,size,options) {
        var template, controller;
        switch (type) {
            case 'preview':
                template = 'pages/modals/preview.html';
                controller = 'ProductPreviewController';
                break;
            case 'add-color':
                template = 'pages/modals/add-color.html';
                controller = 'AddColorController';
                break;
            case 'search':
                template = 'pages/modals/admin-search.html';
                controller = 'AdminSearchController';
                break;


        }

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: template,
            controller: controller,
            size: size,
            resolve: {
                options:  function () {
                    return options;
                },
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });

    };


    //-----TESTING-------------//

    $scope.name;
    // initialize the array
    $scope.data=[[{"name":"*Colour"},{"color":""}],
        [{"name":"*Style ref"},{"style_ref":""}],
        [{"name":"Alt Style ref"},{"alt_style_ref":""}],
        [{"name":"On UES page"},{"onUS":""}],
        [{"name":"Image"},{"image":""}],
        [{"name":"Schools Using It"},{"schools":""}],
        [{"name":"Size"},{"size":""}],
    ];

    // add a column
    $scope.addColumn = function(){
        //you must cycle all the rows and add a column
        //to each one
        $scope.data.forEach(function($row){
            $row.push({"en":""})
        });
    };

    // remove the selected column
    $scope.removeColumn = function (index) {
        // remove the column specified in index
        // you must cycle all the rows and remove the item
        // row by row
        $scope.data.forEach(function (row) {
            row.splice(index, 1);

            //if no columns left in the row push a blank array
            if (row.length === 0) {
                row.data = [];
            }
        });
    };

    // remove the selected row
    $scope.removeRow = function(index){
        // remove the row specified in index
        $scope.data.splice( index, 1);
        // if no rows left in the array create a blank array
        if ($scope.data.length() === 0){
            $scope.data = [];
        }
    };

    //add a row in the array
    $scope.addRow = function(){
        // create a blank array
        var newrow = [];

        // if array is blank add a standard item
        if ($scope.data.length === 0) {
            newrow = [{'en':''}];
        } else {
            // else cycle thru the first row's columns
            // and add the same number of items

            $scope.data[0].forEach(function (row, key) {
                if(key === 0) {
                    newrow.push({'name': $scope.name});
                }  else {
                    newrow.push({'en':''});
                }
            });
        }
        // add the new row at the end of the array
        $scope.data.push(newrow);
    };
});




//--Preview modal Controller-------------->
uesApp.controllerProvider.register('ProductPreviewController', function ($scope, $uibModal,$uibModalInstance,$log){

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

uesApp.controllerProvider.register('AddColorController', function ($scope, $uibModal,$uibModalInstance,$log){

    $scope.id = 0;
    $scope.addColorData = {
        id: $scope.id,
        color: '',
        style_ref: '',
        alt_style_ref: ''

    };

    console.log('at AddColorController');
    $scope.colour = null;
    $scope.style_ref = null;
    $scope.alt_style_ref = null;

    $scope.save = function(){

        $scope.addColorData.id = $scope.id;
        $scope.addColorData.color = $scope.colour;
        $scope.addColorData.style_ref = $scope.style_ref;
        $scope.addColorData.alt_style_ref = $scope.alt_style_ref;

        console.log('saved!');
        console.log($scope.addColorData);
        $uibModalInstance.dismiss('cancel');


        console.log('after loging $scope color data!');
    };
    $scope.id++;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});





