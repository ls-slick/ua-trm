/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('UschoolsController', function ($scope,$http, $location) {

    $scope.greaterThan = function(prop, val){
        return function(item){
            return item[prop] > val;
        }
    };

    $scope.maxSize = 9;
    $scope.actives = {
        page: 1,
        total: 0
    };
    $scope.lives = {
        page: 1,
        total: 0
    };
    $scope.withdrawns = {
        page: 1,
        total: 0
    };
    $scope.suspendeds = {
        page: 1,
        total: 0
    };

    $scope.updateActive = function(){
        $http.post($scope.serverstring+'api/v1.0/organization/getorglist', {
            "q": $scope.actives.query,
            "t": 'a',
            "p": $scope.actives.page
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.actives.schools = data.organizations;
                    $scope.actives.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

    /* Pagination */
    $scope.activePageChanged = function(currentPage) {
        //$scope.apage = currentPage;
        //console.log('Page changed to: ' + currentPage);
        $scope.updateActive();
    };

    $scope.$watch('actives.query', function (newValue) {
        $scope.actives.page = 1;
        $scope.updateActive();
    }, true);

    $scope.$parent.viewOrg = function (orgid){
        if(orgid){
            $location.path('/orginfo/'+orgid);
        } else {
           // alert(orgid+'invalid')
        }
    };

    $scope.updateLive = function(){
        $http.post($scope.serverstring+'api/v1.0/organization/getorglist', {
            "q": $scope.lives.query,
            "t": 'l',
            "p": $scope.lives.page
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.lives.schools = data.organizations;
                    $scope.lives.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

    /* Pagination */
    $scope.livePageChanged = function(currentPage) {
        //$scope.apage = currentPage;
        $scope.lives.page = currentPage;
        //console.log('Page changed to: ' + currentPage);
        $scope.updateLive();
    };

    $scope.$watch('lives.query', function (newValue) {
        $scope.lives.page = 1;
        $scope.updateLive();
    }, true);

    $scope.updateWithdrawn = function(){
        $http.post($scope.serverstring+'api/v1.0/organization/getorglist', {
            "q": $scope.withdrawns.query,
            "t": 'w',
            "p": $scope.withdrawns.page
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.withdrawns.schools = data.organizations;
                    $scope.withdrawns.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

    /* Pagination */
    $scope.withdrawnsPageChanged = function(currentPage) {
        //$scope.apage = currentPage;
        $scope.withdrawns.page = currentPage;
        //console.log('Page changed to: ' + currentPage);
        $scope.updateWithdrawn();
    };

    $scope.$watch('withdrawns.query', function (newValue) {
        $scope.withdrawns.page = 1;
        $scope.updateWithdrawn();
    }, true);

    $scope.updateSuspended = function(){
        $http.post($scope.serverstring+'api/v1.0/organization/getorglist', {
            "q": $scope.suspendeds.query,
            "t": 's',
            "p": $scope.suspendeds.page
        }).
            success(function (data, status, headers, config) {
                if (angular.isObject(data)) {
                    $scope.suspendeds.schools = data.organizations;
                    $scope.suspendeds.total = data.totalrows;
                }
            }).
            error(function (data, status, headers, config) {
                // log error
            });
    };

    /* Pagination */
    $scope.suspendedsPageChanged = function(currentPage) {
        //$scope.apage = currentPage;
        $scope.suspendeds.page = currentPage;
        //console.log('Page changed to: ' + currentPage);
        $scope.updateSuspended();
    };

    $scope.$watch('suspendeds.query', function (newValue) {
        $scope.suspendeds.page = 1;
        $scope.updateSuspended();
    }, true);


});

