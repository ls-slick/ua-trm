/**
 * Created by andyl on 16/12/2015.
 */
var uesApp = angular.module('uesApp');

uesApp.controllerProvider.register('SessionController', function ($scope, $http, $location, $route, $rootScope, routeFactory, cssInjector, scriptLoader) {
    // There could have been a refresh.
    $http.get($scope.serverstring+'api/v1.0/login/check').
        success(function (data, status, headers, config) {
            $rootScope.session = data.data;
            angular.forEach(data.metrics.user_routes, function (options, key) {
                routeFactory.addRoute(key, options);
            });

            if(data.metrics.hasOwnProperty('css')){
                cssInjector.add(data.metrics.css);
            }

            // apply additional scripts
            if(data.metrics.hasOwnProperty('script')){
                scriptLoader.loadScript(data.metrics.script);
            }

            if(data.metrics.hasOwnProperty('include')){
                $rootScope.adminurl = data.metrics.include;
            }

            if(data.metrics.hasOwnProperty('includehelp')){
                $rootScope.helpurl = data.metrics.includehelp;
            }

            // set default route.
            $route.routes['null'] = angular.extend({
                redirectTo: data.metrics.default_route,
                reloadOnSearch: true,
                caseInsensitiveMatch: false
            });

            $rootScope.menuItems = data.metrics.menu_items;


            // redirect to default.
            $location.path(data.metrics.default_route); // non existing page will force to default route.
        }).
        error(function (data, status, headers, config) {
            $rootScope.session = null;
            angular.forEach(data.metrics.user_routes, function (options, key) {
                routeFactory.addRoute(key, options);
            });

            // set default route.
            $route.routes['null'] = angular.extend({
                redirectTo: data.metrics.default_route,
                reloadOnSearch: true,
                caseInsensitiveMatch: false
            });

            $rootScope.menuItems = data.metrics.menu_items;

            // redirect to default.
            $location.path(data.metrics.default_route); // non existing page will force to default route.
        });
});