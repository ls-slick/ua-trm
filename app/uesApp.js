/**
 * Created by andyl on 20/11/2015.
 *
 * Angular main APP file.
 *
 * User Level: Public
 */

(function() {

    var uesApp = angular.module('uesApp', [
        'ngRoute',
        'angularModalService',
        'typer',
        'ngAnimate',
        'angular-loading-bar',
        'ui.bootstrap',
        'ui.bootstrap.datetimepicker',
        'ui.dateTimeInput',
        'angular.css.injector',
        'ADM-dateTimePicker',
        'zingchart-angularjs'
    ]);

    // Setup routes.  Initial route only allows a session check against the server.  Server response will contain routes applicable to the user type.
    uesApp.config(
        function($routeProvider, $controllerProvider, $sceDelegateProvider) {

            uesApp.controllerProvider = $controllerProvider;

            $routeProvider.
            when('/session', {
                templateUrl: 'pages/session.html',
                title: ''
            }).
            otherwise({
                redirectTo: '/session'
            });

            $sceDelegateProvider.resourceUrlWhitelist([
                // Allow same origin resource loads.
                'self',
                // Allow loading from our assets domain.  Notice the difference between * and **.
                'http://www.ff-ues.com/**',
                'http://s3.amazonaws.com/**'
            ]);
        });

    uesApp.run(function($rootScope, $route, $http, $location, $uibModalStack, $cacheFactory, $interval, $timeout) {

        $rootScope.adminurl = '';
        $rootScope.loadingnow = false;
        $rootScope.messages = [];

        // Change for remote server testing
        $rootScope.serverstring = 'http://www.slick-tools.com/ff-ues/';
        $rootScope.serverstring = '';



        $rootScope.$on("$routeChangeStart", function(event, toState, toParams, fromState, fromParams) {
            // close open modals.
            $uibModalStack.dismissAll();
        });

        $rootScope.$on("$routeChangeSuccess", function(event, current) {
            // Change page title, based on Route information
            $interval.cancel($rootScope.sessionTimer);

            $rootScope.title = $route.current.title;
            $rootScope.isCollapsed = true;

            // update last page clicked.
            $http.get($rootScope.serverstring + 'api/v1.0/navigation/snapshot?p=' + $location.path());

            //check session and keep it alive
            if ($rootScope.session) {
                $rootScope.sessionTimer = $interval(function() {
                    //instead of changing location to '#/session', which may cause self-reloading problems
                    //do GET to API to let server know that user is still on the page and keep it alive

                    $http.get($rootScope.serverstring + 'api/v1.0/login/keepalive'). //TODO put keepsessionalive API here
                    success(function(data, status, headers, config) {

                    }).error(function(data, status, headers, config) {
                        // log error
                        if ($route.current.title != 'Login') {
                            confirm("You have been logged out");
                            location.reload();
                        }
                        $scope.messages = data.messages;
                    });

                    //$location.path('#/session');
                }, 1000 * 60 * 2); // make sure the API session lives.  Send a ping every 2 mins of inactivity.
            }

            if (typeof ga == 'function') {
                ga('set', {
                    page: $location.path(),
                    title: $route.current.title
                });
                ga('send', 'pageview');
            }

        });

        $rootScope.updateLists = function() {

                $http.get($rootScope.serverstring + 'api/v1.0/forms/elements').
                success(function(data, status, headers, config) {
                    // save elements
                    $rootScope.formElements = data;
                }).
                error(function(data, status, headers, config) {
                    // log error
                });

        };

        $rootScope.updateLists();


    });

    uesApp.factory('getOrgNotesFactory', function($http) {

        return {
            updateNotes: updateNotes
        };

        function updateNotes(orgid) {
            return $http.get('api/v1.0/admin/getorgnotes?orgid=' + orgid)
                //     .success(function(data, status, headers, config) {
                //         if (angular.isObject(data)) {
                //             //console.log(data.data);
                //             return data.data;
                //         }
                //     }).
                // error(function(data, status, headers, config) {
                //     // log error
                //     return data;
                // });
        }
    });

    uesApp.factory('routeFactory', function($route, $templateCache) {
        return {
            addRoute: function(path, options) {
                $route.routes[path] = angular.extend(
                    options,
                    path && this.pathRegExp(path, options.templateUrl));

                // create redirection for trailing slashes
                if (path) {
                    var redirectPath = (path[path.length - 1] == '/') ? path.substr(0, path.length - 1) : path + '/';

                    $route.routes[redirectPath] = angular.extend({
                            redirectTo: path
                        },
                        this.pathRegExp(redirectPath, options.templateUrl));
                }

                return this;
            },
            pathRegExp: function(path, opts) {
                var insensitive = opts.caseInsensitiveMatch,
                    ret = {
                        originalPath: path,
                        regexp: path
                    },
                    keys = ret.keys = [];

                path = path.replace(/([().])/g, '\\$1')
                    .replace(/(\/)?:(\w+)([\?\*])?/g, function(_, slash, key, option) {
                        var optional = option === '?' ? option : null;
                        var star = option === '*' ? option : null;
                        keys.push({
                            name: key,
                            optional: !!optional
                        });
                        slash = slash || '';
                        return '' + (optional ? '' : slash) + '(?:' + (optional ? slash : '') + (star && '(.+?)' || '([^/]+)') + (optional || '') + ')' + (optional || '');
                    })
                    .replace(/([\/$\*])/g, '\\$1');

                ret.regexp = new RegExp('^' + path + '$', insensitive ? 'i' : '');
                return ret;
            },
            clearRoutes: function() {
                // loop through routes
                angular.forEach($route.routes, function(value, key) {
                    // if the key is not 'null' remove it.
                    if (key != 'null') {
                        // Remove template.
                        $templateCache.remove($route.routes[key].templateUrl);
                        // remove route
                        delete($route.routes[key]);
                    }
                });
            }
        };
    });

    uesApp.service('scriptLoader', function() {
        return {
            loadScript: function(url, type, charset) {
                if (type === undefined) type = 'text/javascript';
                if (url) {
                    var script = document.querySelector("script[src*='" + url + "']");
                    if (!script) {
                        var heads = document.getElementsByTagName("head");
                        if (heads && heads.length) {
                            var head = heads[0];
                            if (head) {
                                script = document.createElement('script');
                                script.setAttribute('src', url);
                                //console.log(url+' - Added');
                                script.setAttribute('type', type);
                                if (charset) script.setAttribute('charset', charset);
                                head.appendChild(script);
                            }
                        }
                    }
                    return script;
                }
            }
        };
    });

    uesApp.service('ControllerChecker', ['$controller', function($controller) {
        return {
            exists: function(controllerName) {
                if (typeof window[controllerName] == 'function') {
                    return true;
                }
                try {
                    $controller(controllerName);
                    return true;
                } catch (error) {
                    return !(error instanceof TypeError);
                }
            }
        };
    }]);

    uesApp.directive('checkRequired', function() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, element, attrs, ngModel) {
                ngModel.$validators.checkRequired = function(modelValue, viewValue) {
                    var value = modelValue || viewValue;
                    var match = scope.$eval(attrs.ngTrueValue) || true;
                    return value && match === value;
                };
            }
        };
    });

    uesApp.directive("ngFileModel", [function () {
        return {
            scope: {
                ngFileModel: "="
            },
            link: function (scope, element, attributes) {
                element.bind("change", function (changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.ngFileModel = {
                                lastModified: changeEvent.target.files[0].lastModified,
                                lastModifiedDate: changeEvent.target.files[0].lastModifiedDate,
                                name: changeEvent.target.files[0].name,
                                size: changeEvent.target.files[0].size,
                                type: changeEvent.target.files[0].type,
                                data: loadEvent.target.result
                            };
                        });
                    };
                    reader.readAsDataURL(changeEvent.target.files[0]);
                });
            }
        }
    }]);

    uesApp.directive("fileread", [function () {
        return {
            scope: {
                fileread: "="
            },
            link: function (scope, element, attributes) {
                element.bind("change", function (changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.fileread = loadEvent.target.result;
                        });
                    }
                    reader.readAsDataURL(changeEvent.target.files[0]);
                });
            }
        }
    }]);

    uesApp.directive('emailNotExists', function($http, $rootScope) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, element, attrs, ngModel) {

                ngModel.$parsers.unshift(function(value) {
                    var valid = true;
                    if (value) {
                        // test and set the validity after update.
                        $http.post($rootScope.serverstring + 'api/v1.0/organization/checkemail', {
                            email: value
                        }).
                        success(function(data, status, headers, config) {
                            // doesnt exist
                            valid = true;
                            ngModel.$setValidity('emailInUse', valid);
                            // if it's valid, return the value to the model,
                            // otherwise return undefined.
                            return valid ? value : undefined;

                        }).
                        error(function(data, status, headers, config) {
                            // exists
                            valid = false;
                            ngModel.$setValidity('emailInUse', valid);
                            // if it's valid, return the value to the model,
                            // otherwise return undefined.
                            return valid ? value : undefined;
                        });
                    }
                    return valid ? value : undefined;
                });
            }
        };
    });

    uesApp.directive('pwCheck', [function() {
        return {
            require: 'ngModel',
            link: function(scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.pwCheck;
                elem.add(firstPassword).on('keyup', function() {
                    scope.$apply(function() {
                        var v = elem.val() === $(firstPassword).val();
                        ctrl.$setValidity('pwmatch', v);
                    });
                });
            }
        }
    }]);

    uesApp.filter('objLimitTo', [function() {
        return function(obj, limit) {
            var keys = Object.keys(obj);
            if (keys.length < 1) {
                return [];
            }

            var ret = new Object,
                count = 0;
            angular.forEach(keys, function(key, arrayIndex) {
                if (count >= limit) {
                    return false;
                }
                ret[key] = obj[key];
                count++;
            });
            return ret;
        };
    }]);

    uesApp.filter('objFilter', function() {
        return function(items, search) {
            var result = [];
            angular.forEach(items, function(value, key) {
                angular.forEach(value, function(value2, key2) {
                    if (value2 === search) {
                        result.push(value2);
                    }
                })
            });
            return result;

        }
    });
    /*
        uesApp.directive('multi', ['$parse', '$rootScope', function ($parse, $rootScope) {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, elem, attrs, ngModelCtrl) {
                    var validate = $parse(attrs.multi)(scope);
    
                    ngModelCtrl.$viewChangeListeners.push(function () {
                        // ngModelCtrl.$setValidity('multi', validate());
                        $rootScope.$broadcast('multi:valueChanged');
                    });
    
                    var deregisterListener = scope.$on('multi:valueChanged', function (event) {
                        ngModelCtrl.$setValidity('multi', validate());
                    });
                    scope.$on('$destroy', deregisterListener); // optional, only required for $rootScope.$on
                }
            };
        }]);*/

    uesApp.directive('minTotal', [function() {
        return {
            require: ['ngModel', '^form'],
            restrict: 'A',
            link: function(scope, elem, attrs, ctrls) {

                form = ctrls[1];
                ngModel = ctrls[0];

                ngModel.$parsers.unshift(function(value) {
                    var valid = true;

                    var otherMinTotals = angular.element(document.querySelectorAll("[min-total]"));

                    var totalEntered = 0;
                    angular.forEach(otherMinTotals, function(options, key) {
                        if (options.value) {
                            totalEntered = totalEntered + parseInt(options.value);
                        }
                    });

                    valid = totalEntered >= attrs.minTotal;

                    // if it's valid, return the value to the model,
                    // otherwise return undefined.
                    return valid ? value : undefined;

                });

            }
        }
    }]);

    uesApp.directive('datepicker', function() {
        return {
            link: function(scope, el, attr) {
                $(el).datepicker({
                    onSelect: function(dateText) {
                        console.log(dateText);
                        var expression = attr.ngModel + " = " + "'" + dateText + "'";
                        scope.$apply(expression);
                        console.log(scope.startDate);
                        // how do i set this elements model property ?
                    }
                });
            }
        };
    });

    uesApp.directive('customOnChange', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var onChangeHandler = scope.$eval(attrs.customOnChange);
                element.bind('change', onChangeHandler);
            }
        };
    });

    uesApp.filter('toArray', function() {
        return function(obj, addKey) {
            if (!angular.isObject(obj)) return obj;
            if (addKey === false) {
                return Object.keys(obj).map(function(key) {
                    return obj[key];
                });
            } else {
                return Object.keys(obj).map(function(key) {
                    var value = obj[key];
                    return angular.isObject(value) ?
                        Object.defineProperty(value, '$key', { enumerable: false, value: key }) : { $key: key, $value: value };
                });
            }
        };
    });

    uesApp.directive('ngFocusIf', ["$timeout", function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                if (scope.$eval(attrs['ngFocusIf'])) {
                    $timeout(function() {
                        element.focus();
                    }, 0);
                }
            }
        };
    }]);

    uesApp.directive('focusMe', function($timeout, $parse) {
        return {
            //scope: true,   // optionally create a child scope
            link: function(scope, element, attrs) {
                var model = $parse(attrs.focusMe);
                scope.$watch(model, function(value) {
                    if (value === true) {
                        $timeout(function() {
                            element[0].focus();
                        });
                    }
                });
                // Might be showing errors - model.assign not a fn
                // to address @blesh's comment, set attribute value to 'false'
                // on blur event:  
                // element.bind('blur', function() {
                //     scope.$apply(model.assign(scope, false));
                // });
            }
        };
    });

    uesApp.directive('embedSrc', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var current = element;
                scope.$watch(attrs.embedSrc, function() {
                    var clone = element
                        .clone()
                        .attr('src', attrs.embedSrc);
                    current.replaceWith(clone);
                    current = clone;
                });
            }
        };
    })



    uesApp.filter('abs', function() {
        return function(val) {
            return Math.abs(val);
        }
    });

    uesApp.filter('nonBlank', function() {
        return function(input, filter, isEnable) {
            // if isEnable then filter out wines
            // input = list to be filtered
            // filter = object to find
            // isEnable = whether to filter
            if (isEnable) {
                var result = [];
                angular.forEach(input, function(branch) {
                    angular.forEach(filter, function(value, key) {
                        // type = key
                        if (value === branch[key]) {
                            result.push(branch);
                        }
                    });

                });
                return result;
            } else {
                // otherwise just do not any filter just send input without changes
                return input
            }
        };
    });

    uesApp.filter('colorExists', function() {
        return function(input, filter) {
            if (!filter) {
                return input;
            }
            var found = null;
            var result = [];
            angular.forEach(input, function(color) {
                found = false;
                angular.forEach(filter, function(value, key) {
                    if (color.colour_id == value) {
                        found = true;
                    }
                });
                if (found != true) {
                    result.push(color);
                }
            });
            return result;
        }
    });

    //check if 'input' array of emblems
    //has height smaller than filter (maxSize)
    uesApp.filter('smallLogo', function() {
        return function(input, filter) {
            if (!filter) {
                return input;
            }
            var found = null;
            var result = [];
            angular.forEach(input, function(logo) {
                found = false;
                var height = parseFloat(logo.size__mm_.split('x')[1]);
                if (height <= filter.maxSize) {
                    found = true;
                    result.push(logo);
                }
            });
            return result;
        }
    });

    uesApp.filter('reverse', function() {
        return function(items) {
            if (items) {
                return items.slice().reverse();
            }
        };
    });


    uesApp.controller("MenuCtrl", function($scope, $location, $rootScope, $sce, $window, $timeout) {
        $scope.menuClass = function(page) {
            var current = $location.path().substring(1);
            return page === current ? "active" : "";
        };
        $rootScope.menuItems = [];
        $scope.rs = $rootScope;
        $scope.rs.isCollapsed = true;


        $scope.copyURL = function(e) {
            document.execCommand('copy', false, document.getElementById(e).select());
        };

        $scope.viewPage = function() {

            $window.open('https://uniforms.tllab.co.uk/ueslink/' + $rootScope.session.organizations[$rootScope.session.showingorg].organization_id + '.school', '_blank');
        };

    });

    uesApp.controller('ModalController', function($scope, $uibModal, $log) {

        $scope.items = ['item1', 'item2', 'item3'];

        $scope.modalResult = null;

        $scope.showModal = function(type, size, options) {
            var template, controller;
            switch (type) {
                case 'r':
                    template = 'pages/modals/register.html';
                    controller = 'ModalInstanceCtrl';
                    break;
                case 'n':
                default:
                    template = 'pages/modals/nominate.html';
                    controller = 'NominateInstanceCtrl';
                    break;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: template,
                controller: controller,
                size: size,
                resolve: {
                    options: function() {
                        return options;
                    },
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });

        };

    });

    uesApp.controller('NominateInstanceCtrl', function($scope, $uibModalInstance, $http, options, items, $timeout, $filter) {

        $scope.options = options;
        $scope.items = items;
        $scope.messages = [];
        $scope.parentforename = null;
        $scope.parentsurname = null;
        $scope.parentemail = null;
        $scope.checkbox1 = null

        $scope.ok = function() {
            $scope.messages = [];
            if ($scope.nominateForm.$valid) {
                $scope.doSubmit();
            }
        };

        $scope.doSubmit = function() {
            $http.post($scope.serverstring + 'api/v1.0/organization/nominate', {
                urnid: options.urn,
                parentforename: $scope.parentforename,
                parentsurname: $scope.parentsurname,
                parentemail: $scope.parentemail
            }).
            success(function(data, status, headers, config) {
                $scope.messages = data.messages;
                // Update the search results?
                var searchbox = $('#q');
                var temp = searchbox.val();


                var searchScope = angular.element($("#schoolsearchwidget")).scope(); // Get the scope of the search list object.
                //filter the array
                try {
                    // Find the school that matches this URN.
                    var foundItem = $filter('filter')(searchScope.schools, { URN: options.urn }, true)[0];
                    // var index = searchScope.schools.indexOf(foundItem);
                    foundItem.nominations++; // increase nominations on the page by 1.
                } catch (err) {
                    //console.log('error: ' + err.message);
                }

                $timeout(function() {
                    // Make sure they see the success message before it closes.
                    $uibModalInstance.close();
                }, 1000);

            }).
            error(function(data, status, headers, config) {
                // Update messages
                $scope.messages = data.messages;
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    });

    //---------------------------------------------------------------------//
    //------------------------ConfirmController----------------------------//
    //---------------------------------------------------------------------//
    uesApp.controller('ConfirmController', function($scope, $uibModalInstance, options, $sce) {

        //------DATA------/
        $scope.question = options;
        $scope.question.message = $sce.trustAsHtml($scope.question.message);

        //------MODAL-----/
        $scope.ok = function() {
            $uibModalInstance.close(true);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

    });

})();