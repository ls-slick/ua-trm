<?php
/**
 * Created by PhpStorm.
 * User: andyl
 * Date: 10/09/2015
 * Time: 16:15
 */

register_shutdown_function('shutdown');
set_exception_handler('eshutdown');

function shutdown() {
    $error = error_get_last();
    if ($error['type'] === E_ERROR) {
        // fatal error has occured
        // default to just show te generic json error.
        //echo '{"errors":[{"message":"We are unable to perform search operation now. Service currently unavailable please try again later","errorCode":"school-search-error"}]}';
        echo '{"errors":[{"message":"'.$error['message'].'"}]}';
    }
}

function eshutdown() {
    echo '{"errors":[{"message":"We are unable to perform search operation now. Service currently unavailable please try again later","errorCode":"school-search-error"}]}';
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function echolog($message,$type='info',$pane='messages'){

    $fileTrace = debug_backtrace();
    $backtrace = $fileTrace[0];

    $fh = fopen($backtrace['file'], 'r');
    $line = 0;
    $code = '';
    while (++$line <= $backtrace['line']) {
        $code = fgets($fh);
    }
    fclose($fh);

    //preg_match('/'. __FUNCTION__ .'\s*\((.*)\)\s*;/u', $code, $name);
    preg_match('/\$[^,)]*/u', $code, $name);
    // $name = explode(",",$name[0]); // Just want the first arg if many.
    $reference = $name[0];

    $calledFile = date('r')."".basename($backtrace['file']).'('.$backtrace['line'].') '.$reference.' : ';

    echo $calledFile.print_r($message,1);
}

function getLEAName($id){

    $LEAQuery = dbq("select local_authority_name from local_authorities where local_authority_id = :ID",[
        ':ID' => $id
    ]);

    $LEA = dbf($LEAQuery);

    return $LEA['local_authority_name'];
}

function getTitle($id){

    $titleQuery = dbq("select user_title from user_titles where user_title_id = :ID",[
        ':ID' => $id
    ]);

    $title = dbf($titleQuery);

    return $title['user_title'];
}

function getLogoDetails($logoID){

    $logoQuery = dbq("select * from organization_logos where organization_logo_id = :ID",[
        ':ID' => $logoID
    ]);

    $logo = dbf($logoQuery);

    return $logo;

}


function getColourString($string){
    $colours = explode(',',$string);

    $colourString = '';

    foreach($colours as $colour){

        if(!empty($colourString)){
            $colourString .= "\n";
        }

        $colourDetailQuery = dbq("select colour_full_name from threads where colour_code = :CODE",
            [
                ':CODE' => $colour
            ]
        );

        $colourDetail = dbf($colourDetailQuery);

        $colourString .= "\t".$colourDetail['colour_full_name'];
    }

    return $colourString;

}


function getSchoolType($id){

    $typeQuery = dbq("select organization_type_name from organization_types where organization_type_id = :ID",[
        ':ID' => $id
    ]);

    $type = dbf($typeQuery);

    return $type['organization_type_name'];
}


function getEduPhase($id){

    $phaseQuery = dbq("select education_phase_name from education_phases where education_phase_id = :ID",[
        ':ID' => $id
    ]);

    $phase = dbf($phaseQuery);

    return $phase['education_phase_name'];
}

function jsonOutput($data){
    // Clear OB first.
    ob_end_clean();
    if(isset($data['type'])){
        header("HTTP/1.1 ".$data['type']);
        unset($data['type']);
    }
    header('Content-Type: application/json;charset=utf-8');

    //header('Content-Type: application/json');
    echo json_encode($data);
}

function ListIn($dir, $prefix = '',$extn=null) {
    $dir = rtrim($dir, '\\/');
    $result = array();

    $h = opendir($dir);
    while (($f = readdir($h)) !== false) {
        if ($f !== '.' and $f !== '..') {

            if (is_dir("$dir/$f")) {
                $result = array_merge($result, ListIn("$dir/$f", "$prefix$f/",$extn));
            } else {
                if(!is_null($extn)){
                    // Just this extension
                    $fileExtension = explode(".",$f);
                    $fileExtension = array_pop($fileExtension);
                    if($fileExtension == $extn){
                        $result[] = $prefix.$f;
                    }
                } else {
                    $result[] = $prefix.$f;
                }

            }
        }
    }
    closedir($h);

    return $result;
}

function redirect($url){
    ob_end_clean();
    header('location:'.$url);
    exit;
}

if(!function_exists('boolval')){
    function boolval($value){
        return (bool) $value;
    }
}

////
// Return all HTTP GET variables, except those passed as a parameter
function getGetParams($exclude_array = '') {

    if (!is_array($exclude_array)) $exclude_array = array();

    $get_url = '';
    if (is_array($_GET) && (sizeof($_GET) > 0)) {
        reset($_GET);
        while (list($key, $value) = each($_GET)) {
            if ( (strlen($value) > 0) && ($key != 'error') && (!in_array($key, $exclude_array)) && ($key != 'x') && ($key != 'y') ) {
                $get_url .= $key . '=' . rawurlencode(stripslashes($value)) . '&';
            }
        }
    }
    return $get_url;
}

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

////
// Return a random value
function new_rand($min = null, $max = null)
{
    static $seeded;

    if (!isset($seeded)) {
        mt_srand((double)microtime() * 1000000);
        $seeded = true;
    }

    if (isset($min) && isset($max)) {
        if ($min >= $max) {
            return $min;
        } else {
            return mt_rand($min, $max);
        }
    } else {
        return mt_rand();
    }
}

function resolveTicket($ticketid){
    if($ticketid == "") {
        return false;
    }

    dbq("update tickets set ticket_alert = 0 where ticket_id = :TICKETID",
        [
            ':TICKETID' => $ticketid
        ]
    );

    return true;
}

////
//! Send email (text/html) using MIME
// This is the central mail function. The SMTP Server should be configured
// correct in php.ini
// Parameters:
// $to_name           The name of the recipient, e.g. "Jan Wildeboer"
// $to_email_address  The eMail address of the recipient,
//                    e.g. jan.wildeboer@gmx.de
// $email_subject     The subject of the eMail
// $email_text        The text of the eMail, may contain HTML entities
// $from_email_name   The name of the sender, e.g. Shop Administration
// $from_email_adress The eMail address of the sender,
//                    e.g. info@mytepshop.com
function getFile($filename) {
    $return = '';
    if ($fp = fopen($filename, 'rb')) {
        while (!feof($fp)) {
            $return .= fread($fp, 1024);
        }
        fclose($fp);
        return $return;
    } else {
        return false;
    }
}

function ues_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address, $email_title="") {
return;
    if($email_title == "") {
        $email_title = $email_subject;
    }
    include_once(DIR_CLASSES.'class.phpmailer.php');

    $body    = eregi_replaceb("�\\�",'',getFile(DIR_INCLUDES.'mailtemplate.html'));

    $from     = "noreply@ff-ues.com";
    $fromName = "F&F Uniforms";

    $alt = $email_text;
    $body = str_replace("~CONTENT~",nl2br($email_text),$body);
    $body = str_replace("~SUBJECT~",nl2br($email_subject),$body);
    $body = str_replace("~TITLE~",nl2br($email_title),$body);

    $mail    		= new PHPMailer();
    // Additional
    $mail->IsSMTP();

    $mail->Mailer = 'smtp';
    $mail->CharSet = 'UTF-8';
    $mail->Host     = "smtp.gmail.com";
    $mail->Subject	= $email_subject;
    $mail->SMTPDebug = false;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "ssl";
    $mail->Port = 465;
    $mail->Username = 'andy@slickstitch.com';
    $mail->Password = 'K4N8zyfF';

    // Use SMTP..
    //Removed to stop it attaching images
    $mail->MsgHTML($body);
    $mail->From		= $from;
    if($from_email_address <> $from){
        $from_email_name .= " <".$from_email_address.">";
    }
    $mail->FromName	= $from_email_name;
    $mail->IsHTML(true);
    $mail->Body = $body;
    $mail->AltBody	= $alt;
    $to_email_addresses = explode(',',$to_email_address);
    foreach($to_email_addresses as $email_address)
    {
        $mail->AddAddress(trim($email_address), $to_name);
    }

    //$mail->AddAddress("artworkfiles@gmail.com", "Andy GMail");


    if($mail->Send()){
        //  die;
    } else {

    }

}


function addTicket($type,$organization,$subject,$message,$alert=null,$alertdate=null,$alertuser=null,$alertgroup=null){

    $message = str_replace("<br />","\n",$message);

    if($alert){
        if(empty($alertuser)){
            $alertuser = $_SESSION['session']['user']['user_id'];
        }
    }
    dbq("insert into tickets (
                ticket_organization,
                ticket_type_id,
                ticket_status_id,
                ticket_open_date,
                ticket_close_date,
                ticket_subject,
                ticket_description,
                ticket_user_id,
                ticket_user_name,
                ticket_complete,
                ticket_alert,
                ticket_alert_date,
                ticket_alert_user_id,
                ticket_alert_user_type_id,
                ticket_modified_date,
                ticket_deadline
              ) VALUES (
                :TICKETORGANIZATION,
                :TICKETTYPEID,
                :TICKETSTATUSID,
                :TICKETOPENDATE,
                :TICKETCLOSEDATE,
                :TICKETSUBJECT,
                :TICKETDESCRIPTION,
                :TICKETUSERID,
                :TICKETUSERNAME,
                :TICKETCOMPLETE,
                :TICKETALERT,
                :TICKETALERTDATE,
                :TICKETALERTUSERID,
                :TICKETALERTUSERTYPEID,
                :TICKETMODIFIEDDATE,
                :TICKETDEADLINE
              )",[
                ':TICKETORGANIZATION' => $organization,
                ':TICKETTYPEID' => $type,
                ':TICKETSTATUSID' => null,
                ':TICKETOPENDATE' => date("Y-m-d G:i:s"),
                ':TICKETCLOSEDATE' => null,
                ':TICKETSUBJECT' => $subject,
                ':TICKETDESCRIPTION' => $message,
                ':TICKETUSERID' => $_SESSION['session']['user']['user_id'],
                ':TICKETUSERNAME' => null,
                ':TICKETCOMPLETE' => null,
                ':TICKETALERT' => $alert,
                ':TICKETALERTDATE' => $alertdate,
                ':TICKETALERTUSERID' => $alertuser,
                ':TICKETALERTUSERTYPEID' => $alertgroup,
                ':TICKETMODIFIEDDATE' => null,
                ':TICKETDEADLINE' => null
            ]
    );
}

function audit($type,$organization,$details=null){
    // user is always session or API if not.

    if(!isset($_SESSION['session'])){
        $userid = 1;
    } else {
        $userid = $_SESSION['session']['user']['user_id'];
    }
    // Insert into audit table
    dbq("insert into audit_log (
              audit_log_event_id,
              audit_timestamp,
              audit_event_info,
              user_id,
              organization_id,
              ip_address
            ) VALUES (
              :AUDITLOGEVENTID,
              NOW(),
              :AUDITEVENTINFO,
              :USERID,
              :ORGANIZATIONID,
              :IPADDRESS
            )",
        [
              ':AUDITLOGEVENTID' => $type,
              ':AUDITEVENTINFO' => $details,
              ':USERID' => $userid,
              ':ORGANIZATIONID' => $organization,
              ':IPADDRESS' => (empty(ip2long($_SERVER['REMOTE_ADDR'])) ? '0' : ip2long($_SERVER['REMOTE_ADDR']))
        ]);
}

function isBright($rgb) {
    // returns brightness value from 0 to 255
    list($r,$g,$b) = explode(",",$rgb);

    $brightness = (($r * 299) + ($g * 587) + ($b * 114)) / 1000;
    $isbright = ($brightness > 126) ? true : false;
    return $isbright;
}

if(!function_exists('password_hash')){
    function password_hash($strPassword, $numAlgo = 1, $arrOptions = array())
    {
            $salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
            $salt = base64_encode($salt);
            $salt = str_replace('+', '.', $salt);
            $hash = crypt($strPassword, '$2y$10$' . $salt . '$');

        return $hash;
    }

    function password_verify($strPassword, $strHash)
    {

            $strHash2 = crypt($strPassword, $strHash);
            $boolReturn = $strHash == $strHash2;

        return $boolReturn;
    }
}

function refreshSession()
{

    $newSession = [];

    // set the data if needed?  May just work off true for now and redirect front end.
    $userDataQuery = dbq("SELECT * FROM users JOIN user_types USING (user_type_id) WHERE user_id = :USERID", [':USERID' => $_SESSION['session']['user']['user_id']]);
    $userData = dbf($userDataQuery);

    unset($userData['user_password']); // Don't want that out there.
    $newSession = [
        'user' => $userData,
        'users' => []
    ];

    $addressDataQuery = dbq("SELECT * FROM addresses WHERE user_id = :USERID", [':USERID' => $userData['user_id']]);
    while ($addressData = dbf($addressDataQuery)) {
        unset($addressData['user_id']); // Don't want that out there.
        $newSession['addresses'][] = $addressData;
    }

    // Add org data

    $organizationQuery = dbq("SELECT *, o.organization_id FROM organizations o
													JOIN users_to_organizations u2o USING (organization_id)
													LEFT JOIN organization_logos ol ON (ol.organization_logo_id = o.organization_default_logo_id  AND ol.logo_deleted != 1)
													WHERE u2o.user_id = :USERID", [':USERID' => $userData['user_id']]);

    while ($organization = dbf($organizationQuery)) {

        unset($organization['user_id']); // Don't want that out there.
        $newSession['organizations'][$organization['organization_id']] = $organization;
        $newSession['organizations'][$organization['organization_id']]['type'] = 'O';
        $newSession['showingorg'] = $organization['organization_id'];

        $addressDataQuery = dbq("SELECT * FROM addresses WHERE organization_id = :ORGID", [':ORGID' => $organization['organization_id']]);
        while ($addressData = dbf($addressDataQuery)) {
            unset($addressData['user_id']); // Don't want that out there.
            $newSession['organizations'][$organization['organization_id']]['addresses'][] = $addressData;
        }

        // get users for this org.
        $newSession['users'][$organization['organization_id']] = [];

        $orgUserDataQuery = dbq("SELECT u.*, u2o.user_primary FROM users u JOIN users_to_organizations u2o USING (user_id) WHERE u2o.organization_id = :ORGID", [':ORGID' => $organization['organization_id']]);
        while ($orgUserData = dbf($orgUserDataQuery)) {
            unset($orgUserData['user_password']); // Don't want that out there.
            $newSession['users'][$organization['organization_id']][] = $orgUserData;
        }

        // Houses?
        $houseSet = false;
        if ($organization['organization_has_subunits'] == 1) {
            // has houses.  get the houses.

            $houseDataQuery = dbq("select *, o.organization_id from organizations o
								left JOIN organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id and ol.logo_deleted != 1)
								where organization_parent_id = :ORGID",[':ORGID' => $organization['school_URN']]);
            while ($houseData = dbf($houseDataQuery)) {
                $houseData['organization_parent_name'] = $organization['organization_name'];
                $houseData['organization_form_signed'] = $organization['organization_form_signed'];

                $newSession['organizations'][$houseData['organization_id']] = $houseData;
                $newSession['organizations'][$houseData['organization_id']]['type'] = 'H';
                // add main org address here.

                $newSession['organizations'][$houseData['organization_id']]['addresses'] = $newSession['organizations'][$organization['organization_id']]['addresses'];
                $newSession['users'][$houseData['organization_id']] = $newSession['users'][$organization['organization_id']];

                if (!$houseSet) {
                    $newSession['showingorg'] = $houseData['organization_id'];
                    $houseSet = true;
                }
            }
        }

    }

    if(isset($_SESSION['session']['showingorg'])){
        $newSession['showingorg'] = $_SESSION['session']['showingorg'];
    }

    $_SESSION['session'] = $newSession;

}

function getCountryName($countryID){
    $countryQuery = dbq("select countries_name from countries where countries_id = :CID",
        [
            ':CID' => $countryID
        ]
    );
    $country = dbf($countryQuery);
    return $country['countries_name'];
}

function debugMail($details) {

    $subject = 'Debug Info';
    $title = $subject;
    $template = 'tesco-ff-uniforms';
    $tag = ['ff-debug'];

    $html = print_r($details,1);

    $emailAddress = [
        'Andrew Lindsay' => 'andy@slickstitch.com'
    ];

    $template_content = array(
        [
            'name' => 'emailtitle',
            'content' => $title
        ],
        [
            'name' => 'main',
            'content' => $html
        ]
    );

    send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);

}

function inExit($URN){
    $exitQuery = dbq("select * from exit_data where school_id = :URN",[
        ':URN' => $URN
    ]);

    if(dbnr($exitQuery) > 0){
        return true;
    }

    return false;
}
//send_ues_mail($emailAddress, $subject, $replaceFields, $template, $tag);

function shorten($num)
{
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-';
    $shortenedId = '';
    while($num>0) {
        $remainder = $num % 64;
        $num = ($num-$remainder) / 64;
        $shortenedId = $alphabet{$remainder} . $shortenedId;
    };
    return $shortenedId;
}

function send_ues_mail($to_email_addresses, $email_subject, $template_content, $customer_template_name='tesco-ff-uniforms', $extra_tags = null, $attachments = null){
return;
    if(SHOW_DEBUG){
        // This is just dev, only send to IT.
        $to_email_addresses = [
            'SSIT' => 'it@slickstitch.com'
        ];
    }

    try {
        $mandrill = new Mandrill('-5Sh4T6e9Jpr8SMyZNiycQ');

        $message = array(
            'subject' => $email_subject,
            'from_email' => 'no-reply@ff-ues.com',
            'from_name' => 'F&F Uniforms',
           /* 'to' => array(
                array(
                    'email' => $to_email_address,
                    'name' => $to_name,
                    'type' => 'to'
                )
            ),*/
            'important'=> false,
            'track_opens' => null,
            'track_clicks' => false,
            'auto_text' => null,
            'auto_html' => null,
            'inline_css' => null,
            'url_strip_qs' => null,
            'preserve_recipients' => null,
            'view_content_link' => null,
            'bcc_address' => null,
            'tracking_domain' => null,
            'signing_domain' => 'ff-ues.com',
            'return_path_domain' => null,
            'merge' => true,
            'merge_language' => 'mailchimp',
            'global_merge_vars' => null,
            'merge_vars' => null,
            'metadata' => array('website' => 'www.ff-ues.com'),
            'recipient_metadata' => null,
            'tags' => array('ff-ues-email'),
            'subaccount' => 'ff-uniforms',
        );

        foreach($to_email_addresses as $emailname => $emailaddress){
            $message['to'][] = [
                'email' => $emailaddress,
                'name' => $emailname,
                'type' => 'to'
            ];
        }

        if(is_array($extra_tags)){
            foreach($extra_tags as $tag){
                $message['tags'][] = $tag;
            }
        }

        if(is_array($attachments)){
                // add attachment.
                $message['attachments'] = $attachments;
        }

        $async = false;
        $ip_pool = 'Main Pool';
        //$send_at = '1999-01-01 12:34:56';
        $result = $mandrill->messages->sendTemplate($customer_template_name, $template_content, $message, $async, $ip_pool);//, $send_at);

      //  print_r($result);

        if ($result[0]['status'] == 'sent')
        {
         //   echo '[ok] mail sent<br />';
            return true;
        } else {
            return false;
        }
    } catch (Mandrill_Error $e) {
        echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
        return false;
    }


}

function getCategoryID($catname) {
    $catIDQuery = dbq("select product_category_id from product_categories where product_category_name = :CNAME",
        [
            ':CNAME' => $catname
        ]
    );

    if(dbnr($catIDQuery) > 0){
        $catID = dbf($catIDQuery);
        return $catID['product_category_id'];
    }

    return 21; // default to accessories
}

function fast_mime_content_type($filename) {

    $mime_types = array(

        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',

        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',

        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',

        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',

        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',

        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',

        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    );

    $ext = @strtolower(array_pop(explode('.',$filename)));

    if (array_key_exists($ext, $mime_types)) {
        return $mime_types[$ext];
    }
    elseif (function_exists('finfo_open')) {
        $finfo = finfo_open(FILEINFO_MIME);
        $mimetype = finfo_file($finfo, $filename);
        finfo_close($finfo);
        return $mimetype;
    }
    else {
        return 'application/octet-stream';
    }
}



function getOrgDetails($orgID){
    $orgQuery = dbq("SELECT * FROM organizations o
								WHERE o.organization_id = :ORGID								",
        [
            ':ORGID' => $orgID
        ]
    );

    $org = dbf($orgQuery);
    return $org;
}

function colourFromRGB($rgb){
    $colourQuery = dbq("SELECT colour_name FROM colours
								WHERE colour_rgb = :RGB",
        [
            ':RGB' => $rgb
        ]
    );

    if(dbnr($colourQuery) > 0){
        // this is a house get the ID.
        $colour = dbf($colourQuery);
        return $colour['colour_name'];
    }

    return 'Unknown';
}

function getSchoolNameFromURN($URN){
    $parentOrgQuery = dbq("SELECT organization_name FROM organizations o
								WHERE o.school_URN = :URN",
        [
            ':URN' => $URN
        ]
    );

    if(dbnr($parentOrgQuery) > 0){
        // this is a house get the ID.
        $parentOrg = dbf($parentOrgQuery);
        return $parentOrg['organization_name'];
    }

    return 'Unknown';
}


function getMasterURN($URN){
    $parentOrgQuery = dbq("SELECT oo.school_URN FROM organizations o
								JOIN organizations oo on (oo.school_URN = o.organization_parent_id)
								WHERE o.school_URN = :URN
								AND o.organization_parent_id IS NOT NULL",
        [
            ':URN' => $URN
        ]
    );

    if(dbnr($parentOrgQuery) > 0){
        // this is a house get the ID.
        $parentOrg = dbf($parentOrgQuery);
        return $parentOrg['school_URN'];
    }

    return $URN;
}

function getMasterOrgID($orgID){
    $parentOrgQuery = dbq("SELECT oo.organization_id FROM organizations o
								JOIN organizations oo on (oo.school_URN = o.organization_parent_id)
								WHERE o.organization_id = :ORGID
								AND o.organization_parent_id IS NOT NULL",
        [
            ':ORGID' => $orgID
        ]
    );

    if(dbnr($parentOrgQuery) > 0){
        // this is a house get the ID.
        $parentOrg = dbf($parentOrgQuery);
        return $parentOrg['organization_id'];
    }

    return $orgID;
}


function getOrgIDFromURN($URN){
    $parentOrgQuery = dbq("SELECT organization_id FROM organizations
								WHERE school_URN = :URN",
        [
            ':URN' => $URN
        ]
    );

    if(dbnr($parentOrgQuery) > 0){
        // this is a house get the ID.
        $parentOrg = dbf($parentOrgQuery);
        return $parentOrg['organization_id'];
    }

    return false;
}



function eregib($a,$b,$c=null){
    return preg_match("/".$a."/i",$b,$c);
}

function eregb($a,$b,$c=null){
    return preg_match("/".$a."/",$b,$c);
}

function ereg_replaceb($a,$b,$c){
    return preg_replace("/".$a."/",$b,$c);
}

function eregi_replaceb($a,$b,$c){
    return preg_replace("/".$a."/i",$b,$c);
}

function timeecho($string=''){
    echo $string.' - '.microtime(true).PHP_EOL.PHP_EOL;
}