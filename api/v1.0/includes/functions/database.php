<?php
/**
 * Created by PhpStorm.
 * User: andyl
 * Date: 10/09/2015
 * Time: 16:15
 */

function dbq($Query,$Parameters=array(),$testMode = false){
    global $mysql_PDO;

    $thisQuery = $mysql_PDO->prepare($Query);
    $thisQuery->execute($Parameters);
    if($thisQuery->errorCode()!=0){
        dbe($thisQuery,$Parameters,$thisQuery->errorInfo());
    }

    if($testMode){
        echo "<pre>".print_r($thisQuery,1)."</pre>";
        echo "<pre>".print_r($Parameters,1)."</pre>";
        die($Query);
    }
    return $thisQuery;
}

function dbqr($Query,$Parameters=array()){
    global $mysql_PDO;
    $thisQuery = $mysql_PDO->prepare($Query);
    $thisQuery->execute($Parameters);
    return $thisQuery;
}

function dbf($resource,$die = false){
    $returnFetch = $resource->fetch(PDO::FETCH_ASSOC);
    if($die){
        die("<pre>".print_r($returnFetch,1)."</pre>");
    }
    if(is_array($returnFetch)){
        return $returnFetch;
    } else {
        return false;
    }
}

function dbe($Query,$Parameters,$errorCode) {
    global $errorList;
    //throw new Exception();
    $query_string = '';
    $query_string .= isset($Query->errorInfo[1]) ? $Query->errorInfo[1] ." - " : '';
    $query_string .= isset($Query->errorInfo[2]) ? $Query->errorInfo[2] ." - " : '';
    $query_string .= isset($Query->queryString) ? $Query->queryString : '';


    //." - ".$Query->__errorInfo[2]." - ".$Query->__query;
    $errorCode = print_r($errorCode,1);
    die($errorCode);
    $backtrace = debug_backtrace();
    $backtraceString = '';
    for($i=0;$i<sizeof($backtrace);$i++){
        $backtraceString .= str_replace("/home/stot/public_html/","",$backtrace[$i]['file']). " - Line ". $backtrace[$i]['line']. " Queried by " . $backtrace[$i]['function']. " ";
        $backtraceString .= "\n\n";
    }
    $backtraceString .= "\n\n".print_r($Parameters,1)."\n\n";
    throw new Exception($errorCode);
    //tep_db_query_raw("INSERT INTO `sql_log` (`sql_query` ,`spare`,`backtrace`) VALUES (:QUERY, now(), :BACKTRACE)",array(":QUERY"=>$query_string,":BACKTRACE"=>$backtraceString));
    //die('<font color="#ff0000">There has been an error with this page.  The site owners have been informed however if you would like help with this issue, please see the <a href="contact_us.php">Contact Us</a> page.</font>');
    //die('<font color="#ff0000">There has been an error with this page.  The site owners have been informed however if you would like help with this issue, please see the <a href="contact_us.php">Contact Us</a> page.</font><pre>'.$errorCode."\n\n".$query_string."\n\n".$backtraceString."\n\n".print_r($_SESSION,1)."\n\n".print_r($errorList,1)."</pre>");

}

function dbo($string) {
    return htmlspecialchars($string);
}

function dbi($string) {
    return addslashes($string);
}

function dbpi($string) {
    if (is_string($string)) {
        return trim(tep_sanitize_string(stripslashes($string)));
    } elseif (is_array($string)) {
        reset($string);
        while (list($key, $value) = each($string)) {
            $string[$key] = dbpi($value);
        }
        return $string;
    } else {
        return $string;
    }
}

function dbp($table, $data, $action = 'insert', $parameters = '', $link = 'db_link',$paramArray=array()) {
    reset($data);
    $paramData = array();
    if ($action == 'insert') {
        $query = 'insert into ' . $table . ' (';
        while (list($columns, ) = each($data)) {
            $query .= '`'.$columns . '`, ';
        }
        $query = substr($query, 0, -2) . ') values ( ';
        reset($data);
        $fieldNum = 1;
        while (list(, $value) = each($data)) {
            switch ((string)$value) {
                case 'now()':
                    $query .= 'now() , ';
                    break;
                case 'null':
                    $query .= 'null , ';
                    break;
                default:
                    $query .= ':PERFORM_'.$fieldNum.'_FIELD , ';
                    $paramData[':PERFORM_'.$fieldNum.'_FIELD'] = dbi($value);
                    $fieldNum++;
                    break;
            }
        }
        $query = substr($query, 0, -2) . ')';
    } elseif ($action == 'update') {
        $query = 'update ' . $table . ' set ';
        $fieldNum = 1;
        while (list($columns, $value) = each($data)) {
            switch ((string)$value) {
                case 'now()':
                    $query .= $columns . ' = now(), ';
                    break;
                case 'null':
                    $query .= $columns .= ' = null, ';
                    break;
                default:
                    $query .= $columns . ' = :PERFORM_'.$fieldNum.'_FIELD, ';
                    $paramData[':PERFORM_'.$fieldNum.'_FIELD'] = $value;
                    $fieldNum++;
                    break;
            }
        }
        $query = substr($query, 0, -2) . ' where ' . $parameters;
    }
    $paramData = array_merge($paramData,$paramArray);

    return dbq($query,$paramData);
}
// rewrite below.

function dbnr($db_query) {
    return $db_query->rowCount();
}

function dbs($db_query, $row_number) {
    $db_query->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT, $row_number);
    return ;
}

function dbid() {
    global $mysql_PDO;
    return $mysql_PDO->lastInsertId();
}
