<?php
function debugTimer($startstop,$name){
    if(!SHOW_DEBUG) {
        return;
    }
    global $debugbar;
    $fileTrace = debug_backtrace();
    $calledFile = "".basename($fileTrace[0]['file']).'('.$fileTrace[0]['line'].') : ';
    switch(strtolower($startstop)){
        case 'start' :
            $debugbar['time']->startMeasure($name, $calledFile.$name);
            break;
        case 'stop' :
            $debugbar['time']->stopMeasure($name);
            break;
    }
};

function debugAdd($message,$pane='messages'){
    if(!SHOW_DEBUG) {
        return;
    }
    global $debugbar;
    $displaymessage = $message;

    $fileTrace = debug_backtrace();
    $backtrace = $fileTrace[0];

    $fh = fopen($backtrace['file'], 'r');
    $line = 0;
    while (++$line <= $backtrace['line']) {
        $code = fgets($fh);
    }
    fclose($fh);

    //preg_match('/'. __FUNCTION__ .'\s*\((.*)\)\s*;/u', $code, $name);
	preg_match('/\$[^,)]*/u', $code, $name);
   // $name = explode(",",$name[0]); // Just want the first arg if many.
    $reference = $name[0];

    $calledFile = "".basename($backtrace['file']).'('.$backtrace['line'].') : ';

    if(is_array($displaymessage) || is_object($displaymessage)){
        //$displaymessage = "<pre class=' hljs php'>".print_r($displaymessage,1)."</pre>";
        //$displaymessage = $debugbar[$pane]->getDataFormatter()->formatVar($displaymessage);
        $displaymessage = (array)$displaymessage;
        $displaymessage['debugbarvar'] = $calledFile.$reference;
        //$displaymessage = "<pre>".print_r($displaymessage,1)."</pre>";
        if(!isset($debugbar[$pane]) || !is_object($debugbar[$pane])){
           $debugbar->addCollector(new DebugBar\DataCollector\MessagesCollector($pane));
        }

        $debugbar[$pane]->info($displaymessage);

    } else {
        $debugbar[$pane]->info($calledFile.$reference." = ".$displaymessage."");
    }
    usleep(1); // Makes them appear in order.
}

function debugEcho(&$subject){
    if(!SHOW_DEBUG) {
        return;
    }
	    $fileTrace = debug_backtrace();
    $backtrace = $fileTrace[0];

    $fh = fopen($backtrace['file'], 'r');
    $line = 0;
    while (++$line <= $backtrace['line']) {
        $code = fgets($fh);
    }
    fclose($fh);

    preg_match('/'. __FUNCTION__ .'\s*\((.*)\)\s*;/u', $code, $name);
    $name = explode(",",$name[1]); // Just want the first arg if many.
    $reference = $name[0];
   
    echo $reference.' - '.$subject."</br>";
}

function debugRenderHead(){
    if(!SHOW_DEBUG) {
        return;
    }
    global $debugbarRenderer;
    echo $debugbarRenderer->renderHead();
}

function debugRender(){
    if(!SHOW_DEBUG) {
        return;
    }
    global $debugbarRenderer;
	$GLOBALS['USER_CONSTANTS'] = get_defined_constants(true)['user']; // Add user defined constants to the request stuff.
    echo $debugbarRenderer->render();
}

// Die but output the phpdebug bar.
function debugDie(){
    if(!SHOW_DEBUG) {
        return;
    }

    echo "<!DOCTYPE html><html><head>";
    debugRenderHead();
    echo "</head><body>";
    debugRender();
    echo "</body></html>";
    die;
}

?>