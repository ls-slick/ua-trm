<?php
/**
 * Created by PhpStorm.
 * User: andyl
 * Date: 28/10/2015
 * Time: 11:53
 */
class eBOS_API extends RestClient
{
    private $usedbasetoken = true;
    private $tokenTime = EBOS_LAST_ACTION;
    private $authenticated = false;
    private $token = EBOS_TOKEN;
    private $debug = false;

    /**
     * @method string eBOS_API() - create the class and log in.
     * @param  string $user   username to be verified.
     * @param  string $pass   password to be verified.
     */
    public function eBOS_API($user,$pass,$debug = false) {
        // use database token?
        $this->authenticated = $this->login($user,$pass);

       // $this->debug = $debug;
    }

    private function getLastAction(){
        if($this->tokenTime < (time() - (60*8))){
            $ebosTokenTimeQuery = dbq("select config_value from configuration where config_key = :CONFKEY",
                [
                    ':CONFKEY' => 'EBOS_LAST_ACTION'
                ]
            );

            $ebosTokenTime = dbf($ebosTokenTimeQuery);
            $this->tokenTime = $ebosTokenTime['config_value'];

        }

        return $this->tokenTime;
    }

    private function setLastAction(){
        if($this->usedbasetoken) {
            $this->tokenTime = time();
            $ebosTokenTimeQuery = dbq("UPDATE configuration SET config_value = :ACTIONTIME WHERE config_key = :CONFKEY",
                [
                    ':ACTIONTIME' => $this->tokenTime,
                    ':CONFKEY' => 'EBOS_LAST_ACTION'
                ]
            );
        }

    }

    private function setDbaseToken(){
        $ebosTokenQuery = dbq("update configuration set config_value = :TOKEN where config_key = :CONFKEY",
            [
                ':TOKEN' => $this->token,
                ':CONFKEY' => 'EBOS_TOKEN'
            ]
        );
    }

/* General Methods */

    /**
     * @method string login() - Authenticate and log in, store auth token.
     * @param  string $user   username to be verified.
     * @param  string $pass   password to be verified.
     * @return  bool
     */
    public function login($user,$pass){

        // use database token?
        if($this->usedbasetoken){
            // use the token in the database if valid.
            if($this->getLastAction() < (time() - (60*9)) ){
                // Timeout - refresh login
                $this->logout();

                // Start new login.
                $endpoint = '/api/v1/authenticate';

                $request = [
                    "email" => $user,
                    "password" => $pass
                ];

                $authRequest = RestClient::post(
                    APIServer.$endpoint,
                    $request);

                $authResponse = json_decode($authRequest->getResponse());

                if($authResponse->authenticated){
                    // $this->accessToken = $authResponse->access_token;
                    $this->token = $authResponse->access_token;
                    $this->setDbaseToken();
                    $this->setLastAction();
                }

                return boolval($authResponse->authenticated);

            } else {

                return true; // Token already valid.

            }
        } else {
            // Start new login.
            $endpoint = '/api/v1/authenticate';

            $request = [
                "email" => $user,
                "password" => $pass
            ];

            $authRequest = RestClient::post(
                APIServer.$endpoint,
                $request);

            $authResponse = json_decode($authRequest->getResponse());

            if($authResponse->authenticated){
                // $this->accessToken = $authResponse->access_token;
                $this->token = $authResponse->access_token;
            }

            return boolval($authResponse->authenticated);
        }

    }

    /**
     * @method string logout() - force the remote session auth token to be destroyed.
     * @return  bool
     */
    public function logout(){
        $endpoint = '/api/v1/logout';

        $authRequest = RestClient::delete(
            APIServer.$endpoint,
            null,
            null,
            null,
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());

        if(isset($authResponse->successful)){
            $this->token = '';  // Leave the token in for testing.

            return boolval($authResponse->successful);
        } else {
            $this->token = '';  // Leave the token in for testing.

            return true;
        }


    }

    /**
     * @method string getThreadsFromEMB() - get thread colour list from an emb file.
     * @param  string $file  full file name and folder
     * @return  array
     */
    function getThreadsFromEMB($file){

        // use wilcom.

        $data_string = '<xml>
                    <files>
                        <file filename="'.$file.'" filecontents="'.base64_encode(file_get_contents($file)).'"/>
                    </files>
                 </xml>' ;

        $api_connect = [
            'appId' => WILCOM_API_APP_ID,
            'appKey' => WILCOM_API_KEY,
            'requestXml' => $data_string
        ];

        $postdata = http_build_query($api_connect);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://ewa.wilcom.com/1.3/api/designInfo');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        $xml_output = simplexml_load_string( $output );
        $json_convert = json_encode((array) $xml_output );
        $json = (array)json_decode( $json_convert,true );
        //return $json;
        $foundColours = [];
        if(isset($json['design_info']['colorways'])){
            foreach($json['design_info']['colorways'] as $colourway){
                foreach($colourway['threads']['thread'] as $threadinfo){
                    if(isset($threadinfo['@attributes'])){
                        if(empty($threadinfo['@attributes']['code'])){
                            break(2);
                        }
                        $foundColours[] = $threadinfo['@attributes']['code'];
                    } else {
                        if(empty($threadinfo['code'])){
                            break(2);
                        }
                        $foundColours[] = $threadinfo['code'];
                    }


                }
            }
        } else {
            return false;
        }

        $stopSequence = [];
        if(isset($json['design_info']['stop_sequence'])){
            foreach($json['design_info']['stop_sequence']['stop_record'] as $stop){
                if(isset($stop['@attributes'])){
                    $stopSequence[] = $foundColours[$stop['@attributes']['color_idx']];
                } else {
                    $stopSequence[] = $foundColours[$stop['color_idx']];
                }
            }
        } else {
            return false;
        }

        return $this->substituteThreads($stopSequence);

    }


    public function substituteThreads($threadArray){

        $threadSubs = [
            '1068' => '1069',
            '1168' => '1169',
            '1079' => '1051',
            '1002' => '1001',
            '1003' => '1001',
            '1004' => '1001',
            '1005' => '1001',
            '1801' => '1001',
            '1134' => '1076',
            '1180' => '1223',
            '1385' => '1384',
            '1133' => '1029',
            '1006' => '1000'
        ];

        $outArray = [];

        foreach($threadArray as $thread){
            $outArray[] = (isset($threadSubs[$thread]) ? $threadSubs[$thread] : $thread);
        }

        return $outArray;
    }

    public function parseDSTHeader($file){

        $fp = fopen($file, 'rb+');
        // Read the header.
        $header = fread($fp, 512);

        $headerArray = array();

        for ($i=0;$i<512;$i++) {
            $var = "";
            if ($header[$i]==':') {
                $valpos=$i+1;
                $varpos=$i-2;
                $var = substr($header,$varpos,2);
                // Need to find the next :
                for ($n=$valpos;$n<512;$n++) {
                    if ($header[$n]==':') {
                        $valend = ($n-$valpos)-3;
                        $headerArray[$var] = substr($header,$valpos,$valend);
                        break;
                    }
                }

            }
        }

        $result = [
            'fullwidth' => $headerArray['+X'] + $headerArray['-X'],
            'mmwidth' => ($headerArray['+X'] + $headerArray['-X']) / 10,
            'fullheight' => $headerArray['+Y'] + $headerArray['-Y'],
            'mmheight' => ($headerArray['+Y'] + $headerArray['-Y']) / 10,
            'totalColours' => $headerArray['CO']+1,
            'stitchCount' => $headerArray['ST']
        ];

        return $result;

    }

    /**
     * @method string findCustomerDesign() - return a single design details.
     * @param  integer $custid  customerID
     * @param  integer $designNumber  designNumber
     * @return  object
     */
    public function findCustomerDesign($custid,$designNumber){

        $designsResponse = $this->getCustomerDesigns($custid,1,9999999); // all designs regardless.
        foreach($designsResponse as $root => $designs){
            foreach($designs as $design) {
                if ($design->number = $designNumber){
                    return $design->id;
                }
            }
        }

        return false;
    }

/* Customer Methods */

    /**
     * @method string getCustomers() - return a list of customers.
     * @return  object
     * GET /api/v1/customers
     */
    public function getCustomers(){
        $endpoint = '/api/v1/customers';

        $authRequest = RestClient::get(
            APIServer.$endpoint,
            null,
            null,
            null,
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string getCustomerDetails($id) - return a single customer details.
     * @param  string $id   customer id
     * @return  object
     * GET /api/v1/customers
     */
    public function getCustomerDetails($id){
        $endpoint = "/api/v1/customers/$id";

        $authRequest = RestClient::get(
            APIServer.$endpoint,
            null,
            null,
            null,
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string getCustomerEmbroideryOrders($id) - return a list of a customers embroidery orders.
     * @param  string $id   customer id
     * @param  array $filter  filter array for orders
     * @return  object
     * GET /api/v1/customers/$id/embroidery_orders;
     */
    public function getCustomerEmbroideryOrders($id,$filter = []){
        $endpoint = "/api/v1/customers/$id/embroidery_orders";
        // TODO  make this handle filters better.  Loop through passed filters and use them in place of default filters
        if(empty($filter)){
            $filter = '{
                "filter": {
                    "status": ["new", "accepted", "progress", "completed", "despatched", "cancelled", "on_query", "on_hold"],
                    "stock_status": ["blank", "all_in", "due_in", "not_in", "part_in", "overdue", "excess"],
                    "date_placed_after": "",
                    "date_placed_before": "",
                    "date_completed_after": "",
                    "date_completed_before": "",
                    "date_despatched_after": "",
                    "date_despatched_before": "",
                    "owner": "all_users",
                    "archived": "hidden",
                    "ss_pick": "included",
                    "custom_icon": [""],
                    "per_page": "20",
                    "page": "1"
                }

            }';
        } else {
            $filter = json_encode($filter);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $filter,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string getCustomerWarehouseOrders($id) - return a list of a customers warehouse orders.
     * @param  string $id   customer id
     * @param  array $filter  filter array for orders
     * @return  object
     * GET /api/v1/customers/$id/warehouse_orders;
     */
    public function getCustomerWarehouseOrders($id,$filter = []){
        $endpoint = "/api/v1/customers/$id/warehouse_orders";
        // TODO  make this handle filters better.  Loop through passed filters and use them in place of default filters
        if(empty($filter)){
            $filter = '{
                "filter": {
                    "status": ["stock_in", "stock_part_in", "stock_despatched", "stock_part_despatched"],
                    "date_placed_after": "",
                    "date_placed_before": "",
                    "owner": "all_users",
                    "archived": "hidden",
                    "per_page": "20",
                    "page": "1"
                }
            }';
        } else {
            $filter = json_encode($filter);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $filter,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string getCustomerArtworkOrders($id) - return a list of a customers artwork orders.
     * @param  string $id   customer id
     * @param  array $filter  filter array for orders
     * @return  object
     * GET /api/v1/customers/$id/artwork_orders;
     */
    public function getCustomerArtworkOrders($id,$filter = []){
        $endpoint = "/api/v1/customers/$id/artwork_orders";
        // TODO  make this handle filters better.  Loop through passed filters and use them in place of default filters
        if(empty($filter)){
            $filter = '{
                  "filter": {
                        "status": ["new", "in_progress", "completed", "sample_order", "awaiting_approval", "approved", "rejected", "response_failure", "cancelled", "on_query", "on_hold"],
                        "date_placed_after": "",
                        "date_placed_before": "",
                        "date_completed_after": "",
                        "date_completed_before": "",
                        "owner": "all_users",
                        "artwork_team_id": "",
                        "archived": "hidden",
                        "per_page": "20",
                        "page": "1"
                   }
            }';
        } else {
            $filter = json_encode($filter);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $filter,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string getCustomerDigitizingOrders($id) - return a list of a customers digitizing orders.
     * @param  string $id   customer id
     * @param  array $filter  filter array for orders
     * @return  object
     * GET /api/v1/customers/$id/digitizing_orders;
     */
    public function getCustomerDigitizingOrders($id,$filter = []){
        $endpoint = "/api/v1/customers/$id/digitizing_orders";
        // TODO  make this handle filters better.  Loop through passed filters and use them in place of default filters
        if(empty($filter)){
            // Default all data.
            $filter = '{
                "filter": {
                    "status": ["new", "in_progress", "completed", "sample_order", "awaiting_approval", "swatch_sent", "requested_swatch", "approved", "rejected", "response_failure", "cancelled", "on_query", "on_hold"],
                    "date_placed_after": "",
                    "date_placed_before": "",
                    "date_completed_after": "",
                    "date_completed_before": "",
                    "owner": "all_users",
                    "digitizing_team_id": "",
                    "archived": "hidden",
                    "per_page": "20",
                    "page": "1"
                }
            }';
        } else {
            $filter = json_encode($filter);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $filter,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string getCustomerDesigns() - return a list of customers.
     * @param  string $id  customer id
     * @param  string $search  customer id
     * @param  int $page  page number
     * @param  int $perPage   results per page.
     * @return  object
     * GET /api/v1/designs?per_page=$perPage&page=$page
     */
    public function getCustomerDesigns($id,$search,$page=1,$perPage=20){
        $endpoint = "/api/v1/customers/$id/designs?query=$search&per_page=$perPage&page=$page";

        $authRequest = RestClient::get(
            APIServer.$endpoint,
            null,
            null,
            null,
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

/* Design Tool Methods */

    /**
     * @method string getDesigns() - return a list of customers.
     * @param  string $search  search term for name or number
     * @param  int $page  page number
     * @param  int $perPage   results per page.
     * @return  object
     * GET /api/v1/designs?per_page=$perPage&page=$page
     */
    public function getDesigns($search='',$page=1,$perPage=20, $showDeleted='false',$modifiedAfter = null,$modifiedBefore = null){
        $endpoint = "/api/v1/designs?query=$search&per_page=$perPage&page=$page&show_deleted=$showDeleted";

        if(!is_null($modifiedAfter)){
            $endpoint .= "&modified_after=".urlencode($modifiedAfter);
        }

        if(!is_null($modifiedBefore)){
            $endpoint .= "&modified_before=".urlencode($modifiedBefore);
        }

        $authRequest = RestClient::get(
            APIServer.$endpoint,
            null,
            null,
            null,
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
	$this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string getDesignDetails() - return a single design details.
     * @param  integer $id  designID
     * @return  object
     * GET /api/v1/designs/$id
     */
    public function getDesignDetails($id){
        $endpoint = "/api/v1/designs/$id";

        $authRequest = RestClient::get(
            APIServer.$endpoint,
            null,
            null,
            null,
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string createDesign() - update an eBOS design.
     * @param  array $designdetail  designDetails
     * @return  object
     * PUT /api/v1/designs/$id
     */
    public function createDesign($designdetail){
        $endpoint = "/api/v1/designs";

        // TODO make this check for blanks.
        if(empty($designdetail)){
            // Nothing to do
            return false;
            // Default all data.
            /*
             $designdetail = [
                 'design' => [
                     'number' => '', # required
                     'name' => 'ANDY_TEST', # required
                     'notes' => '',
                     'threads' => [],
                     'dst' => [ # required
                         'data' => '', # required
                         'filename' => '', # required
                     ],
                 ]
            ];*/
        } else {
            $designdetail = json_encode($designdetail);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $designdetail,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());

        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string updateDesign() - update an eBOS design.
     * @param  integer $id  designID
     * @param   array $designdetail   what needs to change.
     * @return  object
     * PUT /api/v1/designs/$id
     */
    public function updateDesign($id,$designdetail){



        $endpoint = "/api/v1/designs/$id";

        // TODO make this check for blanks.
        if(empty($designdetail)){
            // Nothing to do
            return false;
            // Default all data.
            /*
             $designdetail = [
                 'design' => [
                     'number' => '', # required
                     'name' => 'ANDY_TEST', # required
                     'notes' => '',
                     'threads' => [],
                     'dst' => [ # required
                         'data' => '', # required
                         'filename' => '', # required
                     ],
                 ]
            ];*/
        } else {
            if(!isset($designdetail['design']['jump_stitches'])){
                $designdetail['design']['jump_stitches'] = 'true';
            }
            $designdetail = json_encode($designdetail);
        }

        $authRequest = RestClient::put(
            APIServer.$endpoint,
            $designdetail,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());



        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string deleteDesign() - delete an eBOS design.
     * @param  integer $id  designID
     * @return  object
     * PUT /api/v1/designs/$id
     */
    public function deleteDesign($id){
        $endpoint = "/api/v1/designs/$id";

        // TODO make this check for blanks.

        $authRequest = RestClient::delete(
            APIServer.$endpoint,
            null,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());

        $this->setLastAction();
        return $authResponse;
    }

/* Embroidery Order Methods */

    /**
     * @method string getEmbroideryOrders($id) - return a list of logged in customers embroidery orders.
     * @param  array $filter  filter array for orders
     * @return  object
     * POST /api/v1/embroidery_orders/search
     */
    public function getEmbroideryOrders($filter = []){
        $endpoint = "/api/v1/embroidery_orders/search";
        // TODO  make this handle filters better.  Loop through passed filters and use them in place of default filters
        if(empty($filter)){
            $filter = '{
                "filter": {
                    "status": ["new", "accepted", "progress", "completed", "despatched", "cancelled", "on_query", "on_hold"],
                    "stock_status": ["blank", "all_in", "due_in", "not_in", "part_in", "overdue", "excess"],
                    "date_placed_after": "",
                    "date_placed_before": "",
                    "date_completed_after": "",
                    "date_completed_before": "",
                    "date_despatched_after": "",
                    "date_despatched_before": "",
                    "owner": "all_users",
                    "archived": "hidden",
                    "ss_pick": "included",
                    "custom_icon": [""],
                    "per_page": "20",
                    "page": "1"
                }

            }';
        } else {
            $filter = json_encode($filter);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $filter,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string getEmbroideryOrderDetails() - return a single design details.
     * @param  integer $id  orderID
     * @return  object
     * GET /api/v1/embroidery_orders/$id
     */
    public function getEmbroideryOrderDetails($id){
        $endpoint = "/api/v1/embroidery_orders/$id";

        $authRequest = RestClient::get(
            APIServer.$endpoint,
            null,
            null,
            null,
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string createEmbroideryOrder() - Create an order and return a single Embroidery order details.
     * @param  string $emailaddress  email address of main order user.
     * @param  integer $customerID  customer id if creating an order for a customer as an admin user.
     * @return  object
     * POST /api/v1/embroidery_orders
     */
    public function createEmbroideryOrder($emailaddress,$customerID=null){
        $endpoint = "/api/v1/embroidery_orders";
        if(empty($emailaddress)){
            //must have an email.
           return false;
        } else {
            $orderdetails['order']['email'] = $emailaddress;
            if(!is_null($customerID)){
                // doing this for another customer
                $orderdetails['order']['customer_id'] = $customerID;
            }

            $orderdetails = json_encode($orderdetails);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $orderdetails,
            null,
            null,
            'application/json',
            $this->token);
        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }
	
	/**
     * @method string createEmbroideryOrderDecoration() - Create a decoration on an order and return a single Embroidery order details.
     * @param  string $orderid Embroidery order id.
     * @param  integer $decorationdetails  Decoration details
     * @return object
     * POST  /api/v1/embroidery_orders/:embroidery_order_id/decorations
     */
    public function createEmbroideryOrderDecoration($orderid,$decorationdetails){
        $endpoint = "/api/v1/embroidery_orders/$orderid/decorations";
		$itemdetails = [];
        if(empty($orderid) || empty($decorationdetails)){            
           return false;
        } else {
            $itemdetails['decoration'] = $decorationdetails;           
            $itemdetails = json_encode($itemdetails);
        }
		echo APIServer.$endpoint;
		echo $this->token;
		print_r($itemdetails);
        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $itemdetails,
            null,
            null,
            'application/json',
            $this->token);
			
			
        $authResponse = json_decode($authRequest->getResponse());

        return $authResponse;
    }
	
	/**
     * @method string updateEmbroideryOrderDecoration() - Update a decoration on an order and return a single Embroidery order details.
     * @param  string $id Embroidery order id.
     * @param  integer $decorationdetails  Decoration details
     * @return object
     * POST  /api/v1/embroidery_orders/:embroidery_order_id/decorations
     */
    public function updateEmbroideryOrderDecoration($orderid,$decorationid,$decorationdetails){
        $endpoint = "/api/v1/embroidery_orders/$orderid/decorations/$decorationid";
		$itemdetails = [];
        if(empty($orderid) || empty($decorationid) || empty($decorationdetails)){
            //must have an email.
           return false;
        } else {
            $itemdetails['decoration'] = $decorationdetails;

            $itemdetails = json_encode($itemdetails);
        }

        $authRequest = RestClient::put(
            APIServer.$endpoint,
            $itemdetails,
            null,
            null,
            'application/json',
            $this->token);
        $authResponse = json_decode($authRequest->getResponse());

        return $authResponse;
    }

	/**
     * @method string deleteEmbroideryOrderDecoration() - Delete a decoration on an order and return a single Embroidery order details.
     * @param  string $id Embroidery order id.
     * @param  integer $decorationdetails  Decoration details
     * @return object
     * POST  /api/v1/embroidery_orders/:embroidery_order_id/decorations
     */
    public function deleteEmbroideryOrderDecoration($orderid,$decorationid){
        $endpoint = "/api/v1/embroidery_orders/$orderid/decorations/$decorationid";
		$itemdetails = [];
        if(empty($orderid) || empty($decorationid)){            
           return false;
        } 

        $authRequest = RestClient::delete(
            APIServer.$endpoint,
            null,
            null,
            null,
            'application/json',
            $this->token);
        $authResponse = json_decode($authRequest->getResponse());

        return $authResponse;
    }
	
    /**
     * @method string updateEmbroideryOrderStatus() - Create an order and return a single Embroidery order details.
     * @param  integer $id  orderID
     * @param  string $status  New Status
     * @return  bool
     * PUT /api/v1/embroidery_orders/:id/change_status
     */
    public function updateEmbroideryOrderStatus($id,$status,$notifiy = true,$udr = true){
        $endpoint = "/api/v1/embroidery_orders/$id/change_status";
        if(empty($status)){
            //must have an email.
            return false;
        }
        $statusArray['status'] = $status;
        $statusArray['notify_customer'] = $notifiy;
        $statusArray['create_udr'] = $udr;

        $statusArray = json_encode($statusArray);

        $authRequest = RestClient::put(
            APIServer.$endpoint,
            $statusArray,
            null,
            null,
            'application/json',
            $this->token);
        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        if(isset($authResponse->successful) && $authResponse->successful == true){
            return true;
        }
        return $authResponse;
    }

    /**
     * @method string updateEmbroideryOrder() - Update an order and return a single Embroidery order details.
     * @param  integer $id  orderID
     * @param  array $fields  New fields to be updated.
     * @return  mixed
     * PUT /api/v1/embroidery_orders/:id
     */
    public function updateEmbroideryOrder($id,$fields){
        $endpoint = "/api/v1/embroidery_orders/$id";
        if(empty($fields) || empty($id)){
            //must have something to do.
            return false;
        }

        $orderObject = [];
        foreach($fields as $key => $value){
            $orderObject['order'][$key] = $value;
        }

        $orderObject = json_encode($orderObject);

        $authRequest = RestClient::put(
            APIServer.$endpoint,
            $orderObject,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

/* Digitizing Order Methods */

    /**
     * @method string getDigitizingOrders($filter) - return a list of logged in customers digitizing orders.
     * @param  array $filter  filter array for orders
     * @return  object
     * POST /api/v1/digitizing_orders/search
     */
    public function getDigitizingOrders($filter = []){
        $endpoint = "/api/v1/digitizing_orders/search";
        // TODO  make this handle filters better.  Loop through passed filters and use them in place of default filters
        if(empty($filter)){
            $filter = '{
                "filter": {
                    "status": ["new", "in_progress", "completed", "sample_order", "awaiting_approval", "swatch_sent", "requested_swatch", "approved", "rejected", "response_failure", "cancelled", "on_query", "on_hold"],
                    "date_placed_after": "",
                    "date_placed_before": "",
                    "date_completed_after": "",
                    "date_completed_before": "",
                    "owner": "all_users",
                    "digitizing_team_id": "",
                    "archived": "hidden",
                    "per_page": "20",
                    "page": "1"
                }
            }';
        } else {
            $filter = json_encode($filter);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $filter,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string getDigitizingOrderDetails($id) - return a single design details.
     * @param  integer $id  orderID
     * @return  object
     * GET /api/v1/digitizing_orders/$id
     */
    public function getDigitizingOrderDetails($id){
        $endpoint = "/api/v1/digitizing_orders/$id";

        $authRequest = RestClient::get(
            APIServer.$endpoint,
            null,
            null,
            null,
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string createDigitizingOrder() - Create an order and return a single Digitizing order details.
     * @param  string $type  type of order.  new, amendment or quote
     * @param  string $emailaddress  email address of main order contact.
     * @param  integer $customerID  customer id if creating an order as admin user
     * @return  object
     * POST /api/v1/digitizing_orders
     */
    public function createDigitizingOrder($type,$emailaddress,$customerID=null){
        $endpoint = "/api/v1/digitizing_orders";
        if(empty($type) || empty($emailaddress)){
            //must have an email and type
            return false;
        } else {
            $orderdetails['order']['type'] = $type;
            $orderdetails['order']['email'] = $emailaddress;
            if(!is_null($customerID)){
                // doing this for another customer
                $orderdetails['order']['customer_id'] = $customerID;
            }

            $orderdetails = json_encode($orderdetails);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $orderdetails,
            null,
            null,
            'application/json',
            $this->token);
        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string updateDigitizingOrderStatus() - Create an order and return a single Embroidery order details.
     * @param  integer $id  orderID
     * @param  string $status  New Status
     * @return  bool
     * PUT /api/v1/digitizing_orders/:id/change_status
     */
    public function updateDigitizingOrderStatus($id,$status,$notifiy = true){
        $endpoint = "/api/v1/digitizing_orders/$id/change_status";

        if(empty($status)){
            //must have an email.
            return false;
        }
        $statusArray['status'] = $status;
        $statusArray['notify_customer'] = $notifiy;

        $statusArray = json_encode($statusArray);

        $authRequest = RestClient::put(
            APIServer.$endpoint,
            $statusArray,
            null,
            null,
            'application/json',
            $this->token);
        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        if(isset($authResponse->successful) && $authResponse->successful == true){
            return true;
        }
        return $authResponse;
    }

    /**
     * @method string updateDigitizingOrder() - update an order and return a single Digitizing order details.
     * @param  integer $id  orderID
     * @param  array $fields  New fields to be updated.
     * @return  mixed
     * PUT /api/v1/digitizing_orders/:id
     */
    public function updateDigitizingOrder($id,$fields){
        $endpoint = "/api/v1/digitizing_orders/$id";
        if(empty($fields) || empty($id)){
            //must have something to do.
            return false;
        }

        $orderObject = [];
        foreach($fields as $key => $value){
            $orderObject['order'][$key] = $value;
        }

        $orderObject = json_encode($orderObject);

        $authRequest = RestClient::put(
            APIServer.$endpoint,
            $orderObject,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string rejectDigitizingOrder() - Reject a digitizing order with a reason.
     * @param  integer $id  orderID
     * @param  string $reason  reason for rejecting.
     * @return  mixed
     * PUT /api/v1/digitizing_orders/:id/wrong_design
     */
    public function rejectDigitizingOrder($id,$reason){
        $endpoint = "/api/v1/digitizing_orders/$id/wrong_design";
        if(empty($reason) || empty($id)){
            //must have something to do.
            return false;
        }

        $orderObject = [];

        $orderObject['reason'] = $reason;

        $orderObject = json_encode($orderObject);

        $authRequest = RestClient::put(
            APIServer.$endpoint,
            $orderObject,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

/* Artwork Order Methods */

    /**
     * @method string getArtworkOrders($filter) - return a list of logged in customers artwork orders.
     * @param  array $filter  filter array for orders
     * @return  object
     * POST /api/v1/artwork_orders/search
     */
    public function getArtworkOrders($filter = []){
        $endpoint = "/api/v1/artwork_orders/search";
        // TODO  make this handle filters better.  Loop through passed filters and use them in place of default filters
        if(empty($filter)){
            $filter = '{
                "filter": {
                    "status": ["new", "in_progress", "completed", "sample_order", "awaiting_approval", "approved", "rejected", "response_failure", "cancelled", "on_query", "on_hold"],
                    "date_placed_after": "",
                    "date_placed_before": "",
                    "date_completed_after": "",
                    "date_completed_before": "",
                    "owner": "all_users",
                    "digitizing_team_id": "",
                    "archived": "hidden",
                    "per_page": "20",
                    "page": "1"
                }
            }';
        } else {
            $filter = json_encode($filter);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $filter,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string getArtworkOrderDetails() - return a single artwork order details.
     * @param  integer $id  orderID
     * @return  object
     * GET /api/v1/artwork_orders/$id
     */
    public function getArtworkOrderDetails($id){
        $endpoint = "/api/v1/artwork_orders/$id";

        $authRequest = RestClient::get(
            APIServer.$endpoint,
            null,
            null,
            null,
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string createArtworkOrder() - Create an order and return a single Artwork order details.
     * @param  integer $type  type of order  -  new, amendment or quote
     * @param  integer $emailaddress  email address of main order contact
     * @param   integer $customerID  customer id if creating an order as admin user
     * @return  object
     * POST /api/v1/artwork_orders
     */
    public function createArtworkOrder($type,$emailaddress,$customerID=null){
        $endpoint = "/api/v1/artwork_orders";
        if(empty($type) || empty($emailaddress)){
            //must have an email and type
            return false;
        } else {
            $orderdetails['order']['type'] = $type;
            $orderdetails['order']['email'] = $emailaddress;
            if(!is_null($customerID)){
                // doing this for another customer
                $orderdetails['order']['customer_id'] = $customerID;
            }

            $orderdetails = json_encode($orderdetails);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $orderdetails,
            null,
            null,
            'application/json',
            $this->token);
        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string updateArtworkOrderStatus() - Create an order and return a single Embroidery order details.
     * @param  integer $id  orderID
     * @param  string $status  New Status
     * @return  bool
     * PUT /api/v1/artwork_orders/:id/change_status
     */
    public function updateArtworkOrderStatus($id,$status,$notifiy = true){
        $endpoint = "/api/v1/artwork_orders/$id/change_status";

        if(empty($status)){
            //must have an email.
            return false;
        }
        $statusArray['status'] = $status;
        $statusArray['notify_customer'] = $notifiy;

        $statusArray = json_encode($statusArray);

        $authRequest = RestClient::put(
            APIServer.$endpoint,
            $statusArray,
            null,
            null,
            'application/json',
            $this->token);
        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        if(isset($authResponse->successful) && $authResponse->successful == true){
            return true;
        }
        return $authResponse;
    }

    /**
     * @method string updateArtworkOrder() - update an order and return a single Digitizing order details.
     * @param  integer $id  orderID
     * @param  array $fields  New fields to be updated.
     * @return  mixed
     * PPUT /api/v1/artwork_orders/:id
     */
    public function updateArtworkOrder($id,$fields){
        $endpoint = "/api/v1/artwork_orders/$id";

        if(empty($fields) || empty($id)){
            //must have something to do.
            return false;
        }

        $orderObject = [];
        foreach($fields as $key => $value){
            $orderObject['order'][$key] = $value;
        }

        $orderObject = json_encode($orderObject);

        $authRequest = RestClient::put(
            APIServer.$endpoint,
            $orderObject,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

/* Warehouse Order Methods */

    /**
     * @method string getWarehouseOrders($filter) - return a list of logged in customers artwork orders.
     * @param  array $filter  filter array for orders
     * @return  object
     * POST /api/v1/warehouse_orders/search
     */
    public function getWarehouseOrders($filter = []){
        $endpoint = "/api/v1/warehouse_orders/search";
        // TODO  make this handle filters better.  Loop through passed filters and use them in place of default filters
        if(empty($filter)){
            $filter = '{
                "filter": {
                    "customer_id": "",
                    "status": ["stock_in", "stock_part_in", "stock_despatched", "stock_part_despatched"],
                    "date_placed_after": "",
                    "date_placed_before": "",
                    "owner": "all_users",
                    "archived": "hidden",
                    "per_page": "20",
                    "page": "1"
                }
            }';
        } else {
            $filter = json_encode($filter);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $filter,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string getWarehouseOrderDetails() - return a single warehouse order details.
     * @param  integer $id  orderID
     * @return  object
     * GET /api/v1/warehouse_orders/$id
     */
    public function getWarehouseOrderDetails($id){
        $endpoint = "/api/v1/warehouse_orders/$id";

        $authRequest = RestClient::get(
            APIServer.$endpoint,
            null,
            null,
            null,
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string createWarehouseOrder() - Create an order and return a single Warehouse order details.
     * @param  integer $id  orderID
     * @return  object
     * POST /api/v1/warehouse_orders
     */
    public function createWarehouseOrder($expectedQty,$qty,$location,$customerID=null,$details=null){
        $endpoint = "/api/v1/warehouse_orders";
        if(empty($expectedQty) || empty($qty) || empty($location)){
            //must have an email and type
            return false;
        } else {
            $orderdetails['order']['box_qty'] = $qty;
            $orderdetails['order']['expected_box_qty'] = $expectedQty;
            $orderdetails['order']['location_name'] = $location;
            if(!is_null($customerID)){
                // doing this for another customer
                $orderdetails['order']['customer_id'] = $customerID;
            }

			if(!is_null($details)){
				$orderdetails['order'] = array_merge($orderdetails['order'],$details);
			}
			
            $orderdetails = json_encode($orderdetails);
        }

        $authRequest = RestClient::post(
            APIServer.$endpoint,
            $orderdetails,
            null,
            null,
            'application/json',
            $this->token);
        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

    /**
     * @method string updateWarehouseOrderStatus() - Create an order and return a single Embroidery order details.
     * @param  integer $id  orderID
     * @param  string $status  New Status
     * @return  bool
     * PUT /api/v1/warehouse_orders/:id/change_status
     */
    public function updateWarehouseOrderStatus($id,$boxes){
        $endpoint = "/api/v1/warehouse_orders/$id/change_status";
        if(empty($boxes)){
            //must have an email.
            return false;
        }
        $boxesObject['boxes'] = $boxes;
        $boxesObject = json_encode($boxesObject);

        $authRequest = RestClient::put(
            APIServer.$endpoint,
            $boxesObject,
            null,
            null,
            'application/json',
            $this->token);
        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        if(isset($authResponse->successful) && $authResponse->successful == true){
            return true;
        }
        return $authResponse;
    }

    /**
     * @method string updateWarehouseOrder() - update an order and return a single Digitizing order details.
     * @param  integer $id  orderID
     * @param  array $fields  New fields to be updated.
     * @return  mixed
     * PPUT /api/v1/warehouse_orders/:id
     */
    public function updateWarehouseOrder($id,$fields){
        $endpoint = "/api/v1/warehouse_orders/$id";

        if(empty($fields) || empty($id)){
            //must have something to do.
            return false;
        }

        $orderObject = [];
        foreach($fields as $key => $value){
            $orderObject['order'][$key] = $value;
        }

        $orderObject = json_encode($orderObject);

        $authRequest = RestClient::put(
            APIServer.$endpoint,
            $orderObject,
            null,
            null,
            'application/json',
            $this->token);

        $authResponse = json_decode($authRequest->getResponse());
        $this->setLastAction();
        return $authResponse;
    }

}


/**
 * Class RestClient
 * Wraps HTTP calls using cURL, aimed for accessing and testing RESTful webservice.
 * By Diogo Souza da Silva <manifesto@manifesto.blog.br>
 */
class RestClient {

    private $curl ;
    private $url ;
    private $response ="";
    private $headers = array();

    private $method="GET";
    private $params=null;
    private $contentType = null;
    private $accessToken = null;
    private $file =null;

    private $lastLogID = null;

    /**
     * Private Constructor, sets default options
     */
    private function __construct() {
        $this->curl = curl_init();
        curl_setopt($this->curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($this->curl,CURLOPT_AUTOREFERER,true); // This make sure will follow redirects
        curl_setopt($this->curl,CURLOPT_FOLLOWLOCATION,true); // This too
        curl_setopt($this->curl,CURLOPT_HEADER,true); // THis verbose option for extracting the headers
    }


    private function ebosRequestLog(){

        dbq("INSERT into ebos_calls (
              ebos_call_method,
              ebos_call_endpoint,
              ebos_call_request_time,
              ebos_call_request )
          VALUE (
              :EBOSCALLMETHOD,
              :EBOSCALLENDPOINT,
              NOW(),
              :EBOSCALLREQUEST
          )",
            [
                ':EBOSCALLMETHOD' => $this->method,
                ':EBOSCALLENDPOINT' => $this->url,
                ':EBOSCALLREQUEST' => $this->params // Already json here.
            ]
        );

        $this->lastLogID = dbid();
    }

    private function ebosResponseLog(){

        dbq("UPDATE ebos_calls set ebos_call_response = :RESPONSE, ebos_call_response_time = NOW() where ebos_call_id = :CALLID",
            [
                ':RESPONSE' => $this->response, // Already json
                ':CALLID' => $this->lastLogID
            ]
        );

    }


    /**
     * Execute the call to the webservice
     * @return RestClient
     */
    public function execute() {

        $this->ebosRequestLog();

        if($this->method === "POST") {
            curl_setopt($this->curl,CURLOPT_POST,true);
            curl_setopt($this->curl,CURLOPT_POSTFIELDS,$this->params);
        } else if($this->method == "GET"){
            curl_setopt($this->curl,CURLOPT_HTTPGET,true);
            $this->treatURL();
        } else if($this->method === "PUT") {
            curl_setopt($this->curl,CURLOPT_PUT,true);
            $this->treatURL();
            $this->file = tmpFile();
            fwrite($this->file,$this->params);
            fseek($this->file,0);
            curl_setopt($this->curl,CURLOPT_INFILE,$this->file);
            curl_setopt($this->curl,CURLOPT_INFILESIZE,strlen($this->params));
        } else {
            curl_setopt($this->curl,CURLOPT_CUSTOMREQUEST,$this->method);
        }

        $sendHeaders = [];
        // Change for header token.
        if($this->contentType != null) {
            $sendHeaders[] = "Content-Type: ".$this->contentType;
        }

        if($this->accessToken != null) {
            $sendHeaders[] = "Access-Token: ".$this->accessToken;
            //curl_setopt($this->curl,CURLOPT_HTTPHEADER,array("Content-Type: ".$this->contentType));
        }

        if(!empty($sendHeaders)){
            curl_setopt($this->curl,CURLOPT_HTTPHEADER,$sendHeaders);
        }
       
        curl_setopt($this->curl,CURLOPT_URL,$this->url);
        $r = curl_exec($this->curl);

      
        $this->treatResponse($r); // Extract the headers and response

        $this->ebosResponseLog();

        return $this ;
    }

    /**
     * Treats URL
     */
    private function treatURL(){
        if(is_array($this->params) && count($this->params) >= 1) { // Transform parameters in key/value pars in URL
            if(!strpos($this->url,'?'))
                $this->url .= '?' ;
            foreach($this->params as $k=>$v) {
                $this->url .= "&".urlencode($k)."=".urlencode($v);
            }
        }
        return $this->url;
    }

    /*
     * Treats the Response for extracting the Headers and Response
     */
    private function treatResponse($r) {
        if($r == null or strlen($r) < 1) {
            return;
        }
        $parts  = explode("\n\r",$r); // HTTP packets define that Headers end in a blank line (\n\r) where starts the body
        while(preg_match('@HTTP/1.[0-1] 100 Continue@',$parts[0]) or preg_match("@Moved@",$parts[0])) {
            // Continue header must be bypass
            for($i=1;$i<count($parts);$i++) {
                $parts[$i - 1] = trim($parts[$i]);
            }
            unset($parts[count($parts) - 1]);
        }
        preg_match("@Content-Type: ([a-zA-Z0-9-]+/?[a-zA-Z0-9-]*)@",$parts[0],$reg);// This extract the content type
        $this->headers['content-type'] = $reg[1];
        preg_match("@HTTP/1.[0-1] ([0-9]{3}) ([a-zA-Z ]+)@",$parts[0],$reg); // This extracts the response header Code and Message
        $this->headers['code'] = $reg[1];
        $this->headers['message'] = $reg[2];
        $this->response = "";
        for($i=1;$i<count($parts);$i++) {//This make sure that exploded response get back togheter
            if($i > 1) {
                $this->response .= "\n\r";
            }
            $this->response .= $parts[$i];
        }
    }

    /*
     * @return array
     */
    public function getHeaders() {
        return $this->headers;
    }

    /*
     * @return string
     */
    public function getResponse() {
        return $this->response ;
    }

    /*
     * HTTP response code (404,401,200,etc)
     * @return int
     */
    public function getResponseCode() {
        return (int) $this->headers['code'];
    }

    /*
     * HTTP response message (Not Found, Continue, etc )
     * @return string
     */
    public function getResponseMessage() {
        return $this->headers['message'];
    }

    /*
     * Content-Type (text/plain, application/xml, etc)
     * @return string
     */
    public function getResponseContentType() {
        return $this->headers['content-type'];
    }

    /**
     * This sets that will not follow redirects
     * @return RestClient
     */
    public function setNoFollow() {
        curl_setopt($this->curl,CURLOPT_AUTOREFERER,false);
        curl_setopt($this->curl,CURLOPT_FOLLOWLOCATION,false);
        return $this;
    }

    /**
     * This closes the connection and release resources
     * @return RestClient
     */
    public function close() {
        curl_close($this->curl);
        $this->curl = null ;
        if($this->file !=null) {
            fclose($this->file);
        }
        return $this ;
    }

    /**
     * Sets the URL to be Called
      * @param $url
     * @return RestClient
     */
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }

    /**
     * Set the Content-Type of the request to be send
     * Format like "application/xml" or "text/plain" or other
     * @param string $contentType
     * @return RestClient
     */
    public function setContentType($contentType) {
        $this->contentType = $contentType;
        return $this;
    }

    /**
     * Set the Access-Token of the request to be send
     * @param string $accessToken
     * @return bool
     */
    public function setAccessToken($accessToken) {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * Set the Credentials for BASIC Authentication
     * @param string $user
     * @param string $pass
     * @return RestClient
     */
    public function setCredentials($user,$pass) {
        if($user != null) {
            curl_setopt($this->curl,CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
            curl_setopt($this->curl,CURLOPT_USERPWD,"{$user}:{$pass}");
        }
        return $this;
    }

    /**
     * Set the Request HTTP Method
     * For now, only accepts GET and POST
     * @param string $method
     * @return RestClient
     */
    public function setMethod($method) {
        $this->method=$method;
        return $this;
    }

    /**
     * Set Parameters to be send on the request
     * It can be both a key/value par array (as in array("key"=>"value"))
     * or a string containing the body of the request, like a XML, JSON or other
     * Proper content-type should be set for the body if not a array
     * @param mixed $params
     * @return RestClient
     */
    public function setParameters($params) {
        $this->params=$params;

//        if($this->debug){
//            print_r($params);
//        }

        return $this;
    }

    /**
     * Creates the RESTClient
     * @param string $url=null [optional]
     * @return RestClient
     */
    public static function createClient($url=null) {
        $client = new RestClient ;
        if($url != null) {
            $client->setUrl($url);
        }
        return $client;
    }

    /**
     * Convenience method wrapping a commom POST call
     * @param string $url
      * @param mixed $params
      * @param string $user=null [optional]
      * @param string $pwd=null [optional]
     * @param string $contentType="multpary/form-data" [optional] commom post (multipart/form-data) as default
     * @param string $accessToken=null [optional]
     * @return RestClient
     */
    public static function post($url,$params=null,$user=null,$pwd=null,$contentType="multipart/form-data",$accessToken=null) {
        return self::call("POST",$url,$params,$user,$pwd,$contentType,$accessToken);
    }

    /**
     * Convenience method wrapping a commom PUT call
     * @param string $url
     * @param string $body
     * @param string $user=null [optional]
      * @param string $pwd=null [optional]
     * @param string $contentType=null [optional]
      * @param string $accessToken=null [optional]
     * @return RestClient
     */
    public static function put($url,$body,$user=null,$pwd=null,$contentType=null, $accessToken=null) {
        return self::call("PUT",$url,$body,$user,$pwd,$contentType,$accessToken);
    }

    /**
     * Convenience method wrapping a commom GET call
     * @param string $url
      * @param array $params
     * @param string $user=null [optional]
      * @param string $pwd=null [optional]
      * @param string $accessToken=null [optional]
     * @return RestClient
     */
    public static function get($url,array $params=null,$user=null,$pwd=null,$accessToken=null) {
        return self::call("GET",$url,$params,$user,$pwd,null,$accessToken);
    }

    /**
     * Convenience method wrapping a commom delete call
     * @param string $url
      * @param array $params
      * @param string $user=null [optional]
      * @param string $pwd=null [optional]
      * @param string $accessToken=null [optional]
     * @return RestClient
     */
    public static function delete($url,array $params=null,$user=null,$pwd=null,$contentType=null,$accessToken=null) {
        return self::call("DELETE",$url,$params,$user,$pwd,$contentType,$accessToken);
    }

    /**
     * Convenience method wrapping a commom custom call
     * @param string $method
     * @param string $url
     * @param string $body
     * @param string $user=null [optional]
      * @param string $pwd=null [optional]
     * @param string $contentType=null [optional]
     * @param string $accessToken=null [optional]
     * @return RestClient
     */
    public static function call($method,$url,$body,$user=null,$pwd=null,$contentType=null,$accessToken=null) {

        return self::createClient($url)
            ->setParameters($body)
            ->setMethod($method)
            ->setCredentials($user,$pwd)
            ->setContentType($contentType)
            ->setAccessToken($accessToken)
            ->execute()
            ->close();
    }
}
