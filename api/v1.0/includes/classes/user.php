<?php
/**
 * Created by PhpStorm.
 * User: andyl
 * Date: 10/09/2015
 * Time: 16:55
 */

class user
{

    public $userdetails = null;
    public $userLevel = null;
    private $menuArray = null;

    public function __construct($userarray)
    {
        // put stuff into the class.
        $this->userdetails = $userarray;
        $this->userLevel = $userarray['admin'];
    }

    public function getMenuList(){
        // Work from database.
        if($this->userLevel == 1){
            // Admin User
            $this->menuArray = [
               'Check Order In' => 'check_in_box.php',
               'Process Order' => 'process_order.php',
               'View Orders' => 'order_list.php',
               'Admin' => 'admin.php',
               '' => '',
               'Log Out' => 'logout.php'
            ];
        } elseif ($this->userLevel == 2){
            // DCC User
            $this->menuArray = [
                'View Orders' => 'order_list.php',
                '' => '',
                'Log Out' => 'logout.php'
            ];
        } else{
            $this->menuArray = [
                // Standard User
                'Check Order In' => 'check_in_box.php',
                'Process Order' => 'process_order.php',
                '' => '',
                'Log Out' => 'logout.php'
            ];
        }
        return $this->menuArray;
    }

    function getUserGraphs(){
        if($this->userLevel != 0) {
            // All except machinists.
            ?>
            <div>
                <script src="./style/highcharts/highcharts.js"></script>
                <script src="./style/highcharts/highcharts-more.js"></script>
                <script src="./style/highcharts/modules/data.js"></script>
                <script src="./style/highcharts/modules/solid-gauge.js"></script>

                <div class="row">

                    <?php
                    $priorityOrdersQuery = dbq("SELECT count(*) AS num_orders FROM `order_header` oh where oh.order_priority = 1 and order_status_id != 5");
                    $priorityOrders = dbf($priorityOrdersQuery);
                    //$priorityOrders['num_orders'] += 10;
                    $queryOrdersQuery = dbq("SELECT count(*) AS num_orders FROM `order_header` oh where oh.order_query = 1 and order_status_id != 5");
                    $queryOrders = dbf($queryOrdersQuery);
                    ?>

                    <div class='col-xs-12 col-md-6' id="container-priority" data-title="Number In Priority">
                    </div>
                    <div class='col-xs-12 col-md-6' id="container-query" data-title="Number In Query">
                    </div>
                    <script>
                        var gaugeOptions = {

                            chart: {
                                type: 'solidgauge'
                            },

                            title: null,

                            pane: {
                                center: ['50%', '85%'],
                                size: '100%',
                                startAngle: -90,
                                endAngle: 90,
                                background: {
                                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                                    innerRadius: '60%',
                                    outerRadius: '100%',
                                    shape: 'arc'
                                }
                            },

                            tooltip: {
                                enabled: false
                            },

                            // the value axis
                            yAxis: {
                                stops: [
                                    [0.1, '#55BF3B'], // green
                                    [0.6, '#DDDF0D'], // yellow
                                    [0.8, '#DF5353'] // red
                                ],
                                lineWidth: 0,
                                minorTickInterval: null,
                                tickPixelInterval: null,
                                tickWidth: 0,
                                title: {
                                    y: -100
                                },
                                labels: {
                                    y: 16
                                }
                            },

                            plotOptions: {
                                solidgauge: {
                                    dataLabels: {
                                        y: 5,
                                        borderWidth: 0,
                                        useHTML: true
                                    }
                                }
                            }
                        };

                        // The priority gauge
                        $('#container-priority').highcharts(Highcharts.merge(gaugeOptions, {
                            yAxis: {
                                min: 0,
                                max: 10,
                                title: {
                                    text: 'Number In Priority'
                                }
                            },

                            credits: {
                                enabled: false
                            },

                            series: [{
                                name: 'Orders',
                                data: [<?php echo $priorityOrders['num_orders']; ?>],
                                dataLabels: {
                                    format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                                    '<span style="font-size:12px;color:silver">orders</span></div>'
                                },
                                tooltip: {
                                    valueSuffix: ' orders'
                                }
                            }]

                        }));


                        // The Query gauge
                        $('#container-query').highcharts(Highcharts.merge(gaugeOptions, {
                            yAxis: {
                                min: 0,
                                max: 20,
                                title: {
                                    text: 'Number In Query'
                                }
                            },

                            credits: {
                                enabled: false
                            },

                            series: [{
                                name: 'Orders',
                                data: [<?php echo $queryOrders['num_orders']; ?>],
                                dataLabels: {
                                    format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                                    '<span style="font-size:12px;color:silver">orders</span></div>'
                                },
                                tooltip: {
                                    valueSuffix: ' orders'
                                }
                            }]
                        }));

                    </script>

                    <div class='col-xs-12 col-md-6 highchart' data-title="Orders By Status" data-type="pie">
                        <table class="hidden" id="orders_by_status">
                            <thead>
                            <tr>
                                <th>Status</th><th>Number Of Orders</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            //orders by status
                            $ordersByStatusQuery = dbq("SELECT os.order_status_name, COUNT(order_id) AS num_orders FROM `order_header` oh join order_status os using (order_status_id) GROUP BY order_status_id");
                            while($ordersByStatus = dbf($ordersByStatusQuery)){
                                echo "<tr><td>{$ordersByStatus['order_status_name']}</td><td>{$ordersByStatus['num_orders']}</td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class='col-xs-12 col-md-6 highchart' data-title="Units By Status" data-type="pie">
                        <table class="hidden" id="orders_by_status">
                            <thead>
                            <tr>
                                <th>Status</th><th>Number Of Items</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            //open orders by status
                            $ordersByStatusQuery = dbq("SELECT os.order_status_name, sum(order_item_quantity) AS num_items FROM `order_header` oh join order_status os using (order_status_id) join order_items oi using (order_id) GROUP BY order_status_id");
                            while($ordersByStatus = dbf($ordersByStatusQuery)){
                                echo "<tr><td>{$ordersByStatus['order_status_name']}</td><td>{$ordersByStatus['num_items']}</td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class='col-xs-12 col-md-6 highchart' data-title="Open Orders By Status" data-type="pie">
                        <table class="hidden" id="orders_by_status">
                            <thead>
                            <tr>
                                <th>Status</th><th>Number Of Orders</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            //open orders by status
                            $ordersByStatusQuery = dbq("SELECT os.order_status_name, COUNT(order_id) AS num_orders FROM `order_header` oh join order_status os using (order_status_id) where oh.order_status_id != 5 GROUP BY order_status_id");
                            while($ordersByStatus = dbf($ordersByStatusQuery)){
                                echo "<tr><td>{$ordersByStatus['order_status_name']}</td><td>{$ordersByStatus['num_orders']}</td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

                    <div class='col-xs-12 col-md-6' id="test" data-title="Units By Status" data-type="bar">
                    </div>
                    <script>
                        $('#test').highcharts({

                            chart: {
                                type: 'columnrange',
                                inverted: true
                            },

                            title: {
                                text: 'Order Turn Around Variation by month'
                            },

                            xAxis: {
                                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                            },

                            yAxis: {
                                title: {
                                    text: 'Days'
                                }
                            },

                            plotOptions: {
                                columnrange: {
                                    dataLabels: {
                                        enabled: true,
                                        formatter: function () {
                                            return this.y;
                                        }
                                    }
                                }
                            },

                            legend: {
                                enabled: false
                            },

                            series: [{
                                name: 'Days',
                                data: [
                                    [1, 5],
                                    [1, 4],
                                    [2, 4],
                                    [1, 5],
                                    [2, 6],
                                    [3, 6],
                                    [4, 7],
                                    [2, 6],
                                    [2, 5],
                                    [2, 4],
                                    [1, 5],
                                    [1, 3]
                                ]
                            }]

                        });
                    </script>
                </div>
                <script>
                    $(function () {
                        $('.highchart').each(function(){
                            var table = $(this).find('table:first').prop('id');
                            var title = $(this).data('title');
                            var type = $(this).data('type');

                            $(this).highcharts({
                                data: {
                                    table: table
                                },
                                chart: {
                                    type: type,
                                },
                                title: {
                                    text: title
                                },
                                yAxis: {
                                    allowDecimals: false,
                                    title: {
                                        text: 'Orders'
                                    }
                                }
                            });
                        });


                    });
                </script>


            </div>
            <?php
        }

    }
}