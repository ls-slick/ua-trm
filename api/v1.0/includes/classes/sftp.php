<?php
/**
 * Created by PhpStorm.
 * User: andyl
 * Date: 06/03/2016
 * Time: 12:48
 */

class SFTPConnection
{
    private $connection;
    private $sftp;

    public function __construct($host, $port=22)
    {
        $this->connection = @ssh2_connect($host, $port, array('hostkey'=>'ssh-rsa'));
        if (! $this->connection)
            throw new Exception("Could not connect to $host on port $port.");
    }

    public function login($username, $password, $publicKey=null, $privateKey=null)
    {

        if(!is_null($publicKey)){
            // login using keypairs.
            if (!ssh2_auth_pubkey_file($this->connection, $username,
                $publicKey,
                $privateKey, $password)) {
                throw new Exception("Could not authenticate with username $username " .
                    "and password $password.");
            }
        } else {
            if (!@ssh2_auth_password($this->connection, $username, $password)){
                throw new Exception("Could not authenticate with username $username " .
                    "and password $password.");
            }
        }

        $this->sftp = @ssh2_sftp($this->connection);
        if (!$this->sftp)
            throw new Exception("Could not initialize SFTP subsystem.");
    }

    public function uploadFile($local_file, $remote_file)
    {
        $sftp = $this->sftp;
        $stream = @fopen("ssh2.sftp://$sftp$remote_file", 'w');

        if (! $stream)
            throw new Exception("Could not open file: $remote_file");

        $data_to_send = @file_get_contents($local_file);
        if ($data_to_send === false)
            throw new Exception("Could not open local file: $local_file.");

        if (@fwrite($stream, $data_to_send) === false)
            throw new Exception("Could not send data from file: $local_file.");

        @fclose($stream);
    }
}

?>