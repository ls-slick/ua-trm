<?php
/*
  Example usage:

  $messageStack = new messageStack();
  $messageStack->add('general', 'Error: Error 1', 'error');
  $messageStack->add('general', 'Error: Error 2', 'warning');
  if ($messageStack->size('general') > 0) echo $messageStack->output('general');
*/

  class messageStack {

    var $messages;

// class constructor
    function __construct() {

      $this->messages = array();

      if (isset($_SESSION['messageToStack'])) {
        for ($i=0, $n=sizeof($_SESSION['messageToStack']); $i<$n; $i++) {
          $this->add($_SESSION['messageToStack'][$i]['text'], $_SESSION['messageToStack'][$i]['type']);
        }
        unset($_SESSION['messageToStack']);
      }
    }
                      
// class methods
    function add($message, $type = 'error') {
      switch(true){
        case ($type == 'error') :
          $this->messages[] = array('class' => 'alert-danger', 'title'=> 'Error!', 'text' => $message);
          break;
        case ($type == 'warning') :
          $this->messages[] = array('class' => 'alert-warning', 'title'=> 'Alert!', 'text' => $message);
          break;
        case ($type == 'success') :
          $this->messages[] = array('class' => 'alert-success', 'title'=> 'Success!', 'text' => $message);
          break;
        default :
          $this->messages[] = array('class' => 'alert-info', 'title'=> 'Info!', 'text' => $message);
          break;
      }
    }

    function reset() {
      $this->messages = array();
    }

    function output() {
      $outputString = '';
      for ($i=0, $n=sizeof($this->messages); $i<$n; $i++) {
            $outputString .= '
            <div class="alert '.$this->messages[$i]['class'].'">
              <strong>'.$this->messages[$i]['title'].'</strong> '.$this->messages[$i]['text'].'
            </div>
            ';
      }
      // Now unset them?
      $this->messages = array();
      echo $outputString;
    }

    function size($class) {
      $count = 0;

      for ($i=0, $n=sizeof($this->messages); $i<$n; $i++) {
        if ($this->messages[$i]['class'] == $class) {
          $count++;
        }
      }

      return $count;
    }
  }
?>
