<?php
/*
  Example usage:

  $messageStack = new messageStack();
  $messageStack->add('general', 'Error: Error 1', 'error');
  $messageStack->add('general', 'Error: Error 2', 'warning');
  if ($messageStack->size('general') > 0) echo $messageStack->output('general');
*/

  class statusStack {

    var $messages;

// class constructor
    function __construct() {

      $this->messages = array();

      if (isset($_SESSION['statusToStack'])) {
        for ($i=0, $n=sizeof($_SESSION['statusToStack']); $i<$n; $i++) {
          $this->add($_SESSION['statusToStack'][$i]['text'], $_SESSION['statusToStack'][$i]['type']);
        }
        unset($_SESSION['statusToStack']);
      }
    }
                      
// class methods
    function add($message, $type = 'error') {
      switch(true){
        case ($type == 'error') :
          $this->messages[] = array('class' => 'alert-danger', 'title'=> '<span class="glyphicon glyphicon-remove"></span>', 'text' => $message);
          break;
        case ($type == 'warning') :
          $this->messages[] = array('class' => 'alert-warning', 'title'=> '<span class="glyphicon glyphicon-warning-sign"></span>', 'text' => $message);
          break;
        case ($type == 'success') :
          $this->messages[] = array('class' => 'alert-success', 'title'=> '<span class="glyphicon glyphicon-ok"></span>', 'text' => $message);
          break;
        default :
          $this->messages[] = array('class' => 'alert-info', 'title'=> '<span class="glyphicon glyphicon-asterisk"></span>', 'text' => $message);
          break;
      }
    }

    function reset() {
      $this->messages = array();
    }

    function output() {
      $outputString = '';
      for ($i=0, $n=sizeof($this->messages); $i<$n; $i++) {
            $outputString .= '
            <div id="status" class="alert '.$this->messages[$i]['class'].'">
              '.$this->messages[$i]['title'].' '.$this->messages[$i]['text'].'
            </div>
            ';
      }
      // Now unset them?
      $this->messages = array();
      echo $outputString;
    }

    function size($class) {
      $count = 0;

      for ($i=0, $n=sizeof($this->messages); $i<$n; $i++) {
        if ($this->messages[$i]['class'] == $class) {
          $count++;
        }
      }

      return $count;
    }
  }
?>
