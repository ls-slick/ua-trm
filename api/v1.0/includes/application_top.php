<?php
/**
 * Created by PhpStorm.
 * User: andyl
 * Date: 10/09/2015
 * Time: 16:13
 */

ob_start();
require('configure-new.php');

require(DIR_FUNCTIONS.'database.php');
require(DIR_FUNCTIONS.'general.php');
require(DIR_CLASSES.'user.php');
require(DIR_CLASSES.'message_stack.php');
require(DIR_CLASSES.'status_stack.php');
require(DIR_CLASSES.'ftp.php');
require(DIR_CLASSES.'Mandrill.php');

session_start();

if(!isset($_SESSION['messagestack'])){
    $_SESSION['messagestack'] = new messageStack();
}
if(!isset($_SESSION['statusstack'])){
    $_SESSION['statusstack'] = new statusStack();
}

//Check a user is logged in, if not, send them to login.
/*
if (!isset($_SESSION['session']) || !is_object($_SESSION['session'])) {
    $nologinPages = array('logout.php','login.php','readCSV.php');
    if(!in_array(basename($_SERVER['PHP_SELF']),$nologinPages)){
        header("location:logout.php");
        exit;
    }
    $headerMenu = array();
} else {
    $headerMenu = $_SESSION['session']->getMenuList();
}
*/
// Connect to the database.
$mysql_PDO = new PDO(DB_CONNECT_STRING, DB_USER, DB_PASS);

$mysql_PDO->setAttribute(PDO::ATTR_PERSISTENT, true);
// set utf8
dbq("SET NAMES utf8");


// create configuration defines.
$configurationQuery = dbq('select * from configuration');
while($configurationResult = dbf($configurationQuery)){
    define($configurationResult['config_key'],$configurationResult['config_value']);
}
/*
$dpd = new dpd();
$dpd->connect(DPD_FTP_SERVER);
$dpd->login(DPD_FTP_USER,DPD_FTP_PASS);
*/
