<?php

ini_set('memory_limit',-1);

$allowedIPS = array(
    '87.84.145.101',
    '87.84.145.98',
    '86.0.41.225'  //Andy Home
);

$isAllowed = (in_array($_SERVER['REMOTE_ADDR'],$allowedIPS)) ? TRUE : FALSE;

if(!$isAllowed && 1 == 2){
    header("HTTP/1.0 404 Not Found");
?>
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL <?php echo $_SERVER['REQUEST_URI'];?> was not found on this server.</p>
<p>Additionally, a 404 Not Found
error was encountered while trying to use an ErrorDocument to handle the request.</p>
</body></html>
<?php
    die;
}
/**
 * 
 * 
 * GNU General Public License (Version 2, June 1991) 
 * 
 * This program is free software; you can redistribute 
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details. 
 *
 * @author Corey Maynard <http://coreymaynard.com/>
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */

require('includes/application_top.php');

$entityBody = json_decode(file_get_contents('php://input')); // Read json from post body.
$data = [
	'type' => 404, // Not found as standard
	'messages' => [], // list of messages.
	'data' => [], // Hold the data to be used on the page
	'metrics'=>[
		'time' => microtime(),
		'default_route' => '/main'
	]  // Hold flags and metrics required to progress app.
];

//
//ini_set('memory_limit','256M');
  /*
function __autoload($className) {
	if (file_exists("restObjects/rest_$className.php")) {
		require_once "restObjects/rest_$className.php";
	} else if (file_exists("$className.php")) {
		require_once "$className.php";
	} else {
		user_error("There is no $className", 'E_ERROR');
	}
}   */

if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
	$_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}

	require_once "restServer.php";
   	require_once "restObject.php";

try {

	$API = new restServer($_REQUEST['request'], $_SERVER['HTTP_ORIGIN']);

	/* register objects */
    require_once "restObjects/rest_".$API->getEndpoint().".php";
	$API->register($API->getEndpoint());
    /* Set the output method */
    $API->setOutput('json');
	/* process API */
    $myResponse = $API->processAPI();

	echo $myResponse;
} catch (Exception $e) {
	echo json_encode(array('error' => $e->getMessage()));
}
?>