<?php

/**
 * Rest object example
 * 
 * GNU General Public License (Version 2, June 1991) 
 * 
 * This program is free software; you can redistribute 
 * it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, 
 * or (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class test extends restObject {

	/**
	 * 
	 * @param string $method
	 * @param array $request
	 * @param string $file
	 */
	public function __construct($method, $request = null, $file = null) {
		parent::__construct($method, $request, $file);
	}

	/**
	 *  Resets active products to 1.
	 */
	public function reset() {

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		dbq('delete from products_to_organizations where organization_id in (5627)');
		dbq("insert into products_to_organizations (product_id, organization_id, organization_logo_id) values (509,5627,7891)");
		dbq("insert into products_to_organizations (product_id, organization_id, organization_logo_id) values (510,5627,7891)");
		dbq("insert into products_to_organizations (product_id, organization_id, organization_logo_id) values (511,5627,7891)");
		dbq("insert into products_to_organizations (product_id, organization_id, organization_logo_id) values (512,5627,7891)");
		dbq("insert into products_to_organizations (product_id, organization_id, organization_logo_id) values (513,5627,7891)");
		return true;
	}

	/**
	 *  Removes test active products.
	 */

	public function remove() {

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$id = rand(509,513); // random removal.

		dbq('delete from products_to_organizations where product_id = :ID',[
			':ID' => $id
		]);

		return true;
	}

	public function posttome(){

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [
				[
					'type' => 'danger',
					'message' => 'This is a failure message.'
				],
				[
					'type' => 'success',
					'message' => 'This is a success message.'
				]
			], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime(),
			]  // Hold flags and metrics required to progress app.
		];

		$data['request'] = $this->request;
		return $data;

	}

	public function ftp(){
		$conn_id = ftp_connect(TESCO_FTP_HOST);
		if($conn_id){
			$login_result = ftp_login($conn_id, TESCO_FTP_USER, TESCO_FTP_PASS);

			if($login_result){
				echo 'logged in';
				ftp_pasv($conn_id, true);
				// list?
				$ftplist = ftp_nlist($conn_id,'/');
				return $ftplist;
			} else {
				// error login in.
				return 'no login';
			}
		}
	}
}
