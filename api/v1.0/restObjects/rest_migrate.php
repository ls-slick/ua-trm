<?php

/**
 * Rest object example
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class migrate extends restObject
{

    /**
     *
     * @param string $method
     * @param array $request
     * @param string $file
     */
    public function __construct($method, $request = null, $file = null)
    {
        parent::__construct($method, $request, $file);
    }

	public function processusers()
	{

		ini_set('max_execution_time', 0);

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		// Make organization type list.
		$LEAQuery = dbq("SELECT * FROM local_authorities");
		$LEAID = [];
		while ($LEA = dbf($LEAQuery)) {
			$LEAID[$LEA['local_authority_name']] = $LEA['local_authority_id'];
		}

		// Make LEA list.
		$orgTypeQuery = dbq("SELECT * FROM organization_types");
		$orgType = [];
		while ($orgTypes = dbf($orgTypeQuery)) {
			$orgType[$orgTypes['organization_type_name']] = $orgTypes['organization_type_id'];
		}

		// Make Education Phase list.
		$phaseQuery = dbq("SELECT * FROM education_phases");
		$phaseID = [];
		while ($phase = dbf($phaseQuery)) {
			$phaseID[$phase['education_phase_name']] = $phase['education_phase_id'];
		}

		$data = [];
		// select the old data.
		$uesRippedSchoolsQuery = dbq("SELECT organization_default_address_id, organization_id, school_URN FROM organizations where organization_id > 1");

		$break = false;

		while ($uesRippedSchools = dbf($uesRippedSchoolsQuery)) {
			// return $uesRippedSchools;
			// add users.

			$uesRippedSchoolUsersQuery = dbq("SELECT * FROM ues_school_users usu
 											  JOIN ues_school_data usd on (usu.school_id = usd.id)
 											  WHERE usd.account = :SCHOOLID", [':SCHOOLID' => $uesRippedSchools['school_URN']]);

			while ($uesRippedSchoolUsers = dbf($uesRippedSchoolUsersQuery)) {

				dbq("INSERT INTO users
						(
							user_id,
							user_type_id,
							user_default_address_id,
							user_name,
							user_first_name,
							user_last_name,
							user_email,
							user_password,
							user_avatar,
							user_work_phone,
							user_mobile_phone,
							user_home_phone,
							user_home_email,
							user_title_id,
							user_salutation
						) VALUES (
							:USERID,
							:USERTYPEID,
							:USERDEFAULTADDRESSID,
							:USERNAME,
							:USERFIRSTNAME,
							:USERLASTNAME,
							:USEREMAIL,
							:USERPASSWORD,
							:USERAVATAR,
							:USERWORKPHONE,
							:USERMOBILEPHONE,
							:USERHOMEPHONE,
							:USERHOMEEMAIL,
							:USERTITLEID,
							:USERSALUTATION
						);",
					[
						':USERID' => null,
						':USERTYPEID' => 1, // organization
						':USERDEFAULTADDRESSID' => $uesRippedSchools['organization_default_address_id'],
						':USERNAME' => null,
						':USERFIRSTNAME' => $uesRippedSchoolUsers['first_name'],
						':USERLASTNAME' => $uesRippedSchoolUsers['last_name'],
						':USEREMAIL' => $uesRippedSchoolUsers['email'],
						':USERPASSWORD' => '$2y$10$jnKCsKF1o.j/u3peRASaB.QI/F84gTcshpkqq45K97//NPbouiBgq',
						':USERAVATAR' => null,
						':USERWORKPHONE' => null,
						':USERMOBILEPHONE' => null,
						':USERHOMEPHONE' => null,
						':USERHOMEEMAIL' => null,
						':USERTITLEID' => 1,
						':USERSALUTATION' => $uesRippedSchoolUsers['name_with_title']
					]);

				$userID = dbid();

				dbq("
					insert into users_to_organizations (
						user_id,
						organization_id
					) values (
						:USERID,
						:ORGID
					)
				",[
					':USERID' => $userID,
					':ORGID' => $uesRippedSchools['organization_id']
				]);
			}

			//dbq("UPDATE ues_school_data SET imported = 1 WHERE id = :ID", [':ID' => $uesRippedSchools['id']]);

			if ($break) {
				break;
			}
			//$counter++;

		}

		return ['done'];
	}

	public function uesschools()
    {

        ini_set('max_execution_time', 0);

        $counter = 0;


		$houseLines = [];
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        // Make organization type list.
        $LEAQuery = dbq("SELECT * FROM local_authorities");
        $LEAID = [];
        while ($LEA = dbf($LEAQuery)) {
            $LEAID[$LEA['local_authority_name']] = $LEA['local_authority_id'];
        }

        // Make LEA list.
        $orgTypeQuery = dbq("SELECT * FROM organization_types");
        $orgType = [];
        while ($orgTypes = dbf($orgTypeQuery)) {
            $orgType[$orgTypes['organization_type_name']] = $orgTypes['organization_type_id'];
        }

        // Make Education Phase list.
        $phaseQuery = dbq("SELECT * FROM education_phases");
        $phaseID = [];
        while ($phase = dbf($phaseQuery)) {
            $phaseID[$phase['education_phase_name']] = $phase['education_phase_id'];
        }

        $data = [];
        // select the old data.
        $uesRippedSchoolsQuery = dbq("SELECT * FROM ues_school_data WHERE imported = 0");

        $break = false;

        while ($uesRippedSchools = dbf($uesRippedSchoolsQuery)) {

            $isHouse = false;
            $parentID = null;

			if(!is_null($uesRippedSchools['house_of'])){
				$houseLines[$uesRippedSchools['house_of']] = $uesRippedSchools['house_of']; // keep track of these to set the is house flag later.
			}

            // Find the corresponding school in the main schools database.
            $rootSchoolsQuery = dbq("SELECT * FROM school_data WHERE URN = :ACCOUNT", [':ACCOUNT' => $uesRippedSchools['account']]);
			$livestatus = 3;
			if(!is_null($uesRippedSchools['live_status'])){
				$livestatus = 2; // Activating
			}

            if (dbnr($rootSchoolsQuery) > 0) {
                // found it  Make it active?
                dbq("UPDATE school_data SET school_status_id = ".$livestatus." WHERE URN = :ACCOUNT", [':ACCOUNT' => $uesRippedSchools['account']]);
            } else {
                // not there.  Make a new one?

                // could this be a house??
				if(!is_null($uesRippedSchools['house_of'])){
					// Is a house.  associate it with a original school_data?
					$rootSchoolsQuery = dbq("SELECT * FROM school_data WHERE URN = :ACCOUNT", [':ACCOUNT' => $uesRippedSchools['house_of']]);

				} else {
					// not a house.  Create an entry.

					dbq("INSERT INTO school_data
						(
							id118,
							HEADEMAILDRIECT,
							URN,
							Name,
							LEAName,
							SchoolType,
							Denomination,
							Address1,
							Address2,
							Address3,
							Town,
							County,
							Area,
							Postcode,
							PostcodeSubArea,
							Phone,
							Fax,
							URL,
							Email,
							Emaill118Direct,
							Pupils,
							`School Capacity`,
							LA,
							`Phase of Education`,
							`Type of Establishment`,
							NumberOfPupilsTakingFreeSchoolMeals,
							Nominations,
							school_status_id
						)
					 VALUES
						(
							:ID118,
							:HEADEMAILDRIECT,
							:URN,
							:NAME,
							:LEANAME,
							:SCHOOLTYPE,
							:DENOMINATION,
							:ADDRESS1,
							:ADDRESS2,
							:ADDRESS3,
							:TOWN,
							:COUNTY,
							:AREA,
							:POSTCODE,
							:POSTCODESUBAREA,
							:PHONE,
							:FAX,
							:URL,
							:EMAIL,
							:EMAILL118DIRECT,
							:PUPILS,
							:SCHOOLCAPACITY,
							:LA,
							:PHASEOFEDUCATION,
							:TYPEOFESTABLISHMENT,
							:NUMBEROFPUPILSTAKINGFREESCHOOLMEALS,
							:NOMINATIONS,
							:SCHOOL_STATUS_ID
						);",
						[
							':ID118' => null,
							':HEADEMAILDRIECT' => 0,
							':URN' => $uesRippedSchools['account'],
							':NAME' => $uesRippedSchools['name'],
							':LEANAME' => $uesRippedSchools['local_authority_name'],
							':SCHOOLTYPE' => $uesRippedSchools['school_type'],
							':DENOMINATION' => '',
							':ADDRESS1' => $uesRippedSchools['address_1'],
							':ADDRESS2' => $uesRippedSchools['address_2'],
							':ADDRESS3' => $uesRippedSchools['address_3'],
							':TOWN' => $uesRippedSchools['city'],
							':COUNTY' => $uesRippedSchools['state'],
							':AREA' => $uesRippedSchools['city'],
							':POSTCODE' => $uesRippedSchools['postcode'],
							':POSTCODESUBAREA' => $uesRippedSchools['postcode'],
							':PHONE' => $uesRippedSchools['phone'],
							':FAX' => null,
							':URL' => $uesRippedSchools['website'],
							':EMAIL' => null,
							':EMAILL118DIRECT' => null,
							':PUPILS' => $uesRippedSchools['pupils_count'],
							':SCHOOLCAPACITY' => null,
							':LA' => null,
							':PHASEOFEDUCATION' => $uesRippedSchools['education_phase_name'],
							':TYPEOFESTABLISHMENT' => null,
							':NUMBEROFPUPILSTAKINGFREESCHOOLMEALS' => null,
							':NOMINATIONS' => null,
							':SCHOOL_STATUS_ID' => $livestatus // 3 = active.
						]
					);

					$rootSchoolsQuery = dbq("SELECT * FROM school_data WHERE URN = :ACCOUNT", [':ACCOUNT' => $uesRippedSchools['account']]);

				}


            }

            $rootSchool = dbf($rootSchoolsQuery); // base school from school_data

            // Make inserts into org database tables.

            dbq("INSERT INTO organizations (
							organization_id,
							organization_type_id,
							organization_name,
							organization_member_count,
							organization_default_address_id,
							organization_telephone,
							organization_fax,
							organization_email,
							organization_url,
							school_data_id,
							school_local_authority_id,
							school_education_phase_id,
							organization_default_logo_id,
							organization_funds,
							organization_status_id,
							school_URN,
							organization_has_subunits,
							organization_parent_id
						) VALUES (
					 		NULL,
							:SCHOOLTYPE,
							:NAME,
							:PCOUNT,
							NULL,
							:PHONE,
							NULL,
							NULL,
							:WEBSITE,
							:SDID,
							:LEAID,
							:PHID,
							NULL,
							:BALANCE,
							1,
							:URN,
							:HASHOUSES,
							:PARENTID
					 	);",
                [
                    ':SCHOOLTYPE' => (isset($orgType[$uesRippedSchools['school_type']]) ? $orgType[$uesRippedSchools['school_type']] : 1),
                    ':NAME' => $uesRippedSchools['name'],
                    ':PCOUNT' => $uesRippedSchools['pupils_count'],
                    ':PHONE' => $uesRippedSchools['phone'],
                    ':WEBSITE' => $uesRippedSchools['website'],
                    ':SDID' => $rootSchool['school_data_id'],
                    ':LEAID' => $LEAID[$uesRippedSchools['local_authority_name']],
                    ':PHID' => $phaseID[$uesRippedSchools['education_phase_name']],
                    ':BALANCE' => $uesRippedSchools['funds'],
                    ':URN' => $uesRippedSchools['account'],
                    ':HASHOUSES' => 0,
                    ':PARENTID' => $uesRippedSchools['house_of']
                ]
            );

            $newID = dbid();

            // Insert addresses.
            dbq("INSERT INTO addresses (
					address_id,
					entry_gender,
					entry_company,
					entry_firstname,
					entry_lastname,
					entry_street_address,
					entry_street_address_2,
					entry_city,
					entry_suburb,
					entry_state,
					entry_postcode,
					entry_country_id,
					user_id,
					organization_id
				) VALUES (
					:ADDRESSID,
					:ENTRYGENDER,
					:ENTRYCOMPANY,
					:ENTRYFIRSTNAME,
					:ENTRYLASTNAME,
					:ENTRYSTREETADDRESS,
					:ENTRYSTREETADDRESS2,
					:ENTRYCITY,
					:ENTRYSUBURB,
					:ENTRYSTATE,
					:ENTRYPOSTCODE,
					:ENTRYCOUNTRYID,
					:USERID,
					:ORGANIZATIONID
				)", [
                    ':ADDRESSID' => null,
                    ':ENTRYGENDER' => null,
                    ':ENTRYCOMPANY' => $uesRippedSchools['name'],
                    ':ENTRYFIRSTNAME' => null,
                    ':ENTRYLASTNAME' => null,
                    ':ENTRYSTREETADDRESS' => $uesRippedSchools['address_1'],
                    ':ENTRYSTREETADDRESS2' => $uesRippedSchools['address_2'],
                    ':ENTRYSUBURB' => $uesRippedSchools['address_3'],
                    ':ENTRYCITY' => $uesRippedSchools['city'],
                    ':ENTRYSTATE' => $uesRippedSchools['state'],
                    ':ENTRYPOSTCODE' => $uesRippedSchools['postcode'],
                    ':ENTRYCOUNTRYID' => 222,
                    ':USERID' => null,
                    ':ORGANIZATIONID' => $newID
                ]
            );

            $addressID = dbid();

            dbq("UPDATE organizations SET organization_default_address_id = :DEFAULTID WHERE organization_id = :OID", [':OID' => $newID, ':DEFAULTID' => $addressID]);

            // add users.

            $uesRippedSchoolUsersQuery = dbq("SELECT * FROM ues_school_users WHERE school_id = :SCHOOLID ", [':SCHOOLID' => $uesRippedSchools['id']]);

            while ($uesRippedSchoolUsers = dbf($uesRippedSchoolUsersQuery)) {
                dbq("INSERT INTO users
						(
							user_id,
							user_type_id,
							user_default_address_id,
							user_name,
							user_first_name,
							user_last_name,
							user_email,
							user_password,
							user_avatar,
							user_work_phone,
							user_mobile_phone,
							user_home_phone,
							user_home_email,
							user_title_id,
							user_salutation
						) VALUES (
							:USERID,
							:USERTYPEID,
							:USERDEFAULTADDRESSID,
							:USERNAME,
							:USERFIRSTNAME,
							:USERLASTNAME,
							:USEREMAIL,
							:USERPASSWORD,
							:USERAVATAR,
							:USERWORKPHONE,
							:USERMOBILEPHONE,
							:USERHOMEPHONE,
							:USERHOMEEMAIL,
							:USERTITLEID,
							:USERSALUTATION
						);",
                    [
                        ':USERID' => null,
                        ':USERTYPEID' => 1, // organization
                        ':USERDEFAULTADDRESSID' => $addressID,
                        ':USERNAME' => null,
                        ':USERFIRSTNAME' => $uesRippedSchoolUsers['first_name'],
                        ':USERLASTNAME' => $uesRippedSchoolUsers['last_name'],
                        ':USEREMAIL' => $uesRippedSchoolUsers['email'],
                        ':USERPASSWORD' => '$2y$10$jnKCsKF1o.j/u3peRASaB.QI/F84gTcshpkqq45K97//NPbouiBgq',
                        ':USERAVATAR' => null,
                        ':USERWORKPHONE' => null,
                        ':USERMOBILEPHONE' => null,
                        ':USERHOMEPHONE' => null,
                        ':USERHOMEEMAIL' => null,
                        ':USERTITLEID' => 1,
                        ':USERSALUTATION' => $uesRippedSchoolUsers['name_with_title']
                    ]);
            }

			// need to add users to orgs.

            dbq("UPDATE ues_school_data SET imported = 1 WHERE id = :ID", [':ID' => $uesRippedSchools['id']]);

            if ($break) {
                break;
            }
            $counter++;

        }

		// update all the houses to be houses.$houseLines
		dbq("update organizations set organization_has_subunits = 1 where school_URN IN ('".implode("','",$houseLines)."')");

        return $data;
    }

    public function updatebackgrounds()
    {
        // Get logo from logos table.
        ini_set('max_execution_time', 0);

        $logoOrgQuery = dbq("SELECT * FROM organization_logos WHERE primary_background_colour IS NULL");

        while ($logoOrg = dbf($logoOrgQuery)) {

            // find the associated ues_school
           // $accountNo = @reset(@explode('-', $logoOrg['tesco_style_ref']));

                // get the image.

                // what format is it?
				if(file_exists('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$logoOrg['tesco_style_ref'].'.jpg')){
					$file = imagecreatefromjpeg('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$logoOrg['tesco_style_ref'].'.jpg');
					$rgb = imagecolorat($file, 2, 2);
					$r = ($rgb >> 16) & 0xFF;
					$g = ($rgb >> 8) & 0xFF;
					$b = $rgb & 0xFF;
				} elseif(file_exists('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$logoOrg['tesco_style_ref'].'.JPG')){
					$file = imagecreatefromjpeg('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$logoOrg['tesco_style_ref'].'.JPG');
					$rgb = imagecolorat($file, 2, 2);
					$r = ($rgb >> 16) & 0xFF;
					$g = ($rgb >> 8) & 0xFF;
					$b = $rgb & 0xFF;
				} else {
					// grey as standard.
					$r = 127;
					$g = 127;
					$b = 127;
				}

                // update the logo background?
                dbq("UPDATE organization_logos SET primary_background_colour = :BACKGROUND WHERE organization_logo_id = :OLID", [':BACKGROUND' => $r . ',' . $g . ',' . $b, ':OLID' => $logoOrg['organization_logo_id']]);
                //return $r.','.$g.','.$b;

        }
        return 'done';
    }

	public function harvestlogos()
	{
		ini_set('max_execution_time', 0);
		$logoOrgQuery = dbq("SELECT * FROM ues_school_logos_real WHERE logos_copied = 0");

		while ($logoOrg = dbf($logoOrgQuery)) {
			$logo = @reset(explode("?", basename($logoOrg['image_url'])));

			if (!stristr('example-emblem.png', $logoOrg['image_url'])) {
				copy($logoOrg['image_url'], 'C:\\wamp\\www\\ues\\images\\ueslogos\\' . $logo);

				dbq("UPDATE ues_school_logos_real SET logos_copied = 1 WHERE id = :ID",
					[
						':ID' => $logoOrg['id']
					]
				);
				//break;
			}


		}

	}

    public function ebosupdate()
    {
        // Get the logos from ebos.  7000 logos.
        ini_set('max_execution_time', 0);

        include(DIR_CLASSES . 'eBOS_API.php');

        $eBOS_API = new eBOS_API(APIUser, APIPass);

        $logoOrgQuery = dbq("SELECT o.* FROM organizations o
								join ues_school_logos_real ul on (ul.school_id = o.school_URN)
								WHERE o.logos_imported = 0
								GROUP BY o.`organization_id`");

        while ($logoOrg = dbf($logoOrgQuery)) {
            // find designs for this school

            if (!empty($logoOrg['school_URN'])) {
                //sleep(1); //needs a delay for some reason.
                $designs = $eBOS_API->getDesigns($logoOrg['school_URN'] . '-');

                if (!empty($designs)) {
                    // Add logos to database.
                    $designArray = [];
					//echo "<pre>".print_r($designs,1)."</pre>"; die;
                    foreach ($designs->designs as $design) {
                        //sleep(1); //needs a delay for some reason.
                        $designDetail = $eBOS_API->getDesignDetails($design->id);
                        $designArray[] = $designDetail;
                        // Add to dbase?
                        if (is_object($designDetail) &&
                            is_object($designDetail->design)
                        ) {

                            if (@is_null($designDetail->design->destroyed_at)) {
                                // make the threads.
								//return (array)$designDetail->design;
                                //$PNG = "http://s3.amazonaws.com/ebos3production/design_tool_designs/logos/000/186/307/origin_png/101753-1A.png?1439316767";
								$pngLink = $designDetail->design->logo->png_url;

                                $svgasPNG = $pngLink;// str_replace("original/" . $designDetail->design->number . ".svg", "origin_png/" . $designDetail->design->number . ".png", $designDetail->design->logo->url);


								// what format is it?
								if(file_exists('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.jpg')){
									$file = imagecreatefromjpeg('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.jpg');
									$rgb = imagecolorat($file, 2, 2);
									$r = ($rgb >> 16) & 0xFF;
									$g = ($rgb >> 8) & 0xFF;
									$b = $rgb & 0xFF;
								} elseif(file_exists('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.JPG')){
									$file = imagecreatefromjpeg('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.JPG');
									$rgb = imagecolorat($file, 2, 2);
									$r = ($rgb >> 16) & 0xFF;
									$g = ($rgb >> 8) & 0xFF;
									$b = $rgb & 0xFF;
								} else {
									// grey as standard.
									$r = 127;
									$g = 127;
									$b = 127;
								}

								// update the logo background?
								//dbq("UPDATE organization_logos SET primary_background_colour = :BACKGROUND WHERE organization_logo_id = :OLID", [':BACKGROUND' => $r . ',' . $g . ',' . $b, ':OLID' => $logoOrg['organization_logo_id']]);
								//return $r.','.$g.','.$b;

                                dbq("INSERT INTO organization_logos
								(
									organization_logo_id,
									organization_unit_id,
									organization_id,
									school_name,
									tesco_style_ref,
									tesco_alt_style_code,
									product_group,
									product_name__colour,
									emblem_code,
									stitch_count,
									size__mm_,
									position,
									thread_1,
									thread_2,
									thread_3,
									thread_4,
									thread_5,
									thread_6,
									thread_7,
									thread_8,
									thread_9,
									thread_10,
									thread_11,
									thread_12,
									thread_13,
									thread_14,
									thread_15,
									logo_approved,
									logo_url,
									logo_svg_url,
									primary_background_colour
								) VALUES (
									:ORGANIZATIONLOGOID,
									:ORGANIZATIONUNITID,
									:ORGANIZATIONID,
									:SCHOOLNAME,
									:TESCOSTYLEREF,
									:TESCOALTSTYLECODE,
									:PRODUCTGROUP,
									:PRODUCTNAMECOLOUR,
									:EMBLEMCODE,
									:STITCHCOUNT,
									:SIZEMM,
									:POSITION,
									:THREAD1,
									:THREAD2,
									:THREAD3,
									:THREAD4,
									:THREAD5,
									:THREAD6,
									:THREAD7,
									:THREAD8,
									:THREAD9,
									:THREAD10,
									:THREAD11,
									:THREAD12,
									:THREAD13,
									:THREAD14,
									:THREAD15,
									:LOGOAPPROVED,
									:LOGOURL,
									:LOGOSVGURL,
									:PRIMARYBACKGROUNDCOLOUR
								);",
                                    [
                                        ':ORGANIZATIONLOGOID' => null,
                                        ':ORGANIZATIONUNITID' => null,
                                        ':ORGANIZATIONID' => $logoOrg['organization_id'],
                                        ':SCHOOLNAME' => $designDetail->design->name,
                                        ':TESCOSTYLEREF' => $designDetail->design->number,
                                        ':TESCOALTSTYLECODE' => null,
                                        ':PRODUCTGROUP' => null,
                                        ':PRODUCTNAMECOLOUR' => null,
                                        ':EMBLEMCODE' => null,
                                        ':STITCHCOUNT' => $designDetail->design->stitch_count,
                                        ':SIZEMM' => $designDetail->design->width . 'x' . $designDetail->design->height,
                                        ':POSITION' => null,
                                        ':THREAD1' => null,
                                        ':THREAD2' => null,
                                        ':THREAD3' => null,
                                        ':THREAD4' => null,
                                        ':THREAD5' => null,
                                        ':THREAD6' => null,
                                        ':THREAD7' => null,
                                        ':THREAD8' => null,
                                        ':THREAD9' => null,
                                        ':THREAD10' => null,
                                        ':THREAD11' => null,
                                        ':THREAD12' => null,
                                        ':THREAD13' => null,
                                        ':THREAD14' => null,
                                        ':THREAD15' => null,
                                        ':LOGOAPPROVED' => 1,
                                        ':LOGOURL' => $svgasPNG,
                                        ':LOGOSVGURL' => $designDetail->design->logo->url,
                                        ':PRIMARYBACKGROUNDCOLOUR' => $r . ',' . $g . ',' . $b,
                                    ]
                                );

                                $logoID = dbid();

                                if (is_null($logoOrg['organization_default_logo_id'])) {
                                    // update default logo
                                    dbq("UPDATE organizations SET organization_default_logo_id = :LOGOID WHERE organization_id = :ORGID", [':LOGOID' => $logoID, ':ORGID' => $logoOrg['organization_id']]);
                                }
                            }

                        } else {
                            // 500 error?
                            return (array)$designDetail;
                        }
                    }

                } else {
                    // its empty?  No designs for this school?
                    return (array)$designs;

                }
            }
            dbq("UPDATE organizations SET logos_imported = 1 WHERE organization_id = :ORGID", [':ORGID' => $logoOrg['organization_id']]);

			//die;
        }

        /*foreach($designs->designs as $design){

            $designDetail = $authRequest->getDesignDetails($design->id);

        }*/

    }

    public function getFonts()
    {
        $fontString = '
@font-face {
    font-family: "Tesco Icons";
    font-style: normal;
    font-weight: 400;
    src: url("../fonts/tescoicons.eot?#iefix") format("embedded-opentype"), url("../fonts/tescoicons.svg") format("svg"), url("../fonts/tescoicons.woff") format("woff"), url("../fonts/tescoicons.ttf") format("opentype");
}
@font-face {
    font-family: "Tesco";
    font-style: normal;
    font-weight: 400;
    src: url("../fonts/tesco.eot?#iefix") format("eot"), url("../fonts/tesco.svg#tescoregular") format("svg"), url("../fonts/tesco.woff") format("woff"), url("../fonts/tesco.ttf") format("opentype");
}
@font-face {
    font-family: "Tesco";
    font-style: normal;
    font-weight: 700;
    src: url("../fonts/tescob.eot?#iefix") format("eot"), url("../fonts/tescob.svg#tescobold") format("svg"), url("../fonts/tescob.woff") format("woff"), url("../fonts/tescob.ttf") format("opentype");
}
@font-face {
    font-family: "Tesco";
    font-style: italic;
    font-weight: 400;
    src: url("../fonts/tescoi.eot?#iefix") format("eot"), url("../fonts/tescoi.svg#tescoitalic") format("svg"), url("../fonts/tescoi.woff") format("woff"), url("../fonts/tescoi.ttf") format("opentype");
}
@font-face {
    font-family: "Hudl";
    font-style: normal;
    font-weight: 400;
    src: url("../fonts/hudlasap-regular?#iefix") format("eot"), url("../fonts/hudlasap-regular.svg#tescoitalic") format("svg"), url("../fonts/hudlasap-regular.woff") format("woff"), url("../fonts/hudlasap-regular.ttf") format("opentype");
}
@font-face {
    font-family: "Hudl";
    font-style: normal;
    font-weight: 700;
    src: url("../fonts/hudlasap-bold?#iefix") format("eot"), url("../fonts/hudlasap-bold.svg#tescoitalic") format("svg"), url("../fonts/hudlasap-bold.woff") format("woff"), url("../fonts/hudlasap-bold.ttf") format("opentype");
}
@font-face {
    font-family: "Roboto";
    font-style: normal;
    font-weight: 400;
    src: url("../fonts/roboto-medium-webfont?#iefix") format("eot"), url("../fonts/roboto-medium-webfont.svg#tescoitalic") format("svg"), url("../fonts/roboto-medium-webfont.woff") format("woff"), url("../fonts/roboto-medium-webfont.ttf") format("opentype");
}
@font-face {
    font-family: "Roboto";
    font-weight: 400;
    src: url("../fonts/roboto-bold-webfont?#iefix") format("eot"), url("../fonts/roboto-bold-webfont.svg#tescoitalic") format("svg"), url("../fonts/roboto-bold-webfont.woff") format("woff"), url("../fonts/roboto-bold-webfont.ttf") format("opentype");
}
';
        preg_match_all('/url\(([\s])?([\"|\'])?(.*?)([\"|\'])?([\s])?\)/i', $fontString, $matches, PREG_PATTERN_ORDER);
        $data = [];
        if ($matches) {
            foreach ($matches[3] as $match) {
                // do whatever you want with those matches, adapt paths by changing them to absolute or CDN paths
                // - "images/bg.gif" -> "/path_to_css_module/images/bg.gif"
                // - "images/bg.gif" -> "http://cdn.domain.tld/path_to_css_module/images/bg.gif"
                $thisFile = str_replace("../fonts/", "http://www.tesco.com/directuiassets/SiteAssets/NonSeasonal/en_GB/fonts/", $match);
                $data[] = $thisFile;

                $fileContent = file_get_contents($thisFile);
                $filename = str_replace("http://www.tesco.com/directuiassets/SiteAssets/NonSeasonal/en_GB/fonts/", '', $thisFile);
                if (stristr($filename, '?')) {
                    $filename = substr($filename, 0, strpos($filename, '?'));
                }
                file_put_contents('Fonts/' . $filename, $fileContent);
                //break;


            }
        }

        return $data;
    }

	private function schoolExists($urn){
		// may need to check based on parent_id
		// check first.

		$schoolQuery = dbq("select * from organizations o where o.school_URN = :URN",[
			':URN' => $urn
		]);

		return boolval(dbnr($schoolQuery));
	}

	private function findSchoolID($urn){
		$schoolQuery = dbq("select * from school_data sd join organizations o on (sd.school_data_id = o.school_data_id) where o.school_URN = :URN",[
			':URN' => $urn
		]);

		if(dbnr($schoolQuery) > 0){
			$school = dbf($schoolQuery);
			return $school;
		} else {
			return false;
		}
	}

	private function findColourID($stylecode){
		$styleQuery = dbq("select * from product_colours where product_colour_style_ref = :STYLECODE",[
			':STYLECODE' => $stylecode
		]);

		if(dbnr($styleQuery) > 0){
			$style = dbf($styleQuery);
			return $style;
		} else {
			return false;
		}
	}

	private function findLogoID($logocode){
		$logoQuery = dbq("select * from organization_logos where tesco_style_ref = :LOGOCODE",[
			':LOGOCODE' => $logocode
		]);

		if(dbnr($logoQuery) > 0){
			$logo = dbf($logoQuery);
			return $logo;
		} else {
			return false;
		}
	}

	public function importplain(){

		session_destroy(); // close session so we can do other stuff.

		// load file.
		$testFile = 'PLAINUNIFORMTEMPLATE.csv';
		$filecontent = file_get_contents($testFile);

		//echo $filecontent;

		$csv = array_map('str_getcsv', file($testFile));

		//echo "<pre>".print_r($csv)."</pre>";

		$headers = [];
		$headerDone = false;
		$products = [];
		$productFields = ['product_name','product_description','product_size_text','product_image',"product_decorated","product_category_name","product_emblem_small","product_new","product_soon","product_awaiting","product_price","product_emblem_position"];
		$colourFields = ['product_colour_style_ref','colour_name','product_colour_image_url'];
		$lineFields = ['products_size_name','product_variation_sku','product_variation_price'];

		foreach($csv as $csvLine){
			if(!$headerDone){
				$headerKeys = $csvLine;
				$headers = array_flip($csvLine);
				$headerDone = true;
				continue; // skip ver the rest.
			}

			foreach($csvLine as $key => $value){
				$csvLine[$key] = mb_convert_encoding(trim(addslashes($value)),'ASCII');
			};

			// Process line

			if(!isset($products[$csvLine[$headers['product_name']]])){
				$products[$csvLine[$headers['product_name']]] = [];
				foreach($csvLine as $key => $value){
					if(in_array($headerKeys[$key],$productFields)){
						$products[$csvLine[$headers['product_name']]][$headerKeys[$key]] = $value;
					}
				};
				$products[$csvLine[$headers['product_name']]]['colours'] = [];
			}

			if(!isset($products[$csvLine[$headers['product_name']]]['colours'][$csvLine[$headers['product_colour_style_ref']]])){
				$products[$csvLine[$headers['product_name']]]['colours'][$csvLine[$headers['product_colour_style_ref']]] = [];

				foreach($csvLine as $key => $value){
					if(in_array($headerKeys[$key],$colourFields)){
						$products[$csvLine[$headers['product_name']]]['colours'][$csvLine[$headers['product_colour_style_ref']]][$headerKeys[$key]] = $value;
					}
				};

				$products[$csvLine[$headers['product_name']]]['colours'][$csvLine[$headers['product_colour_style_ref']]]['sizes'] = [];

			}

			$products[$csvLine[$headers['product_name']]]['colours'][$csvLine[$headers['product_colour_style_ref']]]['sizes'][] = [
				'products_size_name' => $csvLine[$headers['products_size_name']],
				'product_variation_sku'	=> $csvLine[$headers['product_variation_sku']],
				'product_variation_price' => $csvLine[$headers['product_variation_price']]
			];
			//return $products;
		}

		$data['type'] = 200;
		$data['data'] = $products;

		// echo $products;

		// now add them to the database.

		foreach($products as $productName => $productData){
			// check if its in the database.
			$productExistsQuery = dbq("select p.product_id from products p where p.product_name = :PRODUCTNAME",
				[
					':PRODUCTNAME' => $productName
				]
			);

			if(dbnr($productExistsQuery) > 0){
				// already exists?
				$productExists = dbf($productExistsQuery);
				$prodID = $productExists['product_id'];
			} else {
				// insert it.
				dbq("INSERT INTO products (
						product_name,
						product_description,
						product_size_text,
						product_image,
						product_url,
						product_active,
						product_decorated,
						product_category_id,
						product_emblem_small,
						product_new,
						product_soon,
						product_awaiting,
						product_sku,
						product_price,
						product_ref,
						product_alt_ref,
						product_test_only,
						product_emblem_position
					  ) values (
						:PRODUCT_NAME,
						:PRODUCT_DESCRIPTION,
						:PRODUCT_SIZE_TEXT,
						:PRODUCT_IMAGE,
						:PRODUCT_URL,
						:PRODUCT_ACTIVE,
						:PRODUCT_DECORATED,
						:PRODUCT_CATEGORY_ID,
						:PRODUCT_EMBLEM_SMALL,
						:PRODUCT_NEW,
						:PRODUCT_SOON,
						:PRODUCT_AWAITING,
						:PRODUCT_SKU,
						:PRODUCT_PRICE,
						:PRODUCT_REF,
						:PRODUCT_ALT_REF,
						:PRODUCT_TEST_ONLY,
						:PRODUCT_EMBLEM_POSITION
					  )",
					[
						':PRODUCT_NAME' => $productData['product_name'],
						':PRODUCT_DESCRIPTION' => $productData['product_description'],
						':PRODUCT_SIZE_TEXT' => $productData['product_size_text'],
						':PRODUCT_IMAGE' => $productData['product_image'],
						':PRODUCT_URL' => null,
						':PRODUCT_ACTIVE' => 0,
						':PRODUCT_DECORATED' => $productData['product_decorated'],
						':PRODUCT_CATEGORY_ID' => getCategoryID($productData['product_category_name']),
						':PRODUCT_EMBLEM_SMALL' => $productData['product_emblem_small'],
						':PRODUCT_NEW' =>  $productData['product_new'],
						':PRODUCT_SOON' =>  $productData['product_soon'],
						':PRODUCT_AWAITING' => $productData['product_awaiting'],
						':PRODUCT_SKU' => null,
						':PRODUCT_PRICE' => $productData['product_price'],
						':PRODUCT_REF' => null,
						':PRODUCT_ALT_REF' => null,
						':PRODUCT_TEST_ONLY' => 0,
						':PRODUCT_EMBLEM_POSITION' => $productData['product_emblem_position']
					]
				);

				$prodID = dbid();
			}

			// check if the colour exists for this product.
			dbq("select * from ");

		}

		return $data;
	}

	public function import(){

		session_destroy(); // close session so we can do other stuff.

		// load file.
		$testFile = 'PLAINUNIFORMTEMPLATE.csv';
		$filecontent = file_get_contents($testFile);

		//echo $filecontent;

		$csv = array_map('str_getcsv', file($testFile));

		//echo "<pre>".print_r($csv)."</pre>";

		$headers = [];
		$headerDone = false;
		$products = [];
		$productFields = ['product_name','product_description','product_size_text','product_image',"product_decorated","product_category_name","product_emblem_small","product_new","product_soon","product_awaiting","product_price","product_emblem_position"];
		$colourFields = ['product_colour_style_ref','colour_name','product_colour_image_url'];
		$lineFields = ['products_size_name','product_variation_sku','product_variation_price'];

		foreach($csv as $csvLine){
			if(!$headerDone){
				$headerKeys = $csvLine;
				$headers = array_flip($csvLine);
				$headerDone = true;
				continue; // skip ver the rest.
			}

			foreach($csvLine as $key => $value){
				$csvLine[$key] = mb_convert_encoding(trim(addslashes($value)),'ASCII');
			};

			// Process line

			if(!isset($products[$csvLine[$headers['product_name']]])){
				$products[$csvLine[$headers['product_name']]] = [];
				foreach($csvLine as $key => $value){
					if(in_array($headerKeys[$key],$productFields)){
						$products[$csvLine[$headers['product_name']]][$headerKeys[$key]] = $value;
					}
				};
				$products[$csvLine[$headers['product_name']]]['colours'] = [];
			}

			if(!isset($products[$csvLine[$headers['product_name']]]['colours'][$csvLine[$headers['product_colour_style_ref']]])){
				$products[$csvLine[$headers['product_name']]]['colours'][$csvLine[$headers['product_colour_style_ref']]] = [];

				foreach($csvLine as $key => $value){
					if(in_array($headerKeys[$key],$colourFields)){
						$products[$csvLine[$headers['product_name']]]['colours'][$csvLine[$headers['product_colour_style_ref']]][$headerKeys[$key]] = $value;
					}
				};

				$products[$csvLine[$headers['product_name']]]['colours'][$csvLine[$headers['product_colour_style_ref']]]['sizes'] = [];

			}

			$products[$csvLine[$headers['product_name']]]['colours'][$csvLine[$headers['product_colour_style_ref']]]['sizes'][] = [
				'products_size_name' => $csvLine[$headers['products_size_name']],
				'product_variation_sku'	=> $csvLine[$headers['product_variation_sku']],
				'product_variation_price' => $csvLine[$headers['product_variation_price']]
			];
			//return $products;
		}
		//print_r($products);
		$data['type'] = 200;
		$data['data'] = $products;
		//print_r($data);

		//$products = json_encode($products);
		//echo $products;

		// now add them to the database.

		foreach($products as $productName => $productData){
			// check if its in the database.
			$productExistsQuery = dbq("select p.product_id from products p where p.product_name = :PRODUCTNAME",
				[
					':PRODUCTNAME' => $productName
				]
			);

			if(dbnr($productExistsQuery) > 0){
				// already exists?
				$productExists = dbf($productExistsQuery);
				$prodID = $productExists['product_id'];
			} else {
				// insert it.
				dbq("INSERT INTO products (
						product_name,
						product_description,
						product_size_text,
						product_image,
						product_url,
						product_active,
						product_decorated,
						product_category_id,
						product_emblem_small,
						product_new,
						product_soon,
						product_awaiting,
						product_sku,
						product_price,
						product_ref,
						product_alt_ref,
						product_test_only,
						product_emblem_position
					  ) values (
						:PRODUCT_NAME,
						:PRODUCT_DESCRIPTION,
						:PRODUCT_SIZE_TEXT,
						:PRODUCT_IMAGE,
						:PRODUCT_URL,
						:PRODUCT_ACTIVE,
						:PRODUCT_DECORATED,
						:PRODUCT_CATEGORY_ID,
						:PRODUCT_EMBLEM_SMALL,
						:PRODUCT_NEW,
						:PRODUCT_SOON,
						:PRODUCT_AWAITING,
						:PRODUCT_SKU,
						:PRODUCT_PRICE,
						:PRODUCT_REF,
						:PRODUCT_ALT_REF,
						:PRODUCT_TEST_ONLY,
						:PRODUCT_EMBLEM_POSITION
					  )",
					[
						':PRODUCT_NAME' => $productData['product_name'],
						':PRODUCT_DESCRIPTION' => $productData['product_description'],
						':PRODUCT_SIZE_TEXT' => $productData['product_size_text'],
						':PRODUCT_IMAGE' => $productData['product_image'],
						':PRODUCT_URL' => null,
						':PRODUCT_ACTIVE' => 0,
						':PRODUCT_DECORATED' => $productData['product_decorated'],
						':PRODUCT_CATEGORY_ID' => getCategoryID($productData['product_category_name']),
						':PRODUCT_EMBLEM_SMALL' => $productData['product_emblem_small'],
						':PRODUCT_NEW' =>  $productData['product_new'],
						':PRODUCT_SOON' =>  $productData['product_soon'],
						':PRODUCT_AWAITING' => $productData['product_awaiting'],
						':PRODUCT_SKU' => null,
						':PRODUCT_PRICE' => $productData['product_price'],
						':PRODUCT_REF' => null,
						':PRODUCT_ALT_REF' => null,
						':PRODUCT_TEST_ONLY' => 0,
						':PRODUCT_EMBLEM_POSITION' => $productData['product_emblem_position']
					]
				);

				$prodID = dbid();
			}

			// check if the colour exists for this product.
			dbq("select * from ");

		}

		return $data;
	}

public function addraf(){

	$fromDate = '2016-10-01 00:00:00';
	$toDate = '2016-12-01 00:00:00';

	$RAFQuery = dbq("SELECT o.organization_id, o.school_URN, o.organization_parent_id, o.organization_live_date, ro.organization_id as roid, ro.school_URN as rourn, ro.organization_parent_id as ropid, ro.organization_live_date as rold FROM organizations o JOIN organizations ro ON (ro.`organization_id` = o.`organization_raf_organization_id`) WHERE o.`organization_live_date` > :FROMDATE AND o.`organization_live_date` < :TODATE ",
		[
			':FROMDATE' => $fromDate,
			':TODATE' => $toDate
		]
	);

	$donations = [];
	while($RAF = dbf($RAFQuery)){
		// insert a donation if not a house that has gone live.

		if(is_null($RAF['organization_parent_id'])){

			$livetime = strtotime($RAF['organization_live_date']);
			//live school
			$orgdetail = getOrgDetails($RAF['organization_id']);

			dbq("insert into donations (
						organization_id,
						organization_urn,
						donation_name,
						donation_accounted_for,
						donation_amount,
						donation_new_balance,
						donation_category,
						donation_paid,
						donation_created_at,
						donation_display_date,
						donation_description
					)  values (
						:ORGID,
						:ORGURN,
						'tesco_donation',
						:ACCOUNTED,
						:AMOUNT,
						:BALANCE,
						5,
						0,
						:DATE,
						:DATEB,
						:DESC
					)",
				[
					':ORGID' => $RAF['organization_id'],
					':ORGURN' => $RAF['school_URN'],
					':ACCOUNTED' => date('Y-m-d H:i:s',strtotime('last day of this month',$livetime)),
					':AMOUNT' => '50',
					':BALANCE' => $orgdetail['organization_funds_balance'] + 50,
					':DATE' => $RAF['organization_live_date'],
					':DATEB' => $RAF['organization_live_date'],
					':DESC' => 'Extra donation during '.date("F Y",$livetime).' (RAF Bonus)'
				]
			);

			// update organization balance.
			dbq("update organizations set organization_funds_balance = :BALANCE, organization_funds = organization_funds + :THISDONATION where organization_id = :ORGID",
				[
					':BALANCE' => $orgdetail['organization_funds_balance'] + 50,
					':THISDONATION' => $orgdetail['organization_funds_balance'],
					':ORGID' => $RAF['organization_id']
				]
			);

			// Refering school
			$rorgdetail = getOrgDetails($RAF['roid']);

			dbq("insert into donations (
						organization_id,
						organization_urn,
						donation_name,
						donation_accounted_for,
						donation_amount,
						donation_new_balance,
						donation_category,
						donation_paid,
						donation_created_at,
						donation_display_date,
						donation_description
					)  values (
						:ORGID,
						:ORGURN,
						'tesco_donation',
						:ACCOUNTED,
						:AMOUNT,
						:BALANCE,
						5,
						0,
						:DATE,
						:DATEB,
						:DESC
					)",
				[
					':ORGID' => $RAF['roid'],
					':ORGURN' => $RAF['rourn'],
					':ACCOUNTED' => date('Y-m-d H:i:s',strtotime('last day of this month',$livetime)),
					':AMOUNT' => '50',
					':BALANCE' => $rorgdetail['organization_funds_balance'] + 50,
					':DATE' => $RAF['organization_live_date'],
					':DATEB' => $RAF['organization_live_date'],
					':DESC' => 'Extra donation during '.date("F Y",$livetime).' (RAF Bonus)'
				]
			);

			// update organization balance.
			dbq("update organizations set organization_funds_balance = :BALANCE, organization_funds = organization_funds + :THISDONATION where organization_id = :ORGID",
				[
					':BALANCE' => $rorgdetail['organization_funds_balance'] + 50,
					':THISDONATION' => $rorgdetail['organization_funds_balance'],
					':ORGID' => $RAF['roid']
				]
			);
		}

	}

	return $donations;

}



	public function testftp(){
		// upload a copy to v-stock

		$vstockHost = 'v-source.co.uk';
		$vstockUser = 'tescofandf_slickstich';
		$vstockPass = 'daHekWonvu';
		$vstockStatusFolder = 'live/incoming'; // live

		$vs_conn_id = ftp_connect($vstockHost);

		if($vs_conn_id) {
			$login_result = ftp_login($vs_conn_id, $vstockUser, $vstockPass);

			if ($login_result) {
				ftp_pasv($vs_conn_id, true);

				// move done files.
				$fileList = ftp_nlist($vs_conn_id,$vstockStatusFolder);

				//return $fileList;

				foreach($fileList as $file){
					if((stristr($file,'.done') && $file != '.done') || stristr($file,'.psv_processed')){
						// move it to the .done folder.
						ftp_rename($vs_conn_id,$file,str_replace('incoming/','incoming/.done/',$file));
					}
				}

			}
		}
	}

	public function importrawdonations()
	{

		ini_set('max_execution_time','3000');
		ini_set('memory_limit',-1);

		$FOLDER = 'import/atg/';

		$filelist = glob($FOLDER.'*.xls*');

		include(DIR_CLASSES . 'PHPExcel.php');
		include(DIR_CLASSES . 'PHPExcel/IOFactory.php');

		$donations = [];

		$houseIDs = [
			'901',
			'902',
			'903',
			'904',
			'905',
			'906',
			'907',
			'908',
			'909',
			'910',
			'911',
			'912',
			'913',
			'914',
			'915',
			'916',
			'917',
			'918',
			'919',
		];

		foreach($filelist as $fileName) {

			$inputFileType = PHPExcel_IOFactory::identify($fileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($fileName);

			// convert dates to correct format.
			for ($i = 2; $i < $objPHPExcel->getActiveSheet()->getHighestRow(); $i++) {

				$masterURN = getMasterURN($objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue());

				$houseident = substr($masterURN,0,3);

				if(in_array($houseident,$houseIDs)){
					// probably a house not on the system, just knock off the first 3 chars.
					$masterURN = substr($masterURN,3);
				}

				if(is_numeric($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue())){
					$date = date('Y-m-d',PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue()));
				} else {
					$date = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue();
				}

				dbq("insert into organization_sales (
							school_urn,
							dispatched_date,
							order_number,
							style_ref,
							EAN,
							quantity,
							price_paid,
							donation_amount
						  ) VALUES (
							:SCHOOL_URN,
							:DISPATCHED_DATE,
							:ORDER_NUMBER,
							:STYLE_REF,
							:EAN,
							:QUANTITY,
							:PRICE_PAID,
							:DONATION_AMOUNT
						  )",
					[
						':SCHOOL_URN' => $masterURN,
						':DISPATCHED_DATE' => $date,
						':ORDER_NUMBER' => $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue(),
						':STYLE_REF' => $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue(),
						':EAN' => $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue(),
						':QUANTITY' => $objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue(),
						':PRICE_PAID' => $objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue(),
						':DONATION_AMOUNT' => $objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue()
					]
				);

			}
		}

		$FOLDER = 'import/parker/';

		$filelist = glob($FOLDER.'*.xls*');

		foreach($filelist as $fileName) {

			$inputFileType = PHPExcel_IOFactory::identify($fileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($fileName);

			// convert dates to correct format.
			for ($i = 2; $i < $objPHPExcel->getActiveSheet()->getHighestRow(); $i++) {
				// make sure we didnt reach the end.
				$cellValA = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue();
				$dateVal = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue();

				if(!stristr($cellValA,'total') && !empty($cellValA)){
					// total is in the last one.
					$masterURN = getMasterURN($cellValA);

					$houseident = substr($masterURN,0,3);

					if(in_array($houseident,$houseIDs)){
						// probably a house not on the system, just knock off the first 3 chars.
						$masterURN = substr($masterURN,3);
					}

					// TODO fsdlfj;lkjhgs;dkfjgh;kdjfghkdfjhgkjlkjn'gakn;lgfz;lkh

					//$dateStrring = date("Y-m-d",strtotime(str_replace("/","-",$objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue())));

					$dateStrring = date('Y-m-d',PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue()));
					//$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(2, $i, strtotime(str_replace("/","-",$objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue())), PHPExcel_Cell_DataType::TYPE_STRING);

					dbq("insert into raw_donations (
						order_ref,
						order_date,
						spare1,
						school_urn,
						style_ref,
						ean,
						quantity,
						unit_price,
						line_price,
						line_donation_value
					  ) VALUES (
						:ORDER_REF,
						:ORDER_DATE,
						:SPARE1,
						:SCHOOL_URN,
						:STYLE_REF,
						:EAN,
						:QUANTITY,
						:UNIT_PRICE,
						:LINE_PRICE,
						:LINE_DONATION_VALUE
					  )",
						[
							':ORDER_REF' => 0,
							':ORDER_DATE' => $dateStrring,
							':SPARE1' => 0,
							':SCHOOL_URN' => $masterURN,
							':STYLE_REF' => 'Parker Lane recycling - '.$objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue().'kg',
							':EAN' => 0,
							':QUANTITY' => 0,
							':UNIT_PRICE' => 0,
							':LINE_PRICE' => 0,
							':LINE_DONATION_VALUE' => floatval($objPHPExcel->getActiveSheet()->getCell('F'.$i)->getCalculatedValue())
						]
					);
				}
			}
		}

		return $donations;
	}

	public function ripProductDetails()
	{
		/*

		 */


		ini_set('max_execution_time','14400');
		ini_set('memory_limit',-1);

		$FOLDER = 'UESProducts/';

		$filelist = glob($FOLDER.'*.xls*');

		include(DIR_CLASSES . 'PHPExcel.php');
		include(DIR_CLASSES . 'PHPExcel/IOFactory.php');

		$products = [];

		foreach($filelist as $fileName) {

			$inputFileType = PHPExcel_IOFactory::identify($fileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($fileName);

			//return 'here';
			//return $objPHPExcel->getActiveSheet()->getHighestColumn();


			$headerRow = [];
			for ($col = 'A'; $col != $objPHPExcel->getActiveSheet()->getHighestColumn(); $col++) {
				$headerRow[] = $objPHPExcel->getActiveSheet()->getCell($col.'3')->getValue();
			}

			if(!isset($products[$fileName])){
				$products[$fileName] = [];
			}

					// convert dates to correct format.
					//for ($row = 4; $row < $objPHPExcel->getActiveSheet()->getHighestRow(); $row++) {
					for ($row = 4; $row < 10; $row++) {
						//for ($row = 4; $row < 20; $row++) {
						$dataRow = [];
						for ($col = 'A'; $col != $objPHPExcel->getActiveSheet()->getHighestColumn(); $col++) {
							$dataRow[] = $objPHPExcel->getActiveSheet()->getCell($col.$row)->getValue();
						}
						$dataRow = array_combine($headerRow,$dataRow);
						$dataRow['price'] = 0;
						switch($dataRow['variant_method']){
							case 'Parent' :
								// part_number = style ref of parent.

								if(!isset($products[$fileName][$dataRow['part_number']])){
									$dataRow['price'] = 999; // set silly high so we can go lower.
									$products[$fileName][$dataRow['part_number']] = $dataRow;
									$products[$fileName][$dataRow['part_number']]['slaves'] = [];
								} else {
									$products[$fileName][$dataRow['part_number']] = $dataRow;
								}
								break;
							case 'Child' :
							case 'Single product' :
						// option_of = parent
						if(!isset($products[$fileName][$dataRow['option_of']])){
							$products[$fileName][$dataRow['option_of']] = [];
							$products[$fileName][$dataRow['option_of']]['slaves'] = [];
						}

						$priceQuery = dbq("select product_variation_price from products_sizes_to_products_colours where product_variation_sku = :EAN",
							[
								'EAN' => $dataRow['gtin_number']
							]
						);

						if(!isset($products[$fileName][$dataRow['option_of']]['price'])){
							$products[$fileName][$dataRow['option_of']]['price'] = 999;
						}
						if(dbnr($priceQuery) > 0){
							$priceRes = dbf($priceQuery);
							$dataRow['price'] = $priceRes['product_variation_price'];
							if($dataRow['price'] < $products[$fileName][$dataRow['option_of']]['price']){
								$products[$fileName][$dataRow['option_of']]['price'] = $dataRow['price'];
							}
						} else {
							$dataRow['price'] = 0;
						}
						$products[$fileName][$dataRow['option_of']]['slaves'][] = $dataRow;
						break;
				}

				//$products[$fileName][] = array_combine($headerRow,$dataRow);

				/*
				if(is_numeric($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue())){
					$date = date('Y-m-d',PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue()));
				} else {
					$date = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue();
				}*/
			}

		}

		file_put_contents('Productlist.json',json_encode($products));

		return 'Done';
	}

	public function linkTPNBTable()
	{

		ini_set('max_execution_time','14400');
		ini_set('memory_limit',-1);

		$FOLDER = 'UESProducts/';

		$filelist = glob($FOLDER.'*.xls*');

		include(DIR_CLASSES . 'PHPExcel.php');
		include(DIR_CLASSES . 'PHPExcel/IOFactory.php');

		$products = [];
		$headerRow = [];

		foreach($filelist as $fileName) {

			$inputFileType = PHPExcel_IOFactory::identify($fileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($fileName);

			//return 'here';
			//return $objPHPExcel->getActiveSheet()->getHighestColumn();

			for ($col = 'A'; $col != $objPHPExcel->getActiveSheet()->getHighestColumn(); $col++) {
				$headerRow[] = $objPHPExcel->getActiveSheet()->getCell($col.'3')->getValue();
			}

		}

		$headerRow = array_unique($headerRow);

		$createTable = "CREATE TABLE `ues`.`virtualstock_lists`(";
		$createTable .= "`virtualstock_list_filename` VARCHAR(255),";
		foreach($headerRow as $headerRowValue){
			$createTable .= "`$headerRowValue` VARCHAR(255),";
		}
		$createTable .= " );";

		return $createTable;
	}

	public function linkTPNBDataImport()
	{

		ini_set('max_execution_time','14400');
		ini_set('memory_limit',-1);

		$FOLDER = 'UESProducts/';

		$filelist = glob($FOLDER.'*.xls*');

		include(DIR_CLASSES . 'PHPExcel.php');
		include(DIR_CLASSES . 'PHPExcel/IOFactory.php');

		$products = [];

		foreach($filelist as $fileName) {

			$inputFileType = PHPExcel_IOFactory::identify($fileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($fileName);

			//return 'here';
			//return $objPHPExcel->getActiveSheet()->getHighestColumn();


			$headerRow = [];
			for ($col = 'A'; $col != $objPHPExcel->getActiveSheet()->getHighestColumn(); $col++) {
				$headerRow[] = $objPHPExcel->getActiveSheet()->getCell($col.'3')->getValue();
			}

			if(!isset($products[$fileName])){
				$products[$fileName] = [];
			}

			// convert dates to correct format.
			for ($row = 4; $row < $objPHPExcel->getActiveSheet()->getHighestRow(); $row++) {
			//for ($row = 4; $row < 10; $row++) {
				$dataRow = [];
				for ($col = 'A'; $col != $objPHPExcel->getActiveSheet()->getHighestColumn(); $col++) {
					$dataRow[] = $objPHPExcel->getActiveSheet()->getCell($col.$row)->getValue();
				}
				$dataRow = array_combine($headerRow,$dataRow);
				$dataRowTokens = array_map(function($val){
					return ':'.strtoupper($val);
				},$headerRow);
				$dataRowTokens = array_combine($dataRowTokens,$dataRow);
				// insert into database.

				//return "INSERT INTO `virtualstock_lists` (`virtualstock_list_filename`,`".implode('`,`',$headerRow)."`) VALUES ('".basename($fileName)."','".implode("','",array_keys($dataRowTokens))."')";
				dbq("INSERT INTO `virtualstock_lists` (`virtualstock_list_filename`,`".implode('`,`',$headerRow)."`) VALUES ('".basename($fileName)."',".implode(",",array_keys($dataRowTokens)).")",
					$dataRowTokens
				);
			}

		}

		return 'done';
	}

	public function stockByTPNB()
	{
		$tpnbQuery = dbq("SELECT ptl.`product_tpnb_link_tpnb`, ptl.`product_tpnb_link_description`, pb.`ean`, SUM(pb.qty) FROM product_tpnb_link ptl LEFT JOIN
							prodbal pb ON pb.ean = ptl.product_tpnb_link_ean LEFT JOIN
							products_sizes_to_products_colours ps2pc ON ps2pc.product_variation_sku = pb.ean GROUP BY ptl.`product_tpnb_link_tpnb` ORDER BY pb.`ean` DESC");
	}

	public function salesByTPNB()
	{
		$tpnbQuery = dbq("SELECT
  os.`EAN`, tpl.`product_tpnb_link_tpnb`, SUM(os.`quantity`) AS totalQty
FROM
  `organization_sales` os
  LEFT JOIN `product_tpnb_link` tpl ON tpl.`product_tpnb_link_ean` = os.`EAN`
  LEFT JOIN `products_sizes_to_products_colours` ps2pc ON ps2pc.`product_variation_sku` = os.`EAN`
  WHERE os.`dispatched_date` >= '2018-01-01'
  GROUP BY tpl.`product_tpnb_link_tpnb`");
	}

	public function salesByTPNBbySchool()
	{
		$tpnbQuery = dbq("SELECT
  os.`school_urn`, tpl.`product_tpnb_link_tpnb`, sum(os.`quantity`) as totalQty
FROM
  `organization_sales` os
  LEFT join `product_tpnb_link` tpl on tpl.`product_tpnb_link_ean` = os.`EAN`
  LEFT JOIN `products_sizes_to_products_colours` ps2pc ON ps2pc.`product_variation_sku` = os.`EAN`
  WHERE os.`dispatched_date` >= '2018-01-01'
  GROUP BY os.school_urn, tpl.`product_tpnb_link_tpnb`");
	}

	public function importrawatg()
	{

		ini_set('max_execution_time','3000');
		ini_set('memory_limit',-1);

		include(DIR_CLASSES . 'PHPExcel.php');
		include(DIR_CLASSES . 'PHPExcel/IOFactory.php');

		$donations = [];

		$houseIDs = [
			'901',
			'902',
			'903',
			'904',
			'905',
			'906',
			'907',
			'908',
			'909',
			'910',
			'911',
			'912',
			'913',
			'914',
			'915',
			'916',
			'917',
			'918',
			'919',
		];

		$FOLDER = 'import/atg/';

		$filelist = glob($FOLDER.'*.xls*');
		//$filelist = glob($FOLDER.'*June.xlsx');

		foreach($filelist as $fileName) {

			$inputFileType = PHPExcel_IOFactory::identify($fileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($fileName);

			//$filecontent = file_get_contents($file);

			// convert dates to correct format.
			for ($i = 2; $i < $objPHPExcel->getActiveSheet()->getHighestRow(); $i++) {

				/*$objPHPExcel->getActiveSheet()->getStyle('C'.$i)
					->getNumberFormat()
					->setFormatCode(
						PHPExcel_Style_NumberFormat::FORMAT_TEXT
					);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$i)
					->getNumberFormat()
					->setFormatCode(
						PHPExcel_Style_NumberFormat::FORMAT_TEXT
					);

				$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(2, $i, PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue()), PHPExcel_Cell_DataType::TYPE_STRING);
				$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(3, $i, PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue()), PHPExcel_Cell_DataType::TYPE_STRING);*/

				//echo $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue().PHP_EOL;
				//echo date("Y-m-d",strtotime(str_replace("/","-",$objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue()))),
				//die(date("Y-m-d",PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue())));

				$masterURN = getMasterURN($objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue());

				$houseident = substr($masterURN,0,3);

				if(in_array($houseident,$houseIDs)){
					// probably a house not on the system, just knock off the first 3 chars.
					$masterURN = substr($masterURN,3);
				}

				//$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(1, $i, PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue()), PHPExcel_Cell_DataType::TYPE_STRING);
				//$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(2, $i, strtotime(str_replace("/","-",$objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue())), PHPExcel_Cell_DataType::TYPE_STRING);

				dbq("insert into organization_sales (
							school_urn,
							dispatched_date,
							order_number,
							style_ref,
							EAN,
							quantity,
							price_paid,
							donation_amount
						  ) VALUES (
							:SCHOOL_URN,
							:DISPATCHED_DATE,
							:ORDER_NUMBER,
							:STYLE_REF,
							:EAN,
							:QUANTITY,
							:PRICE_PAID,
							:DONATION_AMOUNT
						  )",
					[
						':SCHOOL_URN' => $masterURN,
						':DISPATCHED_DATE' => date("Y-m-d",strtotime(str_replace("/","-",$objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue()))),
						':ORDER_NUMBER' => $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue(),
						':STYLE_REF' => $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue(),
						':EAN' => $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue(),
						':QUANTITY' => $objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue(),
						':PRICE_PAID' => $objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue(),
						':DONATION_AMOUNT' => $objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue()
					]
				);
			}

			//$unixTimeStamp = PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCell('B2')->getValue());

//			echo $unixTimeStamp;

			/*$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
			$objWriter->save($fileName . '.csv');*/
/*
			$inputFileType = null;
			$objReader = null;
			$objPHPExcel = null;
			$objWriter = null;

			$csv = array_map('str_getcsv', file($fileName.'.csv'));

			$headerKeys = [];
			$headers = [];
			$headerDone = false;



			foreach($csv as $csvLine){
				if(!$headerDone){
					$headerKeys = $csvLine;
					$headers = array_flip($csvLine);
					$headerDone = true;
					continue; // skip ver the rest.
				}

				if(empty($csvLine[$headers['SCHOOL_ID']])){
					break; // reached the end of the csv
				}

				$masterURN = getMasterURN($csvLine[$headers['SCHOOL_ID']]);

				$houseident = substr($masterURN,0,3);

				if(in_array($houseident,$houseIDs)){
					// probably a house not on the system, just knock off the first 3 chars.
					$masterURN = substr($masterURN,3);
				}

				//$masterOID = getOrgIDFromURN($masterURN);

				//echo $csvLine[$headers['Dispatch Date']].PHP_EOL;
				$orderMonth = date("Y-m",(int)$csvLine[$headers['DESPATCH_DATE']]);

				foreach($csvLine as $key => $value){
					$csvLine[$key] = mb_convert_encoding(trim(addslashes($value)),'ASCII'); // clean the text.  Bloody Excel.
				};

				// Process line

				if(!isset($donations[$masterURN])){
					$donations[$masterURN] = [];
				}

				if(!isset($donations[$masterURN][$orderMonth])){
					$donations[$masterURN][$orderMonth] = [
						'value' => 0,
						'donation' => 0
					];
				}

				$donations[$masterURN][$orderMonth]['value'] += $csvLine[$headers['PRICE_PAID']];
				$donations[$masterURN][$orderMonth]['donation'] += $csvLine[$headers['DONATION']];

			}*/


		}

		return $donations;
	}


	public function importrawparker()
	{

	}

    public function associateplainstock()
    {
		// function to associate plain stock to schools.
		// open CSV file.
		session_destroy(); // close session so we can do other stuff.

		$testFile = 'plainassociations.csv';
		$csv = array_map('str_getcsv', file($testFile));

		$headers = [];
		$headerDone = false;
		$selectedData = [];
		foreach($csv as $csvLine){

			$accountNum = null; // reset account number

			foreach($csvLine as $key => $value){

				if(!$headerDone){
					// fill the headers
					$stylecode = @reset(explode(" ",$value));
					$headers[$key] = $stylecode;
				} else {
					// real data
						if($key == 0){
							// account number
							$accountNum = $value;
							if(!isset($selectedData[$accountNum])){
								$selectedData[$accountNum] = [];
							}
						} elseif($key > 1 && !empty($value)){
							// data
							$selectedData[$accountNum][] =
								$headers[$key];
						}
					}
			}

			$headerDone = true;
			if(!is_null($accountNum) && empty($selectedData[$accountNum])){
				unset($selectedData[$accountNum]);
			}

			if(!$this->schoolExists($accountNum)){
				// no point associating schools we dont know about?
				unset($selectedData[$accountNum]);
			}

		}

$count = 0;
		// ok lets add some see what happens.
		foreach($selectedData as $urn => $stylecodes){

			$schoolStuff = $this->findSchoolID($urn);

			// remove whats there now.  NULL logo id is plain.
			dbq('delete from products_colours_to_organizations where organization_id = :ORGID and organization_logo_id IS NULL',
				[
					':ORGID' => $schoolStuff['organization_id'],
				]);

			foreach($stylecodes as $stylecode){
				//check it exists, get its id.
				if($styleArray = $this->findColourID($stylecode)){
					//

					// insert...
					dbq("insert into products_colours_to_organizations (
									product_colour_id,
									organization_id,
									organization_logo_id
								  ) VALUES (
								  	:PRODCOLID,
								  	:ORGID,
								  	:ORGLOGOID
								  )",
						[
							':PRODCOLID' => $styleArray['product_colour_id'],
							':ORGID' => $schoolStuff['organization_id'],
							':ORGLOGOID' => null
						]
					);


				}
			}
			$count++;

		//	if($count > 2) return ['done'];
		}

 		return ['done'];

	}

	public function shrinkeboslogos()
	{
		session_destroy(); // stop the session so we can keep doing other things...

		ini_set('max_execution_time', 0);
		$logoCount = 0;
		$logosQuery = dbq("SELECT * from organization_logos where logo_url like '%amazonaws%'");

		while ($logos = dbf($logosQuery)) {
			$url = $logos['logo_url'];

			$image = file_get_contents($url);

			$im = new Imagick();
			$im->readImageBlob($image);
			$im->scaleImage(200,200,true); // 200px best fir

			//$filename = basename($url);

			$image = @reset(explode("?", basename($logos['logo_url'])));

			// Save it.
			$im->writeImage('C:\\wamp\\www\\ues\\images\\emblems\\'.$image);

			$newimage = 'images/emblems/'.$image;

			// update dbase.
			dbq("UPDATE organization_logos SET logo_url = :NEWIMG WHERE organization_logo_id = :OLID",
				[
					':NEWIMG' => $newimage,
					':OLID' => $logos['organization_logo_id']
				]
			);
			//if($logoCount > 100){
			//	break;
			//}
			$logoCount++;

		}

	}

	public function updatehousestatus(){
		$schoolHousesQuery = dbq("UPDATE organizations houses
									JOIN organizations o ON (o.`school_URN` = houses.`organization_parent_id`)
									SET houses.`organization_status_id` = o.`organization_status_id`");
	}

	public function copybanks(){
		ini_set('max_execution_time', 0);
		$schoolBanksQuery = dbq("SELECT usd.account, usb.* FROM ues_school_data usd JOIN ues_school_bank_accounts usb ON (usb.`school_id` = usd.`id`)");

		while ($schoolBanks = dbf($schoolBanksQuery)) {
			if(!empty($schoolBanks['account_number'])){
				// find the org this relates to.
				if($organizationID = getOrgIDFromURN($schoolBanks['account'])){
					// add a bank account for them
					dbq("INSERT into organization_bank_accounts (
								organization_id,
								organization_bank_sort_code_1,
								organization_bank_sort_code_2,
								organization_bank_sort_code_3,
								organization_bank_account,
								organization_bank_name,
								organization_bank_address_id
							  ) VALUES (
							  	:ORGANIZATIONID,
								:ORGANIZATIONBANKSORTCODE1,
								:ORGANIZATIONBANKSORTCODE2,
								:ORGANIZATIONBANKSORTCODE3,
								:ORGANIZATIONBANKACCOUNT,
								:ORGANIZATIONBANKNAME,
								:ORGANIZATIONBANKADDRESSID
							  )
 					",
						[
							':ORGANIZATIONID' => $organizationID,
							':ORGANIZATIONBANKSORTCODE1' => $schoolBanks['sort_code_1'],
							':ORGANIZATIONBANKSORTCODE2' => $schoolBanks['sort_code_2'],
							':ORGANIZATIONBANKSORTCODE3' => $schoolBanks['sort_code_3'],
							':ORGANIZATIONBANKACCOUNT' => $schoolBanks['account_number'],
							':ORGANIZATIONBANKNAME' => $schoolBanks['name'],
							':ORGANIZATIONBANKADDRESSID' => null
						]);
				}


			}
		}
		return ['done'];
	}

	public function copyimages()
	{

		ini_set('max_execution_time', 0);

/*		// Products
		$productsQuery = dbq("SELECT * from products");

		while ($product = dbf($productsQuery)) {

			$product['product_image'] = str_replace("/small/","/original/",$product['product_image']);

			$image = @reset(explode("?", basename($product['product_image'])));

			if (!stristr('example-emblem', $product['product_image'])) {

				if(copy($product['product_image'], 'C:\\wamp\\www\\ues\\images\\products\\' . $image)){
					$newimage = 'images/products/'.$image;
					dbq("UPDATE products SET product_image = :NEWIMG WHERE product_id = :PID",
						[
							':NEWIMG' => $newimage,
							':PID' => $product['product_id']
						]
					);
				}

			}

		}*/


		// Products
		$productsColoursQuery = dbq("SELECT * from product_colours");

		while ($productsColours = dbf($productsColoursQuery)) {

			$productsColours['product_colour_image_url'] = str_replace("/small/","/original/",$productsColours['product_colour_image_url']);

			$image = @reset(explode("?", basename($productsColours['product_colour_image_url'])));

			if (!stristr('example-emblem', $productsColours['product_colour_image_url'])) {

				if(copy($productsColours['product_colour_image_url'], 'C:\\wamp\\www\\ues\\images\\products\\' . $image)){
					$newimage = 'images/products/'.$image;
					dbq("UPDATE product_colours SET product_colour_image_url = :NEWIMG WHERE product_colour_id = :PID",
						[
							':NEWIMG' => $newimage,
							':PID' => $productsColours['product_colour_id']
						]
					);
				}

			}

		}

	}

	public function updatelogousage(){

		// Need to set them all to 0.

		$logoQuery = dbq("SELECT ol.*, COUNT(pc.products_colours_to_organizations_id) as realusing FROM organization_logos ol
								JOIN products_colours_to_organizations pc USING (organization_logo_id)
								GROUP BY ol.`organization_logo_id`");

		dbq("update organization_logos set products_using_logo = 0");

		if(dbnr($logoQuery) > 0){
			while($logo = dbf($logoQuery)){
				dbq("update organization_logos set products_using_logo = :PUSING where organization_logo_id = :ORGLID",[
					':PUSING' => $logo['realusing'],
					':ORGLID' => $logo['organization_logo_id']
				]);
			}

		}
	}

	public function updateproductusage(){

		$productQuery = dbq("SELECT * from product_colours");

		if(dbnr($productQuery) > 0){
			while($product = dbf($productQuery)){
				// count entried in product_colours_to_organizations.
				$pc2oQuery = dbq("select * from products_colours_to_organizations where product_colour_id = :PCID",
					[
						':PCID' => $product['product_colour_id']
					]
				);

				echo $product['product_colour_style_ref'].'-'.dbnr($pc2oQuery)."<br/>";
				dbq("update product_colours set product_colour_school_count = :SCOUNT where product_colour_id = :PCID",
					[
						':SCOUNT' => dbnr($pc2oQuery),
						':PCID' => $product['product_colour_id']
					]
				);
			}

		}
	}

	public function updateebosprocessing(){

		ini_set('max_execution_time', 0);
		session_destroy(); // stop the session so we can keep doing other things...

		$deadLogoQuery = dbq("SELECT  * from organization_logos where logo_url = '/assets/processing.png'");

		if(dbnr($deadLogoQuery) > 0){
			include(DIR_CLASSES . 'eBOS_API.php');

			$eBOS_API = new eBOS_API(APIUser, APIPass);

			while($deadLogo = dbf($deadLogoQuery)){

				// update ebos
				$designs = $eBOS_API->getDesigns($deadLogo['tesco_style_ref']);

				if (!empty($designs)) {
					// check the current logo.
					//echo "<pre>".print_r($designs,1)."</pre>"; die;
					foreach ($designs->designs as $design) {
						//sleep(1); //needs a delay for some reason.
						$designDetail = $eBOS_API->getDesignDetails($design->id);
						// check that its still the same.
						if($designDetail->design->logo->png_url == '/assets/processing.png'){
							// do an update to try and push a png change.

							$designupdatedetail = [
									 'design' => [
										 'number' => $designDetail->design->number, # required
										 'name' => $designDetail->design->name, # required
										 'notes' => $designDetail->design->notes." +update"
									 ]
								 ];
//return $designupdatedetail;
							$updateResult = $eBOS_API->updateDesign($design->id,$designupdatedetail);
							//return (array)$updateResult;

						} else {
							// update to the new one.
							dbq("update organization_logos set logo_url = :NEWURL where organization_logo_id = :OLID",
								[
									':NEWURL' => $designDetail->design->logo->png_url,
									':OLID' => $deadLogo['organization_logo_id']
								]
							);
						}

					}
				}
			}

		}
	}

	public function keepoldstock()
	{
		// function to associate plain stock to schools.
		ini_set('max_execution_time',30000);
		session_destroy(); // close session so we can do other stuff.

		// open CSV file.
		$testFile = 'embassociations.csv';
		$csv = array_map('str_getcsv', file($testFile));

		$headers = [];
		$headerDone = false;
		$selectedData = [];

		$codesStartColumn = 10;

		foreach($csv as $csvLine) {

			foreach($csvLine as $line){
				$codesStartColumn--;
				if($codesStartColumn < 0) {
					// insert.
					$parts = explode(' ',$line,2);
					dbq("insert into export_products (
								tesco_style_code,
								tesco_style_name,
								decorated
							  ) VALUES (
							  	:TESCO_STYLE_CODE,
								:TESCO_STYLE_NAME,
								:DECORATED
							  ) ",
						[
							':TESCO_STYLE_CODE' => $parts[0],
							':TESCO_STYLE_NAME' => $parts[1],
							':DECORATED' => 1,
						]
					);
				};
			}
			// Only care about first line.
			break;
		}
		//die;
		// Plain
		$testFile = 'plainassociations.csv';
		$csv = array_map('str_getcsv', file($testFile));

		$headers = [];
		$headerDone = false;
		$selectedData = [];

		$codesStartColumn = 2;

		foreach($csv as $csvLine){
			foreach($csvLine as $line){
				$codesStartColumn--;
				if($codesStartColumn < 0) {
					// insert.
					$parts = explode(' ',$line,2);
					dbq("insert into export_products (
								tesco_style_code,
								tesco_style_name,
								decorated
							  ) VALUES (
							  	:TESCO_STYLE_CODE,
								:TESCO_STYLE_NAME,
								:DECORATED
							  ) ",
						[
							':TESCO_STYLE_CODE' => $parts[0],
							':TESCO_STYLE_NAME' => $parts[1],
							':DECORATED' => 0,
						]
					);
				};
			}
			// Only care about first line.
			break;
		}
	}

	public function updatedefaultlogos()
	{
		// function to associate plain stock to schools.
		ini_set('max_execution_time',30000);
		session_destroy(); // close session so we can do other stuff.

		// open CSV file.
		$testFile = 'embassociations.csv';
		$csv = array_map('str_getcsv', file($testFile));

		$headers = [];
		$headerDone = false;
		$selectedData = [];
		foreach($csv as $csvLine){

			$accountNum = null; // reset account number
			if(!$headerDone){
				$headerDone = true;
				continue;
			}

			$accountNum = $csvLine[1]; // reset account number
			$accountLogo = $csvLine[2];
			$orgID = getOrgIDFromURN($accountNum);

			$logoQuery = dbq("select organization_logo_id from organization_logos where organization_id = :ORGID and tesco_style_ref = :EBOSREF",
				[
					':ORGID' => $orgID,
					':EBOSREF' => $accountLogo
				]
			);

			if(dbnr($logoQuery) > 0){
				// found it
				$logo = dbf($logoQuery);

				dbq("update organizations set organization_default_logo_id = :LOGOID where organization_id = :ORGID",
					[
						':LOGOID' => $logo['organization_logo_id'],
						':ORGID' => $orgID
					]
				);

			} else {
				//leave alons.
				echo $accountLogo." not found \n";
			}
		}

		return ['done'];

	}

	public function associateembstock()
	{
		// function to associate plain stock to schools.
		ini_set('max_execution_time',30000);
		session_destroy(); // close session so we can do other stuff.

		// open CSV file.
		$testFile = 'embassociations.csv';
		$csv = array_map('str_getcsv', file($testFile));

		$headers = [];
		$headerDone = false;
		$selectedData = [];
		foreach($csv as $csvLine){

			$accountNum = null; // reset account number

			foreach($csvLine as $key => $value){

				if(!$headerDone){
					// fill the headers
					$stylecode = @reset(explode(" ",$value));
					$headers[$key] = $stylecode;
				} else {
					// real data
					if($key == 1){
						// account number
						$accountNum = $value;
						if(!isset($selectedData[$accountNum])){
							$selectedData[$accountNum] = [];
						}
					} elseif($key > 9 && !empty($value)){
						// data
						// create logo details too.
						$selectedData[$accountNum][] =
							$headers[$key]."|".$value;
					}
				}

			}

			$headerDone = true;
			if(!is_null($accountNum) && empty($selectedData[$accountNum])){
				unset($selectedData[$accountNum]);
			}

			if(!$this->schoolExists($accountNum)){
				// no point associating schools we dont know about
				unset($selectedData[$accountNum]);
			}

		}

		//return $selectedData;

		$count = 0;
		// ok lets add some see what happens.
		foreach($selectedData as $urn => $stylecodes){

			//if($urn != "902990000747"){
			//	continue;
			//	}

			$schoolStuff = $this->findSchoolID($urn);

			//return($schoolStuff);
			// remove whats there now.  NOT NULL logo id is embroidered.
			dbq('delete from products_colours_to_organizations where organization_id = :ORGID and organization_logo_id IS NOT NULL',
				[
					':ORGID' => $schoolStuff['organization_id'],
				]
			);

			foreach($stylecodes as $stylecodeandLogo){

				list($stylecode,$logo) = explode("|",$stylecodeandLogo);

				$masterURN = getMasterURN($urn);

				$logoref = $masterURN.'-'.$logo;
				//$this->findLogoID();

				$logoID = $this->findLogoID($logoref);
				// Find the logo?  Logo not there?

				//return $logoID;
				//check it exists, get its id.
				if($styleArray = $this->findColourID($stylecode)){
					// only insert if logoID and Style ID are both valid

					if($logoID){
						// insert...
						dbq("insert into products_colours_to_organizations (
									product_colour_id,
									organization_id,
									organization_logo_id
								  ) VALUES (
								  	:PRODCOLID,
								  	:ORGID,
								  	:ORGLOGOID
								  )",
							[
								':PRODCOLID' => $styleArray['product_colour_id'],
								':ORGID' => $schoolStuff['organization_id'],
								':ORGLOGOID' => $logoID['organization_logo_id']
							]
						);
					}

				}
			}
			$count++;

		//	if($count > 1) return ['done'];
		}

		return ['done'];

	}

	public function copyusers()
	{
		// create audit list for IDs.
		ini_set('max_execution_time', 7000);


		$userQuery = dbq("SELECT * from ues_users uu
 							join organizations o on (o.school_URN = uu.school_urn)
 							order by uu.school_urn");

		while($user = dbf($userQuery)){
			// check if the user is already on the org?
			$checkUserOrgQuery = dbq("select * from users u
									join users_to_organizations u2o using (user_id)
									where u.user_email = :EMAIL
									and u2o.organization_id = :ORGID",
				[
					':EMAIL' => $user['email'],
					':ORGID' => $user['organization_id']
				]
			);

			if(dbnr($checkUserOrgQuery) == 0){
				// user not there..  Check if they have an account on any other account?

				$checkUserOtherOrgQuery = dbq("select * from users u
									where u.user_email = :EMAIL",
					[
						':EMAIL' => $user['email']
					]
				);

				if(dbnr($checkUserOtherOrgQuery) > 0){
					// already exists.  grab the ID.
					$checkUserOtherOrg = dbf($checkUserOtherOrgQuery);
					$userID = $checkUserOtherOrg['user_id'];

				} else {
					// dont exist, add it in.

					dbq("INSERT INTO users
						(
							user_id,
							user_type_id,
							user_default_address_id,
							user_name,
							user_first_name,
							user_last_name,
							user_email,
							user_password,
							user_avatar,
							user_work_phone,
							user_mobile_phone,
							user_home_phone,
							user_home_email,
							user_title_id,
							user_salutation
						) VALUES (
							:USERID,
							:USERTYPEID,
							:USERDEFAULTADDRESSID,
							:USERNAME,
							:USERFIRSTNAME,
							:USERLASTNAME,
							:USEREMAIL,
							:USERPASSWORD,
							:USERAVATAR,
							:USERWORKPHONE,
							:USERMOBILEPHONE,
							:USERHOMEPHONE,
							:USERHOMEEMAIL,
							:USERTITLEID,
							:USERSALUTATION
						);",
						[
							':USERID' => null,
							':USERTYPEID' => 1, // organization
							':USERDEFAULTADDRESSID' => $user['organization_default_address_id'],
							':USERNAME' => $user['full_name'],
							':USERFIRSTNAME' => $user['first_name'],
							':USERLASTNAME' => $user['last_name'],
							':USEREMAIL' => $user['email'],
							':USERPASSWORD' => '$2y$10$jnKCsKF1o.j/u3peRASaB.QI/F84gTcshpkqq45K97//NPbouiBgq',
							':USERAVATAR' => null,
							':USERWORKPHONE' => null,
							':USERMOBILEPHONE' => null,
							':USERHOMEPHONE' => null,
							':USERHOMEEMAIL' => null,
							':USERTITLEID' => 1,
							':USERSALUTATION' => $user['full_name']
						]);

					$userID = dbid();
				}

				// add the user to the new org!
				dbq("
					insert into users_to_organizations (
						user_id,
						organization_id
					) values (
						:USERID,
						:ORGID
					)
				",[
					':USERID' => $userID,
					':ORGID' => $user['organization_id']
				]);

				//return $user;
			}

		}
		return ['done'];
	}

	public function getmissinglogos(){

		ini_set('max_execution_time', 7000);

		$missingLogoQuery = dbq("SELECT * FROM logos WHERE logo_code NOT IN (SELECT tesco_style_ref FROM organization_logos) AND logo_school_id IN (SELECT school_URN FROM organizations) GROUP BY logo_code
 									");
		//$missingLogoQuery = dbq("SELECT * FROM ues_school_logos_real WHERE tesco_style_ref NOT IN (SELECT tesco_style_ref FROM organization_logos) AND school_id IN (SELECT school_URN FROM organizations) AND approved = 1 GROUP BY tesco_style_ref
		$data = [];

		// Get the logos from ebos.  7000 logos.
		ini_set('max_execution_time', 0);

		include(DIR_CLASSES . 'eBOS_API.php');

		$eBOS_API = new eBOS_API(APIUser, APIPass);

		while($missingLogo = dbf($missingLogoQuery)) {
			$schoolURN = @reset(explode("-",$missingLogo['logo_code']));
			$organizationID = getOrgIDFromURN($schoolURN);

			// check they are in exit_data
			if(!inExit($schoolURN)){
				// Dont care about you.
				continue;
			}

			// check if its on ebos now?
			$designs = $eBOS_API->getDesigns($missingLogo['logo_code']);
			//return (array)$designs->designs;
			if (!empty($designs->designs) && $organizationID) {
				// Add logos to database.


				 foreach ($designs->designs as $design) {

					$designDetail = $eBOS_API->getDesignDetails($design->id);
					// echo "<pre>".print_r($designDetail,1)."</pre>"; die;
					$designArray[] = $designDetail;
					// Add to dbase?
					if (is_object($designDetail) &&
						is_object($designDetail->design) ) {

						if (@is_null($designDetail->design->destroyed_at)) { // Not deleted.
							// make the threads.
							//return (array)$designDetail->design;
							$pngLink = $designDetail->design->logo->png_url;

							$svgasPNG = $pngLink;// str_replace("original/" . $designDetail->design->number . ".svg", "origin_png/" . $designDetail->design->number . ".png", $designDetail->design->logo->url);

							// what format is it?
							if(file_exists('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.jpg')){
								$file = imagecreatefromjpeg('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.jpg');
								$rgb = imagecolorat($file, 2, 2);
								$r = ($rgb >> 16) & 0xFF;
								$g = ($rgb >> 8) & 0xFF;
								$b = $rgb & 0xFF;
							} elseif(file_exists('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.JPG')){
								$file = imagecreatefromjpeg('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.JPG');
								$rgb = imagecolorat($file, 2, 2);
								$r = ($rgb >> 16) & 0xFF;
								$g = ($rgb >> 8) & 0xFF;
								$b = $rgb & 0xFF;
							} else {
								// grey as standard.
								$r = 127;
								$g = 127;
								$b = 127;
							}

							$image = file_get_contents($svgasPNG);

							$im = new Imagick();
							$im->readImageBlob($image);
							$im->scaleImage(200,200,true); // 200px best fir

							//$filename = basename($url);

							$image = @reset(explode("?", basename($svgasPNG)));

							// Save it.
							$im->writeImage('C:\\wamp\\www\\ues\\images\\emblems\\'.$image);

							$svgasPNG = 'images/emblems/'.$image;

							//update the logo background?
							//dbq("UPDATE organization_logos SET primary_background_colour = :BACKGROUND WHERE organization_logo_id = :OLID", [':BACKGROUND' => $r . ',' . $g . ',' . $b, ':OLID' => $logoOrg['organization_logo_id']]);
							//return $r.','.$g.','.$b;
							$logoOrgQuery = dbq("select * from organizations where organization_id = :ORGID",
								[
									':ORGID' => $organizationID
								]
							);
							$logoOrg = dbf($logoOrgQuery);

							dbq("INSERT INTO organization_logos
								(
									organization_logo_id,
									organization_unit_id,
									organization_id,
									school_name,
									tesco_style_ref,
									tesco_alt_style_code,
									product_group,
									product_name__colour,
									emblem_code,
									stitch_count,
									size__mm_,
									position,
									thread_1,
									thread_2,
									thread_3,
									thread_4,
									thread_5,
									thread_6,
									thread_7,
									thread_8,
									thread_9,
									thread_10,
									thread_11,
									thread_12,
									thread_13,
									thread_14,
									thread_15,
									logo_approved,
									logo_url,
									logo_svg_url,
									primary_background_colour
								) VALUES (
									:ORGANIZATIONLOGOID,
									:ORGANIZATIONUNITID,
									:ORGANIZATIONID,
									:SCHOOLNAME,
									:TESCOSTYLEREF,
									:TESCOALTSTYLECODE,
									:PRODUCTGROUP,
									:PRODUCTNAMECOLOUR,
									:EMBLEMCODE,
									:STITCHCOUNT,
									:SIZEMM,
									:POSITION,
									:THREAD1,
									:THREAD2,
									:THREAD3,
									:THREAD4,
									:THREAD5,
									:THREAD6,
									:THREAD7,
									:THREAD8,
									:THREAD9,
									:THREAD10,
									:THREAD11,
									:THREAD12,
									:THREAD13,
									:THREAD14,
									:THREAD15,
									:LOGOAPPROVED,
									:LOGOURL,
									:LOGOSVGURL,
									:PRIMARYBACKGROUNDCOLOUR
								);",
								[
									':ORGANIZATIONLOGOID' => null,
									':ORGANIZATIONUNITID' => null,
									':ORGANIZATIONID' => $organizationID,
									':SCHOOLNAME' => $designDetail->design->name,
									':TESCOSTYLEREF' => $designDetail->design->number,
									':TESCOALTSTYLECODE' => null,
									':PRODUCTGROUP' => null,
									':PRODUCTNAMECOLOUR' => null,
									':EMBLEMCODE' => null,
									':STITCHCOUNT' => $designDetail->design->stitch_count,
									':SIZEMM' => $designDetail->design->width . 'x' . $designDetail->design->height,
									':POSITION' => null,
									':THREAD1' => null,
									':THREAD2' => null,
									':THREAD3' => null,
									':THREAD4' => null,
									':THREAD5' => null,
									':THREAD6' => null,
									':THREAD7' => null,
									':THREAD8' => null,
									':THREAD9' => null,
									':THREAD10' => null,
									':THREAD11' => null,
									':THREAD12' => null,
									':THREAD13' => null,
									':THREAD14' => null,
									':THREAD15' => null,
									':LOGOAPPROVED' => 1,
									':LOGOURL' => $svgasPNG,
									':LOGOSVGURL' => $designDetail->design->logo->url,
									':PRIMARYBACKGROUNDCOLOUR' => $r . ',' . $g . ',' . $b,
								]
							);

							$logoID = dbid();

							if (is_null($logoOrg['organization_default_logo_id'])) {
								// update default logo
								dbq("UPDATE organizations SET organization_default_logo_id = :LOGOID WHERE organization_id = :ORGID", [':LOGOID' => $logoID, ':ORGID' => $logoOrg['organization_id']]);
							}
						}

					} else {
						// 500 error?
						return (array)$designDetail;
					}
					 break;
				}

			} else {
				// its empty?  No designs for this school?
				// need to create this on ebos.
				$ftp_server="ftp.slickstitch.com";
				$ftp_user="tesco";
				$ftp_pass="q46ovvB";

				$conn_id = ftp_connect($ftp_server);

				$login_result = ftp_login($conn_id, $ftp_user, $ftp_pass);

				ftp_pasv($conn_id, true);

				$remote_path = "/Tesco/Approved Schools/" . $schoolURN . "/" . $missingLogo['logo_code'] . ".dst";	// Is this case sensitive?
				$remote_path_caps = "/Tesco/Approved Schools/" . $schoolURN . "/" . $missingLogo['logo_code'] . ".DST";
				$local_path = "dst/".$missingLogo['logo_code'].".dst";

				// check if the DST is there.
				$logoRetrieved = false;

				if (@ftp_get($conn_id, $local_path, $remote_path, FTP_BINARY)) {
					$logoRetrieved = true;
				} elseif (@ftp_get($conn_id, $local_path, $remote_path_caps, FTP_BINARY)) {
					$logoRetrieved = true;
				}

				//echo $local_path; die;
				if($logoRetrieved && file_exists($local_path)){
					// upload it.

					$designdetail = [
						'design' => [
							'number' => $missingLogo['logo_code'],  // random string for testing.  Must be unique - No Spaces.
							'name' => $missingLogo['logo_school'],  // random name for testing
							'notes' => 'Created by F&F UES',
							'threads' => explode(',', $missingLogo['logo_thread_array']), // Must be strings not integers.
							'dst' => [
								'data' => base64_encode(file_get_contents($local_path)),
								'filename' => $missingLogo['logo_code'].'.dst'
							]
						]
					];

					echo "<pre>".print_r($designdetail,1)."</pre>";

					//return $designdetail;
					$newDesign = $eBOS_API->createDesign($designdetail);

					// if it worked, add entries to organization logos.
					if(!isset($newDesign->error)){
						// worked.

					} else {
						$data[$missingLogo['logo_code']] = 'didnt work';
					}
					// return $newDesign;
					//echo "<pre>".print_r($newDesign,1)."</pre>";
				}

			}

		}

		return $data;
	}

	public function checktemp(){
		ini_set('max_execution_time', 0);

		include(DIR_CLASSES . 'eBOS_API.php');

		$eBOS_API = new eBOS_API(APIUser, APIPass);

		$designs1 = $eBOS_API->getDesigns('-TMP',1,1000);
		//echolog($designs1);
		sleep(1);
		$designs2 = $eBOS_API->getDesigns('-TMP',2,1000);
		//echolog($designs2);
		sleep(1);
		$designs3 = $eBOS_API->getDesigns('-TMP',3,1000);
		//echolog($designs3);
		sleep(1);
		$designs4 = $eBOS_API->getDesigns('-TMP',4,1000);
		//echolog($designs4);
		sleep(1);

		$designs = (object) array_merge((array) $designs1->designs, (array) $designs2->designs);
		//echolog($designs);
		$designs = (object) array_merge((array) $designs, (array) $designs3->designs);
		//echolog($designs);
		$designs = (object) array_merge((array) $designs, (array) $designs4->designs);
		//echolog($designs);



		// loop through and see if its TMP in our database.
		foreach($designs as $design){
			// Lookup in dbase.
			$logoQuery = dbq("select * from organization_logos where ebos_id = :EBOSID",
			 	[
					':EBOSID' => $design->id
				]
			);

			if(dbnr($logoQuery) > 0){
				$logo = dbf($logoQuery);
				if($logo['tesco_style_ref'] != $design->number){
					// needs to update eBOS.
					dbq("update organization_logos set needs_to_update_ebos = 1 where organization_logo_id = :OLID",
						[
							':OLID' => $logo['organization_logo_id']
						]
					);
				}
			}
		}
	}

	public function importeans(){
		// create EAN list from data.
		$vendaInfoQuery = dbq("SELECT * FROM venda_list vl JOIN product_colours pc ON (pc.`product_colour_style_ref` = vl.`stylecode`) WHERE vl.on_venda = 1 GROUP BY vl.ean");

		while($vendaInfo = dbf($vendaInfoQuery)){

			// add entry for product sizes to colours?
			dbq("insert into products_sizes_to_products_colours (
					product_size_id,
					product_colour_id,
					product_variation_sku,
					product_variation_price
				 ) values (
				 	:PRODUCTSIZEID,
					:PRODUCTCOLOURID,
					:PRODUCTVARIATIONSKU,
					:PRODUCTVARIATIONPRICE
				 )",
				[
					':PRODUCTSIZEID' => $vendaInfo['size_id'],
					':PRODUCTCOLOURID' => $vendaInfo['product_colour_id'],
					':PRODUCTVARIATIONSKU' => $vendaInfo['ean'],
					':PRODUCTVARIATIONPRICE' => null
				]
			);
		}

	}

	public function addpolyalternatives(){

		$orgsFound = [];

		$sweatProductArray = [
			'4' => '208', // Girls sweat cardigan
			'2' => '209', // Sweat Roundneck
			'123' => '210'  // Sweat V-Neck
		];

		foreach($sweatProductArray as $oldID => $newID){
			// get old ID in all colours.
			$coloursQuery = dbq("select * from product_colours pc join products p on p.product_id = pc.product_id where pc.product_id = :PRODID",
				[
					':PRODID' => $oldID
				]
			);

			while($colours = dbf($coloursQuery)){
				// check if there is a polyc version?
				$polySearch = dbq("select * from product_colours pc join products p on p.product_id = pc.product_id join colours c on c.colour_id = pc.colour_id where pc.product_id = :NEWPRODID and pc.colour_id = :COLOURID",
					[
						':NEWPRODID' => $newID,
						':COLOURID' => $colours['colour_id']
					]
				);
				if(dbnr($polySearch) > 0){
					// there is a polyc version of this item.
					// Select all schools who have this non poly version.
					$poly = dbf($polySearch);

					$schoolUsage = dbq("select pc2o.*, ol.tesco_style_ref from products_colours_to_organizations pc2o join organization_logos ol on ol.organization_logo_id = pc2o.organization_logo_id where pc2o.product_colour_id = :PCID",
						[
							':PCID' => $colours['product_colour_id']
						]
					);

					while($school = dbf($schoolUsage)){
						// return($school);
						// insert the pol item into their selection.

						// Check its not already there.
						$newCheck = dbq("SELECT * from products_colours_to_organizations WHERE
											product_colour_id = :PCID AND
											organization_id = :OID AND
											organization_logo_id = :OLID",
							[
								':PCID' => $poly['product_colour_id'],
								':OID' => $school['organization_id'],
								':OLID' => $school['organization_logo_id']
							]
						);

						if(dbnr($newCheck) == 0){
							// Not already selected by them.
							dbq("insert into products_colours_to_organizations (
									product_colour_id,
									organization_id,
									organization_logo_id
								) VALUES (
									:PCID,
									:OID,
									:OLID
								)",
								[
									':PCID' => $poly['product_colour_id'],
									':OID' => $school['organization_id'],
									':OLID' => $school['organization_logo_id']
								]
							);

							$masterURN = getMasterOrgID($school['organization_id']);
							if(!isset($orgsFound[$masterURN])){
								dbq("UPDATE organizations set has_sweats = 1 where organization_id = :OID",
									[
										':OID' => $masterURN
									]
								);

								$orgsFound[$masterURN] = $masterURN;
							}

							// Add an audit entry.
							audit(3,$school['organization_id'],$poly['product_name'].' ('.$poly['colour_name'].') - '.$school['tesco_style_ref']);
							//die;
						}
					}
				}
			}
		}

		return $sweatProductArray;
	}

	public function addsweatorg(){

		$orgsFound = [];
		$sweatProductArray = [
			'4' => '208', // Girls sweat cardigan
			'2' => '209', // Sweat Roundneck
			'123' => '210'  // Sweat V-Neck
		];

		foreach($sweatProductArray as $oldID => $newID){
			// get old ID in all colours.
			$coloursQuery = dbq("select * from product_colours pc join products p on p.product_id = pc.product_id where pc.product_id = :PRODID",
				[
					':PRODID' => $oldID
				]
			);

			while($colours = dbf($coloursQuery)){
				// check if there is a polyc version?
				$polySearch = dbq("select * from product_colours pc join products p on p.product_id = pc.product_id join colours c on c.colour_id = pc.colour_id where pc.product_id = :NEWPRODID and pc.colour_id = :COLOURID",
					[
						':NEWPRODID' => $newID,
						':COLOURID' => $colours['colour_id']
					]
				);
				if(dbnr($polySearch) > 0){
					// there is a polyc version of this item.
					// Select all schools who have this non poly version.
					$poly = dbf($polySearch);

					$schoolUsage = dbq("select pc2o.*, ol.tesco_style_ref from products_colours_to_organizations pc2o join organization_logos ol on ol.organization_logo_id = pc2o.organization_logo_id where pc2o.product_colour_id = :PCID",
						[
							':PCID' => $colours['product_colour_id']
						]
					);

					while($school = dbf($schoolUsage)){
						// return($school);
						// insert the pol item into their selection.
						$masterURN = getMasterOrgID($school['organization_id']);
						if(!isset($orgsFound[$masterURN])){
							dbq("UPDATE organizations set has_sweats = 1 where organization_id = :OID",
								[
									':OID' => $masterURN
								]
							);

							$orgsFound[$masterURN] = $masterURN;
						}

						// Add an audit entry.
						//audit(3,$school['organization_id'],$poly['product_name'].' ('.$poly['colour_name'].') - '.$school['tesco_style_ref']);
						//die;
					}
				}
			}
		}

		return $sweatProductArray;
	}

	public function fixeans(){
		$productQuery = dbq("select * from products_sizes_to_products_colours");

		while($product = dbf($productQuery)){
			dbq("UPDATE products_sizes_to_products_colours set product_variation_sku = :SKU where products_sizes_to_colours_id = :ID",
				[
					':SKU' => trim($product['product_variation_sku']),
					':ID' => $product['products_sizes_to_colours_id']
				]
			);
		}
	}

	public function updatelogometrics(){

		ini_set('max_execution_time', 14000);
		session_destroy(); // allow other things to run.

		$ebosLogoQuery = dbq("SELECT * FROM organization_logos WHERE ebos_id is null AND `logo_approved` = 1");

		include(DIR_CLASSES . 'eBOS_API.php');

		$eBOS_API = new eBOS_API(APIUser, APIPass);

		while($ebosLogo = dbf($ebosLogoQuery)){

			// update id on database.

			$designs = $eBOS_API->getDesigns($ebosLogo['tesco_style_ref']);

			if (!empty($designs->designs)) {
				// Add logos to database.

				foreach ($designs->designs as $design) {
					$designDetail = $eBOS_API->getDesignDetails($design->id);

					// make the threads
					$threadArray = [];
					foreach($designDetail->design->threads as $thread){
						$threadArray[] = $thread->code;
					}

					dbq("update organization_logos set
							  ebos_id = :EBOSID,
							  stitch_count = :STITCHCOUNT,
							  thread_array = :THREADARRAY,
							  logo_colour_count = :COLOURCOUNT
						  where
						  	organization_logo_id = :ORGLID",
						[
							':EBOSID' => $design->id,
							':STITCHCOUNT' => $designDetail->design->stitch_count,
							':THREADARRAY' => implode(',',$threadArray),
							':COLOURCOUNT' => sizeof($threadArray),
							':ORGLID' => $ebosLogo['organization_logo_id'],
						]
					);
					//return (array)$designDetail;
					$designDetail = null;
				}
			}
		}
	}

	public function setdefaultlogos(){

		$orgQuery = dbq("SELECT * FROM organizations WHERE organization_status_id = 3"); // live

		while($org = dbf($orgQuery)) {
			// TEST
			// are there products with logos for this one?  HAVE to be if live.  Use the logo that is used the most.
			$logoQuery = dbq("SELECT ol.organization_logo_id,  ol.`tesco_style_ref`, COUNT(ol.`tesco_style_ref`) AS logoused FROM products_colours_to_organizations pc2o
									JOIN organization_logos ol USING (organization_logo_id)
									WHERE pc2o.organization_id = :ORGID
									GROUP BY ol.`tesco_style_ref`
									ORDER BY logoused DESC
									",
				[
					':ORGID' => $org['organization_id']
				]
			);

			if(dbnr($logoQuery) > 0){
				$logos = dbf($logoQuery);
				// update default
				dbq("update organizations set organization_default_logo_id = :LOGOID, organization_default_logo_updated = NOW() where organization_id = :ORGID",
					[
						':LOGOID' => $logos['organization_logo_id'],
						':ORGID' => $org['organization_id']
					]
				);

			} else {
				// nothing.  could this be a house thing?  If its a house with nothing in it, use the main org default logo?
				$parentOrg = getOrgDetails(getMasterOrgID($org['organization_id']));

				if($parentOrg['organization_id'] != $org['organization_id']){
					// this is a house with no products.
					dbq("update organizations set organization_default_logo_id = :LOGOID, organization_default_logo_updated = NOW() where organization_id = :ORGID",
						[
							':LOGOID' => $parentOrg['organization_default_logo_id'],
							':ORGID' => $org['organization_id']
						]
					);
				}

				// what if its not a house but a main org with houses?  does it matter?

			}

		}
		return null;
	}

	public function updateorgemails(){

		$orgQuery = dbq("SELECT * FROM organizations WHERE organization_email is null or organization_email = ''");

		while($org = dbf($orgQuery)) {
			// get the first contact.
			$userQuery = dbq("select * from users join users_to_organizations u2o using (user_id) where organization_id = :ORGID order by u2o.user_to_organization_id ASC",
				[
					':ORGID' => getMasterOrgID($org['organization_id'])
				]
			);

			$user = dbf($userQuery);
			dbq('update organizations set organization_email = :EMAIL where organization_id = :ORGID',
				[
					':EMAIL' => $user['user_email'],
					':ORGID' => $org['organization_id']
				]
			);
		}

	}

	public function updatehousestats(){

		$parentQuery = dbq("SELECT * FROM organizations WHERE organization_has_subunits = 1");

		$data = [];

		while($parent = dbf($parentQuery)) {
			// update houses.
			dbq("update organizations o set
						o.organization_can_bulk = :CANBULK,
						o.organization_status_id = :STATUSID,
						o.organization_form_date = :FORMDATE,
						o.organization_form_signed = :FORMSIGNED
						where o.organization_parent_organization_id = :ORGID",
				[
					':CANBULK' => $parent['organization_can_bulk'],
					':STATUSID' => $parent['organization_status_id'],
					':FORMDATE' => $parent['organization_form_date'],
					':FORMSIGNED' => $parent['organization_form_signed'],
					':ORGID' => $parent['organization_id']
				]
			);
		}
	}

	public function editfolders(){
		$folders = glob('unapproved_logo_files/*');
		foreach($folders as $folder){
			chmod($folder,0777); // allow us to write to it from ftp.
		}

		return $folders;
		/*if(!file_exists('unapproved_logo_files/'.$orgDetail['school_URN'])){
			mkdir('unapproved_logo_files/'.$orgDetail['school_URN']);
			chmod('unapproved_logo_files/'.$orgDetail['school_URN'],0777); // allow us to write to it from ftp.
		}*/
	}

	public function getmissinglogosb(){

		ini_set('max_execution_time', 7000);

		//$missingLogoQuery = dbq("SELECT * FROM logos WHERE logo_code NOT IN (SELECT tesco_style_ref FROM organization_logos) AND logo_school_id IN (SELECT school_URN FROM organizations) GROUP BY logo_code

		$missingLogoQuery = dbq("SELECT * FROM ues_school_logos_real WHERE tesco_style_ref NOT IN (SELECT tesco_style_ref FROM organization_logos) AND school_id IN (SELECT school_URN FROM organizations) AND approved = 1 GROUP BY tesco_style_ref
									");
		$data = [];

		// Get the logos from ebos.  7000 logos.
		ini_set('max_execution_time', 0);

		include(DIR_CLASSES . 'eBOS_API.php');

		$eBOS_API = new eBOS_API(APIUser, APIPass);

		while($missingLogo = dbf($missingLogoQuery)) {
			$schoolURN = @reset(explode("-",$missingLogo['tesco_style_ref']));
			$organizationID = getOrgIDFromURN($schoolURN);

			// check they are in exit_data
			if(!inExit($schoolURN)){
				// Dont care about you.
				continue;
			}

			// check if its on ebos now?
			$designs = $eBOS_API->getDesigns($missingLogo['tesco_style_ref']);
			//return (array)$designs->designs;
			if (!empty($designs->designs) && $organizationID) {
				// Add logos to database.

				foreach ($designs->designs as $design) {

					$designDetail = $eBOS_API->getDesignDetails($design->id);
					// echo "<pre>".print_r($designDetail,1)."</pre>"; die;
					$designArray[] = $designDetail;
					// Add to dbase?
					if (is_object($designDetail) &&
						is_object($designDetail->design) ) {

						if (@is_null($designDetail->design->destroyed_at)) { // Not deleted.
							// make the threads.
							//return (array)$designDetail->design;
							$pngLink = $designDetail->design->logo->png_url;

							$svgasPNG = $pngLink;// str_replace("original/" . $designDetail->design->number . ".svg", "origin_png/" . $designDetail->design->number . ".png", $designDetail->design->logo->url);

							// what format is it?
							if(file_exists('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.jpg')){
								$file = imagecreatefromjpeg('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.jpg');
								$rgb = imagecolorat($file, 2, 2);
								$r = ($rgb >> 16) & 0xFF;
								$g = ($rgb >> 8) & 0xFF;
								$b = $rgb & 0xFF;
							} elseif(file_exists('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.JPG')){
								$file = imagecreatefromjpeg('C:\\wamp\\www\\ues\\images\\ueslogos\\'.$designDetail->design->number.'.JPG');
								$rgb = imagecolorat($file, 2, 2);
								$r = ($rgb >> 16) & 0xFF;
								$g = ($rgb >> 8) & 0xFF;
								$b = $rgb & 0xFF;
							} else {
								// grey as standard.
								$r = 127;
								$g = 127;
								$b = 127;
							}

							$image = file_get_contents($svgasPNG);

							$im = new Imagick();
							$im->readImageBlob($image);
							$im->scaleImage(200,200,true); // 200px best fir

							//$filename = basename($url);

							$image = @reset(explode("?", basename($svgasPNG)));

							// Save it.
							$im->writeImage('C:\\wamp\\www\\ues\\images\\emblems\\'.$image);

							$svgasPNG = 'images/emblems/'.$image;

							//update the logo background?
							//dbq("UPDATE organization_logos SET primary_background_colour = :BACKGROUND WHERE organization_logo_id = :OLID", [':BACKGROUND' => $r . ',' . $g . ',' . $b, ':OLID' => $logoOrg['organization_logo_id']]);
							//return $r.','.$g.','.$b;
							$logoOrgQuery = dbq("select * from organizations where organization_id = :ORGID",
								[
									':ORGID' => $organizationID
								]
							);
							$logoOrg = dbf($logoOrgQuery);

							dbq("INSERT INTO organization_logos
								(
									organization_logo_id,
									organization_unit_id,
									organization_id,
									school_name,
									tesco_style_ref,
									tesco_alt_style_code,
									product_group,
									product_name__colour,
									emblem_code,
									stitch_count,
									size__mm_,
									position,
									thread_1,
									thread_2,
									thread_3,
									thread_4,
									thread_5,
									thread_6,
									thread_7,
									thread_8,
									thread_9,
									thread_10,
									thread_11,
									thread_12,
									thread_13,
									thread_14,
									thread_15,
									logo_approved,
									logo_url,
									logo_svg_url,
									primary_background_colour
								) VALUES (
									:ORGANIZATIONLOGOID,
									:ORGANIZATIONUNITID,
									:ORGANIZATIONID,
									:SCHOOLNAME,
									:TESCOSTYLEREF,
									:TESCOALTSTYLECODE,
									:PRODUCTGROUP,
									:PRODUCTNAMECOLOUR,
									:EMBLEMCODE,
									:STITCHCOUNT,
									:SIZEMM,
									:POSITION,
									:THREAD1,
									:THREAD2,
									:THREAD3,
									:THREAD4,
									:THREAD5,
									:THREAD6,
									:THREAD7,
									:THREAD8,
									:THREAD9,
									:THREAD10,
									:THREAD11,
									:THREAD12,
									:THREAD13,
									:THREAD14,
									:THREAD15,
									:LOGOAPPROVED,
									:LOGOURL,
									:LOGOSVGURL,
									:PRIMARYBACKGROUNDCOLOUR
								);",
								[
									':ORGANIZATIONLOGOID' => null,
									':ORGANIZATIONUNITID' => null,
									':ORGANIZATIONID' => $organizationID,
									':SCHOOLNAME' => $designDetail->design->name,
									':TESCOSTYLEREF' => $designDetail->design->number,
									':TESCOALTSTYLECODE' => null,
									':PRODUCTGROUP' => null,
									':PRODUCTNAMECOLOUR' => null,
									':EMBLEMCODE' => null,
									':STITCHCOUNT' => $designDetail->design->stitch_count,
									':SIZEMM' => $designDetail->design->width . 'x' . $designDetail->design->height,
									':POSITION' => null,
									':THREAD1' => null,
									':THREAD2' => null,
									':THREAD3' => null,
									':THREAD4' => null,
									':THREAD5' => null,
									':THREAD6' => null,
									':THREAD7' => null,
									':THREAD8' => null,
									':THREAD9' => null,
									':THREAD10' => null,
									':THREAD11' => null,
									':THREAD12' => null,
									':THREAD13' => null,
									':THREAD14' => null,
									':THREAD15' => null,
									':LOGOAPPROVED' => 1,
									':LOGOURL' => $svgasPNG,
									':LOGOSVGURL' => $designDetail->design->logo->url,
									':PRIMARYBACKGROUNDCOLOUR' => $r . ',' . $g . ',' . $b,
								]
							);

							$logoID = dbid();

							if (is_null($logoOrg['organization_default_logo_id'])) {
								// update default logo
								dbq("UPDATE organizations SET organization_default_logo_id = :LOGOID WHERE organization_id = :ORGID", [':LOGOID' => $logoID, ':ORGID' => $logoOrg['organization_id']]);
							}
						}

					} else {
						// 500 error?
						return (array)$designDetail;
					}
					break;
				}

			} else {
				// its empty?  No designs for this school?
				// need to create this on ebos.
				$ftp_server="ftp.slickstitch.com";
				$ftp_user="tesco";
				$ftp_pass="q46ovvB";

				$conn_id = ftp_connect($ftp_server);

				$login_result = ftp_login($conn_id, $ftp_user, $ftp_pass);

				ftp_pasv($conn_id, true);

				$remote_path = "/Tesco/Approved Schools/" . $schoolURN . "/" . $missingLogo['tesco_style_ref'] . ".dst";	// Is this case sensitive?
				$remote_path_caps = "/Tesco/Approved Schools/" . $schoolURN . "/" . $missingLogo['tesco_style_ref'] . ".DST";
				$local_path = "dst/".$missingLogo['tesco_style_ref'].".dst";

				// check if the DST is there.
				$logoRetrieved = false;

				if (@ftp_get($conn_id, $local_path, $remote_path, FTP_BINARY)) {
					$logoRetrieved = true;
				} elseif (@ftp_get($conn_id, $local_path, $remote_path_caps, FTP_BINARY)) {
					$logoRetrieved = true;
				}

				//echo $local_path; die;
				if($logoRetrieved && file_exists($local_path)){
					// upload it.

					$designdetail = [
						'design' => [
							'number' => $missingLogo['tesco_style_ref'],  // random string for testing.  Must be unique - No Spaces.
							'name' => $missingLogo['school_name'],  // random name for testing
							'notes' => 'Created by F&F UES',
							'threads' => explode(',', $missingLogo['logo_thread_array']), // Must be strings not integers.
							'dst' => [
								'data' => base64_encode(file_get_contents($local_path)),
								'filename' => $missingLogo['tesco_style_ref'].'.dst'
							]
						]
					];

					echo "<pre>".print_r($designdetail,1)."</pre>";

					//return $designdetail;
					$newDesign = $eBOS_API->createDesign($designdetail);

					// if it worked, add entries to organization logos.
					if(!isset($newDesign->error)){
						// worked.

					} else {
						$data[$missingLogo['logo_code']] = 'didnt work';
					}
					// return $newDesign;
					//echo "<pre>".print_r($newDesign,1)."</pre>";
				}

			}

		}

		return $data;
	}

	public function updatelivestatus(){

		ini_set('max_execution_time', 7000);

		$exitDataQuery = dbq("SELECT ed.*, o.organization_id from exit_data ed JOIN organizations o ON ( o.`school_URN` = ed.`school_id` ) ");

		while($exitData = dbf($exitDataQuery)){
			$invoice = 0;
			$form = 0;
			$signupdate = null;

			$regdate = date("Y-m-d G:i:s",strtotime(str_replace('/','-',$exitData['signupdate'])));

			if($exitData['invoice'] == 'Yes'){
				$invoice = 1;
			}

			if($exitData['formstatus'] == 'Yes'){
				$signupdate = $regdate;
				$form = 1;
			}

			dbq('update organizations set
					organization_form_signed = :FORM,
					organization_live_date = :LIVEDATE,
					organization_registered_date = :REGDATE,
					organization_can_bulk = :INVOICE
					where organization_id = :ORGID',[
					':FORM' => $form,
					':LIVEDATE' => $signupdate,
					':REGDATE' => $regdate,
					':INVOICE' => $invoice,
					':ORGID' => $exitData['organization_id']
				]
			);

		}

	}

	public function updatestages(){
		$stageDataQuery = dbq("UPDATE exit_data ed
									JOIN organizations o ON ( o.`school_URN` = ed.`school_id` )
									JOIN school_statuses ss ON (ed.uniformstatus = ss.`school_status_name`)
									SET o.`organization_status_id` = ss.`school_status_id`");

		$stageDataQuery = dbq("UPDATE exit_data ed
									JOIN school_data sd ON ( sd.`URN` = ed.`school_id` )
									JOIN school_statuses ss ON (ed.uniformstatus = ss.`school_status_name`)
									SET sd.`school_status_id` = ss.`school_status_id`");

	}

	public function copyuniformnotes()
	{
		ini_set('max_execution_time', 7000);

		$uniformNoteQuery = dbq("SELECT uun.*, usd.account, o.`organization_id` FROM ues_uniform_notes uun JOIN ues_school_data usd ON (usd.id = uun.school_id) JOIN organizations o ON (o.school_URN = usd.account)");

		while($uniformNote = dbf($uniformNoteQuery)){

			// insert it as a ticket.

			$taskArray = [
				':TICKETTYPEID' => 13,
				':TICKETORGANIZATION' => $uniformNote['organization_id'],
				':TICKETSTATUSID' => 4,
				':TICKETOPENDATE' => $uniformNote['ues_uniform_notes_date'],
				':TICKETCLOSEDATE' => $uniformNote['ues_uniform_notes_date'],
				':TICKETMODIFIEDDATE' => $uniformNote['ues_uniform_notes_date'],
				':TICKETDEADLINE' => $uniformNote['ues_uniform_notes_date'],
				':TICKETSUBJECT' => 'Uniform Panel Note',
				':TICKETDESCRIPTION' => $uniformNote['ues_uniform_notes_note'],
				':TICKETUSERID' => null,
				':TICKETUSERNAME' => $uniformNote['ues_uniform_note_by']
			];

			//return $taskArray;
			//
			// insert as a ticket.
			dbq("INSERT into tickets (
					ticket_type_id,
					ticket_organization,
					ticket_status_id,
					ticket_open_date,
					ticket_close_date,
					ticket_modified_date,
					ticket_deadline,
					ticket_subject,
					ticket_description,
					ticket_user_id,
					ticket_user_name
				  ) VALUES (
				    :TICKETTYPEID,
					:TICKETORGANIZATION,
					:TICKETSTATUSID,
					:TICKETOPENDATE,
					:TICKETCLOSEDATE,
					:TICKETMODIFIEDDATE,
					:TICKETDEADLINE,
					:TICKETSUBJECT,
					:TICKETDESCRIPTION,
					:TICKETUSERID,
					:TICKETUSERNAME
				  )",
				$taskArray
			);
			//return $taskArray;
		}
	}

	public function copydonations() {
		// create task list for IDs.
		ini_set('max_execution_time', 7000);

		$donationQuery = dbq("SELECT * from ues_donation_data udd join organizations o on (o.school_URN = udd.school_urn)");

		while($donation = dbf($donationQuery)){
			// insert it into new table.
			$createdAt = preg_split("/[\s-]/", $donation['created_at']);
			$createdAt[1] = date("m",strtotime($createdAt[1]));
			$createdAt[2] = (strlen($createdAt[2]) > 2) ? $createdAt[2] : '20'.$createdAt[2];

			$createdAt = $createdAt[2].'-'.$createdAt[1].'-'.$createdAt[0];

			//$createdAt = explode([" ","-"],$donation['created_at']);
			$displayDate = preg_split("/[\s-]/", $donation['display_date']);
			$displayDate[1] = date("m",strtotime($displayDate[1]));
			$displayDate[2] = (strlen($displayDate[2]) > 2) ? $displayDate[2] : '20'.$displayDate[2];

			$displayDate = $displayDate[2].'-'.$displayDate[1].'-'.$displayDate[0];

			$donationArray = [
				':SCHOOLID' => $donation['organization_id'],
				':DONATIONNAME' => $donation['name'],
				':DONATIONACCOUNTEDFOR' => $donation['accounted_for'],
				':DONATIONAMOUNT' => $donation['amount'],
				':DONATIONNEWBALANCE' => $donation['new_balance'],
				':DONATIONCATEGORY' => $donation['category'],
				':DONATIONPAID' => $donation['paid'],
				':DONATIONCREATEDAT' => $createdAt,
				':DONATIONDISPLAYDATE' => $displayDate,
				':DONATIONDESCRIPTION' => $donation['description']
			];


			dbq("insert into donations (
						organization_id,
						donation_name,
						donation_accounted_for,
						donation_amount,
						donation_new_balance,
						donation_category,
						donation_paid,
						donation_created_at,
						donation_display_date,
						donation_description
					  ) VALUES (
						:SCHOOLID,
						:DONATIONNAME,
						:DONATIONACCOUNTEDFOR,
						:DONATIONAMOUNT,
						:DONATIONNEWBALANCE,
						:DONATIONCATEGORY,
						:DONATIONPAID,
						:DONATIONCREATEDAT,
						:DONATIONDISPLAYDATE,
						:DONATIONDESCRIPTION
					  ) ",
				$donationArray
			);
			//break;
		}

	}

	public function logoapproveupdate(){
		dbq("UPDATE ues_school_logos_real lr JOIN organization_logos ol ON (ol.`tesco_style_ref` = lr.`tesco_style_ref`)
			SET ol.`logo_approved_date` = lr.`approved_date`, ol.`logo_approved_name` = lr.`approved_name`");
	}

	public function logonotapproveupdate(){
		$nonApprovedLogosQuery = dbq("SELECT * FROM ues_school_logos_real lr
 										  JOIN organizations o on (o.school_URN = lr.school_id)
 										  WHERE tesco_style_ref NOT IN (SELECT tesco_style_ref FROM organization_logos) AND lr.`approved` = 0");

		// add them in?
		while($nonApprovedLogos = dbf($nonApprovedLogosQuery)){
			//return $nonApprovedLogos;
			// add them to org logos?

			$logoImage = $nonApprovedLogos['image_url'];
			if(stristr($logoImage,"example-emblem.png")){
				$logoImage = 'images/emblems/example-emblem.png';
			} else {
				$image = @reset(explode("?", basename($nonApprovedLogos['image_url'])));
				$logoImage = 'images/ueslogos/'.$image;
			}

			dbq("insert into organization_logos (
							organization_id,
							school_name,
							tesco_style_ref,
							logo_approved,
							logo_url,
							primary_background_colour,
							logo_requested_name,
							logo_requested_date,
							products_using_logo
						  ) VALUES (
						    :ORGANIZATIONID,
							:SCHOOLNAME,
							:TESCOSTYLEREF,
							:LOGOAPPROVED,
							:LOGOURL,
							:PRIMARYBACKGROUNDCOLOUR,
							:LOGOREQUESTEDNAME,
							:LOGOREQUESTEDDATE,
							:PRODUCTSUSINGLOGO
						  )
			",
				[
					':ORGANIZATIONID' => $nonApprovedLogos['organization_id'],
					':SCHOOLNAME' => $nonApprovedLogos['school_name'],
					':TESCOSTYLEREF' => $nonApprovedLogos['tesco_style_ref'],
					':LOGOAPPROVED' => 0,
					':LOGOURL' => $logoImage,
					':PRIMARYBACKGROUNDCOLOUR' => '127,127,127',
					':LOGOREQUESTEDNAME' => $nonApprovedLogos['requested_name'],
					':LOGOREQUESTEDDATE' => $nonApprovedLogos['requested_date'],
					':PRODUCTSUSINGLOGO' => 0
				]
			);
			//break;
			/*
			 {
					"id": "22",
					"school_id": "101266",
					"school_name": "Church Hill School",
					"tesco_style_ref": "101266-1C",
					"tesco_alt_style_code": null,
					"image_url": "http:\/assets\/example-emblem.png",
					"approved": "0",
					"approved_date": null,
					"approved_name": null,
					"requested_date": "2013-10-18 09:22:00",
					"requested_name": "John Harrison",
					"organization_id": "5277",
					"organization_type_id": "1",
					"organization_name": "Church Hill School",
					"organization_member_count": "230",
					"organization_default_address_id": "5016",
					"organization_telephone": "020 83683431",
					"organization_fax": null,
					"organization_email": null,
					"organization_url": "http:\/\/www.churchhill.barnet.sch.uk",
					"school_data_id": "2509",
					"school_local_authority_id": "8",
					"school_education_phase_id": "8",
					"organization_default_logo_id": "7347",
					"organization_funds": "38.9100",
					"organization_status_id": "1",
					"school_URN": "101266",
					"organization_has_subunits": "0",
					"organization_parent_id": null,
					"logos_imported": "1",
					"organization_can_bulk": "0",
					"organization_form_signed": "1",
					"organization_live_date": "2012-02-29 23:00:00",
					"organization_registered_date": "2012-02-29 23:00:00"
				}
			 * */
		}
	}

	public function altplain(){
		$alternatesQuery = dbq("select * from alt_uniform_codes group by product_colour_old_style_ref");
		$alts = [];
		while($alternates = dbf($alternatesQuery)){
			$alts[] = $alternates;
			// update products_colours to be this?
			dbq("update product_colours pc set pc.product_colour_style_ref = :NEWREF where pc.product_colour_style_ref LIKE :OLDREF",
				[
					':NEWREF' => strtoupper($alternates['product_colour_style_ref']),
					':OLDREF' => $alternates['product_colour_old_style_ref']
				]
			);
		}

		return $alts;
	}

	public function copytasks()
	{
		// create task list for IDs.
		ini_set('max_execution_time', 7000);

		$taskQuery = dbq("SELECT ut.*, s.account from ues_school_task_items ut join ues_school_data s on (s.id = ut.school_id)");
		//$taskQuery = dbq("SELECT DISTINCT(user_dropdown_toggle) FROM ues_school_task_items");

		$typeKeys = [
			'Note' => 10,
			'PhoneNote' => 11,
			'ToDoNote' => 12
		];

		$userList = [];
		while($task = dbf($taskQuery)){

			$orgID = getOrgIDFromURN($task['account']);
			$taskArray = [
				':TICKETTYPEID' => $typeKeys[$task['note_note_type']],
				':TICKETORGANIZATION' => $orgID,
				':TICKETSTATUSID' => 4,
				':TICKETOPENDATE' => $task['created'],
				':TICKETCLOSEDATE' => $task['note_deadline'],
				':TICKETMODIFIEDDATE' => $task['edited'],
				':TICKETDEADLINE' => $task['note_deadline'],
				':TICKETSUBJECT' => $task['note_title'],
				':TICKETDESCRIPTION' => $task['description'],
				':TICKETUSERID' => null,
				':TICKETUSERNAME' => $task['user_dropdown_toggle']
			];

		//
			// insert as a ticket.
			dbq("INSERT into tickets (
					ticket_type_id,
					ticket_organization,
					ticket_status_id,
					ticket_open_date,
					ticket_close_date,
					ticket_modified_date,
					ticket_deadline,
					ticket_subject,
					ticket_description,
					ticket_user_id,
					ticket_user_name
				  ) VALUES (
				    :TICKETTYPEID,
					:TICKETORGANIZATION,
					:TICKETSTATUSID,
					:TICKETOPENDATE,
					:TICKETCLOSEDATE,
					:TICKETMODIFIEDDATE,
					:TICKETDEADLINE,
					:TICKETSUBJECT,
					:TICKETDESCRIPTION,
					:TICKETUSERID,
					:TICKETUSERNAME
				  )",
				$taskArray
			);
			//$userID = $this->getNewUserID($task);
			//return $taskArray;
		}
		return $userList;
	}

	private function getNewUserID($oldID){

	}

	public function copyaudit()
	{
		// create audit list for IDs.
		ini_set('max_execution_time', 7000);

		$auditTypeQuery = dbq("SELECT * from audit_log_events");

		$auditKeys = [];
		while($auditType = dbf($auditTypeQuery)){
			$auditKeys[$auditType['audit_log_event_name']] = $auditType['audit_log_event_id'];
		}

		// loop through ues audit and add it to our audit.

		$auditOldQuery = dbq("SELECT ues_audit_log.*, ues_school_data.account, o.`organization_id`
								FROM `ues_audit_log`
								JOIN `ues_school_data` ON (ues_audit_log.`existing_id` = ues_school_data.id)
								JOIN organizations o ON (o.`school_URN` = ues_school_data.account)
");
		while($auditOld = dbf($auditOldQuery)){

			dbq("insert into audit_log (
						audit_log_id,
						audit_timestamp,
						audit_log_event_id,
						audit_event_info,
						organization_id
					  ) VALUES (
					  	:AUDITLOGID,
						:AUDITTIMESTAMP,
						:AUDITLOGEVENTID,
						:AUDITEVENTINFO,
						:ORGANIZATIONID
					  )",
				[
					  	':AUDITLOGID' => $auditOld['audit_log_id'],
						':AUDITTIMESTAMP' => $auditOld['date'],
						':AUDITLOGEVENTID' => $auditKeys[$auditOld['title']],
						':AUDITEVENTINFO' => 'IMPORTED',
						':ORGANIZATIONID' => $auditOld['organization_id']

				]
			);

		}

		return ['done'];
	}

	public function sendemail(){
		session_destroy(); // stop the session so we can keep doing other things...
		$test = false;
// select all the emails from the exit table.
		ini_set('max_execution_time', 7000);
		// get email addresses


		$html = "Dear %s,<br />
<br />
We are pleased to confirm that we made a donation of &pound;%01.2f to your school on 12th February 2018.<br />
<br />
This donation represents 5 percent of the value of F&F Uniform Embroidery Service purchases made between 1st November 2017 and 31st December 2017 and any outstanding balance that may have been owed.<br />
<br />
To check the summary of your donations to date, simply visit www.ff-ues.com and log into your account. Select 'Services' from the dropdown in the toolbar at the top of your screen, and then click 'Donations'.<br />
<br />
Please remember to keep your bank details up to date to ensure that future donations are processed smoothly. To update your bank details, log into your account and go to 'Donations' as before. You'll find a 'Bank Details' tab where you can update your account name, number and sort code.<br />
<br />
<br />
Yours sincerely,<br />
<br />
The F&F Uniforms Team";

		$subject = 'Your F&F Uniform Embroidery Donation Payment';
		$title = $subject;
		$template = 'tesco-ff-uniforms';
		$tag = ['Feb-2018-payment'];

		// Attachment.
		/*
		$attachmentArray[] = [
			'type' => "application/pdf",
			'name' => basename('2016 UES Pricelist v2.pdf'),
			'content' => base64_encode(file_get_contents('2016 UES Pricelist v2.pdf'))
		];
		*/

		$attachmentArray = null;

		if (!$test) {

			/*		$emailQuery = dbq("select * from users u
							  join users_to_organizations u2o using (user_id)
							  join organizations o using (organization_id)
							  where u.user_emailed = 0
							  and u.user_type_id = 1
							  and u2o.user_primary = 1
							  AND o.organization_parent_organization_id IS NULL
							  limit 500");*/

			// Paid donation
			$emailQuery = dbq("SELECT u.user_id, o.organization_name, d.donation_amount, u.user_email FROM users u
							  JOIN users_to_organizations u2o USING (user_id)
							  JOIN organizations o USING (organization_id)
							  JOIN donations d ON (d.`organization_id` = o.`organization_id` AND d.`donation_category` = 2 AND d.`donation_accounted_for` = '2018-02-12')
							  WHERE u.user_emailed = 0
							  AND u.user_type_id = 1
							  AND u2o.user_primary = 1
							  AND o.organization_parent_organization_id IS NULL
							  LIMIT 500");


			// no bank.
			/*$emailQuery = dbq("SELECT * FROM users u
							  JOIN users_to_organizations u2o USING (user_id)
							  JOIN organizations o USING (organization_id)
							  WHERE u.user_emailed = 0
							  AND u.user_type_id = 1
							  AND u2o.user_primary = 1
							  AND o.organization_parent_organization_id IS NULL
							  AND o.organization_bank_verified = 0
							  LIMIT 500");*/

			$emailAddresses = [];

			while ($email = dbf($emailQuery)) {

/*
				$emailAddresses[$email['organization_name']] = [
					'email' => $email['user_email'],
					'urn' => $email['school_URN'],
					'amount' => $email['donation_amount'],
				];*/

				$message = sprintf($html,$email['organization_name'],$email['donation_amount']);

				$template_content = array(
					[
						'name' => 'emailtitle',
						'content' => $title
					],
					[
						'name' => 'main',
						'content' => $message
					]
				);

				$emailAddress = [
					$email['organization_name'] => $email['user_email']
					//,'Andrew Lindsay' => 'andy@slickstitch.com'
				];

				if(send_ues_mail($emailAddress, $subject, $template_content, $template, $tag,$attachmentArray)){

					dbq("UPDATE users SET user_emailed = 1 WHERE user_id = :ID", [
						':ID' => $email['user_id']
					]);
					echo "Sent to ".$email['user_email']."\n";

				}

			}

		} else {

			$emailAddresses = [
				/*'Jonathatn Lonsdale' => [
					'email' => 'jonathan.lonsdale@uk.tesco.com',
					'urn' => '123456',
					'amount' => 123.4,
				],
				'Zara Sotiriou' => [
					'email' => 'Zara.Sotiriou@uk.tesco.com',
					'urn' => '123456',
					'amount' => 123.4,
				],*/
				'Andrew Lindsay' => [
					'email' => 'andy@slickstitch.com',
					'urn' => '123456',
					'amount' => 123.4,
				]
			];

			foreach($emailAddresses as $orgName => $email){

				$message = sprintf($html,$orgName,$email['amount']);

				$template_content = array(
					[
						'name' => 'emailtitle',
						'content' => $title
					],
					[
						'name' => 'main',
						'content' => $message
					]
				);

				$emailAddress = [
					$orgName => $email['email']
					//,'Andrew Lindsay' => 'andy@slickstitch.com'
				];

				if(send_ues_mail($emailAddress, $subject, $template_content, $template, $tag,$attachmentArray)){

					echo "Sent to ".$email['email'];

				}


				//send_ues_mail($emailAddress, $subject, $message, $title, $template, $tag);
			}


		}

		return ['done'];
	}

	public function updatemissingdefaultlogos(){
		$missingQuery = dbq("select * from organizations where organization_default_logo_id is null");

		while($missing = dbf($missingQuery)){
			// find first approved logo.
			$logosQuery = dbq("select * from organization_logos where organization_id = :ORGID and logo_approved = 1",
				[
					':ORGID' => $missing['organization_id']
				]
			);

			if(dbnr($logosQuery) > 0){
				$logos = dbf($logosQuery);
				dbq('update organizations set organization_default_logo_id = :LOGOID where organization_id = :ORGID',
					[
						':LOGOID' => $logos['organization_logo_id'],
						':ORGID' => $missing['organization_id']
					]
				);
			}
		}
	}

	public function addgmosku() {
		// get skus.
		$skuQuery = dbq("SELECT * FROM products WHERE product_sku != ''");

		$randomskus = [];
		while($sku = dbf($skuQuery)){
			$randomskus[] = $sku['product_sku'];
		}

		$productQuery = dbq("select * from products_sizes_to_products_colours");

		while($product = dbf($productQuery)){
			dbq("UPDATE products_sizes_to_products_colours set product_variation_gmo_sku = :SKU where products_sizes_to_colours_id = :ID",
				[
					':SKU' => $randomskus[array_rand($randomskus)],
					':ID' => $product['products_sizes_to_colours_id']
				]
			);
		}

	}

	public function testexcel(){
		// make file and email it here.
		ini_set('memory_limit',-1);
		//include_once('includes/classes/PHPExcel.php');
		include(DIR_CLASSES . 'PHPExcel.php');
		include(DIR_CLASSES . 'PHPExcel/IOFactory.php');

		$FOLDER = 'import/atg/';

		$filelist = glob($FOLDER.'*.xls*');
		//$filelist = glob($FOLDER.'*June.xlsx');

		foreach($filelist as $fileName) {
			echo $fileName."<br />";
			try {
				$inputFileType = PHPExcel_IOFactory::identify($fileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objReader->setReadDataOnly(true);
				$objPHPExcel = $objReader->load($fileName);
				$objPHPExcel = null;
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($fileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
			}
		}

		echo 'Done';
		/*try {
			$inputFileType = PHPExcel_IOFactory::identify($fileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($fileName);

			// Change the file
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A4', 'Customer Name: (URN) NAME')
				->setCellValue('B9', 'COUNTRY')
				->setCellValue('B10', 'ADDRESS1')
				->setCellValue('B11', 'ADDRESS2')
				->setCellValue('B12', 'ADDRESS3')
				->setCellValue('B13', 'TOWN')
				->setCellValue('B14', 'POSTCODE')
				->setCellValue('B18', 'BILLINGNAME')
				->setCellValue('B19', 'BILLINGPHONE')
				->setCellValue('B20', 'BILLINGFAX')
				->setCellValue('B21', 'BILLINGEMAIL')
				->setCellValue('B30', 'Date: '.date('d-m-y'))
			;

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $inputFileType);
			$objWriter->save('invoiceRequests/URN'.$fileName);

		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($fileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}*/

	/*
		// Read the file
		$objReader = PHPExcel_IOFactory::createReader($fileType);
		$objPHPExcel = $objReader->load($fileName);

		// Change the file
		 $objPHPExcel->setActiveSheetIndex(0)
		     ->setCellValue('A1', 'Hello')
		     ->setCellValue('B1', 'World!');

		// Write the file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
		$objWriter->save('copy'.$fileName);*/
	}

	public function updateffimg(){
		// update default logos on FF website.
		ini_set('max_execution_time', 3000);

		$missingQuery = dbq("select * from organizations o join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id) where o.school_URN = 902990002088");

		while($missing = dbf($missingQuery)){
			//Image please.
			$filename = $missing['school_URN'];
			//if(!file_exists('ffimages/' .$filename . '_xs.jpg')){
				$image = file_get_contents('http:' . $missing['logo_url']);
				$im2 = new Imagick();
				$im2->setResolution(300,300);
				//$im2->setImageUnits(1);

				$im2->readImageBlob($image);
				$im2->scaleImage(316, 316, true);

				$im1 = new Imagick();
				$im1->setResolution(300,300);

				//$im1->setImageResolution(300,300);
				$im1->newImage(336, 336, new ImagickPixel('rgb(' . $missing['primary_background_colour'] . ')'));
				$im1->setImageUnits(1);
				$im1->setImageColorspace($im2->getImageColorspace());
				$im1->setImageMatte(true);

				$x = floor((336 - $im2->getImageWidth()) / 2);
				$y = floor((336 - $im2->getImageHeight()) / 2);

				$im1->compositeImage($im2, $im2->getImageCompose(), $x, $y);
				$im1->stripimage();
				// Save it.
				//$im1->setImageFormat("jpeg");
				$im1->scaleImage(75, 75, true);
				// Save locally.
				$im1->setImageResolution(300,300);
				try {

					$im1->writeImage(FS_ROOT.'api\v1.0\ffimages\\' .$filename . '_xs.jpg');
				/*	$im1->writeImage(FS_ROOT.'api\v1.0\ffimages\\' .$filename . '_xs.gif');
					$im1->writeImage(FS_ROOT.'api\v1.0\ffimages\\' .$filename . '_xs.bmp');
					$im1->writeImage(FS_ROOT.'api\v1.0\ffimages\\' .$filename . '_xs.png');*/
				} catch (Exception $e) {
				//	echo "<pre>".print_r($e,1)."</pre>";
				}

				//file_put_contents('ffimages/' .$filename . '_xs.jpg', $im1);
				//echo "<pre>".print_r($im1->getImageResolution(),1)."</pre>";
				//echo "<pre>".print_r($im1->getImageUnits(),1)."</pre>";

				// open with GD?
				// $image = imagecreatefromjpeg('ffimages/' .$filename . '_xs.jpg');

			//}
			//die;
		}

	}

	public function findmissingfflogos(){

		session_destroy(); // dont need a the session here.

		ini_set('max_execution_time', 0);

		$missingQuery = dbq("select * from organizations where organization_status_id = 3 order by organization_id DESC");

		while($missing = dbf($missingQuery)){

			$tescofilename = 'http://i1.adis.ws/i/tesco/'.$missing['school_URN'].'_xs.jpg';

			$file_headers = @get_headers($tescofilename);

			if($file_headers[0] == 'HTTP/1.0 404 Not Found'){
				// update.
				dbq("update organizations set organization_default_logo_update = 1 where organization_id = :ORGID",
					[
						':ORGID' => $missing['organization_id']
					]
				);
			}
		}
	}

	public function checkdeletedlogos(){
		// Check that deleted logos on FF are deleted/renamed on ebos.
		$deletedLogoQuery = dbq("select * from organization_logos where logo_deleted = 1");

		include(DIR_CLASSES . 'eBOS_API.php');

		$eBOS_API = new eBOS_API(APIUser, APIPass);
		$foundDesigns = [];
		while($deletedLogo = dbf($deletedLogoQuery)){
			// check ebos.
			$designs = $eBOS_API->getDesigns($deletedLogo['tesco_style_ref'],1,20,'true');
			foreach($designs->designs as $design){
				if($deletedLogo['tesco_style_ref'] == $design->number){
					$foundDesigns[] = $design;
				}
			}
		}
		return (array)$foundDesigns;
	}

	public function make3dlogo(){
		// load an SVG from system.
		$svgQuery = dbq("select * from organization_logos limit 1");

		$svgRes = dbf($svgQuery);

		ini_set("memory_limit", "-1");
		ini_set("max_execution_time", "-1");

		$svg = file_get_contents($svgRes['logo_svg_url']);
		$svg = str_ireplace("<rect ","<rect fill-opacity=\"0.0\" ",$svg);
		$svg = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'.$svg;
		$svg = str_ireplace('stroke-width: 1.6px','stroke-width: 2.0px; filter:url(#demo2)',$svg);

	//	echo $svg; die;

		//$svgParts = simplexml_load_string($svg);
		$svgParts = new SimpleXMLElement($svg);
		//$test = $svgParts->xpath('//*[@class="normal"]');


		$elementsToRemove = array();
		foreach ($svgParts->xpath('//*[@class="normal"]') as $code) {
				// change opacity.
			$code->addAttribute('opacity','0.0');
				//$elementsToRemove[] = $code;
		}
		$svg = $svgParts->asXML();

		// put them back in one at a time?

//		var_dump($test);



//die;
		//$svgParts = simplexml_load_string($svg);

		// find lines?



		//return $svg;

		// convert SVG to PNG
		try{
			$imageref = new Imagick();
			$imageref->setBackgroundColor(new ImagickPixel('transparent'));
			$imageref->readImageBlob($svg); // blank.


			foreach ($svgParts->xpath('//*[@class="normal"]') as $code) {
				// change opacity.
				$code->attributes()->opacity = 1.0;
				//$elementsToRemove[] = $code;
				// add 3d effect and merge with image.

				$stitchsvg = $svgParts->asXML();

				$stitchimageref = new Imagick();
				$stitchimageref->setBackgroundColor(new ImagickPixel('transparent'));
				$stitchimageref->readImageBlob($stitchsvg); // blank.

				// 3d effect.
				// $stitchimageref->embossImage(5,0);

				// combine back to $imageref

				$imageref->compositeImage($stitchimageref, $stitchimageref->getImageCompose(), 0, 0);
				//$imageref->stripimage();

				$code->attributes()->opacity = 0.0;
			}

			$svg = $svgParts->asXML();


			$imageref->setImageMatte(true);
			$imageref->setImageFormat("png24");
			$imageref->setType(imagick::IMGTYPE_TRUECOLORMATTE);
			//$imageref->embossImage(5,0);
			$imageref->setImageCompressionQuality(60);
			$imageref->writeImage("C:\\wamp\\www\\ues\\api\\v1.0\\3dlogo\\testlogo.png");
		} catch (Exception $e){
			return (array)$e;
		}

	}

	public function setstage(){

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		if (!isset($this->request['URN']) || empty($this->request['URN'])) {
			$this->setError([
				'message' => 'Cannot provide results, required arguments are missing.',
				'errorCode' => 'arguments-missing',
			]);
			return $this->getResponse(500);
		}

		if (!isset($this->request['stage']) || empty($this->request['stage'])) {
			$this->setError([
				'message' => 'Cannot provide results, required arguments are missing.',
				'errorCode' => 'arguments-missing',
			]);
			return $this->getResponse(500);
		}

		// update
		dbq('update organizations o join school_data sd using (school_data_id) set o.organization_status_id = :SID, sd.school_status_id = :SID where school_URN = :URN',
			[
				':URN'=> $this->request['URN'],
				':SID'=> $this->request['stage'],
			]
		);

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];
	}

	public function getebosids(){
		ini_set('max_execution_time', 0);

		include(DIR_CLASSES . 'eBOS_API.php');

		$eBOS_API = new eBOS_API(APIUser, APIPass);

		$designs = $eBOS_API->getDesigns(null,0,50);

		return sizeof($designs->designs);
	}

	public function consolodateddonations()
	{

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];

		// get the uploaded data.
		$organizationDonationQuery = dbq("SELECT * from donations_upload");

		$headers = [
			'org_id' => 'Org ID',
			'school_id' => 'School ID',
			'school_name' => 'School Name',
			'included_urns' => 'Included URNs',
			'amount' => 'Donation Amount',
			'bank_name' => 'Bank Account Name',
			'bank_number' => 'Bank Account Number',
			'bank_sortcode_1' => 'Bank Account Sort Code 1',
			'bank_sortcode_2' => 'Bank Account Sort Code 2',
			'bank_sortcode_3' => 'Bank Account Sort Code 3',
		];

		$bankAccounts = [];

		while($organizationDonation = dbf($organizationDonationQuery)){

			$masterURN = getMasterURN($organizationDonation['donations_upload_urn']);
			$masterOID = getOrgIDFromURN($masterURN);
			if(!isset($csv[$masterURN])){
				// first one.
				$csv[$masterURN] = [
					'org_id' => $masterOID,
					'school_id' => $masterURN,
					'school_name' => getSchoolNameFromURN($masterURN),
					'included_urns' => '',
					'amount' => 0,
				];
			}

			$csv[$masterURN]['amount'] += $organizationDonation['donations_upload_amount'];
			$csv[$masterURN]['included_urns'] .= $organizationDonation['donations_upload_urn'].':';
		}

		$data['data']['contents'] = '';

		// add headers
		foreach($headers as $key => $header){
			$data['data']['contents'] .= '"'.$header.'",';
		}

		$data['data']['contents'] = substr($data['data']['contents'],0,-1) . PHP_EOL; // knock off last ,
		// make it into a CSV
		foreach($csv as $csvline){

			// insert a 'tesco_donation'
			if($masterOID == 0){
				// create a organization or ignore?

			}

			if($csvline['amount'] > 0){
				// insert a 'tesco_donation'
				dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
					[
						':ORGANIZATION_ID' => $csvline['org_id'],
						':ORGANIZATION_URN' => $csvline['school_id'],
						':DONATION_NAME' => 'tesco_donation',
						':DONATION_ACCOUNTED_FOR' => '2016-02-29',
						':DONATION_AMOUNT' => $csvline['amount'],
						':DONATION_NEW_BALANCE' => $csvline['amount'],
						':DONATION_CATEGORY' => 5,
						':DONATION_PAID' => 0,
						':DONATION_DISPLAY_DATE' => '2016-02-29',
						':DONATION_DESCRIPTION' => 'UniformEasy Tesco donation - Relaunch Opening Balance'

					]
				);
			}
		}
		return true;
	}

	public function openingbalance()
	{

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$houseIDs = [
			'901',
			'902',
			'903',
			'904',
			'905',
			'906',
			'907',
			'908',
			'909',
			'910',
			'911',
			'912',
			'913',
			'914',
			'915',
			'916',
			'917',
			'918',
			'919',
		];

		// get the uploaded data.
		$organizationBalanceQuery = dbq("SELECT * from opening_balance where amount > 0");

		while($organizationBalance = dbf($organizationBalanceQuery)){


			$masterURN = getMasterURN($organizationBalance['urn']);

			$houseident = substr($masterURN,0,3);

			if(in_array($houseident,$houseIDs)){
				// probably a house not on the system, just knock off the first 3 chars.
				$masterURN = substr($masterURN,3);
			}

			$masterOID = getOrgIDFromURN($masterURN);

			if(!isset($csv[$masterURN])){
				// first one.
				$csv[$masterURN] = [
					'org_id' => $masterOID,
					'school_id' => $masterURN,
					'school_name' => getSchoolNameFromURN($masterURN),
					'included_urns' => '',
					'amount' => 0,
				];
			}
			$csv[$masterURN]['amount'] += $organizationBalance['amount'];
		}

		// return $csv;

		// make it into a CSV
		foreach($csv as $csvline){

			// insert a 'tesco_donation'
			if($masterOID == 0){
				// create an organization or ignore?

			}

			if($csvline['amount'] > 0){
				// insert a 'tesco_donation'
				dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
					[
						':ORGANIZATION_ID' => $csvline['org_id'],
						':ORGANIZATION_URN' => $csvline['school_id'],
						':DONATION_NAME' => 'tesco_donation',
						':DONATION_ACCOUNTED_FOR' => '2016-01-31',
						':DONATION_AMOUNT' => $csvline['amount'],
						':DONATION_NEW_BALANCE' => $csvline['amount'],
						':DONATION_CATEGORY' => 5,
						':DONATION_PAID' => 0,
						':DONATION_DISPLAY_DATE' => '2016-01-31',
						':DONATION_DESCRIPTION' => 'Total donations accrued up to January 31st 2016'

					]
				);
			}
		}
		return true;
	}

	public function slickpaid()
	{

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$houseIDs = [
			'901',
			'902',
			'903',
			'904',
			'905',
			'906',
			'907',
			'908',
			'909',
			'910',
			'911',
			'912',
			'913',
			'914',
			'915',
			'916',
			'917',
			'918',
			'919',
		];

		// get the uploaded data.
		$organizationBalanceQuery = dbq("SELECT * from slick_payments where amount > 0");

		while($organizationBalance = dbf($organizationBalanceQuery)){

			$masterURN = getMasterURN($organizationBalance['urn']);

			$houseident = substr($masterURN,0,3);

			if(in_array($houseident,$houseIDs)){
				// probably a house not on the system, just knock off the first 3 chars.
				$masterURN = substr($masterURN,3);
			}

			$masterOID = getOrgIDFromURN($masterURN);

			if(!isset($csv[$masterURN])){
				// first one.
				$csv[$masterURN] = [
					'org_id' => $masterOID,
					'school_id' => $masterURN,
					'school_name' => getSchoolNameFromURN($masterURN),
					'included_urns' => '',
					'amount' => 0,
					'date' => $organizationBalance['date']
				];
			}
			$csv[$masterURN]['amount'] += $organizationBalance['amount'];
		}

		// return $csv;

		// make it into a CSV
		foreach($csv as $csvline){

			// insert a 'tesco_donation'
			if($masterOID == 0){
				// create a organization or ignore?
			}

			if($csvline['amount'] > 0){

				// need to get latest balance.
				$urnBalanceQuery = dbq("select * from donations where organization_urn = :ORGURN order by donation_id desc limit 1 ",
					[
						':ORGURN' => $csvline['school_id']
					]
				);

				$currentBalance = 0;
				if(dbnr($urnBalanceQuery) > 0){
					$urnBalance = dbf($urnBalanceQuery);
					$currentBalance = $urnBalance['donation_new_balance'];
				}

				$currentBalance = $currentBalance - $csvline['amount'];
				$currentBalance = max(0,$currentBalance);

				// insert a 'tesco_donation'
				dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
					[
						':ORGANIZATION_ID' => $csvline['org_id'],
						':ORGANIZATION_URN' => $csvline['school_id'],
						':DONATION_NAME' => 'tesco_paid',
						':DONATION_ACCOUNTED_FOR' => $csvline['date'],
						':DONATION_AMOUNT' => $csvline['amount'],
						':DONATION_NEW_BALANCE' => $currentBalance,
						':DONATION_CATEGORY' => 2,
						':DONATION_PAID' => 1,
						':DONATION_DISPLAY_DATE' => $csvline['date'],
						':DONATION_DESCRIPTION' => 'Donation paid via BACS '.date("jS F Y",strtotime($csvline['date']))
					]
				);

				// update organization balance.
				dbq("update organizations set organization_funds_balance = :BALANCE where organization_id = :ORGID",
					[
						':BALANCE' => $currentBalance,
						':ORGID' => $csvline['org_id']
					]
				);
			}
		}
		return true;
	}



	public function importrawrefunds(){
		$filelist = glob('refunds/*.csv');

		foreach($filelist as $file){
			$csv = array_map('str_getcsv', file($file));

			foreach($csv as $csvline){

				$newdate = explode('/',$csvline[1]);

				dbq("insert into raw_refunds (
						order_ref,
						order_date,
						spare1,
						school_urn,
						style_ref,
						ean,
						quantity,
						unit_price,
						line_price,
						line_donation_value
					  ) VALUES (
						:ORDER_REF,
						:ORDER_DATE,
						:SPARE1,
						:SCHOOL_URN,
						:STYLE_REF,
						:EAN,
						:QUANTITY,
						:UNIT_PRICE,
						:LINE_PRICE,
						:LINE_DONATION_VALUE
					  )",
					[
						':ORDER_REF' => $csvline[0],
						':ORDER_DATE' => $newdate[2].'-'.$newdate[1].'-'.$newdate[0],
						':SPARE1' => $csvline[2],
						':SCHOOL_URN' => $csvline[3],
						':STYLE_REF' => $csvline[4],
						':EAN' => $csvline[5],
						':QUANTITY' => $csvline[6],
						':UNIT_PRICE' => $csvline[7],
						':LINE_PRICE' => $csvline[8],
						':LINE_DONATION_VALUE' => $csvline[9]
					]
				);
			}

			rename($file,$file.".done");
			// import into dbase.
		}

		return $filelist;
	}


	public function importrawdonationsold(){
		$filelist = glob('donations/*.csv');

		foreach($filelist as $file){
			$csv = array_map('str_getcsv', file($file));

			foreach($csv as $csvline){

				$newdate = explode('/',$csvline[1]);

				dbq("insert into raw_donations (
						order_ref,
						order_date,
						spare1,
						school_urn,
						style_ref,
						ean,
						quantity,
						unit_price,
						line_price,
						line_donation_value
					  ) VALUES (
						:ORDER_REF,
						:ORDER_DATE,
						:SPARE1,
						:SCHOOL_URN,
						:STYLE_REF,
						:EAN,
						:QUANTITY,
						:UNIT_PRICE,
						:LINE_PRICE,
						:LINE_DONATION_VALUE
					  )",
					[
						':ORDER_REF' => $csvline[0],
						':ORDER_DATE' => $newdate[2].'-'.$newdate[1].'-'.$newdate[0],
						':SPARE1' => $csvline[2],
						':SCHOOL_URN' => $csvline[3],
						':STYLE_REF' => $csvline[4],
						':EAN' => $csvline[5],
						':QUANTITY' => $csvline[6],
						':UNIT_PRICE' => $csvline[7],
						':LINE_PRICE' => $csvline[8],
						':LINE_DONATION_VALUE' => $csvline[9]
					]
				);
			}

			rename($file,$file.".done");
			// import into dbase.
		}

		return $filelist;
	}

	public function fixbankhouses(){
		$bankDetailQuery = dbq("SELECT oba.*, o.organization_parent_organization_id FROM organization_bank_accounts oba JOIN organizations o USING (organization_id) WHERE o.organization_parent_organization_id > 0");

		while($bankDetail = dbf($bankDetailQuery)){

			$masterOrgID = getMasterOrgID($bankDetail['organization_id']);

			$allOrgQuery = dbq("select organization_id from organizations where organization_id = :ORGID or organization_parent_organization_id = :ORGID",
				[
					':ORGID' => $masterOrgID
				]
			);

			while($allOrg = dbf($allOrgQuery)){
				// delete old details if they exist.
				dbq("delete from organization_bank_accounts where organization_id = :ONEORGID",
					[
						':ONEORGID' => $allOrg['organization_id']
					]
				);

				// Insert new details.
				dbq("INSERT INTO organization_bank_accounts (
                        organization_id,
                        organization_bank_sort_code_1,
                        organization_bank_sort_code_2,
                        organization_bank_sort_code_3,
                        organization_bank_account,
                        organization_bank_name,
                        organization_bank_address_id,
                        organization_bank_last_updated
                      ) VALUES (
                        :ORGANIZATIONID,
                        :ORGANIZATIONBANKSORTCODE1,
                        :ORGANIZATIONBANKSORTCODE2,
                        :ORGANIZATIONBANKSORTCODE3,
                        :ORGANIZATIONBANKACCOUNT,
                        :ORGANIZATIONBANKNAME,
                        :ORGANIZATIONBANKADDRESSID,
                        NOW()
                      )",
					[
						':ORGANIZATIONID' => $allOrg['organization_id'],
						':ORGANIZATIONBANKSORTCODE1' => $bankDetail['organization_bank_sort_code_1'],
						':ORGANIZATIONBANKSORTCODE2' => $bankDetail['organization_bank_sort_code_2'],
						':ORGANIZATIONBANKSORTCODE3' => $bankDetail['organization_bank_sort_code_3'],
						':ORGANIZATIONBANKACCOUNT' => $bankDetail['organization_bank_account'],
						':ORGANIZATIONBANKNAME' => $bankDetail['organization_bank_name'],
						':ORGANIZATIONBANKADDRESSID' => $bankDetail['organization_bank_address_id']
					]
				);

				// update org.
				dbq('update organizations set organization_bank_verified = 1 where organization_id = :ORGID',
					[
						':ORGID' =>  $allOrg['organization_id']
					]
				);

				audit(23,$allOrg['organization_id'],'Bank details verified.');

			}

		}

	}

	public function processrawdonations(){
		$donationQuery = dbq("select * from raw_donations where processed = 0 order by order_date asc");

		$donationArray = [];
		while($donation = dbf($donationQuery)){
			// build an array.
			$parentorgurn = getMasterURN($donation['school_urn']);
			$parentorgid = getOrgIDFromURN($parentorgurn);

			if($parentorgid == 0){
				$parentorgid = $donation['school_urn'];
			}

			if(!isset($donationArray[$parentorgid])){
				$donationArray[$parentorgid] = [];
			}

			$month = substr($donation['order_date'],0,7);
			if(!isset($donationArray[$parentorgid][$month])){
				$donationArray[$parentorgid][$month] = 0;
			}

			$donationArray[$parentorgid][$month] += $donation['line_donation_value'];

		}

		// now add as a donation accrued in that month,

		foreach($donationArray as $orgID => $donationdate){
			foreach($donationdate as $datestring => $value){

				// need to get latest balance.
				$urnBalanceQuery = dbq("select * from donations where organization_id = :ORGID order by donation_id desc limit 1 ",
					[
						':ORGID' => $orgID
					]
				);

				$currentBalance = 0;
				if(dbnr($urnBalanceQuery) > 0){
					$urnBalance = dbf($urnBalanceQuery);
					$currentBalance = $urnBalance['donation_new_balance'];
				}

				$currentBalance = $currentBalance + $value;
				$currentBalance = max(0,$currentBalance);

				$orgDetail = getOrgDetails($orgID);

				// insert a 'tesco_donation'
				dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
					[
						':ORGANIZATION_ID' => $orgID,
						':ORGANIZATION_URN' => $orgDetail['school_URN'],
						':DONATION_NAME' => 'tesco_donation',
						':DONATION_ACCOUNTED_FOR' => date("Y-m-d",strtotime('last day of this month',strtotime($datestring.'-01'))),
						':DONATION_AMOUNT' => $value,
						':DONATION_NEW_BALANCE' => $currentBalance,
						':DONATION_CATEGORY' => 5,
						':DONATION_PAID' => 0,
						':DONATION_DISPLAY_DATE' => date("Y-m-d",strtotime('last day of this month',strtotime($datestring.'-01'))),
						':DONATION_DESCRIPTION' => 'Donation accrued during '.date("F",strtotime($datestring.'-01')).' '.date("Y",strtotime($datestring.'-01'))
					]
				);

				// update organization balance.
				dbq("update organizations set organization_funds_balance = :BALANCE, organization_funds = organization_funds + :THISDONATION where organization_id = :ORGID",
					[
						':BALANCE' => $currentBalance,
						':THISDONATION' => $value,
						':ORGID' => $orgID
					]
				);

			}
		}
		return true;
	}

	public function reorderDonations(){
		// re-order the donations to date better.  Need to update the balance to match.
		$fromDate = '2016-09-30';
		$orgID = '6562';

		//$orgQuery = dbq("select * from organizations where organization_id = ".$orgID);
		$orgQuery = dbq("select * from organizations");


		while($org = dbf($orgQuery)){
			$donations = [];

			// find the last balance before the date.
			$selectBalanceQuery = dbq("SELECT * FROM `donations` WHERE `organization_id` = :ORGID and donation_accounted_for <= :FROMDATE ORDER BY 5 DESC",
				[
					':ORGID' => $org['organization_id'],
					':FROMDATE' => $fromDate
				]
			);

			if(dbnr($selectBalanceQuery) > 0){
				$selectBalance = dbf($selectBalanceQuery);
				$openingBalance = $selectBalance['donation_new_balance'];
			} else {
				$openingBalance = 0;
			}
			// find donations after this time.
			$donationQuery = dbq("SELECT * FROM `donations` WHERE `organization_id` = :ORGID and donation_accounted_for > :FROMDATE ORDER BY 5 ASC",
				[
					':ORGID' => $org['organization_id'],
					':FROMDATE' => $fromDate
				]
			);

			if(dbnr($donationQuery) > 0){
				while($donation = dbf($donationQuery)){
					// build new donations.

					if($donation['donation_name'] == 'tesco_paid'){
						$openingBalance -= $donation['donation_amount'];
					} else {
						$openingBalance += $donation['donation_amount'];
					}

					// Add it in in order
					dbq("INSERT into donations (
										  organization_id,
										  organization_urn,
										  donation_name,
										  donation_accounted_for,
										  donation_amount,
										  donation_new_balance,
										  donation_category,
										  donation_paid,
										  donation_created_at,
										  donation_display_date,
										  donation_description
									  ) VALUES (
									  	  :ORGANIZATIONID,
										  :ORGANIZATIONURN,
										  :DONATIONNAME,
										  :DONATIONACCOUNTEDFOR,
										  :DONATIONAMOUNT,
										  :DONATIONNEWBALANCE,
										  :DONATIONCATEGORY,
										  :DONATIONPAID,
										  :DONATIONCREATEDAT,
										  :DONATIONDISPLAYDATE,
										  :DONATIONDESCRIPTION
									  )",
						[
							':ORGANIZATIONID' => $donation['organization_id'],
							':ORGANIZATIONURN' => $donation['organization_urn'],
							':DONATIONNAME' => $donation['donation_name'],
							':DONATIONACCOUNTEDFOR' => $donation['donation_accounted_for'],
							':DONATIONAMOUNT' => $donation['donation_amount'],
							':DONATIONNEWBALANCE' => $openingBalance,
							':DONATIONCATEGORY' => $donation['donation_category'],
							':DONATIONPAID' => $donation['donation_paid'],
							':DONATIONCREATEDAT' => $donation['donation_created_at'],
							':DONATIONDISPLAYDATE' => $donation['donation_accounted_for'],
							':DONATIONDESCRIPTION' => $donation['donation_description']
						]
					);

					// remove old one.
					dbq("delete from `donations` where donation_id = :DONID",
						[
							':DONID' => $donation['donation_id']
						]
					);

				}
			}


		}
		return true;
		
	}

	public function processextrarawdonations(){
		$donationQuery = dbq("select * from raw_donations where processed = 0 order by order_date asc");

		$donationArray = [];
		while($donation = dbf($donationQuery)){
			// build an array.
			$parentorgurn = getMasterURN($donation['school_urn']);
			$parentorgid = getOrgIDFromURN($parentorgurn);

			if($parentorgid == 0){
				$parentorgid = $donation['school_urn'];
			}

			if(!isset($donationArray[$parentorgid])){
				$donationArray[$parentorgid] = [];
			}

			$month = substr($donation['order_date'],0,7);
			if(!isset($donationArray[$parentorgid][$month])){
				if(!isset($donationArray[$parentorgid])){
					$donationArray[$parentorgid] = [];
				}
				$donationArray[$parentorgid][$month] = [];
			}

			if(!isset($donationArray[$parentorgid][$month][$donation['style_ref']])){
				$donationArray[$parentorgid][$month][$donation['style_ref']] = 0;
			}

			$donationArray[$parentorgid][$month][$donation['style_ref']] += $donation['line_donation_value'];

		}

	//	return $donationArray;

		// now add as a donation accrued in that month,

		foreach($donationArray as $orgID => $donationdate){
			foreach($donationdate as $datestring => $valueset){
				foreach($valueset as $descstring => $value) {

					// need to get latest balance.
					$urnBalanceQuery = dbq("select * from donations where organization_id = :ORGID order by donation_id desc limit 1 ",
						[
							':ORGID' => $orgID
						]
					);

					$currentBalance = 0;
					if(dbnr($urnBalanceQuery) > 0){
						$urnBalance = dbf($urnBalanceQuery);
						$currentBalance = $urnBalance['donation_new_balance'];
					}

					$currentBalance = $currentBalance + $value;
					$currentBalance = max(0,$currentBalance);

					$orgDetail = getOrgDetails($orgID);

					// insert a 'tesco_donation'
					dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
						[
							':ORGANIZATION_ID' => $orgID,
							':ORGANIZATION_URN' => $orgDetail['school_URN'],
							':DONATION_NAME' => 'tesco_donation',
							':DONATION_ACCOUNTED_FOR' => date("Y-m-d",strtotime('last day of this month',strtotime($datestring.'-01'))),
							':DONATION_AMOUNT' => $value,
							':DONATION_NEW_BALANCE' => $currentBalance,
							':DONATION_CATEGORY' => 5,
							':DONATION_PAID' => 0,
							':DONATION_DISPLAY_DATE' => date("Y-m-d",strtotime('last day of this month',strtotime($datestring.'-01'))),
							':DONATION_DESCRIPTION' => 'Extra donation during '.date("F",strtotime($datestring.'-01')).' '.date("Y",strtotime($datestring.'-01'))." (".$descstring.")"
						]
					);

					// update organization balance.
					dbq("update organizations set organization_funds_balance = :BALANCE, organization_funds = organization_funds + :THISDONATION where organization_id = :ORGID",
						[
							':BALANCE' => $currentBalance,
							':THISDONATION' => $value,
							':ORGID' => $orgID
						]
					);

				}
			}
		}
		return true;
	}

	public function processrawrefunds(){
		$refundQuery = dbq("select * from raw_refunds where processed = 0 order by order_date asc");

		$refundArray = [];
		while($refund = dbf($refundQuery)){
			// build an array.
			$parentorgurn = getMasterURN($refund['school_urn']);
			$parentorgid = getOrgIDFromURN($parentorgurn);

			if($parentorgid == 0){
				$parentorgid = $refund['school_urn'];
			}

			if(!isset($refundArray[$parentorgid])){
				$refundArray[$parentorgid] = [];
			}

			$month = substr($refund['order_date'],0,7);
			if(!isset($refundArray[$parentorgid][$month])){
				$refundArray[$parentorgid][$month];
			}

			if(!isset($refundArray[$parentorgid][$month][$refund['style_ref']])){
				$refundArray[$parentorgid][$month][$refund['style_ref']] = 0;
			}

			$refundArray[$parentorgid][$month][$refund['style_ref']] += $refund['line_donation_value'];

		}

		//	return $donationArray;

		// now add as a donation accrued in that month,

		foreach($refundArray as $orgID => $refunddate){
			foreach($refunddate as $datestring => $valueset){
				foreach($valueset as $descstring => $value) {

					// need to get latest balance.
					$urnBalanceQuery = dbq("select * from donations where organization_id = :ORGID order by donation_id desc limit 1 ",
						[
							':ORGID' => $orgID
						]
					);

					$currentBalance = 0;
					if(dbnr($urnBalanceQuery) > 0){
						$urnBalance = dbf($urnBalanceQuery);
						$currentBalance = $urnBalance['donation_new_balance'];
					}

					$currentBalance = $currentBalance + $value;
					$currentBalance = max(0,$currentBalance);

					$orgDetail = getOrgDetails($orgID);

					// insert a 'tesco_donation'
					dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
						[
							':ORGANIZATION_ID' => $orgID,
							':ORGANIZATION_URN' => $orgDetail['school_URN'],
							':DONATION_NAME' => 'bank_refund',
							':DONATION_ACCOUNTED_FOR' => date("Y-m-d",strtotime('last day of this month',strtotime($datestring.'-01'))),
							':DONATION_AMOUNT' => $value,
							':DONATION_NEW_BALANCE' => $currentBalance,
							':DONATION_CATEGORY' => 9,
							':DONATION_PAID' => 0,
							':DONATION_DISPLAY_DATE' => date("Y-m-d"),
							':DONATION_DESCRIPTION' => $descstring
						]
					);

					// update organization balance.
					dbq("update organizations set organization_funds_balance = :BALANCE, organization_funds = organization_funds + :THISDONATION where organization_id = :ORGID",
						[
							':BALANCE' => $currentBalance,
							':THISDONATION' => $value,
							':ORGID' => $orgID
						]
					);

				}


			}
		}
		return true;
	}

	public function finddsts(){


		$conn_id = ftp_connect(FTP_HOST);
		if ($conn_id) {
			$login_result = ftp_login($conn_id, FTP_USER, FTP_PASS);
			if ($login_result) {
				ftp_pasv($conn_id, true);

				$filelist = glob('dstcsv/*.csv');

				foreach($filelist as $file){

					$dstfile = basename(str_replace('_LU.csv','.dst',$file));

					$urn = @reset(explode('-',$dstfile));

					echo $urn."<br />";

					ftp_get($conn_id, 'dstcsv/'.$dstfile,'/Tesco/Approved Schools/' . $urn . '/'.$dstfile,FTP_BINARY);

					//echo 'dstcsv/'.$dstfile. ' - ' . '/Tesco/Approved Schools/' . $urn . '/'.$dstfile."<br />";
					//die;
				}

			}
		}

	}

	public function normalizethreads(){

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$substitutions = [
			'1006' => '1000',
			'1068' => '1069',
			'1002' => '1001',
			'1003' => '1001',
			'1004' => '1001',
			'1005' => '1001',
			'1134' => '1076',
			'1168' => '1169',
			'1180' => '1223',
			'1385' => '1384',
			'1079' => '1051',
			'1133' => '1029'
		];

		dbq("UPDATE organization_logos SET logo_substitutions_made = 0 WHERE ( `thread_array` LIKE '%1006%' OR `thread_array` LIKE '%1068%' OR `thread_array` LIKE '%1005%' OR `thread_array` LIKE '%1134%' OR `thread_array` LIKE '%1168%' OR `thread_array` LIKE '%1180%' OR `thread_array` LIKE '%1385%' OR `thread_array` LIKE '%1079%' OR `thread_array` LIKE '%1133%' OR `thread_array` LIKE '%1002%' OR `thread_array` LIKE '%1003%' OR `thread_array` LIKE '%1004%') AND `logo_approved` = '1'");

		$logoQuery = dbq("select * from organization_logos where logo_approved = 1 and logo_substitutions_made = 0");

		while($logo = dbf($logoQuery)){
			// check if thread is in thread array.
			$threadArray = explode(',',$logo['thread_array']);
			$changed = false;
			foreach($threadArray as $key => $thread){
				if(in_array($thread,array_keys($substitutions))){
					$changed = true;
					$threadArray[$key] = $substitutions[$thread];
				}
			}

			if($changed){
				dbq("update organization_logos set thread_array = :THREADARRAY, needs_to_update_ebos = 1, needs_ebos_update = 0, logo_substitutions_made = 1 where organization_logo_id = :ORGLOGOID limit 1",
					[
						':THREADARRAY' => implode(',',$threadArray),
						':ORGLOGOID' => $logo['organization_logo_id'],
					]
				);

			} else {
				dbq("update organization_logos set logo_substitutions_made = 1 where organization_logo_id = :ORGLOGOID limit 1",
					[
						':ORGLOGOID' => $logo['organization_logo_id'],
					]
				);
			}
		}

		return $data;
	}

	public function schooplepaid()
	{

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$houseIDs = [
			'901',
			'902',
			'903',
			'904',
			'905',
			'906',
			'907',
			'908',
			'909',
			'910',
			'911',
			'912',
			'913',
			'914',
			'915',
			'916',
			'917',
			'918',
			'919',
		];

		// get the uploaded data.
		$organizationBalanceQuery = dbq("SELECT * from schoople_payment where amount > 0");

		while($organizationBalance = dbf($organizationBalanceQuery)){

			$masterURN = getMasterURN($organizationBalance['urn']);

			$houseident = substr($masterURN,0,3);

			if(in_array($houseident,$houseIDs)){
				// probably a house not on the system, just knock off the first 3 chars.
				$masterURN = substr($masterURN,3);
			}

			$masterOID = getOrgIDFromURN($masterURN);

			if(!isset($csv[$masterURN])){
				// first one.
				$csv[$masterURN] = [
					'org_id' => $masterOID,
					'school_id' => $masterURN,
					'school_name' => getSchoolNameFromURN($masterURN),
					'included_urns' => '',
					'amount' => 0,
				];
			}
			$csv[$masterURN]['amount'] += $organizationBalance['amount'];
		}

		// return $csv;

		// make it into a CSV
		foreach($csv as $csvline){

			// insert a 'tesco_donation'
			if($masterOID == 0){
				// create a organization or ignore?

			}

			if($csvline['amount'] > 0){

				// need to get latest balance.
				$urnBalanceQuery = dbq("select * from donations where organization_urn = :ORGURN order by donation_id desc limit 1 ",
					[
						':ORGURN' => $csvline['school_id']
					]
				);

				$currentBalance = 0;
				if(dbnr($urnBalanceQuery) > 0){
					$urnBalance = dbf($urnBalanceQuery);
					$currentBalance = $urnBalance['donation_new_balance'];
				}

				$currentBalance = $currentBalance - $csvline['amount'];

				// insert a 'tesco_donation'
				dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
					[
						':ORGANIZATION_ID' => $csvline['org_id'],
						':ORGANIZATION_URN' => $csvline['school_id'],
						':DONATION_NAME' => 'tesco_paid',
						':DONATION_ACCOUNTED_FOR' => '2015-09-30',
						':DONATION_AMOUNT' => $csvline['amount'],
						':DONATION_NEW_BALANCE' => $currentBalance,
						':DONATION_CATEGORY' => 1,
						':DONATION_PAID' => 1,
						':DONATION_DISPLAY_DATE' => '2015-09-30',
						':DONATION_DESCRIPTION' => 'Total amount paid by Schoople (Fabruary 1st 2015 to September 30th 2015)'
					]
				);
			}
		}
		return true;
	}

	public function tescopaid()
	{

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$houseIDs = [
			'901',
			'902',
			'903',
			'904',
			'905',
			'906',
			'907',
			'908',
			'909',
			'910',
			'911',
			'912',
			'913',
			'914',
			'915',
			'916',
			'917',
			'918',
			'919',
		];

		// get the uploaded data.
		$organizationBalanceQuery = dbq("SELECT * from tesco_payments where amount > 0");

		while($organizationBalance = dbf($organizationBalanceQuery)){

			$masterURN = getMasterURN($organizationBalance['urn']);

			$houseident = substr($masterURN,0,3);

			if(in_array($houseident,$houseIDs)){
				// probably a house not on the system, just knock off the first 3 chars.
				$masterURN = substr($masterURN,3);
			}

			$masterOID = getOrgIDFromURN($masterURN);

			if(!isset($csv[$masterURN])){
				// first one.
				$csv[$masterURN] = [
					'org_id' => $masterOID,
					'school_id' => $masterURN,
					'school_name' => getSchoolNameFromURN($masterURN),
					'included_urns' => '',
					'amount' => 0,
				];
			}
			$csv[$masterURN]['amount'] += $organizationBalance['amount'];
		}

		// return $csv;

		// make it into a CSV
		foreach($csv as $csvline){

			// insert a 'tesco_donation'
			if($masterOID == 0){
				// create a organization or ignore?

			}

			if($csvline['amount'] > 0){

				// need to get latest balance.
				$urnBalanceQuery = dbq("select * from donations where organization_urn = :ORGURN order by donation_id desc limit 1 ",
					[
						':ORGURN' => $csvline['school_id']
					]
				);

				$currentBalance = 0;
				if(dbnr($urnBalanceQuery) > 0){
					$urnBalance = dbf($urnBalanceQuery);
					$currentBalance = $urnBalance['donation_new_balance'];
				}

				$currentBalance = $currentBalance - $csvline['amount'];

				// insert a 'tesco_donation'
				dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
					[
						':ORGANIZATION_ID' => $csvline['org_id'],
						':ORGANIZATION_URN' => $csvline['school_id'],
						':DONATION_NAME' => 'tesco_paid',
						':DONATION_ACCOUNTED_FOR' => '2015-01-31',
						':DONATION_AMOUNT' => $csvline['amount'],
						':DONATION_NEW_BALANCE' => $currentBalance,
						':DONATION_CATEGORY' => 0,
						':DONATION_PAID' => 1,
						':DONATION_DISPLAY_DATE' => '2015-01-31',
						':DONATION_DESCRIPTION' => 'Total amount paid by Tesco up to January 31st 2015'
					]
				);
			}
		}
		return true;
	}

/*SELECT
p.`product_name`,
p.`product_description`,
p.`product_size_text`,
p.`product_image`,
p.`product_decorated`,
pca.`product_category_name`,
p.`product_emblem_small`,
p.`product_new`,
p.`product_soon`,
p.`product_awaiting`,
p.`product_price`,
p.`product_emblem_position`,
pc.`product_colour_style_ref`,
c.`colour_name`,
pc.`product_colour_image_url`,
ps.`products_size_name`,
ps2pc.`product_variation_sku`,
ps2pc.`product_variation_price`

FROM products p JOIN product_colours pc USING (`product_id`)
JOIN products_sizes_to_products_colours ps2pc ON (ps2pc.`product_colour_id` = pc.`product_colour_id`)
JOIN colours c ON (c.`colour_id` = pc.`colour_id`)
JOIN product_sizes ps ON (ps.`product_size_id` = ps2pc.`product_size_id`)
JOIN product_categories pca ON (pca.`product_category_id` = p.`product_category_id`)
ORDER BY p.`product_id`, pc.`product_colour_id`, ps.`product_size_id`*/

	public function checkbanks()
	{
		$bankQuery = dbq("SELECT * FROM organizations o JOIN organization_bank_accounts ob USING (organization_id) WHERE o.`organization_bank_verified` = 0 limit 100");

		include(DIR_CLASSES.'bankcheck.php');

		while ($bank = dbf($bankQuery)) {
			//echo "<pre>".print_r($bank,1)."</pre>";
			$pa = new BankAccountValidation ("FN97-ZM77-HE35-FA68", $bank['organization_bank_account'], $bank['organization_bank_sort_code_1']."-".$bank['organization_bank_sort_code_2']."-".$bank['organization_bank_sort_code_3']);
			$pa->MakeRequest();
			if ($pa->HasData()) {
				$data = $pa->HasData();

				// echo "<pre>".print_r($data,1)."</pre>";
				// echo $item["IsDirectDebitCapable"];
				// echo $item["FasterPaymentsSupported"];
				// echo $item["CHAPSSupported"];

				if($data[0]['IsCorrect'][0] == 'True'){

					// bank account is correct.

					//if($data[0]['IsDirectDebitCapable'][0] == true){

						// mark as valid.
						dbq('update organizations set organization_bank_verified = 1 where organization_id = :ORGID',
							[
								':ORGID' => $bank['organization_id']
							]
						);
						// add audit.
						audit(23,$bank['organization_id'],'Bank details verified.');
					/*} else {
						// die('Bank NOT Valid');
						// remove bank details.
						dbq("delete from organization_bank_accounts where organization_id = :ORGID",
							[
								':ORGID' => $bank['organization_id']
							]
						);
						// add audit.
						audit(23,$bank['organization_id'],'Bank details failed verification - Doesn\'t support BACS payments - Account Removed.');
					}*/

				} else {

					// remove bank details.
					dbq("delete from organization_bank_accounts where organization_id = :ORGID",
						[
							':ORGID' => $bank['organization_id']
						]
					);
					// add audit.
					audit(23,$bank['organization_id'],'Bank details failed verification - Details removed.');
				}
			}
		//	die;
		}
	}

	public function updateprimaries(){

		$orgQuery = dbq("SELECT *, COUNT(*) AS primaries FROM users_to_organizations GROUP BY organization_id ORDER BY primaries DESC, user_id ASC");

		while($org = dbf($orgQuery)) {
			if($org['primaries'] == 1){
				// dont need to do 1s.
				break;
			}

			// update them all to not primary, then update the first one to primary?
			dbq('update users_to_organizations set user_primary = 0 where organization_id = :ORGID and user_id != :USERID',
				[
					':ORGID' => $org['organization_id'],
					':USERID' => $org['user_id']
				]
			);

		}
	}

	public function updatepaid(){

		$orgQuery = dbq("SELECT * FROM
							organizations o
  							JOIN organization_bank_accounts oba USING (organization_id)
							WHERE o.`organization_id` NOT IN
								(SELECT organization_id FROM
    									audit_log
    									WHERE audit_log_event_id = 23
    							)");

		while($org = dbf($orgQuery)) {
			audit(23,$org['organization_id'],'Bank details verified.');
		}
	}

	public function updatebalance(){

		ini_set('memory_limit',-1);
		session_destroy();

		$orgQuery = dbq("select * from organizations");

		while($org = dbf($orgQuery)){
			// select latest donation.
			$donationQuery = dbq("select * from donations where organization_id = :ORGID ORDER BY donation_id DESC limit 1",
				[
					':ORGID' => $org['organization_id']
				]
			);

			if(dbnr($donationQuery) == 0){
				// set same as total.
				$newBalance = $org['organization_funds'];
			} else {
				// set as balance.
				$donation = dbf($donationQuery);
				$newBalance = $donation['donation_new_balance'];
			}

			dbq("update organizations set organization_funds_balance = :NEWBAL where organization_id = :ORGID",
				[
					':NEWBAL' => $newBalance,
					':ORGID' => $org['organization_id']
				]
			);
		}
	}

	public function updatefunds(){

		ini_set('memory_limit',-1);
		ini_set('max_execution_time', 0);

		dbq("UPDATE organizations SET organization_funds = 0");
		dbq("UPDATE organizations o JOIN (
								SELECT
									oo.organization_id,
									SUM(d.donation_amount) AS totaldonation
								FROM
									organizations oo
								JOIN
									donations d ON d.`organization_id` = oo.`organization_id`
									AND d.donation_name = 'tesco_donation'
								GROUP BY d.`organization_id`)
							  AS donated ON (donated.organization_id = o.organization_id)
							  SET o.organization_funds = donated.totaldonation;");
	}

	public function updateproductprices(){
		$productQuery = dbq("select * from products");

		while($product = dbf($productQuery)){

			echo "select min(psc.product_variation_price) as new_price from products_sizes_to_products_colours psc join product_colours pc on (pc.product_colour_id = psc.product_colour_id) join products p on (p.product_id = pc.product_id) where p.product_id = ".$product['product_id'];
			$minPriceQuery = dbq("select min(psc.product_variation_price) as new_price from products_sizes_to_products_colours psc join product_colours pc on (pc.product_colour_id = psc.product_colour_id) join products p on (p.product_id = pc.product_id) where p.product_id = :PRODID",
				[
					':PRODID' => $product['product_id']
				]
			);

			while($minPrice = dbf($minPriceQuery)){
				var_dump($minPrice);
				dbq('update products set product_price = :NEWPRICE where product_id = :PRODID',
					[
						':NEWPRICE' => $minPrice['new_price'],
						':PRODID' => $product['product_id']
					]
				);
			}
		}
	}


	public function countthreads(){

		$getLogosQuery = dbq("SELECT thread_array FROM organization_logos ol WHERE ol.`logo_approved` = 1 GROUP BY tesco_style_ref");
		$threadsUsed = [];

		$threadSubs = [
			'1068' => '1069',
			'1168' => '1169',
			'1079' => '1051',
			'1002' => '1001',
			'1003' => '1001',
			'1004' => '1001',
			'1005' => '1001',
			'1801' => '1001',
			'1134' => '1076',
			'1180' => '1223',
			'1385' => '1384',
			'1133' => '1029',
			'1006' => '1000'
		];

		while($getLogos = dbf($getLogosQuery)){

			$logoThreadsUsed = explode(',',$getLogos['thread_array']);

			$logoThreadsUsed = array_unique($logoThreadsUsed);

			foreach($logoThreadsUsed as $key => $value){
				//$value = trim($value);

				if(in_array($value,array_keys($threadSubs))){
					// use the substitute thread
					$value = $threadSubs[$value];
				}

					if(!isset($threadsUsed[$value])){
						$threadsUsed[$value] = 0;
					}

					$threadsUsed[$value] += 1;

			}

		}


		arsort($threadsUsed);
		$i = 0;
		foreach($threadsUsed as $code => $count){
			dbq("update threads set logos_using = :LU, standard = :STD where colour_code = :CODE",
				[
					':LU' => $count,
					':STD' => (int)($i < 50),
					':CODE' => $code
				]
			);
			$i++;
		}
		print_r($threadsUsed);
		die;
		return $threadsUsed;

	}

	public function testebostoken()
	{
		include(DIR_CLASSES . 'eBOS_API.php');
		$eBOS_API = new eBOS_API(APIUser, APIPass);

		$design = $eBOS_API->getDesignDetails(152240);

		$designupdatedetail = [
			'design' => [
				'number' => $design->design->number, # required
				'name' => $design->design->name, # required
				'gallery_images' => [
					[
						'data' => base64_encode(file_get_contents('download.png')),
						'filename' => "download.png",
						'mime_type' => "image/png"
					],
        		],
			]
		];

		$newDesign = $eBOS_API->updateDesign(152240,$designupdatedetail);

		return (array)$design;
	}

	public function testemb(){

		$file = '16143330_D0393242A.emb';

		include(DIR_CLASSES . 'eBOS_API.php');
		$eBOS_API = new eBOS_API(APIUser, APIPass);
/*
		$data_string = '<xml>
                    <files>
                        <file filename="'.$file.'" filecontents="'.base64_encode(file_get_contents($file)).'"/>
                    </files>
                 </xml>' ;

		$api_connect = [
			'appId' => WILCOM_API_APP_ID,
			'appKey' => WILCOM_API_KEY,
			'requestXml' => $data_string
		];

		$postdata = http_build_query($api_connect);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://ewa.wilcom.com/1.3/api/designInfo');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		curl_close($ch);

		$xml_output = simplexml_load_string( $output );
		$json_convert = json_encode((array) $xml_output );
		$json = (array)json_decode( $json_convert,true );
*/

//


		$getThreadsFromEMB[] = $eBOS_API->getThreadsFromEMB('8531420-1C-TMP.emb');
		$getThreadsFromEMB[] = $eBOS_API->getThreadsFromEMB('16143330_D0393242A.emb');

		return $getThreadsFromEMB;
	}

	public function addsamplelogos(){

		include_once(DIR_CLASSES . 'eBOS_API.php');
		$eBOS_API = new eBOS_API(APIUser, APIPass);

		$logoQuery = dbq("SELECT * FROM ss_orders sso JOIN organization_logos ol ON ol.tesco_style_ref = sso.LogoID1 where sso.unique_id = 1");

		while($logodetail = dbf($logoQuery)){

			$size = explode('x',$logodetail['size__mm_']);

			$LUcsv = 'Design Number,Name,Type,Width,Height,Stitch Count,Colours,Last modified,Threads,Notes'."\n";

			$LUcsv .= '"'.$logodetail['tesco_style_ref'].'",';
			$LUcsv .= '"'.$logodetail['school_name'].'",';
			$LUcsv .= '"Embroidery",';
			$LUcsv .= '"'.$size[0].'",';
			$LUcsv .= '"'.$size[1].'",';
			$LUcsv .= '"'.$logodetail['stitch_count'].'",';
			$LUcsv .= '"'.count(explode(',',$logodetail['thread_array'])).'",';
			$LUcsv .= '"'.date("c",$logodetail['logo_last_updated']).'",';
			$LUcsv .= '"'.$logodetail['thread_array'].'",';
			$LUcsv .= '""';

			$LUFileName = 'SampleLogos/'.$logodetail['tesco_style_ref'].'_LU.csv';

			file_put_contents($LUFileName,$LUcsv);

			$remotefile = '/Tesco/SampleLogos/'.$logodetail['tesco_style_ref'].'_LU.csv';
			$localfile = API_ROOT . $LUFileName;

			dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
				[
					':LOCALFILE' => $localfile,
					':REMOTEFILE' => $remotefile,
					':HOSTPREFIX' => ''
				]
			);

			$oldDesign = $eBOS_API->getDesignDetails($logodetail['ebos_id']);

			$DSTFileName = 'SampleLogos/'.$logodetail['tesco_style_ref'].'.dst';

			file_put_contents($DSTFileName,file_get_contents($oldDesign->design->dst->url));

			$remotefile = '/Tesco/SampleLogos/'.$logodetail['tesco_style_ref'].'.dst';
			$localfile = API_ROOT . $DSTFileName;

			dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
				[
					':LOCALFILE' => $localfile,
					':REMOTEFILE' => $remotefile,
					':HOSTPREFIX' => ''
				]
			);
		}

	}

	public function importnewsales(){
		ini_set('max_execution_time', 0);
		session_destroy(); // close session so we can do other stuff.

		// load file.
		$testFile =  'Donations12.10.17.csv';
		$fileHandle = fopen($testFile,'r+');

		$headers = [];
		// Loop through each line of the file in turn
		while (($rowData = fgetcsv($fileHandle, 0)) !== FALSE) {
			if(empty($headers)){
				$headers = array_flip($rowData);
				continue;
			}
			/*
			 {
				"ORDER_NO": 0,
				"SCHOOL_ID": 1,
				"ORDER_DATE": 2,
				"DESPATCH_DATE": 3,
				"STYLE_REFERENCE": 4,
				"EAN": 5,
				"QUANTITY_SOLD": 6,
				"PRICE_PER_UNIT": 7,
				"PRICE_PAID": 8,
				"DONATION": 9,
				"Style Ref": 10
			}
			*/

			dbq("INSERT INTO organization_sales_redo (
						school_urn,
						dispatched_date,
						order_number,
						style_ref,
						EAN,
						quantity,
						price_paid,
						donation_amount
					) VALUES (
						:SCHOOL_URN,
						:DISPATCHED_DATE,
						:ORDER_NUMBER,
						:STYLE_REF,
						:EAN,
						:QUANTITY,
						:PRICE_PAID,
						:DONATION_AMOUNT
					)",
				[
					':SCHOOL_URN' => getMasterURN($rowData[$headers['SCHOOL_ID']]),
					':DISPATCHED_DATE' => date("Y-m-d",DateTime::createFromFormat('!d/m/Y', $rowData[$headers['DESPATCH_DATE']])->getTimestamp()),
					':ORDER_NUMBER' => $rowData[$headers['ORDER_NO']],
					':STYLE_REF' => $rowData[$headers['Style Ref']],
					':EAN' => $rowData[$headers['EAN']],
					':QUANTITY' => $rowData[$headers['QUANTITY_SOLD']],
					':PRICE_PAID' => $rowData[$headers['PRICE_PAID']],
					':DONATION_AMOUNT' => $rowData[$headers['DONATION']]
				]
			);

		}

	}


	public function copyredosales(){
		die;
		ini_set('max_execution_time', 0);
		session_destroy(); // close session so we can do other stuff.
		$donationQuery = dbq("SELECT * FROM organization_sales_redo osr WHERE osr.accounted = 0");

		if(dbnr($donationQuery) > 0) {
			while ($donation = dbf($donationQuery)) {
				dbq("INSERT INTO organization_sales (
						school_urn,
						dispatched_date,
						order_number,
						style_ref,
						EAN,
						quantity,
						price_paid,
						donation_amount
					) VALUES (
						:SCHOOL_URN,
						:DISPATCHED_DATE,
						:ORDER_NUMBER,
						:STYLE_REF,
						:EAN,
						:QUANTITY,
						:PRICE_PAID,
						:DONATION_AMOUNT
					)",
					[
						':SCHOOL_URN' => $donation['school_urn'],
						':DISPATCHED_DATE' => $donation['dispatched_date'],
						':ORDER_NUMBER' => $donation['order_number'],
						':STYLE_REF' => $donation['style_ref'],
						':EAN' => $donation['EAN'],
						':QUANTITY' => $donation['quantity'],
						':PRICE_PAID' => $donation['price_paid'],
						':DONATION_AMOUNT' => $donation['donation_amount']
					]
				);
			}
		}
	}

	public function processothersales(){
		die;
		session_destroy(); // close session so we can do other stuff.

		ini_set('max_execution_time','4000');
		ini_set('memory_limit',-1);

		$donationQuery = dbq("SELECT school_urn, DATE_FORMAT(dispatched_date, '%Y-%m') AS period, SUM(donation_amount) AS donation_amount FROM organization_sales os WHERE os.accounted = 0 GROUP BY school_urn, DATE_FORMAT(dispatched_date, '%Y%m') ORDER BY school_urn, DATE_FORMAT(dispatched_date, '%Y%m')");

		if(dbnr($donationQuery) > 0){
			while($donation = dbf($donationQuery)){
				// add a donation entry.
				// insert a 'tesco_donation'

				$donationDate = date("Y-m-d",strtotime('last day of this month',strtotime($donation['period'].'-01')));

				$orgID = getOrgIDFromURN($donation['school_urn']);
				// need to get latest balance.
				$urnBalanceQuery = dbq("select * from donations where organization_id = :ORGID order by donation_id desc limit 1 ",
					[
						':ORGID' => $orgID
					]
				);

				$currentBalance = 0;
				if(dbnr($urnBalanceQuery) > 0){
					$urnBalance = dbf($urnBalanceQuery);
					$currentBalance = $urnBalance['donation_new_balance'];
				}

				$currentBalance = $currentBalance + $donation['donation_amount'];
				$currentBalance = max(0,$currentBalance);

				$orgDetail = getOrgDetails($orgID);

				// insert a 'tesco_donation'
				dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
					[
						':ORGANIZATION_ID' => $orgID,
						':ORGANIZATION_URN' => $orgDetail['school_URN'],
						':DONATION_NAME' => 'tesco_donation',
						':DONATION_ACCOUNTED_FOR' => $donationDate,
						':DONATION_AMOUNT' => $donation['donation_amount'],
						':DONATION_NEW_BALANCE' => $currentBalance,
						':DONATION_CATEGORY' => 5,
						':DONATION_PAID' => 0,
						':DONATION_DISPLAY_DATE' => $donationDate,
						':DONATION_DESCRIPTION' => 'Donation accrued during '.date("F",strtotime($donation['period'].'-01')).' '.date("Y",strtotime($donation['period'].'-01'))
					]
				);

				// update organization balance.
				dbq("update organizations set organization_funds_balance = :BALANCE, organization_funds = organization_funds + :THISDONATION where organization_id = :ORGID",
					[
						':BALANCE' => $currentBalance,
						':THISDONATION' => $donation['donation_amount'],
						':ORGID' => $orgID
					]
				);

				// Test
				dbq("update organization_sales set accounted = 2 where school_urn = :SCHOOL_URN and accounted = 0 and DATE_FORMAT(dispatched_date, '%Y-%m') = :DATEPERIOD",
					[
						':SCHOOL_URN' => $donation['school_urn'],
						':DATEPERIOD' => $donation['period']
					]
				);
			}
		}
	}
}
