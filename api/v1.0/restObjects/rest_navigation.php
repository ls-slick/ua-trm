<?php

/**
 * Rest object example
 * 
 * GNU General Public License (Version 2, June 1991) 
 * 
 * This program is free software; you can redistribute 
 * it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, 
 * or (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class navigation extends restObject {

	var $ignorePages = [
		'/session',
		'/login',
		'/logoff'
	];
	/**
	 * 
	 * @param string $method
	 * @param array $request
	 * @param string $file
	 */
	public function __construct($method, $request = null, $file = null) {
		parent::__construct($method, $request, $file);
	}
	

	public function snapshot() {

		if (is_array($this->request) && isset($this->request['p']) && !in_array($this->request['p'],$this->ignorePages)){
			$_SESSION['lastpage'] = $this->request['p'];
		}

		return [true];
	}


}
