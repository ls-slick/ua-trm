<?php

/**
 * Rest object example
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class emails extends restObject
{

    /**
     * user data
     */
    public $id;
    public $name;
    public $lastName;
    public $login;

    /**
     *
     * @param string $method
     * @param array $request
     * @param string $file
     */
    public function __construct($method, $request = null, $file = null)
    {
        parent::__construct($method, $request, $file);
        require_once DIR_CLASSES.'Mandrill.php'; //Not required with Composer

    }


    public function listtemplates()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $data = [];
        $mandrill = new Mandrill(MANDRILL_API_KEY);

        $label = '';
        $data = $mandrill->templates->getList($label);

        //print_r($result);


        return $data;
    }


    public function findemail()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $data = [];
        $mandrill = new Mandrill(MANDRILL_API_KEY);
        $name = $this->request['search'];
        $label = '';
        $date_from = '2013-01-01';
        $date_to = date("Y-m-d");
        $senders = array('noreply@ff-ues.com');
        $data = $mandrill->messages->search($name, $date_from, $date_to, ['ff-pass-reset'], $senders, [MANDRILL_API_KEY]);
        //$result = $mandrill->messages->search($query);

        foreach($data as $key => $moredata){
            $emailContent = $mandrill->messages->content($moredata['_id']);
            $data['contents'][] = $emailContent;

        }
        //print_r($result);


        return $data;
    }

    public function gettemplate()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $data = [];
        try {
        $mandrill = new Mandrill(MANDRILL_API_KEY);

        $name = $this->request['name'];
        $data = $mandrill->templates->info($name);
        //print_r($result);
        } catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            // echo 'A mandrill error occurred: ' . get_class($data) . ' - ' . $data->getMessage();
            // A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
           // throw $data;
           $data = (array)$e;
        }

        return $data;
    }

    public function sendEmail()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        // What type of email?  does it matter?
        $data = [];
        $mandrill = new Mandrill(MANDRILL_API_KEY);

        $name = $this->request['name'];
        $data = $mandrill->templates->info($name);
        //print_r($result);

        return $data;
    }

}
