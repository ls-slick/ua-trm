<?php

/**
 * Rest object example
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class donations extends restObject
{

    /**
     *
     * @param string $method
     * @param array $request
     * @param string $file
     */
    public function __construct($method, $request = null, $file = null)
    {
        ini_set('max_execution_time','70000');

        parent::__construct($method, $request, $file);
    }

    public function processsales()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if(DONATIONS_PROCESSING){

            $rows = dbf(dbq("SELECT count(*) as totalRows FROM organization_sales os
							where os.accounted = 0")); // read how many total rows there are without LIMIT.
            $data['totalrows'] = intval($rows['totalRows']);

            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Donations are already processing. '.intval($rows['totalRows']).' remaining.'
            ];

            return $data;

        }

        // start the process in the background.
        exec('bash -c "exec nohup setsid wget -O /dev/null '.API_ROOT.'donations/processsalesbg > /dev/null 2>&1 &"'); // Linux

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Sales processing has started.'
        ];

        return $data;
    }


    public function processsalesbg()
    {
        if(DONATIONS_PROCESSING){
            // already running.  dont run again!
            $rows = dbf(dbq("SELECT count(*) as totalRows FROM organization_sales os
							where os.accounted = 0")); // read how many total rows there are without LIMIT.
            $data['totalrows'] = intval($rows['totalRows']);

            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Donations are already processing. '.intval($rows['totalRows']).' remaining.'
            ];

            return $data;
        }

        //session_destroy(); // close session so we can do other stuff.
        session_write_close(); // close session WITHOUT destroying it so we can do other stuff.
        ini_set('max_execution_time','70000');
        ini_set('memory_limit',-1);

        dbq("update configuration set config_value = 1 where config_key = 'DONATIONS_PROCESSING'");

        $donationQuery = dbq("SELECT o.organization_id, os.school_urn, DATE_FORMAT(os.dispatched_date, '%Y-%m') AS period, SUM(os.donation_amount) AS donation_amount FROM organization_sales os JOIN organizations o ON o.`school_URN` = os.`school_urn` WHERE os.accounted = 0 GROUP BY school_urn, DATE_FORMAT(dispatched_date, '%Y%m') ORDER BY school_urn, DATE_FORMAT(dispatched_date, '%Y%m')");

        if(dbnr($donationQuery) > 0){
            while($donation = dbf($donationQuery)){
                // add a donation entry.
                // insert a 'tesco_donation'

                $donationDate = date("Y-m-d",strtotime('last day of this month',strtotime($donation['period'].'-01')));

                $orgID = $donation['organization_id'];
                // need to get latest balance.
                $urnBalanceQuery = dbq("select * from donations where organization_id = :ORGID order by donation_id desc limit 1 ",
                    [
                        ':ORGID' => $orgID
                    ]
                );

                $currentBalance = 0;
                if(dbnr($urnBalanceQuery) > 0){
                    $urnBalance = dbf($urnBalanceQuery);
                    $currentBalance = $urnBalance['donation_new_balance'];
                }

                $currentBalance = $currentBalance + $donation['donation_amount'];
                $currentBalance = max(0,$currentBalance);

                $orgDetail = getOrgDetails($orgID);

                // insert a 'tesco_donation'
                dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
                    [
                        ':ORGANIZATION_ID' => $orgID,
                        ':ORGANIZATION_URN' => $orgDetail['school_URN'],
                        ':DONATION_NAME' => 'tesco_donation',
                        ':DONATION_ACCOUNTED_FOR' => $donationDate,
                        ':DONATION_AMOUNT' => $donation['donation_amount'],
                        ':DONATION_NEW_BALANCE' => $currentBalance,
                        ':DONATION_CATEGORY' => 5,
                        ':DONATION_PAID' => 0,
                        ':DONATION_DISPLAY_DATE' => $donationDate,
                        ':DONATION_DESCRIPTION' => 'Donation accrued during '.date("F",strtotime($donation['period'].'-01')).' '.date("Y",strtotime($donation['period'].'-01'))
                    ]
                );

                // update organization balance.
                dbq("update organizations set organization_funds_balance = :BALANCE, organization_funds = organization_funds + :THISDONATION where organization_id = :ORGID",
                    [
                        ':BALANCE' => $currentBalance,
                        ':THISDONATION' => $donation['donation_amount'],
                        ':ORGID' => $orgID
                    ]
                );

                dbq("update organization_sales set accounted = 1 where school_urn = :SCHOOL_URN and accounted = 0 and DATE_FORMAT(dispatched_date, '%Y-%m') = :DATEPERIOD",
                    [
                        ':SCHOOL_URN' => $donation['school_urn'],
                        ':DATEPERIOD' => $donation['period']
                    ]
                );


            }
        }

        dbq("update configuration set config_value = 0 where config_key = 'DONATIONS_PROCESSING'");

        //update the stats.
        $donationQuery = dbq('SELECT SUM(donation_amount) as donations_amount FROM donations WHERE donation_category IN (5)');
        $donations = dbf($donationQuery);

        // total live schools not including houses.
        $liveQuery = dbq('SELECT count(*) as live_schools FROM organizations WHERE organization_parent_organization_id is NULL and organization_status_id = 3');
        $live = dbf($liveQuery);

        dbq("update statistics set statistic_value = :DONATIONS where statistic_key = 'organization_funds_raised'",
            [
                ':DONATIONS' => round($donations['donations_amount'],2)
            ]
        );

        dbq("update statistics set statistic_value = :LIVES where statistic_key = 'organizations_registered'",
            [
                ':LIVES' => $live['live_schools']
            ]
        );

    }

    public function processpayments()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['date']) || empty($this->request['date'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        if(PAYMENTS_PROCESSING){

            $rows = dbf(dbq("SELECT count(*) as totalRows FROM slick_payments sp
							where sp.amount > 0")); // read how many total rows there are without LIMIT.
            $data['totalrows'] = intval($rows['totalRows']);

            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Payments are already processing. '.intval($rows['totalRows']).' remaining.'
            ];

            return $data;

        }

        $processDate = date("Y-m-d",strtotime('+4 hours',strtotime($this->request['date']))); // adding 4 hours to allow for DST


        dbq('update slick_payments set `date` = :DATE',
            [
                ':DATE' => $processDate
            ]
        );
        // start the process in the background.
        exec('bash -c "exec nohup setsid wget -O /dev/null '.API_ROOT.'donations/processpaymentsbg > /dev/null 2>&1 &"'); // Linux

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Payments processing has started.'
        ];

        return $data;
    }

    public function processpaymentsbg()
    {

        session_destroy(); // close session so we can do other stuff.

        ini_set('max_execution_time','7000');
        ini_set('memory_limit',-1);

        dbq("update configuration set config_value = 1 where config_key = 'PAYMENTS_PROCESSING'");

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $csv = [];
        $houseIDs = [
            '901',
            '902',
            '903',
            '904',
            '905',
            '906',
            '907',
            '908',
            '909',
            '910',
            '911',
            '912',
            '913',
            '914',
            '915',
            '916',
            '917',
            '918',
            '919',
        ];

        // get the uploaded data.
        $organizationBalanceQuery = dbq("SELECT * from slick_payments where amount > 0");

        while($organizationBalance = dbf($organizationBalanceQuery)){

            $masterURN = getMasterURN($organizationBalance['urn']);

            $houseident = substr($masterURN,0,3);

            if(in_array($houseident,$houseIDs)){
                // probably a house not on the system, just knock off the first 3 chars.
                $masterURN = substr($masterURN,3);
            }

            $masterOID = getOrgIDFromURN($masterURN);

            if(!isset($csv[$masterURN])){
                // first one.
                $csv[$masterURN] = [
                    'org_id' => $masterOID,
                    'school_id' => $masterURN,
                    'school_name' => getSchoolNameFromURN($masterURN),
                    'included_urns' => '',
                    'amount' => 0,
                    'date' => $organizationBalance['date']
                ];
            }
            $csv[$masterURN]['amount'] += $organizationBalance['amount'];
        }

        // return $csv;

        // make it into a CSV
        foreach($csv as $csvline){

            // insert a 'tesco_donation'
            if($masterOID == 0){
                // create a organization or ignore?
            }

            if($csvline['amount'] > 0){

                // need to get latest balance.
                $urnBalanceQuery = dbq("select * from donations where organization_urn = :ORGURN order by donation_id desc limit 1 ",
                    [
                        ':ORGURN' => $csvline['school_id']
                    ]
                );

                $currentBalance = 0;
                if(dbnr($urnBalanceQuery) > 0){
                    $urnBalance = dbf($urnBalanceQuery);
                    $currentBalance = $urnBalance['donation_new_balance'];
                }

                $currentBalance = $currentBalance - $csvline['amount'];
                $currentBalance = max(0,$currentBalance);

                // insert a 'tesco_donation'
                dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
                    [
                        ':ORGANIZATION_ID' => $csvline['org_id'],
                        ':ORGANIZATION_URN' => $csvline['school_id'],
                        ':DONATION_NAME' => 'tesco_paid',
                        ':DONATION_ACCOUNTED_FOR' => $csvline['date'],
                        ':DONATION_AMOUNT' => $csvline['amount'],
                        ':DONATION_NEW_BALANCE' => $currentBalance,
                        ':DONATION_CATEGORY' => 2,
                        ':DONATION_PAID' => 1,
                        ':DONATION_DISPLAY_DATE' => $csvline['date'],
                        ':DONATION_DESCRIPTION' => 'Donation paid via BACS '.date("jS F Y",strtotime($csvline['date']))
                    ]
                );

                // update organization balance.
                dbq("update organizations set organization_funds_balance = :BALANCE where organization_id = :ORGID",
                    [
                        ':BALANCE' => $currentBalance,
                        ':ORGID' => $csvline['org_id']
                    ]
                );
            }

            dbq("delete from slick_payments where urn = :URN",
                [
                    ':URN' => $csvline['school_id']
                ]
            );
        }

        dbq("update configuration set config_value = 0 where config_key = 'PAYMENTS_PROCESSING'");

        return true;
    }

    public function fixdoublepayment()
    {

        session_destroy(); // close session so we can do other stuff.

        ini_set('max_execution_time','7000');
        ini_set('memory_limit',-1);

        dbq("update configuration set config_value = 1 where config_key = 'PAYMENTS_PROCESSING'");

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $csv = [];
        $houseIDs = [
            '901',
            '902',
            '903',
            '904',
            '905',
            '906',
            '907',
            '908',
            '909',
            '910',
            '911',
            '912',
            '913',
            '914',
            '915',
            '916',
            '917',
            '918',
            '919',
        ];

        // get the uploaded data.
        $organizationBalanceQuery = dbq("SELECT * from slick_payments where amount > 0");

        while($organizationBalance = dbf($organizationBalanceQuery)){

            $masterURN = getMasterURN($organizationBalance['urn']);

            $houseident = substr($masterURN,0,3);

            if(in_array($houseident,$houseIDs)){
                // probably a house not on the system, just knock off the first 3 chars.
                $masterURN = substr($masterURN,3);
            }

            $masterOID = getOrgIDFromURN($masterURN);

            if(!isset($csv[$masterURN])){
                // first one.
                $csv[$masterURN] = [
                    'org_id' => $masterOID,
                    'school_id' => $masterURN,
                    'school_name' => getSchoolNameFromURN($masterURN),
                    'included_urns' => '',
                    'amount' => 0,
                    'date' => $organizationBalance['date']
                ];
            }
            $csv[$masterURN]['amount'] += $organizationBalance['amount'];
        }

        // return $csv;

        // make it into a CSV
        foreach($csv as $csvline){

            // insert a 'tesco_donation'
            if($masterOID == 0){
                // create a organization or ignore?
            }

            if($csvline['amount'] > 0){

                dbq('DELETE FROM donations where organization_id = :ORGANIZATION_ID ORDER BY donation_id DESC limit 1',
                    [
                        ':ORGANIZATION_ID' => $csvline['org_id']
                    ]
                );
            }

            dbq("delete from slick_payments where urn = :URN",
                [
                    ':URN' => $csvline['school_id']
                ]
            );
        }

        dbq("update configuration set config_value = 0 where config_key = 'PAYMENTS_PROCESSING'");

        return true;
    }

    public function uploadpayments()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }


        if (!isset($this->request['file']) || empty($this->request['file'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $fileData = array_map('str_getcsv', preg_split("/(\r\n|\n|\r)/",base64_decode(@end(explode(',',$this->request['file']['data'])))));
        $date = $this->request['date'];

        // Insert each one to slick_patments.

        $h = [
           'sort_code',
           'name',
           'account_no',
           'amount',
           'reference',
           '99',
        ];

        foreach($fileData as $rawline){

            if(sizeof($h)==sizeof($rawline)) {
                $csvline = array_combine($h,$rawline);

                dbq('INSERT INTO slick_payments (
                    urn,
                    amount
                  ) VALUES (
                    :URN,
                    :AMOUNT
                  )',
                    [
                        ':URN' => filter_var($csvline['reference'], FILTER_SANITIZE_NUMBER_INT),
                        ':AMOUNT' => $csvline['amount']
                    ]
                );
            }

        }

        return $fileData;
    }

    public function getfailedpayments()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }
        $data = ['data' => []];
        // Admin has extra info.
        if (isset($_SESSION['session']) && $_SESSION['session']['user']['user_type_id'] == 10) {
            // select it.
            $failedQuery = dbq("SELECT * FROM slick_failed_payments sfp");


            while ($failed = dbf($failedQuery)) {
                $data['data'][] = $failed;
            }

        }

        return $data;
    }


    public function getreturnedpayments()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }
        $data = ['data' => []];
        // Admin has extra info.
        if (isset($_SESSION['session']) && $_SESSION['session']['user']['user_type_id'] == 10) {
            // select it.
            $failedQuery = dbq("SELECT * FROM slick_returned_payments srp");


            while ($failed = dbf($failedQuery)) {
                $data['data'][] = $failed;
            }

        }

        return $data;
    }

    public function downloadfixed()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $data = [
            'data' => [
                'filecontent' => '',
                'filename' => ''
            ]
        ];

        // Admin has extra info.
        if (isset($_SESSION['session']) && $_SESSION['session']['user']['user_type_id'] == 10) {
            // select it.
            $failedQuery = dbq("SELECT * FROM slick_failed_payments sfp");

            while ($failed = dbf($failedQuery)) {
                // build CSV
                $data['data']['filename'] = str_replace('.csv','-FIXED-'.date('d-m-Y-G-i-s-').'.csv',$failed['filename']);

                unset($failed['row'],$failed['filename']);
                $data['data']['filecontent'] .= implode(',',$failed).PHP_EOL;
            }

        }

        return $data;
    }

    public function removefailed()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }


        if (!isset($this->request['removerows']) || empty($this->request['removerows'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        foreach($this->request['removerows'] as $row => $remove){
            if(boolval($remove)){
                // get rid.
                $fecthRowQuery = dbq('SELECT * from slick_failed_payments where row = :ROW',
                    [
                        ':ROW' => $row
                    ]
                );

                if(dbnr($fecthRowQuery) > 0){
                    $fecthRow = dbf($fecthRowQuery);

                    // remove bank accounts?
                    $schoolURN = str_replace('FFUES ','',$fecthRow['reference']);

                    // get the master URN.
                    $masterOrgID = getOrgIDFromURN(getMasterURN($schoolURN));

                    // Get all bank details for all houses.
                    $allOrgIDQuery = dbq("select organization_id from organizations where organization_id = :ORGID OR organization_parent_id = :ORGID",[
                        ':ORGID' => $masterOrgID
                    ]);

                    while($allOrgID = dbf($allOrgIDQuery)){

                        dbq('DELETE from organization_bank_accounts where organization_id = :ORGID',
                            [
                                ':ORGID' => $allOrgID['organization_id']
                            ]
                        );

                    }

                    // Add a audit log entry and a new note.
                    audit(23,$masterOrgID,'Failed payment - Removed bank details');
                    addTicket(6,$masterOrgID,'Failed payment - Removed bank details','Barclays rejected bank account details.  Please get new bank account details.',1,date('Y-m-d',strtotime('next monday')).' 09:00:00',null,3); // Add a group ticket for C/S

                    dbq('DELETE from slick_failed_payments where row = :ROW',
                        [
                            ':ROW' => $row
                        ]
                    );

                }
            }
        }

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Lines and bank account details removed.'
        ];


        return $data;
    }

    public function applyreturns()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $fecthRowQuery = dbq('SELECT * from slick_returned_payments');


            while($fecthRow = dbf($fecthRowQuery)){
                // remove bank accounts?
                $schoolURN = $fecthRow['memo'];

                // get the master URN.
                $masterOrgID = getOrgIDFromURN(getMasterURN($schoolURN));

                // Get all bank details for all houses.
                $allOrgIDQuery = dbq("select organization_id from organizations where organization_id = :ORGID OR organization_parent_id = :ORGID",[
                    ':ORGID' => $masterOrgID
                ]);

                while($allOrgID = dbf($allOrgIDQuery)){

                    dbq('DELETE from organization_bank_accounts where organization_id = :ORGID',
                        [
                            ':ORGID' => $allOrgID['organization_id']
                        ]
                    );

                }

                // Add a donation payment back.

                // need to get latest balance.
                $urnBalanceQuery = dbq("select * from donations where organization_urn = :ORGURN order by donation_id desc limit 1 ",
                    [
                        ':ORGURN' => $masterOrgID
                    ]
                );

                $currentBalance = 0;

                if(dbnr($urnBalanceQuery) > 0){
                    $urnBalance = dbf($urnBalanceQuery);
                    $currentBalance = $urnBalance['donation_new_balance'];
                }

                $currentBalance = $currentBalance + $fecthRow['amount'];

                // insert a 'tesco_donation'
                dbq("insert into donations (
								  	organization_id,
								  	organization_urn,
								  	donation_name,
								  	donation_accounted_for,
								  	donation_amount,
								  	donation_new_balance,
								  	donation_category,
								  	donation_paid,
								  	donation_created_at,
								  	donation_display_date,
								  	donation_description
								  ) VALUES (
								  	:ORGANIZATION_ID,
								  	:ORGANIZATION_URN,
								  	:DONATION_NAME,
								  	:DONATION_ACCOUNTED_FOR,
								  	:DONATION_AMOUNT,
								  	:DONATION_NEW_BALANCE,
								  	:DONATION_CATEGORY,
								  	:DONATION_PAID,
								  	NOW(),
								  	:DONATION_DISPLAY_DATE,
								  	:DONATION_DESCRIPTION
								  )",
                    [
                        ':ORGANIZATION_ID' => $masterOrgID,
                        ':ORGANIZATION_URN' => $schoolURN,
                        ':DONATION_NAME' => 'bank_refund',
                        ':DONATION_ACCOUNTED_FOR' => $fecthRow['date'],
                        ':DONATION_AMOUNT' => $fecthRow['amount'],
                        ':DONATION_NEW_BALANCE' => $currentBalance,
                        ':DONATION_CATEGORY' => 9,
                        ':DONATION_PAID' => 0,
                        ':DONATION_DISPLAY_DATE' => $fecthRow['date'],
                        ':DONATION_DESCRIPTION' => 'Customers bank refunded'
                    ]
                );

                // update organization balance.
                dbq("update organizations set organization_funds_balance = :BALANCE where organization_id = :ORGID",
                    [
                        ':BALANCE' => $currentBalance,
                        ':ORGID' => $masterOrgID
                    ]
                );

                // Add a audit log entry and a new note.
                audit(23,$masterOrgID,'Returned payment - Removed bank details');
                addTicket(6,$masterOrgID,'Returned payment - Removed bank details','Customers bank returned payment.  Please get new bank account details.',1,date('Y-m-d',strtotime('next monday')).' 09:00:00',null,3); // Add a group ticket for C/S

                dbq('DELETE from slick_returned_payments where row = :ROW',
                    [
                        ':ROW' => $fecthRow['row']
                    ]
                );
            }






        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Returns applied and bank account details removed.'
        ];


        return $data;
    }


    public function getpaymentswaiting()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        $resultsPerPage = 25;

        // work out limit.
        $startAt = ($this->request['p'] * $resultsPerPage) - ($resultsPerPage);

        // Admin has extra info.
        if (isset($_SESSION['session']) && $_SESSION['session']['user']['user_type_id'] == 10) {
            // select it.
            $salesQuery = dbq("SELECT * FROM slick_payments sp
							where sp.amount > 0
							limit $startAt, $resultsPerPage");

            $data = ['data' => []];
            while ($sales = dbf($salesQuery)) {
                $data['data'][] = $sales;
            }

            $rows = dbf(dbq("SELECT count(*) as totalRows FROM slick_payments sp
							where sp.amount > 0")); // read how many total rows there are without LIMIT.
            $data['totalrows'] = intval($rows['totalRows']);
            $data['resultrows'] = sizeof($data['data']); // incase there are less than 30.

        }
        return $data;
    }

    public function uploadfailedpayments(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['file']) || empty($this->request['file'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $fileData = array_map('str_getcsv', preg_split("/(\r\n|\n|\r)/",base64_decode(@end(explode(',',$this->request['file']['data'])))));
        $fileName = $this->request['file']['name'];

        // Insert each one to slick_patments.

        $h = [
            'sort_code',
            'name',
            'account_no',
            'amount',
            'reference',
            '99',
        ];

        dbq('delete from slick_failed_payments where 1');

        $counter = 1;
        foreach($fileData as $rawline){

            if(sizeof($h)==sizeof($rawline)) {
                $csvline = array_combine($h,$rawline);

                dbq('INSERT INTO slick_failed_payments (
                    row,
                    filename,
                    sort_code,
                    `name`,
                    account_no,
                    amount,
                    reference
                  ) VALUES (
                    :ROW,
                    :FILENAME,
                    :SORT_CODE,
                    :NAME,
                    :ACCOUNT_NP,
                    :AMOUNT,
                    :REFERENCE
                  )',
                    [
                        ':ROW' => $counter,
                        ':FILENAME' => $fileName,
                        ':SORT_CODE' => $csvline['sort_code'],
                        ':NAME' => $csvline['name'],
                        ':ACCOUNT_NP' => $csvline['account_no'],
                        ':AMOUNT' => $csvline['amount'],
                        ':REFERENCE' => $csvline['reference']
                    ]
                );
                $counter++;
            }

        }
        $counter++;
        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Failed lines uploaded.'
        ];

        return $data;
    }

    public function uploadsales(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['file']) || empty($this->request['file'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        include(DIR_CLASSES . 'PHPExcel.php');
        include(DIR_CLASSES . 'PHPExcel/IOFactory.php');

        $donations = [];

        $houseIDs = [
            '901',
            '902',
            '903',
            '904',
            '905',
            '906',
            '907',
            '908',
            '909',
            '910',
            '911',
            '912',
            '913',
            '914',
            '915',
            '916',
            '917',
            '918',
            '919',
        ];

        // Remove anything which isn't a word, whitespace, number
        // or any of the following caracters -_~,;[]().
        $fileName = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $this->request['file']['name']);
        // Remove any runs of periods
        $fileName = mb_ereg_replace("([\.]{2,})", '', $fileName);
        $fileName = 'import/atg/'.$fileName;

        // need to temp save file.
        file_put_contents($fileName,base64_decode(@end(explode(',',$this->request['file']['data']))));

        $inputFileType = PHPExcel_IOFactory::identify($fileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($fileName);

        $starttime = microtime(true);
        // convert dates to correct format.
        for ($i = 2; $i < $objPHPExcel->getActiveSheet()->getHighestRow(); $i++) {

            $masterURN = getMasterURN($objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue());

            $houseident = substr($masterURN,0,3);

            if(in_array($houseident,$houseIDs)){
                // probably a house not on the system, just knock off the first 3 chars.
                $masterURN = substr($masterURN,3);
            }

            if(is_numeric($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue())){
                $date = date('Y-m-d',PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue()));
            } else {
                $date = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue();
            }

            dbq("insert into organization_sales (
							school_urn,
							dispatched_date,
							order_number,
							style_ref,
							EAN,
							quantity,
							price_paid,
							donation_amount
						  ) VALUES (
							:SCHOOL_URN,
							:DISPATCHED_DATE,
							:ORDER_NUMBER,
							:STYLE_REF,
							:EAN,
							:QUANTITY,
							:PRICE_PAID,
							:DONATION_AMOUNT
						  )",
                [
                    ':SCHOOL_URN' => $masterURN,
                    ':DISPATCHED_DATE' => $date,
                    ':ORDER_NUMBER' => $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue(),
                    ':STYLE_REF' => $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue(),
                    ':EAN' => $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue(),
                    ':QUANTITY' => $objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue(),
                    ':PRICE_PAID' => $objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue(),
                    ':DONATION_AMOUNT' => $objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue()
                ]
            );

        }

        $timeTaken = microtime(true) - $starttime;
        $processedFileName = str_replace('import/atg/','import/atg - Done/',$fileName);
        rename($fileName,$processedFileName);

        //update the stats.
        $donationQuery = dbq('SELECT SUM(donation_amount) as donations_amount FROM donations WHERE donation_category IN (5)');
        $donations = dbf($donationQuery);

        // total live schools not including houses.
        $liveQuery = dbq('SELECT count(*) as live_schools FROM organizations WHERE organization_parent_organization_id is NULL and organization_status_id = 3');
        $live = dbf($liveQuery);

        dbq("update statistics set statistic_value = :DONATIONS where statistic_key = 'organization_funds_raised'",
            [
                ':DONATIONS' => round($donations['donations_amount'],2)
            ]
        );

        dbq("update statistics set statistic_value = :LIVES where statistic_key = 'organizations_registered'",
            [
                ':LIVES' => $live['live_schools']
            ]
        );

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Returned lines uploaded.',
            'time' => $timeTaken
        ];

        return $data;
    }

    public function uploadreturnedpayments(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['file']) || empty($this->request['file'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $fileData = array_map('str_getcsv', preg_split("/(\r\n|\n|\r)/",base64_decode(@end(explode(',',$this->request['file']['data'])))));
        $fileName = $this->request['file']['name'];

        // Insert each one to slick_patments.

        $h = []; // file has header

        dbq('DELETE FROM slick_returned_payments WHERE 1');

        foreach($fileData as $rawline){

            if(empty($h)){
                $h = $rawline;
                continue;
            }

            if(sizeof($h)==sizeof($rawline)) {
                $csvline = array_combine($h,$rawline);

                list($day,$month,$year) = explode('/',$csvline['Date']);

               preg_match("/FFUES (.*?)\s/", $csvline['Memo'], $urn);

                if($csvline['Subcategory'] == 'DIRECTDEP'){
                    dbq('INSERT INTO slick_returned_payments (
                        filename,
                        number,
                        `date`,
                        account,
                        amount,
                        memo
                      ) VALUES (
                        :FILENAME,
                        :NUMBER,
                        :DATE,
                        :ACCOUNT,
                        :AMOUNT,
                        :MEMO
                      )',
                        [
                            ':FILENAME' => $fileName,
                            ':NUMBER' => $csvline['Number'],
                            ':DATE' => $year.'-'.$month.'-'.$day,
                            ':ACCOUNT' => $csvline['Account'],
                            ':AMOUNT' => $csvline['Amount'],
                            ':MEMO' => end($urn)
                        ]
                    );
                }

            }

        }

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Returned lines uploaded.'
        ];

        return $data;
    }

    public function clearremainingsales(){

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        // Admin has extra info.
        if (isset($_SESSION['session']) && $_SESSION['session']['user']['user_type_id'] == 10) {
            dbq('update organization_sales set accounted = 9 where accounted = 0');
        }

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Sales updated.'
        ];

        return $data;
    }

    public function getuseremails()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        $resultsPerPage = 25;

        // work out limit.
        $startAt = ($this->request['p'] * $resultsPerPage) - ($resultsPerPage);
        $data = [];
        // Admin has extra info.
        if (isset($_SESSION['session']) && $_SESSION['session']['user']['user_type_id'] == 10) {
            // select it.
            $emailQuery = dbq("SELECT u.user_id, o.organization_name, d.donation_amount, u.user_email FROM users u
							  JOIN users_to_organizations u2o USING (user_id)
							  JOIN organizations o USING (organization_id)
							  JOIN donations d ON (d.`organization_id` = o.`organization_id` AND d.`donation_category` = 2 AND d.`donation_accounted_for` = :PAYMENTDATE)
							  WHERE u.user_emailed = 0
                                AND u.user_type_id = 1
                                AND u2o.user_primary = 1
                                AND o.organization_parent_organization_id IS NULL
							  LIMIT $startAt, $resultsPerPage",
                [
                    ':PAYMENTDATE' => EMAIL_PAYMENT_DATE
                ]);

            $data = ['data' => []];
            while ($emails = dbf($emailQuery)) {
                $data['data'][] = $emails;
            }

            $rows = dbf(dbq("SELECT count(*) as totalRows FROM users u
							  JOIN users_to_organizations u2o USING (user_id)
							  JOIN organizations o USING (organization_id)
							  JOIN donations d ON (d.`organization_id` = o.`organization_id` AND d.`donation_category` = 2 AND d.`donation_accounted_for` = :PAYMENTDATE)
							  WHERE u.user_emailed = 0
                                AND u.user_type_id = 1
                                AND u2o.user_primary = 1
                                AND o.organization_parent_organization_id IS NULL",
                [
                    ':PAYMENTDATE' => EMAIL_PAYMENT_DATE
                ]
            )); // read how many total rows there are without LIMIT.
            $data['lastPeriodStart'] = EMAIL_PERIOD_START;
            $data['lastPeriodEnd'] = EMAIL_PERIOD_END;
            $data['totalrows'] = intval($rows['totalRows']);
            $data['totalrows'] = intval($rows['totalRows']);
            $data['resultrows'] = sizeof($data['data']); // incase there are less than 30.

        }

        return $data;
    }

    public function getsales()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        $resultsPerPage = 25;

        // work out limit.
        $startAt = ($this->request['p'] * $resultsPerPage) - ($resultsPerPage);

        // Admin has extra info.
        if (isset($_SESSION['session']) && $_SESSION['session']['user']['user_type_id'] == 10) {
            // select it.
            $salesQuery = dbq("SELECT * FROM organization_sales os
							where os.accounted = 0
							limit $startAt, $resultsPerPage");

            $data = ['data' => []];
            while ($sales = dbf($salesQuery)) {
                $data['data'][] = $sales;
            }

            $rows = dbf(dbq("SELECT count(*) as totalRows FROM organization_sales os
							where os.accounted = 0")); // read how many total rows there are without LIMIT.
            $data['totalrows'] = intval($rows['totalRows']);
            $data['resultrows'] = sizeof($data['data']); // incase there are less than 30.

        }

        return $data;
    }

    public function sendemailbg(){

        if(EMAILS_PROCESSING){
            $this->setError([
                'message' => 'Emails are already Sending.',
                'errorCode' => 'process-running',
            ]);
            return $this->getResponse(500);
        }

        dbq("UPDATE configuration set config_value = 1 where config_key = 'EMAILS_PROCESSING'");

        session_destroy(); // stop the session so we can keep doing other things...
        $test = false;
// select all the emails from the exit table.
        ini_set('max_execution_time', 70000);
        // get email addresses

        $paymentDateWords = date("jS F Y",strtotime(EMAIL_PAYMENT_DATE));

        $html = "Dear %s,<br />
<br />
We are pleased to confirm that we made a donation of &pound;%01.2f to your school on ".$paymentDateWords.".<br />
<br />
This donation represents 5 percent of the value of F&F Uniform Embroidery Service purchases made between ".EMAIL_PERIOD_START." and ".EMAIL_PERIOD_END." and any outstanding balance that may have been owed.<br />
<br />
To check the summary of your donations to date, simply visit www.ff-ues.com and log into your account. Select 'Services' from the uploadredown in the toolbar at the top of your screen, and then click 'Donations'.<br />
<br />
Please remember to keep your bank details up to date to ensure that future donations are processed smoothly. To update your bank details, log into your account and go to 'Donations' as before. You'll find a 'Bank Details' tab where you can update your account name, number and sort code.<br />
<br />
<br />
Yours sincerely,<br />
<br />
The F&F Uniforms Team";

        $subject = 'Your F&F Uniform Embroidery Donation Payment';
        $title = $subject;
        $template = 'tesco-ff-uniforms';
        $tag = ['FF-payment'];

        // Attachment.
        /*
        $attachmentArray[] = [
            'type' => "application/pdf",
            'name' => basename('2016 UES Pricelist v2.pdf'),
            'content' => base64_encode(file_get_contents('2016 UES Pricelist v2.pdf'))
        ];
        */

        $attachmentArray = null;

        if (!$test) {

            // Paid donation
            $emailQuery = dbq("SELECT u.user_id, o.organization_name, d.donation_amount, u.user_email FROM users u
							  JOIN users_to_organizations u2o USING (user_id)
							  JOIN organizations o USING (organization_id)
							  JOIN donations d ON (d.`organization_id` = o.`organization_id` AND d.`donation_category` = 2 AND d.`donation_accounted_for` = :PAYMENTDATE)
							  WHERE u.user_emailed = 0
							  AND u.user_type_id = 1
							  AND u2o.user_primary = 1
							  AND o.organization_parent_organization_id IS NULL",
                [
                    ':PAYMENTDATE' => EMAIL_PAYMENT_DATE
                ]
            );

            $emailAddresses = [];

            while ($email = dbf($emailQuery)) {

                $message = sprintf($html,$email['organization_name'],$email['donation_amount']);

                $template_content = array(
                    [
                        'name' => 'emailtitle',
                        'content' => $title
                    ],
                    [
                        'name' => 'main',
                        'content' => $message
                    ]
                );

                $emailAddress = [
                    $email['organization_name'] => $email['user_email']
                    //,'Andrew Lindsay' => 'andy@slickstitch.com'
                ];

                if(send_ues_mail($emailAddress, $subject, $template_content, $template, $tag,$attachmentArray)){

                    dbq("UPDATE users SET user_emailed = 1 WHERE user_id = :ID", [
                        ':ID' => $email['user_id']
                    ]);

                }

            }

        } else {

            $emailAddresses = [
                'Andrew Lindsay' => [
                    'email' => 'andy@slickstitch.com',
                    'urn' => '123456',
                    'amount' => 123.4,
                ]
            ];

            foreach($emailAddresses as $orgName => $email){

                $message = sprintf($html,$orgName,$email['amount']);

                $template_content = array(
                    [
                        'name' => 'emailtitle',
                        'content' => $title
                    ],
                    [
                        'name' => 'main',
                        'content' => $message
                    ]
                );

                $emailAddress = [
                    $orgName => $email['email']
                    //,'Andrew Lindsay' => 'andy@slickstitch.com'
                ];

                if(send_ues_mail($emailAddress, $subject, $template_content, $template, $tag,$attachmentArray)){

                   // echo "Sent to ".$email['email'];

                }

            }


        }

        dbq("UPDATE configuration set config_value = 0 where config_key = 'EMAILS_PROCESSING'");

        return ['done'];
    }

    function resetuseremail(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }


        if (!isset($this->request['paymentDate']) || empty($this->request['paymentDate'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $paymentDate = date("Y-m-d",strtotime('+4 hours',strtotime($this->request['paymentDate']))); // adding 4 hours to allow for DST

        dbq('UPDATE users set user_emailed = 0');
        dbq("UPDATE configuration set config_value = :VALUE where config_key = 'EMAIL_PAYMENT_DATE'",
            [
                ':VALUE'=>$paymentDate
            ]
        );

        return true;
    }


    public function processemails()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(EMAILS_PROCESSING){

            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Emails are already sending'
            ];

            return $data;
        }

        if (!isset($this->request['periodFromDate']) || empty($this->request['periodFromDate']) || !isset($this->request['periodToDate']) || empty($this->request['periodToDate'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // update from and to.
        $fromDate = date("jS F Y",strtotime('+4 hours',strtotime($this->request['periodFromDate']))); // adding 4 hours to allow for DST
        $toDate = date("jS F Y",strtotime('+4 hours',strtotime($this->request['periodToDate']))); // adding 4 hours to allow for DST

        dbq("UPDATE configuration set config_value = :VALUE where config_key = 'EMAIL_PERIOD_START'",
            [
                ':VALUE'=>$fromDate
            ]
        );

        dbq("UPDATE configuration set config_value = :VALUE where config_key = 'EMAIL_PERIOD_END'",
            [
                ':VALUE'=>$toDate
            ]
        );

        // start the process in the background.
        exec('bash -c "exec nohup setsid wget -O /dev/null '.API_ROOT.'donations/sendemailbg > /dev/null 2>&1 &"'); // Linux

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Emails sending has started.'
        ];

        return $data;
    }

}
