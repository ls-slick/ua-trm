<?php

/**
 * Rest object example
 * 
 * GNU General Public License (Version 2, June 1991) 
 * 
 * This program is free software; you can redistribute 
 * it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, 
 * or (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class reports extends restObject {

	/**
	 * @param string $method
	 * @param array $request
	 * @param string $file
	 */

	public function __construct($method, $request = null, $file = null) {
		if (!isset($_SESSION['session']['user']['user_type_id']) || $_SESSION['session']['user']['user_type_id'] == 1) {
			// Admin Only so return
			//$this->setError([
			//	'message' => 'not authorized.',
		//		'errorCode' => 'authorization-required',
	//		]);
	//		return $this->getResponse(500);
		}
		ini_set('memory_limit',-1); // No Limit.  Careful.
		parent::__construct($method, $request, $file);
	}


	public function embroideredallocationgmo() {

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];


		// select them.
		// New School	School ID	Default Image	Local Authority	School Name	Address Line 2	Address Line 3	Town/City	State/County	Post Code

		// select all styles first/
		$styleHeaders = [
			'new' => 'New School',
			'school_URN' => 'School ID',
			'organization_id' => 'Organization ID',
			'default_logo' => 'Default Image',
			'la' => 'Local Authority',
			'schoolname' => 'School Name',
			'add2' => 'Address Line 2',
			'add3' => 'Address Line 3',
			'town' => 'Town/City',
			'state' => 'State/County',
			'postcode' => 'Post Code'
		];

		$styleQuery = dbq("select * from product_colours pc
								join products p using (product_id)
								join colours c using (colour_id)
								where p.product_decorated = 1
								order by product_colour_style_ref");
		while($style = dbf($styleQuery)){
			if(!isset($styleHeaders[$style['product_colour_style_ref']])){
				$styleHeaders[$style['product_colour_style_ref']] = $style['product_colour_gmo_ref'].':'.$style['product_name'].'/'.$style['colour_name'];
			}
		}

		// not yet..

		$data['data']['contents'] = '"'.implode('","',$styleHeaders) . '"' . PHP_EOL;;

		// now get each schools allocations.
		$organizationQuery = dbq("SELECT
									  o.school_URN,
									  o.organization_id,
									  odl.tesco_style_ref as default_logo,
									  la.local_authority_name,
									  o.organization_name,
									  a.entry_street_address_2,
									  a.entry_suburb,
									  a.entry_city,
									  a.entry_state,
									  a.entry_postcode,
									  oh.organization_name as organization_house_parent_name,
									  pc.product_colour_style_ref,
									  ol.tesco_style_ref
									  FROM organizations o
									  LEFT JOIN organizations oh on (oh.organization_id = o.organization_parent_organization_id)
									  LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
									  LEFT JOIN addresses a on (o.organization_default_address_id = a.address_id)
									  LEFT JOIN organization_logos odl on (o.organization_default_logo_id = odl.organization_logo_id)
									  LEFT JOIN products_colours_to_organizations p2o on (p2o.organization_id = o.organization_id)
									  LEFT JOIN product_colours pc using (product_colour_id)
									  LEFT JOIN organization_logos ol on (ol.organization_logo_id = p2o.organization_logo_id)
									  WHERE o.organization_status_id = 3
									  AND o.organization_has_subunits = 0
									  AND o.school_URN != 'tescotestschool'
									  ORDER BY o.school_URN");

		// No orgs with sub units,
		// Not testschool
		// only live

		$csv = [];

		while($organization = dbf($organizationQuery )){
			if(!isset($csv[$organization['school_URN']])){
				// first one.
				if(!empty($organization['organization_house_parent_name'])){
					//if(!stristr($organization['organization_name'],$organization['organization_house_parent_name'])){
					// name not in house name.
					//$organization['organization_name'] = trim(str_ireplace($organization['organization_house_parent_name'],'',$organization['organization_name']));
					$organization['organization_name'] = $organization['organization_house_parent_name'] . ' ('.$organization['organization_name'].')';
					//}
				}

				$csv[$organization['school_URN']] = [
					'new' => 0,
					'school_URN' => $organization['school_URN'],
					'organization_id' => $organization['organization_id'],
					'default_logo' => $organization['default_logo'],
					'la' => $organization['local_authority_name'],
					'schoolname' => $organization['organization_name'],
					'add2' => $organization['entry_street_address_2'],
					'add3' => $organization['entry_suburb'],
					'town' => $organization['entry_city'],
					'state' => $organization['entry_state'],
					'postcode' => $organization['entry_postcode']

				];
			}

			$csv[$organization['school_URN']][$organization['product_colour_style_ref']] = @end(explode('-',$organization['tesco_style_ref']));
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';

			foreach($styleHeaders as $key => $header){
				if(isset($csvline[$key])){
					$writeline .= '"'.str_replace("#",'',(trim($csvline[$key]))).'"';
				}
				$writeline .= ',';
			}
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'EmbroideredAllocationsGMO'.date('Y-m-d-G-i-s').'.csv';

		// save it to a file..
		file_put_contents('allocations/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'allocations/'.$data['data']['filename'] ;

		return $data;
	}

	public function embroideredallocation() {

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];


		// select them.
		// New School	School ID	Default Image	Local Authority	School Name	Address Line 2	Address Line 3	Town/City	State/County	Post Code

		// select all styles first/
		$styleHeaders = [
			'new' => 'New School',
			'school_URN' => 'School ID',
			'default_logo' => 'Default Image',
			'la' => 'Local Authority',
			'schoolname' => 'School Name',
			'add2' => 'Address Line 2',
			'add3' => 'Address Line 3',
			'town' => 'Town/City',
			'state' => 'State/County',
			'postcode' => 'Post Code'
		];

		$oldHeaderQuery = dbq("select * from export_products
								where decorated = 1
								order by export_product_id");
		while($oldHeader= dbf($oldHeaderQuery)){
			$styleHeaders[$oldHeader['tesco_style_code']] =  $oldHeader['tesco_style_code'].' '.$oldHeader['tesco_style_name'];
		}

		$styleQuery = dbq("select * from product_colours pc
								join products p using (product_id)
								join colours c using (colour_id)
								where p.product_decorated = 1
								order by product_colour_style_ref");
		while($style = dbf($styleQuery)){
			if(!isset($styleHeaders[$style['product_colour_style_ref']])){
				$styleHeaders[$style['product_colour_style_ref']] = $style['product_colour_style_ref'].' '.$style['product_name'].'/'.$style['colour_name'];
			}
		}

		// not yet..
		
		$data['data']['contents'] = '"'.implode('","',$styleHeaders) . '"' . PHP_EOL;;

		// now get each schools allocations.
		$organizationQuery = dbq("SELECT
									  o.school_URN,
									  odl.tesco_style_ref as default_logo,
									  la.local_authority_name,
									  o.organization_name,
									  a.entry_street_address_2,
									  a.entry_suburb,
									  a.entry_city,
									  a.entry_state,
									  a.entry_postcode,
									  oh.organization_name as organization_house_parent_name,
									  pc.product_colour_style_ref,
									  ol.tesco_style_ref
									  FROM organizations o
									  LEFT JOIN organizations oh on (oh.organization_id = o.organization_parent_organization_id)
									  LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
									  LEFT JOIN addresses a on (o.organization_default_address_id = a.address_id)
									  LEFT JOIN organization_logos odl on (o.organization_default_logo_id = odl.organization_logo_id)
									  LEFT JOIN products_colours_to_organizations p2o on (p2o.organization_id = o.organization_id)
									  LEFT JOIN product_colours pc using (product_colour_id)
									  LEFT JOIN organization_logos ol on (ol.organization_logo_id = p2o.organization_logo_id)
									  WHERE o.organization_status_id = 3
									  AND o.organization_has_subunits = 0
									  AND o.school_URN != 'tescotestschool'
									  ORDER BY o.school_URN");

		// No orgs with sub units,
		// Not testschool
		// only live

		$csv = [];

		while($organization = dbf($organizationQuery )){
			if(!isset($csv[$organization['school_URN']])){
				// first one.
				if(!empty($organization['organization_house_parent_name'])){
					//if(!stristr($organization['organization_name'],$organization['organization_house_parent_name'])){
						// name not in house name.
						//$organization['organization_name'] = trim(str_ireplace($organization['organization_house_parent_name'],'',$organization['organization_name']));
						$organization['organization_name'] = $organization['organization_house_parent_name'] . ' ('.$organization['organization_name'].')';
					//}
				}

				$csv[$organization['school_URN']] = [
					'new' => 0,
					'school_URN' => $organization['school_URN'],
					'default_logo' => $organization['default_logo'],
					'la' => $organization['local_authority_name'],
					'schoolname' => $organization['organization_name'],
					'add2' => $organization['entry_street_address_2'],
					'add3' => $organization['entry_suburb'],
					'town' => $organization['entry_city'],
					'state' => $organization['entry_state'],
					'postcode' => $organization['entry_postcode']

				];
			}

			$csv[$organization['school_URN']][$organization['product_colour_style_ref']] = @end(explode('-',$organization['tesco_style_ref']));
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';

			foreach($styleHeaders as $key => $header){
				if(isset($csvline[$key])){
					$writeline .= '"'.str_replace("#",'',(trim($csvline[$key]))).'"';
				}
				$writeline .= ',';
			}
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'EmbroideredAllocations'.date('Y-m-d-G-i-s').'.csv';

		// save it to a file..
		file_put_contents('allocations/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'allocations/'.$data['data']['filename'] ;

		return $data;
	}

	public function plainallocationgmo() {

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		// select them.
		// New School	School ID	Default Image	Local Authority	School Name	Address Line 2	Address Line 3	Town/City	State/County	Post Code

		// select all styles first/
		$styleHeaders = [
			'school_URN' => 'Account Name',
			'organization_id' => 'Organization ID',
			'schoolname' => 'Establishment Name'
		];

		$styleQuery = dbq("select * from product_colours pc
								join products p using (product_id)
								join colours c using (colour_id)
								where p.product_decorated = 0
								order by product_colour_style_ref");
		while($style = dbf($styleQuery)){
			if(!isset($styleHeaders[trim($style['product_colour_style_ref'])])){
				$styleHeaders[trim($style['product_colour_style_ref'])] = trim($style['product_colour_gmo_ref']).':'.trim($style['product_name']).'/'.trim($style['colour_name']);
			}
		}

		$data['data']['contents'] = '"'.implode('","',$styleHeaders) . '"' . PHP_EOL;;

		// now get each schools allocations.
		$organizationQuery = dbq("SELECT
									  o.school_URN,
									  o.organization_id,
									  o.organization_name,
									  oh.organization_name as organization_house_parent_name,
									  pc.product_colour_style_ref,
									  ol.tesco_style_ref
									  FROM organizations o
									  LEFT JOIN organizations oh on (oh.organization_id = o.organization_parent_organization_id)
									  LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
									  LEFT JOIN products_colours_to_organizations p2o on (p2o.organization_id = o.organization_id)
									  LEFT JOIN product_colours pc using (product_colour_id)
									  LEFT JOIN organization_logos ol on (ol.organization_logo_id = p2o.organization_logo_id)
									  WHERE o.organization_status_id = 3
									  AND o.organization_has_subunits = 0
									  AND o.school_URN != 'tescotestschool'
									  ORDER BY o.school_URN");

		// No orgs with sub units,
		// Not testschool
		// only live

		$csv = [];

		while($organization = dbf($organizationQuery )){

			if(!empty($organization['organization_house_parent_name'])){
				if(!stristr($organization['organization_name'],$organization['organization_house_parent_name'])){
					// name not in house name.
					$organization['organization_name'] = $organization['organization_house_parent_name'] . ' ('.$organization['organization_name'].')';
				}
			}

			if(!isset($csv[$organization['school_URN']])){
				// first one.
				$csv[$organization['school_URN']] = [
					'school_URN' => $organization['school_URN'],
					'organization_id' => $organization['organization_id'],
					'schoolname' => $organization['organization_name']
				];
			}

			$csv[$organization['school_URN']][$organization['product_colour_style_ref']] = 'Yes';
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($styleHeaders as $key => $header){
				if(isset($csvline[$key])){
					$writeline .= '"'.str_replace("#",'',(trim($csvline[$key]))).'"';
				}
				$writeline .= ',';
			}
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'PlainAllocationsGMO'.date('Y-m-d-G-i-s').'.csv';
// save it to a file..
		file_put_contents('allocations/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'allocations/'.$data['data']['filename'] ;
		return $data;
	}


	public function plainallocation() {

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		// select them.
		// New School	School ID	Default Image	Local Authority	School Name	Address Line 2	Address Line 3	Town/City	State/County	Post Code

		// select all styles first/
		$styleHeaders = [
			'school_URN' => 'Account Name',
			'schoolname' => 'Establishment Name'
		];

		$oldHeaderQuery = dbq("select * from export_products
								where decorated = 0
								order by export_product_id");

		while($oldHeader= dbf($oldHeaderQuery)){
			$styleHeaders[$oldHeader['tesco_style_code']] = $oldHeader['tesco_style_code'].' '.$oldHeader['tesco_style_name'];
		}


		$styleQuery = dbq("select * from product_colours pc
								join products p using (product_id)
								join colours c using (colour_id)
								where p.product_decorated = 0
								order by product_colour_style_ref");
		while($style = dbf($styleQuery)){
			if(!isset($styleHeaders[trim($style['product_colour_style_ref'])])){
				$styleHeaders[trim($style['product_colour_style_ref'])] = trim($style['product_colour_style_ref']).' '.trim($style['product_name']).'/'.trim($style['colour_name']);
			}
		}

		$data['data']['contents'] = '"'.implode('","',$styleHeaders) . '"' . PHP_EOL;;

		// now get each schools allocations.
		$organizationQuery = dbq("SELECT
									  o.school_URN,
									  o.organization_name,
									  oh.organization_name as organization_house_parent_name,
									  pc.product_colour_style_ref,
									  ol.tesco_style_ref
									  FROM organizations o
									  LEFT JOIN organizations oh on (oh.organization_id = o.organization_parent_organization_id)
									  LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
									  LEFT JOIN products_colours_to_organizations p2o on (p2o.organization_id = o.organization_id)
									  LEFT JOIN product_colours pc using (product_colour_id)
									  LEFT JOIN organization_logos ol on (ol.organization_logo_id = p2o.organization_logo_id)
									  WHERE o.organization_status_id = 3
									  AND o.organization_has_subunits = 0
									  AND o.school_URN != 'tescotestschool'
									  ORDER BY o.school_URN");

		// No orgs with sub units,
		// Not testschool
		// only live

		$csv = [];

		while($organization = dbf($organizationQuery )){

			if(!empty($organization['organization_house_parent_name'])){
				if(!stristr($organization['organization_name'],$organization['organization_house_parent_name'])){
					// name not in house name.
					$organization['organization_name'] = $organization['organization_house_parent_name'] . ' ('.$organization['organization_name'].')';
				}
			}

			if(!isset($csv[$organization['school_URN']])){
				// first one.
				$csv[$organization['school_URN']] = [
					'school_URN' => $organization['school_URN'],
					'schoolname' => $organization['organization_name']
				];
			}

			$csv[$organization['school_URN']][$organization['product_colour_style_ref']] = 'Yes';
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($styleHeaders as $key => $header){
				if(isset($csvline[$key])){
					$writeline .= '"'.str_replace("#",'',(trim($csvline[$key]))).'"';
				}
				$writeline .= ',';
			}
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'PlainAllocations'.date('Y-m-d-G-i-s').'.csv';
// save it to a file..
		file_put_contents('allocations/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'allocations/'.$data['data']['filename'] ;
		return $data;
	}

	public function createlusheet(){
		// create LU sheet for sapphire.

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];


		// select them.
		// New School	School ID	Default Image	Local Authority	School Name	Address Line 2	Address Line 3	Town/City	State/County	Post Code

		// headers.
		$styleHeaders = [
			'school_URN' => 'School ID',
			'schoolname' => 'School name',
			'product_colour_style_ref' => 'Tesco style ref',
			'product_colour_alt_style_ref' => 'Tesco alt style code',
			'product_category_name' => 'Product group',
			'name_colour' => 'Product name (colour)',
			'tesco_style_ref_logo' => 'Emblem code',
			'stitch_count' => 'Stitch Count',
			'size' => 'Size (mm)',
			'position' => 'Position',
		];

		for($i=1;$i<=30;$i++){
			$styleHeaders['thread_'.$i] = 'Thread '.$i;
		}

		$data['data']['contents'] = implode(',',$styleHeaders) . PHP_EOL;;

		// now get each schools allocations.
		$organizationQuery = dbq("SELECT o.school_URN,
										o.organization_name,
										pc.product_colour_style_ref,
										pc.product_colour_alt_style_ref,
										pca.product_category_name,
									    p.product_name,
									    c.colour_name,
									    ol.tesco_style_ref,
									    ol.stitch_count,
									    ol.size__mm_,
										p.product_emblem_position,
									  	ol.thread_array
										FROM organizations o
									  LEFT JOIN products_colours_to_organizations p2o on (p2o.organization_id = o.organization_id)
									  LEFT JOIN product_colours pc using (product_colour_id)
									  LEFT JOIN products p on (p.product_id = pc.product_id)
									  LEFT JOIN product_categories pca on pca.product_category_id = p.product_category_id
									  LEFT JOIN colours c on pc.colour_id = c.colour_id
									  LEFT JOIN organization_logos ol on (ol.organization_logo_id = p2o.organization_logo_id)
									  WHERE o.organization_status_id = 3
									  AND o.organization_has_subunits = 0
									  AND p.product_decorated = 1
									  AND o.school_URN != 'tescotestschool'
									  ORDER BY o.school_URN");

		// No orgs with sub units,
		// Not testschool
		// only live

		$csv = [];

		while($organization = dbf($organizationQuery )){

				// first one.
				$lineArray = [
					'school_URN' => $organization['school_URN'],
					'schoolname' => $organization['organization_name'],
					'product_colour_style_ref' =>  $organization['product_colour_style_ref'],
					'product_colour_alt_style_ref' =>  $organization['product_colour_alt_style_ref'],
					'product_category_name' =>  $organization['product_category_name'],
					'name_colour' =>  $organization['product_name'].' ('. $organization['colour_name'].')',
					'tesco_style_ref_logo' => $organization['tesco_style_ref'],
					'stitch_count' =>  $organization['stitch_count'],
					'size' =>  $organization['size__mm_'],
					'position' =>  $organization['product_emblem_position'],
				];

				$threads = explode(',',$organization['thread_array']);

				for($i=1;$i<=30;$i++){
					$keynumber = $i-1;
					if(isset($threads[$keynumber])){
						$lineArray['thread_'.$i] = $threads[$keynumber];
					} else {
						$lineArray['thread_'.$i] = '';
					}

				}

				$csv[] = $lineArray;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($styleHeaders as $key => $header){
				if(isset($csvline[$key])){
					$writeline .= '"'.str_replace("#",'',(trim($csvline[$key]))).'"';
				}
				$writeline .= ',';
			}
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}


		$data['data']['filename'] = 'SchoolOffice-Export-'.date('Y-m-d_G-i-s').'.csv';

		return $data;

	}

	public function consolodateddonations()
	{

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];

		// get the uploaded data.
		$organizationDonationQuery = dbq("SELECT * from donations_upload");

		$headers = [
			'school_id' => 'School ID',
			'school_name' => 'School Name',
			'included_urns' => 'Included URNs',
			'amount' => 'Donation Amount',
			'bank_name' => 'Bank Account Name',
			'bank_number' => 'Bank Account Number',
			'bank_sortcode_1' => 'Bank Account Sort Code 1',
			'bank_sortcode_2' => 'Bank Account Sort Code 2',
			'bank_sortcode_3' => 'Bank Account Sort Code 3',
		];

		$bankAccounts = [];

		while($organizationDonation = dbf($organizationDonationQuery)){

			$masterURN = getMasterURN($organizationDonation['donations_upload_urn']);

			if(!isset($csv[$masterURN])){
				// first one.
				$csv[$masterURN] = [
					'school_id' => $masterURN,
					'school_name' => getSchoolNameFromURN($masterURN),
					'included_urns' => '',
					'amount' => 0,
				];
			}

			$csv[$masterURN]['amount'] += $organizationDonation['donations_upload_amount'];
			$csv[$masterURN]['included_urns'] .= $organizationDonation['donations_upload_urn'].':';

			if(!isset($bankAccounts[$masterURN])){
				// get bank details.
				$masterOrgID = getOrgIDFromURN($masterURN);
				$bankQuery = dbq("select organization_bank_account, organization_bank_name, organization_bank_sort_code_1, organization_bank_sort_code_2, organization_bank_sort_code_3 from organization_bank_accounts where organization_id = :ORGID",
					[
						':ORGID' => $masterOrgID
					]
				);
				$bank = dbf($bankQuery);

				$bankAccounts[$masterURN] = $bank;

				if(!empty($bank)){
					$csv[$masterURN]['bank_name'] = $bank['organization_bank_name'];
					$csv[$masterURN]['bank_number'] = $bank['organization_bank_account'];
					$csv[$masterURN]['bank_sortcode_1'] = $bank['organization_bank_sort_code_1'];
					$csv[$masterURN]['bank_sortcode_2'] = $bank['organization_bank_sort_code_2'];
					$csv[$masterURN]['bank_sortcode_3'] = $bank['organization_bank_sort_code_3'];
				}
			}
		}

		$data['data']['contents'] = '';

		// add headers
		foreach($headers as $key => $header){
			$data['data']['contents'] .= '"'.$header.'",';
		}

		$data['data']['contents'] = substr($data['data']['contents'],0,-1) . PHP_EOL; // knock off last ,
		// make it into a CSV
		foreach($csv as $csvline){
			$csvline['included_urns'] = substr($csvline['included_urns'],0,-1); // knock off last :
			$writeline = '';
			foreach($headers as $key => $header){
				if(isset($csvline[$key])){
					if($key == 'amount'){
						$writeline .= ''.$csvline[$key].'';
					} else {
						$writeline .= '"=""'.str_replace("#",'',(trim($csvline[$key]))).'"""';
					}

					//$writeline .= '"'.str_replace("#",'',(trim($csvline[$key]))).'"';
				}
				$writeline .= ',';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$filename = 'ConsolodatedDonations'.date('Y-m-d-G-i-s').'.csv';

		file_put_contents('reports/'.$filename,$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$filename ;

		return $data;

	}

	public function donationpaymentlist(){

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$data['data']['contents'] = '';
		// get the  data.
		$organizationQuery = dbq("select
										CONCAT(oba.organization_bank_sort_code_1,'',oba.organization_bank_sort_code_2,'',oba.organization_bank_sort_code_3) as `Organization Bank Sort Code`,
										oba.organization_bank_name as `Organization Bank Name`,
										oba.organization_bank_account as `Organization Bank Account Number`,
										o.organization_funds_balance as `Organization Funds Balance`,
										CONCAT('FFUES ',o.school_URN) as `School URN`,
										99
									  from organizations o
                                      join organization_bank_accounts oba on (oba.organization_id = o.organization_id)
                                      where o.organization_funds_balance > 0.01 and o.organization_parent_organization_id is null");

		$seenOrgs = [];
		$headersDone = true;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}

			$organization['Organization Funds Balance'] = sprintf("%0.2f",$organization['Organization Funds Balance'] );

			if(!in_array($organization['School URN'],$seenOrgs)){
				$csv[] = $organization;
				$seenOrgs[] = $organization['School URN'];
			}
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= ''.str_replace(",",' ',str_replace("#",'',(trim($field)))).',';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'OrganizationDonations'.date('Y-m-d-G-i-s').'.csv';
		// save it to a file..
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;
	}

	public function donationpaymentlists(){

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$data['data']['contents'] = [''];
		$contentPointer = 0;

		// get the  data.
		$organizationQuery = dbq("select
										CONCAT(oba.organization_bank_sort_code_1,'',oba.organization_bank_sort_code_2,'',oba.organization_bank_sort_code_3) as `Organization Bank Sort Code`,
										oba.organization_bank_name as `Organization Bank Name`,
										oba.organization_bank_account as `Organization Bank Account Number`,
										o.organization_funds_balance as `Organization Funds Balance`,
										CONCAT('FFUES ',o.school_URN) as `School URN`,
										99
									  from organizations o
                                      join organization_bank_accounts oba on (oba.organization_id = o.organization_id)
                                      where o.organization_funds_balance > 0.01 and o.organization_parent_organization_id is null");

		$seenOrgs = [];
		$headersDone = true;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}

			$organization['Organization Funds Balance'] = sprintf("%0.2f",$organization['Organization Funds Balance'] );

			if(!in_array($organization['School URN'],$seenOrgs)){
				$csv[] = $organization;
				$seenOrgs[] = $organization['School URN'];
			}
		}

		// make it into a CSV
		foreach($csv as $csvline){
			if(substr_count($data['data']['contents'][$contentPointer],PHP_EOL) > 999) {
				$contentPointer++;
			}
			if(!isset($data['data']['contents'][$contentPointer])){
				$data['data']['contents'][$contentPointer] = '';
			}
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= ''.str_replace(",",' ',str_replace("#",'',(trim($field)))).',';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'][$contentPointer] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'OrganizationDonations'.date('Y-m-d-G-i-s');
		// save it to a file..

		$zip = new ZipArchive();
		$filename = 'reports/'.$data['data']['filename'].'.zip';

		if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
			exit("cannot open <$filename>\n");
		}

		$filePrefix = 'A';
		foreach($data['data']['contents'] as $onefile){
			$zip->addFromString($filePrefix.'-SPLIT-'.$data['data']['filename'].'.csv', $onefile);
			$filePrefix++;
		}

		$zip->addFromString('FULLFILE-'.$data['data']['filename'].'.csv', implode('', $data['data']['contents']));

		$zip->close();

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.$filename ;
		return $data;
	}

	public function allorganizations()
	{

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("select
											o.school_URN as `School URN`,
											o.organization_parent_id as `Parent URN`,
											o.organization_name as `Organization Name`,
											ep.education_phase_name as `Education Phase`,
											la.local_authority_name as `LEA Name`,
											u.user_name as `Contact Name`,
											o.organization_member_count as `Organization Members`,
											o.organization_telephone as `Organization Phone`,
											o.organization_email as `Organization Email`,
											o.organization_url as `Organization URL`,
											o.organization_funds as `Organization Total Funds`,
											o.organization_funds_balance as `Organization Funds Balance`,
											o.organization_can_bulk as `Organization Bulk Order`,
											o.organization_form_signed as `Organization Form Signed`,
											ss.school_status_name as `Organization Status`,
											ol.tesco_style_ref as `Organization Main Logo`,
											a.entry_company as `Organization Address 1`,
											a.entry_street_address as `Organization Address 2`,
											a.entry_street_address_2 as `Organization Address 3`,
											a.entry_city as `Organization City`,
											a.entry_suburb as `Organization Suburb`,
											a.entry_state as `Organization State`,
											a.entry_postcode as `Organization Postcode`,
											CONCAT(oba.organization_bank_sort_code_1,'-',oba.organization_bank_sort_code_2,'-',oba.organization_bank_sort_code_3) as `Organization Bank Sort Code`,
											oba.organization_bank_account as `Organization Bank Account Number`,
											oba.organization_bank_name as `Organization Bank Name`,
											oba.organization_bank_last_updated as `Organization Bank Updated`,
											IF(o.organization_has_subunits = 0,CONCAT('http://www.tesco.com/direct/',REPLACE(REPLACE(LOWER(o.organization_name),' ','-'),\"'\",''),'/',o.organization_id,'.school'),'') as `Organization Tesco Link`,
											o.organization_live_date as `Organization Live Date`,
											o.organization_withdrawn_date as `Organization Withdrawn Date`,
											o.organization_withdrawn_suspended_reason as `Organization Withdrawn Reason`,
											o.organization_registered_date as `Date Registered`
										  from organizations o
										  JOIN school_statuses ss on (o.organization_status_id = ss.school_status_id)
										  LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
										  left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id)
										  left join addresses a on (a.address_id = o.organization_default_address_id)
										  left join organization_bank_accounts oba on (oba.organization_id = o.organization_id)
										  left join users u on u.user_email = o.organization_email
										  left join education_phases ep on ep.education_phase_id = o.school_education_phase_id
										  GROUP BY o.organization_id
										  ");

		$headersDone = false;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}
			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace("#",'',(trim($field))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'AllOrganizations'.date('Y-m-d-G-i-s').'.csv';
		// save it to a file..
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}

	public function allsweatorganizations()
	{

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("select
											o.school_URN as `School URN`,
											o.organization_parent_id as `Parent URN`,
											o.organization_name as `Organization Name`,
											o.has_sweats as `Added Poly`,
											ep.education_phase_name as `Education Phase`,
											la.local_authority_name as `LEA Name`,
											u.user_name as `Contact Name`,
											o.organization_member_count as `Organization Members`,
											o.organization_telephone as `Organization Phone`,
											o.organization_email as `Organization Email`,
											o.organization_url as `Organization URL`,
											o.organization_funds as `Organization Total Funds`,
											o.organization_funds_balance as `Organization Funds Balance`,
											o.organization_can_bulk as `Organization Bulk Order`,
											o.organization_form_signed as `Organization Form Signed`,
											ss.school_status_name as `Organization Status`,
											ol.tesco_style_ref as `Organization Main Logo`,
											a.entry_company as `Organization Address 1`,
											a.entry_street_address as `Organization Address 2`,
											a.entry_street_address_2 as `Organization Address 3`,
											a.entry_city as `Organization City`,
											a.entry_suburb as `Organization Suburb`,
											a.entry_state as `Organization State`,
											a.entry_postcode as `Organization Postcode`,
											CONCAT(oba.organization_bank_sort_code_1,'-',oba.organization_bank_sort_code_2,'-',oba.organization_bank_sort_code_3) as `Organization Bank Sort Code`,
											oba.organization_bank_account as `Organization Bank Account Number`,
											oba.organization_bank_name as `Organization Bank Name`,
											oba.organization_bank_last_updated as `Organization Bank Updated`,
											IF(o.organization_has_subunits = 0,CONCAT('http://www.tesco.com/direct/',REPLACE(REPLACE(LOWER(o.organization_name),' ','-'),\"'\",''),'/',o.organization_id,'.school'),'') as `Organization Tesco Link`,
											o.organization_live_date as `Organization Live Date`,
											o.organization_withdrawn_date as `Organization Withdrawn Date`,
											o.organization_withdrawn_suspended_reason as `Organization Withdrawn Reason`,
											o.organization_registered_date as `Date Registered`
										  from organizations o
										  JOIN school_statuses ss on (o.organization_status_id = ss.school_status_id)
										  LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
										  left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id)
										  left join addresses a on (a.address_id = o.organization_default_address_id)
										  left join organization_bank_accounts oba on (oba.organization_id = o.organization_id)
										  left join users u on u.user_email = o.organization_email
										  left join education_phases ep on ep.education_phase_id = o.school_education_phase_id
										  GROUP BY o.organization_id
										  ");

		$headersDone = false;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}
			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace("#",'',(trim($field))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'AllSweatOrganizations'.date('Y-m-d-G-i-s').'.csv';
		// save it to a file..
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}


	public function welcomepackreport(){

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		if (!isset($this->request['fromdate']) || empty($this->request['fromdate']) || !isset($this->request['todate']) || empty($this->request['todate'])) {
			$this->setError([
				'message' => 'Cannot provide results, required arguments are missing.',
				'errorCode' => 'arguments-missing',
			]);
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$fromdate = date("Y-m-d",strtotime($this->request['fromdate']))." 00:00:01";
		$todate = date("Y-m-d",strtotime($this->request['todate']))." 23:59:59";

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("SELECT o.school_URN AS `School URN`,
											sso.Date AS `Sample Requested Date`,
											al.audit_timestamp AS `Sample Dispatched Date`,
											p.product_name AS `Product Name`,
											c.colour_name AS `Product Colour`,
											ps.products_size_name AS `Product Size`,
											sso.LogoID1 AS `Logo`,
											al.audit_event_info AS `Dispatch Detail`,
											SUBSTRING_INDEX(al.audit_event_info, ' ', -1) as `Tracking`
									  FROM organizations o
                                      JOIN audit_log al ON (al.audit_log_event_id = 36 AND al.`organization_id` = o.`organization_id`)
                                      JOIN ss_orders sso ON sso.orderId = SUBSTRING_INDEX(al.audit_event_info, ' ', 1)
                                      JOIN products_sizes_to_products_colours ps2pc ON ps2pc.product_variation_sku = sso.SKU
                                      JOIN product_colours pc ON pc.product_colour_id = ps2pc.product_colour_id
                                      JOIN products p ON p.product_id = pc.product_id
                                      JOIN colours c ON c.colour_id = pc.colour_id
                                      JOIN product_sizes ps ON ps.product_size_id = ps2pc.product_size_id
                                      where
                                      al.audit_timestamp >= :FROMDATE
                                      AND
                                      al.audit_timestamp <= :TODATE
                                      ORDER BY o.school_URN
                                      ",
			[
				':FROMDATE' => $fromdate,
				':TODATE' => $todate
			]
		);

		$headersDone = false;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}

			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace("#",'',(trim($field))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		//$data['data']['filename'] = 'ReferAFriendOrganizations'.date('Y-m-d-G-i-s').'.csv';
		$data['data']['filename'] = 'WelcomePacksSent'.date("Y-m-d",strtotime($this->request['fromdate'])).'-'.date("Y-m-d",strtotime($this->request['todate'])).'.csv';
		// save it to a file..
		if(empty($data['data']['contents'])){
			$data['data']['contents'] = 'NO DATA';
		}
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}

	public function samplessentreport(){

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		if (!isset($this->request['fromdate']) || empty($this->request['fromdate']) || !isset($this->request['todate']) || empty($this->request['todate'])) {
			$this->setError([
				'message' => 'Cannot provide results, required arguments are missing.',
				'errorCode' => 'arguments-missing',
			]);
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$fromdate = date("Y-m-d",strtotime($this->request['fromdate']))." 00:00:01";
		$todate = date("Y-m-d",strtotime($this->request['todate']))." 23:59:59";

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("SELECT o.school_URN AS `School URN`,
											sso.Date AS `Sample Requested Date`,
											al.audit_timestamp AS `Sample Dispatched Date`,
											p.product_name AS `Product Name`,
											c.colour_name AS `Product Colour`,
											ps.products_size_name AS `Product Size`,
											sso.LogoID1 AS `Logo`,
											al.audit_event_info AS `Dispatch Detail`,
											SUBSTRING_INDEX(al.audit_event_info, ' ', -1) as `Tracking`
									  FROM organizations o
                                      JOIN audit_log al ON (al.audit_log_event_id = 34 AND al.`organization_id` = o.`organization_id`)
                                      JOIN ss_orders sso ON sso.orderId = SUBSTRING_INDEX(al.audit_event_info, ' ', 1)
                                      JOIN products_sizes_to_products_colours ps2pc ON ps2pc.product_variation_sku = sso.SKU
                                      JOIN product_colours pc ON pc.product_colour_id = ps2pc.product_colour_id
                                      JOIN products p ON p.product_id = pc.product_id
                                      JOIN colours c ON c.colour_id = pc.colour_id
                                      JOIN product_sizes ps ON ps.product_size_id = ps2pc.product_size_id
                                      where
                                      al.audit_timestamp >= :FROMDATE
                                      AND
                                      al.audit_timestamp <= :TODATE
                                      ORDER BY o.school_URN",
			[
				':FROMDATE' => $fromdate,
				':TODATE' => $todate
			]
		);

		$headersDone = false;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}

			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace("#",'',(trim($field))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'SamplesSent'.date("Y-m-d",strtotime($this->request['fromdate'])).'-'.date("Y-m-d",strtotime($this->request['todate'])).'.csv';
		// save it to a file..
		if(empty($data['data']['contents'])){
			$data['data']['contents'] = 'NO DATA';
		}
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}

	public function liveorganizations()
	{

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("select
										o.school_URN as `School URN`,
										o.organization_parent_id as `Parent URN`,
										o.organization_name as `Organization Name`,
										ep.education_phase_name as `Education Phase`,
									    la.local_authority_name as `LEA Name`,
										u.user_name as `Contact Name`,
										o.organization_member_count as `Organization Members`,
										o.organization_telephone as `Organization Phone`,
										o.organization_email as `Organization Email`,
										o.organization_url as `Organization URL`,
										o.organization_funds as `Organization Total Funds`,
										o.organization_funds_balance as `Organization Funds Balance`,
										o.organization_can_bulk as `Organization Bulk Order`,
										o.organization_form_signed as `Organization Form Signed`,
										ss.school_status_name as `Organization Status`,
										ol.tesco_style_ref as `Organization Main Logo`,
										a.entry_company as `Organization Address 1`,
										a.entry_street_address as `Organization Address 2`,
										a.entry_street_address_2 as `Organization Address 3`,
										a.entry_city as `Organization City`,
										a.entry_suburb as `Organization Suburb`,
										a.entry_state as `Organization State`,
										a.entry_postcode as `Organization Postcode`,
										CONCAT(oba.organization_bank_sort_code_1,'-',oba.organization_bank_sort_code_2,'-',oba.organization_bank_sort_code_3) as `Organization Bank Sort Code`,
										oba.organization_bank_account as `Organization Bank Account Number`,
										oba.organization_bank_name as `Organization Bank Name`,
										oba.organization_bank_last_updated as `Organization Bank Updated`,
										IF(o.organization_has_subunits = 0,CONCAT('http://www.tesco.com/direct/',REPLACE(REPLACE(LOWER(o.organization_name),' ','-'),\"'\",''),'/',o.organization_id,'.school'),'') as `Organization Tesco Link`,
										o.organization_live_date as `Organization Live Date`,
										'0000-00-00' AS `Last Donation Date`
									  from organizations o
                                      JOIN school_statuses ss on (o.organization_status_id = ss.school_status_id)
                                      LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
                                      LEFT JOIN organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id)
                                      LEFT JOIN addresses a on (a.address_id = o.organization_default_address_id)
                                      LEFT JOIN organization_bank_accounts oba on (oba.organization_id = o.organization_id)
                                      LEFT JOIN users u on u.user_email = o.organization_email
                                      LEFT JOIN education_phases ep on ep.education_phase_id = o.school_education_phase_id
                                      where o.organization_status_id = 3
                                      GROUP BY o.school_URN");

		$headersDone = false;
		while($organization = dbf($organizationQuery )){

			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}

			// find last donation.
			$donationsQuery = dbq("select donation_display_date from donations where organization_id = :ORGID ORDER BY donation_display_date DESC LIMIT 1",
				[
					':ORGID' => getOrgIDFromURN($organization['School URN'])
				]
			);

			if(dbnr($donationsQuery) > 0){
				$donations = dbf($donationsQuery);
				$organization['Last Donation Date'] = $donations['donation_display_date'];
			} else {
				// Never had a donation so use the Live date.
				$organization['Last Donation Date'] = substr($organization['Organization Live Date'],0,10);
			}
			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace("#",'',(trim($field))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'LiveOrganizations'.date('Y-m-d-G-i-s').'.csv';
		// save it to a file..
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}

	public function withdrawnorganizations()
	{

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("select
										o.school_URN as `School URN`,
										o.organization_parent_id as `Parent URN`,
										o.organization_name as `Organization Name`,
										ep.education_phase_name as `Education Phase`,
										la.local_authority_name as `LEA Name`,
										u.user_name as `Contact Name`,
										o.organization_member_count as `Organization Members`,
										o.organization_telephone as `Organization Phone`,
										o.organization_email as `Organization Email`,
										o.organization_url as `Organization URL`,
										o.organization_funds as `Organization Total Funds`,
										o.organization_funds_balance as `Organization Funds Balance`,
										o.organization_can_bulk as `Organization Bulk Order`,
										o.organization_form_signed as `Organization Form Signed`,
										o.organization_withdrawn_date as `Organization Withdrawn Date`,
										o.organization_withdrawn_suspended_reason as `Organization Withdrawn Reason`,
										pss.school_status_name as `Organization Previous Status`,
										ss.school_status_name as `Organization Status`,
										ol.tesco_style_ref as `Organization Main Logo`,
										a.entry_company as `Organization Address 1`,
										a.entry_street_address as `Organization Address 2`,
										a.entry_street_address_2 as `Organization Address 3`,
										a.entry_city as `Organization City`,
										a.entry_suburb as `Organization Suburb`,
										a.entry_state as `Organization State`,
										a.entry_postcode as `Organization Postcode`,
										CONCAT(oba.organization_bank_sort_code_1,'-',oba.organization_bank_sort_code_2,'-',oba.organization_bank_sort_code_3) as `Organization Bank Sort Code`,
										oba.organization_bank_account as `Organization Bank Account Number`,
										oba.organization_bank_name as `Organization Bank Name`,
										oba.organization_bank_last_updated as `Organization Bank Updated`
									  from organizations o
                                      JOIN school_statuses ss on (o.organization_status_id = ss.school_status_id)
                                      LEFT JOIN school_statuses pss on (o.organization_previous_status_id = pss.school_status_id)
                                      LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
                                      left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id)
                                      left join addresses a on (a.address_id = o.organization_default_address_id)
                                      left join organization_bank_accounts oba on (oba.organization_id = o.organization_id)
                                      left join users u on u.user_email = o.organization_email
                                      left join education_phases ep on ep.education_phase_id = o.school_education_phase_id
                                      where o.organization_status_id = 7");

		$headersDone = false;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}
			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace("#",'',(trim($field))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'WithdrawnOrganizations'.date('Y-m-d-G-i-s').'.csv';
		// save it to a file..
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}

	public function activatingorganizations()
	{

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("SELECT
										o.school_URN AS `School URN`,
										o.organization_parent_id AS `Parent URN`,
										o.organization_name AS `Organization Name`,
										ep.education_phase_name AS `Education Phase`,
										la.local_authority_name AS `LEA Name`,
										o.organization_registered_date AS `Date Registered`,
										'' AS `Days Registered`,
										u.user_name AS `Contact Name`,
										o.organization_member_count AS `Organization Members`,
										o.organization_telephone AS `Organization Phone`,
										o.organization_email AS `Organization Email`,
										o.organization_url AS `Organization URL`,
										o.organization_funds AS `Organization Funds`,
										o.organization_can_bulk AS `Organization Bulk Order`,
										o.organization_form_signed AS `Organization Form Signed`,
										ss.school_status_name AS `Organization Status`,
										ol.tesco_style_ref AS `Organization Main Logo`,
										a.entry_company AS `Organization Address 1`,
										a.entry_street_address AS `Organization Address 2`,
										a.entry_street_address_2 AS `Organization Address 3`,
										a.entry_city AS `Organization City`,
										a.entry_suburb AS `Organization Suburb`,
										a.entry_state AS `Organization State`,
										a.entry_postcode AS `Organization Postcode`,
										CONCAT(oba.organization_bank_sort_code_1,'-',oba.organization_bank_sort_code_2,'-',oba.organization_bank_sort_code_3) AS `Organization Bank Sort Code`,
										oba.organization_bank_account AS `Organization Bank Account Number`,
										oba.organization_bank_name AS `Organization Bank Name`,
										oba.organization_bank_last_updated AS `Organization Bank Updated`,
										o.organization_id AS `Record ID`,
										COUNT(t.`ticket_id`) AS `Note Count`,
										t.`ticket_open_date` AS `Last Note Date`,
										t.`ticket_subject` AS `Last Note Subject`,
										t.`ticket_description` AS `Last Note`
									  FROM organizations o
                                      JOIN school_statuses ss ON (o.organization_status_id = ss.school_status_id)
                                      LEFT JOIN local_authorities la ON (la.local_authority_id = o.school_local_authority_id)
                                      LEFT JOIN organization_logos ol ON (ol.organization_logo_id = o.organization_default_logo_id)
                                      LEFT JOIN addresses a ON (a.address_id = o.organization_default_address_id)
                                      LEFT JOIN organization_bank_accounts oba ON (oba.organization_id = o.organization_id)
                                      LEFT JOIN users u ON u.user_email = o.organization_email
                                      LEFT JOIN education_phases ep ON ep.education_phase_id = o.school_education_phase_id
                                      LEFT JOIN tickets t ON (t.`ticket_organization` = o.`organization_id` AND t.`ticket_type_id` = 10 )
                                      WHERE o.organization_status_id IN (4,5,6) GROUP BY o.organization_id ORDER BY o.`school_URN`, t.`ticket_open_date` DESC");

		$headersDone = false;
		while($organization = dbf($organizationQuery)){

			// find notes.
			/*$noteQuery = dbq("select t.ticket_open_date, t.ticket_subject, t.ticket_description, count(*) as note_count from tickets t where ticket_organization = :ORGID and t.`ticket_type_id` = 10 GROUP BY t.ticket_organization",
				[
					':ORGID' => $organization['organization_id']
				]
			);

			while($note = dbf($noteQuery)){
				$organization['Note Count'] = $note['note_count'];
				$organization['Last Note Date'] = $note['ticket_open_date'];
				$organization['Last Note Subject'] = $note['ticket_subject'];
				$organization['Last Note'] = $note['ticket_description'];
			}*/

			// unset($organization['organization_id']);

			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}

			$enddate = time();

			$regedDays = $enddate - strtotime($organization['Date Registered']);
			$regedDays = ceil($regedDays / 60 / 60 / 24);  //days
			$organization['Days Registered'] = $regedDays;

			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace(["\n","\r"],' ',str_replace("#",'',(trim($field)))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'ActivatingOrganizations'.date('Y-m-d-G-i-s').'.csv';
		// save it to a file..
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}

	public function productlist(){

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("select
										o.school_URN as `School URN`,
										o.organization_parent_id as `Parent URN`,
										o.organization_name as `Organization Name`,
										o.organization_registered_date as `Date Registered`,
										'' as `Days Registered`,
										u.user_name as `Contact Name`,
										o.organization_member_count as `Organization Members`,
										o.organization_telephone as `Organization Phone`,
										o.organization_email as `Organization Email`,
										o.organization_url as `Organization URL`,
										o.organization_funds as `Organization Funds`,
										o.organization_can_bulk as `Organization Bulk Order`,
										o.organization_form_signed as `Organization Form Signed`,
										ss.school_status_name as `Organization Status`,
										ol.tesco_style_ref as `Organization Main Logo`,
										a.entry_company as `Organization Address 1`,
										a.entry_street_address as `Organization Address 2`,
										a.entry_street_address_2 as `Organization Address 3`,
										a.entry_city as `Organization City`,
										a.entry_suburb as `Organization Suburb`,
										a.entry_state as `Organization State`,
										a.entry_postcode as `Organization Postcode`,
										CONCAT(oba.organization_bank_sort_code_1,'-',oba.organization_bank_sort_code_2,'-',oba.organization_bank_sort_code_3) as `Organization Bank Sort Code`,
										oba.organization_bank_account as `Organization Bank Account Number`,
										oba.organization_bank_name as `Organization Bank Name`,
										oba.organization_bank_last_updated as `Organization Bank Updated`
									  from organizations o
                                      JOIN school_statuses ss on (o.organization_status_id = ss.school_status_id)
                                      left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id)
                                      left join addresses a on (a.address_id = o.organization_default_address_id)
                                      left join organization_bank_accounts oba on (oba.organization_id = o.organization_id)
                                      left join users u on u.user_email = o.organization_email
                                      where o.organization_status_id in (4,5,6)");

		$headersDone = false;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}

			$enddate = time();

			$regedDays = $enddate - strtotime($organization['Date Registered']);
			$regedDays = ceil($regedDays / 60 / 60 / 24);  //days
			$organization['Days Registered'] = $regedDays;

			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace("#",'',(trim($field))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'ActivatingOrganizations'.date('Y-m-d-G-i-s').'.csv';
		// save it to a file..
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}

	public function stilldigitizing(){

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("SELECT
										o.school_URN as `School URN`,
										o.organization_telephone as `Organization Phone`,
										o.organization_email as `Organization Email`,
										ss.school_status_name as `Organization Status`,
										ol.tesco_style_ref as `Organization Logo Code`,
										ol.`logo_requested_date` as `Date Requested`
									  from organizations o
                                      JOIN school_statuses ss on (o.organization_status_id = ss.school_status_id)
                                      left join organization_logos ol on (ol.organization_id = o.organization_id)
                                      WHERE
                                      o.organization_status_id != 7 AND
										ol.logo_deleted = 0 AND
										((ol.size__mm_ = '64.8x41.6' AND ol.stitch_count = 3269) OR
										(ol.size__mm_ IS NULL AND ol.stitch_count IS NULL))
										ORDER BY ol.`logo_requested_date` ASC
                                      "
		);

		$headersDone = false;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}

			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace("#",'',(trim($field))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'StillDigitizingList'.date('Y-m-d-G-i-s').'.csv';
		// save it to a file..
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}

	public function nominationlist(){

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		if (!isset($this->request['fromdate']) || empty($this->request['fromdate']) || !isset($this->request['todate']) || empty($this->request['todate'])) {
			$this->setError([
				'message' => 'Cannot provide results, required arguments are missing.',
				'errorCode' => 'arguments-missing',
			]);
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$fromdate = date("Y-m-d",strtotime($this->request['fromdate']))." 00:00:01";
		$todate = date("Y-m-d",strtotime($this->request['todate']))." 23:59:59";

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("SELECT
									sd.URN as `School URN`,
									sd.Name as `School Name`,
									sd.SchoolType as `School Type`,
									sd.Address1 as `Address 1`,
									sd.Address2 as `Address 2`,
									sd.Address3 as `Address 3`,
									sd.Town as `Town`,
									sd.County as `County`,
									sd.Postcode as `Post Code`,
									sd.Phone as `Telephone`,
									sd.Fax as `Fax`,
									sd.URL as `URL`,
									sd.Email as `Email`,
									sd.Pupils as `Pupils`,
									sd.`School Capacity` as `Capacity`,
									ss.school_status_name as `Organization Status`,
									sd.Nominations as `Total Nominations`,
									COUNT(n.`nomination_id`) AS `Period Nominations`
									FROM school_data sd
									JOIN school_statuses ss on ss.school_status_id = sd.school_status_id
									JOIN nominations n ON n.`school_data_id` = sd.`school_data_id`
									WHERE sd.nominations > 0
									AND
									sd.school_status_id IN (1,7)
									AND
									n.`nomination_date`
									AND
                                    n.nomination_date BETWEEN :FROMDATE AND :TODATE
                                    GROUP BY n.`school_data_id`
                                      ",
			[
				':FROMDATE' => $fromdate,
				':TODATE' => $todate
			]
		);

		$headersDone = false;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}

			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace("#",'',(trim($field))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'NominationsList'.date('Y-m-d-G-i-s').'.csv';
		// save it to a file..
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}


	public function referafriendlist(){

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		if (!isset($this->request['fromdate']) || empty($this->request['fromdate']) || !isset($this->request['todate']) || empty($this->request['todate'])) {
			$this->setError([
				'message' => 'Cannot provide results, required arguments are missing.',
				'errorCode' => 'arguments-missing',
			]);
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$fromdate = date("Y-m-d",strtotime($this->request['fromdate']))." 00:00:01";
		$todate = date("Y-m-d",strtotime($this->request['todate']))." 23:59:59";

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("select
										o.school_URN as `School URN`,
										o.organization_parent_id as `Parent URN`,
										o.organization_name as `Organization Name`,
										ep.education_phase_name as `Education Phase`,
										u.user_name as `Contact Name`,
										o.organization_member_count as `Organization Members`,
										o.organization_telephone as `Organization Phone`,
										o.organization_email as `Organization Email`,
										ss.school_status_name as `Organization Status`,
										a.entry_company as `Organization Address 1`,
										a.entry_street_address as `Organization Address 2`,
										a.entry_street_address_2 as `Organization Address 3`,
										a.entry_city as `Organization City`,
										a.entry_suburb as `Organization Suburb`,
										a.entry_state as `Organization State`,
										a.entry_postcode as `Organization Postcode`,
										o.organization_live_date as `Organization Live Date`,
										rafo.school_URN as `RAF School URN`,
										rafo.organization_name as `RAF Organization Name`,
										rafss.school_status_name as `RAF Organization Status`,
										rafa.entry_company as `RAF Organization Address 1`,
										rafa.entry_street_address as `RAF Organization Address 2`,
										rafa.entry_street_address_2 as `RAF Organization Address 3`,
										rafa.entry_city as `RAF Organization City`,
										rafa.entry_suburb as `RAF Organization Suburb`,
										rafa.entry_state as `RAF Organization State`,
										rafa.entry_postcode as `RAF Organization Postcode`
									  from organizations o
                                      JOIN school_statuses ss on (o.organization_status_id = ss.school_status_id)
                                      JOIN organizations rafo on (rafo.organization_id = o.organization_raf_organization_id)
                                      JOIN school_statuses rafss on (rafss.school_status_id = rafo.organization_status_id)
                                      LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
                                      left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id)
                                      left join addresses a on (a.address_id = o.organization_default_address_id)
                                      left join addresses rafa on (rafa.address_id = rafo.organization_default_address_id)
                                      left join users u on u.user_email = o.organization_email
                                      left join education_phases ep on ep.education_phase_id = o.school_education_phase_id
                                      where o.organization_status_id = 3
                                      AND
                                      o.organization_raf_organization_id > 0
                                      AND
                                      o.organization_live_date >= :FROMDATE
                                      AND
                                      o.organization_live_date <= :TODATE
                                      ",
			[
				':FROMDATE' => $fromdate,
				':TODATE' => $todate
			]
		);

		$headersDone = false;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}

			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace("#",'',(trim($field))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'ReferAFriendOrganizations'.date('Y-m-d-G-i-s').'.csv';
		// save it to a file..
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}

	public function welcomepackslist(){

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		$csv = [];
		$data['data']['contents'] = '';
		// get the uploaded data.
		$organizationQuery = dbq("select
										o.school_URN as `School URN`,
										o.organization_parent_id as `Parent URN`,
										o.organization_name as `Organization Name`,
										ep.education_phase_name as `Education Phase`,
										u.user_name as `Contact Name`,
										o.organization_member_count as `Organization Members`,
										o.organization_telephone as `Organization Phone`,
										o.organization_email as `Organization Email`,
										ss.school_status_name as `Organization Status`,
										a.entry_company as `Organization Address 1`,
										a.entry_street_address as `Organization Address 2`,
										a.entry_street_address_2 as `Organization Address 3`,
										a.entry_city as `Organization City`,
										a.entry_suburb as `Organization Suburb`,
										a.entry_state as `Organization State`,
										a.entry_postcode as `Organization Postcode`,
										o.organization_welcome_pack as `Welcome Packs`
									  from organizations o
                                      JOIN school_statuses ss on (o.organization_status_id = ss.school_status_id)
                                      LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
                                      left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id)
                                      left join addresses a on (a.address_id = o.organization_default_address_id)
                                      left join users u on u.user_email = o.organization_email
                                      left join education_phases ep on ep.education_phase_id = o.school_education_phase_id
                                      where o.organization_status_id = 3
                                      and o.organization_welcome_pack > 0");

		$headersDone = false;
		while($organization = dbf($organizationQuery )){
			if(!$headersDone){
				$csv[] = array_keys($organization);
				$headersDone = true;
			}
			$csv[] = $organization;
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($csvline as $field){
				$writeline .= '"'.str_replace("#",'',(trim($field))).'",';
			}
			$writeline = substr($writeline,0,-1); // knock off last ,
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'WelcomePackOrganizations'.date('Y-m-d-G-i-s').'.csv';
		// save it to a file..
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		$data['data']['contents'] = [];
		$data['data']['filename'] = API_ROOT.'reports/'.$data['data']['filename'] ;
		return $data;

	}
}
