<?php

/**
 * Rest object example
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class bulk extends restObject
{

    /**
     * user data
     */
    public $id;
    public $name;
    public $lastName;
    public $login;

    /**
     *
     * @param string $method
     * @param array $request
     * @param string $file
     */
    public function __construct($method, $request = null, $file = null)
    {
        if (!isset($_SESSION['session']['user']['user_type_id']) || $_SESSION['session']['user']['user_type_id'] == 1) {
            // Admin Only so return
            $this->setError([
                'message' => 'not authorized.',
                'errorCode' => 'authorization-required',
            ]);
            return $this->getResponse(500);
        }
        parent::__construct($method, $request, $file);
    }

    public function getbulklist(){
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['p']) || empty($this->request['p'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $resultsPerPage = 25;

        // work out limit.
        $startAt = ($this->request['p'] * $resultsPerPage) - ($resultsPerPage);

        if (isset($this->request['q']) && !empty($this->request['q'])) {

            $searchWords = [$this->request['q']];//explode(' ',$this->request['q']);

            $lookup = [];

            $weighting = "IF(
			`order_name` LIKE '{$this->request['q']}%',  20, IF(`order_name` LIKE '%{$this->request['q']}%', 10, 0)
      							)
      								+ IF(`order_email_address` LIKE '%{$this->request['q']}%', 5,  0)
      								+ IF(`order_contact_name` LIKE '%{$this->request['q']}%', 1,  0)
    							";
            foreach ($searchWords as $index => $word) {
                if (!empty($weighting)) {
                    $weighting .= ' + ';
                }

                $weighting .= "IF(
			`order_name` LIKE '$word%',  20, IF(`order_name` LIKE '%$word%', 10, 0)
      							)
      								+ IF(`order_email_address` LIKE '%$word%', 5,  0)
      								+ IF(`order_contact_name` LIKE '%$word%', 1,  0)
    							";

                $lookup[':LOOKUP' . $index] = '%' . urldecode($word) . '%';
            }
            $weighting .= ' AS `weight`,';
// To search other fields add them to here.
            $matchFields = [
                'order_name',
                'order_email_address',
                'order_contact_name',
                'organization_bulk_order_id',
                'o.school_URN'
            ];

            $searchTerms = '';
            foreach ($lookup as $key => $value) {
                // Group each word into its own OR group.
                $searchTerms .= '(';
                foreach ($matchFields as $field) {
                    if (!stristr($field, '.')) {
                        $searchTerms .= "oba.`$field` LIKE $key OR ";
                    } else {
                        $searchTerms .= "$field LIKE $key OR ";
                    }
                }
                $searchTerms = substr($searchTerms, 0, -3);
                // link OR groups by AND.  each word must be there but could be in any field.
                $searchTerms .= ') AND ';
            }

            $searchTerms = substr($searchTerms, 0, -4);


    } else {
        $weighting = '1 as weight,';
        $searchTerms = '1';
        $lookup = [];
    }


$bulkOrderQuery = dbq("SELECT SQL_CALC_FOUND_ROWS
                        $weighting
                        oba.*, o.school_URN
                      FROM
                        organization_bulk_orders oba
                        JOIN organizations o on o.organization_id = oba.organization_id
                      where ( $searchTerms )

                    ORDER BY oba.order_date DESC
        LIMIT $startAt, $resultsPerPage",$lookup);

        $rows = dbf(dbq("SELECT FOUND_ROWS() as totalRows")); // read how many total rows there are without LIMIT.

        $outputArray = [];

        while($bulkOrder = dbf($bulkOrderQuery)){
            $outputArray[] = $bulkOrder;
        }

        $data = [
            'count' => 0,
            'totalrows' => intval($rows['totalRows']),
            'orders' => $outputArray
        ];

        return $data;
    }

    public function getbulkorder() {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['bid']) || empty($this->request['bid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);

            return $this->getResponse(500);
        }

        $orderQuery = dbq("select obo.*, o.school_URN, o.organization_name from organization_bulk_orders obo join organizations o  on o.organization_id = obo.organization_id where obo.organization_bulk_order_id = :OBID",
            [
                ':OBID' => $this->request['bid']
            ]);



        if(dbnr($orderQuery) > 0){
            $data = [
                'header' => [],
                'lines' => []
            ];

            while($order = dbf($orderQuery)){
                $data['header'] = $order;
                // Split any errors?
                $GMOErrors = explode("\n",substr($order['organization_bulk_order_gmo_order_error'],strpos($order['organization_bulk_order_gmo_order_error'],'[')));
                $errorProducts = [];
                if(!empty($GMOErrors)){
                    // Make them into times etc
                    foreach($GMOErrors as $GMOError){
                        if(!empty($GMOError) && stristr($GMOError,':')){
                            $GMOError = str_replace(['[',']'],'',$GMOError);
                            list($reason,$catid) = explode(':',$GMOError);
                            $errorProducts[$catid] = $reason;
                        }
                    }
                }

                $orderItemQuery = dbq("SELECT oboi.*, ps2pc.product_variation_stock from organization_bulk_order_items oboi join products_sizes_to_products_colours ps2pc on ps2pc.product_variation_sku = oboi.product_ean where oboi.organization_bulk_order_id = :OBID",
                    [
                        ':OBID' => $this->request['bid']
                    ]
                );

                while($orderItems = dbf($orderItemQuery)){
                    if(isset($errorProducts[$orderItems['product_gmo_cat_id']])){
                        $orderItems['GMOERROR'] = $errorProducts[$orderItems['product_gmo_cat_id']];
                    } else {
                        $orderItems['GMOERROR'] = '';
                    }
                    $data['lines'][] = $orderItems;

                }

            }
        } else {
            // error, no order found.
            $this->setError([
                'message' => 'Cannot provide results, Order not found.',
                'errorCode' => 'order-missing',
            ]);

            return $this->getResponse(500);
        }
        return $data;
    }

    public function updatebulk(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if($_SESSION['session']['user']['user_id'] != 4){
            $data['type'] = 500;
            $data['messages'][] =
                [
                    'type' => 'danger',
                    'message' => 'Save disabled during testing.'
                ];

            return $data;
        }


        if (!isset($this->request['bulkData']) || empty($this->request['bulkData'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);

            return $this->getResponse(500);
        }

        // need to add or remove some items.
        if(!empty($this->request['bulkData']['removed'])){
            foreach($this->request['bulkData']['removed'] as $removedItem){
                dbq("update organization_bulk_order_items set product_removed = 1 where organization_bulk_order_item_id = :OBOIID",
                    [
                        ':OBOIID' => $removedItem['organization_bulk_order_item_id']
                    ]
                );
            }
        }

        if(!empty($this->request['bulkData']['added'])){
            // build a product list to work from.
            $productLines = [];
            foreach($this->request['bulkData']['lines'] as $orderLine) {
                $productLines[$orderLine['organization_bulk_order_item_id']] = $orderLine;
            }


            foreach($this->request['bulkData']['added'] as $addedItem){
                // gather info and insert into dbase.
                if(isset($productLines[$addedItem['added']])){

                    // insert right?
                    $update = dbq("insert into organization_bulk_order_items (
                                organization_bulk_order_id,
                                organization_urn,
                                product_colour_style_ref,
                                product_name,
                                product_colour_name,
                                product_colour_image_url,
                                product_ean,
                                product_price,
                                product_quantity,
                                product_line_price,
                                product_size_name,
                                products_sizes_to_colours_id,
                                products_logo_ref,
                                product_gmo_cat_id
                            ) VALUES (
                                :ORGANIZATIONBULKORDERID,
                                :ORGANIZATIONURN,
                                :PRODUCTCOLOURSTYLEREF,
                                :PRODUCTNAME,
                                :PRODUCTCOLOURNAME,
                                :PRODUCTCOLOURIMAGEURL,
                                :PRODUCTEAN,
                                :PRODUCTPRICE,
                                :PRODUCTQUANTITY,
                                :PRODUCTLINEPRICE,
                                :PRODUCTSIZENAME,
                                :PRODUCTSSIZESTOCOLOURSID,
                                :PRODUCTSLOGOREF,
                                :PRODUCTGMOCATID
                            )",
                        [
                            ':ORGANIZATIONBULKORDERID' => $this->request['bulkData']['header']['organization_bulk_order_id'],
                            ':ORGANIZATIONURN' => $this->request['bulkData']['header']['school_URN'],
                            ':PRODUCTCOLOURSTYLEREF' => $productLines[$addedItem['added']]['product_colour_style_ref'],
                            ':PRODUCTNAME' => $productLines[$addedItem['added']]['product_name'],
                            ':PRODUCTCOLOURNAME' => $productLines[$addedItem['added']]['product_colour_name'],
                            ':PRODUCTCOLOURIMAGEURL' => $productLines[$addedItem['added']]['product_colour_image_url'],
                            ':PRODUCTEAN' => $productLines[$addedItem['added']]['product_ean'],
                            ':PRODUCTPRICE' => $productLines[$addedItem['added']]['product_price'],
                            ':PRODUCTQUANTITY' => $productLines[$addedItem['added']]['product_quantity'],
                            ':PRODUCTLINEPRICE' => $productLines[$addedItem['added']]['product_line_price'],
                            ':PRODUCTSIZENAME' => $productLines[$addedItem['added']]['product_size_name'],
                            ':PRODUCTSSIZESTOCOLOURSID' => $productLines[$addedItem['added']]['products_sizes_to_colours_id'],
                            ':PRODUCTSLOGOREF' => $productLines[$addedItem['added']]['products_logo_ref'],
                            ':PRODUCTGMOCATID' => $productLines[$addedItem['added']]['product_gmo_cat_id']
                        ]
                    );


                }
            }
        }

        $totalQuery = dbq("select sum(product_line_price) as orderTotal from organization_bulk_order_items where organization_bulk_order_id = :OBOID and product_removed = 0",
            [
                ':OBOID' => $this->request['bulkData']['header']['organization_bulk_order_id']
            ]
        );

        $total = dbf($totalQuery);

        dbq("update organization_bulk_orders set
                    organization_bulk_order_gmo_sent = 0,
                    organization_bulk_order_gmo_sent_datetime = NULL,
                    order_contact_name = :ORDER_CONTACT_NAME,
                    order_email_address = :ORDER_EMAIL_ADDRESS,
                    order_telephone = :ORDER_TELEPHONE,
                    order_delivery_address_1 = :ORDER_DELIVERY_ADDRESS_1,
                    order_delivery_address_2 = :ORDER_DELIVERY_ADDRESS_2,
                    order_delivery_address_3 = :ORDER_DELIVERY_ADDRESS_3,
                    order_delivery_town = :ORDER_DELIVERY_TOWN,
                    order_delivery_county = :ORDER_DELIVERY_COUNTY,
                    order_delivery_postcode = :ORDER_DELIVERY_POSTCODE,
                    organization_bulk_order_total = :ORDER_TOTAL
                  where
                    organization_bulk_order_id = :OBOID",
            [
                ':OBOID' => $this->request['bulkData']['header']['organization_bulk_order_id'],
                ':ORDER_CONTACT_NAME' => $this->request['bulkData']['header']['order_contact_name'],
                ':ORDER_EMAIL_ADDRESS' => $this->request['bulkData']['header']['order_email_address'],
                ':ORDER_TELEPHONE' => $this->request['bulkData']['header']['order_telephone'],
                ':ORDER_DELIVERY_ADDRESS_1' => $this->request['bulkData']['header']['order_delivery_address_1'],
                ':ORDER_DELIVERY_ADDRESS_2' => $this->request['bulkData']['header']['order_delivery_address_2'],
                ':ORDER_DELIVERY_ADDRESS_3' => $this->request['bulkData']['header']['order_delivery_address_3'],
                ':ORDER_DELIVERY_TOWN' => $this->request['bulkData']['header']['order_delivery_town'],
                ':ORDER_DELIVERY_COUNTY' => $this->request['bulkData']['header']['order_delivery_county'],
                ':ORDER_DELIVERY_POSTCODE' => $this->request['bulkData']['header']['order_delivery_postcode'],
                ':ORDER_TOTAL' => $total['orderTotal']
            ]
        );

        $data['type'] = 200;
        $data['messages'][] =
            [
                'type' => 'success',
                'message' => 'Bulk Order will be re-submitted to ATG.'
            ];

        return $data;
    }
}
