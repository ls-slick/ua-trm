<?php

/**
 * Rest object example
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class faqs extends restObject
{

    /**
     * user data
     */
    public $id;
    public $name;
    public $lastName;
    public $login;

    /**
     *
     * @param string $method
     * @param array $request
     * @param string $file
     */
    public function __construct($method, $request = null, $file = null)
    {
        parent::__construct($method, $request, $file);
    }


    public function getfaqs()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $data = [];

        // Admin has extra info.
        if (isset($_SESSION['session']) && $_SESSION['session']['user']['user_type_id'] != 1) {
            // select it.
            $faqQuery = dbq("SELECT * FROM frequently_asked_questions faq
							JOIN frequently_asked_question_categories faqc USING (frequently_asked_question_category_id)");

            $tempData = [];
            while ($faq = dbf($faqQuery)) {

                $tempData[$faq['frequently_asked_question_category_name']][] = [
                    'id' => $faq['frequently_asked_question_id'],
                    'title' => $faq['frequently_asked_question_text'],
                    'content' => $faq['frequently_asked_question_answer'],
                    'active' => $faq['frequently_asked_question_active'],
                    'uniformactive' => $faq['frequently_asked_question_uniform_active']
                ];


            }
        } else {
            // public
            // select it.
            $faqQuery = dbq("SELECT * FROM frequently_asked_questions faq
							JOIN frequently_asked_question_categories faqc USING (frequently_asked_question_category_id) WHERE frequently_asked_question_active = 1");

            $tempData = [];
            while ($faq = dbf($faqQuery)) {
                // if admin include more detail?


                $tempData[$faq['frequently_asked_question_category_name']][] = [
                    'id' => $faq['frequently_asked_question_id'],
                    'title' => $faq['frequently_asked_question_text'],
                    'content' => $faq['frequently_asked_question_answer']
                ];


            }
        }


        foreach ($tempData as $title => $faqdata) {
            $data[] = [
                'sectionTitle' => $title,
                'sectionFAQs' => $faqdata
            ];
        }


        return $data;
    }

    public function getresources()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $data = [];

        // Admin has extra info.
        if (isset($_SESSION['session']) && $_SESSION['session']['user']['user_type_id'] != 1) {
            // select it.
            $resourceQuery = dbq("SELECT * FROM resources");

            while ($resource = dbf($resourceQuery)) {
                // if admin include more detail?
                $data[] = [
                    'id' => $resource['resource_id'],
                    'title' => $resource['resource_name'],
                    'content' => $resource['resource_filename'],
                    'active' => $resource['resource_active']
                ];
            }
        } else {
            // public
            // select it.
            $resourceQuery = dbq("SELECT * FROM resources WHERE resource_active = 1");

            $tempData = [];
            while ($resource = dbf($resourceQuery)) {
                // if admin include more detail?
                $data[] = [
                    'id' => $resource['resource_id'],
                    'title' => $resource['resource_name'],
                    'content' => $resource['resource_filename']
                ];
            }
        }

        return $data;
    }


}
