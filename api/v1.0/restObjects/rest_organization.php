<?php

/**
 * Rest object example
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class organization extends restObject
{

    /**
     *
     * @param string $method
     * @param array $request
     * @param string $file
     */
    public function __construct($method, $request = null, $file = null)
    {
        parent::__construct($method, $request, $file);
    }

    /**
     * Lookup endpoint.  Search all schools.
     * @return array
     */
    public function lookup()
    {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['q']) || empty($this->request['q'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // split the query into individual words.
        $searchWords = explode(' ', $this->request['q']);
        $weighting = "IF(
			`Name` LIKE '".addslashes($this->request['q'])."%',  20, IF(`Name` LIKE '%".addslashes($this->request['q'])."%', 10, 0)
      							)
      								+ IF(`Town` LIKE '%".addslashes($this->request['q'])."%', 5,  0)
      								+ IF(`Postcode` LIKE '%".addslashes($this->request['q'])."%', 1,  0)
    							";
        foreach ($searchWords as $index => $word) {
            $word = addslashes($word);

            if (!empty($weighting)) {
                $weighting .= ' + ';
            }

            $weighting .= "IF(
			`Name` LIKE '$word%',  20, IF(`Name` LIKE '%$word%', 10, 0)
      							)
      								+ IF(`Town` LIKE '%$word%', 5,  0)
      								+ IF(`Postcode` LIKE '%$word%', 1,  0)
    							";

            $lookup[':LOOKUP' . $index] = '%' . urldecode($word) . '%';
        }
        $weighting .= ' AS `weight`,';

        //echo $weighting;
        //return $weighting;
        $startat = isset($this->request['s']) ? $this->request['s'] : 0;

// To search other fields add them to here.
// Todo make this selective on what it returns.  All words in Name should take priority, then work your way down.
        $matchFields = [
            'Name',
            'o.`organization_name`',
            'Address1',
            'Address2',
            'Town',
            'County',
            'Postcode',
            'a.`entry_postcode`',
            'URL',
            'Phone'
        ];

        $searchTerms = '';
        foreach ($lookup as $key => $value) {
            // Group each word into its own OR group.
            $searchTerms .= '(';


            foreach ($matchFields as $field) {
                if (!stristr($field, '.')) {
                    $searchTerms .= "sd.`$field` LIKE $key OR ";
                } else {
                    $searchTerms .= "$field LIKE $key OR ";
                }

                //$searchTerms .= "sd.`$field` LIKE $key OR ";
            }
            $searchTerms = substr($searchTerms, 0, -3);
            // link OR groups by AND.  each word must be there but could be in any field.
            $searchTerms .= ') AND ';
        }
        $searchTerms = substr($searchTerms, 0, -4);

        $searchSchoolQuery = dbq("select SQL_CALC_FOUND_ROWS
							   $weighting
                               sd.`school_data_id`,
                               sd.`URN`,

                               IF(ISNULL(o.organization_name),sd.`Name`,o.organization_name) as `Name`,
                               IF(ISNULL(a.entry_street_address),sd.`Address1`,a.entry_street_address) as `Address1`,
                               IF(ISNULL(a.entry_street_address_2),sd.`Address2`,a.entry_street_address_2) as `Address2`,
                               IF(ISNULL(a.entry_postcode),sd.`Postcode`,a.entry_postcode) as `Postcode`,

                               o.`organization_id`,
                               ss.`school_status_master_name`,
                               ol.`tesco_style_ref` as logo_ref,
                               ol.`logo_url` as emblem,
                               ol.`primary_background_colour` as emblem_background,
                               IF(ISNULL(o.organization_funds),0,o.organization_funds) as organization_funds,
                               sd.`Nominations` + IF(ISNULL(n.nomination_date),0,COUNT(*)) AS nominations,
                               IF(ISNULL(ol.`school_name`),'Badge',ol.`school_name`) as emblem_name,
                               IF(ISNULL(ol.`logo_url`),'images/emblems/example-emblem.png',ol.`logo_url`) as emblem,
                               IF(ISNULL(ol.`logo_svg_url`),'images/emblems/example-emblem.png',ol.`logo_svg_url`) as emblem_svg
                        from school_data sd
                        join school_statuses ss using (school_status_id)
                        left join nominations n using (school_data_id)
                        left join organizations o using (school_data_id)
                        left join organization_units ou USING (organization_id)
                        left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id and ol.logo_approved = 1)
                        left join addresses a on (a.address_id = o.organization_default_address_id)
                        where ( $searchTerms )
                        GROUP BY sd.school_data_id
                        ORDER BY sd.school_status_id DESC, `weight` DESC, sd.`Name` ASC
                        limit $startat, 30", $lookup);

        // todo confirm what order these should be in.  Active first or purely in alphabetical order?

        $data = ['data' => [], 'metrics' => []];
        while ($searchSchool = dbf($searchSchoolQuery)) {
            // TODO check the emblems.  Create local version.  Smaller.  Maybe sidekiq this?  Or, just run a CURL to do it..  Like that better.

 /*
  			if(strstr($searchSchool['emblem'],"s3.amazonaws.com")){
				// This is a ebos logo so maybe make a local version here?
				$image = file_get_contents($searchSchool['emblem']);

				$im = new Imagick();
				$im->readImageBlob($image);
				$im->scaleImage(200,200);
				$filename = strtok(basename($searchSchool['emblem']),'?');
				// Save it.
				if($im->writeImage('C:\\wamp\\www\\ues\\images\\emblems\\'.$filename)){
					dbq("update organization_logos set logo_url = :NEWEMBLEM where organization_id = :ORGID and logo_url = :OLDEMBLEM",[
						':NEWEMBLEM' => 'images/emblems/'.$filename,
						':ORGID' => $searchSchool['organization_id'],
						':OLDEMBLEM' => $searchSchool['emblem']
					]);

					$searchSchool['emblem'] = 'images/emblems/'.$filename;
				}

			}
*/

            $data['data'][] = $searchSchool;
        }

        $rows = dbf(dbq("SELECT FOUND_ROWS() as totalRows")); // read how many total rows there are without LIMIT.
        $data['metrics']['totalRows'] = intval($rows['totalRows']);
        $data['metrics']['resultRows'] = sizeof($data['data']); // incase there are less than 30.

        return $data;
    }

    /**
     * Lookup endpoint. Search registered schools.
     * @return array
     */

    public function lookupregistered()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if ((!isset($this->request['query']) || !isset($this->request['limit'])) || empty($this->request['query']) || empty($this->request['limit'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // split the query into individual words.
        $searchWords = explode(' ', $this->request['query']);
        $searchLimit = $this->request['limit'];
        //$searchLimit = 10;
        $lookup = [];


        $weighting = "IF(
			`Name` LIKE '".addslashes($this->request['query'])."%',  20, IF(o.organization_name LIKE '%".addslashes($this->request['query'])."%', 10, 0)
      							)
      								+ IF(a.entry_city LIKE '%".addslashes($this->request['query'])."%', 5,  0)
      								+ IF(a.entry_postcode LIKE '%".addslashes($this->request['query'])."%', 1,  0)
    							";
        foreach ($searchWords as $index => $word) {

            $word = addslashes($word);

            if (!empty($weighting)) {
                $weighting .= ' + ';
            }

            $weighting .= "IF(
			`Name` LIKE '$word%',  20, IF(o.organization_name LIKE '%$word%', 10, 0)
      							)
      								+ IF(a.entry_city LIKE '%$word%', 5,  0)
      								+ IF(a.entry_postcode LIKE '%$word%', 1,  0)
    							";

            $lookup[':LOOKUP' . $index] = '%' . urldecode($word) . '%';
        }
        $weighting .= ' AS `weight`,';


        //foreach($searchWords as $index => $word){
        //	$lookup[':LOOKUP'.$index] = '%'.urldecode($word).'%';
        //}

// To search other fields add them to here.
        $matchFields = [
            'o.organization_name',
            'a.entry_street_address',
            'a.entry_street_address_2',
            'a.entry_city',
            'a.entry_state',
            'a.entry_postcode'
        ];

        $searchTerms = '';
        foreach ($lookup as $key => $value) {
            // Group each word into its own OR group.
            $searchTerms .= '(';
           /* foreach ($matchFields as $field) {
                $searchTerms .= "sd.`$field` LIKE $key OR ";
            }*/

            foreach ($matchFields as $field) {
                if (!stristr($field, '.')) {
                    $searchTerms .= "sd.`$field` LIKE $key OR ";
                } else {
                    $searchTerms .= "$field LIKE $key OR ";
                }
            }
            $searchTerms = substr($searchTerms, 0, -3);
            // link OR groups by AND.  each word must be there but could be in any field.
            $searchTerms .= ') AND ';
        }

        $searchTerms = substr($searchTerms, 0, -4);

        $searchSchoolQuery = dbq("select SQL_CALC_FOUND_ROWS
								$weighting
                               sd.`school_data_id`,
                               IF(ISNULL(o.organization_name),sd.`Name`,o.organization_name) as `Name`,
                               IF(ISNULL(a.entry_street_address),sd.`Address1`,a.entry_street_address) as `Address1`,
                               IF(ISNULL(a.entry_street_address_2),sd.`Address2`,a.entry_street_address_2) as `Address2`,
                               IF(ISNULL(a.entry_postcode),sd.`Postcode`,a.entry_postcode) as `Postcode`,

                              IF(ISNULL(a.entry_postcode),sd.`Address3`,'') as `Address3`,
                              IF(ISNULL(a.entry_city),sd.`Town`,a.entry_city) as `Town`,
                              IF(ISNULL(a.entry_state),sd.`County`,a.entry_state) as `County`,
                              IF(ISNULL(a.entry_suburb),sd.`Area`,a.entry_suburb) as `Area`,

                               o.`organization_id`,
                               ss.`school_status_name`,
                               ol.`tesco_style_ref` as logo_ref,
                               ol.`primary_background_colour` as emblem_background,
                               o.organization_has_subunits,
                               o.organization_parent_id,
                               IF(ISNULL(o.organization_funds),0,o.organization_funds) as organization_funds,
                               IF(ISNULL(ol.`logo_url`),'images/emblems/example-emblem.png',ol.`logo_url`) as emblem_png,
                               IF(ISNULL(ol.`logo_svg_url`),'images/emblems/example-emblem.png',ol.`logo_svg_url`) as emblem_svg
                        from school_data sd
                        join school_statuses ss using (school_status_id)
                        left join organizations o using (school_data_id)
                        left join organization_units ou USING (organization_id)
                        left join addresses a on (o.organization_default_address_id = a.address_id)
                        left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id and ol.logo_approved = 1)
                        where ( $searchTerms )
                        AND o.organization_status_id = 3
                        AND o.organization_parent_id IS NULL
                        GROUP BY sd.school_data_id
                        ORDER BY `weight` DESC, sd.`Name` ASC
                        limit $searchLimit", $lookup);

        $data = [
            'count' => 0,
            'organizations' => [],
        ];

        $dataCount = 0;

        // need to split out emblems and address.

        $addressFields = [
            'Address1',
            'Address2',
            'Address3',
            'Town',
            'County',
            'Area',
            'Postcode'
        ];

        $emblemFields = [
            'emblem_png',
            'emblem_svg',
            'emblem_background',
        ];

        $doubleFields = [
            'organization_funds',
        ];

        $ignoreFields = [
            'weight',
            'organization_has_subunits',
            'organization_parent_id'
        ];

        while ($searchSchool = dbf($searchSchoolQuery)) {
            $searchSchool['emblem_png'] = 'https:' . $searchSchool['emblem_png'];
            foreach ($searchSchool as $key => $value) {
                if (!in_array($key, $ignoreFields)) {
                    if (in_array($key, $emblemFields)) {
                        $usevalue = str_replace('http://','https://',$value);
                        $data['organizations'][$dataCount]['emblem'][str_replace('emblem_', '', strtolower($key))] = $usevalue;
                    } else if (in_array($key, $addressFields)) {
                        $data['organizations'][$dataCount]['address'][strtolower($key)] = $value;
                    } else if (in_array($key, $doubleFields)) {
                        $data['organizations'][$dataCount][strtolower($key)] = sprintf("%0.2f", round($value, 2));
                    } else {
                        $data['organizations'][$dataCount][strtolower($key)] = $value;
                    }
                }
            }
            // If this is a house, need the others and the master too.

            // does it have houses.
            if ($searchSchool['organization_has_subunits']) {
                // has houses.
                $dataSubCount = 0;

            /*    // add the main school to the houses.  No?
                foreach ($searchSchool as $key => $value) {
                    if (!in_array($key, $ignoreFields)) {
                        if (in_array($key, $emblemFields)) {
                            $data['organizations'][$dataCount]['subunits'][$dataSubCount]['emblem'][str_replace('emblem_', '', strtolower($key))] = $value;
                        } else if (in_array($key, $addressFields)) {
                            $data['organizations'][$dataCount]['subunits'][$dataSubCount]['address'][strtolower($key)] = $value;
                        } else {
                            if (is_numeric($value) && $value == doubleval($value)) {
                                $value = round($value, 2);
                            }
                            $data['organizations'][$dataCount]['subunits'][$dataSubCount][strtolower($key)] = $value;
                        }
                    }
                }

                $dataSubCount++;*/

                $searchSubSchoolQuery = dbq("select SQL_CALC_FOUND_ROWS
                               sd.`school_data_id`,
                               IF(ISNULL(o.organization_name),sd.`Name`,o.organization_name) as `Name`,
                               IF(ISNULL(a.entry_street_address),sd.`Address1`,a.entry_street_address) as `Address1`,
                               IF(ISNULL(a.entry_street_address_2),sd.`Address2`,a.entry_street_address_2) as `Address2`,
                               IF(ISNULL(a.entry_postcode),sd.`Postcode`,a.entry_postcode) as `Postcode`,
                               sd.`Address3`,
                               sd.`Town`,
                               sd.`County`,
                               sd.`Area`,
                               o.`organization_id`,
                               ss.`school_status_name`,
                               ol.`tesco_style_ref` as logo_ref,
                               ol.`primary_background_colour` as emblem_background,
                               IF(ISNULL(o.organization_funds),0,o.organization_funds) as organization_funds,
                               IF(ISNULL(ol.`logo_url`),'images/emblems/example-emblem.png',ol.`logo_url`) as emblem_png,
                               IF(ISNULL(ol.`logo_svg_url`),'images/emblems/example-emblem.png',ol.`logo_svg_url`) as emblem_svg
                        from school_data sd
                        join school_statuses ss using (school_status_id)
                        left join organizations o using (school_data_id)
                        left join organization_units ou USING (organization_id)
                        left join addresses a on (o.organization_default_address_id = a.address_id)
                        left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id and ol.logo_approved = 1)
                        where o.organization_status_id = 3
                        AND o.organization_parent_organization_id = :PARENTID
                        ORDER BY o.organization_id DESC", [':PARENTID' => $searchSchool['organization_id']]);

                while ($searchSubSchool = dbf($searchSubSchoolQuery)) {
                    $searchSubSchool['emblem_png'] = 'https:' . $searchSubSchool['emblem_png'];
                    foreach ($searchSubSchool as $key => $value) {
                        if (!in_array($key, $ignoreFields)) {
                            if (in_array($key, $emblemFields)) {
                                $usevalue = str_replace('http://','https://',$value);
                                $data['organizations'][$dataCount]['subunits'][$dataSubCount]['emblem'][str_replace('emblem_', '', strtolower($key))] = $usevalue;
                            } else if (in_array($key, $addressFields)) {
                                $data['organizations'][$dataCount]['subunits'][$dataSubCount]['address'][strtolower($key)] = $value;
                            } else {
                                if (is_numeric($value) && $value == doubleval($value)) {
                                    $value = round($value, 2);
                                }
                                $data['organizations'][$dataCount]['subunits'][$dataSubCount][strtolower($key)] = $value;
                            }
                        }
                    }
                    // If this is a house, need the others and the master too.
                    $dataSubCount++;
                }


            } else {
                $data['organizations'][$dataCount]['subunits'] = null;
            }
            $dataCount++;
        }

        $data['count'] = sizeof($data['organizations']);

        return $data;
    }



    /**
     * Lookup endpoint. Search registered schools.
     * @return array
     */

    public function getorglist()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['p']) || empty($this->request['p']) || !isset($this->request['t']) || empty($this->request['t'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $resultsPerPage = 25;

        if (isset($this->request['q']) && !empty($this->request['q'])) {
            // need to search too.
            // Exact match only.

            if(preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/',$this->request['q']) == true)
            {
                $this->request['q'] = str_replace("'", "\'", $this->request['q']);
            }

            $searchWords = [$this->request['q']];//explode(' ',$this->request['q']);

            $lookup = [];

            $weighting = "IF(
			`Name` LIKE '{$this->request['q']}%',  20, IF(`Name` LIKE '%{$this->request['q']}%', 10, 0)
      							)
      								+ IF(`Town` LIKE '%{$this->request['q']}%', 5,  0)
      								+ IF(`Postcode` LIKE '%{$this->request['q']}%', 1,  0)
    							";
            foreach ($searchWords as $index => $word) {
                if (!empty($weighting)) {
                    $weighting .= ' + ';
                }

                $weighting .= "IF(
			`Name` LIKE '$word%',  20, IF(`Name` LIKE '%$word%', 10, 0)
      							)
      								+ IF(`Town` LIKE '%$word%', 5,  0)
      								+ IF(`Postcode` LIKE '%$word%', 1,  0)
    							";

                $lookup[':LOOKUP' . $index] = '%' . urldecode($word) . '%';
            }
            $weighting .= ' AS `weight`,';
// To search other fields add them to here.
            $matchFields = [
                'Name',
                'Address1',
                'Address2',
                'Town',
                'County',
                'Postcode',
                'URL',
                'URN',
                'Phone',
                'ss.school_status_name'
            ];

            $searchTerms = '';
            foreach ($lookup as $key => $value) {
                // Group each word into its own OR group.
                $searchTerms .= '(';
                foreach ($matchFields as $field) {
                    if (!stristr($field, '.')) {
                        $searchTerms .= "sd.`$field` LIKE $key OR ";
                    } else {
                        $searchTerms .= "$field LIKE $key OR ";
                    }
                }
                $searchTerms = substr($searchTerms, 0, -3);
                // link OR groups by AND.  each word must be there but could be in any field.
                $searchTerms .= ') AND ';
            }

            $searchTerms = substr($searchTerms, 0, -4);

        } else {
            $weighting = '1 as weight,';
            $searchTerms = '1';
            $lookup = [];
        }

        // work out limit.
        $startAt = ($this->request['p'] * $resultsPerPage) - ($resultsPerPage);

        $order_by = '`weight` DESC, sd.`Name` ASC';
        switch ($this->request['t']) {
            case 'a' :
                $statusIDs = implode(",", [2, 4, 5, 6]);
                $order_by = 'o.organization_registered_date, `weight` DESC, sd.`Name` ASC';
                break;
            case 'l' :
                $statusIDs = '3';
                $order_by = 'o.organization_live_date DESC, `weight` DESC, sd.`Name` ASC';
                break;
            case 'w' :
                $statusIDs = '7';
                $order_by = '`weight` DESC, wdate DESC, sd.`Name` ASC';
                break;
            case 's' :
                $statusIDs = '8';
                break;
            default :
                $statusIDs = '0';
                break;
        }

        $searchSchoolQuery = dbq("select SQL_CALC_FOUND_ROWS
								$weighting
                               sd.`school_data_id`,
                               sd.`URN`,
                               sd.`Name`,
                               sd.`Address1`,
                               sd.`Address2`,
                               sd.`Address3`,
                               sd.`Town`,
                               sd.`County`,
                               sd.`Area`,
                               sd.`Postcode`,
                               sd.`school_status_id`,
                               o.`organization_id`,
                               ss.`school_status_name`,
                               dol.`tesco_style_ref` as logo_ref,
                               dol.`primary_background_colour` as emblem_background,
                               o.organization_has_subunits,
                               o.organization_parent_id,
                               o.organization_registered_date,
                               o.organization_live_date,
                               o.organization_withdrawn_date as wdate,
                               o.organization_suspended_date as sdate,
                               o.organization_withdrawn_suspended_reason,
                               o.organization_form_signed,
                               IF(ISNULL(o.organization_funds),0,o.organization_funds) as organization_funds,
                               IF(ISNULL(dol.`logo_url`),'images/emblems/example-emblem.png',dol.`logo_url`) as emblem_png,
                               IF(ISNULL(dol.`logo_svg_url`),'images/emblems/example-emblem.png',dol.`logo_svg_url`) as emblem_svg,
                               count(ol.tesco_style_ref) as emblem_count,
                               sum(ol.logo_approved) as approved_emblem_count
                        from school_data sd
                        join school_statuses ss using (school_status_id)
                        left join organizations o using (school_data_id)
                        left join organization_units ou USING (organization_id)
                        left join organization_logos dol on (dol.organization_logo_id = o.organization_default_logo_id and dol.logo_approved = 1)
                        left join organization_logos ol on (ol.organization_id = o.organization_id and ol.logo_deleted = 0)

                        where ( $searchTerms )
                        AND sd.school_status_id in ($statusIDs)
                        AND o.organization_parent_id IS NULL

                        GROUP BY sd.school_data_id
                        ORDER BY $order_by
                        limit $startAt, $resultsPerPage", $lookup);

        $rows = dbf(dbq("SELECT FOUND_ROWS() as totalRows")); // read how many total rows there are without LIMIT.
        //$data['metrics']['totalRows'] = intval($rows['totalRows']);
        //$data['totalrows'] = intval($rows['totalRows']/$resultsPerPage);

        $data = [
            'count' => 0,
            'totalrows' => intval($rows['totalRows']),
            'organizations' => []
        ];

        $dataCount = 0;

        // need to split out emblems and address.

        $addressFields = [
            'Address1',
            'Address2',
            'Address3',
            'Town',
            'County',
            'Area',
            'Postcode'
        ];

        $emblemFields = [
            'emblem_png',
            'emblem_svg',
            'emblem_background',
        ];

        $doubleFields = [
            'organization_funds',
        ];

        $countFields = [
            'emblem_count',
            'approved_emblem_count',
            'product_count',
            'emb_product_count'
        ];


        $ignoreFields = [
            'weight',
            'organization_has_subunits',
            'organization_parent_id'
        ];

        while ($searchSchool = dbf($searchSchoolQuery)) {

            // get
            foreach ($searchSchool as $key => $value) {


                if (in_array($key, $countFields)) {
                    // set to 0 if empty.
                    if(empty($value) || is_null($value)){
                        $value = 0;
                    }
                }

                if (!in_array($key, $ignoreFields)) {

                    if (in_array($key, $emblemFields)) {
                        $data['organizations'][$dataCount]['emblem'][str_replace('emblem_', '', strtolower($key))] = $value;
                    } else if (in_array($key, $addressFields)) {
                        $data['organizations'][$dataCount]['address'][strtolower($key)] = $value;
                    } else if (in_array($key, $doubleFields)) {
                        $data['organizations'][$dataCount][strtolower($key)] = sprintf("%0.2f", round($value, 2));
                    } else {
                        $data['organizations'][$dataCount][strtolower($key)] = $value;
                    }
                }

            }


            // how many days old is it?
            if (!empty($searchSchool['organization_live_date'])) {
                //	$liveDays =
                if (!empty($searchSchool['wdate'])) {
                    $enddate = strtotime($searchSchool['wdate']);
                } elseif (!empty($searchSchool['sdate'])) {
                    $enddate = strtotime($searchSchool['sdate']);
                } else {
                    $enddate = time();
                }

                $liveDays = $enddate - strtotime($searchSchool['organization_live_date']);
                $liveDays = ceil($liveDays / 60 / 60 / 24);  //days
                $data['organizations'][$dataCount]['livedays'] = $liveDays;
            }

            if (!empty($searchSchool['organization_registered_date'])) {
                $enddate = time();
                $regDays = $enddate - strtotime($searchSchool['organization_registered_date']);
                $regDays = ceil($regDays / 60 / 60 / 24);  //days
                $data['organizations'][$dataCount]['sinceregistered'] = $regDays;
            }

            // If this is a house, need the others and the master too.
            $data['organizations'][$dataCount]['housecount'] = 0;

            // does it have houses.
            if ($searchSchool['organization_has_subunits']) {
                // has houses.
                $searchSubSchoolQuery = dbq("SELECT
                              *   FROM
                        organizations o
                        WHERE
                        o.organization_parent_id = :PARENTID
                        ORDER BY o.organization_id DESC", [':PARENTID' => $searchSchool['URN']]);

                while ($searchSubSchool = dbf($searchSubSchoolQuery)) {
                    // If this is a house, need the others and the master too.
                    $data['organizations'][$dataCount]['housecount']++;
                }

            } else {
                $data['organizations'][$dataCount]['subunits'] = null;
            }
            $dataCount++;
        }

        //need to add house count to $data['totalrows']

        return $data;
    }

    /**
     * Lookup endpoint.  Search all schools.
     * @return array
     */

    public function adminlookup()
    {
        // TODO make this lookup contact names too.

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($_SESSION['session']['user']['user_type_id']) || $_SESSION['session']['user']['user_type_id'] == 1) {
            // Admin Only.
            $this->setError([
                'message' => 'not authorized.',
                'errorCode' => 'authorization-required',
            ]);
            return $this->getResponse(500);
        }

        if (!isset($this->request['q']) || empty($this->request['q'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // split the query into individual words.
        $searchWords = explode(' ', $this->request['q']);
        $weighting = "IF(
			`Name` LIKE '{$this->request['q']}%',  20, IF(`Name` LIKE '%{$this->request['q']}%', 10, 0)
      							)
      								+ IF(`Town` LIKE '%{$this->request['q']}%', 5,  0)
      								+ IF(`Postcode` LIKE '%{$this->request['q']}%', 1,  0)
    							";
        foreach ($searchWords as $index => $word) {
            if (!empty($weighting)) {
                $weighting .= ' + ';
            }

            $weighting .= "IF(
			`Name` LIKE '$word%',  20, IF(`Name` LIKE '%$word%', 10, 0)
      							)
      								+ IF(`Town` LIKE '%$word%', 5,  0)
      								+ IF(`Postcode` LIKE '%$word%', 1,  0)
    							";

            $lookup[':LOOKUP' . $index] = '%' . urldecode($word) . '%';
        }
        $weighting .= ' AS `weight`,';

        //echo $weighting;
        //return $weighting;
        $startat = isset($this->request['s']) ? $this->request['s'] : 0;

// To search other fields add them to here.
// Todo make this selective on what it returns.  All words in Name should take priority, then work your way down.
        $matchFields = [
            'Name',
            'o.`organization_name`',
            'Postcode',
            'a.`entry_postcode`',
            'URL',
            'u.`user_email`',
            'Phone',
            'URN'
        ];

        // Add email and contact to this.

        $searchTerms = '';
        foreach ($lookup as $key => $value) {
            // Group each word into its own OR group.
            $searchTerms .= '(';
            foreach ($matchFields as $field) {
                if (!stristr($field, '.')) {
                    $searchTerms .= "sd.`$field` LIKE $key OR ";
                } else {
                    $searchTerms .= "$field LIKE $key OR ";
                }

            }
            $searchTerms = substr($searchTerms, 0, -3);
            // link OR groups by AND.  each word must be there but could be in any field.
            $searchTerms .= ') AND ';
        }
        $searchTerms = substr($searchTerms, 0, -4);


        $searchSchoolQuery = dbq("select SQL_CALC_FOUND_ROWS
							   $weighting
							   sd.*,
							   o.*,
							   ss.*,
							   ol.*,
                               sd.`school_data_id`,
                               sd.`URN`,
                               IF(ISNULL(o.school_local_authority_id),sd.`LEAName`,la.local_authority_name) as `LEAName`,
                               IF(ISNULL(o.organization_name),sd.`Name`,o.organization_name) as `Name`,
                               IF(ISNULL(a.entry_street_address),sd.`Address1`,a.entry_street_address) as `Address1`,
                               IF(ISNULL(a.entry_street_address_2),sd.`Address2`,a.entry_street_address_2) as `Address2`,
                               IF(ISNULL(a.entry_postcode),sd.`Postcode`,a.entry_postcode) as `Postcode`,
                               o.`organization_id`,
                               ss.`school_status_name`,
                               ss.`school_status_master_name`,
                               ol.`tesco_style_ref` as logo_ref,
                               ol.`logo_url` as emblem,
                               ol.`primary_background_colour` as emblem_background,
                               IF(ISNULL(o.organization_funds),0,o.organization_funds) as organization_funds,
                               IF(ISNULL(ol.`school_name`),'Badge',ol.`school_name`) as emblem_name,
                               IF(ISNULL(ol.`logo_url`),'images/emblems/example-emblem.png',ol.`logo_url`) as emblem,
                               IF(ISNULL(ol.`logo_svg_url`),'images/emblems/example-emblem.png',ol.`logo_svg_url`) as emblem_svg,
                               IF(ISNULL(oba.`organization_bank_name`),0,1) as bank_details_entered
                        from school_data sd
                        join school_statuses ss using (school_status_id)
                        left join organizations o using (school_data_id)
                        left join organization_units ou USING (organization_id)
                        left join organization_bank_accounts oba using (organization_id)
                        left join users_to_organizations u2o USING (organization_id)
                        left join users u using (user_id)
                        left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id)
                        left join addresses a on (a.address_id = o.organization_default_address_id)
                        LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
                        where ( $searchTerms )
                        GROUP BY sd.school_data_id
                        ORDER BY sd.school_status_id DESC, `weight` DESC, sd.`Name` ASC
                        limit $startat, 30", $lookup);

        // todo confirm what order these should be in.  Active first or purely in alphabetical order?

        $data = ['data' => [], 'metrics' => []];
        while ($searchSchool = dbf($searchSchoolQuery)) {
            $data['data'][] = $searchSchool;
        }

        $rows = dbf(dbq("SELECT FOUND_ROWS() as totalRows")); // read how many total rows there are without LIMIT.
        $data['metrics']['totalRows'] = intval($rows['totalRows']);
        $data['metrics']['resultRows'] = sizeof($data['data']); // incase there are less than 30.

        return $data;
    }

    public function adduser()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['formData']) || empty($this->request['formData'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // Check if they exist first.
        $checkUserQuery = dbq("SELECT user_first_name, user_last_name, user_id, user_type_id FROM users WHERE user_email = :NEWEMAIL",
            [
                ':NEWEMAIL' => $this->request['formData']['email']
            ]
        );

        $orgdetails = getOrgDetails($this->request['formData']['organizationID']);

        if (dbnr($checkUserQuery) > 0) {
            // user exists
            $checkUser = dbf($checkUserQuery);

            // check they are not an admin.
            if ($checkUser['user_type_id'] != 1) {
                $data['type'] = 401;
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'User with this email cannot be added to any organizations.'
                ];
                return $data;
            }
            // Are they already on this org?
            $checkUserQuery = dbq("SELECT * FROM users_to_organizations WHERE user_id = :UID AND organization_id = :ORGID",
                [
                    ':UID' => $checkUser['user_id'],
                    ':ORGID' => $this->request['formData']['organizationID']
                ]
            );

            // check they are not already on this org.
            if (dbnr($checkUserQuery) > 0) {
                $data['type'] = 401;
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'User is already on this organization.'
                ];
                return $data;
            }

            // add them to this org and send an email.
            $insert = dbq('INSERT INTO users_to_organizations (
					user_id,
					organization_id,
					user_primary
				  ) VALUES (
				  	:UID,
				  	:ORGID,
				  	0
				  )',
                [
                    ':UID' => $checkUser['user_id'],
                    ':ORGID' => $this->request['formData']['organizationID']
                ]
            );

            $emailContent = "Dear " . $checkUser['user_first_name'] . "<br />
<br />
You have been added as a user on the F&F Uniforms account for " . $orgdetails['organization_name'] . "<br />
<br />
You can access this organization by logging in to your existing account and switching to the 'Linked Organizations' tab of the 'Uniform Selection' page.
<br />
<br />
Yours sincerely,<br />
<br />
The F&F Uniforms Team";


        } else {
            // New user. need setting up and a new password.

            $username = $this->request['formData']['firstname'] . ' ' . $this->request['formData']['lastname'];
            $newpass = generateRandomString();
            $newPassHash = password_hash($newpass, PASSWORD_BCRYPT);

            dbq("INSERT INTO users (
					user_id,
					user_type_id,
					user_default_address_id,
					user_name,
					user_first_name,
					user_last_name,
					user_email,
					user_password,
					user_avatar,
					user_work_phone,
					user_mobile_phone,
					user_home_phone,
					user_home_email,
					user_title_id,
					user_salutation
				  ) VALUES (
				  	:USERID,
					:USERTYPEID,
					:USERDEFAULTADDRESSID,
					:USERNAME,
					:USERFIRSTNAME,
					:USERLASTNAME,
					:USEREMAIL,
					:USERPASSWORD,
					:USERAVATAR,
					:USERWORKPHONE,
					:USERMOBILEPHONE,
					:USERHOMEPHONE,
					:USERHOMEEMAIL,
					:USERTITLEID,
					:USERSALUTATION
				  )",
                [
                    ':USERID' => null,
                    ':USERTYPEID' => 1,
                    ':USERDEFAULTADDRESSID' => null,
                    ':USERNAME' => $username,
                    ':USERFIRSTNAME' => $this->request['formData']['firstname'],
                    ':USERLASTNAME' => $this->request['formData']['lastname'],
                    ':USEREMAIL' => $this->request['formData']['email'],
                    ':USERPASSWORD' => $newPassHash,
                    ':USERAVATAR' => null,
                    ':USERWORKPHONE' => null,
                    ':USERMOBILEPHONE' => null,
                    ':USERHOMEPHONE' => null,
                    ':USERHOMEEMAIL' => null,
                    ':USERTITLEID' => null,
                    ':USERSALUTATION' => null
                ]
            );

            $userID = dbid();

            // add them to this org and send an email.
            $insert = dbq('INSERT INTO users_to_organizations (
					user_id,
					organization_id,
					user_primary
				  ) VALUES (
				  	:UID,
				  	:ORGID,
				  	0
				  )',
                [
                    ':UID' => $userID,
                    ':ORGID' => $this->request['formData']['organizationID']
                ]
            );

            $emailContent = "Dear " . $this->request['formData']['firstname'] . "<br />
<br />
You have been added as a user on the F&F Uniforms account for " . $orgdetails['organization_name'] . "<br />
<br />
We have set you up with an account and you can log in at <a href='http://www.ff-ues.com'>www.ff-ues.com</a> using the below details.<br />
<br />
email: " . $this->request['formData']['email'] . "<br />
pass: " . $newpass . "<br />
<br />
Please log in and update your contact details in the 'Your Profile' section of the 'Account' page.<br />
<br />
<br />
Yours sincerely,<br />
<br />
The F&F Uniforms Team";

        }

        $subject = 'Access to F&F Uniforms account';
        $title = $subject;
        $template = 'tesco-ff-uniforms';
        $tag = ['ff-add-user'];

        $toName = $this->request['formData']['firstname'] . " " . $this->request['formData']['lastname'];
        $emailAddress = [
            $toName => $this->request['formData']['email']
            //'Andrew Lindsay' => 'andy@slickstitch.com'
        ];

        $message = $emailContent;

        $template_content = array(
            [
                'name' => 'emailtitle',
                'content' => $title
            ],
            [
                'name' => 'main',
                'content' => $message
            ]
        );

        send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'User added to organization.'
        ];

        // update session

        $_SESSION['session']['users'][$this->request['formData']['organizationID']] = [];

        $orgUserDataQuery = dbq("SELECT u.*, u2o.user_primary FROM users u JOIN users_to_organizations u2o USING (user_id) WHERE u2o.organization_id = :ORGID", [':ORGID' => $this->request['formData']['organizationID']]);
        while ($orgUserData = dbf($orgUserDataQuery)) {
            unset($orgUserData['user_password']); // Don't want that out there.
            $_SESSION['session']['users'][$this->request['formData']['organizationID']][] = $orgUserData;
        }

        refreshSession();

        $data['session'] = $_SESSION['session'];

        return $data;
    }

    public function changeprimaryuser()
    {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['formData']) || empty($this->request['formData'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $masterOrgID = getMasterOrgID($this->request['formData']['organizationID']);
        // Check if they exist first.
        $checkUserQuery = dbq("SELECT * FROM users_to_organizations WHERE user_id = :USERID AND organization_id = :ORGID",
            [
                ':USERID' => $this->request['formData']['user']['user_id'],
                ':ORGID' => $masterOrgID
            ]
        );

        if (dbnr($checkUserQuery) > 0) {

            // remove them
            dbq("UPDATE users_to_organizations SET user_primary = 0 WHERE organization_id = :ORGID",
                [
                    ':ORGID' => $masterOrgID
                ]
            );

            dbq("UPDATE users_to_organizations SET user_primary = 1 WHERE user_id = :USERID AND organization_id = :ORGID",
                [
                    ':USERID' => $this->request['formData']['user']['user_id'],
                    ':ORGID' => $masterOrgID
                ]
            );

            $data['messages'][] = [
                'type' => 'success',
                'message' => 'User has been made primary contact.'
            ];

        } else {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'User is not part of this organization.'
            ];
            return $data;
        }

        $_SESSION['session']['users'][$this->request['formData']['organizationID']] = [];

        $orgUserDataQuery = dbq("SELECT u.*, u2o.user_primary FROM users u JOIN users_to_organizations u2o USING (user_id) WHERE u2o.organization_id = :ORGID", [':ORGID' => $masterOrgID]);
        while ($orgUserData = dbf($orgUserDataQuery)) {
            unset($orgUserData['user_password']); // Don't want that out there.
            $_SESSION['session']['users'][$this->request['formData']['organizationID']][] = $orgUserData;
        }

        $data['session'] = $_SESSION['session'];

        return $data;
    }

    public function deleteuser()
    {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['formData']) || empty($this->request['formData'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $masterOrgID = getMasterOrgID($this->request['formData']['organizationID']);

        // Check if they exist first.
        $checkUserQuery = dbq("SELECT * FROM users_to_organizations WHERE user_id = :USERID AND organization_id = :ORGID",
            [
                ':USERID' => $this->request['formData']['user']['user_id'],
                ':ORGID' => $masterOrgID
            ]
        );

        if (dbnr($checkUserQuery) > 0) {
            // check they are not the only one
            $checkUserQuery = dbq("SELECT * FROM users_to_organizations WHERE organization_id = :ORGID",
                [
                    ':ORGID' => $masterOrgID
                ]
            );

            if (dbnr($checkUserQuery) > 1) {
                // remove them
                dbq("DELETE FROM users_to_organizations WHERE user_id = :USERID AND organization_id = :ORGID",
                    [
                        ':USERID' => $this->request['formData']['user']['user_id'],
                        ':ORGID' => $masterOrgID
                    ]
                );

                // are they on any other orgs?  If not remove the whole account.
                $checkUserOtherQuery = dbq("SELECT * FROM users_to_organizations WHERE user_id = :USERID",
                    [
                        ':USERID' => $this->request['formData']['user']['user_id'],
                    ]
                );

                $data['messages'][] = [
                    'type' => 'success',
                    'message' => 'User has been removed from this organization.'
                ];

                if (dbnr($checkUserOtherQuery) < 1) {
                    // remove the user completely.
                    dbq("DELETE FROM users WHERE user_id = :USERID",
                        [
                            ':USERID' => $this->request['formData']['user']['user_id'],
                        ]
                    );
                    $data['messages'][] = [
                        'type' => 'info',
                        'message' => 'User was not a member of any other organizations so has been removed.'
                    ];
                }


            } else {
                $data['type'] = 401;
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'Cannot remove the only user.'
                ];
                return $data;
            }

        } else {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'User is not part of this organization.'
            ];
            return $data;
        }

        $_SESSION['session']['users'][$this->request['formData']['organizationID']] = [];

        $orgUserDataQuery = dbq("SELECT u.*, u2o.user_primary FROM users u JOIN users_to_organizations u2o USING (user_id) WHERE u2o.organization_id = :ORGID", [':ORGID' => $masterOrgID]);
        while ($orgUserData = dbf($orgUserDataQuery)) {
            unset($orgUserData['user_password']); // Don't want that out there.
            $_SESSION['session']['users'][$this->request['formData']['organizationID']][] = $orgUserData;
        }

        $data['session'] = $_SESSION['session'];

        return $data;
    }

    /**
     * Product Association endpoint.  Search registered schools products.
     * @return array
     */

    public function listproducts()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // Check school is live.

        $orgLiveCheck = getOrgDetails($this->request['orgid']);

        if($orgLiveCheck['organization_status_id'] != 3){
            // ALERT WILL ROBINSON!
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $showarray = [
             2,3,4,5,6,7,10,122,123,124,125,126,150,151,152,31,32,75,111,155,158,163,164,165,166,168,170,171,173,197,
            122
        ];
		
		$hasNameLabels = false;

        $searchOrganization = $this->request['orgid'];

        /*p.product_sku, p.product_name, ol.tesco_style_ref*/
        $data = [
            'count' => 0,
            'skus' => []
        ];

        $productQuery = dbq("SELECT p.* FROM products p
								JOIN product_colours pc using (product_id)
								JOIN colours using (colour_id)
								JOIN products_colours_to_organizations pc2o using (product_colour_id)
								WHERE
								pc.product_colour_active = 1
								and
								pc2o.organization_id = :ORGID
								AND p.product_active = 1
								group by p.product_id",[
                ':ORGID' => $searchOrganization
            ]
        ); // dont want to see the test products

        $altSKUs = [];

        while ($product = dbf($productQuery)) {

            // get colours here?
            $coloursQuery = dbq("select pc2o.products_colours_to_organizations_id, pc.*, c.*, ol.`organization_logo_id`, ol.`tesco_style_ref` , ol.`logo_url`  from product_colours pc
									join colours c using (colour_id)
									JOIN products_colours_to_organizations pc2o using (product_colour_id)
									LEFT JOIN organization_logos ol ON (ol.organization_logo_id = pc2o.`organization_logo_id` AND ol.logo_deleted != 1)
									where
									pc.product_colour_active = 1
									and
									pc2o.organization_id = :ORGID
									and
									product_id = :PRODID",
                [
                    ':ORGID' => $searchOrganization,
                    ':PRODID' => $product['product_id']
                ]
            );

            while ($colours = dbf($coloursQuery)) {
                // need each size
                // get sizes here
                $sizesQuery = dbq("select * from products_sizes_to_products_colours ps2pc
                                    join product_sizes ps on (ps.product_size_id = ps2pc.product_size_id)
									where ps2pc.product_colour_id = :COLOURID",
                    [
                        ':COLOURID' => $colours['product_colour_id']
                    ]
                );

                if(!is_null($colours['product_colour_gmo_ref'])){

                    if($searchOrganization == 10266){

                        if(in_array($product['product_id'],$showarray)){
							
							if($colours['product_colour_id'] == 292){
								$hasNameLabels = true;
							}
							
                            $data['skus'][] = [
                                'product_sku' => trim($colours['product_colour_gmo_ref']),
                                'product_name' => $product['product_name'].' ('.$colours['colour_name'].')',
                                'emblem' => trim($colours['tesco_style_ref']),
                                'emblem_position' => trim($product['product_emblem_position'])


                            ];
                        }

                    } else {
                        $data['skus'][] = [
                            'product_sku' => trim($colours['product_colour_gmo_ref']),
                            'product_name' => $product['product_name'].' ('.$colours['colour_name'].')',
                            'emblem' => trim($colours['tesco_style_ref']),
                            'emblem_position' => trim($product['product_emblem_position'])
                        ];



                        if(!empty($product['product_alt_ref'])){
                            // include the alternative;
                            $altSKUs[trim($colours['product_colour_gmo_ref'])] = [
                                'product' => $product,
                                'colours' => $colours
                            ];
                        }
                    }

                }

            }

        }

        foreach($altSKUs as $CATID => $altSKU){
            // select the alt product.
            $altProdQuery = dbq("SELECT p.*, pc.*, c.* FROM products p
                                JOIN product_colours pc on pc.product_id = p.product_id
                                join colours c on c.colour_id = pc.colour_id
								WHERE p.product_id = :PRODID
								AND pc.colour_id = :PCID
								AND p.product_active = 1",
                [
                    ':PRODID' => $altSKU['product']['product_alt_ref'],
                    ':PCID' => $altSKU['colours']['colour_id']
                ]
            );

            while($altProd = dbf($altProdQuery)){
                // This should get everything we need.
                // Check its not already set.
                // return [$altSKU, $altProd];
                if(!isset($altSKUs[$altProd['product_colour_gmo_ref']])){
                    // Not already added, lets add it in.
                    $data['skus'][] = [
                        'product_sku' => trim($altProd['product_colour_gmo_ref']),
                        'product_name' => $altProd['product_name'].' ('.$altProd['colour_name'].')',
                        'emblem' => trim($altSKU['colours']['tesco_style_ref']),
                        'emblem_position' => trim($product['product_emblem_position'])
                    ];
                }
            }
        }

		// Add name tapes 292
		if(!$hasNameLabels){
			$data['skus'][] = [
                            'product_sku' => trim('279-0655'),
                            'product_name' => 'Name Labels (White)',
                            'emblem' => null,
                            'emblem_position' => null
                        ];
		}
		
        $data['count'] = count($data['skus']);

        return $data;
    }

    public function listproductsold()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $searchOrganization = $this->request['orgid'];

        /*p.product_sku, p.product_name, ol.tesco_style_ref*/
        $data = [
            'count' => 0,
            'skus' => []
        ];

        $productQuery = dbq("SELECT p.* FROM products p
								JOIN product_colours pc using (product_id)
								JOIN colours using (colour_id)
								JOIN products_colours_to_organizations pc2o using (product_colour_id)
								WHERE pc2o.organization_id = :ORGID
								AND p.product_active = 1
								group by p.product_id",[
                ':ORGID' => $searchOrganization
            ]
        ); // dont want to see the test products
      //  $seenSKUs = [];
        while ($product = dbf($productQuery)) {

            // get colours here?
            $coloursQuery = dbq("select pc2o.products_colours_to_organizations_id, pc.*, c.*, ol.`organization_logo_id`, ol.`tesco_style_ref` , ol.`logo_url`  from product_colours pc
									join colours c using (colour_id)
									JOIN products_colours_to_organizations pc2o using (product_colour_id)
									LEFT JOIN organization_logos ol ON (ol.organization_logo_id = pc2o.`organization_logo_id` AND ol.logo_deleted != 1)
									where pc2o.organization_id = :ORGID
									and
									product_id = :PRODID",
                [
                    ':ORGID' => $searchOrganization,
                    ':PRODID' => $product['product_id']
                ]
            );

            while ($colours = dbf($coloursQuery)) {
                // need each size
                // get sizes here
                $sizesQuery = dbq("select * from products_sizes_to_products_colours ps2pc
                                    join product_sizes ps on (ps.product_size_id = ps2pc.product_size_id)
									where ps2pc.product_colour_id = :COLOURID",
                    [
                        ':COLOURID' => $colours['product_colour_id']
                    ]
                );


                while ($sizes = dbf($sizesQuery)) {
                  //  if(!in_array($sizes['product_variation_gmo_sku'],$seenSKUs)){
                        $data['skus'][] = [
                            'product_sku' => $sizes['product_variation_gmo_sku'],
                            'product_name' => $product['product_name'].' ('.$colours['colour_name'].') '.$sizes['products_size_name'],
                            'emblem' => $colours['tesco_style_ref']
                        ];
                  //  }
                  //  $seenSKUs[$sizes['product_variation_gmo_sku']] = $sizes['product_variation_gmo_sku'];
                }
            }

        }

        $data['count'] = count($data['skus']);

        return $data;
    }

    /**
     * Get an organizations EMBROIDERED products,
     * @return array
     */

    public function getorgproducts()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $productQuery = dbq("SELECT p.* FROM products p
								JOIN product_colours pc USING (product_id)
								JOIN colours USING (colour_id)
								JOIN products_colours_to_organizations pc2o USING (product_colour_id)
								WHERE pc2o.organization_id = :ORGID
								AND p.product_active = 1
								AND pc.product_colour_active = 1
								AND p.product_decorated = 1
								AND p.product_test_only = 0
								GROUP BY p.product_id", [
                ':ORGID' => $this->request['orgid']
            ]
        ); // dont want to see the test products

        $productArray = [];
        $altProductArray = [];
        while ($product = dbf($productQuery)) {

            $product['product_image'] = str_replace("/small/", "/medium/", $product['product_image']);

            // get colours here?
            $coloursQuery = dbq("SELECT pc2o.products_colours_to_organizations_id, pc.*, c.*, ol.`organization_logo_id`, ol.`tesco_style_ref` , ol.`logo_url`, ol.`logo_last_updated` FROM product_colours pc
									JOIN colours c USING (colour_id)
									JOIN products_colours_to_organizations pc2o USING (product_colour_id)
									JOIN organization_logos ol USING (organization_logo_id)
									WHERE
									pc.product_colour_active = 1
									and
									pc2o.organization_id = :ORGID
									AND
									product_id = :PRODID",
                [
                    ':ORGID' => $this->request['orgid'],
                    ':PRODID' => $product['product_id']
                ]
            );

            while ($colours = dbf($coloursQuery)) {
                $altProductArray[$colours['products_colours_to_organizations_id']] = $product;
                //product_colour_image_url
                $colours['product_colour_image_url'] = str_replace("/small/", "/medium/", $colours['product_colour_image_url']);
                $altProductArray[$colours['products_colours_to_organizations_id']]['colour'] = $colours;
            }

        }

        $data['products'] = $altProductArray;

        return $data;

    }

    /**
     * Get an organizations PLAIN products,
     * @return array
     */

    public function getorgplainproducts()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $productQuery = dbq("SELECT p.* FROM products p
								JOIN product_colours pc USING (product_id)
								JOIN colours USING (colour_id)
								JOIN products_colours_to_organizations pc2o USING (product_colour_id)
								WHERE pc2o.organization_id = :ORGID
								AND p.product_active = 1
								AND p.product_decorated = 0
								AND p.product_test_only = 0
								GROUP BY p.product_id", [
                ':ORGID' => $this->request['orgid']
            ]
        ); // dont want to see the test products

        $altProductArray = [];
        while ($product = dbf($productQuery)) {

            $product['product_image'] = str_replace("/small/", "/medium/", $product['product_image']);

            // get colours here?
            $coloursQuery = dbq("SELECT pc2o.products_colours_to_organizations_id, pc.*, c.* FROM product_colours pc
									JOIN colours c USING (colour_id)
									JOIN products_colours_to_organizations pc2o USING (product_colour_id)
									WHERE pc2o.organization_id = :ORGID
									AND
									product_id = :PRODID",
                [
                    ':ORGID' => $this->request['orgid'],
                    ':PRODID' => $product['product_id']
                ]
            );

            while ($colours = dbf($coloursQuery)) {
                $altProductArray[$colours['products_colours_to_organizations_id']] = $product;
                $colours['product_colour_image_url'] = str_replace("/small/", "/medium/", $colours['product_colour_image_url']);
                $altProductArray[$colours['products_colours_to_organizations_id']]['colour'] = $colours;
            }


        }

        $data['products'] = $altProductArray;

        return $data;

    }

    /**
     * Get an organizations emblems,
     * @return array
     */

    public function getorgemblems()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // house?
        $masterID = getMasterOrgID($this->request['orgid']);

        //firefly $masterOrgID = getMasterOrgID($orgID); // house?

        $mastercheck =(int)$masterID;
        $parsecheck = (int)$this->request['orgid'];


        if($mastercheck !== $parsecheck) {
            $masterID = $parsecheck;
        }



        $logoQuery = dbq("SELECT * FROM organization_logos
								WHERE organization_id = :ORGID
								 AND logo_deleted != 1", [
                ':ORGID' => $masterID
            ]
        ); // dont want to see the test products

        $logoArray = [];
        while ($logo = dbf($logoQuery)) {

            //$product['product_image'] = str_replace("/small/","/medium/",$product['product_image']);

            $logoArray[] = $logo;
            //$logoArray[$product['product_id']]['colours'] = [];

        }
        $data['emblems'] = $logoArray;

        return $data;

    }

    /**
     * Get an organizations emblems,
     * @return array
     */

    public function getorgdonations()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // house?
        $masterID = getMasterOrgID($this->request['orgid']);

        $donationQuery = dbq("SELECT * FROM donations
								WHERE organization_id = :ORGID", [
                ':ORGID' => $masterID
            ]
        ); // dont want to see the test products

        $donationArray = [];
        while ($donation = dbf($donationQuery)) {
            foreach ($donation as $key => $value) {
                if (stristr($key, "date")) {
                    $donation[$key] = strtotime($value) * 1000;
                }
            }
            //$product['product_image'] = str_replace("/small/","/medium/",$product['product_image']);

            $donationArray[] = $donation;
            //$logoArray[$product['product_id']]['colours'] = [];

        }

        $data['donations'] = $donationArray;

        return $data;

    }

    /**
     * Basket check endpoint.  Search registered schools products.
     * @return array
     */

    public function checkbasket()
    {

        // todo - fix for name tapes.
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['organizations'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200
        ];

        foreach ($this->request['organizations'] as $organization) {
            $tmpdata = [];
            $searchOrganization = $organization['orgid'];
            $basketContent = $organization['logos'];
            $success = true;
            $lookupProductsQuery = dbq("SELECT p.product_sku, p.product_name, ol.tesco_style_ref AS emblem FROM organizations o
									JOIN products_to_organizations p2o USING (organization_id)
									JOIN products p ON (p.product_id = p2o.product_id)
									JOIN organization_logos ol ON (ol.organization_logo_id = p2o.organization_logo_id)
									WHERE
									o.organization_id = :ORGID", [':ORGID' => $searchOrganization]);

            $valid = [];
            while ($lookupProducts = dbf($lookupProductsQuery)) {
                $valid[$lookupProducts['emblem']][] = $lookupProducts['product_sku'];
            }
            $invalid = null;

            $replyCount = 0;
            // loop through what we were given and check it exists as valid.
            foreach ($basketContent as $logoIndex => $logodetail) {


                if (!isset($valid[$logodetail['logoref']])) {

                    // Logo its self is invalid so all the contents will be too.

                    $invalid['logos'][] = [
                        'logoref' => $logodetail['logoref'],
                        'skus' => $logodetail['skus']
                    ];

                } else {
                    // loop through the skus.
                    $tempinvalidKey = '';
                    $tempinvalid = [];
                    foreach ($logodetail['skus'] as $sku) {

                        if (!in_array($sku, $valid[$logodetail['logoref']])) {
                            // invalid
                            $success = false;

                            $tempinvalidKey = $logodetail['logoref'];
                            $tempinvalid[] = $sku;

                        }
                    }
                    // add it here if needs it.
                    if (!empty($tempinvalid)) {
                        $invalid['logos'][] = [
                            'logoref' => $tempinvalidKey,
                            'skus' => $tempinvalid
                        ];
                    }

                }
            }

            if ($invalid['logos'] == $basketContent) {
                $invalid = null;
            }
            // Get the product list from the database, make sure the skus are in there.


            $tmpdata['valid'] = $success;
            $tmpdata['orgid'] = $searchOrganization;
            $tmpdata['exceptions'] = $invalid;

            $data['organizations'][] = $tmpdata;

        }

        return $data;
    }

    /**
     * URN Creation.  Get the next URN ID for the specified org type.
     * @return string
     */

    private function getNextURN($type)
    {

        $URNQuery = dbq("SELECT * FROM configuration WHERE config_key = 'NEXT_URN'");
        $URN = dbf($URNQuery);
        dbq("UPDATE configuration SET config_value = config_value+1 WHERE config_key = 'NEXT_URN'");

        switch ($type) {
            case 1: // school.
                //990001267
                $newURN = sprintf("99%07d", $URN['config_value']);
                break;
            case 2:
                //890000623
                $newURN = sprintf("89%07d", $URN['config_value']);
                break;
            case 3:
                //890000623
                $newURN = sprintf("79%07d", $URN['config_value']);
                break;
            case 4:
                //890000623
                $newURN = sprintf("69%07d", $URN['config_value']);
                break;
            case 5:
                //890000623
                $newURN = sprintf("59%07d", $URN['config_value']);
                break;
            case 6:
                //890000623
                $newURN = sprintf("49%07d", $URN['config_value']);
                break;
            default: // other
                //890000623
                $newURN = sprintf("89%07d", $URN['config_value']);
                break;
        }
        return $newURN;
    }

    public function getorgdonationbank()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgid']) || empty($this->request['orgid'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200,
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on a page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // Check this bank account belongs to this orgid.
        $bankOrgCheck = dbq("SELECT * FROM organization_bank_accounts WHERE organization_id = :ORGID",
            [
                ':ORGID' => $this->request['orgid']
            ]
        );

        if (dbnr($bankOrgCheck) > 0) {
            $bankOrg = dbf($bankOrgCheck);
            $data['data'] = [
                'saved' => true,
                'accountName' => $bankOrg['organization_bank_name'],
                'accountNumber' => $bankOrg['organization_bank_account'],
                'SortCode' => [
                    $bankOrg['organization_bank_sort_code_1'],
                    $bankOrg['organization_bank_sort_code_2'],
                    $bankOrg['organization_bank_sort_code_3']
                ]
            ];
        } else {
            $data['type'] = 404;
            return $data;
        }

        return $data;
    }

    public function registerOrganization()
    {
        // Function to register a new user

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        $data = [];

        if (!isset($this->request['formdata']) || empty($this->request['formdata'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // Check email doesnt exist again.
        $emailQuery = dbq("SELECT * FROM users WHERE user_email = :EMAIL", [':EMAIL' => $this->request['formdata']['emailaddress']]);

        if (dbnr($emailQuery) > 0) {
            $this->setError([
                'message' => 'User exists.',
                'errorCode' => 'user-exists',
            ]);
            return $this->getResponse(500);
        }

        // did they refer a friend?
        if(isset($this->request['formdata']['rafid']) && !empty($this->request['formdata']['rafid'])){
            // Check they exist.
            $rafQuery = dbq("SELECT * FROM organizations WHERE school_URN = :REFURN",
                [
                    ':REFURN' => $this->request['formdata']['rafid']
                ]
            );

            if (dbnr($rafQuery) == 0) {
                $this->setError([
                    'message' => 'Refer a friend organization doesnt exist.',
                    'errorCode' => 'raf-non-exist',
                ]);
                return $this->getResponse(500);
            }
        }


        // check org isnt already setup.

        $orgURN = null;

        $schoolDataID = false;

        if (!isset($this->request['formdata']['orglea'])) {
            $this->request['formdata']['orglea'] = 219; // Other
        }

        if (!isset($this->request['formdata']['orgphase'])) {
            $this->request['formdata']['orgphase'] = 11; // Other
        }

        if (!isset($this->request['formdata']['orgurl'])) {
            $this->request['formdata']['orgurl'] = '';
        }
        // ok so make a school and attach it to the URNID if exists.
        if (isset($this->request['formdata']['orgurn']) && !empty($this->request['formdata']['orgurn'])) {

            $existsQuery = dbq("select organization_id from organizations o where o.school_URN = :URN",
                [
                    ':URN' => $this->request['formdata']['orgurn']
                ]
            );

            if(dbnr($existsQuery) > 0){
                // this exists already!
                $this->setError([
                    'message' => 'This school is already registered.  please log in to your existing account.',
                    'errorCode' => 'school-doesnt-exist',
                ]);
                return $this->getResponse(500);
            }

            // an organization should be there in the schools table.  lets check.
            $urnQuery = dbq("SELECT school_data_id, URN FROM school_data WHERE URN = :URN", [':URN' => $this->request['formdata']['orgurn']]);

            if (dbnr($urnQuery) > 0) {
                // It exists so we can use it.
                // need to add an organization, user and link to school_data
                $orgURN = $this->request['formdata']['orgurn'];
                $urnData = dbf($urnQuery);
                $schoolDataID = $urnData['school_data_id'];
            } else {
                $this->setError([
                    'message' => 'Cant find school in school table.',
                    'errorCode' => 'school-doesnt-exist',
                ]);
                return $this->getResponse(500);
            }

        } else {
            // need to create a school entry in school_data table.
            $orgURN = $this->getNextURN($this->request['formdata']['orgtype']);

            dbq("INSERT INTO school_data (
                                `id118`,
                                `HEADEMAILDRIECT`,
                                `URN`,
                                `Name`,
                                `LEAName`,
                                `SchoolType`,
                                `Denomination`,
                                `Address1`,
                                `Address2`,
                                `Address3`,
                                `Town`,
                                `County`,
                                `Area`,
                                `Postcode`,
                                `PostcodeSubArea`,
                                `Phone`,
                                `Fax`,
                                `URL`,
                                `Email`,
                                `Emaill118Direct`,
                                `Pupils`,
                                `School Capacity`,
                                `LA`,
                                `Phase of Education`,
                                `Type of Establishment`,
                                `NumberOfPupilsTakingFreeSchoolMeals`,
                                `Nominations`,
                                `school_status_id`
                             ) VALUES (
                                :ID118,
                                :HEADEMAILDRIECT,
                                :URN,
                                :NAME,
                                :LEANAME,
                                :SCHOOLTYPE,
                                :DENOMINATION,
                                :ADDRESS1,
                                :ADDRESS2,
                                :ADDRESS3,
                                :TOWN,
                                :COUNTY,
                                :AREA,
                                :POSTCODE,
                                :POSTCODESUBAREA,
                                :PHONE,
                                :FAX,
                                :URL,
                                :EMAIL,
                                :EMAILL118DIRECT,
                                :PUPILS,
                                :SCHOOLCAPACITY,
                                :LA,
                                :PHASEOFEDUCATION,
                                :TYPEOFESTABLISHMENT,
                                :NUMBEROFPUPILSTAKINGFREESCHOOLMEALS,
                                :NOMINATIONS,
                                :SCHOOL_STATUS_ID
                             )",
                [
                    ':ID118' => null,
                    ':HEADEMAILDRIECT' => 0,
                    ':URN' => $orgURN,
                    ':NAME' => $this->request['formdata']['orgname'],
                    ':LEANAME' => getLEAName($this->request['formdata']['orglea']),
                    ':SCHOOLTYPE' => getSchoolType($this->request['formdata']['orgtype']),
                    ':DENOMINATION' => null,
                    ':ADDRESS1' => $this->request['formdata']['orgaddress1'],
                    ':ADDRESS2' => $this->request['formdata']['orgaddress2'],
                    ':ADDRESS3' => $this->request['formdata']['orgaddress3'],
                    ':TOWN' => $this->request['formdata']['orgtown'],
                    ':COUNTY' => $this->request['formdata']['orgcounty'],
                    ':AREA' => null,
                    ':POSTCODE' => $this->request['formdata']['orgpostcode'],
                    ':POSTCODESUBAREA' => $this->request['formdata']['orgpostcode'],
                    ':PHONE' => $this->request['formdata']['orgphone'],
                    ':FAX' => null,
                    ':URL' => $this->request['formdata']['orgurl'],
                    ':EMAIL' => null,
                    ':EMAILL118DIRECT' => null,
                    ':PUPILS' => $this->request['formdata']['orgmembercount'],
                    ':SCHOOLCAPACITY' => null,
                    ':LA' => null,
                    ':PHASEOFEDUCATION' => getEduPhase($this->request['formdata']['orgphase']),
                    ':TYPEOFESTABLISHMENT' => null,
                    ':NUMBEROFPUPILSTAKINGFREESCHOOLMEALS' => null,
                    ':NOMINATIONS' => null,
                    ':SCHOOL_STATUS_ID' => 4
                ]);

            $schoolDataID = dbid();
        }

        if (!$schoolDataID) {
            // Something went wrong
            $this->setError([
                'message' => 'School data id not set.',
                'errorCode' => 'school-data-error',
            ]);
            return $this->getResponse(500);
        }

        // Should have a school URN now and school_data_id so can add the organization.

        // did they refer a friend?
        if(isset($this->request['formdata']['rafid']) && !empty($this->request['formdata']['rafid'])){
            // we know it exists as there would have been an error otherwise.
            $rafID = getOrgIDFromURN($this->request['formdata']['rafid']);
        } else {
            $rafID = null;
        }

        dbq("INSERT INTO organizations (
					organization_id,
					organization_type_id,
					organization_name,
					organization_member_count,
					organization_default_address_id,
					organization_telephone,
					organization_fax,
					organization_email,
					organization_url,
					school_data_id,
					school_local_authority_id,
					school_education_phase_id,
					organization_default_logo_id,
					organization_funds,
					organization_status_id,
					school_URN,
					organization_has_subunits,
					organization_parent_id,
					logos_imported,
					organization_registered_date,
					organization_raf_organization_id
				  ) VALUES (
				  	:ORGANIZATIONID,
					:ORGANIZATIONTYPEID,
					:ORGANIZATIONNAME,
					:ORGANIZATIONMEMBERCOUNT,
					:ORGANIZATIONDEFAULTADDRESSID,
					:ORGANIZATIONTELEPHONE,
					:ORGANIZATIONFAX,
					:ORGANIZATIONEMAIL,
					:ORGANIZATIONURL,
					:SCHOOLDATAID,
					:SCHOOLLOCALAUTHORITYID,
					:SCHOOLEDUCATIONPHASEID,
					:ORGANIZATIONDEFAULTLOGOID,
					:ORGANIZATIONFUNDS,
					:ORGANIZATIONSTATUSID,
					:SCHOOLURN,
					:ORGANIZATIONHASSUBUNITS,
					:ORGANIZATIONPARENTID,
					:LOGOSIMPORTED,
					NOW(),
					:ORGANIZATIONRAFORGANIZATIONID
				  )",
            [
                ':ORGANIZATIONID' => null,
                ':ORGANIZATIONTYPEID' => $this->request['formdata']['orgtype'],
                ':ORGANIZATIONNAME' => $this->request['formdata']['orgname'],
                ':ORGANIZATIONMEMBERCOUNT' => $this->request['formdata']['orgmembercount'],
                ':ORGANIZATIONDEFAULTADDRESSID' => null,
                ':ORGANIZATIONTELEPHONE' => $this->request['formdata']['orgphone'],
                ':ORGANIZATIONFAX' => null,
                ':ORGANIZATIONEMAIL' => $this->request['formdata']['emailaddress'],
                ':ORGANIZATIONURL' => $this->request['formdata']['orgurl'],
                ':SCHOOLDATAID' => $schoolDataID,
                ':SCHOOLLOCALAUTHORITYID' => $this->request['formdata']['orglea'],
                ':SCHOOLEDUCATIONPHASEID' => $this->request['formdata']['orgphase'],
                ':ORGANIZATIONDEFAULTLOGOID' => null,
                ':ORGANIZATIONFUNDS' => 0,
                ':ORGANIZATIONSTATUSID' => 4,
                ':SCHOOLURN' => $orgURN,
                ':ORGANIZATIONHASSUBUNITS' => 0,
                ':ORGANIZATIONPARENTID' => null,
                ':LOGOSIMPORTED' => null,
                ':ORGANIZATIONRAFORGANIZATIONID' => $rafID
            ]
        );

        $organizationID = dbid();

        // Add an address.
        dbq("INSERT INTO addresses (
					address_id,
					entry_gender,
					entry_company,
					entry_firstname,
					entry_lastname,
					entry_street_address,
					entry_street_address_2,
					entry_city,
					entry_suburb,
					entry_state,
					entry_postcode,
					entry_country_id,
					user_id,
					organization_id
				  ) VALUES (
				  	:ADDRESSID,
					:ENTRYGENDER,
					:ENTRYCOMPANY,
					:ENTRYFIRSTNAME,
					:ENTRYLASTNAME,
					:ENTRYSTREETADDRESS,
					:ENTRYSTREETADDRESS2,
					:ENTRYCITY,
					:ENTRYSUBURB,
					:ENTRYSTATE,
					:ENTRYPOSTCODE,
					:ENTRYCOUNTRYID,
					:USERID,
					:ORGANIZATIONID
				  )",
            [
                ':ADDRESSID' => null,
                ':ENTRYGENDER' => null,
                ':ENTRYCOMPANY' => $this->request['formdata']['orgname'],
                ':ENTRYFIRSTNAME' => $this->request['formdata']['forename'],
                ':ENTRYLASTNAME' => $this->request['formdata']['surname'],
                ':ENTRYSTREETADDRESS' => $this->request['formdata']['orgaddress1'],
                ':ENTRYSTREETADDRESS2' => $this->request['formdata']['orgaddress2'],
                ':ENTRYCITY' => $this->request['formdata']['orgtown'],
                ':ENTRYSUBURB' => $this->request['formdata']['orgaddress3'],
                ':ENTRYSTATE' => $this->request['formdata']['orgcounty'],
                ':ENTRYPOSTCODE' => $this->request['formdata']['orgpostcode'],
                ':ENTRYCOUNTRYID' => $this->request['formdata']['orgcountry'],
                ':USERID' => null,
                ':ORGANIZATIONID' => $organizationID
            ]
        );

        $addressID = dbid();

        // assign address to org
        dbq("UPDATE organizations SET organization_default_address_id = :ADDRESSID WHERE organization_id = :ORGID",
            [
                ':ADDRESSID' => $addressID,
                ':ORGID' => $organizationID
            ]
        );

        // create a user

        $username = $this->request['formdata']['forename'] . ' ' . $this->request['formdata']['surname'];
        $newpass = generateRandomString();
        $newPassHash = password_hash($newpass, PASSWORD_BCRYPT);

        dbq("INSERT INTO users (
					user_id,
					user_type_id,
					user_default_address_id,
					user_name,
					user_first_name,
					user_last_name,
					user_email,
					user_password,
					user_avatar,
					user_work_phone,
					user_mobile_phone,
					user_home_phone,
					user_home_email,
					user_title_id,
					user_salutation
				  ) VALUES (
				  	:USERID,
					:USERTYPEID,
					:USERDEFAULTADDRESSID,
					:USERNAME,
					:USERFIRSTNAME,
					:USERLASTNAME,
					:USEREMAIL,
					:USERPASSWORD,
					:USERAVATAR,
					:USERWORKPHONE,
					:USERMOBILEPHONE,
					:USERHOMEPHONE,
					:USERHOMEEMAIL,
					:USERTITLEID,
					:USERSALUTATION
				  )",
            [
                ':USERID' => null,
                ':USERTYPEID' => 1,
                ':USERDEFAULTADDRESSID' => $addressID,
                ':USERNAME' => $username,
                ':USERFIRSTNAME' => $this->request['formdata']['forename'],
                ':USERLASTNAME' => $this->request['formdata']['surname'],
                ':USEREMAIL' => $this->request['formdata']['emailaddress'],
                ':USERPASSWORD' => $newPassHash,
                ':USERAVATAR' => null,
                ':USERWORKPHONE' => $this->request['formdata']['telephone'],
                ':USERMOBILEPHONE' => null,
                ':USERHOMEPHONE' => null,
                ':USERHOMEEMAIL' => null,
                ':USERTITLEID' => $this->request['formdata']['regtitle'],
                ':USERSALUTATION' => null
            ]
        );

        $userID = dbid();

        // insert into users to organizations.#
        dbq("INSERT INTO users_to_organizations (
				user_id,
				organization_id,
				user_primary
			  ) VALUES (
			  	:USERID,
				:ORGANIZATIONID,
				:USERPRIMARY
			  )",
            [
                ':USERID' => $userID,
                ':ORGANIZATIONID' => $organizationID,
                ':USERPRIMARY' => 1
            ]
        );


        // send them an email.
        //ues_mail($username,$this->request['formdata']['emailaddress'],'Welcome','Welcome to F&F Uniforms,  Your new password Is: '.$newpass."\n\n",'F&F Uniforms','noreply@ff-ues.com');
        //ues_mail($username,'artworkfiles@gmail.com','Welcome','Welcome to F&F Uniforms,  Your new password Is: '.$newpass."\n\n.You can now log in.\n\n",'F&F Uniforms','noreply@ff-ues.com');

        $subject = 'New Account on F&F Uniforms';
        $title = $subject;
        $template = 'tesco-ff-uniforms';
        $tag = ['ff-registration'];
        $email_address = $this->request['formdata']['emailaddress'];
        $toName = $this->request['formdata']['forename'] . " " . $this->request['formdata']['surname'];

        $html = "There has been a new registration on the F&F Uniforms site.<br />
						<br />
						<b style=\"color:red;\">LOGO REQUEST EMAIL SENT DIRECTLY</b>
						<br />
						Name: $toName<br />
						Email: $email_address<br />
						<b>Organization Details</b><br />
						URN: {$orgURN}<br />
						Type: " . getSchoolType($this->request['formdata']['orgtype']) . "<br />
						Name: {$this->request['formdata']['orgname']}<br />
						Members: {$this->request['formdata']['orgmembercount']}<br />
						Telephone: {$this->request['formdata']['orgphone']}<br />
						Email Address: {$this->request['formdata']['emailaddress']}<br />
						Website: {$this->request['formdata']['orgurl']}<br />
						<br />
						<br />
						<br />
						Yours sincerely,<br />
						<br />
						The F&F Uniforms Team";


        $emailAddress = [
            'F&F Uniform Support' => 'ues@uniformembroideryservice.com',
            'Andrew Lindsay' => 'andy@slickstitch.com'
        ];

        $template_content = array(
            [
                'name' => 'emailtitle',
                'content' => $title
            ],
            [
                'name' => 'main',
                'content' => $html
            ]
        );

        // Attach price list.
        $attachmentArray = [];

        $attachmentArray[] = [
            'type' => "application/pdf",
            'name' => 'UES_2018_June_Price_List.pdf',
            'content' => base64_encode(file_get_contents(API_ROOT .'UES_2018_June_Price_List.pdf'))
        ];

        send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);

        // Custom email for user.
        $subject = 'Your new account on F&F Uniforms';
        $title = $subject;
        $template = 'tesco-ff-uniforms-new-account';
        $tag = ['ff-registration'];
        $email_address = $this->request['formdata']['emailaddress'];
        $toName = $this->request['formdata']['forename'] . " " . $this->request['formdata']['surname'];

        $userEmailAddress = [
            $toName => $email_address
        ];

        $user_template_content = array(
            [
                'name' => 'emailtitle',
                'content' => $title
            ],
            [
                'name' => 'persontitle',
                'content' => $toName
            ]
        );

        send_ues_mail($userEmailAddress, $subject, $user_template_content, $template, $tag, $attachmentArray);

        // RequestData
        /*
         formdata
            Object {
                orgname: "yurtutrurt"
                orgtype: "1"
                orgurl:	"http://www.erwtert.tter"
                orgphone:	"555 555 5555"
                orgmembercount: "123"
                orgaddress1: "rfdsfgsdf"
                orgaddress2: "sdfgsdfg"
                orgaddress3: "sdfgsdfg"
                orgtown: "sdfgsdfg"
                orgcounty: "sdfgsdf"
                orgpostcode: "gsdfgsdfg"
                orgcountry: "sdfgsdfg"
                regtitle: "0"
                forename: "sdfgsdfgsdfg"
                surname: "sdfgsdfg"
                jobrole: "sdfgsdfgsdfg"
                telephone: "555 555 55555"
                emailaddress: "asdfsad@asdfasdf.com"
                rafid: "35345"
                checkbox1:	"1"
            }
        */

        $data = [
            'type' => 200,
            'messages' => [
                'type' => 'success',
                'message' => 'Your account has been set up.  Please check your email for log in details.'
            ], // list of messages.
            'data' => [], // Hold the data to be used on a page
            'metrics' => [
                'redirect' => '#/login'
            ]  // Hold flags and metrics required to progress app.
        ];

//		$data['request'] = $this->request;

        return $data;
    }

    public function updateorgbank()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        $data = [];

        if (!isset($this->request['formData']) || empty($this->request['formData'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Required arguments are missing.'
            ];
            return $data;
        }

        if (!isset($this->request['formData']['orgid']) || empty($this->request['formData']['orgid'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Required arguments are missing.'
            ];
            return $data;
        }

        if (!isset($_SESSION['session']['organizations'][$this->request['formData']['orgid']])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Organization not found.'
            ];
            return $data;
        }

        // validate bank details.
        /*
        include(DIR_CLASSES.'bankcheck.php');

        $pa = new BankAccountValidation ("FN97-ZM77-HE35-FA68", $this->request['formData']['accountNumber'], $this->request['formData']['SortCode'][0]."-".$this->request['formData']['SortCode'][1]."-".$this->request['formData']['SortCode'][2]);
        $pa->MakeRequest();

        if ($pa->HasData()) {
            $data = $pa->HasData();

            if ($data[0]['IsCorrect'][0] == 'False') {
                // details not valid.
                $data['type'] = 401;
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'Account details failed verification. Please check account details carefully.'
                ];
                return $data;
            }
        }
*/
        $masterOrgID = getMasterOrgID($this->request['formData']['orgid']);

        $allOrgQuery = dbq("select organization_id from organizations where organization_id = :ORGID or organization_parent_organization_id = :ORGID",
            [
                ':ORGID' => $masterOrgID
            ]
        );

        while($allOrg = dbf($allOrgQuery)){
            // delete old details if they exist.
            dbq("delete from organization_bank_accounts where organization_id = :ONEORGID",
                [
                    ':ONEORGID' => $allOrg['organization_id']
                ]
            );

            // Insert new details.
            dbq("INSERT INTO organization_bank_accounts (
                        organization_id,
                        organization_bank_sort_code_1,
                        organization_bank_sort_code_2,
                        organization_bank_sort_code_3,
                        organization_bank_account,
                        organization_bank_name,
                        organization_bank_address_id,
                        organization_bank_last_updated
                      ) VALUES (
                        :ORGANIZATIONID,
                        :ORGANIZATIONBANKSORTCODE1,
                        :ORGANIZATIONBANKSORTCODE2,
                        :ORGANIZATIONBANKSORTCODE3,
                        :ORGANIZATIONBANKACCOUNT,
                        :ORGANIZATIONBANKNAME,
                        :ORGANIZATIONBANKADDRESSID,
                        NOW()
                      )",
                [
                    ':ORGANIZATIONID' => $allOrg['organization_id'],
                    ':ORGANIZATIONBANKSORTCODE1' => $this->request['formData']['SortCode'][0],
                    ':ORGANIZATIONBANKSORTCODE2' => $this->request['formData']['SortCode'][1],
                    ':ORGANIZATIONBANKSORTCODE3' => $this->request['formData']['SortCode'][2],
                    ':ORGANIZATIONBANKACCOUNT' => $this->request['formData']['accountNumber'],
                    ':ORGANIZATIONBANKNAME' => $this->request['formData']['accountName'],
                    ':ORGANIZATIONBANKADDRESSID' => null
                ]
            );

            // update org.
            dbq('update organizations set organization_bank_verified = 1 where organization_id = :ORGID',
                [
                    ':ORGID' =>  $allOrg['organization_id']
                ]
            );

            audit(23,$allOrg['organization_id'],'Bank details verified.');

        }

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Details updated.'
        ];

        return $data;
    }

// TODO Register Organization
    public function updateorganization()
    {
        /*
		 formdata
			firstName
				"yturt"
			lastName
				"rtyurtyu"
			email
				"rtyurtyu@rtyery.com"
			message
				"sfasfasdfas asdfasf asdf asd"
	    */

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        $data = [];

        if (!isset($this->request['formdata']) || empty($this->request['formdata'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        if (!isset($this->request['calltype']) || empty($this->request['calltype'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }


        switch ($this->request['calltype']) {
            case 'user' :

                if (!empty($this->request['formdata']['currentpassword'])) {
                    $password = trim($this->request['formdata']['currentpassword']);
                    // they want to change their password.
                    $checkUserQuery = dbq("SELECT user_id, user_password FROM users WHERE user_id = :USERID", [':USERID' => $_SESSION['session']['user']['user_id']]);
                    $checkUser = dbf($checkUserQuery);

                    // check this password is valid.
                    if (!password_verify($password, $checkUser['user_password'])) {
                        $data['type'] = 401;
                        $data['messages'][] = [
                            'type' => 'danger',
                            'message' => 'Current password does not match.'
                        ];

                        return $data;
                    } else {
                        // update the password only.

                        if ($this->request['formdata']['pw1'] != $this->request['formdata']['pw2']) {
                            $data['type'] = 401;
                            $data['messages'][] = [
                                'type' => 'danger',
                                'message' => 'New passwords do not match.'
                            ];

                            return $data;
                        } else {
                            $newPass = password_hash($this->request['formdata']['pw1'], PASSWORD_BCRYPT);

                            dbq("UPDATE users SET
								user_password = :NEWPASS
							  WHERE
								user_id = :USERID
								",
                                [
                                    ':NEWPASS' => $newPass,
                                    ':USERID' => $_SESSION['session']['user']['user_id']
                                ]);
                            audit(20, $this->request['formdata']['orgid']);
                            $data['messages'][] = [
                                'type' => 'success',
                                'message' => 'Password updated.'
                            ];
                        }

                    }
                }
                /*
 					user_id,
					user_name,
					user_first_name,
					user_last_name,
					user_email,
					user_password,
					user_avatar,
					user_work_phone,
					user_mobile_phone,
					user_home_phone,
					user_home_email,
					user_title_id,
					user_salutation
 * */
                // update.

                dbq("UPDATE users SET
						user_first_name = :FIRSTNAME,
						user_last_name = :LASTNAME,
						user_email = :USEREMAIL,
						user_work_phone = :WORKPHONE,
						user_mobile_phone = :MOBPHONE,
						user_home_phone = :HOMEPHONE,
						user_home_email = :HOMEEMAIL,
						user_title_id = :TITLEID,
						user_salutation = :SALUTATION
					WHERE
						user_id = :USERID
					",
                    [
                        ':FIRSTNAME' => $this->request['formdata']['userfirstname'],
                        ':LASTNAME' => $this->request['formdata']['userlastname'],
                        ':USEREMAIL' => $this->request['formdata']['useremail'],
                        ':WORKPHONE' => $this->request['formdata']['userworkphone'],
                        ':MOBPHONE' => $this->request['formdata']['usermobilephone'],
                        ':HOMEPHONE' => $this->request['formdata']['userhomephone'],
                        ':HOMEEMAIL' => $this->request['formdata']['userhomeemail'],
                        ':TITLEID' => $this->request['formdata']['usertitle'],
                        ':SALUTATION' => null,
                        ':USERID' => $_SESSION['session']['user']['user_id'],
                    ]
                );
                audit(6, $this->request['formdata']['orgid']);
                $data['type'] = 200;
                $data['messages'][] = [
                    'type' => 'success',
                    'message' => 'Details updated.'
                ];

                return $data;

                break;
            case 'org' :
                /*
                  	organization_id,
					organization_type_id,
					organization_name,
					organization_member_count,
					organization_default_address_id,
					organization_telephone,
					organization_fax,
					organization_email,
					organization_url,
					school_data_id,
					school_local_authority_id,
					school_education_phase_id,
					organization_default_logo_id,
					organization_funds,
					organization_status_id,
					school_URN,
					organization_has_subunits,
					organization_parent_id,
					logos_imported
                 * */
                // update.

                // TODO Check the org belongs to this user!

                dbq("UPDATE organizations SET
						organization_type_id = :ORGTYPEID,
						organization_name = :ORGNAME,
						organization_url = :ORGURL,
						organization_member_count = :ORGMEMBERS,
						organization_telephone = :ORGPHONE
					WHERE
						organization_id = :ORGID
					",
                    [
                        ':ORGTYPEID' => $this->request['formdata']['orgtype'],
                        ':ORGNAME' => $this->request['formdata']['orgname'],
                        ':ORGURL' => $this->request['formdata']['orgurl'],
                        ':ORGMEMBERS' => $this->request['formdata']['orgmembercount'],
                        ':ORGPHONE' => $this->request['formdata']['orgtelephone'],
                        ':ORGID' => $this->request['formdata']['orgid'],
                    ]
                );

                // Address.

                // TODO  check that this address belongs to this person.

                // $this->request['formdata']['addressid'],
                /*
				$scope.formdata.addressid = $scope.user.organizations[0].addresses[0].address_id;
				$scope.formdata.orgaddress1 = $scope.user.organizations[0].addresses[0].entry_street_address;
				$scope.formdata.orgaddress2 = $scope.user.organizations[0].addresses[0].entry_street_address_2;
				$scope.formdata.orgaddress3 = $scope.user.organizations[0].addresses[0].entry_suburb;
				$scope.formdata.orgtown = $scope.user.organizations[0].addresses[0].entry_city;
				$scope.formdata.orgcounty = $scope.user.organizations[0].addresses[0].entry_state;
				$scope.formdata.orgpostcode = $scope.user.organizations[0].addresses[0].entry_postcode;
				$scope.formdata.orgcountry = $scope.user.organizations[0].addresses[0].entry_country_id;

				 * */
                dbq("UPDATE addresses SET
						entry_street_address = :STREET1,
						entry_street_address_2 = :STREET2,
						entry_suburb = :STREET3,
						entry_city = :CITY,
						entry_state = :STATE,
						entry_postcode = :POSTCODE,
						entry_country_id = :COUNTRYID
					WHERE
						organization_id = :ORGID
						AND
						address_id = :ADDID
					",
                    [
                        ':STREET1' => $this->request['formdata']['orgaddress1'],
                        ':STREET2' => $this->request['formdata']['orgaddress2'],
                        ':STREET3' => $this->request['formdata']['orgaddress3'],
                        ':CITY' => $this->request['formdata']['orgtown'],
                        ':STATE' => $this->request['formdata']['orgcounty'],
                        ':POSTCODE' => $this->request['formdata']['orgpostcode'],
                        ':COUNTRYID' => $this->request['formdata']['orgcountry'],
                        ':ORGID' => $this->request['formdata']['orgid'],
                        ':ADDID' => $this->request['formdata']['addressid']
                    ]
                );

                audit(11, $this->request['formdata']['orgid']);

                $data['type'] = 200;
                $data['messages'][] = [
                    'type' => 'success',
                    'message' => 'Details updated.'
                ];

                return $data;

                break;
            default :
                break;
        }
        /*
		$orgUserDataQuery = dbq("select u.*, u2o.user_primary from users u join users_to_organizations u2o using (user_id) where u2o.organization_id = :ORGID",[':ORGID' => $this->request['formData']['organizationID']]);
		while($orgUserData = dbf($orgUserDataQuery)){
			unset($orgUserData['user_password']); // Don't want that out there.
			$_SESSION['session']['users'][$this->request['formData']['organizationID']][] = $orgUserData;
		}

		$data['session'] = $_SESSION;*/

        return $this->request;
    }

    public function checkemail()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['email']) || empty($this->request['email'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $emailQuery = dbq("SELECT * FROM users WHERE user_email = :EMAIL", [':EMAIL' => $this->request['email']]);

        if (dbnr($emailQuery) > 0) {
            // $details = dbf($emailQuery);
            // debugMail([$details,$this->request]);
            $this->setError([
                'message' => 'User exists.',
                'errorCode' => 'user-exists',
            ]);
            return $this->getResponse(500);
        }

        return 'true';
    }

     public function checkpostcode()
    {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }
       // $this->request['Postcode'] ='BH23 2BS';
        $data['type'] = 200;

        //JOIN organizations o ON o.`school_data_id` = sd.`school_data_id`
        $selectQuery = dbq("SELECT DISTINCT(sd.Name),sd.Postcode,sd.Town,sd.County,sd.Phone,sd.URL,sd.Pupils,sd.URN,sd.LEAName,sd.Address1,sd.Address2,sd.Address3,sc.`school_status_name` 
                            FROM school_data sd
                            JOIN school_statuses sc ON sc.`school_status_id` = sd.`school_status_id`
                            LEFT JOIN organizations o ON o.`school_data_id` = sd.`school_data_id`
                            WHERE REPLACE(sd.Postcode, ' ', '')  = REPLACE(:POSTCODE,' ', '')",
                    [
                        ':POSTCODE' => $this->request['Postcode']
                    ]);

        if(dbnr($selectQuery) > 0)
        {
            while($fetchQuery = dbf($selectQuery))
            {
                $data[] =
                    [
                        'Name'               => $fetchQuery['Name'],
                        'Address1'           => $fetchQuery['Address1'],
                        'Address2'           => $fetchQuery['Address2'],
                        'Address3'           => $fetchQuery['Address3'],
                        'County'             => $fetchQuery['County'],
                        'Town'               => $fetchQuery['Town'],
                        'Postcode'           => $fetchQuery['Postcode'],
                        'Phone'              => $fetchQuery['Phone'],
                        'URL'                => $fetchQuery['URL'],
                        'Pupils'             => $fetchQuery['Pupils'],
                        'school_status_name' => $fetchQuery['school_status_name'],
                        'URN'                => $fetchQuery['URN']
                    ];
            }
        }
        else
        {
            $data['type'] = 403;
            $data['messages'][] = [
                'type'    => 'danger',
                'message' => 'Postcode not found.'
            ];
        }
        return $data;
    }

    public function getschooldata()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['sdid']) || !is_numeric($this->request['sdid']) || empty($this->request['sdid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $searchSchool = $this->request['sdid'];
        // Get the product list from the database, make sure the skus are in there.
        // TODO limit only to form data.
        $schoolDataQuery = dbq("SELECT Address1, Address2, Address3, Area, County, Name, Postcode, Town, URL, URN, Pupils, `School Capacity` FROM school_data WHERE school_data_id = :SDID", [':SDID' => $searchSchool]);
        $schoolData = dbf($schoolDataQuery);

        if (!is_null($schoolData['Pupils'])) {
            $schoolData['membercount'] = $schoolData['Pupils'];
        } elseif (!is_null($schoolData['School Capacity'])) {
            $schoolData['membercount'] = $schoolData['School Capacity'];
        } else {
            $schoolData['membercount'] = '';
        }
        unset($schoolData['School Capacity'], $schoolData['Pupils']);
        $data['data'] = $schoolData;
        return $data;
    }

    private function getSchoolDataID($urn)
    {
        // find out if the use has nominated before?
        $schoolLookup = dbq("select school_data_id from school_data where URN = :URNID", [
            ':URNID' => $urn
        ]);

        $school = dbf($schoolLookup);

        return $school['school_data_id'];
    }

    private function getSchoolName($sdid)
    {
        // find out if the use has nominated before?
        $schoolLookup = dbq("SELECT `Name` FROM school_data WHERE school_data_id = :SDID", [
            ':SDID' => $sdid
        ]);

        $school = dbf($schoolLookup);

        return $school['Name'];
    }

    public function nominate()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }


        if (!isset($this->request['urnid']) || !is_numeric($this->request['urnid']) || empty($this->request['urnid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $dataID = $this->getSchoolDataID($this->request['urnid']);
        $email_address = $this->request['parentemail'];
        $forename = $this->request['parentforename'];
        $surname = $this->request['parentsurname'];

        // find out if the use has nominated before?
        $checkepeatNomination = dbq("SELECT 1 FROM nominations WHERE nominee_email = :EMAIL AND school_data_id = :DATAID", [
            ':EMAIL' => $this->request['parentemail'],
            ':DATAID' => $dataID
        ]);

        if (dbnr($checkepeatNomination) > 0) {
            // repeat nomination.  Send error about already nominating.
            $data['type'] = '401';
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'You have already nominated this school.'
            ];
        } else {
            // Add the nomination and update counters.

            $insertNom = dbq("INSERT INTO nominations (
					nomination_date,
					school_data_id,
					nominee_forename,
					nominee_surname,
					nominee_email
				  ) VALUES (
				  	NOW(),
					:DATAID,
					:NFNAME,
					:NSNAME,
					:NEMAIL
				  )", [
                    ':DATAID' => $dataID,
                    ':NFNAME' => $forename,
                    ':NSNAME' => $surname,
                    ':NEMAIL' => $email_address
                ]
            );

            if ($insertNom) {
                dbq("UPDATE school_data SET Nominations = Nominations + 1 WHERE school_data_id = :DATAID", [
                    ':DATAID' => $dataID
                ]);

                // Send an email
                //ues_mail('TEST NAME','artworkfiles@gmail.com','Nomination Request','Thank you for your nomination of '.$this->getSchoolName($dataID)."\n\n"."We will contact your school and advise them of your nominaion.  Please find attached a leaflet for you to print out and give to your school to help make them aware of the service and its advantages.  You can also spread the word to other parents who can also make nominations.",'F&F Uniforms','noreply@ff-ues.com');


                $subject = 'Nomination Request from F&F Uniforms';
                $title = $subject;
                $template = 'tesco-ff-uniforms';
                $tag = ['ff-monination'];

                $emailAddress = [
                    'F&F Uniform Support' => 'ues@uniformembroideryservice.com',
                    'Andrew Lindsay' => 'andy@slickstitch.com'
                ];

                $toName = $forename . " " . $surname;
                $schoolInfo = nl2br(strip_tags($this->request['formdata']['message']));

                $html = "There has been a new nomination from the F&F Uniforms site.<br />
						<br />
						<br />
						Name: $toName<br />
						Email: $email_address<br />
						School Details: " . $this->getSchoolName($dataID) . " - (URN-" . $this->getSchoolDataID($this->request['urnid']) . ")<br />
						<br />
						<br />
						<br />
						Yours sincerely,<br />
						<br />
						The F&F Uniforms Team";

                $userhtml = "Thank you for your nomination of <b>" . $this->getSchoolName($dataID) . "</b><br/><br/>
						We will contact your school and advise them of your nomination.<br />
						<br />
						<br />
						<br />
						Yours sincerely,<br />
						<br />
						The F&F Uniforms Team";


                $emailAddress = [
                    'F&F Uniform Support' => 'ues@uniformembroideryservice.com',
                    'Andrew Lindsay' => 'andy@slickstitch.com'
                ];

                $userEmailAddress = [
                    $toName => $email_address
                ];


                $template_content = array(
                    [
                        'name' => 'emailtitle',
                        'content' => $title
                    ],
                    [
                        'name' => 'main',
                        'content' => $html
                    ]
                );

                $user_template_content = array(
                    [
                        'name' => 'emailtitle',
                        'content' => $title
                    ],
                    [
                        'name' => 'main',
                        'content' => $userhtml
                    ]
                );

                send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);
                send_ues_mail($userEmailAddress, $subject, $user_template_content, $template, $tag);

                // audit
                //audit(21,$_SESSION['session']['user']['user_id'],);
            }

            $data['messages'][] = [
                'type' => 'success',
                'message' => 'Nomination added'
            ];
        }

        return $data;
    }

    public function removeproduct()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['deleteEmbData']) || empty($this->request['deleteEmbData'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $audittype = 16; // plain product remove

        // check that org ID is in session.
        $checkExistsQuery = dbq("SELECT
                                    p.product_decorated,
                                    p.product_name,
                                    c.colour_name,
                                    ol.tesco_style_ref,
                                    ol.organization_logo_id,
                                    pc.product_colour_id
                                    FROM products_colours_to_organizations pco
 									JOIN product_colours pc USING (product_colour_id)
 									JOIN products p USING (product_id)
 									JOIN colours c USING (colour_id)
 									LEFT JOIN organization_logos ol USING (organization_logo_id)
 									WHERE pco.products_colours_to_organizations_id = :PCOID AND pco.organization_id = :OID",
            [
                ':PCOID' => $this->request['deleteEmbData']['productColourOrgID'],
                ':OID' => $this->request['deleteEmbData']['organizationID']
            ]
        );

        /*	$checkExistsQuery = dbq("select * from products_colours_to_organizations pco
 									JOIN product_colours pc using (product_colour_id)
 									JOIN products p using (product_id)
 									where pco.products_colours_to_organizations_id = :PCOID and pco.organization_id = :OID",
			[
				':PCOID' => $this->request['deleteEmbData']['productColourOrgID'],
				':OID' => $this->request['deleteEmbData']['organizationID']
			]
		);*/

        if (dbnr($checkExistsQuery) > 0 && isset($_SESSION['session']['organizations'][$this->request['deleteEmbData']['organizationID']])) {
            $checkExists = dbf($checkExistsQuery);
            $logoID = null;
            $productColourID = $checkExists['product_colour_id'];

            if ($checkExists['product_decorated']) {
                $productString = $checkExists['product_name'] . ' (' . $checkExists['colour_name'] . ') - ' . $checkExists['tesco_style_ref'];
                $audittype = 14;  // embroidered remove
                // make sure its not the last one if embroidered.
                $checkEmbQuery = dbq("SELECT * FROM products_colours_to_organizations pco
 									JOIN product_colours pc USING (product_colour_id)
 									JOIN products p USING (product_id)
 									WHERE p.product_decorated = 1 AND pco.organization_id = :OID",
                    [
                        ':OID' => $this->request['deleteEmbData']['organizationID']
                    ]
                );

                if (dbnr($checkEmbQuery) <= 1) {
                    // cant delete last item.
                    $data['type'] = 403;
                    $data['messages'][] = [
                        'type' => 'danger',
                        'message' => 'Sorry, you cannot remove the last item.'
                    ];

                    return $data;
                }

            } else {
                $productString = $checkExists['product_name'] . ' (' . $checkExists['colour_name'] . ')';
            }
            // delete it.
            dbq("DELETE FROM products_colours_to_organizations WHERE products_colours_to_organizations_id = :PCOID AND organization_id = :OID",
                [
                    ':PCOID' => $this->request['deleteEmbData']['productColourOrgID'],
                    ':OID' => $this->request['deleteEmbData']['organizationID']
                ]
            );

            // update the product usage
            dbq("update product_colours set product_colour_school_count = product_colour_school_count - 1 where product_colour_id = :PCID",
                [
                    ':PCID' => $productColourID
                ]
            );

            if(!empty($logoID)){
                // update logo usage
                dbq("update organization_logos set products_using_logo = products_using_logo - 1 where organization_logo_id = :ORGANIZATIONLOGOID",
                    [
                        ':ORGANIZATIONLOGOID' => $logoID
                    ]
                );
            }

            audit($audittype, $this->request['deleteEmbData']['organizationID'], $productString);

            $data['messages'][] = [
                'type' => 'success',
                'message' => 'Item removed.'
            ];

        } else {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'The product cannot be removed.'
            ];
        }

        return $data;
    }


    public function addemblemrequest()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['requestData']) || empty($this->request['requestData'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        if (!isset($this->request['requestData']['description']) || empty($this->request['requestData']['description'])) {

            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Please fill out a description.'
            ];
            return $data;
        }

        if (!isset($_SESSION['session']['organizations'][$this->request['requestData']['orgid']])) {

            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Organization not valid.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $changeType = $this->request['requestData']['logoType'];

        if(isset($this->request['requestData']['logoWidth']) && !empty($this->request['requestData']['logoWidth'])){
            $this->request['requestData']['description'] .= "\n\n"."Logo Size: ".$this->request['requestData']['logoWidth'].'mm wide x '.$this->request['requestData']['logoHeight'].'mm high';
        }

        $attachments = false;
        $attachmentArray = null;
        $attachmentEbosArray = null;
        $ticketAttachmentArray = [];
        $firstEmblem = '';
        if (is_array($this->request['requestData']['attachments'])) {
            foreach ($this->request['requestData']['attachments'] as $attachment) {

                if ($attachment['type'] == 'emblem') {

                    if(empty($firstEmblem)){
                        $firstEmblem = $attachment['name'];
                    }

                    // just a URL.
                    $attachmentArray[] = [
                        'type' => "image/png",
                        'name' => basename('http:'  . $attachment['url']),
                        'content' => base64_encode(file_get_contents('http:' . $attachment['url']))
                    ];

                    $attachmentEbosArray[] = [
                        'filename' => basename('http:'  . $attachment['url']),
                            'data' => base64_encode(file_get_contents('http:' . $attachment['url']))
                    ];

                    $ticketAttachmentArray[] = 'http:'  . $attachment['url'];

                    $attachments = true;
                } else {
                    $masterOrgId =  getMasterOrgID($this->request['requestData']['orgid']);
                    //Save file to server.
                    $filedata = @end(explode('base64,', $attachment['url'], 2));
                    $filedata = base64_decode($filedata);

                    if (!file_exists('attachments/' . $masterOrgId)) {
                        mkdir('attachments/' . $masterOrgId);
                    }

                    $attachment['name'] = str_replace(' ','_',$attachment['name']); // handle invalid characters?

                    $newfilename = explode('.', $attachment['name']);
                    array_splice($newfilename, sizeof($newfilename) - 1, 0, shorten(time()));

                    $newfilename = 'attachments/' . $masterOrgId . '/' . implode('.', $newfilename);

                    file_put_contents($newfilename, $filedata);

                    if (empty($attachment['type'])) {
                        $attachment['type'] = 'text/plain';
                    }

                    $attachmentArray[] = [
                        'type' => "image/png",
                        'name' => basename(API_ROOT . $newfilename),
                        'content' => base64_encode(file_get_contents(API_ROOT . $newfilename))
                    ];

                    $attachmentEbosArray[] = [
                        'filename' => basename(API_ROOT . $newfilename),
                        'data' => base64_encode(file_get_contents(API_ROOT . $newfilename))
                    ];

                    $ticketAttachmentArray[] = API_ROOT . $newfilename;
                    $attachments = true;
                    sleep(1);
                }

            }
            if(empty($firstEmblem) && $changeType == "existing"){
                // shouldnt be here, need to know what emblem to change.
                $data['type'] = 401;
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'Please include the emblem to update.'
                ];
                return $data;
            }
        }

        $masterOrgID = getMasterOrgID($this->request['requestData']['orgid']);

        // work out next logo ID.
        $logoCodeQuery = dbq("SELECT DISTINCT(tesco_style_ref) FROM organization_logos WHERE organization_id = :ORGID AND logo_deleted != 1",
            [
                ':ORGID' => $masterOrgID
            ]
        );


        //"existing"
        //return $changeType;
        if($changeType == "existing"){

            //$existingCodeParts = explode('-', $firstEmblem);
            $existingCodeParts = explode('-', str_replace('-TMP','',$firstEmblem));
            $codeParts = explode('|', chunk_split(end($existingCodeParts), 1, '|'));

            $codeSuffix = [
                $codeParts[0],
                $codeParts[1]
            ];

            // Highest letter for same number
            while ($logoCode = dbf($logoCodeQuery)) {

                $codeParts = explode('-', str_replace('-TMP', '', $logoCode['tesco_style_ref']));
                $codeParts = explode('|', chunk_split(end($codeParts), 1, '|'));

                if ($codeParts[0] == $codeSuffix[0]) {
                    // this is the same logo (in theory)
                    if ($codeParts[1] > $codeSuffix[1]) {
                        // set it to the highest letter found for safety.
                        $codeSuffix[1] = $codeParts[1];
                        //$codeSuffix[1]++;

                    }
                }
            }

            $codeSuffix[1]++;

        } else {

            $codeSuffix = [
                0,
                'A'
            ];

            $stages[] = $codeSuffix;
            // add a new number.
            while ($logoCode = dbf($logoCodeQuery)) {

                $codeParts = explode('-', str_replace('-TMP', '', $logoCode['tesco_style_ref']));
                $codeParts = explode('|', chunk_split(end($codeParts), 1, '|'));

                if ($codeParts[0] > $codeSuffix[0]) {
                    // set it to the highest number found for safety.  Only do this for NEW designs.
                    $codeSuffix[0] = $codeParts[0];
                    $stages[] = $codeSuffix;
                }

            }
            $codeSuffix[1] = 'A';
            $codeSuffix[0]++;

        }


        $orgInfo = getOrgDetails($this->request['requestData']['orgid']);
        $masterOrgInfo = getOrgDetails($masterOrgID);

        $logoOrgName = $orgInfo['organization_name'];

        if($orgInfo['organization_id'] != $masterOrgInfo['organization_id'] ){
            // house. change name.
            $logoOrgName = $masterOrgInfo['organization_name'];
        }

        $newCode = $masterOrgInfo['school_URN'] . '-' . implode('', $codeSuffix);

        // Add a placeholder design into eBOS design Tool.
        include_once(DIR_CLASSES . 'eBOS_API.php');

        $eBOS_API = new eBOS_API(APIUser, APIPass); // TODO make admin.

        $ticketmessage = $this->request['requestData']['description'] . "\n\n" . implode("\n", $ticketAttachmentArray);

        $designdetail = [
            'design' => [
                'number' => $newCode . '-TMP',  // random string for testing.  Must be unique - No Spaces.
                'name' => $logoOrgName,  // random name for testing
                'notes' => 'Created by F&F UES' . "\n\n" . $ticketmessage,
                'threads' => [
                    '1000'
                ], // Must be strings not integers.
                'dst' => [
                    'data' => base64_encode(file_get_contents('dst/tempdesign.dst')),
                    'filename' => 'tempdesign.dst'
                ]
            ]
        ];


        $newDesign = $eBOS_API->createDesign($designdetail);  // Works.
        //$newDesign = $eBOS_API->createDesign($designdetail);  // Works.


        if (isset($newDesign->successful) && $newDesign->successful == false) {
            $data['type'] = 401;

            $data['messages'][] = [
                'type' => 'danger',
                'message' => $newDesign->message
            ];

            return $data;
        }

        // add an entry to logos table.
        dbq("INSERT INTO organization_logos (
					organization_id,
					school_name,
					tesco_style_ref,
					stitch_count,
					size__mm_,
					logo_approved,
					logo_url,
					logo_svg_url,
					primary_background_colour,
					logo_requested_name,
					logo_requested_date,
					products_using_logo,
					logo_on_ebos,
					ebos_id
				  ) VALUES (
					:ORGANIZATIONID,
					:SCHOOLNAME,
					:TESCOSTYLEREF,
					:STITCHCOUNT,
					:SIZEMM,
					:LOGOAPPROVED,
					:LOGOURL,
					:LOGOSVGURL,
					:PRIMARYBACKGROUNDCOLOUR,
					:LOGOREQUESTEDNAME,
					:LOGOREQUESTEDDATE,
					:PRODUCTSUSINGLOGO,
					:LOGOONEBOS,
				    :EBOSID
				  )",
            [
                ':ORGANIZATIONID' => $masterOrgID,
                ':SCHOOLNAME' => $logoOrgName,
                ':TESCOSTYLEREF' => $newCode . '-TMP',
                ':STITCHCOUNT' => $newDesign->design->stitch_count,
                ':SIZEMM' => $newDesign->design->width . 'x' . $newDesign->design->height,
                ':LOGOAPPROVED' => false,
                ':LOGOURL' => '//www.ff-ues.com/images/emblems/tempdesign.png',
                ':LOGOSVGURL' => '//www.ff-ues.com/images/emblems/tempdesign.svg',
                ':PRIMARYBACKGROUNDCOLOUR' => '191,191,189',
                ':LOGOREQUESTEDNAME' => $_SESSION['session']['user']['user_first_name'] . ' ' . $_SESSION['session']['user']['user_last_name'],
                ':LOGOREQUESTEDDATE' => date("Y-m-d G:i:s"),
                ':PRODUCTSUSINGLOGO' => 0,
                ':LOGOONEBOS' => 1,
                ':EBOSID' => $newDesign->design->id
            ]
        );

        if(isset($this->request['requestData']['createOrder']) && $this->request['requestData']['createOrder'] == true){
            // create a digitizing order.
            $newDigitizingOrder = $eBOS_API->createDigitizingOrder('new', 'artworkfiles@gmail.com');

            if (isset($newDigitizingOrder->successful) && $newDigitizingOrder->successful == false) {
                // Error email?
                $ticketmessage = '!!!! Couldnt create eBOS order !!!!' . "\n\n" . $ticketmessage;
                $ebosMessage = '!!!! Couldnt create eBOS order !!!!<br />';
            } else {
                $newdetails = [
                    'client_order_number' => $newCode,
                    //'notification_email_addresses'=>['artworkfiles@gmail.com'],
                    'special_instructions' => $this->request['requestData']['description'],
                    'dst_required' => true,
                    'emb_required' => true,
                    'pdf_required' => true,
                    'attachments' => $attachmentEbosArray // Not using attachments.
                ];

                $updateDigitizingOrderDetails = $eBOS_API->updateDigitizingOrder($newDigitizingOrder->order->id, $newdetails);

                // add a note about digitizing order.
                $ebosMessage = 'eBOS digitizing order: ' . $updateDigitizingOrderDetails->order->ebos_order_number . '<br />';
                $ticketmessage = 'eBOS digitizing order: ' . $updateDigitizingOrderDetails->order->ebos_order_number . "\n\n" . $ticketmessage;
            }

        }

        // Send an email.
        $subject = 'New Logo Request';
        $title = $subject;
        $template = 'tesco-ff-uniforms';
        $tag = ['ff-new-emblem'];

        $SCHOOLNAME = $logoOrgName . ' - (' . $masterOrgInfo['school_URN'] . ')';

        $html = "
		A new emblem request has been made from	$SCHOOLNAME.<br />
					<br />
					Emblem Code: $newCode<br />
					<br />
					$ebosMessage
					<br />
					The description given is:<br />" . $this->request['requestData']['description'] . "<br /><br />";

        if ($attachments) {
            // add info about them.
            $html .= "The user has uploaded the attached files for reference.<br /><br />";
        }

        $emailAddress = [
            //'F&F Uniform Support' => 'ues@uniformembroideryservice.com',
            'Andrew Lindsay' => 'it@slickstitch.com'
        ];

        $template_content = array(
            [
                'name' => 'emailtitle',
                'content' => $title
            ],
            [
                'name' => 'main',
                'content' => $html
            ]
        );

        send_ues_mail($emailAddress, $subject, $template_content, $template, $tag, $attachmentArray);

        // record activity.
        audit(4, $masterOrgID);
        // Add as to-do Note.

        // add attachment links to
        addTicket(3, $masterOrgID, 'New Emblem Request.', $ticketmessage,true,date('Y-m-d'),null,3);

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Your request has been sent and a new logo will be created in the next 7-10 days.'
        ];

        return $data;
    }

    public function requestsample()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['emblem']) || empty($this->request['emblem'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // customer wants a sample.  Check its not been sampled before?  Not at the moment.
        $emblemCheckQuery = dbq("SELECT * FROM organization_logos WHERE organization_logo_id = :OLID",
            [
                ':OLID' => $this->request['emblem']['organization_logo_id']
            ]
        );

        if (dbnr($emblemCheckQuery) < 1) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Logo not found.'
            ];
            return $data;
        }

        $emblemCheck = dbf($emblemCheckQuery);

        if ($emblemCheck['logo_sampled'] == 1) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo is already being sampled.'
            ];
            return $data;
        }

        if ($emblemCheck['logo_digitized'] == 0) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo is still being digitized.'
            ];
            return $data;
        }

        if ($emblemCheck['logo_approved'] == 1) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo is already approved.'
            ];
            return $data;
        }

        // ok update the background and get a sample out.
        dbq("UPDATE organization_logos SET primary_background_colour = :NEWBG, logo_sampled = 0 WHERE organization_logo_id = :OLID",
            [
                ':NEWBG' => $this->request['emblem']['primary_background_colour'],
                ':OLID' => $this->request['emblem']['organization_logo_id']
            ]
        );

        $orgInfo = getOrgDetails($this->request['emblem']['organization_id']);

        // Send an email first.
        // Send an email.
        $subject = 'New Sample Request';
        $title = $subject;
        $template = 'tesco-ff-uniforms';
        $tag = ['ff-new-sample'];

        $SCHOOLNAME = $orgInfo['organization_name'] . ' (' . $orgInfo['school_URN'] . ')';

        $html = "A new request for sample embroidery has been made from	$SCHOOLNAME.<br />
					<br />
					<b>Emblem Code:</b> " . $this->request['emblem']['tesco_style_ref'] . "<br />
					<b>Garment Colour:</b> " . colourFromRGB($this->request['emblem']['primary_background_colour']) . "<br />";

        $emailAddress = [
            //'F&F Uniform Support' => 'ues@uniformembroideryservice.com',
            'Andrew Lindsay' => 'andy@slickstitch.com'
        ];

        $template_content = array(
            [
                'name' => 'emailtitle',
                'content' => $title
            ],
            [
                'name' => 'main',
                'content' => $html
            ]
        );

        send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);
        // TODO Create sample order on ebos.  later...

        // Audit
        audit(27, $this->request['emblem']['organization_id'], $this->request['emblem']['tesco_style_ref'] . ' (' . colourFromRGB($this->request['emblem']['primary_background_colour']) . ')');
        // Note?
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Your logo has been sent for sampling.'
        ];

        return $data;
    }

    public function approvelogo()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['emblem']) || empty($this->request['emblem'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // customer wants a sample.  Check its not been sampled before?  Not at the moment.
        $emblemCheckQuery = dbq("SELECT * FROM organization_logos WHERE organization_logo_id = :OLID",
            [
                ':OLID' => $this->request['emblem']['organization_logo_id']
            ]
        );

        if (dbnr($emblemCheckQuery) < 1) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Logo not found.'
            ];
            return $data;
        }

        $emblemCheck = dbf($emblemCheckQuery);

      /*  if ($emblemCheck['logo_sampled'] == 0) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo has not been sampled.'
            ];
            return $data;
        }*/

        if ($emblemCheck['logo_digitized'] == 0) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo is already still being digitized.'
            ];
            return $data;
        }

        if ($emblemCheck['logo_approved'] == 1) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo is already approved.'
            ];
            return $data;
        }

        $orgInfo = getOrgDetails($this->request['emblem']['organization_id']);

        $fileArray = [
            'dst' => $this->request['emblem']['tesco_style_ref'] . '.dst',
            'pdf' => $this->request['emblem']['tesco_style_ref'] . '.pdf',
            'jpg' => $this->request['emblem']['tesco_style_ref'] . '.jpg'
        ];

        // make a jpg.
        $image = file_get_contents('http:'.$emblemCheck['logo_url']);
        $data['metrics']['startim'] = time();

        $im2 = new Imagick();
        $im2->setResolution(300,300);
        $im2->readImageBlob($image);
        $im2->scaleImage(316, 316, true);

        $im1 = new Imagick();
        $im1->setResolution(300,300);
        $im1->newImage(336, 336, new ImagickPixel('rgb(' . $this->request['emblem']['primary_background_colour'] . ')'));
        $im1->setImageUnits(1);
        $im1->setImageColorspace($im2->getImageColorspace());
        $im1->setImageMatte(true);

        $x = floor((336 - $im2->getImageWidth()) / 2);
        $y = floor((336 - $im2->getImageHeight()) / 2);

        $im1->compositeImage($im2, $im2->getImageCompose(), $x, $y);
        // Save it.
        $im1->setImageFormat("jpeg");

        file_put_contents('unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $this->request['emblem']['tesco_style_ref'] . '.jpg', $im1);
        $data['metrics']['endim'] = time();

// upload files to FTP.
        $conn_id = ftp_connect(FTP_HOST);
        if ($conn_id) {
            $login_result = ftp_login($conn_id, FTP_USER, FTP_PASS);
            if ($login_result) {
                ftp_pasv($conn_id, true);
                // upload.
                foreach ($fileArray as $filetype => $filename) {
                    $newname = str_replace('-TMP', '', $filename);
                    $remotefile = '/Tesco/Approved Schools/' . trim($orgInfo['school_URN']) . '/' . $newname;
                    $localfile = API_ROOT . 'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $filename;

                    if(!@ftp_chdir($conn_id,'/Tesco/Approved Schools/' . trim($orgInfo['school_URN']) . '/')){
                        // doesnt exist.
                        ftp_mkdir($conn_id,'/Tesco/Approved Schools/' . trim($orgInfo['school_URN']) . '/');
                    }

                    if (!ftp_put($conn_id, $remotefile, $localfile, FTP_BINARY)) {
                        // error, add to queue to do another time
                        dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                            [
                                ':LOCALFILE' => $localfile,
                                ':REMOTEFILE' => $remotefile,
                                ':HOSTPREFIX' => ''
                            ]
                        );

                    }
                }
            } else {
                // error login in.
                foreach ($fileArray as $filetype => $filename) {
                    $newname = str_replace('-TMP', '', $filename);
                    $remotefile = '/Tesco/Approved Schools/' . trim($orgInfo['school_URN']) . '/' . $newname;
                    $localfile = API_ROOT . 'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $filename;
                        // error, add to queue to do another time
                        dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                            [
                                ':LOCALFILE' => $localfile,
                                ':REMOTEFILE' => $remotefile,
                                ':HOSTPREFIX' => ''
                            ]
                        );
                }
            }
        } else {
            foreach ($fileArray as $filetype => $filename) {
                $newname = str_replace('-TMP', '', $filename);
                $remotefile = '/Tesco/Approved Schools/' . trim($orgInfo['school_URN']) . '/' . $newname;
                $localfile = API_ROOT . 'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $filename;
                // error, add to queue to do another time
                dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                    [
                        ':LOCALFILE' => $localfile,
                        ':REMOTEFILE' => $remotefile,
                        ':HOSTPREFIX' => ''
                    ]
                );
            }
        }

        $newname = str_replace('-TMP', '', $this->request['emblem']['tesco_style_ref']);
        // ok update it.
        dbq("UPDATE organization_logos SET
                  logo_approved = 1,
                  logo_sampled = 0,
                  tesco_style_ref = :NEWREF,
                  needs_to_update_ebos = 1,
                  logo_approved_date = NOW(),
                  logo_approved_name = :USERNAME
                  WHERE organization_logo_id = :OLID",
            [
                ':NEWREF' => $newname,
                ':USERNAME' => $_SESSION['session']['user']['user_first_name']." ".$_SESSION['session']['user']['user_last_name'],
                ':OLID' => $this->request['emblem']['organization_logo_id']
            ]
        );

        //If this is their first approval, set it as default!
        $approvedLogos = dbq("select * from organization_logos where logo_approved = 1 and organization_id = :OID",
            [
                ':OID' => $orgInfo['organization_id']
            ]
        );

        if(dbnr($approvedLogos) == 1){
            // first logo, update default logo.
            dbq("UPDATE organizations SET
                  organization_default_logo_id = :OLID
                  WHERE organization_id = :OID",
                [
                    ':OLID' => $this->request['emblem']['organization_logo_id'],
                    ':OID' => $orgInfo['organization_id']
                ]
            );

            // upload this one to Tesco for FF Website.

           /* $im1->scaleImage(75, 75, true);

            $smallfilename = @reset(explode('-',$this->request['emblem']['tesco_style_ref']));

            file_put_contents('unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $smallfilename . '_xs.jpg', $im1);

            $conn_id = null;
            $conn_id = ftp_connect(TESCO_FTP_HOST,21,10);
            if ($conn_id) {
                $login_result = ftp_login($conn_id, TESCO_FTP_USER, TESCO_FTP_PASS);

                if ($login_result) {
                    ftp_pasv($conn_id, true);
                    // upload.

                        $newname = str_replace('-TMP', '', $smallfilename . '_xs.jpg');
                        $localfile = API_ROOT.'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $smallfilename . '_xs.jpg';
                        if (!ftp_put($conn_id, $newname, $localfile, FTP_BINARY)) {
                            // error, add to queue to do another time
                            dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                                [
                                    ':LOCALFILE' => $localfile,
                                    ':REMOTEFILE' => $newname,
                                    ':HOSTPREFIX' => 'TESCO_'
                                ]
                            );
                            return $data;
                        }


                } else {
                    // error loging in.
                    // error, add to queue to do another time
                   
                        $newname = str_replace('-TMP', '',  $smallfilename . '_xs.jpg');
                        $localfile = API_ROOT . 'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' .  $smallfilename . '_xs.jpg';
                        dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                            [
                                ':LOCALFILE' => $localfile,
                                ':REMOTEFILE' => $newname,
                                ':HOSTPREFIX' => 'TESCO_'
                            ]
                        );


                }
            } else {
                // Error.
                // error, add to queue to do another time

                    $newname = str_replace('-TMP', '',  $smallfilename . '_xs.jpg');
                    $localfile = API_ROOT . 'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' .  $smallfilename . '_xs.jpg';
                    dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                        [
                            ':LOCALFILE' => $localfile,
                            ':REMOTEFILE' => $newname,
                            ':HOSTPREFIX' => 'TESCO_'
                        ]
                    );

            }*/


        }

        // Audit
        audit(28, $this->request['emblem']['organization_id'], $this->request['emblem']['tesco_style_ref'] . ' (' . colourFromRGB($this->request['emblem']['primary_background_colour']) . ')');
        // Note?
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Your logo has been approved and can now be used on garments.'
        ];

        return $data;
    }

    public function addproduct()
    {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['colourAdd']) || empty($this->request['colourAdd'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];
        $logoID = null;
        $audittype = 5;  // plain product add
        switch ($this->request['colourAdd']['embroided']) {
            case '0' :
                // plain
                // check its not already there.
                $checkExistsQuery = dbq("SELECT * FROM products_colours_to_organizations
									WHERE product_colour_id = :PCOLOURID
									AND organization_id = :OID",
                    [
                        ':PCOLOURID' => $this->request['colourAdd']['pColourID'],
                        ':OID' => $this->request['colourAdd']['organizationID']
                    ]
                );
                break;
            case '1' :
                // embroidered
                $audittype = 3;  // embroidered product add
                $logoID = $this->request['colourAdd']['logoID'];
                // check its not already there with this logo.
                $checkExistsQuery = dbq("SELECT * FROM products_colours_to_organizations
									WHERE product_colour_id = :PCOLOURID
									AND organization_id = :OID
									AND organization_logo_id = :LOGOID",
                    [
                        ':PCOLOURID' => $this->request['colourAdd']['pColourID'],
                        ':OID' => $this->request['colourAdd']['organizationID'],
                        ':LOGOID' => $logoID
                    ]
                );
                break;
        }

        if (dbnr($checkExistsQuery) > 0 && isset($_SESSION['session']['organizations'][$this->request['colourAdd']['organizationID']])) {
            // already exists.
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'The product configuration already exists.'
            ];
            return $data;
        }

        // add it in!
        dbq("INSERT INTO products_colours_to_organizations (
							product_colour_id,
							organization_id,
							organization_logo_id
						  ) VALUES (
						  	:PRODUCTCOLOURID,
							:ORGANIZATIONID,
							:ORGANIZATIONLOGOID
						  ) ",
            [
                ':PRODUCTCOLOURID' => $this->request['colourAdd']['pColourID'],
                ':ORGANIZATIONID' => $this->request['colourAdd']['organizationID'],
                ':ORGANIZATIONLOGOID' => $logoID
            ]
        );

        // update the product usage
        dbq("update product_colours set product_colour_school_count = product_colour_school_count + 1 where product_colour_id = :PCID",
            [
                ':PCID' => $this->request['colourAdd']['pColourID']
            ]
        );

        if(!empty($logoID)){
            // update logo usage
            dbq("update organization_logos set products_using_logo = products_using_logo + 1 where organization_logo_id = :ORGANIZATIONLOGOID",
                [
                    ':ORGANIZATIONLOGOID' => $logoID
                ]
            );
        }

        $checkExistsQuery = dbq("SELECT
                                    p.product_decorated,
                                    p.product_name,
                                    c.colour_name,
                                    ol.tesco_style_ref
                                    FROM products_colours_to_organizations pco
 									JOIN product_colours pc USING (product_colour_id)
 									JOIN products p USING (product_id)
 									JOIN colours c USING (colour_id)
 									LEFT JOIN organization_logos ol USING (organization_logo_id)
 									WHERE pco.product_colour_id = :PCID AND pco.organization_id = :OID",
            [
                ':PCID' => $this->request['colourAdd']['pColourID'],
                ':OID' => $this->request['colourAdd']['organizationID']
            ]
        );

        $checkExists = dbf($checkExistsQuery);

        if ($checkExists['product_decorated']) {
            $productString = $checkExists['product_name'] . ' (' . $checkExists['colour_name'] . ') - ' . $checkExists['tesco_style_ref'];
            // update to live

            $masterOrgID = getMasterOrgID($this->request['colourAdd']['organizationID']);
            // use master to set whole school live?
            $orgDetail = getOrgDetails($this->request['colourAdd']['organizationID']);
            $masterOrgDetail = getOrgDetails($masterOrgID);

            if($orgDetail['organization_status_id'] >= 4 && $orgDetail['organization_status_id'] <= 6){
                // this org/house is stage 1 - 3.  Added embroidered product. Update to live now.  Make sure master is live if house.
                dbq("update organizations set organization_status_id = 3, organization_live_date = NOW() where organization_id = :THISORG",
                    [
                        ':THISORG' => $this->request['colourAdd']['organizationID']
                    ]
                );

                if(empty($masterOrgDetail['organization_live_date'])){
                    // master not live.  set live.
                    dbq("update organizations set organization_status_id = 3, organization_live_date = NOW() where organization_id = :MASTERORG",
                        [
                            ':MASTERORG' => $masterOrgID,
                        ]
                    );
                }

                dbq("update school_data set school_status_id = 3 where school_data_id = :SDID",
                    [
                        ':SDID' => $orgDetail['school_data_id']
                    ]
                );

                // set this logo as the default logo for the org.
                dbq("update organizations set organization_default_logo_id = :LOGOID, organization_default_logo_update = 1 where organization_id = :ORGID",
                    [
                        ':LOGOID' => $logoID,
                        ':ORGID' => $orgDetail['organization_id']
                    ]
                );
            }


        } else {
            $productString = $checkExists['product_name'] . ' (' . $checkExists['colour_name'] . ')';
        }

        audit($audittype, $this->request['colourAdd']['organizationID'], $productString);

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Item added.'
        ];

        return $data;
    }

    public function rejectemblem()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['emblem']) || empty($this->request['emblem'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // customer wants a sample.  Check its not been sampled before?  Not at the moment.
        $emblemCheckQuery = dbq("SELECT * FROM organization_logos WHERE organization_logo_id = :OLID",
            [
                ':OLID' => $this->request['emblem']['organization_logo_id']
            ]
        );

        if (dbnr($emblemCheckQuery) < 1) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Logo not found.'
            ];
            return $data;
        }

        $emblemCheck = dbf($emblemCheckQuery);

        if ($emblemCheck['logo_digitized'] == 0) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo is still being digitized.'
            ];
            return $data;
        }

        if ($emblemCheck['logo_approved'] == 1) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo is already approved.'
            ];
            return $data;
        }

        $orgInfo = getOrgDetails($this->request['emblem']['organization_id']);

        // Reject.

        dbq('update organization_logos set
                logo_reject_reason = :REASON,
                logo_approved = 0,
                logo_sampled = 0,
                logo_digitized = 0
              WHERE
                organization_logo_id = :OLID
               ',
            [
                ':REASON' => $this->request['description'],
                ':OLID' => $this->request['emblem']['organization_logo_id']
            ]
        );

        audit(29,$this->request['emblem']['organization_id']);

        addTicket(2,$this->request['emblem']['organization_id'],'Logo Rejected : '.$emblemCheck['tesco_style_ref'],$this->request['description'],1,date("Y-m-d G:i:s"),null,3); // set an alert for all golley slater users.

        $data['messages'][] = [
            'type' => 'info',
            'message' => 'We have passed your comments on to our design team.'
        ];

        return $data;
    }

    public function addnewhouse()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['requestData']) || empty($this->request['requestData'])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        // Make sure the org is for this user.
        if (!isset($_SESSION['session']['organizations'][$this->request['requestData']['orgId']])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Organization not found.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $masterOrgID = getMasterOrgID($this->request['requestData']['orgId']);
        // need to copy the org and add a new one with new details
        $parentOrg = getOrgDetails($masterOrgID);

        $houseOrg = $parentOrg;

        if(empty($houseOrg)){
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Organization not found.'
            ];
            return $data;
        }

        $houseQtyQuery = dbq("select * from organizations where organization_parent_organization_id = :ORGID",
            [
                ':ORGID' => $parentOrg['organization_id']
            ]
        );

        $housecount = dbnr($houseQtyQuery);

        // put organization ID into parent.
        unset($houseOrg['organization_id']);

        $houseOrg['organization_parent_organization_id'] = $parentOrg['organization_id'];
        $houseOrg['organization_parent_id'] = $parentOrg['school_URN'];
        $houseOrg['organization_name'] = $this->request['requestData']['houseName'];
        $houseOrg['organization_has_subunits'] = 0;

        $urnprefix = 901 + $housecount;
        $houseOrg['school_URN'] = $urnprefix.$parentOrg['school_URN'];

        if($houseOrg['organization_status_id'] == 3 && $housecount > 0){
            // live parent, put this in stage 3, otherwise leave it as is.
            $houseOrg['organization_status_id'] = 6;
        }

        $fieldValues = [];
        foreach($houseOrg as $key => $value){
            $newKey = ':'.strtoupper($key);
            $fieldValues[$newKey] = $value;
        }
        // Insert this.
        dbq("insert into organizations (`".implode('`,`',array_keys($houseOrg))."`) values (".implode(",",array_keys($fieldValues)).")",
            $fieldValues
        );

        $houseID = dbid();

        //$data['sql'] = "insert into organizations (`".implode('`,`',array_keys($houseOrg))."`) values ('".implode("','",$houseOrg)."')";

        // needs address
        $addressQuery = dbq("select * from addresses where organization_id = :ORGID",
            [
                ':ORGID' => $parentOrg['organization_id']
            ]
        );

        while($address = dbf($addressQuery)){
            $setdefault = false;

            if($address['address_id'] == $parentOrg['organization_default_address_id']){
                $setdefault = true;
            }

            unset($address['address_id'],$address['user_id']);
            $address['organization_id'] = $houseID;

            $fieldValues = [];
            foreach($address as $key => $value){
                $newKey = ':'.strtoupper($key);
                $fieldValues[$newKey] = $value;
            }


            dbq("insert into addresses (`".implode('`,`',array_keys($address))."`) values (".implode(",",array_keys($fieldValues)).")",
                $fieldValues
            );

            if($setdefault){
                $defaultAddress = dbid();
                dbq("update organizations set organization_default_address_id = :ADDRESSID where organization_id = :ORGID",
                    [
                        ':ADDRESSID' => $defaultAddress,
                        ':ORGID' => $parentOrg['organization_id']
                    ]
                );
            }

        }

        // needs bank
        $banksQuery = dbq("select * from organization_bank_accounts where organization_id = :ORGID",
            [
                ':ORGID' => $parentOrg['organization_id']
            ]
        );

        while($banks = dbf($banksQuery)){

            unset($banks['organization_bank_account_id']);
            $banks['organization_id'] = $houseID;

            $fieldValues = [];
            foreach($banks as $key => $value){
                $newKey = ':'.strtoupper($key);
                $fieldValues[$newKey] = $value;
            }

            // copy the bank details for the organization into the house.
            dbq("insert into organization_bank_accounts (`".implode('`,`',array_keys($banks))."`) values (".implode(",",array_keys($fieldValues)).")",
                $fieldValues
            );

        }

        dbq("update organizations set organization_has_subunits = 1 where organization_id = :PARENTORGID",
            [
                ':PARENTORGID' => $parentOrg['organization_id']
            ]
        );
        
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'House Added.'
        ];

        if($housecount == 0){
            // first house, move any products that were for the main school to the first house.
            dbq("update products_colours_to_organizations set organization_id = :HOUSEID where organization_id = :PARENTORGID",
                [
                    ':HOUSEID' => $houseID,
                    ':PARENTORGID' => $parentOrg['organization_id']
                ]
            );
            $data['messages'][] = [
                'type' => 'info',
                'message' => 'Products associated with the main organization are now on the first house.'
            ];
        }

        audit(30,$parentOrg['organization_id'],$houseOrg['organization_name'].' ('.$houseOrg['school_URN'].')');

        refreshSession();

        $data['session'] = $_SESSION['session'];

        return $data;
    }

    public function lookupsingle()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['oid']) || !is_numeric($this->request['oid']) || empty($this->request['oid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // split the query into individual words.
        $orgID = $this->request['oid'];

        $searchSchoolQuery = dbq("SELECT
                               sd.`school_data_id`,

                               IF(ISNULL(o.organization_name),sd.`Name`,o.organization_name) as `Name`,
                               po.organization_name as organization_house_parent_name,
                               IF(ISNULL(a.entry_street_address),sd.`Address1`,a.entry_street_address) as `Address1`,
                               IF(ISNULL(a.entry_street_address_2),sd.`Address2`,a.entry_street_address_2) as `Address2`,
                               IF(ISNULL(a.entry_postcode),sd.`Postcode`,a.entry_postcode) as `Postcode`,

                              IF(ISNULL(a.entry_postcode),sd.`Address3`,'') as `Address3`,
                              IF(ISNULL(a.entry_city),sd.`Town`,a.entry_city) as `Town`,
                              IF(ISNULL(a.entry_state),sd.`County`,a.entry_state) as `County`,
                              IF(ISNULL(a.entry_suburb),sd.`Area`,a.entry_suburb) as `Area`,

                               o.`organization_id`,
                               ss.`school_status_name`,
                               ol.`tesco_style_ref` AS logo_ref,
                               ol.`primary_background_colour` AS emblem_background,

                               IF(ISNULL(po.organization_funds),IF(ISNULL(o.organization_funds),0,o.organization_funds),IF(ISNULL(po.organization_funds),0,po.organization_funds)) as `organization_funds`,
                               IF(ISNULL(ol.`logo_url`),'images/emblems/example-emblem.png',ol.`logo_url`) AS emblem_png,
                               IF(ISNULL(ol.`logo_svg_url`),'images/emblems/example-emblem.png',ol.`logo_svg_url`) AS emblem_svg
                        FROM school_data sd
                        JOIN school_statuses ss USING (school_status_id)
                        LEFT JOIN organizations o USING (school_data_id)
                        left join organizations po on po.organization_id = o.organization_parent_organization_id
                        LEFT JOIN organization_units ou ON (ou.organization_id = o.organization_id)
                        left join addresses a on (o.organization_default_address_id = a.address_id)
                        LEFT JOIN organization_logos ol ON (ol.organization_logo_id = o.organization_default_logo_id AND ol.logo_approved = 1)
                        WHERE o.organization_id = :ORGID
                        AND o.organization_status_id = 3
                        GROUP BY sd.school_data_id", [':ORGID' => $orgID]);

        $data = [
            'count' => 0,
            'organizations' => [],
        ];

        $dataCount = 0;

        // need to split out emblems and address.

        $addressFields = [
            'Address1',
            'Address2',
            'Address3',
            'Town',
            'County',
            'Area',
            'Postcode'
        ];

        $emblemFields = [
            'emblem_png',
            'emblem_svg',
            'emblem_background',
        ];

        while ($searchSchool = dbf($searchSchoolQuery)) {
            // Add house name?

            if(!empty($searchSchool['organization_house_parent_name']) && !stristr($searchSchool['Name'],$searchSchool['organization_house_parent_name'])){
                //if(!stristr($organization['organization_name'],$organization['organization_house_parent_name'])){
                // name not in house name.
                //$organization['organization_name'] = trim(str_ireplace($organization['organization_house_parent_name'],'',$organization['organization_name']));
                $searchSchool['Name'] = $searchSchool['organization_house_parent_name'] . ' ('.$searchSchool['Name'].')';
                //}
            }
            unset($searchSchool['organization_house_parent_name']);

            $searchSchool['emblem_png'] = 'http:' . $searchSchool['emblem_png'];
            foreach ($searchSchool as $key => $value) {
                if (in_array($key, $emblemFields)) {
                    $usevalue = str_replace('http://','https://',$value);
                    $data['organizations'][$dataCount]['emblem'][str_replace('emblem_', '', strtolower($key))] = $usevalue;
                } else if (in_array($key, $addressFields)) {
                    $data['organizations'][$dataCount]['address'][strtolower($key)] = $value;
                } else {
                    if (is_numeric($value) && $value == doubleval($value)) {
                        $value = round($value, 2);
                    }
                    $data['organizations'][$dataCount][strtolower($key)] = $value;
                }
            }
            // If this is a house, need the others and the master too.
            $dataCount++;
        }

        $data['count'] = sizeof($data['organizations']);

        return $data;
    }

    function syncpulse(){
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['logoref'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // Check logo exists.
        $checklogoQuery = dbq("select * from organization_logos ol where ol.tesco_style_ref = :LOGOREF",
            [
                ':LOGOREF' => $this->request['logoref']
            ]
        );

        if (dbnr($checklogoQuery) < 1) {

            return false;
        }

        dbq("DELETE from pulse_upload where pulse_upload_logo = :LOGOREF",
            [
                ':LOGOREF' => $this->request['logoref']
            ]
        );

        dbq("insert into pulse_upload (pulse_upload_logo) VALUES (:LOGOREF)",
            [
                ':LOGOREF' => $this->request['logoref']
            ]
        );

        return true;
    }

    /**
     * Get an organizations EMBROIDERED products,
     * @return array
     */

    public function getorgproductsandsizes()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $masterOrgID = getMasterOrgID($this->request['orgid']);

        $allOrgsQuery = dbq("select * from organizations where organization_id = :ORGID or organization_parent_organization_id = :ORGID",
            [
                ':ORGID' => $masterOrgID
            ]
        );
        $orgIDs = [];
        while($allOrgs = dbf($allOrgsQuery)){
            $orgIDs[] = $allOrgs['organization_id'];
        }
        $data['metrics'] = $orgIDs;

        $productQuery = dbq("SELECT p.*, pc2o.organization_id FROM products p
								JOIN product_colours pc USING (product_id)
								JOIN colours USING (colour_id)
								JOIN products_colours_to_organizations pc2o USING (product_colour_id)
								WHERE pc2o.organization_id IN (".implode(',',$orgIDs).")
								AND p.product_active = 1
								AND p.product_test_only = 0
								AND p.product_decorated = 1

								ORDER BY pc2o.organization_id, p.product_decorated", [
                ':ORGID' => $this->request['orgid']
            ]
        ); // dont want to see the test products

        $productArray = [];
        $altProductArray = [];
        while ($product = dbf($productQuery)) {
            if(!isset($altProductArray[$product['organization_id']]['orgname'])){
                $orgDetail = getOrgDetails($product['organization_id']);
                $altProductArray[$product['organization_id']]['orgid'] = $orgDetail['organization_id'];
                $altProductArray[$product['organization_id']]['orgname'] = $orgDetail['organization_name'];
            }

            $product['product_image'] = str_replace("/small/", "/medium/", $product['product_image']);

            // get colours here?
            $coloursQuery = dbq("SELECT pc2o.products_colours_to_organizations_id, pc.*, c.*, ol.`organization_logo_id`, ol.`tesco_style_ref` , ol.`logo_url`  FROM product_colours pc
									JOIN colours c USING (colour_id)
									JOIN products_colours_to_organizations pc2o USING (product_colour_id)
									LEFT JOIN organization_logos ol USING (organization_logo_id)
									WHERE pc2o.organization_id = :ORGID
									AND
									product_id = :PRODID",
                [
                    ':ORGID' => $product['organization_id'],
                    ':PRODID' => $product['product_id']
                ]
            );

            while ($colours = dbf($coloursQuery)) {
                $altProductArray[$product['organization_id']]['products'][$colours['products_colours_to_organizations_id']] = $product;
                //product_colour_image_url
                $colours['product_colour_image_url'] = str_replace("/small/", "/medium/", $colours['product_colour_image_url']);
                $altProductArray[$product['organization_id']]['products'][$colours['products_colours_to_organizations_id']]['colour'] = $colours;
                // get sizes.

                    // get available sizes
                    $sizeQuery = dbq("select * from products_sizes_to_products_colours ps2pc
                                    JOIN product_sizes ps on ps.product_size_id = ps2pc.product_size_id
                                    where ps2pc.product_colour_id = :PCID
                                    ORDER BY ps.product_size_sort_order",[
                            ':PCID' => $colours['product_colour_id']
                        ]
                    );

                    $altProductArray[$product['organization_id']]['products'][$colours['products_colours_to_organizations_id']]['colour']['sizes'] = [];
                    while($size = dbf($sizeQuery)){
                        if($size['product_variation_listed'] == 0){
                            // Dont let them order unlisted items.
                            $size['product_variation_stock'] = 0;
                        }
                        $altProductArray[$product['organization_id']]['products'][$colours['products_colours_to_organizations_id']]['colour']['sizes'][] = $size;
                    }

            }

        }

        $data['products'] = $altProductArray;

        return $data;

    }

    public function placebulkorder()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['requestData']) || empty($this->request['requestData'])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        // Make sure the org is for this user.
        if (!isset($_SESSION['session']['organizations'][$this->request['requestData']['orgId']])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Organization not found.'
            ];
            return $data;
        }

        // Make sure there are 50 items.
        if (!isset($this->request['requestData']['amount'])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Sorry, you must order 50 or more items.'
            ];
            return $data;
        }

        $total = 0;
        foreach($this->request['requestData']['amount'] as $orgID => $amounts){
            if(!empty($amounts)){
                $total += array_sum($amounts);
            }
        }

        if ($total < 20) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Sorry, you must order 50 or more items.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

      //  $data['data']['request'] = $this->request;

        // put an entry in the database.
        dbq("insert into organization_bulk_orders (
                  organization_id,
                  order_date,
                  order_name,
                  order_contact_name,
                  order_telephone,
                  order_email_address,
                  order_delivery_address_1,
                  order_delivery_address_2,
                  order_delivery_address_3,
                  order_delivery_town,
                  order_delivery_county,
                  order_delivery_postcode,
                  order_delivery_country,
                  organization_bulk_order_sent,
                  organization_bulk_order_carriage,
                  organization_bulk_order_total
              ) VALUES (
                  :ORGANIZATION_ID,
                  NOW(),
                  :ORDER_NAME,
                  :ORDER_CONTACT_NAME,
                  :ORDER_TELEPHONE,
                  :ORDER_EMAIL_ADDRESS,
                  :ORDER_DELIVERY_ADDRESS_1,
                  :ORDER_DELIVERY_ADDRESS_2,
                  :ORDER_DELIVERY_ADDRESS_3,
                  :ORDER_DELIVERY_TOWN,
                  :ORDER_DELIVERY_COUNTY,
                  :ORDER_DELIVERY_POSTCODE,
                  :ORDER_DELIVERY_COUNTRY,
                  :ORGANIZATION_BULK_ORDER_SENT,
                  :ORGANIZATION_BULK_ORDER_CARRIAGE,
                  :ORGANIZATION_BULK_ORDER_TOTAL
              )",
            [
                ':ORGANIZATION_ID' => $this->request['requestData']['orgId'],
                ':ORDER_NAME' => $this->request['requestData']['orgname'],
                ':ORDER_CONTACT_NAME' => $this->request['requestData']['orgcontactname'],
                ':ORDER_TELEPHONE' => $this->request['requestData']['orgtelephone'],
                ':ORDER_EMAIL_ADDRESS' => $this->request['requestData']['orgemailaddress'],
                ':ORDER_DELIVERY_ADDRESS_1' => $this->request['requestData']['orgaddress1'],
                ':ORDER_DELIVERY_ADDRESS_2' => $this->request['requestData']['orgaddress2'],
                ':ORDER_DELIVERY_ADDRESS_3' => $this->request['requestData']['orgaddress3'],
                ':ORDER_DELIVERY_TOWN' => $this->request['requestData']['orgtown'],
                ':ORDER_DELIVERY_COUNTY' => $this->request['requestData']['orgcounty'],
                ':ORDER_DELIVERY_POSTCODE' => $this->request['requestData']['orgpostcode'],
                ':ORDER_DELIVERY_COUNTRY' => $this->request['requestData']['orgcountry'],
                ':ORGANIZATION_BULK_ORDER_SENT' => 0,
                ':ORGANIZATION_BULK_ORDER_CARRIAGE' => null,
                ':ORGANIZATION_BULK_ORDER_TOTAL' => null
            ]
        );

        $bulkOrderID = dbid(); // TODO set start at 1100

        // Insert items.

        $orderTotal = 0;
        $message = '';
        foreach($this->request['requestData']['amount'] as $orgid => $amounts){
            if(!empty($amounts)){
                $orgdetails = getOrgDetails($orgid);

                foreach($amounts as $products_sizes_to_colours_id => $quantity){

                    if($quantity < 1){
                        continue;
                    }
                    // get the product details.
                    $productQuery = dbq("select * from products p
                                        JOIN product_colours pc USING (product_id)
                                        JOIN products_sizes_to_products_colours ps2pc USING (product_colour_id)
                                        JOIN product_sizes ps USING (product_size_id)
                                        JOIN colours c using (colour_id)
                                        LEFT JOIN products_colours_to_organizations pc2o on (pc2o.product_colour_id = pc.product_colour_id and pc2o.organization_id = :ORGID)
                                        LEFT JOIN organization_logos ol on (ol.organization_logo_id = pc2o.organization_logo_id)
                                        WHERE ps2pc.products_sizes_to_colours_id = :PS2PCID",
                        [
                            ':ORGID' => $orgid,
                            ':PS2PCID' => $products_sizes_to_colours_id
                        ]
                    );

                    $product = dbf($productQuery);

                    dbq("insert into organization_bulk_order_items (
                  organization_bulk_order_id,
                  organization_urn,
                  product_colour_style_ref,
                  product_name,
                  product_colour_name,
                  product_colour_image_url,
                  product_ean,
                  product_price,
                  product_quantity,
                  product_line_price,
                  product_size_name,
                  products_sizes_to_colours_id,
                  products_logo_ref,
                  product_gmo_cat_id
              ) VALUES (
                  :ORGANIZATION_BULK_ORDER_ID,
                  :ORGANIZATION_URN,
                  :PRODUCT_COLOUR_STYLE_REF,
                  :PRODUCT_NAME,
                  :PRODUCT_COLOUR_NAME,
                  :PRODUCT_COLOUR_IMAGE_URL,
                  :PRODUCT_EAN,
                  :PRODUCT_PRICE,
                  :PRODUCT_QUANTITY,
                  :PRODUCT_LINE_PRICE,
                  :PRODUCT_SIZE_NAME,
                  :PRODUCTS_SIZES_TO_COLOURS_ID,
                  :PRODUCTS_LOGO_REF,
                  :PRODUCTS_GMO_CAT_ID
              )",
                        [
                            ':ORGANIZATION_BULK_ORDER_ID' => $bulkOrderID,
                            ':ORGANIZATION_URN' => $orgdetails['school_URN'],
                            ':PRODUCT_COLOUR_STYLE_REF' => $product['product_colour_style_ref'],
                            ':PRODUCT_NAME' => $product['product_name'],
                            ':PRODUCT_COLOUR_NAME' => $product['colour_name'],
                            ':PRODUCT_COLOUR_IMAGE_URL' => $product['product_colour_image_url'],
                            ':PRODUCT_EAN' => $product['product_variation_sku'],
                            ':PRODUCT_PRICE' => $product['product_variation_price'],
                            ':PRODUCT_QUANTITY' => $quantity,
                            ':PRODUCT_LINE_PRICE' => $product['product_variation_price'] * $quantity,
                            ':PRODUCT_SIZE_NAME' => $product['products_size_name'],
                            ':PRODUCTS_SIZES_TO_COLOURS_ID' => $products_sizes_to_colours_id,
                            ':PRODUCTS_LOGO_REF' => $product['tesco_style_ref'],
                            ':PRODUCTS_GMO_CAT_ID' => $product['product_variation_gmo_sku']
                        ]
                    );
                    $orderTotal += $quantity * $product['product_variation_price'];

                    // add logo to this!
                    $message .= $product['product_colour_style_ref'] . ' : ' . $product['product_name'] . '('.$product['colour_name'].') - ['.$product['tesco_style_ref'].'] - '.$product['products_size_name'] . ' x '.$quantity."\n";
                }
            }

        }

        dbq("update organization_bulk_orders set organization_bulk_order_total = :TOTALORDER where organization_bulk_order_id = :OBOID",
            [
                ':TOTALORDER' => $orderTotal,
                ':OBOID' => $bulkOrderID
            ]
        );

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Order forwarded on to the customer service team.'
        ];


        // send email.
        $emailContent = "Thank you for your bulk order request.  Please find request details below.<br />
<br />
<b>Bulk Order Request Number:</b> $bulkOrderID<br /><br />".nl2br($message)."<br />
<br />
Your order request has been passed to our customer service team and they will be in touch if there are any issues.<br />
<br />
<br />
Yours sincerely,<br />
<br />
The F&F Uniforms Team";

        $subject = 'Bulk order request '.$bulkOrderID.' on F&F Uniforms';
        $title = $subject;
        $template = 'tesco-ff-uniforms';
        $tag = ['ff-bulk-request'];

        if($this->request['requestData']['orgemailaddress'] != $_SESSION['session']['user']['user_email']){
            // send two emails.
            $emailAddress = [
                //,
                $this->request['requestData']['orgcontactname'] => $this->request['requestData']['orgemailaddress'],
                $_SESSION['session']['user']['user_name'] => $_SESSION['session']['user']['user_email'],
               // 'Andrew Lindsay' => 'andy@slickstitch.com'
            ];
        } else {
            $emailAddress = [
                $_SESSION['session']['user']['user_name'] => $_SESSION['session']['user']['user_email'],
                //'Andrew Lindsay' => 'andy@slickstitch.com'
            ];
        }
        $message = $emailContent;

        $template_content = array(
            [
                'name' => 'emailtitle',
                'content' => $title
            ],
            [
                'name' => 'main',
                'content' => $message
            ]
        );

        send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);

        // Add note.
        audit(31,$this->request['requestData']['orgId'],$bulkOrderID);

        addTicket(9,$this->request['requestData']['orgId'],'Placed Bulk Order: '.$bulkOrderID,$message);
        return $data;

    }
    public function submitbilling() {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['requestData']) || empty($this->request['requestData'])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        // Make sure the org is for this user.
        if (!isset($_SESSION['session']['organizations'][$this->request['requestData']['orgId']])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Organization not found.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $data['request'] = $this->request['requestData'];

        // put into database// send email.
        $masterOrg = getMasterOrgID($this->request['requestData']['orgId']);
        $masterOrgDetail = getOrgDetails($masterOrg);
        // insert address.
        dbq("insert into addresses (
                entry_company,
                entry_firstname,
                entry_lastname,
                entry_street_address,
                entry_street_address_2,
                entry_city,
                entry_suburb,
                entry_state,
                entry_postcode,
                entry_country_id,
                user_id,
                organization_id
            ) VALUES (
                :ENTRY_COMPANY,
                :ENTRY_FIRSTNAME,
                :ENTRY_LASTNAME,
                :ENTRY_STREET_ADDRESS,
                :ENTRY_STREET_ADDRESS_2,
                :ENTRY_CITY,
                :ENTRY_SUBURB,
                :ENTRY_STATE,
                :ENTRY_POSTCODE,
                :ENTRY_COUNTRY_ID,
                :USER_ID,
                :ORGANIZATION_ID
            )",
            [
                ':ENTRY_COMPANY' => $masterOrgDetail['organization_name'],
                ':ENTRY_FIRSTNAME' => $this->request['requestData']['invName'],
                ':ENTRY_LASTNAME' => null,
                ':ENTRY_STREET_ADDRESS' => $this->request['requestData']['invAddress1'],
                ':ENTRY_STREET_ADDRESS_2' => isset($this->request['requestData']['invAddress2']) ? $this->request['requestData']['invAddress2'] : null,
                ':ENTRY_CITY' => $this->request['requestData']['invTown'],
                ':ENTRY_SUBURB' => isset($this->request['requestData']['invAddress3']) ? $this->request['requestData']['invAddress3'] : null,
                ':ENTRY_STATE' => $this->request['requestData']['invCounty'],
                ':ENTRY_POSTCODE' => $this->request['requestData']['invPostCode'],
                ':ENTRY_COUNTRY_ID' => $this->request['requestData']['invCountry'],
                ':USER_ID' => null,
                ':ORGANIZATION_ID' => $this->request['requestData']['orgId']
            ]
        );

        $invAddressID = dbid();

        dbq("update organizations set organization_invoice_email = :EMAIL,
                organization_invoice_address_id = :ADDID,
                organization_invoice_phone = :INVPHONE,
                organization_can_bulk = 1
                where organization_id = :ORGID OR organization_parent_organization_id = :ORGID",
            [
                ':EMAIL' => $this->request['requestData']['billemailaddress'],
                ':ADDID' => $invAddressID,
                ':INVPHONE' => $this->request['requestData']['billPhone'],
                ':ORGID' => $masterOrg
            ]
        );

        // make file and email it here.
        include_once('includes/classes/PHPExcel.php');

        $fileName = 'blank_invoicing_data.xls';

        $countryName = getCountryName($this->request['requestData']['invCountry']);

        try {

            $inputFileType = PHPExcel_IOFactory::identify($fileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($fileName);

            // Change the file
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A4', 'Customer Name: ('.$masterOrgDetail['school_URN'].') '.$masterOrgDetail['organization_name'])
                ->setCellValue('B9', $countryName)
                ->setCellValue('B10', $this->request['requestData']['invAddress1'])
                ->setCellValue('B11', isset($this->request['requestData']['invAddress2']) ? $this->request['requestData']['invAddress2'] : '')
                ->setCellValue('B12', isset($this->request['requestData']['invAddress3']) ? $this->request['requestData']['invAddress3'] : '')
                ->setCellValue('B13', $this->request['requestData']['invTown'])
                ->setCellValue('B14', $this->request['requestData']['invPostCode'])
                ->setCellValue('B18', $this->request['requestData']['invName'])
                ->setCellValue('B19', $this->request['requestData']['billPhone'])
                ->setCellValue('B20', 'N/A')
                ->setCellValue('B21', $this->request['requestData']['billemailaddress'])
                ->setCellValue('B30', 'Date: '.date('d-m-y'))
            ;

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $inputFileType);
            $objWriter->save('invoiceRequests/'.$masterOrgDetail['school_URN'].'_invoicing_data.xls');

            // email it.

            $emailContent = '('.$masterOrgDetail['school_URN'].') '.$masterOrgDetail['organization_name']." have requested to open an invoice account.  Please find CX form attached.
<br />
<br />
Yours sincerely,<br />
<br />
The F&F Uniforms Team";

            $subject = 'Invoice Account request from F&F Uniforms';
            $title = $subject;
            $template = 'tesco-ff-uniforms';
            $tag = ['ff-inv-request'];

            $attachmentArray[] = [
                'type' => "application/vnd.ms-excel",
                'name' => basename('invoiceRequests/'.$masterOrgDetail['school_URN'].'_invoicing_data.xls'),
                'content' => base64_encode(file_get_contents(API_ROOT . 'invoiceRequests/'.$masterOrgDetail['school_URN'].'_invoicing_data.xls'))
            ];

            $emailAddress = [
                'Andrew Lindsay' => 'andy@slickstitch.com',
                'School Embroidery Queries' => 'School.embqueries@uk.tesco.com'
            ];

            $message = $emailContent;

            $template_content = array(
                [
                    'name' => 'emailtitle',
                    'content' => $title
                ],
                [
                    'name' => 'main',
                    'content' => $message
                ]
            );

            send_ues_mail($emailAddress, $subject, $template_content, $template, $tag, $attachmentArray);

        } catch(Exception $e) {
            debugMail($e->getMessage());
        }

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Your form has been submitted.  You can now place bulk orders.'
        ];

        refreshSession();

        $data['session'] = $_SESSION['session'];

        return $data;
    }



}

