<?php

/**
 * Rest object example
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class forms extends restObject {

	/**
	 *
	 * @param string $method
	 * @param array $request
	 * @param string $file
	 */
	public function __construct($method, $request = null, $file = null) {
		parent::__construct($method, $request, $file);
	}


	public function elements() {

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}
		// Get all the standard drop down elements.
		$data = [];

		// Title
		$titleQuery = dbq("select user_title_id, user_title from user_titles where user_title_id != 1");

		while($title = dbf($titleQuery)){
			$data['titles'][] = [
				'id' => $title['user_title_id'],
				'title' => $title['user_title']
			];
		}
		// Type
		$orgTypeQuery = dbq("select * from organization_types");

		while($orgType = dbf($orgTypeQuery)){
			$data['types'][] = [
				'id' => $orgType['organization_type_id'],
				'title' => $orgType['organization_type_name'],
				'membernoun' => $orgType['organization_member_noun']
			];
		}

		// Countries
		$countriesQuery = dbq("select * from countries where country_live = 1");

		while($countries = dbf($countriesQuery)){
			$data['countries'][] = [
				'id' => $countries['countries_id'],
				'title' => $countries['countries_name'],
			];
		}
		// Education Phase
		$phaseQuery = dbq("select * from education_phases");

		while($phase = dbf($phaseQuery)){
			$data['phases'][] = [
				'id' => $phase['education_phase_id'],
				'title' => $phase['education_phase_name'],
			];
		}
		// Local Authorities
		$laQuery = dbq("select * from local_authorities");

		while($la = dbf($laQuery)){
			$data['leas'][] = [
				'id' => $la['local_authority_id'],
				'title' => $la['local_authority_name'],
			];
		}

		// Colours
		$colourQuery = dbq("select * from colours ORDER BY colour_name");

		while($colour = dbf($colourQuery)){
			$data['colours'][] = [
				'id' => $colour['colour_id'],
				'title' => $colour['colour_name'],
				'rgb' => $colour['colour_rgb'],
			];
		}

		// Categories
		$catQuery = dbq("SELECT pc.*, COUNT(p.`product_id`) as plaincount FROM product_categories pc LEFT JOIN products p ON (p.product_category_id = pc.`product_category_id` AND p.`product_decorated` = 0 ) where product_category_active = 1 GROUP BY pc.`product_category_id`");

		while($cat = dbf($catQuery)){
			$data['categories'][] = [
				'id' => $cat['product_category_id'],
				'title' => $cat['product_category_name'],
				'plain' => $cat['plaincount']
			];
		}

		if (!isset($_SESSION['session']['user']['user_type_id']) || $_SESSION['session']['user']['user_type_id'] == 1) {
			// Admin Only after here so return what we have if not admin.
			return $data;
		}
		// Admin things?

		// Categories
		$userTypeQuery = dbq("SELECT * from user_types where user_type_id != 1");

		while($userType = dbf($userTypeQuery)){
			$data['usertypes'][$userType['user_type_id']] = [
				'id' => $userType['user_type_id'],
				'title' => $userType['user_type_name']
			];
		}

		// Tickets
		$ticketTypeQuery = dbq("SELECT * from ticket_types");

		while($ticketType = dbf($ticketTypeQuery)){
			$data['tickettypes'][$ticketType['ticket_type_id']] = [
				'id' => $ticketType['ticket_type_id'],
				'title' => $ticketType['ticket_type_name']
			];
		}

		//firefly
        // Organization Units
        $organizationUnitTypeQuery = dbq("SELECT organization_unit_name, organization_unit_id from organization_units group by organization_unit_name order by organization_unit_id desc;");

        while($organizationUnitType = dbf($organizationUnitTypeQuery)){
            $data['organizationunittypes'][$organizationUnitType['organization_unit_id']] = [
                'id' => $organizationUnitType['organization_unit_id'],
                'title' => $organizationUnitType['organization_unit_name']
            ];
        }


		// Audit
		$auditTypeQuery = dbq("SELECT * from audit_log_events");

		while($auditType = dbf($auditTypeQuery)){
			$data['audittypes'][$auditType['audit_log_event_id']] = [
				'id' => $auditType['audit_log_event_id'],
				'title' => $auditType['audit_log_event_name']
			];
		}

		// Threads
		$threadQuery = dbq("SELECT * from threads");

		while($thread = dbf($threadQuery)){
			$txtcolour = isBright($thread['colour_rgb']) ? '#000' : '#fff';
			$data['threads'][$thread['colour_code']] = [
				'id' => $thread['thread_id'],
				'title' => $thread['colour_full_name'],
				'rgb' => $thread['colour_rgb'],
				'text_rgb' => $txtcolour
			];
		}

		// Withdrawal reasons
		$withdrawReasonQuery = dbq('SELECT * FROM organization_withdraw_reasons where organization_withdraw_reason_active = 1 order by organization_withdraw_reason_id ASC');

        while($getwithdrawReason = dbf($withdrawReasonQuery))
        {
            $data['withdraw_reasons'][] = [
                'id' => $getwithdrawReason['organization_withdraw_reason_id'],
                'title' => $getwithdrawReason['organization_withdraw_reason_text'],
            ];
        }

		return $data;
	}

	public function sendcontact(){
		/*
		 formdata
			firstName
				"yturt"
			lastName
				"rtyurtyu"
			email
				"rtyurtyu@rtyery.com"
			message
				"sfasfasdfas asdfasf asdf asd"
		*/

		$data = [
			'type' => 401, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime(),
			]  // Hold flags and metrics required to progress app.
		];

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		if(!isset($this->request['formdata']) || empty($this->request['formdata'])){
			$data['messages'][] = [
				'type' => 'danger',
				'message' => 'You must fill in all required fields.'
			];

			return $data;
		}

		if(!isset($this->request['formdata']['email']) || empty($this->request['formdata']['email'])){
			$data['messages'][] = [
				'type' => 'danger',
				'message' => 'An email address is required.'
			];

			return $data;
		}

		if( !isset($this->request['formdata']['message'])
			|| empty($this->request['formdata']['message'])
			|| strlen($this->request['formdata']['message']) < 10){
			$data['messages'][] = [
				'type' => 'danger',
				'message' => 'Your message appears to be too short.  please check your message.'
			];

			return $data;
		}

		// got here, lets try and send.

		// TODO  add to a database table

		$subject = 'Contact from F&F Uniforms';
		$title = $subject;
		$template = 'tesco-ff-uniforms';
		$tag = ['ff-contact-form'];

		$emailAddress = [
			'F&F Uniform Support' => 'ues@uniformembroideryservice.com',
			'Andrew Lindsay' => 'andy@slickstitch.com',
		];

		$toName = $this->request['formdata']['firstName']." ".$this->request['formdata']['lastName'];
		$toEmail = $this->request['formdata']['email'];
        $URN = ( !isset($this->request['formdata']['urn']) || empty($this->request['formdata']['urn'])) ? 'not provided' : $this->request['formdata']['urn'];
        $sentmessage = nl2br(strip_tags($this->request['formdata']['message']));


		$html = "There has been a new contact from the F&F Uniforms site.<br />
<br />
<br />
Name: $toName<br />
Email: $toEmail<br />
URN : $URN<br />
Message: $sentmessage<br />
<br />
<br />
<br />
Yours sincerely,<br />
<br />
The F&F Uniforms Team";


		/*
		$emailAddress = [
			$toName => $user['user_email']
		];
		*/

		$template_content = array(
			[
				'name' => 'emailtitle',
				'content' => $title
			],
			[
				'name' => 'main',
				'content' => $html
			]
		);

		send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);

		$data['type'] = 200;
		$data['messages'][] = [
			'type' => 'success',
			'message' => 'Your message has been sent.  We will contact you shortly.'
		];
		// check
		$data['request'] = $this->request;
		return $data;

	}
}
