<?php

/**
 * Rest object example
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class admin extends restObject
{

    /**
     * user data
     */
    public $id;
    public $name;
    public $lastName;
    public $login;

    /**
     *
     * @param string $method
     * @param array $request
     * @param string $file
     */
    public function __construct($method, $request = null, $file = null)
    {
        if (!isset($_SESSION['session']['user']['user_type_id']) || $_SESSION['session']['user']['user_type_id'] == 1) {
            // Admin Only so return
            $this->setError([
                'message' => 'not authorized.',
                'errorCode' => 'authorization-required',
            ]);
            return $this->getResponse(500);
        }
        parent::__construct($method, $request, $file);
    }


    public function getFAQs()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['user']) || empty($this->request['user'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // update it.
        $userQuery = dbq("SELECT * FROM users
								WHERE user_email = :EMAILADDRESS", [':EMAILADDRESS' => $this->request['user']]);

        if (dbnr($userQuery) > 0) {
            $user = dbf($userQuery);
            $newpass = $this->generateRandomString();
            $newPassHash = password_hash($newpass, PASSWORD_BCRYPT);

            $userQuery = dbq("UPDATE users SET user_password = :NEWPASS
								WHERE user_id = :USERID", [':NEWPASS' => $newPassHash, ':USERID' => $user['user_id']]);

            // Send an email
            ues_mail('TEST NAME', 'artworkfiles@gmail.com', 'Password Reset', 'New Password Is: ' . $newpass . "\n\n", 'F&F Uniforms', 'noreply@ff-ues.com');

            $data = [
                'type' => 200,
                'messages' => ['password changed.  Please check your email.'] // list of messages.
            ];

        } else {
            // error not found.
            $data = [
                'type' => 401, // Not authorized as standard
                'messages' => ['email address not found.' . $this->request['user']] // list of messages.
            ];
        }

        return $data;
    }

    public function getadminusers(){

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }



        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $userQuery = dbq("select * from users where user_type_id != 1");
        while($user = dbf($userQuery)){

            $data['data'][] = $user;
        }

        return $data;
    }


    public function getorgnotes(){

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgid']) || empty($this->request['orgid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

//firefly
//        $ticketQuery = dbq("select
//                            t.ticket_id,
//                            t.ticket_subject,
//                            t.ticket_description,
//                            t.ticket_type_id,
//                            t.ticket_open_date,
//                            t.ticket_alert,
//                            t.ticket_alert_date,
//                            IF(ISNULL(t.ticket_user_name),CONCAT(u.`user_first_name`,' ',u.`user_last_name`),t.ticket_user_name) AS ticket_user_name
//                            from tickets t
//                            join ticket_types using (ticket_type_id)
//                            left join users u on (u.user_id = t.ticket_user_id)
//                            where t.ticket_organization = :TICKETORG
//                            order by ticket_open_date desc",[
//                    ':TICKETORG' => $this->request['orgid']
//            ]
//        );

        $ticketQuery = dbq("select 
                        t.ticket_id,
                        t.ticket_subject,
                        t.ticket_description,
                        t.ticket_type_id,
                        t.ticket_open_date,
                        t.ticket_alert,
                        t.ticket_alert_date,
                        IF(ISNULL(t.ticket_user_name),CONCAT(u.`user_first_name`,' ',u.`user_last_name`),t.ticket_user_name) AS ticket_user_name,
                        u2.ticket_id, 
                        u2.user_id
                        from tickets t
                        join ticket_types using (ticket_type_id) 
                        left join users u on (u.user_id = t.ticket_user_id)
                        left join tickets_to_users u2 on (u2.ticket_id = t.ticket_id)
                        where t.ticket_organization = :TICKETORG and  u2.user_id is null
                        order by ticket_open_date desc",[
                ':TICKETORG' => $this->request['orgid']
            ]
        );
        while($ticket = dbf($ticketQuery)){
            foreach($ticket as $key => $value){
                if(stristr($key,"date")){
                    $ticket[$key] = strtotime($value) * 1000;
                }
            }
            $data['data'][] = $ticket;

        }




        $ticketPrivateQuery = dbq("select 
                        t.ticket_id,
                        t.ticket_subject,
                        t.ticket_description,
                        t.ticket_type_id,
                        t.ticket_open_date,
                        t.ticket_alert,
                        t.ticket_alert_date,
                        IF(ISNULL(t.ticket_user_name),CONCAT(u.`user_first_name`,' ',u.`user_last_name`),t.ticket_user_name) AS ticket_user_name,
                        u2.ticket_id, 
                        u2.user_id
                        from tickets t
                        join ticket_types using (ticket_type_id) 
                        left join users u on (u.user_id = t.ticket_user_id)
                        left join tickets_to_users u2 on (u2.ticket_id = t.ticket_id)
                        where t.ticket_organization = :TICKETORG and  u2.user_id = :USERID
                        order by ticket_open_date desc",[
                ':TICKETORG' => $this->request['orgid'],
                ':USERID' => $_SESSION['session']['user']['user_id']

            ]
        );
        while($ticketpriv = dbf($ticketPrivateQuery)){
            foreach($ticketpriv as $key => $value){
                if(stristr($key,"date")){
                    $ticketpriv[$key] = strtotime($value) * 1000;
                }
            }
             $data['datapriv'][] = $ticketpriv;
        }

        return $data;
    }



    public function getorgaudit(){

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgid']) || empty($this->request['orgid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $ticketQuery = dbq("SELECT al.audit_timestamp, al.audit_log_event_id, IF(ISNULL(al.`audit_log_name`),CONCAT(u.`user_first_name`,' ',u.`user_last_name`),al.`audit_log_name`) AS audit_log_name, al.audit_event_info
                                FROM audit_log al
                                LEFT JOIN users u USING (user_id)
                                WHERE al.organization_id = :OID
                                order by al.audit_timestamp desc, al.`audit_log_id` DESC",[
                ':OID' => $this->request['orgid']
            ]
        );
        while($ticket = dbf($ticketQuery)){
            foreach($ticket as $key => $value){
                if(stristr($key,"audit_timestamp")){
                    $ticket[$key] = strtotime($value) * 1000;
                }
            }
            $data['data'][] = $ticket;
        }

        return $data;
    }

    public function removeemblem(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['embData']) || empty($this->request['embData'])){
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // Should rename it and delete it on ebos too.
        include_once(DIR_CLASSES . 'eBOS_API.php');
        $eBOS_API = new eBOS_API(APIUser, APIPass);

        $newDesignNumber = $this->request['embData']['tesco_style_ref'].shorten(time()); // rename it so we dont get duplicates.

        // Update system first?

        dbq("update organization_logos set logo_deleted = 1, tesco_style_ref = :NEWREF, needs_to_update_ebos = 1, update_customer = 0, alert_user = 0 where organization_logo_id = :OLID",
            [
                ':NEWREF' => $newDesignNumber,
                ':OLID' => $this->request['embData']['organization_logo_id']
            ]
        );

        audit(15,$this->request['embData']['organization_id'],$this->request['embData']['tesco_style_ref']);

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Emblem removed.'
        ];

     /*   if(empty($this->request['embData']['ebos_id'])){

            $designs = $eBOS_API->getDesigns($this->request['embData']['tesco_style_ref']);

            if (!empty($designs->designs)) {
                // Change logos on eBOS
                foreach ($designs->designs as $design) {
                    $designDetail = $eBOS_API->getDesignDetails($design->id);
                    if($designDetail->design->number == $this->request['embData']['tesco_style_ref']){
                        //Ok lets delete it.
                        // $designDetail->design->number = $newDesignNumber;
                        // $designDetail->design->name = $newDesignNumber;
                        // $designDetail->design->notes = $designDetail->design->notes."\n\n".'Removed from F&F UES. - '.date('r').' - '.$_SESSION['session']['user']['user_first_name'].' '.$_SESSION['session']['user']['user_last_name'];
                        // $designDetail->design->destroyed_at = date('c'); // 2016-03-07T16:34:50+00:00

                        $designChanges = [
                            'design' => [
                                'number' => $newDesignNumber,
                                'name' => $newDesignNumber,
                                'notes' => $designDetail->design->notes."\n\n".'Removed from F&F UES. - '.date('r').' - '.$_SESSION['session']['user']['user_first_name'].' '.$_SESSION['session']['user']['user_last_name']
                            ]
                        ];

                        $designUpdate = $eBOS_API->updateDesign($design->id,$designChanges);
                        if(isset($designUpdate->successful) && $designUpdate->successful == true) {
                            $deleteDesign = $eBOS_API->deleteDesign($this->request['embData']['ebos_id']);
                        }
                        $data['data']['designDetail'] = (array)$designUpdate;
                        break;
                    }
                    $designDetail = null;
                }
            } else {
                // Not on ebos so lets just set it as deleted in database.
            }

        } else {
            $designDetail = $eBOS_API->getDesignDetails($this->request['embData']['ebos_id']);
            //Ok lets delete it.

            $designChanges = [
                'design' => [
                    'number' => $newDesignNumber,
                    'name' => $newDesignNumber,
                    //TODO this line is throwing error while deleting emblem on admin
                    //'notes' => $designDetail->design->notes."\n\n".'Removed from F&F UES. - '.date('r').' - '.$_SESSION['session']['user']['user_first_name'].' '.$_SESSION['session']['user']['user_last_name']
                ]
            ];

            $designUpdate = $eBOS_API->updateDesign($this->request['embData']['ebos_id'],$designChanges);
            if(isset($designUpdate->successful) && $designUpdate->successful == true) {
                $deleteDesign = $eBOS_API->deleteDesign($this->request['embData']['ebos_id']);
            }

            $designDetail = null;

        }*/

        return $data;
    }

    public function updateemblemrequest(){
        // take files and create new logo for it?

    }

    public function addemblemrequest(){


        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['requestData']) || empty($this->request['requestData'])){
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        if(!isset($this->request['requestData']['description']) || empty($this->request['requestData']['description'])){

            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Please fill out a description.'
            ];
            return $data;
        }

        if(!isset($this->request['requestData']['orgid']) || empty($this->request['requestData']['orgid'])){

            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Org ID missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime(),
                'messagetime' => 3000
            ]  // Hold flags and metrics required to progress app.
        ];
        //Firefly grab master orgid then  check

        $masterOrgID =  getMasterOrgID($this->request['requestData']['orgid']);



        $mastercheck =(int)$masterOrgID;
        $parsecheck = (int)$this->request['requestData']['orgid'];


        if($mastercheck !== $parsecheck) {
            $masterOrgID = $parsecheck;
        }


        $changeType = $this->request['requestData']['logoType'];

        $this->request['requestData']['description'] .= "\n\n"."Logo Size: ".$this->request['requestData']['logoWidth'].'mm wide x '.$this->request['requestData']['logoHeight'].'mm high';

        $attachments = false;
        $attachmentArray = null;
        $attachmentEbosArray = null;
        $ticketAttachmentArray = [];
        $firstEmblem = '';
        if (is_array($this->request['requestData']['attachments'])) {
            foreach ($this->request['requestData']['attachments'] as $attachment) {

                if ($attachment['type'] == 'emblem') {

                    if(empty($firstEmblem)){
                        $firstEmblem = $attachment['name'];
                    }

                    // just a URL.
                    $attachmentArray[] = [
                        'type' => "image/png",
                        'name' => basename($attachment['url']),
                        'content' => base64_encode(file_get_contents('http:'.$attachment['url']))
                    ];

                    $attachmentEbosArray[] = [
                        'filename' => basename(HTTP_ROOT.$attachment['url']),
                        'data' => base64_encode(file_get_contents('http:'.$attachment['url'])),
                        'mime_type' => "image/png"
                    ];

                    $ticketAttachmentArray[] = 'http:'.$attachment['url'];

                    $attachments = true;
                } else {


                    //Firefly $masterOrgId =  getMasterOrgID($this->request['requestData']['orgid']);
                    //Save file to server.
                    $filedata = @end(explode('base64,',$attachment['url'],2));
                    $filedata = base64_decode($filedata);

                    if (!file_exists('attachments/' . $masterOrgID)) {
                        mkdir('attachments/' . $masterOrgID);
                    }

                    $attachment['name'] = str_replace(' ','_',$attachment['name']); // handle invalid characters?

                    $newfilename = explode('.', $attachment['name']);
                    array_splice($newfilename, sizeof($newfilename) - 1, 0, shorten(time()));

                    $newfilename = 'attachments/' . $masterOrgID . '/' . implode('.', $newfilename);

                    file_put_contents($newfilename,$filedata);

                    if(empty($attachment['type'])){
                        $attachment['type'] = 'text/plain';
                    }

                    $attachmentArray[] = [
                        'type' => $attachment['type'],
                        'name' => basename(API_ROOT.$newfilename),
                        'content' => base64_encode(file_get_contents(API_ROOT.$newfilename))
                    ];

                    $attachmentEbosArray[] = [
                        'filename' => basename(API_ROOT.$newfilename),
                        'data' => base64_encode(file_get_contents(API_ROOT.$newfilename)),
                        'mime_type' => $attachment['type']
                    ];

                    $ticketAttachmentArray[] = API_ROOT.$newfilename;
                    $attachments = true;
                    sleep(1);
                }

            }
            if(empty($firstEmblem) && $changeType == "existing"){
                // shouldnt be here, need to know what emblem to change.
                $data['type'] = 401;
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'Please include the emblem to update.'
                ];
                return $data;
            }
        }

       //Firefly $masterOrgID = getMasterOrgID($this->request['requestData']['orgid']);

        // work out next logo ID.
        $logoCodeQuery = dbq("select distinct(tesco_style_ref) from organization_logos where organization_id = :ORGID and logo_deleted != 1",
            [
                ':ORGID' => $masterOrgID
            ]
        );

        //"existing"

        if($changeType == "existing"){

            $existingCodeParts = explode('-', str_replace('-TMP','',$firstEmblem));
            $codeParts = end($codeParts); // split URN and Logo Code

            $codeNumber = filter_var($codeParts,FILTER_SANITIZE_NUMBER_INT);
            $codeLetter = str_replace($codeNumber,'',$codeParts);

            $codeSuffix = [
                $codeNumber,
                $codeLetter
            ];

            // Highest letter for same number
            while ($logoCode = dbf($logoCodeQuery)) {
                $codeParts = str_replace('-TMP', '', $logoCode['tesco_style_ref']); // get rid of the TMP suffix.
                $codeParts = @end(explode('-', $codeParts)); // split URN and Logo Code

                $codeNumber = filter_var($codeParts,FILTER_SANITIZE_NUMBER_INT);
                $codeLetter = str_replace($codeNumber,'',$codeParts);

                if ($codeNumber == $codeSuffix[0]) {
                    // this is the same logo (in theory)
                    if ($codeLetter > $codeSuffix[1]) {
                        // set it to the highest letter found for safety.
                        $codeSuffix[1] = $codeLetter;
                        //$codeSuffix[1]++;

                    }
                }
            }

            $codeSuffix[1]++;

        } else {

            $codeSuffix = [
                0,
                'A'
            ];

            $stages[] = $codeSuffix;
            // add a new number.
            while ($logoCode = dbf($logoCodeQuery)) {

                $codeParts = explode('-', str_replace('-TMP', '', $logoCode['tesco_style_ref']));
                $codeParts = end($codeParts); // split URN and Logo Code

                $codeNumber = filter_var($codeParts,FILTER_SANITIZE_NUMBER_INT);
                $codeLetter = str_replace($codeNumber,'',$codeParts);

                if ($codeNumber > $codeSuffix[0]) {
                    // set it to the highest number found for safety.  Only do this for NEW designs.
                    $codeSuffix[0] = $codeNumber;
                    $stages[] = $codeSuffix;
                }
            }
            $codeSuffix[1] = 'A';
            $codeSuffix[0]++;

        }

        if(!is_numeric($codeSuffix[0])){
            $emailcontent = $firstEmblem;
            debugMail($emailcontent);
        }

        $orgInfo = getOrgDetails($this->request['requestData']['orgid']);
        $masterOrgInfo = getOrgDetails($masterOrgID);

        $logoOrgName = $orgInfo['organization_name'];

        if($orgInfo['organization_id'] != $masterOrgInfo['organization_id'] ){
            // house. change name.
            $logoOrgName = $masterOrgInfo['organization_name'];
        }

        $newCode = $masterOrgInfo['school_URN'] . '-' . implode('', $codeSuffix);


        // Add a placeholder design into eBOS design Tool.
        include_once(DIR_CLASSES . 'eBOS_API.php');

        $eBOS_API = new eBOS_API(APIUser, APIPass);

        $ticketmessage = $this->request['requestData']['description']."\n\n".implode("\n",$ticketAttachmentArray);

        $designdetail = [
            'design' => [
                'number' => $newCode.'-TMP',  // random string for testing.  Must be unique - No Spaces.
                'name' => $logoOrgName,  // random name for testing
                'notes' => 'Created by F&F UES'."\n\n".$ticketmessage,
                'threads' => [
                    '1000'
                ], // Must be strings not integers.
                'dst' => [
                    'data' => base64_encode(file_get_contents('dst/tempdesign.dst')),
                    'filename' => 'tempdesign.dst'
                ]
            ]
        ];

        $newDesign = $eBOS_API->createDesign($designdetail);  // Works.

        if (isset($newDesign->successful) && $newDesign->successful == false) {
            $data['type'] = 401;

            $data['messages'][] = [
                'type' => 'danger',
                'message' => $newDesign->message
            ];

            return $data;
        }

        $alertUser = 0;//$_SESSION['session']['user']['user_id'];
        $alertComment = '';
        $threadsUpdated = 1;

        // todo check if this will need a specific alert when files are updated.  e.g. New threads.
        if(isset($this->request['requestData']['existsdescription']) && !empty($this->request['requestData']['existsdescription'])){
            $alertUser = $_SESSION['session']['user']['user_id'];
            $alertComment = $this->request['requestData']['existsdescription'];
            $threadsUpdated = 0;
        }

//firefly print_r("masterorgid is set to" . $masterOrgID);
        // add an entry to logos table.
        dbq("insert into organization_logos (
					organization_id,
					school_name,
					tesco_style_ref,
					stitch_count,
					size__mm_,
					logo_approved,
					logo_url,
					logo_svg_url,
					primary_background_colour,
					logo_requested_name,
					logo_requested_date,
					products_using_logo,
					logo_on_ebos,
					ebos_id,
					alert_user,
					alert_comment,
					threads_updated
				  ) VALUES (
					:ORGANIZATIONID,
					:SCHOOLNAME,
					:TESCOSTYLEREF,
					:STITCHCOUNT,
					:SIZEMM,
					:LOGOAPPROVED,
					:LOGOURL,
					:LOGOSVGURL,
					:PRIMARYBACKGROUNDCOLOUR,
					:LOGOREQUESTEDNAME,
					:LOGOREQUESTEDDATE,
					:PRODUCTSUSINGLOGO,
					:LOGOONEBOS,
					:EBOSID,
					:LOGGEDINUSER,
					:ALERTCOMMENT,
					:THREADSUPDATED
				  )",
            [
                ':ORGANIZATIONID' => $masterOrgID,
                ':SCHOOLNAME' => $logoOrgName,
                ':TESCOSTYLEREF' => $newCode . '-TMP',
                ':STITCHCOUNT' => $newDesign->design->stitch_count,
                ':SIZEMM' => $newDesign->design->width . 'x' . $newDesign->design->height,
                ':LOGOAPPROVED' => false,
                ':LOGOURL' => '//www.ff-ues.com/images/emblems/tempdesign.png',
                ':LOGOSVGURL' => '//www.ff-ues.com/images/emblems/tempdesign.svg',
                ':PRIMARYBACKGROUNDCOLOUR' => '191,191,189',
                ':LOGOREQUESTEDNAME' => $_SESSION['session']['user']['user_first_name'].' '.$_SESSION['session']['user']['user_last_name'],
                ':LOGOREQUESTEDDATE' => date("Y-m-d G:i:s"),
                ':PRODUCTSUSINGLOGO' => 0,
                ':LOGOONEBOS' => 1,
                ':EBOSID' => $newDesign->design->id,
                ':LOGGEDINUSER' => $alertUser,
                ':ALERTCOMMENT' => $alertComment,
                ':THREADSUPDATED' => $threadsUpdated
            ]
        );

        $newLogoID = dbid();


        if(isset($this->request['requestData']['createOrder']) && $this->request['requestData']['createOrder'] == true){
            // create a digitizing order.
            $newDigitizingOrder = $eBOS_API->createDigitizingOrder('new',$_SESSION['session']['user']['user_email']);

            if (isset($newDigitizingOrder->successful) && $newDigitizingOrder->successful == true) {
                $newdetails = [
                    'client_order_number'=>$newCode,
                    'notification_email_addresses'=>[$_SESSION['session']['user']['user_email']],
                    'special_instructions' => "SAMPLE SENT FROM SAPPHIRE\n\n".$this->request['requestData']['description']."\n\n".EBOS_DIGITIZING_FOOTER,
                    'dst_required' => true,
                    'emb_required' => true,
                    'pdf_required' => true,
                    'attachments' => $attachmentEbosArray // Not using attachments.
                ];

                $updateDigitizingOrderDetails = $eBOS_API->updateDigitizingOrder($newDigitizingOrder->order->id,$newdetails);



                // add digitized id to organization logo
                $updateQuery = dbq("UPDATE organization_logos SET ebos_digitizing_id = :EBOS_DIGITIZING_ID WHERE organization_logo_id = :ORGANIZATION_LOGO_ID ",
                    [
                        ':EBOS_DIGITIZING_ID'   => $newDigitizingOrder->order->id,
                        ':ORGANIZATION_LOGO_ID' => $newLogoID
                    ]);


                // add a note about digitizing order.
                $ticketmessage = 'eBOS digitizing order: '.$updateDigitizingOrderDetails->order->ebos_order_number."\n\n".$ticketmessage;
                $extrasuccess = ' eBOS Order Number: '.$updateDigitizingOrderDetails->order->ebos_order_number;
                $data['messages'][] = [
                    'type' => 'info',
                    'message' =>  'eBOS Order Number: '.$updateDigitizingOrderDetails->order->ebos_order_number
                ];
            } else {
                // Error email?
                $ticketmessage = '!!!! Couldnt create eBOS order !!!!'."\n\n".json_encode((array)$newDigitizingOrder)."\n\n".$ticketmessage;
                $extrasuccess = ' !!!! Couldnt create eBOS order !!!!';
            }

        } else {
            $data['messages'][] = [
                'type' => 'warning',
                'message' => 'No eBOS order was created.'
            ];
            $extrasuccess = '';
        }

        // Update stage if applicable.
        if($masterOrgInfo['organization_status_id'] == 4){
            // registered. Update to stage 2 now.
            dbq("update organizations set organization_status_id = 5 where organization_id = :MASTERORG or organization_parent_organization_id = :MASTERORG",
                [
                    ':MASTERORG' => $masterOrgInfo['organization_id']
                ]
            );
            dbq("update school_data set school_status_id = 5 where school_data_id = :SDID",
                [
                    ':SDID' => $masterOrgInfo['school_data_id']
                ]
            );
        }

        // Update previous stage if withdrawn/suspended so that when put live its correct stage.
        if($masterOrgInfo['organization_previous_status_id'] == 4){
            // registered. Update to stage 2 now.
            dbq("update organizations set organization_previous_status_id = 5 where organization_id = :MASTERORG or organization_parent_organization_id = :MASTERORG",
                [
                    ':MASTERORG' => $masterOrgInfo['organization_id']
                ]
            );

        }

        /*  Dont need an email if admin set it up.  // Send an email.

        $subject = 'New Logo Request';
        $title = $subject;
        $template = 'tesco-ff-uniforms';
        $tag = ['ff-new-emblem'];

        $SCHOOLNAME = $logoOrgName . ' - (' . $masterOrgInfo['school_URN'] . ')';

        $html = "
		A new emblem request has been made from	$SCHOOLNAME.<br />
					<br />
					Emblem Code: $newCode<br />
					<br />
					$ebosMessage
					<br />
					The description given is:<br />".$this->request['requestData']['description']."<br /><br />";

        if($attachments){
            // add info about them.
            $html .= "The user has uploaded the attached files for reference.<br /><br />";
        }

        $emailAddress = [
            //'F&F Uniform Support' => 'ues@uniformembroideryservice.com',
            'Andrew Lindsay' => 'andy@slickstitch.com'
        ];

        $template_content = array(
            [
                'name' => 'emailtitle',
                'content' => $title
            ],
            [
                'name' => 'main',
                'content' => $html
            ]
        );

        send_ues_mail($emailAddress, $subject, $template_content, $template, $tag, $attachmentArray);*/

        // record activity.
        audit(4, $masterOrgID, $newCode.'-TMP');
        // Add as to-do Note.

        // add attachment links to
        addTicket(13, $masterOrgID, 'New Emblem Request.', $ticketmessage);

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Request added.'
        ];

        $data['metrics']['messagetime'] = 1500 + (sizeof($data['messages']) * 1500); // 1.5 seconds per message.

        return $data;
    }

    public function getorgemblems(){

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['urn']) || empty($this->request['urn'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $orgID = getOrgIDFromURN($this->request['urn']);

        //firefly $masterOrgID = getMasterOrgID($orgID); // house?


        // Firefly check if the Master Record has changes and use suborgid
        $mastercheck =(int)$masterOrgID;
        $parsecheck = (int)$this->request['orgid'];


        if($mastercheck !== $parsecheck) {
            $masterOrgID = $parsecheck;
        }



        $logoQuery = dbq("SELECT * FROM organization_logos
								WHERE organization_id = :OID
								AND logo_deleted != 1
								ORDER BY logo_approved DESC, tesco_style_ref ASC",[
                ':OID' => $masterOrgID
            ]
        );

        // emblems
        $logoArray = [];
        while ($logo = dbf($logoQuery)) {
            $logoArray[] = $logo;
        }

        $data['data']['emblems'] = $logoArray;

        return $data;
    }

    public function getorgdetail(){

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgid']) || empty($this->request['orgid'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $organizationQuery = dbq("select *, o.*, o.organization_name as 'full_title_name', op.organization_name as 'organization_house_parent_name' from organizations o
                                      JOIN school_statuses ss on (o.organization_status_id = ss.school_status_id)
                                      left join organizations op on (op.organization_id = o.organization_parent_organization_id)
                                      left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id)
                                      left join addresses a on (a.address_id = o.organization_default_address_id)
                                      left join organization_bank_accounts oba on (oba.organization_id = o.organization_id)
                                      where o.organization_id = :OID",[
                ':OID' => $this->request['orgid']
            ]
        );

     // Firefly changed to just grabbing curent orgid
        $masterOrgID = $this->request['orgid'];
        $organization = dbf($organizationQuery);

        if(!empty($organization['organization_house_parent_name']) && !stristr($organization['organization_name'],$organization['organization_house_parent_name'])){
            //if(!stristr($organization['organization_name'],$organization['organization_house_parent_name'])){
            // name not in house name.
            //$organization['organization_name'] = trim(str_ireplace($organization['organization_house_parent_name'],'',$organization['organization_name']));
            $organization['full_title_name'] = $organization['organization_house_parent_name'];
            $organization['full_title_name_sub'] =  $organization['organization_name'];
            //}
        }

        $data['data']['orgdetail'] = $organization;

        // get users.

        $organizationUsersQuery = dbq("select * from users u join users_to_organizations u2o using (user_id)
                                      where u2o.organization_id = :OID",[
                ':OID' => $masterOrgID
            ]
        );

        while($organizationUsers = dbf($organizationUsersQuery)){
            $data['data']['users'][] = $organizationUsers;
        }

        // Firefly check if the Master Record has changes and use suborgid

        $mastercheck =(int)$masterOrgID;
        $parsecheck = (int)$this->request['orgid'];

        if($mastercheck !== $parsecheck) {
            $masterOrgID = $parsecheck;
        }

        //grab school_data_id and use to group results
        $organization_school_id = $organization['school_data_id'];



        // firefly change get houses.

//        $organizationHouseQuery = dbq("select *, o.* from organizations o
//                                      left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id and ol.logo_deleted != 1)
//                                      left join addresses a on (a.address_id = o.organization_default_address_id)
//                                      where o.organization_parent_organization_id = :OPOID",[
//                ':OPOID' => $masterOrgID
//            ]
//        );
//        $data['data']['houses'] = [];
//        while($organizationHouse = dbf($organizationHouseQuery)){
//            $data['data']['houses'][] = $organizationHouse;
//        }
//


        $organizationHouseQuery = dbq("select *, o.* from organizations o
left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id and ol.logo_deleted != 1)
left join addresses a on (a.address_id = o.organization_default_address_id)
 where o.organization_parent_organization_id=:OPOID order by school_URN_parent",[
                ':OPOID' => $masterOrgID
            ]
        );

        $organizationSubHouseQuery = dbq("SELECT
    concat(trim(t1.organization_name),':',t1.organization_id)  AS lev1,
    concat(trim(t2.organization_name),':',t2.organization_id)  AS lev2,
    concat(trim(t3.organization_name),':',t3.organization_id)  AS lev3,
    concat(trim(t4.organization_name),':',t4.organization_id)  AS lev4,
    concat(trim(t5.organization_name),':',t5.organization_id)  AS lev5,
    concat(trim(t6.organization_name),':',t6.organization_id)  AS lev6,
    concat(trim(t7.organization_name),':',t7.organization_id)  AS lev7,
    concat(trim(t8.organization_name),':',t8.organization_id)  AS lev8,
    concat(trim(t9.organization_name),':',t9.organization_id)  AS lev9,
    concat(trim(t10.organization_name),':',t10.organization_id)  AS lev10
FROM
    organizations AS t1
    LEFT JOIN organizations AS t2 ON
    t2.organization_parent_organization_id = t1.organization_id
    LEFT JOIN organizations AS t3 ON
    t3.organization_parent_organization_id = t2.organization_id
    LEFT JOIN organizations AS t4 ON
    t4.organization_parent_organization_id = t3.organization_id
    LEFT JOIN organizations AS t5 ON
    t5.organization_parent_organization_id = t4.organization_id
    LEFT JOIN organizations AS t6 ON
    t6.organization_parent_organization_id = t5.organization_id
    LEFT JOIN organizations AS t7 ON
    t7.organization_parent_organization_id = t6.organization_id
    LEFT JOIN organizations AS t8 ON
    t8.organization_parent_organization_id = t7.organization_id
    LEFT JOIN organizations AS t9 ON
    t9.organization_parent_organization_id = t8.organization_id
    LEFT JOIN organizations AS t10 ON
    t10.organization_parent_organization_id = t9.organization_id

WHERE
    t1.organization_id = :OPOID ",[
                ':OPOID' => $masterOrgID
            ]
        );

//
//        $organizationSubHouseUnitsQuery = dbq("SELECT
//                  concat(concat(trim(organizations.organization_name), ':', trim(organizations.organization_id)),'*',concat(trim(organizations.organization_name), ' (', trim(organization_units.organization_unit_name), ')', ':', trim(organizations.organization_id))) as nameclass
//                FROM
//                  organizations
//                  INNER JOIN organization_units ON organizations.organization_id =
//                    organization_units.organization_id
//                where organizations.school_data_id  =:ORGSID ",[
//                ':ORGSID' => $organization_school_id
//            ]
//        );


//firefly
        $orgUserAdminQuery = dbq("
                SELECT
                  users_to_organizations.user_id,
                  users_to_organizations.organization_id,
                  users.user_id AS user_id1,
                  users.user_type_id,
                  user_types.user_type_id AS user_type_id1,
                  user_types.user_type_name,
                  users.user_name,
                  users.user_email,
                  organizations.organization_id AS organization_id1,
                  organizations.school_data_id,
                  concat(    users.user_name, ' (' , organizations.organization_name ,')') as userorg
                FROM
                  users
                  INNER JOIN users_to_organizations ON users.user_id =
                    users_to_organizations.user_id
                  INNER JOIN user_types ON user_types.user_type_id =
                    users.user_type_id
                  INNER JOIN organizations ON users_to_organizations.organization_id = organizations.organization_id
                WHERE
                 organizations.school_data_id = :ORGSID and user_types.user_type_id !=1;",[
                                ':ORGSID' =>    $organization_school_id
                            ]
        );


        $data['data']['houses'] = [];
        $data['data']['subhouses'] = [];
        $data['data']['orgadmins'] = [];
       // $data['data']['subhousesunits'] = [];

        while($organizationHouse = dbf($organizationHouseQuery)){
            $data['data']['houses'][] = $organizationHouse;
        }

        while($organizationSubHouse = dbf($organizationSubHouseQuery)){
            $data['data']['subhouses'][] = $organizationSubHouse;
        }

        while($organizationAdmins = dbf($orgUserAdminQuery)){
            $data['data']['orgadmins'][] = $organizationAdmins;
        }

//        while($organizationSubHouses = dbf($organizationSubHouseUnitsQuery)){
//            $data['data']['subhousesunits'][] = $organizationSubHouses;
//        }
//




       //firefly removed check for masterid $masterOrgID = getMasterOrgID($this->request['orgid']);
        // house?
        $logoQuery = dbq("SELECT * FROM organization_logos
								WHERE organization_id = :OID
								AND logo_deleted != 1
								ORDER BY logo_approved DESC, tesco_style_ref ASC",[
                ':OID' => $masterOrgID
            ]
        );

        // emblems
        $logoArray = [];
        while ($logo = dbf($logoQuery)) {
            $logoArray[] = $logo;
        }

        $data['data']['emblems'] = $logoArray;

        $donationQuery = dbq("SELECT * FROM donations
								WHERE organization_id = :OID",[
                ':OID' => $this->request['orgid']
            ]
        );

        $donationArray = [];
        while ($donation = dbf($donationQuery)) {
            foreach($donation as $key => $value){
                if(stristr($key,"date")){
                    $donation[$key] = strtotime($value) * 1000;
                }
            }

            $donationArray[] = $donation;
        }

        $data['data']['donations'] = $donationArray;

        return $data;
    }



    public function updatedefaultlogo(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['organization_id']) || !isset($this->request['organization_logo_id']) || empty($this->request['organization_id']) || empty($this->request['organization_logo_id'])){
            $data['type'] = 403;
            $data['messages'][] =
                [
                    'type' => 'danger',
                    'message' => 'Cannot provide results, required arguments are missing.'
                ];

            return $data;
        }

        // check logo belongs to org.

        $masterOrgID = getMasterOrgID($this->request['organization_id']);

        $logoCheckQuery = dbq("select * from organization_logos where organization_logo_id = :LOGOID and organization_id = :ORGID",
            [
                ':LOGOID' => $this->request['organization_logo_id'],
                ':ORGID' => $masterOrgID
            ]
        );

        if(dbnr($logoCheckQuery) < 1){
            // error.
            $data['type'] = 403;
            $data['messages'][] =
                [
                    'type' => 'danger',
                    'message' => 'Logo does not belong to this organization.'
                ];

            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        dbq("update organizations set organization_default_logo_id = :LOGOID, organization_default_logo_update = 1 where organization_id = :ORGID",
            [
                ':LOGOID' => $this->request['organization_logo_id'],
                ':ORGID' => $this->request['organization_id']
            ]
        );

        $data['messages'][] =
            [
                'type' => 'success',
                'message' => 'Default logo updated.  Logo will update on Tesco site within a few hours.'
            ];


        // Update.
        return $data;
    }

    public function updateorg(){
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['updateFormDetails']) || empty($this->request['updateFormDetails'])){
            $data['type'] = 403;
            $data['messages'][] =
                [
                    'type' => 'danger',
                    'message' => 'Cannot provide results, required arguments are missing.'
                ];

            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // $data['data'] = $this->request;
        // return $data;
        // do the update.

        $olddetail = getOrgDetails($this->request['updateFormDetails']['organization_id']);

        if(!isset($this->request['updateFormDetails']['school_education_phase_id']) || empty($this->request['updateFormDetails']['school_education_phase_id'])) {
            $this->request['updateFormDetails']['school_education_phase_id'] = 11; // Other
        }

        dbq("update organizations set
						organization_type_id = :ORGTYPEID,
					    school_local_authority_id = :LEAID,
					    school_education_phase_id = :PHASEID,
						organization_name = :ORGNAME,
						organization_url = :ORGURL,
						organization_member_count = :ORGMEMBERS,
						organization_telephone = :ORGPHONE,
						organization_welcome_pack = :ORGWELCOME,
						organization_oracle_id = :ORACLEID
					where
						organization_id = :ORGID
					",
            [
                ':ORGTYPEID' => $this->request['updateFormDetails']['organization_type_id'],
                ':ORGNAME' => $this->request['updateFormDetails']['organization_name'],
                ':LEAID' => $this->request['updateFormDetails']['school_local_authority_id'],
                ':PHASEID' => $this->request['updateFormDetails']['school_education_phase_id'],
                ':ORGURL' => $this->request['updateFormDetails']['organization_url'],
                ':ORGMEMBERS' => $this->request['updateFormDetails']['organization_member_count'],
                ':ORGPHONE' => $this->request['updateFormDetails']['organization_telephone'],
                ':ORGWELCOME' => $this->request['updateFormDetails']['organization_welcome_pack'],
                ':ORACLEID' => $this->request['updateFormDetails']['organization_oracle_id'],
                ':ORGID' => $this->request['updateFormDetails']['organization_id']

            ]
        );

        // Address.

        // TODO  check that this address belongs to this person.

        dbq("update addresses set
						entry_street_address = :STREET1,
						entry_street_address_2 = :STREET2,
						entry_suburb = :STREET3,
						entry_city = :CITY,
						entry_state = :STATE,
						entry_postcode = :POSTCODE,
						entry_country_id = :COUNTRYID
					where
						organization_id = :ORGID
						AND
						address_id = :ADDID
					",
            [
                ':STREET1' => $this->request['updateFormDetails']['entry_street_address'],
                ':STREET2' => $this->request['updateFormDetails']['entry_street_address_2'],
                ':STREET3' => $this->request['updateFormDetails']['entry_suburb'],
                ':CITY' => $this->request['updateFormDetails']['entry_city'],
                ':STATE' => $this->request['updateFormDetails']['entry_state'],
                ':POSTCODE' => $this->request['updateFormDetails']['entry_postcode'],
                ':COUNTRYID' => $this->request['updateFormDetails']['entry_country_id'],
                ':ORGID' => $this->request['updateFormDetails']['organization_id'],
                ':ADDID' => $this->request['updateFormDetails']['address_id']
            ]
        );

        audit(11,$this->request['updateFormDetails']['organization_id']);
        if($olddetail['organization_welcome_pack'] != $this->request['updateFormDetails']['organization_welcome_pack']){
            audit(32,$this->request['updateFormDetails']['organization_id'],$this->request['updateFormDetails']['organization_welcome_pack']);
        }

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Organization details updated.'
        ];

        return $data;
    }

    public function updateorgbank(){
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['updateFormDetails']) || empty($this->request['updateFormDetails'])){
            $data['type'] = 403;
            $data['messages'][] =
                [
                    'type' => 'danger',
                    'message' => 'Cannot provide results, required arguments are missing.'
                ];

            return $data;
        }
/*
        // validate bank details.
        include(DIR_CLASSES.'bankcheck.php');

        $pa = new BankAccountValidation ("FN97-ZM77-HE35-FA68", $this->request['updateFormDetails']['organization_bank_account'], $this->request['updateFormDetails']['organization_bank_sort_code_1']."-".$this->request['updateFormDetails']['organization_bank_sort_code_2']."-".$this->request['updateFormDetails']['organization_bank_sort_code_3']);
        $pa->MakeRequest();

        if ($pa->HasData()) {
            $data = $pa->HasData();

            if ($data[0]['IsCorrect'][0] == 'False') {
                // details not valid.
                $data['type'] = 401;
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'Account details failed verification. Please check account details carefully.'
                ];
                return $data;
            }
        }
*/

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

//        $data['data'] = $this->request;
        // return $data;
        // do the update.

        $masterOrgID = getMasterOrgID($this->request['updateFormDetails']['organization_id']);

        $allOrgQuery = dbq("select organization_id from organizations where organization_id = :ORGID or organization_parent_organization_id = :ORGID",
            [
                ':ORGID' => $masterOrgID
            ]
        );

        while($allOrg = dbf($allOrgQuery)){

            // delete old details if they exist.
            dbq("delete from organization_bank_accounts where organization_id = :ONEORGID",
                [
                    ':ONEORGID' => $allOrg['organization_id']
                ]
            );

            // Insert new details.
            dbq("INSERT INTO organization_bank_accounts (
                        organization_id,
                        organization_bank_sort_code_1,
                        organization_bank_sort_code_2,
                        organization_bank_sort_code_3,
                        organization_bank_account,
                        organization_bank_name,
                        organization_bank_address_id,
                        organization_bank_last_updated
                      ) VALUES (
                        :ORGANIZATIONID,
                        :ORGANIZATIONBANKSORTCODE1,
                        :ORGANIZATIONBANKSORTCODE2,
                        :ORGANIZATIONBANKSORTCODE3,
                        :ORGANIZATIONBANKACCOUNT,
                        :ORGANIZATIONBANKNAME,
                        :ORGANIZATIONBANKADDRESSID,
                        NOW()
                      )",
                [
                    ':ORGANIZATIONID' => $this->request['updateFormDetails']['organization_id'],
                    ':ORGANIZATIONBANKACCOUNT' => $this->request['updateFormDetails']['organization_bank_account'],
                    ':ORGANIZATIONBANKNAME' => $this->request['updateFormDetails']['organization_bank_name'],
                    ':ORGANIZATIONBANKSORTCODE1' => $this->request['updateFormDetails']['organization_bank_sort_code_1'],
                    ':ORGANIZATIONBANKSORTCODE2' => $this->request['updateFormDetails']['organization_bank_sort_code_2'],
                    ':ORGANIZATIONBANKSORTCODE3' => $this->request['updateFormDetails']['organization_bank_sort_code_3'],
                    ':ORGANIZATIONBANKADDRESSID' => null
                ]

            );

            // update org.
            dbq('update organizations set organization_bank_verified = 1 where organization_id = :ORGID',
                [
                    ':ORGID' =>  $allOrg['organization_id']
                ]
            );

            audit(23,$allOrg['organization_id'],'Bank details verified.');

        }


       /* if(isset($this->request['updateFormDetails']['organization_bank_account_id']) && !empty($this->request['updateFormDetails']['organization_bank_account_id'])){
            // update
            dbq("update organization_bank_accounts set
						organization_bank_account = :ORGBANKACC,
						organization_bank_name = :ORGBANKNAME,
						organization_bank_sort_code_1 = :ORGBANKSORT1,
						organization_bank_sort_code_2 = :ORGBANKSORT2,
						organization_bank_sort_code_3 = :ORGBANKSORT3,
						organization_bank_last_updated = NOW()
					where
						organization_bank_account_id = :ORGBANKID
						AND
						organization_id = :ORGID
					",
                [
                    ':ORGBANKACC' => $this->request['updateFormDetails']['organization_bank_account'],
                    ':ORGBANKNAME' => $this->request['updateFormDetails']['organization_bank_name'],
                    ':ORGBANKSORT1' => $this->request['updateFormDetails']['organization_bank_sort_code_1'],
                    ':ORGBANKSORT2' => $this->request['updateFormDetails']['organization_bank_sort_code_2'],
                    ':ORGBANKSORT3' => $this->request['updateFormDetails']['organization_bank_sort_code_3'],
                    ':ORGBANKID' => $this->request['updateFormDetails']['organization_bank_account_id'],
                    ':ORGID' => $this->request['updateFormDetails']['organization_id'],
                ]
            );
        } else {
            // insert
            dbq("insert into organization_bank_accounts (
                        organization_id,
                        organization_bank_sort_code_1,
                        organization_bank_sort_code_2,
                        organization_bank_sort_code_3,
                        organization_bank_account,
                        organization_bank_name,
                        organization_bank_last_updated
                    ) VALUES (
                        :ORGID,
                        :ORGBANKSORT1,
                        :ORGBANKSORT2,
                        :ORGBANKSORT3,
                        :ORGBANKACC,
                        :ORGBANKNAME,
                        NOW()
                    )
            ",
                [
                    ':ORGBANKACC' => $this->request['updateFormDetails']['organization_bank_account'],
                    ':ORGBANKNAME' => $this->request['updateFormDetails']['organization_bank_name'],
                    ':ORGBANKSORT1' => $this->request['updateFormDetails']['organization_bank_sort_code_1'],
                    ':ORGBANKSORT2' => $this->request['updateFormDetails']['organization_bank_sort_code_2'],
                    ':ORGBANKSORT3' => $this->request['updateFormDetails']['organization_bank_sort_code_3'],
                    ':ORGID' => $this->request['updateFormDetails']['organization_id'],
                ]);
        }

        // update org.
        dbq('update organizations set organization_bank_verified = 1 where organization_id = :ORGID',
            [
                ':ORGID' => $this->request['updateFormDetails']['organization_id']
            ]
        );

        // Address.

        audit(23,$this->request['updateFormDetails']['organization_id'],'Bank details verified.');

        */


        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Bank details updated.'
        ];

        return $data;
    }

    public function removeproduct(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['deleteProdData']) || empty($this->request['deleteProdData'])){
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $audittype = 16; // plain product remove

      //  $data['data'] = $this->request;
       // return $data;

        // check that org ID is in session.
        $checkExistsQuery = dbq("select
                                    p.product_decorated,
                                    p.product_name,
                                    c.colour_name,
                                    ol.tesco_style_ref,
                                    ol.organization_logo_id,
                                    pc.product_colour_id
                                    from products_colours_to_organizations pco
 									JOIN product_colours pc using (product_colour_id)
 									JOIN products p using (product_id)
 									JOIN colours c using (colour_id)
 									left join organization_logos ol using (organization_logo_id)
 									where pco.products_colours_to_organizations_id = :PCOID and pco.organization_id = :OID",
            [
                ':PCOID' => $this->request['deleteProdData']['productColourOrgID'],
                ':OID' => $this->request['deleteProdData']['organizationID']
            ]
        );

        if(dbnr($checkExistsQuery) > 0){
            $checkExists = dbf($checkExistsQuery);
            $logoID = null;
            $productColourID = $checkExists['product_colour_id'];

            if($checkExists['product_decorated']){
                $productString = $checkExists['product_name'].' ('.$checkExists['colour_name'].') - '.$checkExists['tesco_style_ref'];
                $audittype = 14;  // embroidered remove
                $logoID = $checkExists['organization_logo_id'];
                // make sure its not the last one if embroidered.
                $checkEmbQuery = dbq("select * from products_colours_to_organizations pco
 									JOIN product_colours pc using (product_colour_id)
 									JOIN products p using (product_id)
 									where p.product_decorated = 1 and pco.organization_id = :OID",
                    [
                        ':OID' => $this->request['deleteProdData']['organizationID']
                    ]
                );

                if(dbnr($checkEmbQuery) <= 1){
                    // cant delete last item.
                    $data['type'] = 403;
                    $data['messages'][] = [
                        'type' => 'danger',
                        'message' => 'Sorry, you cannot remove the last item. $checkEmbQuery'
                    ];

                    return $data;
                }

            } else {
                $productString = $checkExists['product_name'].' ('.$checkExists['colour_name'].')';
            }

            // delete it.
            dbq("delete from products_colours_to_organizations where products_colours_to_organizations_id = :PCOID and organization_id = :OID",
                [
                    ':PCOID' => $this->request['deleteProdData']['productColourOrgID'],
                    ':OID' => $this->request['deleteProdData']['organizationID']
                ]
            );

            // update the product usage
            dbq("update product_colours set product_colour_school_count = product_colour_school_count - 1 where product_colour_id = :PCID",
                [
                    ':PCID' => $productColourID
                ]
            );

            if(!empty($logoID)){
                // update logo usage
                dbq("update organization_logos set products_using_logo = products_using_logo - 1 where organization_logo_id = :ORGANIZATIONLOGOID",
                    [
                        ':ORGANIZATIONLOGOID' => $logoID
                    ]
                );
            }

            audit($audittype,$this->request['deleteProdData']['organizationID'],$productString);

            $data['messages'][] = [
                'type' => 'success',
                'message' => 'Item removed.'
            ];

        } else {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'The product cannot be removed. $checkExistsQuery'
            ];
        }

        return $data;
    }

    public function getembstyles() {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $productQuery = dbq("SELECT * FROM products p
								WHERE p.product_active = 1
								AND p.product_decorated = 1
								and p.product_test_only = 0"
        ); // dont want to see the test products

        while($product = dbf($productQuery)){
            $data['data'][] = $product;
        }

        return $data;
    }

    public function getplnstyles() {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $productQuery = dbq("SELECT * FROM products p
								WHERE p.product_active = 1
								AND p.product_decorated = 0
								and p.product_test_only = 0"
        ); // dont want to see the test products

        while($product = dbf($productQuery)){
            $data['data'][] = $product;
        }

        return $data;
    }

    public function getproductcolours() {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['pid']) || empty($this->request['pid'])){
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $productColoursQuery = dbq("SELECT * FROM product_colours pc
                                        JOIN colours c using (colour_id)
								        WHERE pc.product_id = :PID and pc.product_colour_active = 1",
            [
                ':PID' => $this->request['pid']
            ]
        ); // dont want to see the test products

        while($productColour = dbf($productColoursQuery)){
            $data['data'][$productColour['product_colour_id']] = $productColour;
        }

        return $data;
    }



        public function getproductsizes()
        {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        $data['type'] = 200; 
        
        if(!isset($this->request['product_colour_id']) || empty($this->request['product_colour_id']))
        {
            $data['type'] = 403;
            $data['massage'][] =
                [
                    'type' => 'danger',
                    'message' => 'Cannot provide results, required arguments are missing.'
                ];
            return $data;
        }

       $selectQuery = dbq("SELECT psc.product_variation_gmo_sku, psc.product_variation_stock, ps.`products_size_name` , psc.product_variation_sku, ps.`product_size_id`, psc.product_variation_price, psc.products_sizes_to_colours_id
                            FROM products_sizes_to_products_colours psc 
                            JOIN `product_sizes` ps ON ps.`product_size_id` = psc.`product_size_id`
                            WHERE psc.`product_colour_id` = :PRODUCT_COLOUR_ID
                            ORDER BY ps.`product_size_sort_order` ASC",
            [
                ':PRODUCT_COLOUR_ID' => $this->request['product_colour_id']
            ]);

        while($fetchQuery = dbf($selectQuery))
        {
            $data[] = [
                'product_size_name' => $fetchQuery['products_size_name'],
                'product_size_id'   => $fetchQuery['product_size_id'],
                'product_sku'       => $fetchQuery['product_variation_sku'],
                'product_gmo_sku'   => $fetchQuery['product_variation_gmo_sku'],
                'product_qty'       => $fetchQuery['product_variation_stock'],
                'product_price'     => $fetchQuery['product_variation_price'],
                'products_sizes_to_colours_id' => $fetchQuery['products_sizes_to_colours_id']
            ];
        }
        return $data;
    }

    public function addproduct(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['addProdData']) || empty($this->request['addProdData'])){
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];
        $logoID = null;
        $audittype = 5;  // plain product add
        switch($this->request['addProdData']['embroidered']){
            case '0' :
                // plain
                // check its not already there.
                $checkExistsQuery = dbq("select * from products_colours_to_organizations
									where product_colour_id = :PCOLOURID
									and organization_id = :OID",
                    [
                        ':PCOLOURID' => $this->request['addProdData']['colour'],
                        ':OID' => $this->request['addProdData']['orgid']
                    ]
                );
                break;
            case '1' :
                // embroidered
                $audittype = 3;  // embroidered product add
                $logoID = $this->request['addProdData']['logo'];
                // check its not already there with this logo.
                $checkExistsQuery = dbq("select * from products_colours_to_organizations
									where product_colour_id = :PCOLOURID
									and organization_id = :OID
									and organization_logo_id = :LOGOID",
                    [
                        ':PCOLOURID' => $this->request['addProdData']['colour'],
                        ':OID' => $this->request['addProdData']['orgid'],
                        ':LOGOID' => $logoID
                    ]
                );
                break;
        }

        if(dbnr($checkExistsQuery) > 0){
            // already exists.
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'The product configuration already exists.'
            ];
            return $data;
        }

        // add it in!
        dbq("insert into products_colours_to_organizations (
							product_colour_id,
							organization_id,
							organization_logo_id
						  ) VALUES (
						  	:PRODUCTCOLOURID,
							:ORGANIZATIONID,
							:ORGANIZATIONLOGOID
						  ) ",
            [
                ':PRODUCTCOLOURID' => $this->request['addProdData']['colour'],
                ':ORGANIZATIONID' => $this->request['addProdData']['orgid'],
                ':ORGANIZATIONLOGOID' => $logoID
            ]
        );

        // update the product usage
        dbq("update product_colours set product_colour_school_count = product_colour_school_count + 1 where product_colour_id = :PCID",
            [
                ':PCID' => $this->request['addProdData']['colour']
            ]
        );

        if(!empty($logoID)){
            // update logo usage
            dbq("update organization_logos set products_using_logo = products_using_logo + 1 where organization_logo_id = :ORGANIZATIONLOGOID",
                [
                    ':ORGANIZATIONLOGOID' => $logoID
                ]
            );
        }

        $checkExistsQuery = dbq("select
                                    p.product_decorated,
                                    p.product_name,
                                    c.colour_name,
                                    ol.tesco_style_ref
                                    from products_colours_to_organizations pco
 									JOIN product_colours pc using (product_colour_id)
 									JOIN products p using (product_id)
 									JOIN colours c using (colour_id)
 									left join organization_logos ol using (organization_logo_id)
 									where pco.product_colour_id = :PCID and pco.organization_id = :OID
 									 and ol.logo_deleted != 1",
            [
                ':PCID' => $this->request['addProdData']['colour'],
                ':OID' => $this->request['addProdData']['orgid']
            ]
        );

        $checkExists = dbf($checkExistsQuery);

        if($checkExists['product_decorated']) {

            $productString = $checkExists['product_name'].' ('.$checkExists['colour_name'].') - '.$checkExists['tesco_style_ref'];
            // update to live
            $masterOrgID = getMasterOrgID($this->request['addProdData']['orgid']);
            // use master to set whole school live?
            $orgDetail = getOrgDetails($this->request['addProdData']['orgid']);
            $masterOrgDetail = getOrgDetails($masterOrgID);

            if($orgDetail['organization_status_id'] >= 4 && $orgDetail['organization_status_id'] <= 6){
                // this org/house is stage 1 - 3.  Added embroidered product. Update to live now.  Make sure master is live if house.
                dbq("update organizations set organization_status_id = 3, organization_live_date = NOW() where organization_id = :THISORG",
                    [
                        ':THISORG' => $this->request['addProdData']['orgid']
                    ]
                );

                if(empty($masterOrgDetail['organization_live_date'])){
                    // master not live.  set live.
                    dbq("update organizations set organization_status_id = 3, organization_live_date = NOW() where organization_id = :MASTERORG",
                        [
                            ':MASTERORG' => $masterOrgID,
                        ]
                    );
                }

                dbq("update school_data set school_status_id = 3 where school_data_id = :SDID",
                    [
                        ':SDID' => $orgDetail['school_data_id']
                    ]
                );

                // set this logo as the default logo for the org.
                dbq("update organizations set organization_default_logo_id = :LOGOID, organization_default_logo_update = 1 where organization_id = :ORGID",
                    [
                        ':LOGOID' => $logoID,
                        ':ORGID' => $orgDetail['organization_id']
                    ]
                );
            }

        } else {
            $productString = $checkExists['product_name'].' ('.$checkExists['colour_name'].')';
        }

        audit($audittype, $this->request['addProdData']['orgid'],$productString);

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Item added.'
        ];

        return $data;
    }

    public function suspendorg(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['requestData']) || empty($this->request['requestData'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // check org exists.
        $userCheckQuery = dbq("select * from organizations WHERE organization_id = :ORGID",
            [
                ':ORGID' => $this->request['requestData']['orgId']
            ]
        );


        if(dbnr($userCheckQuery) > 0){
            // Org exists.
            // record the detail.
            dbq("UPDATE organizations o
                    join school_data sd on sd.school_data_id = o.school_data_id
                    set
                    o.organization_suspended_date = NOW(),
                    o.organization_status_id = 8,
                    o.organization_withdrawn_suspended_reason = :REASON,
                    sd.school_status_id = 8
                    where o.organization_id = :ORGID",
                [
                    ':REASON' => $this->request['requestData']['description'],
                    ':ORGID' => $this->request['requestData']['orgId']
                ]
            );

            audit(24,$this->request['requestData']['orgId'],$this->request['requestData']['description']); // Maybe add a reason?

            $data['messages'][] = [
                'type' => 'success',
                'message' => 'Organization suspended.'
            ];

        } else {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Organization not found.'
            ];
        }

        return $data;
    }

    public function withdraworg(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['requestData']) || empty($this->request['requestData'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // check org exists.
        $userCheckQuery = dbq("select * from organizations WHERE organization_id = :ORGID",
            [
                ':ORGID' => $this->request['requestData']['orgId']
            ]
        );


        if(dbnr($userCheckQuery) > 0){
            // Org exists.
            $orgdetail = dbf($userCheckQuery);
            // record the detail.
            dbq("UPDATE organizations o
                    join school_data sd on sd.school_data_id = o.school_data_id
                    set
                    o.organization_withdrawn_date = NOW(),
                    o.organization_withdrawn_suspended_reason = :REASON,
                    o.organization_withdraw_reason_id = :REASON_ID,
                    o.organization_previous_status_id = :OLDSTATUS,
                    o.organization_status_id = 7,
                    sd.school_status_id = 7
                    where o.organization_id = :ORGID",
                [   
                    ':REASON_ID' => $this->request['requestData']['reasonID'],
                    ':REASON' => $this->request['requestData']['description'],
                    ':OLDSTATUS' => $orgdetail['organization_status_id'],
                    ':ORGID' => $this->request['requestData']['orgId']
                ]
            );

            audit(25,$this->request['requestData']['orgId'],$this->request['requestData']['description']); // Maybe add a reason?

            $data['messages'][] = [
                'type' => 'success',
                'message' => 'Organization withdrawn.'
            ];

        } else {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Organization not found.'
            ];
        }

        return $data;
    }

    public function addnewhouse()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['requestData']) || empty($this->request['requestData'])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // OK as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // need to copy the org and add a new one with new details
        $parentOrg = getOrgDetails($this->request['requestData']['orgId'],True);


       // print_r("orgidparet:" . $this->request['requestData']['orgId']);



        $houseOrg = $parentOrg;


        if(empty($houseOrg)){
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Organization not found.'
            ];
            return $data;
        }

        //fetch masterOrgId, get it's name and compare with user given house name
        $masterOrgId = getMasterOrgID($this->request['requestData']['orgId']);
        $masterDetails = getOrgDetails($masterOrgId);
        $school_data_id  = $masterDetails['school_data_id'];

        $parentOrgId  =$this->request['requestData']['orgId'];

        //throw an error if the masterOrg name is the same as user given house name
        if(stristr($this->request['requestData']['houseName'] , $masterDetails['organization_name']))
        {
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Please do not use the school name as part of house name.'
            ];
            return $data;
        }

        //check if there is already house with this name at this top level
        $houseNameCheckQuery = dbq("SELECT * FROM organizations
                                    WHERE organization_name = :HOUSENAME
                                    and organization_parent_organization_id = :ORGID",
            [
                ':HOUSENAME' => $this->request['requestData']['houseName'],
                ':ORGID' => $masterOrgId
            ]
        );

        //if there is - throw an error
        if(dbnr($houseNameCheckQuery) > 0){
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'There is already a house with this name.'
            ];
            return $data;
        }

        //firefly get master urn from master data

        $master_school_URN = $masterDetails['school_URN'];

        $houseURNQuery = dbq("SELECT 
                            (SELECT  school_URN FROM organizations WHERE  school_data_id=:SDID ORDER BY organization_id LIMIT 1) as 'root',
                            (SELECT  school_URN FROM organizations WHERE  school_data_id=:SDID ORDER BY organization_id DESC LIMIT 1) as 'lastadded'",
            [
                ':SDID' => $school_data_id
            ]
        );

        $URNS = dbf($houseURNQuery);

        if($URNS['root'] == $URNS['lastadded']){
            //no sub houses so lets start the kick off for new generation of sub-house
            //print_r("NO SUB HOUSES THIS IS A ROOT ORG SO LETS CREATE A SUBHOUSE BASED ON APPENDING 901 TO FRONT OF URN:" . $URNS['root']);

            $urnprefix = 901;
            $newURN = $urnprefix.$URNS['root'];


        } else {
            //no sub houses so lets start the kick off for new generation of sub-house
            //print_r("SUB HOUSES EXIST:" . $URNS['lastadded']);

            $newURN = $urnprefix.$URNS['lastadded'] + 1000000000;

        }

        $rootURN = $URNS['root'];
        $parentURN =$parentOrg['school_URN'];

        if($rootURN == $parentURN) {
         //   print_r("<pre>This is a level 1 house so copy " . $newURN  . " into both urn and parenturn fields</pre>");

            $houseOrg['school_URN'] =  $newURN;
            $houseOrg['school_URN_parent'] =  $newURN;
        //    echo '<br/>';
        } else {
         //   print_r("<pre>This is a sub level house so copy " . $newURN  . "  into urn and " . $parentURN . " into parenturn</pre>");

            $houseOrg['school_URN'] =  $newURN;
            $houseOrg['school_URN_parent'] =  $parentURN;
        //    echo '<br/>';

        }

        // put organization ID into parent.
        unset($houseOrg['organization_id']);

        $houseOrg['organization_parent_organization_id'] = $parentOrg['organization_id'];

        //firefly
        $houseOrg['organization_parent_id'] =$rootURN;
        $houseOrg['organization_name'] = $this->request['requestData']['houseName'] . " (" . $this->request['requestData']['houseType'] . ")";
        $houseOrg['organization_has_subunits'] = 0;


        if($houseOrg['organization_status_id'] == 3 && $housecount > 0){
            // live parent, put this house in stage 3, otherwise leave it as is.
            $houseOrg['organization_status_id'] = 6;
        }

        $fieldValues = [];
        foreach($houseOrg as $key => $value){
            $newKey = ':'.strtoupper($key);
            $fieldValues[$newKey] = $value;
        }


        // Insert this.
        dbq("insert into organizations (`".implode('`,`',array_keys($houseOrg))."`) values (".implode(",",array_keys($fieldValues)).")",
            $fieldValues
        );




        $houseID = dbid();

        //firefly
        dbq("insert into organization_units (organization_id, organization_unit_name) values (" .$parentOrgId. ",'" .$this->request['requestData']['houseType'] ."')");



        //$data['sql'] = "insert into organizations (`".implode('`,`',array_keys($houseOrg))."`) values ('".implode("','",$houseOrg)."')";

        // needs address
        $addressQuery = dbq("select * from addresses where organization_id = :ORGID",
            [
                ':ORGID' => $parentOrg['organization_id']
            ]
        );

        while($address = dbf($addressQuery)){
            foreach($address as $key => $value){
                $address[$key] = addslashes($value);
            }

            $setdefault = false;

            if($address['address_id'] == $parentOrg['organization_default_address_id']){
                $setdefault = true;
            }

            unset($address['address_id'],$address['user_id']);
            $address['organization_id'] = $houseID;

            dbq("insert into addresses (`".implode('`,`',array_keys($address))."`) values ('".implode("','",$address)."')");

            if($setdefault){
                $defaultAddress = dbid();
                dbq("update organizations set organization_default_address_id = :ADDRESSID where organization_id = :HOUSEID",
                    [
                        ':ADDRESSID' => $defaultAddress,
                        ':HOUSEID' => $houseID
                    ]
                );
            }

        }

        // needs bank
        $banksQuery = dbq("select * from organization_bank_accounts where organization_id = :ORGID",
            [
                ':ORGID' => $parentOrg['organization_id']
            ]
        );

        while($banks = dbf($banksQuery)){

            foreach($banks as $key => $value){
                $banks[$key] = addslashes($value);
            }

            unset($banks['organization_bank_account_id']);
            $banks['organization_id'] = $houseID;

            // copy the bank details for the organization into the house.
            dbq("insert into organization_bank_accounts (`".implode('`,`',array_keys($banks))."`) values ('".implode("','",$banks)."')");

        }


        //firefly


        dbq("update organizations set organization_has_subunits = 1 where school_URN = :PARENTURN",
            [
                ':PARENTURN' =>  $parentURN
            ],true
        );

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'House Added.'
        ];

        if($housecount == 0){
            // first house, move any products that were for the main school to the first house.
            dbq("update products_colours_to_organizations set organization_id = :HOUSEID where organization_id = :PARENTORGID",
                [
                    ':HOUSEID' => $houseID,
                    ':PARENTORGID' => $parentOrg['organization_id']
                ]
            );

            // add info pane.
            $data['messages'][] = [
                'type' => 'info',
                'message' => 'Products associated with the main organization are now on the first house.'
            ];
        }

        audit(30,$parentOrg['organization_id'],$houseOrg['organization_name'].' ('.$houseOrg['school_URN'].')');

        return $data;
    }

    public function reinstateorg(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['orgData']) || empty($this->request['orgData'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // check org exists.
        $userCheckQuery = dbq("select * from organizations WHERE organization_id = :ORGID",
            [
                ':ORGID' => $this->request['orgData']['organization_id']
            ]
        );


        if(dbnr($userCheckQuery) > 0){
            // Org exists.
            $orgdetail = dbf($userCheckQuery);

            if(!is_null($orgdetail['organization_previous_status_id'])){
                $newstatus = $orgdetail['organization_previous_status_id'];
            } else {
                // Work it out?
                if(!is_null($orgdetail['organization_live_date'])) {
                    // Must have been live before!
                    $newstatus = 3;  // Live
                } else {
                    // Must be a stage.
                    // If they have approved logo stage 3.
                    $checkLogo = dbq("select * from organization_logos where organization_id = :ORGID and logo_approved = 1",
                        [
                            ':ORGID' => $this->request['orgData']['organization_id']
                        ]
                    );

                    if(dbnr($checkLogo) > 0){
                        $newstatus = 6; // stage 3
                    } else {
                        $checkLogoTMP = dbq("select * from organization_logos where organization_id = :ORGID",
                            [
                                ':ORGID' => $this->request['orgData']['organization_id']
                            ]
                        );

                        if(dbnr($checkLogoTMP) > 0){
                            $newstatus = 5; // stage 2
                        } else {
                            $newstatus = 4; // stage 1
                        }
                    }

                }


            }
            // record the detail.
            dbq("UPDATE organizations o
                    join school_data sd on sd.school_data_id = o.school_data_id
                    set
                    o.organization_withdrawn_date = NULL,
                    o.organization_withdrawn_suspended_reason = NULL,
                    o.organization_suspended_date = NULL,
                    o.organization_status_id = :NEWSTATUS,
                    sd.school_status_id = :NEWSTATUS
                    where o.organization_id = :ORGID",
                [
                    ':NEWSTATUS' => $newstatus,
                    ':ORGID' => $this->request['orgData']['organization_id']
                ]
            );

            audit(26,$this->request['orgData']['organization_id']); // Maybe add a reason?

            $data['messages'][] = [
                'type' => 'success',
                'message' => 'Organization re-instated.'
            ];

        } else {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Organization not found.'
            ];
        }

        return $data;
    }

    public function addlogofiles(){
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['requestData']) || empty($this->request['requestData'])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        // check that all files are there.
        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // check all 3 files are there
        if(!isset($this->request['requestData']['attachments']) || sizeof($this->request['requestData']['attachments']) != 3){
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required files are missing.'
            ];
            return $data;
        }

        // Check the emblem exists.

        $emblemCheckQuery = dbq("select * from organization_logos where organization_logo_id = :OLID",
            [
                ':OLID' => $this->request['requestData']['emblem']['organization_logo_id']
            ]
        );

        if(dbnr($emblemCheckQuery) < 1){
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Emblem not found.'
            ];
            return $data;
        }

        $emblemCheck = dbf($emblemCheckQuery);

        $orgDetail = getOrgDetails($this->request['requestData']['emblem']['organization_id']);
        $masterOrgDetail =  getOrgDetails(getMasterOrgID($this->request['requestData']['emblem']['organization_id']));
        //save to file structure.  Check school folder.
        if(!file_exists('unapproved_logo_files/'.$orgDetail['school_URN'])){
            mkdir('unapproved_logo_files/'.$orgDetail['school_URN']);
            chmod('unapproved_logo_files/'.$orgDetail['school_URN'],0777); // allow us to write to it from ftp.
        }

        // save the files.
        foreach($this->request['requestData']['attachments'] as $attachment){
            $fileXTN = @end(explode('.',$attachment['name']));
            $fileXTN = strtolower($fileXTN);
            $filedata = @end(explode('base64,',$attachment['url'],2));
            $filedata = base64_decode($filedata);
            file_put_contents('unapproved_logo_files/'.$orgDetail['school_URN'].'/'.$emblemCheck['tesco_style_ref'].'.'.$fileXTN,$filedata);
        }

        // upload logo to ebos.  Nedd colours.
        include_once(DIR_CLASSES . 'eBOS_API.php');
        $eBOS_API = new eBOS_API(APIUser, APIPass);

        $embColours = $eBOS_API->getThreadsFromEMB('unapproved_logo_files/'.$orgDetail['school_URN'].'/'.$emblemCheck['tesco_style_ref'].'.emb');
       //$dstStats = $eBOS_API->parseDSTHeader('unapproved_logo_files/'.$orgDetail['school_URN'].'/'.$emblemCheck['tesco_style_ref'].'.dst');

        if(!isset($this->request['requestData']['emblem']['ebos_id']) || empty($this->request['requestData']['emblem']['ebos_id'])){
            // need to find it or make it.
            $designs = $eBOS_API->getDesigns($emblemCheck['tesco_style_ref']);
            $ebos_id = null;

            if (!empty($designs)) {
                // Add logos to database.
                $designArray = [];
                //echo "<pre>".print_r($designs,1)."</pre>"; die;
                foreach ($designs->designs as $design) {
                    if($design->number == $emblemCheck['tesco_style_ref']){
                        $ebos_id = $design->id ;
                    }
                }
            }

            if(is_null($ebos_id)){
                // doesnt exist.  create one for it.

                $designdetail = [
                    'design' => [
                        'number' => $emblemCheck['tesco_style_ref'].'-TMP',
                        'name' => $emblemCheck['school_name'],  // random name for testing
                        'notes' => 'Created by F&F UES',
                        'threads' => [
                            '1000'
                        ], // Must be strings not integers.
                        'dst' => [
                            'data' => base64_encode(file_get_contents('dst/tempdesign.dst')),
                            'filename' => 'tempdesign.dst'
                        ]
                    ]
                ];

                $newDesign = $eBOS_API->createDesign($designdetail);  // Works.

                if (isset($newDesign->successful) && $newDesign->successful == false) {
                    $data['type'] = 401;

                    $data['messages'][] = [
                        'type' => 'danger',
                        'message' => $newDesign->message
                    ];

                    return $data;
                }

                $ebos_id = $newDesign->design->id ;
                dbq('update organization_logos set ebos_id = :EBOSID, logo_on_ebos = 1 where organization_logo_id = :OLID',
                    [
                        ':EBOSID' => $ebos_id,
                        ':OLID' => $emblemCheck['organization_logo_id']
                    ]
                );

            }
        } else {
            $ebos_id = $this->request['requestData']['emblem']['ebos_id'];
        }

        $newDesign = $eBOS_API->getDesignDetails($ebos_id);

        /* $result = [
            'fullwidth' => $headerArray['+X'] + $headerArray['-X'],
            'mmwidth' => ($headerArray['+X'] + $headerArray['-X']) / 10,
            'fullheight' => $headerArray['+Y'] + $headerArray['-Y'],
            'mmheight' => ($headerArray['+Y'] + $headerArray['-Y']) / 10,
            'totalColours' => $headerArray['CO']+1,
            'stitchCount' => $headerArray['ST']
        ];
        */

        $designChanges = [
            'design' => [
                'threads' => $embColours,
                'number' => $newDesign->design->number,
                'name' => $newDesign->design->name,
                'notes' => $newDesign->design->notes."\n\n".'Uploaded digitized design from F&F UES. - '.date('r').' - '.$_SESSION['session']['user']['user_first_name'].' '.$_SESSION['session']['user']['user_last_name'],
                'dst' => [
                    'data' => base64_encode(file_get_contents('unapproved_logo_files/'.$orgDetail['school_URN'].'/'.$emblemCheck['tesco_style_ref'].'.dst')),
                    'filename' => $emblemCheck['tesco_style_ref'].'.dst'
                ]
            ]
        ];

        // $data['data']['designChanges'] = $designChanges;

        $designUpdate = $eBOS_API->updateDesign($ebos_id,$designChanges);

        $sampleSent = 0;
        if(isset($this->request['requestData']['emblem']) && !empty($this->request['requestData']['emblem']) && $this->request['requestData']['emblem'] == true){
            $sampleSent = 1;
        }
/*
        $designChanges = [
            'design' => [
                'threads' => $embColours,
                'number' => $newDesign->design->number,
                'name' => $newDesign->design->name
            ]
        ];

        $designThreadUpdate = $eBOS_API->updateDesign($ebos_id,$designChanges);*/

        // TODO check true check update customer.
        dbq('update organization_logos set needs_ebos_update = 1, logo_digitized = 1, update_customer = 0, logo_sampled = :LOGOSAMPLED, primary_background_colour = :PRIMBACK where organization_logo_id = :OLID',
            [
                ':OLID' => $this->request['requestData']['emblem']['organization_logo_id'],
                ':PRIMBACK' => $this->request['requestData']['emblem']['primary_background_colour'],
                ':LOGOSAMPLED' => $sampleSent
            ]
        );

        // Update stage if applicable.
       /* if($masterOrgDetail['organization_status_id'] == 5){
            // registered. Update to stage 3 now.
            dbq("update organizations set organization_status_id = 6 where organization_id = :MASTERORG or organization_parent_organization_id = :MASTERORG",
                [
                    ':MASTERORG' => $masterOrgDetail['organization_id']
                ]
            );

            dbq("update school_data set school_status_id = 6 where school_data_id = :SDID",
                [
                    ':SDID' => $masterOrgDetail['school_data_id']
                ]
            );

        }*/

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Ebos design uploaded. Design on site will update within 30 mins.'
        ];

        return $data;
    }

    public function addformtoorg(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['formData']) || empty($this->request['formData'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];
        /*
formData

date
	"2016-03-02T16:03:28.179Z"

orgid
	"9302"

userid
	"4"*/

        // check org exists.
        $userCheckQuery = dbq("select * from organizations WHERE organization_id = :ORGID and organization_form_signed = 0",
            [
                ':ORGID' => $this->request['formData']['orgid']
            ]
        );

        if(dbnr($userCheckQuery) > 0){
            // Org exists.
            // record the detail.
            $date = substr($this->request['formData']['date'],0,10);
            $data['data'] = $date;
            dbq("UPDATE organizations set
                    organization_form_signed = 1,
                    organization_form_date = :LDATE
                    where organization_id = :ORGID",
                [
                    ':LDATE' => $date,
                    ':ORGID' => $this->request['formData']['orgid']
                ]
            );

            audit(22,$this->request['formData']['orgid']);

            $data['messages'][] = [
                'type' => 'success',
                'message' => 'Organization form updated.'
            ];

        } else {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Organization not found or form already received.'
            ];
        }

        return $data;
    }

    public function createuser(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['userdata']) || empty($this->request['userdata'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $username = $this->request['userdata']['forename'].' '.$this->request['userdata']['surname'];

        $newpass = generateRandomString();
        $password = password_hash($newpass, PASSWORD_BCRYPT);

        // create a user.
        dbq(
            "INSERT into users (
                user_type_id,
                user_name,
                user_first_name,
                user_last_name,
                user_email,
                user_password
              ) VALUES (
                :USERTYPEID,
                :USERNAME,
                :USERFIRSTNAME,
                :USERLASTNAME,
                :USEREMAIL,
                :USERPASSWORD
              )", [
            ':USERTYPEID' => $this->request['userdata']['type'],
            ':USERNAME' => $username,
            ':USERFIRSTNAME' => $this->request['userdata']['forename'],
            ':USERLASTNAME' => $this->request['userdata']['surname'],
            ':USEREMAIL' => $this->request['userdata']['email'],
            ':USERPASSWORD' => $password
        ]
        );

        // send an email.

        $subject = 'New admin account for F&F Uniforms';
        $title = $subject;
        $template = 'tesco-ff-new-admin-account';
        $tag = ['ff-admin-pass'];

        $emailAddress = [
            $username => $this->request['userdata']['email']
            //'Andrew Lindsay' => 'andy@slickstitch.com'
        ];

        $template_content = array(
            [
                'name' => 'emailtitle',
                'content' => $title
            ],
            [
                'name' => 'personname',
                'content' => $username
            ],
            [
                'name' => 'personemail',
                'content' => $this->request['userdata']['email']
            ],
            [
                'name' => 'newpassword',
                'content' => $newpass
            ]
        );

        send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);

        return $data;
    }

    public function updateuser(){
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['userData'])){
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];


        // check user exists.
        $userCheckQuery = dbq("select * from users WHERE user_id = :USERID",
            [
                ':USERID' => $this->request['userData']['userID']
            ]
        );

        if(dbnr($userCheckQuery) > 0){

            $updateuser = dbq("update users set user_email = :EMAIL,
                                  user_first_name = :FIRSTNAME,
                                  user_last_name = :LASTNAME
                        WHERE
                        user_id = :USERID
                                  ",
                [
                    ':EMAIL' => $this->request['userData']['email'],
                    ':FIRSTNAME' => $this->request['userData']['firstname'],
                    ':LASTNAME' => $this->request['userData']['lastname'],
                    ':USERID' => $this->request['userData']['userID'],
                ]
            );

            if($updateuser){
                $data['messages'][] = [
                    'type' => 'success',
                    'message' => 'User changed.'
                ];
            } else {
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'There was a problem updating the user.'
                ];
            }

        } else {
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'User not found.'
            ];
        }

        return $data;
    }

    public function getuseralerts(){

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // check org exists.
        $alertQuery = dbq("select t.*, tt.*, o.organization_name, o.school_URN, o.organization_telephone from tickets t
                              JOIN organizations o on (o.organization_id = t.ticket_organization)
                              LEFT JOIN ticket_types tt on tt.ticket_type_id = t.ticket_type_id
                              WHERE t.ticket_alert = 1 and t.ticket_alert_date <= NOW() and (t.ticket_alert_user_id = :USERID OR t.ticket_alert_user_type_id = :UTYPEID)
                              ORDER BY t.ticket_alert_date DESC",
            [
                ':USERID' => $_SESSION['session']['user']['user_id'],
                ':UTYPEID' => $_SESSION['session']['user']['user_type_id']
            ]
        );

        if(dbnr($alertQuery) > 0) {
            while($alert = dbf($alertQuery)){
                $data['data'][] = $alert;
            }
        }

        return $data;
    }

    public function addnote(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['newNote'])){
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // check org exists.
        $userCheckQuery = dbq("select * from organizations WHERE organization_id = :ORGID",
            [
                ':ORGID' => $this->request['newNote']['orgid']
            ]
        );

        if(dbnr($userCheckQuery) > 0){

            // is there an alert?
            // $alertDate = isset($this->request['newNote']['myDate']) ? $this->request['newNote']['myDate'] : null;
            if(isset($this->request['newNote']['myDate'])){
                //$alertDate = substr($this->request['newNote']['myDate'],0,10) . " 08:00:00";
                $alertDate = date("Y-m-d 08:00:00",strtotime($this->request['newNote']['myDate']));
            } else {
                $alertDate = null;
            }

            if(isset($this->request['newNote']['notesubject']) && !empty($this->request['newNote']['notesubject'])){
                $subject = $this->request['newNote']['notesubject'];
            } else {
                $subject =  'No subject';
            }



           // $subject = ((!isset($this->request['newNote']['notesubject']) || empty($this->request['newNote']['notesubject'])) ? 'No subject' : $this->request['newNote']['notesubject']);
            // give a 30 min window before its urgent.
            $alertDeadline = isset($this->request['newNote']['myDate']) ? date("Y-m-d G:i:s",strtotime($alertDate) + ( 60 * 30 )) : null;

            $alertSet = (isset($this->request['newNote']['setAlert']) && $this->request['newNote']['setAlert'] == true) ? 1 : null;

            $groupNoteTypes = [2,6,13];
            $groupID = (in_array($this->request['newNote']['notetype'],$groupNoteTypes)) ? 3 : null; // Add to all Golley users if uniform or amend.

            $addNote = dbq("INSERT INTO tickets (
                                ticket_organization,
                                ticket_type_id,
                                ticket_status_id,
                                ticket_open_date,
                                ticket_subject,
                                ticket_description,
                                ticket_user_id,
                                ticket_alert,
                                ticket_alert_user_id,
                                ticket_alert_user_type_id,
                                ticket_modified_date,
                                ticket_alert_date,
                                ticket_deadline
                              ) VALUES (
                                :TICKETORGANIZATION,
                                :TICKETTYPEID,
                                :TICKETSTATUSID,
                                NOW(),
                                :TICKETSUBJECT,
                                :TICKETDESCRIPTION,
                                :TICKETUSERID,
                                :TICKETALERT,
                                :TICKETALERTUSERID,
                                :TICKETALERTUSERTYPEID,
                                NOW(),
                                :TICKETALERTDATE,
                                :TICKETDEADLINE
                              )",
                [
                    ':TICKETORGANIZATION' => $this->request['newNote']['orgid'],
                    ':TICKETTYPEID' => $this->request['newNote']['notetype'],
                    ':TICKETSTATUSID' => null,
                    ':TICKETSUBJECT' => $subject,
                    ':TICKETDESCRIPTION' => $this->request['newNote']['notedesc'],
                    ':TICKETUSERID' =>$this->request['newNote']['userid'],
                    ':TICKETALERT' => $alertSet,
                    ':TICKETALERTUSERID' => $this->request['newNote']['userid'],
                    ':TICKETALERTUSERTYPEID' => $groupID,
                    ':TICKETALERTDATE' => $alertDate,
                    ':TICKETDEADLINE' => $alertDeadline

                ]
            );

            if($addNote){
                $data['messages'][] = [
                    'type' => 'success',
                    'message' => 'Note Added.'
                ];

                $thisNoteID = dbid();

                //firefly
                if(isset($this->request['newNote']['noteToWho']) && !empty($this->request['newNote']['noteToWho']  && $this->request['newNote']['noteToWho'] !=999999)){

                    $addnoteroute = $this->request['newNote']['noteToWho'];

                    $addroute = dbq("INSERT INTO tickets_to_users (
                                ticket_id,
                                user_id
                              ) VALUES (
                                :TICKETID,
                                :NOTETOWHO
                              )",
                        [
                            ':NOTETOWHO' => $this->request['newNote']['noteToWho'],
                            ':TICKETID' =>  $thisNoteID

                        ]
                    );

                    if($addroute){
                        $data['messages'][] = [
                            'type' => 'success',
                            'message' => 'Note Added And Route Added.'
                        ];



                    } else {
                        $data['messages'][] = [
                            'type' => 'danger',
                            'message' => 'There was a problem adding the note to the users.'
                        ];
                    }









                }







                // dont want to do this one.
                // remove tasks
              /*  dbq("update tickets set ticket_alert = 0 where ticket_organization = :TICKETORGANIZATION and ticket_alert_date <= NOW() AND ticket_id <> :TICKETID",
                    [
                        ':TICKETID' => $thisNoteID,
                        ':TICKETORGANIZATION' => $this->request['newNote']['orgid'],
                    ]
                );*/

            } else {
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'There was a problem adding the note.'
                ];
            }

        } else {
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'There was a problem adding the note.'
            ];
        }

        return $data;

    }

     public function deletenote()
    {
        if(!$this->isMethodCorrect('POST'))
        {
            return $this->getResponse(500);
        }

        if(!isset($this->request['ticket_id']) || empty($this->request['ticket_id']))
        {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $alertDelete = dbq('UPDATE tickets SET ticket_alert = 0 WHERE ticket_id = :TICKET_ID',
            [
                ':TICKET_ID' => $this->request['ticket_id']
            ]);

        $data['type'] = 200;
        $data['messages'][] =
            [
                'type' => 'success',
                'message' => 'Alert message successfully deleted'
            ];
        return $data;
    }


    /**
     * Get an organizations EMBROIDERED products,
     * @return array
     */

    public function removehouse()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['houseid']) || !isset($this->request['orgid'])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $this->getResponse(500);
        }

        // check org exists.
        $houseCheckQuery = dbq("select * from organizations WHERE organization_id = :HOUSEID and organization_parent_organization_id = :ORGID",
            [
                ':HOUSEID' => $this->request['houseid'],
                ':ORGID' => $this->request['orgid']
            ]
        );

        if(dbnr($houseCheckQuery) < 1){
            // doesnt exist.
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'House not found or not part of this organization.'
            ];
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // last house?
        $housesCount = dbq('select * from organizations where organization_parent_organization_id = :ORGID',
            [
                ':ORGID' => $this->request['orgid']
            ]
        );

        if(dbnr($housesCount) == 1){
            // last house.  move product not delete...
            dbq("update products_colours_to_organizations set organization_id = :ORGID where organization_id = :HOUSEID",
                [
                    ':ORGID' => $this->request['orgid'],
                    ':HOUSEID' => $this->request['houseid']
                ]
            );

            // Set main org as not having subunits.
            dbq('update organizations set organization_has_subunits = 0 where organization_id = :ORGID',
                [
                    ':ORGID' => $this->request['orgid']
                ]
            );

        } else {

            // TODO remove logo usage stats.

            // remove product allocations.
            dbq("delete from products_colours_to_organizations where organization_id = :HOUSEID",
                [
                    ':HOUSEID' => $this->request['houseid']
                ]
            );
        }

        // remove bank account.
        dbq("delete from organization_bank_accounts where organization_id = :HOUSEID",
            [
                ':HOUSEID' => $this->request['houseid']
            ]
        );


        // delete audit trail.
        dbq("delete from audit_log where organization_id = :HOUSEID",
            [
                ':HOUSEID' => $this->request['houseid']
            ]
        );

        // delete addresses.
        dbq("delete from addresses where organization_id = :HOUSEID",
            [
                ':HOUSEID' => $this->request['houseid']
            ]
        );

        // delete organization.
        dbq("delete from organizations where organization_id = :HOUSEID",
            [
                ':HOUSEID' => $this->request['houseid']
            ]
        );

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'House removed.'
        ];

        return $data;

    }
        /**
     * Get an organizations EMBROIDERED products,
     * @return array
     */

    public function getorgproducts()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['orgid'])){
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $productQuery = dbq("SELECT p.* FROM products p
								JOIN product_colours pc using (product_id)
								JOIN colours using (colour_id)
								JOIN products_colours_to_organizations pc2o using (product_colour_id)
								WHERE pc2o.organization_id = :ORGID
								AND p.product_active = 1
								AND pc.product_colour_active = 1
								AND p.product_decorated = 1
								and p.product_test_only = 0
								group by p.product_id",[
                ':ORGID' => $this->request['orgid']
            ]
        ); // dont want to see the test products

        $productArray = [];
        $altProductArray = [];
        while ($product = dbf($productQuery)) {

            $product['product_image'] = str_replace("/small/","/medium/",$product['product_image']);

            // get colours here?
            $coloursQuery = dbq("select pc2o.products_colours_to_organizations_id, pc.*, c.*, ol.`organization_logo_id`, ol.`tesco_style_ref` , ol.`logo_url`, ol.`logo_last_updated`, sum(ps2pc.product_variation_stock) as prd_qty
                                    from product_colours pc
									join colours c using (colour_id)
									JOIN products_colours_to_organizations pc2o using (product_colour_id)
									JOIN organization_logos ol using (organization_logo_id)
									LEFT JOIN products_sizes_to_products_colours ps2pc using (product_colour_id)
									where pc2o.organization_id = :ORGID
									and
									product_id = :PRODID
									and ol.logo_deleted != 1
									GROUP BY
									pc.product_colour_id",
                [
                    ':ORGID' => $this->request['orgid'],
                    ':PRODID' => $product['product_id']
                ]
            );

            while ($colours = dbf($coloursQuery)) {
                $altProductArray[$colours['products_colours_to_organizations_id']] = $product;
                //product_colour_image_url
                $colours['product_colour_image_url'] = str_replace("/small/","/medium/",$colours['product_colour_image_url']);
                $altProductArray[$colours['products_colours_to_organizations_id']]['colour'] = $colours;
            }

        }

        $data['products'] = $altProductArray;

        $productQuery = dbq("SELECT p.* FROM products p
								JOIN product_colours pc using (product_id)
								JOIN colours using (colour_id)
								JOIN products_colours_to_organizations pc2o using (product_colour_id)
								WHERE pc2o.organization_id = :ORGID
								AND p.product_active = 1
								AND p.product_decorated = 0
								and p.product_test_only = 0
								group by p.product_id",[
                ':ORGID' => $this->request['orgid']
            ]
        ); // dont want to see the test products

        $altProductArray = [];
        while ($product = dbf($productQuery)) {

            $product['product_image'] = str_replace("/small/","/medium/",$product['product_image']);

            // get colours here?
            $coloursQuery = dbq("select pc2o.products_colours_to_organizations_id, pc.*, c.* from product_colours pc
									join colours c using (colour_id)
									JOIN products_colours_to_organizations pc2o using (product_colour_id)
									where pc2o.organization_id = :ORGID
									and
									product_id = :PRODID",
                [
                    ':ORGID' => $this->request['orgid'],
                    ':PRODID' => $product['product_id']
                ]
            );

            while ($colours = dbf($coloursQuery)) {
                $altProductArray[$colours['products_colours_to_organizations_id']] = $product;
                $colours['product_colour_image_url'] = str_replace("/small/","/medium/",$colours['product_colour_image_url']);
                $altProductArray[$colours['products_colours_to_organizations_id']]['colour'] = $colours;
            }



        }

        $data['plainproducts'] = $altProductArray;

        return $data;

    }


    public function resetpassword(){
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['user'])){
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }
        //return $this->request['user'];
        $userdetail = $this->request['user'];
       // return (array)$userdetail;
        $userEmail = $userdetail['user_email'];
        // update it.
        $userQuery = dbq("SELECT * FROM users
								WHERE user_email = :EMAILADDRESS", [':EMAILADDRESS' => $userEmail]);

        if (dbnr($userQuery) > 0) {
            $user = dbf($userQuery);
           // return (array)$user;
            $newpass = generateRandomString();
            $newPassHash = password_hash($newpass, PASSWORD_BCRYPT);

            $userQuery = dbq("UPDATE users SET user_password = :NEWPASS
								WHERE user_id = :USERID", [':NEWPASS' => $newPassHash, ':USERID' => $user['user_id']]);

            print_r("pass:" .  $newpass);

            // Send an email.
            $subject = 'Password reset for F&F Uniforms';
            $title = $subject." ($userEmail)";
            $template = 'tesco-ff-new-password-1';
            $tag = ['ff-pass-reset'];
            $toName = $user['user_first_name']." ".$user['user_last_name'];
            $emailAddress = [
                $toName => $user['user_email'],
                'UES' => 'ues@uniformembroideryservice.com' // copy in UES
                //'UES' => 'andy@slickstitch.com' // copy in UES
            ];

            $template_content = array(
                [
                    'name' => 'emailtitle',
                    'content' => $title
                ],
                [
                    'name' => 'personname',
                    'content' => $user['user_first_name']." ".$user['user_last_name']
                ],
                [
                    'name' => 'newpassword',
                    'content' => $newpass
                ]
            );

            send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);

            $data = [
                'type' => 200,
                'messages' => [
                    [
                        'type' => 'success',
                        'message' => 'Password changed.  Please check your email.'
                    ]
                ] // list of messages.
            ];

        } else {
            // error not found.
            $data = [
                'type' => 401, // Not authorized as standard
                'messages' => [
                    [
                        'type' => 'danger',
                        'message' => 'Email address '.$userEmail.' not found.'
                    ]
                ] // list of messages.
            ];
        }

        return $data;
    }

    public function approvelogo()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['emblem']) || empty($this->request['emblem'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // customer wants a sample.  Check its not been sampled before?  Not at the moment.
        $emblemCheckQuery = dbq("SELECT * FROM organization_logos WHERE organization_logo_id = :OLID",
            [
                ':OLID' => $this->request['emblem']['organization_logo_id']
            ]
        );

        if (dbnr($emblemCheckQuery) < 1) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Logo not found.'
            ];
            return $data;
        }

        $emblemCheck = dbf($emblemCheckQuery);

        /*if ($emblemCheck['logo_sampled'] == 0) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo has not been sampled.'
            ];
            return $data;
        }*/

        if ($emblemCheck['logo_digitized'] == 0) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo is already still being digitized.'
            ];
            return $data;
        }

        if ($emblemCheck['logo_approved'] == 1) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo is already approved.'
            ];
            return $data;
        }

        $orgInfo = getOrgDetails($this->request['emblem']['organization_id']);

        $fileArray = [
            'dst' => $this->request['emblem']['tesco_style_ref'] . '.dst',
            'pdf' => $this->request['emblem']['tesco_style_ref'] . '.pdf',
            'jpg' => $this->request['emblem']['tesco_style_ref'] . '.jpg'
        ];

        // make a jpg.
        $image = file_get_contents('http:'.$emblemCheck['logo_url']);
        $data['metrics']['startim'] = time();

        $im2 = new Imagick();
        $im2->setResolution(300,300);
        $im2->readImageBlob($image);
        $im2->scaleImage(316, 316, true);

        $im1 = new Imagick();
        $im1->setResolution(300,300);
        $im1->newImage(336, 336, new ImagickPixel('rgb(' . $this->request['emblem']['primary_background_colour'] . ')'));
        $im1->setImageUnits(1);
        $im1->setImageColorspace($im2->getImageColorspace());
        $im1->setImageMatte(true);

        $x = floor((336 - $im2->getImageWidth()) / 2);
        $y = floor((336 - $im2->getImageHeight()) / 2);

        $im1->compositeImage($im2, $im2->getImageCompose(), $x, $y);
        // Save it.
        $im1->setImageFormat("jpeg");

        file_put_contents('unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $this->request['emblem']['tesco_style_ref'] . '.jpg', $im1);
        $data['metrics']['endim'] = time();

// upload files to FTP.
        $conn_id = ftp_connect(FTP_HOST);
        if ($conn_id) {
            $login_result = ftp_login($conn_id, FTP_USER, FTP_PASS);
            if ($login_result) {
                ftp_pasv($conn_id, true);
                // upload.
                foreach ($fileArray as $filetype => $filename) {
                    $newname = str_replace('-TMP', '', $filename);
                    $remotefile = '/Tesco/Approved Schools/' . trim($orgInfo['school_URN']) . '/' . $newname;
                    $localfile = API_ROOT . 'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $filename;

                    if(!@ftp_chdir($conn_id,'/Tesco/Approved Schools/' . trim($orgInfo['school_URN']) . '/')){
                        // doesnt exist.
                        ftp_mkdir($conn_id,'/Tesco/Approved Schools/' . trim($orgInfo['school_URN']) . '/');
                    }

                    if (!ftp_put($conn_id, $remotefile, $localfile, FTP_BINARY)) {
                        // error, add to queue to do another time
                        dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                            [
                                ':LOCALFILE' => $localfile,
                                ':REMOTEFILE' => $remotefile,
                                ':HOSTPREFIX' => ''
                            ]
                        );

                    }
                }
            } else {
                // error login in.
                foreach ($fileArray as $filetype => $filename) {
                    $newname = str_replace('-TMP', '', $filename);
                    $remotefile = '/Tesco/Approved Schools/' . trim($orgInfo['school_URN']) . '/' . $newname;
                    $localfile = API_ROOT . 'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $filename;
                    // error, add to queue to do another time
                    dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                        [
                            ':LOCALFILE' => $localfile,
                            ':REMOTEFILE' => $remotefile,
                            ':HOSTPREFIX' => ''
                        ]
                    );
                }
            }
        } else {
            foreach ($fileArray as $filetype => $filename) {
                $newname = str_replace('-TMP', '', $filename);
                $remotefile = '/Tesco/Approved Schools/' . trim($orgInfo['school_URN']) . '/' . $newname;
                $localfile = API_ROOT . 'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $filename;
                // error, add to queue to do another time
                dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                    [
                        ':LOCALFILE' => $localfile,
                        ':REMOTEFILE' => $remotefile,
                        ':HOSTPREFIX' => ''
                    ]
                );
            }
        }


        $newname = str_replace('-TMP', '', $this->request['emblem']['tesco_style_ref']);
        // ok update it.
        dbq("UPDATE organization_logos SET
                  logo_approved = 1,
                  logo_sampled = 0,
                  tesco_style_ref = :NEWREF,
                  needs_to_update_ebos = 1,
                  logo_approved_date = NOW(),
                  logo_approved_name = :USERNAME
                  WHERE organization_logo_id = :OLID",
            [
                ':NEWREF' => $newname,
                ':USERNAME' => $_SESSION['session']['user']['user_first_name']." ".$_SESSION['session']['user']['user_last_name'],
                ':OLID' => $this->request['emblem']['organization_logo_id']
            ]
        );

        //If this is their first approval, set it as default!
        $approvedLogos = dbq("select * from organization_logos where logo_approved = 1 and organization_id = :OID",
            [
                ':OID' => $orgInfo['organization_id']
            ]
        );

        if(dbnr($approvedLogos) == 1){
            // first logo, update default logo.
            dbq("UPDATE organizations SET
                  organization_default_logo_id = :OLID
                  WHERE organization_id = :OID",
                [
                    ':OLID' => $this->request['emblem']['organization_logo_id'],
                    ':OID' => $orgInfo['organization_id']
                ]
            );

            // upload this one to Tesco for FF Website.

/*            $im1->scaleImage(75, 75, true);

            $smallfilename = @reset(explode('-',$this->request['emblem']['tesco_style_ref']));

            file_put_contents('unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $smallfilename . '_xs.jpg', $im1);

            $conn_id = null;
            $conn_id = ftp_connect(TESCO_FTP_HOST,21,10);
            if ($conn_id) {
                $login_result = ftp_login($conn_id, TESCO_FTP_USER, TESCO_FTP_PASS);

                if ($login_result) {
                    ftp_pasv($conn_id, true);
                    // upload.


                        //new name = URN and _sm.
                        // only if primary?
                        $newname = str_replace('-TMP', '', $smallfilename . '_xs.jpg');
                        $localfile = API_ROOT.'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $smallfilename . '_xs.jpg';
                        if (!ftp_put($conn_id, $newname, $localfile, FTP_BINARY)) {
                            // error, add to queue to do another time
                            dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                                [
                                    ':LOCALFILE' => $localfile,
                                    ':REMOTEFILE' => $newname,
                                    ':HOSTPREFIX' => 'TESCO_'
                                ]
                            );
                            return $data;
                        }


                } else {
                    // error logging in.
                    // error, add to queue to do another time


                        $newname = str_replace('-TMP', '',  $smallfilename . '_xs.jpg');
                        $localfile = API_ROOT . 'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' .  $smallfilename . '_xs.jpg';
                        dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                            [
                                ':LOCALFILE' => $localfile,
                                ':REMOTEFILE' => $newname,
                                ':HOSTPREFIX' => 'TESCO_'
                            ]
                        );

                }
            } else {
                // Error.
                // error, add to queue to do another time

                    $newname = str_replace('-TMP', '',  $smallfilename . '_xs.jpg');
                    $localfile = API_ROOT . 'unapproved_logo_files/' . trim($orgInfo['school_URN']) . '/' . $smallfilename . '_xs.jpg';
                    dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
                        [
                            ':LOCALFILE' => $localfile,
                            ':REMOTEFILE' => $newname,
                            ':HOSTPREFIX' => 'TESCO_'
                        ]
                    );

            }*/

        }






        // Audit
        audit(28, $this->request['emblem']['organization_id'], $this->request['emblem']['tesco_style_ref'] . ' (' . colourFromRGB($this->request['emblem']['primary_background_colour']) . ')');
        // Note?
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'The logo has been approved and can now be used on garments.'
        ];

        return $data;
    }

    public function rejectemblem()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['emblem']) || empty($this->request['emblem'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // customer wants a sample.  Check its not been sampled before?  Not at the moment.
        $emblemCheckQuery = dbq("SELECT * FROM organization_logos WHERE organization_logo_id = :OLID",
            [
                ':OLID' => $this->request['emblem']['organization_logo_id']
            ]
        );

        if (dbnr($emblemCheckQuery) < 1) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Logo not found.'
            ];
            return $data;
        }

        $emblemCheck = dbf($emblemCheckQuery);

        if ($emblemCheck['logo_digitized'] == 0) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo is still being digitized.'
            ];
            return $data;
        }

        if ($emblemCheck['logo_approved'] == 1) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'This logo is already approved.'
            ];
            return $data;
        }

        $orgInfo = getOrgDetails($this->request['emblem']['organization_id']);

        // Reject.
        // Wrong Design

        include_once(DIR_CLASSES . 'eBOS_API.php');
        $eBOS_API = new eBOS_API(APIUser, APIPass);

        $orderDetail = $eBOS_API->getDigitizingOrderDetails($emblemCheck['ebos_digitizing_id']);

        /*$newdetails = [
            'special_instructions' => $this->request['requestData']['description'],
        ];*/

        $updateDigitizingOrderDetails = $eBOS_API->rejectDigitizingOrder($orderDetail->order->id,$this->request['description']);

        //$updateDigitizingOrderDetails = $eBOS_API->updateDigitizingOrder($orderDetail->order->id,$newdetails);

        if(isset($updateDigitizingOrderDetails->successful) && $updateDigitizingOrderDetails->successful == true){

            dbq('update organization_logos set
                logo_reject_reason = :REASON,
                logo_approved = 0,
                logo_sampled = 0,
                logo_digitized = 0,
                complete_order = 0
              WHERE
                organization_logo_id = :OLID
               ',
                [
                    ':REASON' => $this->request['description'],
                    ':OLID' => $this->request['emblem']['organization_logo_id']
                ]
            );

            audit(29,$this->request['emblem']['organization_id']);

            // addTicket(2,$this->request['emblem']['organization_id'],'Logo Rejected : '.$emblemCheck['tesco_style_ref'],$this->request['description'],1,date("Y-m-d G:i:s"),null,3); // set an alert for all golley slater users.
            addTicket(2,$this->request['emblem']['organization_id'],'Logo Rejected : '.$emblemCheck['tesco_style_ref'],$this->request['description']); // set an alert for all golley slater users.

            $data['messages'][] = [
                'type' => 'info',
                'message' => 'We have passed your comments on to our design team.'
            ];
        } else {
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'We could not update the eBOS order. Order Status: '.$orderDetail->order->status
            ];
        }

        return $data;
    }

    public function updatehouse(){
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['houseData'])){
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // check house exists.
        $houseCheckQuery = dbq("SELECT * FROM organizations WHERE organization_id = :ORGID",
            [
                ':ORGID' => $this->request['houseData']['houseID']
            ]
        );

        $masterOrgId = getMasterOrgID($this->request['houseData']['houseID']);

        $masterDetails = getOrgDetails($masterOrgId);


        if(stristr($this->request['houseData']['houseName'] , $masterDetails['organization_name']))
        {
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Please do not use the school name as part of house name.'
            ];
            return $data;
        }

        //check if there is already such house name
        $houseNameCheckQuery = dbq("SELECT * FROM organizations
                                    WHERE organization_name = :HOUSENAME
                                    and organization_parent_organization_id = :ORGID",
            [
                ':HOUSENAME' => $this->request['houseData']['houseName'],
                ':ORGID' => $masterOrgId
            ]
        );

        if(dbnr($houseNameCheckQuery) > 0){
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'There is already a house with this name.'
                ];
            return $data;
        }

 	$data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];


        if(dbnr($houseCheckQuery) > 0){

            $updatehouse= dbq("UPDATE organizations SET organization_name = :HOUSENAME
                               WHERE organization_id = :HOUSEID
                                  ",
                [
                    ':HOUSENAME' => $this->request['houseData']['houseName'],
                    ':HOUSEID' => $this->request['houseData']['houseID'],
                ]
            );

            if($updatehouse){
                $data['messages'][] = [
                    'type' => 'success',
                    'message' => 'House changed.'
                ];
            } else {
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'There was a problem updating the house.'
                ];
            }

        } else {
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'House not found.'
            ];
        }

        return $data;
    }

    public function adduser()
    {

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['formData']) || empty($this->request['formData'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        // Check if they exist first.
        $checkUserQuery = dbq("SELECT user_first_name, user_last_name, user_id, user_type_id FROM users WHERE user_email = :NEWEMAIL",
            [
                ':NEWEMAIL' => $this->request['formData']['email']
            ]
        );

        $orgdetails = getOrgDetails($this->request['formData']['organizationID']);

        //examine checkbox for copying user to child organizations
        $copytodescendants  =  $this->request['formData']['copytodescendants'];


        if (dbnr($checkUserQuery) > 0) {
            // user exists
            $checkUser = dbf($checkUserQuery);

            // check they are not an admin.
            if ($checkUser['user_type_id'] != 1) {
                $data['type'] = 401;
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'User with this email cannot be added to any organizations.'
                ];
                return $data;
            }
            // Are they already on this org?
            $checkUserQuery = dbq("SELECT * FROM users_to_organizations WHERE user_id = :UID AND organization_id = :ORGID",
                [
                    ':UID' => $checkUser['user_id'],
                    ':ORGID' => $this->request['formData']['organizationID']
                ]
            );

            // check they are not already on this org.
            if (dbnr($checkUserQuery) > 0) {
                $data['type'] = 401;
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'User is already on this organization.'
                ];
                return $data;
            }

            // add them to this org and send an email.
            $insert = dbq('INSERT INTO users_to_organizations (
					user_id,
					organization_id,
					user_primary
				  ) VALUES (
				  	:UID,
				  	:ORGID,
				  	0
				  )',
                [
                    ':UID' => $checkUser['user_id'],
                    ':ORGID' => $this->request['formData']['organizationID']
                ]
            );





            $emailContent = "Dear " . $checkUser['user_first_name'] . "<br />
<br />
You have been added as a user on the F&F Uniforms account for " . $orgdetails['organization_name'] . "<br />
<br />
You can access this organization by logging in to your existing account and switching to the 'Linked Organizations' tab of the 'Uniform Selection' page.
<br />
<br />
Yours sincerely,<br />
<br />
The F&F Uniforms Team";


        } else {
            // New user. need setting up and a new password.

            $username = $this->request['formData']['firstname'] . ' ' . $this->request['formData']['lastname'];
            $newpass = generateRandomString();
            $newPassHash = password_hash($newpass, PASSWORD_BCRYPT);

            dbq("INSERT INTO users (
					user_id,
					user_type_id,
					user_default_address_id,
					user_name,
					user_first_name,
					user_last_name,
					user_email,
					user_password,
					user_avatar,
					user_work_phone,
					user_mobile_phone,
					user_home_phone,
					user_home_email,
					user_title_id,
					user_salutation
				  ) VALUES (
				  	:USERID,
					:USERTYPEID,
					:USERDEFAULTADDRESSID,
					:USERNAME,
					:USERFIRSTNAME,
					:USERLASTNAME,
					:USEREMAIL,
					:USERPASSWORD,
					:USERAVATAR,
					:USERWORKPHONE,
					:USERMOBILEPHONE,
					:USERHOMEPHONE,
					:USERHOMEEMAIL,
					:USERTITLEID,
					:USERSALUTATION
				  )",
                [
                    ':USERID' => null,
                    ':USERTYPEID' => 1,
                    ':USERDEFAULTADDRESSID' => null,
                    ':USERNAME' => $username,
                    ':USERFIRSTNAME' => $this->request['formData']['firstname'],
                    ':USERLASTNAME' => $this->request['formData']['lastname'],
                    ':USEREMAIL' => $this->request['formData']['email'],
                    ':USERPASSWORD' => $newPassHash,
                    ':USERAVATAR' => null,
                    ':USERWORKPHONE' => null,
                    ':USERMOBILEPHONE' => null,
                    ':USERHOMEPHONE' => null,
                    ':USERHOMEEMAIL' => null,
                    ':USERTITLEID' => null,
                    ':USERSALUTATION' => null
                ]
            );

            $userID = dbid();

            // add them to this org and send an email.
            $insert = dbq('INSERT INTO users_to_organizations (
					user_id,
					organization_id,
					user_primary
				  ) VALUES (
				  	:UID,
				  	:ORGID,
				  	0
				  )',
                [
                    ':UID' => $userID,
                    ':ORGID' => $this->request['formData']['organizationID']
                ]
            );

            $emailContent = "Dear " . $this->request['formData']['firstname'] . "<br />
<br />
You have been added as a user on the F&F Uniforms account for " . $orgdetails['organization_name'] . "<br />
<br />
We have set you up with an account and you can log in at <a href='http://www.ff-ues.com'>www.ff-ues.com</a> using the below details.<br />
<br />
email: " . $this->request['formData']['email'] . "<br />
pass: " . $newpass . "<br />
<br />
Please log in and update your contact details in the 'Your Profile' section of the 'Account' page.<br />
<br />
<br />
Yours sincerely,<br />
<br />
The F&F Uniforms Team";

        }

        $subject = 'Access to F&F Uniforms account';
        $title = $subject;
        $template = 'tesco-ff-uniforms';
        $tag = ['ff-add-user'];

        $toName = $this->request['formData']['firstname'] . " " . $this->request['formData']['lastname'];
        $emailAddress = [
            $toName => $this->request['formData']['email']
            //'Andrew Lindsay' => 'andy@slickstitch.com'
        ];

        $message = $emailContent;

        $template_content = array(
            [
                'name' => 'emailtitle',
                'content' => $title
            ],
            [
                'name' => 'main',
                'content' => $message
            ]
        );

      //firefly  send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'User added to organization.'
        ];

        return $data;
    }

    public function changeprimaryuser()
    {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['formData']) || empty($this->request['formData'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $masterOrgID = getMasterOrgID($this->request['formData']['organizationID']);

        // Check if they exist first.
        $checkUserQuery = dbq("SELECT * FROM users_to_organizations WHERE user_id = :USERID AND organization_id = :ORGID",
            [
                ':USERID' => $this->request['formData']['user']['user_id'],
                ':ORGID' => $masterOrgID
            ]
        );

        if (dbnr($checkUserQuery) > 0) {

            // remove them
            dbq("UPDATE users_to_organizations SET user_primary = 0 WHERE organization_id = :ORGID",
                [
                    ':ORGID' => $masterOrgID
                ]
            );

            dbq("UPDATE users_to_organizations SET user_primary = 1 WHERE user_id = :USERID AND organization_id = :ORGID",
                [
                    ':USERID' => $this->request['formData']['user']['user_id'],
                    ':ORGID' => $masterOrgID
                ]
            );

            $data['messages'][] = [
                'type' => 'success',
                'message' => 'User has been made primary contact.'
            ];

        } else {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'User is not part of this organization.'
            ];
            return $data;
        }

        return $data;
    }

    public function deleteuser()
    {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['formData']) || empty($this->request['formData'])) {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $masterOrgID = getMasterOrgID($this->request['formData']['organizationID']);

        // Check if they exist first.
        $checkUserQuery = dbq("SELECT * FROM users_to_organizations WHERE user_id = :USERID AND organization_id = :ORGID",
            [
                ':USERID' => $this->request['formData']['user']['user_id'],
                ':ORGID' => $masterOrgID
            ]
        );

        if (dbnr($checkUserQuery) > 0) {
            // check they are not the only one
            $checkUserQuery = dbq("SELECT * FROM users_to_organizations WHERE organization_id = :ORGID",
                [
                    ':ORGID' => $masterOrgID
                ]
            );

            if (dbnr($checkUserQuery) > 1) {
                // remove them
                dbq("DELETE FROM users_to_organizations WHERE user_id = :USERID AND organization_id = :ORGID",
                    [
                        ':USERID' => $this->request['formData']['user']['user_id'],
                        ':ORGID' => $masterOrgID
                    ]
                );

                // are they on any other orgs?  If not remove the whole account.
                $checkUserOtherQuery = dbq("SELECT * FROM users_to_organizations WHERE user_id = :USERID",
                    [
                        ':USERID' => $this->request['formData']['user']['user_id'],
                    ]
                );

                $data['messages'][] = [
                    'type' => 'success',
                    'message' => 'User has been removed from this organization.'
                ];

                if (dbnr($checkUserOtherQuery) < 1) {
                    // remove the user completely.
                    dbq("DELETE FROM users WHERE user_id = :USERID",
                        [
                            ':USERID' => $this->request['formData']['user']['user_id'],
                        ]
                    );
                    $data['messages'][] = [
                        'type' => 'info',
                        'message' => 'User was not a member of any other organizations so has been removed.'
                    ];
                }


            } else {
                $data['type'] = 401;
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'Cannot remove the only user.'
                ];
                return $data;
            }

        } else {
            $data['type'] = 401;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'User is not part of this organization.'
            ];
            return $data;
        }

        return $data;
    }

    public function thread_update()
    {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(isset($this->request['organization_logo_id']) || empty($this->request['organization_logo_id']))
        {
            if (!isset($this->request['organization_logo_id']) || empty($this->request['organization_logo_id']))
            {
                $data['type'] = 403;
                $data['messages'][] = [
                    'type' => 'danger',
                    'message' => 'Cannot provide results, required arguments are missing.'
                ];
                return $data;
            }
        }

        $logoBefore = getLogoDetails($this->request['organization_logo_id']);

        $coloursBefore = getColourString($logoBefore['thread_array']);
        $coloursAfter = getColourString(implode(',',$this->request['threads']));

        $updateQuery = dbq("UPDATE organization_logos SET thread_array = :THREADS, logo_digitized = 1, needs_to_update_ebos = 1, threads_updated = 1, alert_ticket_id = 0 WHERE organization_logo_id = :ORGANIZATION_ID ",
            [
                ':THREADS'         => implode(',',$this->request['threads']),
                ':ORGANIZATION_ID' =>  $this->request['organization_logo_id']
            ]);

        $orgID = getMasterOrgID($this->request['organization_id']);

        audit(9, $orgID,$this->request['tesco_style_ref']);

        addTicket(10, $orgID, 'Threads Updated.', $this->request['tesco_style_ref'] . ' threads set from '."\n\n".$coloursBefore."\n\n to \n\n". $coloursAfter);

        resolveTicket($this->request['alert_ticket_id']); // remove the ticket telling us to change the threads.

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Thread update successfully'
        ];
        return $data;
    }


    public function samplerequest()
    {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['sample_request']['organization_logo_id']) || empty($this->request['sample_request']['organization_logo_id']))
        {
            $data['type'] = 403;
            $data['massage'][] =
                [
                    'type' => 'danger',
                    'message' => 'Cannot provide results, required arguments are missing.'
                ];
            return $data;
        }

        $logodetail = getLogoDetails($this->request['sample_request']['organization_logo_id']);

        $ticketmessage = $this->request['sample_request']['product']['product_name']  . ' - ' . $this->request['sample_request']['colour'] . ' - ' . $this->request['sample_request']['product_size'] . ' (' . $this->request['sample_request']['emblem_style_ref']. ')'  ;

        $insertQuery = dbq("INSERT INTO ss_orders
                                        (
                                          deliveryContact,
                                          deliveryAddress,
                                          deliveryTown,
                                          deliveryCounty,
                                          deliveryPostCode,
                                          deliveryPhone,
                                          deliveryEmail,
                                          NumberofLines,
                                          NumberofItems,
                                          Date,
                                          Notes,
                                          SKU,
                                          Qty,
                                          LogoID1,
                                          LogoPosition1,
                                          LineSent )
                                  VALUES(
                                          'Golley Slater',
                                          :SCHOOLNAME,
                                          'Richmond House,Guiseley',
                                          'Leeds',
                                          'LS20 8BS',
                                          '01943 484444',
                                          '',
                                          '1',
                                          '1',
                                          NOW(),
                                          :NOTES,
                                          :SKU,
                                          '1',
                                          :LOGO_ID1,
                                          :LOGO_POSITION,
                                          '0'
                                        ) ",
            [
                ':NOTES'          => $ticketmessage,
                ':SKU'            => $this->request['sample_request']['product_sku'],
                ':SCHOOLNAME'     => $logodetail['school_name'].' ('.$logodetail['tesco_style_ref'].')',
                ':LOGO_ID1'       => $this->request['sample_request']['emblem_style_ref'],
                ':LOGO_POSITION'  => $this->request['sample_request']['product']['product_emblem_position'],
            ]);

        $orderID = dbid();

        $ticketmessage = 'Sample order SAMP'.sprintf('%06d',$orderID) . ':'."\n" . $ticketmessage ;

        $logodetail = getLogoDetails($this->request['sample_request']['organization_logo_id']);

        $updateOrderId = dbq("UPDATE ss_orders SET orderId = :ORDERID WHERE unique_id = :UNIQUEID ",
            [
                ':ORDERID' => 'SAMP'.sprintf('%06d',$orderID),
                ':UNIQUEID' => $orderID
            ]);

        if($logodetail['logo_approved'] == 1){
            $bgColour = $logodetail['primary_background_colour'];
        } else {
            $bgColour = $this->request['sample_request']['background_rgb'];
        }

        $updateLogoSample = dbq("UPDATE organization_logos SET logo_sampled = '0' ,primary_background_colour = :BACKGROUND_COLOUR  WHERE organization_logo_id = :ORGANIZATION_LOGO_ID ",
            [
                ':ORGANIZATION_LOGO_ID' => $this->request['sample_request']['organization_logo_id'],
                ':BACKGROUND_COLOUR'    => $bgColour
            ]);


        $size = explode('x',$logodetail['size__mm_']);
        // upload logos to Pulse

        $LUcsv = 'Design Number,Name,Type,Width,Height,Stitch Count,Colours,Last modified,Threads,Notes'."\n";

        $LUcsv .= '"'.$logodetail['tesco_style_ref'].'",';
        $LUcsv .= '"'.$logodetail['school_name'].'",';
        $LUcsv .= '"Embroidery",';
        $LUcsv .= '"'.$size[0].'",';
        $LUcsv .= '"'.$size[1].'",';
        $LUcsv .= '"'.$logodetail['stitch_count'].'",';
        $LUcsv .= '"'.count(explode(',',$logodetail['thread_array'])).'",';
        $LUcsv .= '"'.date("c",$logodetail['logo_last_updated']).'",';
        $LUcsv .= '"'.$logodetail['thread_array'].'",';
        $LUcsv .= '""';

        $LUFileName = 'SampleLogos/'.$logodetail['tesco_style_ref'].'_LU.csv';

        file_put_contents($LUFileName,$LUcsv);

        $remotefile = '/Tesco/SampleLogos/'.$logodetail['tesco_style_ref'].'_LU.csv';
        $localfile = API_ROOT . $LUFileName;

        dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
            [
                ':LOCALFILE' => $localfile,
                ':REMOTEFILE' => $remotefile,
                ':HOSTPREFIX' => ''
            ]
        );

        include_once(DIR_CLASSES . 'eBOS_API.php');
        $eBOS_API = new eBOS_API(APIUser, APIPass);

        $oldDesign = $eBOS_API->getDesignDetails($logodetail['ebos_id']);

        $DSTFileName = 'SampleLogos/'.$logodetail['tesco_style_ref'].'.dst';

        file_put_contents($DSTFileName,file_get_contents($oldDesign->design->dst->url));

        $remotefile = '/Tesco/SampleLogos/'.$logodetail['tesco_style_ref'].'.dst';
        $localfile = API_ROOT . $DSTFileName;

        dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
            [
                ':LOCALFILE' => $localfile,
                ':REMOTEFILE' => $remotefile,
                ':HOSTPREFIX' => ''
            ]
        );

        addTicket(10, $this->request['sample_request']['organization_id'], 'Sample Request.', $ticketmessage);
        audit(27,$this->request['sample_request']['organization_id']);

        $data['type'] = 200;
        $data['messages'][] =
            [
                'type' => 'success',
                'message' => 'Sample request sent successfully.'
            ];
        return $data;
    }

    public function copyexistingemblem()
    {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if (!isset($this->request['organization_id']) || empty($this->request['organization_id'])) {
            $data['type'] = 403;
            $data['massage'][] =
                [
                    'type' => 'danger',
                    'message' => 'Cannot provide results, required arguments are missing.'
                ];
            return $data;
        }

        $emblemName =  $this->request['tesco_style_ref'];
        $existingfileName = explode('-', str_replace('-TMP', '', $emblemName));
        $codeParts = end($existingfileName);

        $codeNumber = filter_var($codeParts,FILTER_SANITIZE_NUMBER_INT);
        $codeLetter = str_replace($codeNumber,'',$codeParts);


        $codeSuffix = [
            $codeNumber, // 1
            $codeLetter // A
        ];

        $masterOrgID = getMasterOrgID($this->request['organization_id']);

        $logocodeQuery = dbq("SELECT DISTINCT(tesco_style_ref) FROM organization_logos WHERE organization_id = :ORGANIZATION_ID AND logo_deleted != 1",
            [
                ':ORGANIZATION_ID' => $masterOrgID
            ]);
        while ($fetchLogocode = dbf($logocodeQuery))
        {
            $letterCodepart = explode('-', str_replace('-TMP', '', $fetchLogocode['tesco_style_ref']));
            $letterCodepart = end($letterCodepart);

            $codeNumber = filter_var($letterCodepart,FILTER_SANITIZE_NUMBER_INT);
            $codeLetter = str_replace($codeNumber,'',$letterCodepart);

            if ($codeNumber == $codeSuffix[0]) {
                if ($codeLetter > $codeSuffix[1]) {
                    $codeSuffix[1] = $codeLetter;
                }
            }
        }
        $codeSuffix[1]++;

        $orgInfo = getOrgDetails($this->request['organization_id']);
        $masterOrgInfo = getOrgDetails($masterOrgID);

        if ($orgInfo['organization_id'] != $masterOrgInfo['organization_id']) {
            $logoOrgName = $masterOrgInfo['organization_name'];
        } else {
            $logoOrgName = $orgInfo['organization_name'];
        }

        $newCode = $masterOrgInfo['school_URN'] . '-' . implode('', $codeSuffix); // 100240-1B

        // create design  into eBOS
        include_once(DIR_CLASSES . 'eBOS_API.php');

        $eBOS_API = new eBOS_API(APIUser, APIPass);

        $ticketmessage = 'New logo created ('.$newCode . '-TMP'.')';

        $alertmessage = ''.$newCode . '-TMP needs colours updating.';


        if(!isset($this->request['ebos_id']) || empty($this->request['ebos_id']))
        {
            $designs = $eBOS_API->getDesigns($this->request['tesco_style_ref']);
            $ebos_id = null;

            if (!empty($designs)) {
                foreach ($designs->designs as $design) {
                    if($design->number == $this->request['tesco_style_ref']){
                        $ebos_id = $design->id ;
                    }
                }
            }
        }
        else
        {
            $ebos_id = $this->request['ebos_id'];
        }


        $oldDesign = $eBOS_API->getDesignDetails($ebos_id); // todo get ebos ID if not here.


        $alertUser = 0;//$_SESSION['session']['user']['user_id'];
        $alertComment = '';
        $threadsUpdated = 1;
        $completed = 1;
        $needsupdate = 1;

        // todo check if this will need a specific alert when files are updated.  e.g. New threads.
        if(isset($this->request['description']) && !empty($this->request['description'])){
            $alertUser = $_SESSION['session']['user']['user_id'];
            $alertComment = $this->request['description'];
            $threadsUpdated = 0;
            $completed = 0;
            $needsupdate = 0;
        }

        $designdetail = [
            'design' => [
                'number' => $newCode . '-TMP',
                'name' => $logoOrgName,
                'notes' => 'Created by F&F UES' . "\n\n" . $ticketmessage,
                'threads' => $this->request['emblemThreads'], // Must be strings not integers.
                'dst' => [
                    'data' => base64_encode(file_get_contents($oldDesign->design->dst->url)),
                    'filename' => basename($oldDesign->design->dst->url)
                ]
            ]
        ];

        $newDesign = $eBOS_API->createDesign($designdetail);

        if (isset($newDesign->successful) && $newDesign->successful == false) {
            $data['type'] = 401;

            $data['messages'][] = [
                'type' => 'danger',
                'message' => $newDesign->message
            ];
            return $data;
        }

        // Entry into organization_logo table

        $organizationLogoQuery = dbq("INSERT INTO organization_logos (
                                                    organization_id,
                                                    school_name,
                                                    tesco_style_ref,
                                                    stitch_count,
                                                    size__mm_,
                                                    logo_approved,
                                                    logo_url,
                                                    thread_array,
                                                    logo_colour_count,
                                                    logo_svg_url,
                                                    primary_background_colour,
                                                    logo_requested_name,
                                                    logo_requested_date,
                                                    products_using_logo,
                                                    logo_on_ebos,
                                                    needs_ebos_update,
                                                    ebos_id,
                                                    logo_digitized,
                                                    complete_order,
					                                alert_user,
					                                alert_comment,
					                                threads_updated,
					                                ebos_digitizing_id
                                                    )
                                            VALUES (
                                                    :ORGANIZATIONID,
                                                    :SCHOOLNAME,
                                                    :TESCOSTYLEREF,
                                                    :STITCHCOUNT,
                                                    :SIZEMM,
                                                    :LOGOAPPROVED,
                                                    :LOGOURL,
                                                    :LOGOTHREADS,
                                                    :LOGOCOLOURCOUNT,
                                                    :LOGOSVGURL,
                                                    :PRIMARYBACKGROUNDCOLOUR,
                                                    :LOGOREQUESTEDNAME,
                                                    :LOGOREQUESTEDDATE,
                                                    :PRODUCTSUSINGLOGO,
                                                    :LOGOONEBOS,
                                                    :NEEDSUPDATE,
                                                    :EBOSID,
                                                    :LOGODIGITIZED,
                                                    :COMPLETE,
                                                    :LOGGEDINUSER,
                                                    :ALERTCOMMENT,
                                                    :THREADSUPDATED,
                                                    :EBOSDIGITIZINGID
                                                  )",
            [
                ':ORGANIZATIONID' => $masterOrgID,
                ':SCHOOLNAME' => $this->request['school_name'],
                ':TESCOSTYLEREF' => $newCode . '-TMP',
                ':STITCHCOUNT' => $this->request['stitch_count'],
                ':SIZEMM' => $this->request['size__mm_'],
                ':LOGOAPPROVED' => false,
                ':LOGOURL' => $this->request['logo_url'],
                ':LOGOTHREADS' => implode(',',$this->request['emblemThreads']),
                ':LOGOCOLOURCOUNT' => $this->request['logo_colour_count'],
                ':LOGOSVGURL' => $this->request['logo_svg_url'],
                ':PRIMARYBACKGROUNDCOLOUR' => $this->request['primary_background_colour'],
                ':LOGOREQUESTEDNAME' => $_SESSION['session']['user']['user_first_name'] . ' ' . $_SESSION['session']['user']['user_last_name'],
                ':LOGOREQUESTEDDATE' => date("Y-m-d G:i:s"),
                ':PRODUCTSUSINGLOGO' => 0,
                ':LOGOONEBOS' => 1,
                ':NEEDSUPDATE' => $needsupdate,
                ':EBOSID' => $newDesign->design->id,
                ':LOGODIGITIZED' => $this->request['logo_digitized'],
                ':COMPLETE' => $completed,
                ':LOGGEDINUSER' => $alertUser,
                ':ALERTCOMMENT' => $alertComment,
                ':THREADSUPDATED' => $threadsUpdated,
                ':EBOSDIGITIZINGID' => $this->request['ebos_digitizing_id']
            ]
        );


        /*
          dbq("insert into organization_logos (
					organization_id,
					school_name,
					tesco_style_ref,
					stitch_count,
					size__mm_,
					logo_approved,
					logo_url,
					logo_svg_url,
					primary_background_colour,
					logo_requested_name,
					logo_requested_date,
					products_using_logo,
					logo_on_ebos,
					ebos_id,
					alert_user,
					alert_comment,
					threads_updated
				  ) VALUES (
					:ORGANIZATIONID,
					:SCHOOLNAME,
					:TESCOSTYLEREF,
					:STITCHCOUNT,
					:SIZEMM,
					:LOGOAPPROVED,
					:LOGOURL,
					:LOGOSVGURL,
					:PRIMARYBACKGROUNDCOLOUR,
					:LOGOREQUESTEDNAME,
					:LOGOREQUESTEDDATE,
					:PRODUCTSUSINGLOGO,
					:LOGOONEBOS,
					:EBOSID,
					:LOGGEDINUSER,
					:ALERTCOMMENT,
					:THREADSUPDATED
				  )",
            [
                ':ORGANIZATIONID' => $masterOrgID,
                ':SCHOOLNAME' => $logoOrgName,
                ':TESCOSTYLEREF' => $newCode . '-TMP',
                ':STITCHCOUNT' => $newDesign->design->stitch_count,
                ':SIZEMM' => $newDesign->design->width . 'x' . $newDesign->design->height,
                ':LOGOAPPROVED' => false,
                ':LOGOURL' => '//www.ff-ues.com/images/emblems/tempdesign.png',
                ':LOGOSVGURL' => '//www.ff-ues.com/images/emblems/tempdesign.svg',
                ':PRIMARYBACKGROUNDCOLOUR' => '191,191,189',
                ':LOGOREQUESTEDNAME' => $_SESSION['session']['user']['user_first_name'].' '.$_SESSION['session']['user']['user_last_name'],
                ':LOGOREQUESTEDDATE' => date("Y-m-d G:i:s"),
                ':PRODUCTSUSINGLOGO' => 0,
                ':LOGOONEBOS' => 1,
                ':EBOSID' => $newDesign->design->id,
                ':LOGGEDINUSER' => $alertUser,
                ':ALERTCOMMENT' => $alertComment,
                ':THREADSUPDATED' => $threadsUpdated
            ]
        );
         * */
        // record activity.
        audit(4, $masterOrgID, $newCode . '-TMP');
        // Add as to-do Note.

        // add attachment links to
        addTicket(3, $masterOrgID, 'New Emblem Created.', $ticketmessage);
        // addTicket(3, $masterOrgID, 'Update Emblem Colours.', $alertmessage, true, date('Y-m-d G:i:s', time() + (60 * 6)));

        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Logo Created.'
        ];
        return $data;
    }

    
    public function emblembgupdate()
    {
        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['emblem']) || empty($this->request['emblem']))
        {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $updateQuery = dbq("UPDATE organization_logos SET primary_background_colour = :BACKGROUND_COLOUR
                            WHERE organization_logo_id = :ORGANIZATION_ID ",
            [
                ':BACKGROUND_COLOUR' => $this->request['emblem']['primary_background_colour'],
                ':ORGANIZATION_ID'   => $this->request['emblem']['organization_logo_id']
            ]);

        $orgID = getMasterOrgID($this->request['emblem']['organization_id']);

        audit(9,$orgID,$this->request['emblem']['tesco_style_ref']);

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => 'Emblem background has been updated'
        ];
        return $data;
    }

    public function setquicknote()
    {
        if(!$this->isMethodCorrect('POST'))
        {
            return $this->getResponse(500);
        }

        if(!isset($this->request['organization_id']) && empty($this->request['organization_id']))
        {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results , required arguments are missing.'
            ];

            return $data;
        }

        if($this->request['text'] <> '')
        {
            $checkidQuery = dbq('SELECT * FROM organization_quick_notes WHERE organization_id = :ORGANIZATION_ID',
                [
                    ':ORGANIZATION_ID' => $this->request['organization_id']
                ]);

            if(dbnr($checkidQuery) > 0)
            {
                $noteQuery = dbq('UPDATE organization_quick_notes SET `quick_note` = :QUICK_NOTE WHERE organization_id = :ORGANIZATION_ID ',
                    [
                        ':ORGANIZATION_ID' => $this->request['organization_id'],
                        ':QUICK_NOTE'      => $this->request['text']
                    ]);

                $data['type'] = 200;
                $data['messages'][] = [
                    'type' => 'success',
                    'message' => 'Your note has been updated successfully.'
                ];
            }
            else
            {
                $noteQuery = dbq("INSERT INTO organization_quick_notes(
                                      organization_id,
                                      quick_note
                              )VALUES(
                                      :ORGANIZATION_ID,
                                      :QUICK_NOTE
                                      )",
                    [
                        ':ORGANIZATION_ID' => $this->request['organization_id'],
                        ':QUICK_NOTE'      => $this->request['text']
                    ]);

                $data['type'] = 200;
                $data['messages'][] = [
                    'type' => 'success',
                    'message' => 'Your note has been submitted successfully.'
                ];
            }
        }
        else{

            $deleteNote = dbq('DELETE FROM organization_quick_notes WHERE organization_id = :ORGANIZATION_ID',
                [
                    ':ORGANIZATION_ID' => $this->request['organization_id']
                ]);

            $data['type'] = 200;
            $data['messages'][] = [
                'type' => 'success',
                'message' => 'Your note has been deleted successfully.'
            ];
        }
        return $data;
    }

    public function getquicknote()
    {
        if(!$this->isMethodCorrect('POST'))
        {
            return $this->getResponse(500);
        }

        if(!isset($this->request['organization_id']) && empty($this->request['organization_id']))
        {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results , required arguments are missing.'
            ];
            return $data;
        }
        $noteQuery = dbq("SELECT * FROM `organization_quick_notes` WHERE organization_id = :ORGID",
            [
                ':ORGID' => $this->request['organization_id']
            ]);

        $noteFetch = dbf($noteQuery);
        $data = [
            'quick_note' => $noteFetch['quick_note']
        ];

        return $data;
    }

    public function sendsample()
    {
        if(!$this->isMethodCorrect('POST'))
        {
            return $this->getResponse(500);
        }

        if(!isset($this->request['emblemid']) || empty($this->request['emblemid']) || !isset($this->request['orgid']) || empty($this->request['orgid']))
        {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results , required arguments are missing.'
            ];
            return $data;
        }

        $masterOrgDetail = getOrgDetails(getMasterOrgID($this->request['orgid']));
        $logoDetail = getLogoDetails($this->request['emblemid']);

        $ticketMessage = 'Sample '. $logoDetail['tesco_style_ref'] . ' sent to organization. ';

        // Update stage if applicable.
        if($masterOrgDetail['organization_status_id'] == 5){
            // registered. Update to stage 3 now.
            dbq("update organizations set organization_status_id = 6 where organization_id = :MASTERORG or organization_parent_organization_id = :MASTERORG",
                [
                    ':MASTERORG' => $masterOrgDetail['organization_id']
                ]
            );

            dbq("update school_data set school_status_id = 6 where school_data_id = :SDID",
                [
                    ':SDID' => $masterOrgDetail['school_data_id']
                ]
            );

            $ticketMessage .= "\n".'School status updated.';

        }

        addTicket(14,$masterOrgDetail['organization_id'],'Sample Sent to organization.','Sample '. $logoDetail['tesco_style_ref'] . ' sent to organization. ');
        audit(37,$masterOrgDetail['organization_id'],$logoDetail['tesco_style_ref']);

        $data['type'] = 200;
        $data['messages'][] = [
            'type' => 'success',
            'message' => $ticketMessage
        ];

        return $data;
    }
   

}
