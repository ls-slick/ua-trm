<?php

/**
 * Rest object example
 * 
 * GNU General Public License (Version 2, June 1991) 
 * 
 * This program is free software; you can redistribute 
 * it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, 
 * or (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class stats extends restObject {

	/**
	 * 
	 * @param string $method
	 * @param array $request
	 * @param string $file
	 */
	public function __construct($method, $request = null, $file = null) {
		parent::__construct($method, $request, $file);
	}
	

	public function top() {

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}
		
		$getStatisticsQuery=dbq("select *
                        from statistics");

		while($getStatistics = dbf($getStatisticsQuery)){
			$data['statistics'][$getStatistics['statistic_key']] = (int)$getStatistics['statistic_value'];
		}

		return $data;
	}

	public function demand() {

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$getDemandQuery=dbq("select *
                        from demand_data
                        order by demand_data_id");

		$graphData = [
			'xScaleTicks' => [],

			'demandTarget' => [],
			'demandActual' => [],

			'outputTarget' => [],
			'outputActual' => [],

			'potTarget' => [],
			'potActual' => [],

			'propTarget' => [],
			'propActual' => [],

			'todayTime' => strtotime("Monday this week")*1000

		];



		while($getDemand = dbf($getDemandQuery)){
			$graphData['xScaleTicks'][] = strtotime($getDemand['demand_week'])*1000;

			$graphData['demandTarget'][] = (int)$getDemand['demand_ty_demand_target'];
			$graphData['demandActual'][] = (int)$getDemand['demand_ty_demand_actual'];

			$graphData['outputTarget'][] = (int)$getDemand['demand_ty_output_target'];
			$graphData['outputActual'][] = (int)$getDemand['demand_ty_output_actual'];

			$graphData['potTarget'][] = (int)$getDemand['demand_week_opening_pot_target'];
			$graphData['potActual'][] = (int)$getDemand['demand_week_opening_pot_actual'];

			$graphData['propTarget'][] = (double)$getDemand['demand_proposition_days_target'];
			$graphData['propActual'][] = (double)$getDemand['demand_proposition_days_actual'];
		}


		return $graphData;
	}

}
