<?php

/**
 * Rest object example
 * 
 * GNU General Public License (Version 2, June 1991) 
 * 
 * This program is free software; you can redistribute 
 * it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, 
 * or (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class cron extends restObject {

	var $logstring;
	/**
	 * 
	 * @param string $method
	 * @param array $request
	 * @param string $file
	 */

	public function __construct($method, $request = null, $file = null) {
		//session_destroy();  // No need to tie up a session with a cron job.
		$_SESSION['session']['user']['user_id'] = 1;
		parent::__construct($method, $request, $file);
	}

	public function embroideredallocation() {

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];


		// select them.
		// New School	School ID	Default Image	Local Authority	School Name	Address Line 2	Address Line 3	Town/City	State/County	Post Code

		// select all styles first/
		$styleHeaders = [
			'new' => 'New School',
			'school_URN' => 'School ID',
			'default_logo' => 'Default Image',
			'la' => 'Local Authority',
			'schoolname' => 'School Name',
			'add2' => 'Address Line 2',
			'add3' => 'Address Line 3',
			'town' => 'Town/City',
			'state' => 'State/County',
			'postcode' => 'Post Code'
		];

		$oldHeaderQuery = dbq("select * from export_products
								where decorated = 1
								order by export_product_id");
		while($oldHeader= dbf($oldHeaderQuery)){
			$styleHeaders[$oldHeader['tesco_style_code']] =  $oldHeader['tesco_style_code'].' '.$oldHeader['tesco_style_name'];
		}

		$styleQuery = dbq("select * from product_colours pc
								join products p using (product_id)
								join colours c using (colour_id)
								where p.product_decorated = 1
								order by product_colour_style_ref");
		while($style = dbf($styleQuery)){
			if(!isset($styleHeaders[$style['product_colour_style_ref']])){
				$styleHeaders[$style['product_colour_style_ref']] = $style['product_colour_style_ref'].' '.$style['product_name'].'/'.$style['colour_name'];
			}
		}

		// not yet..

		$data['data']['contents'] = implode(',',$styleHeaders) . PHP_EOL;;

		// now get each schools allocations.
		$organizationQuery = dbq("SELECT *, odl.tesco_style_ref as default_logo FROM organizations o
									  LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
									  LEFT JOIN addresses a on (o.organization_default_address_id = a.address_id)
									  LEFT JOIN organization_logos odl on (o.organization_default_logo_id = odl.organization_logo_id)
									  LEFT JOIN products_colours_to_organizations p2o on (p2o.organization_id = o.organization_id)
									  LEFT JOIN product_colours pc using (product_colour_id)
									  LEFT JOIN organization_logos ol on (ol.organization_logo_id = p2o.organization_logo_id)
									  WHERE o.organization_status_id = 3
									  AND o.organization_has_subunits = 0
									  AND o.school_URN != 'tescotestschool'
									  and ol.logo_deleted != 1
									  ORDER BY o.school_URN");

		// No orgs with sub units,
		// Not testschool
		// only live

		$csv = [];

		while($organization = dbf($organizationQuery )){
			if(!isset($csv[$organization['school_URN']])){
				// first one.
				$csv[$organization['school_URN']] = [
					'new' => 0,
					'school_URN' => $organization['school_URN'],
					'default_logo' => $organization['default_logo'],
					'la' => $organization['local_authority_name'],
					'schoolname' => $organization['organization_name'],
					'add2' => $organization['entry_street_address_2'],
					'add3' => $organization['entry_suburb'],
					'town' => $organization['entry_city'],
					'state' => $organization['entry_state'],
					'postcode' => $organization['entry_postcode']
				];
			}

			$csv[$organization['school_URN']][$organization['product_colour_style_ref']] = @end(explode('-',$organization['tesco_style_ref']));
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($styleHeaders as $key => $header){
				if(isset($csvline[$key])){
					$writeline .= '"'.str_replace("#",'',(trim($csvline[$key]))).'"';
				}
				$writeline .= ',';
			}
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}


		$data['data']['filename'] = 'EmbroideredAllocations'.date('Y-m-d-G-i-s').'.csv';


		return $data;
	}

	public function plainallocation() {

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		// select them.
		// New School	School ID	Default Image	Local Authority	School Name	Address Line 2	Address Line 3	Town/City	State/County	Post Code

		// select all styles first/
		$styleHeaders = [
			'school_URN' => 'Account Name',
			'schoolname' => 'Establishment Name'
		];


		$oldHeaderQuery = dbq("select * from export_products
								where decorated = 0
								order by export_product_id");
		while($oldHeader= dbf($oldHeaderQuery)){
			$styleHeaders[$oldHeader['tesco_style_code']] = $oldHeader['tesco_style_code'].' '.$oldHeader['tesco_style_name'];
		}


		$styleQuery = dbq("select * from product_colours pc
								join products p using (product_id)
								join colours c using (colour_id)
								where p.product_decorated = 0
								order by product_colour_style_ref");
		while($style = dbf($styleQuery)){
			if(!isset($styleHeaders[$style['product_colour_style_ref']])){
				$styleHeaders[$style['product_colour_style_ref']] = $style['product_colour_style_ref'].' '.$style['product_name'].'/'.$style['colour_name'];
			}
		}

		$data['data']['contents'] = '"'.implode('","',$styleHeaders) . '"' . PHP_EOL;;

		// now get each schools allocations.
		$organizationQuery = dbq("SELECT *, odl.tesco_style_ref as default_logo FROM organizations o
									  LEFT JOIN local_authorities la on (la.local_authority_id = o.school_local_authority_id)
									  LEFT JOIN addresses a on (o.organization_default_address_id = a.address_id)
									  LEFT JOIN organization_logos odl on (o.organization_default_logo_id = odl.organization_logo_id)
									  LEFT JOIN products_colours_to_organizations p2o on (p2o.organization_id = o.organization_id)
									  LEFT JOIN product_colours pc using (product_colour_id)
									  LEFT JOIN organization_logos ol on (ol.organization_logo_id = p2o.organization_logo_id)
									  WHERE o.organization_status_id = 3
									  AND o.organization_has_subunits = 0
									  AND o.school_URN != 'tescotestschool'
									  and ol.logo_deleted != 1
									  ORDER BY o.school_URN");

		// No orgs with sub units,
		// Not testschool
		// only live

		$csv = [];

		while($organization = dbf($organizationQuery )){
			if(!isset($csv[$organization['school_URN']])){
				// first one.
				$csv[$organization['school_URN']] = [
					'school_URN' => $organization['school_URN'],
					'schoolname' => $organization['organization_name']
				];
			}

			$csv[$organization['school_URN']][$organization['product_colour_style_ref']] = 'Yes';
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($styleHeaders as $key => $header){
				if(isset($csvline[$key])){
					$writeline .= '"'.str_replace("#",'',(trim($csvline[$key]))).'"';
				}
				$writeline .= ',';
			}
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}


		$data['data']['filename'] = 'PlainAllocations'.date('Y-m-d-G-i-s').'.csv';

		return $data;
	}

	public function createlusheet(){

		// create LU sheet for sapphire.
		ini_set('memory_limit',-1); // No Limit.  Careful.

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];

		// select them.
		// New School	School ID	Default Image	Local Authority	School Name	Address Line 2	Address Line 3	Town/City	State/County	Post Code

		// headers.
		$styleHeaders = [
			'school_URN' => 'School ID',
			'schoolname' => 'School name',
			'product_colour_style_ref' => 'Tesco style ref',
			'product_colour_alt_style_ref' => 'Tesco alt style code',
			'product_category_name' => 'Product group',
			'name_colour' => 'Product name (colour)',
			'tesco_style_ref_logo' => 'Emblem code',
			'stitch_count' => 'Stitch Count',
			'size' => 'Size (mm)',
			'position' => 'Position',
		];

		for($i=1;$i<=30;$i++){
			$styleHeaders['thread_'.$i] = 'Thread '.$i;
		}

		$data['data']['contents'] = implode(',',$styleHeaders) . PHP_EOL;;

		// now get each schools allocations.
		$organizationQuery = dbq("SELECT o.school_URN,
										o.organization_parent_id,
										o.organization_name,
										pc.product_colour_style_ref,
										pc.product_colour_alt_style_ref,
										pca.product_category_name,
									    p.product_name,
									    c.colour_name,
									    ol.tesco_style_ref,
									    ol.stitch_count,
									    ol.size__mm_,
										p.product_emblem_position,
									  	ol.thread_array
										FROM organizations o
									  LEFT JOIN products_colours_to_organizations p2o on (p2o.organization_id = o.organization_id)
									  LEFT JOIN product_colours pc using (product_colour_id)
									  LEFT JOIN products p on (p.product_id = pc.product_id)
									  LEFT JOIN product_categories pca on pca.product_category_id = p.product_category_id
									  LEFT JOIN colours c on pc.colour_id = c.colour_id
									  LEFT JOIN organization_logos ol on (ol.organization_logo_id = p2o.organization_logo_id)
									  WHERE o.organization_status_id = 3
									  AND o.organization_has_subunits = 0
									  AND p.product_decorated = 1
									  AND o.school_URN != 'tescotestschool'
									  and ol.logo_deleted != 1
									  ORDER BY o.school_URN");

		// No orgs with sub units,
		// Not testschool
		// only live

		$csv = [];

		while($organization = dbf($organizationQuery )){

			// first one.
			$lineArray = [
				'school_URN' => $organization['school_URN'],
				'schoolname' => $organization['organization_name'],
				'product_colour_style_ref' =>  $organization['product_colour_style_ref'],
				'product_colour_alt_style_ref' =>  $organization['product_colour_alt_style_ref'],
				'product_category_name' =>  $organization['product_category_name'],
				'name_colour' =>  $organization['product_name'].' ('. $organization['colour_name'].')',
				'tesco_style_ref_logo' => $organization['tesco_style_ref'],
				'stitch_count' =>  $organization['stitch_count'],
				'size' =>  $organization['size__mm_'],
				'position' =>  $organization['product_emblem_position'],
			];

			$threads = explode(',',$organization['thread_array']);

			for($i=1;$i<=30;$i++){
				$keynumber = $i-1;
				if(isset($threads[$keynumber])){
					$lineArray['thread_'.$i] = $threads[$keynumber];
				} else {
					$lineArray['thread_'.$i] = '';
				}

			}

			$csv[] = $lineArray;

			if(!is_null($organization['organization_parent_id'])){
				// add the same entry for this parent?
				$lineArray['school_URN'] = $organization['organization_parent_id'];
				$csv[] = $lineArray;
			}
		}

		// make it into a CSV
		foreach($csv as $csvline){
			$writeline = '';
			foreach($styleHeaders as $key => $header){
				if(isset($csvline[$key])){
					$writeline .= '"'.str_replace("#",'',(trim($csvline[$key]))).'"';
				}
				$writeline .= ',';
			}
			$data['data']['contents'] .= $writeline . PHP_EOL;
		}

		$data['data']['filename'] = 'SchoolOffice-Export-'.date('Y-m-d_G-i-s').'.csv';

		// save locally.
		file_put_contents('lu_sheets/'.$data['data']['filename'],$data['data']['contents']);

		// upload it.
		$conn_id = ftp_connect(FTP_HOST);
		if($conn_id){
			$login_result = ftp_login($conn_id, FTP_USER, FTP_PASS);

			if($login_result){
				ftp_pasv($conn_id, true);
				// upload.
				if(!ftp_put($conn_id,'/Tesco/LU/'.$data['data']['filename'],'./lu_sheets/'.$data['data']['filename'],FTP_ASCII)){
					// error
					debugMail("LU Sheet@ Did not upload.");
				}
			} else {
				// error login in.
				debugMail("LU Sheet: Could not log into FTP.");
			}
		}

		return null;

	}

	public function processftpqueue()
	{

		// should just do them by host.

		$ftpQueuesQuery = dbq("SELECT * FROM ftp_queue GROUP BY ftp_queue_host_prefix");

		while ($ftpQueues = dbf($ftpQueuesQuery)) {
			if(is_null($ftpQueues['ftp_queue_retries'])){
				$ftpQueues['ftp_queue_retries'] = 0;
			}

			$conn_id = null;
			$conn_id = ftp_connect(constant($ftpQueues['ftp_queue_host_prefix'] . 'FTP_HOST'),21,15);
			if ($conn_id) {
				$login_result = ftp_login($conn_id, constant($ftpQueues['ftp_queue_host_prefix'] . 'FTP_USER'), constant($ftpQueues['ftp_queue_host_prefix'] . 'FTP_PASS'));
				if ($login_result) {

					ftp_pasv($conn_id, true);
					$ftpQueueQuery = dbq("SELECT * FROM ftp_queue where ftp_queue_host_prefix = :FTPHOST",
						[
							':FTPHOST' => $ftpQueues['ftp_queue_host_prefix']
						]
					);

					while ($ftpQueue = dbf($ftpQueueQuery)) {

						// does it need a folder creating?
						if(empty($ftpQueues['ftp_queue_host_prefix'])){
							// Yes
							$folder = str_replace(basename($ftpQueue['ftp_queue_remote_file']),'',$ftpQueue['ftp_queue_remote_file']);
							if(!@ftp_chdir($conn_id,$folder)){
								// doesnt exist.
								ftp_mkdir($conn_id,$folder);
							}
						}

						if (!ftp_put($conn_id, $ftpQueue['ftp_queue_remote_file'], $ftpQueue['ftp_queue_local_file'], FTP_BINARY)) {
							// add one to retry.
							dbq('UPDATE ftp_queue SET ftp_queue_retries = :RETRIES WHERE ftp_queue_id = :QID',
								[
									':RETRIES' => $ftpQueue['ftp_queue_retries'] + 1,
									':QID' => $ftpQueue['ftp_queue_id']
								]
							);
						} else {
							// remove from queue.
							dbq('delete from ftp_queue WHERE ftp_queue_id = :QID',
								[
									':QID' => $ftpQueue['ftp_queue_id']
								]
							);
						}
					}
				} else {

					// couldnt connect.
					dbq('UPDATE ftp_queue SET ftp_queue_retries = :RETRIES where ftp_queue_host_prefix = :FTPHOST',
						[
							':RETRIES' => $ftpQueues['ftp_queue_retries'] + 1,
							':FTPHOST' => $ftpQueues['ftp_queue_host_prefix']
						]);
				}
				$conn_id = null;
			} else {

				// couldnt connect.
				dbq('UPDATE ftp_queue SET ftp_queue_retries = :RETRIES where ftp_queue_host_prefix = :FTPHOST',
					[
						':RETRIES' => $ftpQueues['ftp_queue_retries'] + 1,
						':FTPHOST' => $ftpQueues['ftp_queue_host_prefix']
					]
				);
			}



		}

		return null;
	}

	public function updatetoebosold()
	{
		// updates name, number and threads to match system.


		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime(true)
			]  // Hold flags and metrics required to progress app.
		];

		$logoQuery = dbq("SELECT * FROM organization_logos WHERE needs_to_update_ebos = 1 limit 1");

		if (dbnr($logoQuery) > 0) {
			include(DIR_CLASSES . 'eBOS_API.php');

			$data['metrics']['$eBOS_API'] = [
				'start' => microtime(true),
				'stop' => null,
				'runtime' => 0
			];

			$eBOS_API = new eBOS_API(APIUser, APIPass);

			$data['metrics']['$eBOS_API']['stop'] = microtime(true);
			$data['metrics']['$eBOS_API']['runtime'] = $data['metrics']['$eBOS_API']['stop'] - $data['metrics']['$eBOS_API']['start'];

			while ($logo = dbf($logoQuery)) {
				// find in ebos,

				if(empty($logo['ebos_id'])){
					// need to find it

					$data['metrics']['$designs'] = [
						'start' => microtime(true),
						'stop' => null,
						'runtime' => 0
					];

					$designs = $eBOS_API->getDesigns($logo['tesco_style_ref'],1,20,'true');

					$data['metrics']['$designs']['stop'] = microtime(true);
					$data['metrics']['$designs']['runtime'] = $data['metrics']['$designs']['stop'] - $data['metrics']['$designs']['start'];

					$ebos_id = null;

					if (!empty($designs)) {
						// Add logos to database.
						foreach ($designs->designs as $design) {
							if($design->number == $logo['tesco_style_ref'] || $design->number == $logo['tesco_style_ref'].'-TMP'){
								$ebos_id = $design->id ;
								break;
							}
						}
					}

					// update row.
					dbq("update organization_logos set ebos_id = :EBOSID where organization_logo_id = :OLID",
						[
							':EBOSID' => $ebos_id,
							':OLID' => $logo['organization_logo_id']
						]
					);
				} else {
					$ebos_id = $logo['ebos_id'];
				}

				if(!empty($ebos_id)){

					$data['metrics']['$newDesign'] = [
						'start' => microtime(true),
						'stop' => null,
						'runtime' => 0
					];

					$newDesign = $eBOS_API->getDesignDetails($ebos_id);

					$data['metrics']['$newDesign']['stop'] = microtime(true);
					$data['metrics']['$newDesign']['runtime'] = $data['metrics']['$newDesign']['stop'] - $data['metrics']['$newDesign']['start'];

					$existingNotes = '';
					if(isset($newDesign->design->notes) && !empty($newDesign->design->notes)){
						$existingNotes = $newDesign->design->notes;
					}
					$designChanges = [
						'design' => [
							'threads' => explode(',',$logo['thread_array']),
							'number' => $logo['tesco_style_ref'],
							'name' => $logo['school_name'],
							'notes' => $existingNotes."\n\n".'Logo Updated from F&F : '.date('Y-m-d G:i:s')
						]
					];

					$data['metrics']['$designUpdate'] = [
						'start' => microtime(true),
						'stop' => null,
						'runtime' => 0
					];

					$designUpdate = $eBOS_API->updateDesign($ebos_id,$designChanges);

					$data['metrics']['$designUpdate']['stop'] = microtime(true);
					$data['metrics']['$designUpdate']['runtime'] = $data['metrics']['$designUpdate']['stop'] - $data['metrics']['$designUpdate']['start'];

					// TODO - Check success before running below.
					dbq("update organization_logos set needs_to_update_ebos = 0, needs_ebos_update = 1 where organization_logo_id = :OLID",
						[
							':OLID' => $logo['organization_logo_id']
						]
					);

					$newDesign = null;
				}

			}
		}

		return $data;
	}

	public function updatetoebos()
	{
		// updates name, number and threads to match system.

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime(true)
			]  // Hold flags and metrics required to progress app.
		];

		$logoQuery = dbq("SELECT * FROM organization_logos WHERE needs_to_update_ebos = 1");

		if (dbnr($logoQuery) > 0) {
			include(DIR_CLASSES . 'eBOS_API.php');

			$data['metrics']['$eBOS_API'] = [
				'start' => microtime(true),
				'stop' => null,
				'runtime' => 0
			];

			$eBOS_API = new eBOS_API(APIUser, APIPass);

			$data['metrics']['$eBOS_API']['stop'] = microtime(true);
			$data['metrics']['$eBOS_API']['runtime'] = $data['metrics']['$eBOS_API']['stop'] - $data['metrics']['$eBOS_API']['start'];

			while ($logo = dbf($logoQuery)) {
				// find in ebos,

				if(empty($logo['ebos_id'])){
					// need to find it

					$data['metrics']['$designs'.$logo['tesco_style_ref']] = [
						'start' => microtime(true),
						'stop' => null,
						'runtime' => 0
					];

					$designs = $eBOS_API->getDesigns($logo['tesco_style_ref']);

					$data['metrics']['$designs'.$logo['tesco_style_ref']]['stop'] = microtime(true);
					$data['metrics']['$designs'.$logo['tesco_style_ref']]['runtime'] = $data['metrics']['$designs'.$logo['tesco_style_ref']]['stop'] - $data['metrics']['$designs'.$logo['tesco_style_ref']]['start'];

					$ebos_id = null;

					if (!empty($designs)) {
						// Add logos to database.
						//print_r($designs);
						if(isset($designs->designs)){
							if(empty($designs->designs)) {
								if($logo['logo_deleted'] == 1){
									// update row.
									dbq("update organization_logos set needs_to_update_ebos = 0 where organization_logo_id = :OLID",
										[
											':OLID' => $logo['organization_logo_id']
										]
									);
								}
								break;
							}
							foreach ($designs->designs as $design) {
								if($design->number == $logo['tesco_style_ref'] || $design->number == $logo['tesco_style_ref'].'-TMP'){
									$ebos_id = $design->id ;
									break;
								}
							}
						}
					}

					// update row.
					dbq("update organization_logos set ebos_id = :EBOSID where organization_logo_id = :OLID",
						[
							':EBOSID' => $ebos_id,
							':OLID' => $logo['organization_logo_id']
						]
					);
				} else {
					$ebos_id = $logo['ebos_id'];
				}

				if(!empty($ebos_id)){

					$data['metrics']['$newDesign'.$ebos_id] = [
						'start' => microtime(true),
						'stop' => null,
						'runtime' => 0
					];

					$newDesign = $eBOS_API->getDesignDetails($ebos_id);
					//$newDesign = $eBOS_API->getcus($ebos_id);

					$data['metrics']['$newDesign'.$ebos_id]['stop'] = microtime(true);
					$data['metrics']['$newDesign'.$ebos_id]['runtime'] = $data['metrics']['$newDesign'.$ebos_id]['stop'] - $data['metrics']['$newDesign'.$ebos_id]['start'];

					$existingNotes = '';
					if(isset($newDesign->design->notes) && !empty($newDesign->design->notes)){
						$existingNotes = $newDesign->design->notes;
					}
					$designChanges = [
						'design' => [
							'threads' => explode(',',$logo['thread_array']),
							'number' => $logo['tesco_style_ref'],
							'name' => $logo['school_name'],
							'notes' => $existingNotes."\n\n".'Logo Updated from F&F : '.date('Y-m-d G:i:s')
						]
					];

					$data['metrics']['$designUpdate'.$ebos_id] = [
						'start' => microtime(true),
						'stop' => null,
						'runtime' => 0
					];

					$designUpdate = $eBOS_API->updateDesign($ebos_id,$designChanges);

					$updatefromebos = 1;
					if($logo['logo_deleted'] == 1){
						$deleteDesign = $eBOS_API->deleteDesign($ebos_id);
						$updatefromebos = 0;
					}

					$data['metrics']['$designUpdate'.$ebos_id]['stop'] = microtime(true);
					$data['metrics']['$designUpdate'.$ebos_id]['runtime'] = $data['metrics']['$designUpdate'.$ebos_id]['stop'] - $data['metrics']['$designUpdate'.$ebos_id]['start'];

					// TODO - Check success before running below.
					dbq("update organization_logos set needs_to_update_ebos = 0, needs_ebos_update = :UPDATEFROM where organization_logo_id = :OLID",
						[
							':OLID' => $logo['organization_logo_id'],
							':UPDATEFROM' => $updatefromebos
						]
					);

					$newDesign = null;
				}

			}
		}

		return $data;
	}

	public function updatestats(){

		$donationQuery = dbq('SELECT SUM(donation_amount) as donations_amount FROM donations WHERE donation_category IN (5)');
		$donations = dbf($donationQuery);

		// total live schools not including houses.
		$liveQuery = dbq('SELECT count(*) as live_schools FROM organizations WHERE organization_parent_organization_id is NULL and organization_status_id = 3');
		$live = dbf($liveQuery);

		dbq("update statistics set statistic_value = :DONATIONS where statistic_key = 'organization_funds_raised'",
			[
				':DONATIONS' => round($donations['donations_amount'],2)
			]
		);

		dbq("update statistics set statistic_value = :LIVES where statistic_key = 'organizations_registered'",
			[
				':LIVES' => $live['live_schools']
			]
		);

	}

	public function updatefromebos()
	{

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics' => [
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];


		// Lets make sure all files have been uploaded correctly.  If not try again.

		$missingUploadsQuery = dbq("SELECT * FROM organization_logos ol JOIN organizations o USING(organization_id) WHERE
										ol.logo_deleted = 0 AND
										ol.logo_digitized = 1 AND
										((ol.size__mm_ = '64.8x41.6' AND ol.stitch_count = 3269) OR
										(ol.size__mm_ IS NULL AND ol.stitch_count IS NULL)) AND
										needs_ebos_update = 0
										order by ol.priority_process DESC");

		if(dbnr($missingUploadsQuery) > 0){

			include_once(DIR_CLASSES . 'eBOS_API.php');
			$eBOS_API = new eBOS_API(APIUser, APIPass);

			$extensions = [
				'dst',
				'pdf',
				'emb'
			];

			while($missingUploads = dbf($missingUploadsQuery)){

				// start a new ebos each time?
				//$eBOS_API = new eBOS_API(APIUser, APIPass);

				// check for upload files.
				echo "checking ".$missingUploads['tesco_style_ref']."\n";

				$masterOrgDetail =  getOrgDetails($missingUploads['organization_id']);
				//save to file structure.  Check school folder.
				if(!file_exists('unapproved_logo_files/'.$masterOrgDetail['school_URN'])){
					// No upload file for this logo.
					echo 'folder not found '.$masterOrgDetail['school_URN']."\n\n";
					continue; // skip while.
				}

				// folder exists, do the files?
				foreach($extensions as $xtn){
					//echo 'unapproved_logo_files/'.$masterOrgDetail['school_URN'].'/'.$missingUploads['tesco_style_ref'].'.'.$xtn."<br />";
					if(!file_exists('unapproved_logo_files/'.$masterOrgDetail['school_URN'].'/'.$missingUploads['tesco_style_ref'].'.'.$xtn)){
						// file missing.
						echo 'file not found '.$masterOrgDetail['school_URN'].'/'.$missingUploads['tesco_style_ref'].'.'.$xtn;
						continue 2; // skip while.
					}
				}

				// Files exist.  Check ebos.
				if(empty($missingUploads['ebos_id'])) {
					// no ebos logo.  upload it straight.

					echo "getting ".$missingUploads['tesco_style_ref']." ebosID \n";

					$embColours = $eBOS_API->getThreadsFromEMB('unapproved_logo_files/'.$masterOrgDetail['school_URN'].'/'.$missingUploads['tesco_style_ref'].'.emb');

					//

					$designChanges = [
						'design' => [
							'threads' => $embColours,
							'number' => $missingUploads['tesco_style_ref'],
							'name' => $missingUploads['school_name'],
							'notes' => 'Uploaded digitized design from F&F UES. - '.date('r'),
							'dst' => [
								'data' => base64_encode(file_get_contents('unapproved_logo_files/'.$masterOrgDetail['school_URN'].'/'.$missingUploads['tesco_style_ref'].'.dst')),
								'filename' => $missingUploads['tesco_style_ref'].'.dst'
							]
						]
					];

					echo "<pre>".print_r($designChanges,1)."</pre>\n";

					$designUpdate = $eBOS_API->createDesign($designChanges);

					if(isset($designUpdate->successful) && $designUpdate->successful == 'true'){
						dbq('update organization_logos set needs_ebos_update = 1, ebos_id = :EBOSID where organization_logo_id = :OLID',
							[
								':EBOSID' => $designUpdate->design->id,
								':OLID' => $missingUploads['organization_logo_id'],
							]
						);
						echo "update ".$missingUploads['tesco_style_ref']." SUCCESS\n\n";
					} else {
						echo "update ".$missingUploads['tesco_style_ref']." FAILED\n\n";
					}

				} else {
					// just needs the upload to ebos.

					echo "using ".$missingUploads['tesco_style_ref']." existing ebosID\n\n";

					// echo "<pre>".print_r($missingUploads,1)."</pre>";
					$newDesign = $eBOS_API->getDesignDetails($missingUploads['ebos_id']);

					$embColours = $eBOS_API->getThreadsFromEMB('unapproved_logo_files/'.$masterOrgDetail['school_URN'].'/'.$missingUploads['tesco_style_ref'].'.emb');

					$designChanges = [
						'design' => [
							'threads' => $embColours,
							'number' => $newDesign->design->number,
							'name' => $newDesign->design->name,
							'notes' => $newDesign->design->notes."\n\n".'Uploaded digitized design from F&F UES. - '.date('r'),
							'dst' => [
								'data' => base64_encode(file_get_contents('unapproved_logo_files/'.$masterOrgDetail['school_URN'].'/'.$missingUploads['tesco_style_ref'].'.dst')),
								'filename' => $missingUploads['tesco_style_ref'].'.dst'
							]
						]
					];

					echo "<pre>".print_r($designChanges,1)."</pre>\n";

					//$data['data']['designChanges'][] = $designChanges;

					$designUpdate = $eBOS_API->updateDesign($missingUploads['ebos_id'],$designChanges);

					echo "<pre>".print_r($designUpdate,1)."</pre>\n";

					if (isset($designUpdate->successful) && $designUpdate->successful == 'true'){
						dbq('update organization_logos set needs_ebos_update = 1 where organization_logo_id = :OLID',
							[
								':OLID' => $missingUploads['organization_logo_id'],
							]
						);
						echo "update ".$missingUploads['tesco_style_ref']." SUCCESS\n\n";
					} else {

						echo "update ".$missingUploads['tesco_style_ref']." FAILED\n\n";
					}
				}

				//$eBOS_API->logout();
				//$eBOS_API = null;
			}
		}

		$logoQuery = dbq("select * from organization_logos where needs_ebos_update > 0 and logo_deleted = 0 order by priority_process DESC, logo_requested_date DESC limit 50");

		if(dbnr($logoQuery) > 0) {
			include_once(DIR_CLASSES . 'eBOS_API.php');
			$eBOS_API = new eBOS_API(APIUser, APIPass);

			while ($logo = dbf($logoQuery)) {

				// find in ebos,
				$this->logstring .= json_encode($logo).PHP_EOL;
				if(empty($logo['ebos_id'])){
					// need to find it
					$designs = $eBOS_API->getDesigns($logo['tesco_style_ref']);
					$ebos_id = null;

					if (!empty($designs)) {
						// Add logos to database.
						foreach ($designs->designs as $design) {
							if($design->number == $logo['tesco_style_ref'] || $design->number == $logo['tesco_style_ref'].'-TMP'){
								$ebos_id = $design->id ;
								break;
							}
						}
					}

					// update row.
					dbq("update organization_logos set ebos_id = :EBOSID where organization_logo_id = :OLID",
						[
							':EBOSID' => $ebos_id,
							':OLID' => $logo['organization_logo_id']
						]
					);
				} else {
					$ebos_id = $logo['ebos_id'];
				}

				$this->logstring .= $ebos_id.PHP_EOL;

				if (!empty($ebos_id)) {
					// check the current logo.
							timeecho('getDesignDetail');
							//sleep(1); //needs a delay for some reason.
							$designDetail = $eBOS_API->getDesignDetails($ebos_id);
							timeecho('getDesignDetail');

							$this->logstring .= json_encode($designDetail).PHP_EOL;
							// check that its still the same.

							if(stristr($designDetail->design->logo->png_url,'/assets/processing.png')){
								// cant do it.
								echo $ebos_id." - ".$logo['tesco_style_ref'].' still processing.'."\n";
								// update
								if($logo['needs_ebos_update'] > 4){
									// tried 5 times now and it still didnt work.  Force an update.

									$designChanges = [
										'design' => [
											'number' => $logo['tesco_style_ref'],
											'name' => $logo['school_name']
										]
									];
									timeecho('updateDesign');
									$designUpdate = $eBOS_API->updateDesign($ebos_id,$designChanges);
									timeecho('updateDesign');

									// update the needs_ebos_update flag back to 1.
									dbq("update organization_logos set needs_ebos_update = 1 WHERE organization_logo_id = :OLID",
										[
											':OLID' => $logo['organization_logo_id']
										]
									);
								} else {
									// update the needs_ebos_update flag.
									dbq("update organization_logos set needs_ebos_update = needs_ebos_update + 1 WHERE organization_logo_id = :OLID",
										[
											':OLID' => $logo['organization_logo_id']
										]
									);
								}
							} else {
								// update to match ebos.
								$url = $designDetail->design->logo->png_url;
								//$url = @reset(explode("?", $url));

								if(!empty($url)){

									echo $url."<br/>";

									$imageContent = file_get_contents($url);

									//echo "<pre>".print_r($imageContent,1)."</pre>";

									try {
										$im = new Imagick();
										$im->readImageBlob($imageContent);
										$im->scaleImage(200,200,true); // 200px best fit

										//$filename = basename($url);

										$image = @reset(explode("?", basename($url)));

										//return $image;

										// Save it.
										// check if we are overwriting?
										if(file_exists(FS_ROOT.'images/emblems/'.$image)){
											// delete first.
											unlink(FS_ROOT.'images/emblems/'.$image);
										}

										$im->writeImage(FS_ROOT.'images/emblems/'.$image);

										$newimage 	  = '//www.ff-ues.com/images/emblems/'.$image;
										$newsvgimgage = str_replace('http://','//',$designDetail->design->logo->url);

										$threadArray = [];

										foreach($designDetail->design->threads as $thread){
											$threadArray[] = $thread->code;
										}

										// update dbase.
										dbq("UPDATE organization_logos SET
											logo_url = :NEWIMG,
											logo_svg_url = :NEWSVGIMG,
											stitch_count = :STITCHCOUNT,
											size__mm_ = :SIZEMM,
											thread_array = :THREADARRAY,
											logo_colour_count = :COLOURCOUNT,
											ebos_id = :EBOSID,
											needs_ebos_update = 0,
											update_customer = 0,
											priority_process = 0,
											logo_last_updated = :TIMEST
							
										  WHERE
										  	organization_logo_id = :OLID",
											[
												':NEWIMG' => $newimage,
												':NEWSVGIMG' => $newsvgimgage,
												':STITCHCOUNT' =>  $designDetail->design->stitch_count,
												':SIZEMM' => $designDetail->design->width . 'x' . $designDetail->design->height,
												':THREADARRAY' => implode(',',$threadArray),
												':COLOURCOUNT' => sizeof($threadArray),
												':EBOSID' => $ebos_id,
												':TIMEST' => time(),
												':OLID' => $logo['organization_logo_id']
											]
										);

										// set alert to say logo was updated.
										//addTicket()

										if($logo['update_customer'] == 1){

											// send email.
											$emailContent = "
Emblem {$logo['tesco_style_ref']} on the F&F Uniforms website has been updated.<br />
Please log into your account and review any changes.<br />
<br />
<br />
Yours sincerely,<br />
<br />
The F&F Uniforms Team";

											$subject = 'Emblem '.$logo['tesco_style_ref'].' updated on F&F Uniforms';
											$title = $subject;
											$template = 'tesco-ff-uniforms';
											$tag = ['ff-emblem-update'];

											// get primary user, or requested user!
											$userQuery = dbq("SELECT * FROM users u JOIN users_to_organizations u2o USING (user_id) WHERE u2o.organization_id = :ORGID",
												[
													':ORGID' => $logo['organization_id']
												]
											);

											$emailAddress = [];

											while($user = dbf($userQuery)){
												$emailAddress[$user['user_name']] =  $user['user_email'];
											}

											$message = $emailContent;

											$template_content = array(
												[
													'name' => 'emailtitle',
													'content' => $title
												],
												[
													'name' => 'main',
													'content' => $message
												]
											);

											send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);

										}

										$data[] = [
											':NEWIMG' => $newimage,
											':OLID' => $logo['organization_logo_id']
										];
									} catch(Exception $e) {
										echo $logo['tesco_style_ref'].' Cant open image';
										// echo "<pre>".print_r($e,1)."</pre>";
										// couldnt read file?
										if($logo['needs_ebos_update'] > 2){
											// tried 3 times now and it still didnt work.  Force an update. Notes overwriting.

											$designChanges = [
												'design' => [
													'number' => $logo['tesco_style_ref'],
													'name' => $logo['school_name']
												]
											];
											timeecho('updateDesign'.__LINE__);
											$designUpdate = $eBOS_API->updateDesign($ebos_id,$designChanges);
											timeecho('updateDesign'.__LINE__);

											// update the needs_ebos_update flag back to 1.
											dbq("update organization_logos set needs_ebos_update = 1 WHERE organization_logo_id = :OLID",
												[
													':OLID' => $logo['organization_logo_id']
												]
											);
										} else {
											// update the needs_ebos_update flag.
											dbq("update organization_logos set needs_ebos_update = needs_ebos_update + 1 WHERE organization_logo_id = :OLID",
												[
													':OLID' => $logo['organization_logo_id']
												]
											);
										}
									}

								} else {

									if($logo['needs_ebos_update'] > 2){
										// tried 3 times now and it still didnt work.  Force an update.

											$designChanges = [
											'design' => [
												'number' => $logo['tesco_style_ref'],
												'name' => $logo['school_name']
											]
										];
										timeecho('updateDesign'.__LINE__);
										$designUpdate = $eBOS_API->updateDesign($ebos_id,$designChanges);
										timeecho('updateDesign'.__LINE__);

										// update the needs_ebos_update flag back to 1.
										dbq("update organization_logos set needs_ebos_update = 1 WHERE organization_logo_id = :OLID",
											[
												':OLID' => $logo['organization_logo_id']
											]
										);
									} else {
										// update the needs_ebos_update flag.
										dbq("update organization_logos set needs_ebos_update = needs_ebos_update + 1 WHERE organization_logo_id = :OLID",
											[
												':OLID' => $logo['organization_logo_id']
											]
										);
									}

									echo $ebos_id.' Empty'."\n";

								//	echo "<pre>".print_r($eBOS_API,1)."</pre>";
									echo "<pre>".print_r($designDetail,1)."</pre>";
								}

							}

							$designDetail = null;

				} else {
					// logo not found on eBOS.
					echo $logo['tesco_style_ref'].' not found'."\n";
					// set needs update to 0 so that it wont be picked up next time?
					dbq("update organization_logos set needs_ebos_update = 0 WHERE organization_logo_id = :OLID",
						[
							':OLID' => $logo['organization_logo_id']
						]
					);
				}

			} // End while($logo)
			//$eBOS_API->logout();
			//$eBOS_API = null;
		}

		return null;
	}

	public function sendbulk(){

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$bulkOrderQuery = dbq("select obo.*, o.school_URN, o.organization_name, o.organization_parent_organization_id, '' as clubcard_number, 'Yes' as is_new, '' as contact_name from organization_bulk_orders obo
									join organizations o USING (organization_id)
									where organization_bulk_order_sent = 0");

		if(dbnr($bulkOrderQuery) > 0) {
			// have bulks to send.

			$headers = [
				'organization_bulk_order_id' => 'Order no.',
				'order_date' => 'Order date',
				'school_URN' => 'School/House ID',
				'is_new' => 'New Customer',
				'order_delivery_postcode' => 'Postcode',
				'order_delivery_email' => 'Email',
				'order_contact_name' => 'Contact name',
				'order_telephone' => 'Telephone',
				'clubcard_number' => 'Clubcard Number',
				'product_colour_style_ref' => 'Style Ref',
				'product_name' => 'Description',
				'product_size_name' => 'Size',
				'product_ean' => 'EAN',
				'product_price' => 'Price per unit',
				'product_quantity' => 'Qty',
				'product_line_price' => 'Total price',
				'order_delivery_address_1' => 'Delivery Address 1',
				'order_delivery_address_2' => 'Delivery Address 2',
				'order_delivery_address_3' => 'Delivery Address 3',
				'order_delivery_town' => 'Delivery Town',
				'order_delivery_county' => 'Delivery County',
				'order_delivery_postcode_again' => 'Delivery Postcode'
			];

			while($bulkOrder = dbf($bulkOrderQuery)){

				if($bulkOrder['organization_parent_organization_id'] > 0) {
					// House.  Needs a house name.
					$parentOrg = getOrgDetails($bulkOrder['organization_parent_organization_id']);
					if(!stristr($bulkOrder['organization_name'],$parentOrg['organization_name'])){
						$bulkOrder['organization_name'] = $parentOrg['organization_name'] . ' ('.$bulkOrder['organization_name'].')';
					}
				}

				$csv = [];

				$csv[] = $headers;

				$bulkOrderItemsQuery = dbq("select oboi.* from organization_bulk_order_items oboi
									where oboi.organization_bulk_order_id = :OBOID",
					[
						':OBOID' => $bulkOrder['organization_bulk_order_id']
					]
				);

				// get items too.
				while($bulkOrderItems = dbf($bulkOrderItemsQuery)){
					$csvline = [
						'organization_bulk_order_id' => $bulkOrder['organization_bulk_order_id'],
						'order_date' => $bulkOrder['order_date'],
						'school_URN' => $bulkOrderItems['organization_urn'],
						'is_new' => $bulkOrder['is_new'],
						'order_delivery_postcode' => $bulkOrder['order_delivery_postcode'],
						'order_delivery_email' => $bulkOrder['order_email_address'],
						'order_contact_name' => $bulkOrder['order_contact_name'],
						'order_telephone' => $bulkOrder['order_telephone'],
						'clubcard_number' => $bulkOrder['clubcard_number'],
						'product_colour_style_ref' => $bulkOrderItems['product_colour_style_ref'],
						'product_name' => $bulkOrderItems['product_name'] . ' ('.$bulkOrderItems['product_colour_name'] .')',
						'product_size_name' => $bulkOrderItems['product_size_name'],
						'product_ean' => $bulkOrderItems['product_ean'],
						'product_price' => $bulkOrderItems['product_price'],
						'product_quantity' => $bulkOrderItems['product_quantity'],
						'product_line_price' => $bulkOrderItems['product_line_price'],
						'order_delivery_address_1' => $bulkOrder['order_delivery_address_1'],
						'order_delivery_address_2' => $bulkOrder['order_delivery_address_2'],
						'order_delivery_address_3' => $bulkOrder['order_delivery_address_3'],
						'order_delivery_town' => $bulkOrder['order_delivery_town'],
						'order_delivery_county' => $bulkOrder['order_delivery_county'],
						'order_delivery_postcode_again' => $bulkOrder['order_delivery_postcode']
					];

					$csv[] = $csvline;
				}

				// save the CSV
				$fileContents = '';
				foreach($csv as $csvline){
					$writeline = '';

					foreach($headers as $key => $header){
						if(isset($csvline[$key])){
							if(is_numeric($csvline[$key]) && $key != 'product_ean' && $key != 'school_URN'){
								$writeline .= ''.$csvline[$key].'';
							} else {
								$writeline .= '"=""'.str_replace("#",'',(trim($csvline[$key]))).'"""';
							}
						}
						$writeline .= ',';
					}
					$fileContents .= $writeline . PHP_EOL;
				}

				$filename = 'BulkOrder'.$bulkOrder['organization_bulk_order_id'].'.csv';

				// save it to a file..
				file_put_contents('bulkorders/'.$filename,$fileContents);

				// Send an email.
				$subject = 'New Bulk Order: '.$bulkOrder['organization_bulk_order_id'];
				$title = $subject;
				$template = 'tesco-ff-uniforms';
				$tag = ['ff-bulk-order'];

				$SCHOOLNAME = $bulkOrder['organization_name'];

				$html = "
					A new bulk order request has been made from	$SCHOOLNAME.<br />
					<br />
					Order Number: {$bulkOrder['organization_bulk_order_id']}<br />
					<br />
					<br />";

				$emailAddress = [
					//'F&F Uniform Support' => 'ues@uniformembroideryservice.com',
					'Megan Bettles' => 'Megan.Bettles@uk.tesco.com',
					'Zara Sotiriou' => 'Zara.Sotiriou@uk.tesco.com',
					'Alyson Smithers' => 'Alyson.Smithers@uk.tesco.com ',
					'CSC UES' => 'csc.ues@uk.tesco.com',
					'School Embqueries' => 'School.embqueries@uk.tesco.com',
					'Andrew Lindsay' => 'andy@slickstitch.com'
				];

				$template_content = array(
					[
						'name' => 'emailtitle',
						'content' => $title
					],
					[
						'name' => 'main',
						'content' => $html
					]
				);
				$attachmentArray = [];
				$attachmentArray[] = [
					'type' => "text/csv",
					'name' => basename($filename),
					'content' => base64_encode(file_get_contents(API_ROOT . 'bulkorders/'.$filename))
				];

				send_ues_mail($emailAddress, $subject, $template_content, $template, $tag, $attachmentArray);

				// Mark as sent.
				dbq('update organization_bulk_orders set organization_bulk_order_sent = 1 where organization_bulk_order_id = :OBOID',
					[
						':OBOID' => $bulkOrder['organization_bulk_order_id']
					]
				);

			}


		}

		file_get_contents("http://www.ff-ues.com/api/v1.0/gmo/exportbulk"); // call GMO export while testing.

		return null;
	}

	public function sendbounces(){

		if (!$this->isMethodCorrect('GET')) {
			return $this->getResponse(500);
		}

		$data = [];
		$mandrill = new Mandrill(MANDRILL_API_KEY);
		//$name = $this->request['search'];
		$query = "bounced";//$this->request['search'];
		$label = '';

		$date_from = date("Y-m-d",strtotime('yesterday'));
		$date_to = date("Y-m-d",strtotime('yesterday'));

		$senders = array('noreply@ff-ues.com');
		$data = $mandrill->messages->search($query, $date_from, $date_to, ['ff-registration'], $senders, [MANDRILL_API_KEY]);

		//$result = $mandrill->messages->search($query);

		if(sizeof($data) > 0){
			$csv = [];

			$csv[] = [
				'Date-time',
				'Email address',
				'Status',
				'Reason',
				'Extra info',
				'Subject'
			];

			foreach($data as $key => $moredata){
				//$emailContent = $mandrill->messages->content($moredata['_id']);
				//$data['contents'][] = $emailContent;
				$csv[] = [
					date("Y-m-d H:i:s",$moredata['ts']),
					$moredata['email'],
					$moredata['state'],
					$moredata['bounce_description'],
					$moredata['diag'],
					$moredata['subject']
				];
			}

			//print_r($result);
			$csvString = '';
			// make it into a CSV
			foreach($csv as $csvline){
				foreach($csvline as $value){
					$csvString .= '"'.str_replace("#",'',(trim($value))).'"';
					$csvString .= ',';
				}

				$csvString .= PHP_EOL;
			}


			$filename = 'reports/BounceReport'.date('Y-m-d-G-i-s').'.csv';

			file_put_contents($filename,$csvString);

			$attachmentArray = [];
			$attachmentArray[] = [
				'type' => "text/csv",
				'name' => basename($filename),
				'content' => base64_encode(file_get_contents(API_ROOT . $filename))
			];

		}

		// Find emails opened or not.
		$query = "subject:Your new account on F&F Uniforms";//$this->request['search'];
		$label = '';

		$date_from = date("Y-m-d",strtotime("-3 days"));
		$date_to = date("Y-m-d",strtotime("-3 days"));

		$senders = array('noreply@ff-ues.com');
		$data = $mandrill->messages->search($query, $date_from, $date_to, ['ff-registration'], $senders, [MANDRILL_API_KEY]);

		//$result = $mandrill->messages->search($query);
		if(sizeof($data) > 0) {
			$csv = [];

			$csv[] = [
				'Date-time',
				'Email address',
				'Status',
				'Subject'
			];

			foreach($data as $key => $moredata){
				if($moredata['opens'] == 0){
					// not opened.  add to email.
					if(!stristr($moredata['state'],'bounce')){
						// if not bounced.
						$csv[] = [
							date("Y-m-d H:i:s",$moredata['ts']),
							$moredata['email'],
							$moredata['state'],
							$moredata['subject']
						];
					}
				}
			}

			if(sizeof($csv) > 1){

				$csvString = '';
				// make it into a CSV
				foreach($csv as $csvline){
					foreach($csvline as $value){
						$csvString .= '"'.str_replace("#",'',(trim($value))).'"';
						$csvString .= ',';
					}

					$csvString .= PHP_EOL;
				}

				$filename = 'reports/UnopenedReport'.date('Y-m-d-G-i-s').'.csv';

				file_put_contents($filename,$csvString);

				if(!isset($attachmentArray)){
					$attachmentArray = [];
				}

				$attachmentArray[] = [
					'type' => "text/csv",
					'name' => basename($filename),
					'content' => base64_encode(file_get_contents(API_ROOT . $filename))
				];
			}

		}

		if(!empty($attachmentArray)){
			// Send an email.
			$subject = 'Bounced/Unopened emails reports';
			$title = $subject;
			$template = 'tesco-ff-uniforms';
			$tag = ['ff-ues-email'];

			$html = "Bounced and Unopened email reports attached.";

			$emailAddress = [
				'Andrew Lindsay' => 'andy@slickstitch.com',
				'UES' => 'ues@uniformembroideryservice.com'
			];

			$template_content = array(
				[
					'name' => 'emailtitle',
					'content' => $title
				],
				[
					'name' => 'main',
					'content' => $html
				]
			);

			send_ues_mail($emailAddress, $subject, $template_content, $template, $tag, $attachmentArray);
			return true;
		}

		return false;
	}

	public function collectstock(){
		// Connect to FTP.  Read PSV.

		$conn_id = ftp_connect(FTP_HOST);

		if($conn_id){

			$login_result = ftp_login($conn_id, FTP_USER, FTP_PASS);

			if($login_result){
				ftp_pasv($conn_id, true);

				$stockfiles = ftp_nlist($conn_id,'/Tesco/ProdBal/');

				foreach($stockfiles as $filename){
					if(stristr($filename,'.psv') && !stristr($filename,'stock_tesco_today.psv')){
						// found it.
						//echo 'Found '.$filename;
						if(ftp_get($conn_id,'stock_files/'.basename($filename),$filename,FTP_ASCII)){

							ftp_put($conn_id,'/Tesco/ProdBal/stock_tesco_today.psv','stock_files/'.basename($filename),FTP_ASCII); // Make a 'today' copy

							ftp_rename($conn_id,$filename,str_replace('/ProdBal/','/ProdBal/processed/',$filename));

							// process it.
							$csvData = file_get_contents('stock_files/'.basename($filename));

							dbq("update products_sizes_to_products_colours set product_variation_stock = 0");

							$lines = explode(PHP_EOL, $csvData);

							foreach ($lines as $line) {
								$stockline = str_getcsv($line,'|');

								dbq("update products_sizes_to_products_colours set product_variation_stock = :STOCK where product_variation_sku = :EAN",
									[
										':STOCK' => $stockline[2],
										':EAN' => trim($stockline[0])
									]
								);

							}

							// upload a copy to v-stock

							$vstockHost = 'v-source.co.uk';
							$vstockUser = 'tescofandf_slickstich';
							$vstockPass = 'daHekWonvu';
							$vstockStatusFolder = 'live/incoming'; // live

							$vs_conn_id = ftp_connect($vstockHost);

							if($vs_conn_id) {
								$login_result = ftp_login($vs_conn_id, $vstockUser, $vstockPass);

								if ($login_result) {
									ftp_pasv($vs_conn_id, true);

									// move done files.
									$fileList = ftp_nlist($vs_conn_id,$vstockStatusFolder);

									foreach($fileList as $file){
										if((stristr($file,'.done') && $file != '.done') || stristr($file,'.psv_processed')){
											// move it to the .done folder.
											ftp_rename($vs_conn_id,$file,str_replace('incoming/','incoming/.done/',$file));
										}
									}

									// upload it to VS FTP.
									if(ftp_put($vs_conn_id,$vstockStatusFolder.'/'.basename($filename),'stock_files/'.basename($filename),FTP_BINARY)){
										rename('stock_files/'.basename($filename),'stock_files/processed/'.basename($filename));
									}
								}
							}

						} else {
							//echo 'Error downloading';
						}
					}
				}

			} else {
				// error login in.
				debugMail("Stock Sheet: Could not log into FTP.");
			}
		}
	}

	public function processedifiles(){
		// Log in to Virtual Stock FTP and get files, put on SS FTP.
		$vstockHost = 'v-source.co.uk';
		$vstockUser = 'tescofandf_slickstich';
		$vstockPass = 'daHekWonvu';
		//$vstockOrderFolder = 'uat/outgoing'; // testing
		//$vstockStatusFolder = 'uat/incoming'; // testing
		$vstockOrderFolder = 'live/outgoing'; // live
		$vstockStatusFolder = 'live/incoming'; // live

		$sstockOrderFolder = 'Tesco/DA';
		$sstockStatusFolder = 'Tesco/Out';

		$vs_conn_id = ftp_connect($vstockHost);

		$ss_conn_id = ftp_connect(IMS_FTP_HOST);
		if($ss_conn_id) {
			$ss_login_result = ftp_login($ss_conn_id, IMS_FTP_USER, IMS_FTP_PASS);

			if ($ss_login_result) {
				ftp_pasv($ss_conn_id, true);
			}
		} else {
			return 'NO SS FTP';
		}

		if($vs_conn_id){
			$login_result = ftp_login($vs_conn_id, $vstockUser, $vstockPass);

			if($login_result){
				ftp_pasv($vs_conn_id, true);
				// download.
				$ftpFiles = ftp_nlist($vs_conn_id,$vstockOrderFolder);

				if(is_array($ftpFiles)){
					//var_dump($ftpFiles);
					foreach($ftpFiles as $filename){
						if(stristr($filename,'.csv')){
							// download it to local then upload to SS FTP.
							$filenameonly = basename($filename);
							if(ftp_get($vs_conn_id,'edi/'.$filenameonly,$filename,FTP_ASCII)){
								// move it to done on VS.
								ftp_rename($vs_conn_id,$filename,str_replace('outgoing/','outgoing/.done/',$filename));

								// upload it to SS FTP.
								if(ftp_put($ss_conn_id,$sstockOrderFolder.'/'.$filenameonly,'edi/'.$filenameonly,FTP_BINARY)){
									rename('edi/'.$filenameonly,'edi/processed/'.$filenameonly);
								}

							} else {
								// couldnt get it?
							}

						}
					}
				}

			} else {
				// error login in.
				//debugMail("LU Sheet: Could not log into FTP.");
			}
		}

		// grab SS messages and send to VS.
		$ssftpFiles = ftp_nlist($ss_conn_id,$sstockStatusFolder);

		if(is_array($ssftpFiles)) {
			//var_dump($ftpFiles);
			foreach ($ssftpFiles as $ssfilename) {
				if (stristr($ssfilename, '.psv')) {
					// just upload it.
					$filenameonly = basename($ssfilename);
					if(ftp_get($ss_conn_id,'edi/'.$filenameonly,$ssfilename,FTP_ASCII)){

						// move it to done on VS.
						ftp_rename($ss_conn_id,$ssfilename,str_replace('Out/','Out/Sent/',$ssfilename));

						if(stristr($ssfilename, 'despatch')){
							// make an OSN from it.
							$csvData = file_get_contents('edi/'.$filenameonly);

							$lines = explode(PHP_EOL, $csvData);

							$OSN = '';;
							foreach ($lines as $line) {

								$despatchLine = str_getcsv($line,'|');

								if($despatchLine[0] == 'shipped'){
									// create OSN style CSV.

									if(!stristr($despatchLine[1],'SAMP') && !stristr($despatchLine[1],'WELC')){
										$OSN .= '"'.$despatchLine[1].'",'.
											'"",'.
											'"'.$despatchLine[4].'",'.
											''.$despatchLine[5].','.
											//'"'.$despatchLine[6].'",'.
											'"Dispatched",'.
											''.date('d/m/y').','.
											'"'.date('G:i').'",'.
											'"'.$despatchLine[7].'",'.
											''.$despatchLine[13].','.
											PHP_EOL;
									} elseif(stristr($despatchLine[1],'SAMP') || stristr($despatchLine[1],'WELC')) {
										// process for here.

										$orderDetailQuery = dbq("select * from ss_orders sso join organization_logos ol on ol.tesco_style_ref = sso.LogoID1 where orderId = :ORDERID",
											[
												':ORDERID' => $despatchLine[1]
											]
										);

										if(dbnr($orderDetailQuery) > 0){
											$orderDetail = dbf($orderDetailQuery);

											if(stristr($despatchLine[1],'SAMP')){
												//addTicket(14,$orderDetail['organization_id'],'Sample Order Update','Sapphire status has been updated to : '.$despatchLine[0]);
												audit(34,$orderDetail['organization_id'],$despatchLine[1].' ('.$orderDetail['tesco_style_ref'].') : '.$despatchLine[0].' : '.$despatchLine[7]);
											} else {
												//addTicket(14,$orderDetail['organization_id'],'Welcome Pack Order Update','Sapphire status has been updated to : '.$despatchLine[0]);
												audit(35,$orderDetail['organization_id'],$despatchLine[1].' ('.$orderDetail['tesco_style_ref'].') : '.$despatchLine[0].' : '.$despatchLine[7]);
											}

										}
									}

								}

							}

							if(!empty($OSN)){
								$OSNName = 'OSN_'.date('Ymd_Gis').'.TXT';

								if(file_put_contents('edi/'.$OSNName,$OSN)){
									rename('edi/'.$filenameonly,'edi/processed/'.$filenameonly);
									// upload it to VS FTP.
									if(ftp_put($vs_conn_id,$vstockStatusFolder.'/'.$OSNName,'edi/'.$OSNName,FTP_BINARY)){
										rename('edi/'.$OSNName,'edi/processed/'.$OSNName);
									}
								}
								sleep(1); // filename must be different.
							} else {
								// nothing to do.
								rename('edi/'.$filenameonly,'edi/processed/'.$filenameonly);
							}

						} else {
							// make an OSN from it.
							$csvData = file_get_contents('edi/'.$filenameonly);

							$lines = explode(PHP_EOL, $csvData);

							foreach ($lines as $line) {
								$osnLine = str_getcsv($line,'|');
								if(stristr($osnLine[1],'SAMP') || stristr($osnLine[1],'WELC')){
									// process for here.

									$orderDetailQuery = dbq("select * from ss_orders sso join organization_logos ol on ol.tesco_style_ref = sso.LogoID1 where orderId = :ORDERID",
										[
											':ORDERID' => $osnLine[1]
										]
									);

									if(dbnr($orderDetailQuery) > 0){
										$orderDetail = dbf($orderDetailQuery);

										if(stristr($despatchLine[1],'SAMP')){
											//addTicket(14,$orderDetail['organization_id'],'Sample Order Update','Sapphire status has been updated to : '.$despatchLine[0]);
											audit(34,$orderDetail['organization_id'],$osnLine[1].' ('.$orderDetail['tesco_style_ref'].') : '.$osnLine[0].' : '.$osnLine[7]);
										} else {
											//addTicket(14,$orderDetail['organization_id'],'Welcome Pack Order Update','Sapphire status has been updated to : '.$despatchLine[0]);
											audit(35,$orderDetail['organization_id'],$osnLine[1].' ('.$orderDetail['tesco_style_ref'].') : '.$osnLine[0].' : '.$osnLine[7]);
										}

										/*
										if(stristr($osnLine[1],'SAMP')){
											//addTicket(14,$orderDetail['organization_id'],'Sample Order Update','Sapphire status has been updated to : '.$despatchLine[0]);
											audit(34,$orderDetail['organization_id'],$osnLine[1].' ('.$orderDetail['tesco_style_ref'].') : '.$osnLine[0]);
										} else {
											//addTicket(14,$orderDetail['organization_id'],'Welcome Pack Order Update','Sapphire status has been updated to : '.$despatchLine[0]);
											audit(35,$orderDetail['organization_id'],$osnLine[1].' ('.$orderDetail['tesco_style_ref'].') : '.$osnLine[0]);
										}
										*/

									}
								}
							}

							// upload it to VS FTP.
							if(ftp_put($vs_conn_id,$vstockStatusFolder.'/'.$filenameonly,'edi/'.$filenameonly,FTP_BINARY)){
								rename('edi/'.$filenameonly,'edi/processed/'.$filenameonly);
							}
						}

					} else {
						// couldnt get it?
					}
				}
			}
		}

		return '';

	}

	public function processbackfeed(){

		$backfeedLineQuery = dbq("select * from backfeed");

		while($backfeedLine = dbf($backfeedLineQuery)){
			// update.

			if(!empty($backfeedLine['parent_cat_id'])){
				dbq("UPDATE product_colours set product_colour_gmo_ref = :PARENTCATID where product_colour_style_ref = :STYLEREF",
					[
						':PARENTCATID' => $backfeedLine['parent_cat_id'],
						':STYLEREF' => $backfeedLine['style_ref']
					]
				);
			} else {
				// Need to deactivate it?
			}

			if(!empty($backfeedLine['cat_id'])){
				dbq("UPDATE products_sizes_to_products_colours set product_variation_gmo_sku = :CATID where product_variation_sku = :EAN",
					[
						':CATID' => $backfeedLine['cat_id'],
						':EAN' => $backfeedLine['gtin']
					]
				);
			} else {
				// need to deactivate it?
			}

			// remove from backfeed.
			dbq("delete from backfeed where backfeed_id = :BACKFEEDID",
				[
					':BACKFEEDID' => $backfeedLine['backfeed_id']
				]
			);

		}
		return;
	}

	public function updatePrices(){

	}

	public function updateffimg(){
		// update default logos on FF website.
		ini_set('max_execution_time', 3000);
		ini_set('display_errors','On');

		$uploadDefaultLogoQuery = dbq("select *, o.organization_id from organizations o join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id) where o.organization_default_logo_update = 1");

		while($uploadDefaultLogo = dbf($uploadDefaultLogoQuery)){
			//Image please.
			echo $uploadDefaultLogo['logo_url'];
			$filename = $uploadDefaultLogo['school_URN'];

			$image = file_get_contents('http:' . $uploadDefaultLogo['logo_url']);
			$im2 = new Imagick();
			$im2->setResolution(300,300);
			//$im2->setImageUnits(1);

			$im2->readImageBlob($image);
			$im2->scaleImage(316, 316, true);

			$im1 = new Imagick();
			$im1->setResolution(300,300);

			//$im1->setImageResolution(300,300);
			$im1->newImage(336, 336, new ImagickPixel('rgb(' . $uploadDefaultLogo['primary_background_colour'] . ')'));
			$im1->setImageUnits(1);
			$im1->setImageColorspace($im2->getImageColorspace());
			$im1->setImageMatte(true);

			$x = floor((336 - $im2->getImageWidth()) / 2);
			$y = floor((336 - $im2->getImageHeight()) / 2);

			$im1->compositeImage($im2, $im2->getImageCompose(), $x, $y);
			$im1->stripimage();
			// Save it.
			//$im1->setImageFormat("jpeg");
			$im1->scaleImage(75, 75, true);
			// Save locally.
			$im1->setImageResolution(300,300);
			try {
				$newname = $filename . '_xs.jpg';
				$localfile = FS_ROOT.'api/v1.0/ffimages/' .$newname;

				$im1->writeImage($localfile);
				// add to FTP queue.
				dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
					[
						':LOCALFILE' => $localfile,
						':REMOTEFILE' => $newname,
						':HOSTPREFIX' => 'TESCO_'
					]
				);

				dbq("update organizations set organization_default_logo_update = 0 where organization_id = :ORGID",
					[
						':ORGID' => $uploadDefaultLogo['organization_id'],
					]
				);
			} catch (Exception $e) {
				echo "<pre>".print_r($e,1)."</pre>";
			}

		}

	}

	public function updatetrackerstats(){

		$url="http://www.slick-tools.com/sapphiredata/api/v1.0/pick/getOrderCounts";

		$postdata = http_build_query(
			[
				't' => 3
			]
		);

		$opts = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata
			)
		);

		$context = stream_context_create($opts);

		$newData = json_decode(file_get_contents($url, false, $context));

		return (array)$newData;
	}

	public function updatealltrackerstats(){

		$trackerQuery = dbq("select * from demand_data where demand_week < :TODAY order by demand_week asc",
			[
				':TODAY' => date("Y-m-d")
			]
		);

		$url="http://www.slick-tools.com/sapphiredata/api/v1.0/pick/getOrderCounts";

		$dateResults = [];
		while($trackerRes = dbf($trackerQuery)){
			// get the details.

			$startofWeek = $trackerRes['demand_week'];
			$startofWeekTime = strtotime($trackerRes['demand_week']);
			$startofnextWeek = strtotime('monday next week',$startofWeekTime);

			for($dow=$startofWeekTime;$dow < $startofnextWeek;$dow+=(60*60*24)){

				$postdata = http_build_query(
					[
						't' => 3,
						'd' => date("Y-m-d", $dow)
					]
				);

				$opts = array('http' =>
					array(
						'method'  => 'POST',
						'header'  => 'Content-type: application/x-www-form-urlencoded',
						'content' => $postdata
					)
				);

				$context = stream_context_create($opts);

				$newData = json_decode(file_get_contents($url, false, $context));

				$newData = (array)$newData;

				if(!isset($dateResults[$startofWeek])){
					foreach($newData['orderStatuses'] as $dateKey => $values){
						$dateResults[$startofWeek] = (array)$values;
					}
				} else {
					foreach($newData['orderStatuses'] as $dateKey => $values){
						//$values = (array)$values;
						foreach($values as $key => $value){
							$dateResults[$startofWeek][$key] += $value;
						}
					}
				}
			}

			// add up?

			dbq("update demand_data set demand_ty_demand_actual = :DEMAND, demand_ty_output_actual = :OUTPUT where demand_data_id = :DDID",
				[
					'DEMAND' => $dateResults[$startofWeek]['Confirmed'],
					'OUTPUT' => $dateResults[$startofWeek]['Dispatched'],
					'DDID' => $trackerRes['demand_data_id'],
				]
			);

		}


		return (array)$dateResults;
	}

	private function cronlog($method,$line,$data = []){
		dbq("insert into cron_log (
					cron_method,
					cron_line,
					cron_date,
					cron_log_data
			) VALUES (
				  	:CRONMETHOD,
					:CRONLINE,
					NOW(),
					:CRONLOGDATA
			)",
			[
				':CRONMETHOD' => $method,
				':CRONLINE' => $line,
				':CRONLOGDATA' => json_encode((array)$data)
			]
		);
	}

    public function addlogofile()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        print_r("adfadsfads");

		$this->cronlog(__METHOD__,__LINE__,'Start');

		//load threads first for checking.
		$threadArray = [];
		$threadQuery = dbq("select * from threads");
		while($thread = dbf($threadQuery)){
			$threadArray[] = trim($thread['colour_code']);
		}

		$destination = './unapproved_logo_files/';

        $selectQuery = dbq("SELECT
								*
							FROM
								organization_logos
							WHERE
								logo_digitized = 0
							AND
								ebos_digitizing_id =102419
							AND
								complete_order = 0
							AND
								logo_deleted = 0
							");

        if(dbnr($selectQuery) > 0)
        {
            include_once(DIR_CLASSES . 'eBOS_API.php');
            $eBOS_API = new eBOS_API(APIUser, APIPass); // TODO make admin.

			$this->cronlog(__METHOD__,__LINE__,$eBOS_API);

            while($fetchQuery = dbf($selectQuery))
            {

				echo "checking eBOS order ".$fetchQuery['ebos_digitizing_id'];

				$this->cronlog(__METHOD__,__LINE__,"checking eBOS order ".$fetchQuery['ebos_digitizing_id']);

                $orgDetail = getOrgDetails($fetchQuery['organization_id']);
                $masterOrgDetail =  getOrgDetails(getMasterOrgID($fetchQuery['organization_id']));

                if(!file_exists($destination.$orgDetail['school_URN'])){
                    mkdir($destination.$orgDetail['school_URN']);
                    chmod($destination.$orgDetail['school_URN'],0777); // allow us to write to it from ftp.
                }

                $digitizingOrderDetails = $eBOS_API->getDigitizingOrderDetails($fetchQuery['ebos_digitizing_id']);

				$this->cronlog(__METHOD__,__LINE__,$digitizingOrderDetails);

				if(isset($digitizingOrderDetails->order)){
					// Is it complete?
					echo " - found - ".$digitizingOrderDetails->order->status;
					if($digitizingOrderDetails->order->status == 'completed' || $digitizingOrderDetails->order->status == 'sample_order'){

						$oldDesigns = explode(',',$fetchQuery['ebos_design_id']);

						foreach($digitizingOrderDetails->order->designs as $orderDesign){
							if(!in_array($orderDesign->id,$oldDesigns)){
								// new one we havent seen before.

								file_put_contents($destination.$masterOrgDetail['school_URN'].'/'.$fetchQuery['tesco_style_ref'].'.dst',file_get_contents($orderDesign->dst->url));
                                echo " - destination - ".$destination.$masterOrgDetail['school_URN'].'/'.$fetchQuery['tesco_style_ref'] . " dropping file:" .$orderDesign->dst->url . "\n";
								file_put_contents($destination.$masterOrgDetail['school_URN'].'/'.$fetchQuery['tesco_style_ref'].'.pdf',file_get_contents($orderDesign->pdf->url));
                                echo " - destination - ".$destination.$masterOrgDetail['school_URN'].'/'.$fetchQuery['tesco_style_ref'] . " dropping file:" .$orderDesign->pdf->url . "\n";
								file_put_contents($destination.$masterOrgDetail['school_URN'].'/'.$fetchQuery['tesco_style_ref'].'.emb',file_get_contents($orderDesign->emb->url));
                                echo " - destination - ".$destination.$masterOrgDetail['school_URN'].'/'.$fetchQuery['tesco_style_ref'] . " dropping file:" .$orderDesign->emb->url . "\n";

								// Add newest design ID to datebase.

								$embColours = $eBOS_API->getThreadsFromEMB($destination.$orgDetail['school_URN'].'/'.$fetchQuery['tesco_style_ref'].'.emb');
								$this->cronlog(__METHOD__,__LINE__,$embColours);

								// TODO check that all threads are madeira.  If not reject here.

								$allMadeira = true;

								foreach($embColours as $embColour){
									if(!in_array($embColour,$threadArray)){
										$allMadeira = false;
									}
								}

								if($allMadeira){

									if(!isset($fetchQuery['ebos_id']) || empty($fetchQuery['ebos_id']))
									{
										$designs = $eBOS_API->getDesigns($fetchQuery['tesco_style_ref']);
										$this->cronlog(__METHOD__,__LINE__,$designs);

										$ebos_id = null;

										if (!empty($designs)) {

											foreach ($designs->designs as $design) {
												if($design->number == $fetchQuery['tesco_style_ref']){
													$ebos_id = $design->id ;
												}
											}
										}

										if(is_null($ebos_id)){
											// doesnt exist.  create one for it.
											$designdetail = [
												'design' => [
													'number' => $fetchQuery['tesco_style_ref'].'-TMP',
													'name' => $fetchQuery['school_name'],  // random name for testing
													'notes' => 'Created by F&F UES',
													'threads' => [
														'1000'
													],
													'dst' => [
														'data' => base64_encode(file_get_contents('dst/tempdesign.dst')),
														'filename' => 'tempdesign.dst'
													]
												]
											];

											$newDesign = $eBOS_API->createDesign($designdetail);  // Works.
											$this->cronlog(__METHOD__,__LINE__,$newDesign);

											if (isset($newDesign->successful) && $newDesign->successful == false) {
												$data['type'] = 401;

												$data['messages'][] = [
													'type' => 'danger',
													'message' => $newDesign->message
												];
												return $data;
											}

											$ebos_id = $newDesign->design->id ;
											$updateQuery = dbq('UPDATE organization_logos SET ebos_id = :EBOSID, logo_on_ebos = 1 WHERE organization_logo_id = :OLID',
												[
													':EBOSID' => $ebos_id,
													':OLID' => $fetchQuery['organization_logo_id']
												]
											);
										}
									}
									else
									{
										$ebos_id = $fetchQuery['ebos_id'];
									}

									$newDesign = $eBOS_API->getDesignDetails($ebos_id);
									$this->cronlog(__METHOD__,__LINE__,$newDesign);

									$designChanges = [
										'design' => [
											'threads' => $embColours,
											'number' => $newDesign->design->number,
											'name' => $newDesign->design->name,
											'notes' => $newDesign->design->notes."\n\n".'Uploaded digitized design from F&F UES. - '.date('r').' - '.$_SESSION['session']['user']['user_first_name'].' '.$_SESSION['session']['user']['user_last_name'],
											'dst' => [
												'data' => base64_encode(file_get_contents($destination.$masterOrgDetail['school_URN'].'/'.$fetchQuery['tesco_style_ref'].'.dst')),
												'filename' => $fetchQuery['tesco_style_ref'].'.dst'
											]
										]
									];

									$designUpdate = $eBOS_API->updateDesign($ebos_id,$designChanges);
									$this->cronlog(__METHOD__,__LINE__,$designUpdate);

									if(isset($designUpdate->successful) && $designUpdate->successful == true){
										$updateQuery = dbq("update organization_logos
													set
															needs_ebos_update = 1,
															logo_digitized = 1,
															update_customer = 0,
															primary_background_colour = :PRIMBACK,
                                           					ebos_design_id = IF(ebos_design_id IS NULL, :EBOS_DESIGN_ID , CONCAT(ebos_design_id, ',',:EBOS_DESIGN_ID)),
                                           					complete_order = 1,
                                           					alert_user = 0

                                    WHERE organization_logo_id = :OLID",
											[
												':PRIMBACK' => $fetchQuery['primary_background_colour'],
												':EBOS_DESIGN_ID' => $orderDesign->id,
												':OLID' => $fetchQuery['organization_logo_id']
											]
										);


										// SENT EMAIL
										$emailContent = 'Logo Number '. $fetchQuery['tesco_style_ref'].' for '.$fetchQuery['school_name'] . ' has been updated with eBOS Digitization order '.$digitizingOrderDetails->order->ebos_order_number.' design files. Please review';

										$subject = 'Review design';
										$title = $subject;
										$template = 'tesco-ff-uniforms';
										$tag = ['ff-design-review'];

										$emailAddress = [];

										foreach($digitizingOrderDetails->order->notification_email_addresses as $email){

											$emailAddress[$email] = $email;
										}

										$emailAddress['it@slickstitch.com'] = 'it@slickstitch.com';

										$message = $emailContent;

										$template_content = array(
											[
												'name' => 'emailtitle',
												'content' => $title
											],
											[
												'name' => 'main',
												'content' => $message
											]
										);



										if($fetchQuery['alert_user'] != 0){
											// TODO add an alert.
											addTicket(3,$fetchQuery['organization_id'],'Change Logo '.$fetchQuery['tesco_style_ref'].' Colours',$fetchQuery['alert_comment'],true,date('Y-m-d G:i:s',(time()+60*5)),$fetchQuery['alert_user'],3);
											$ticketID = dbid();
											dbq("UPDATE organization_logos set alert_ticket_id = :ALERTID where organization_logo_id = :OLID",
												[
													':ALERTID' => $ticketID,
													':OLID' => $fetchQuery['organization_logo_id']
												]
											);
										} else {
											addTicket(3, $masterOrgDetail['organization_id'], 'Emblem Updated.', $emailContent, true, date('Y-m-d G:i:s',(time()+60*5)), null);
											//return 'sucessfully mail sent';
										}

										send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);

										// add attachment links to

									} else {
										//	return [(array)$digitizingOrderDetails,(array)$designUpdate];
									}

								} else {
									// reject design ask for only madeira colours.
									$updateDigitizingOrderDetails = $eBOS_API->rejectDigitizingOrder($fetchQuery['ebos_digitizing_id'],'Design contains non Madeira colours.  Please use only Madeira colours.');
									$this->cronlog(__METHOD__,__LINE__,$updateDigitizingOrderDetails);

									if(isset($updateDigitizingOrderDetails->successful) && $updateDigitizingOrderDetails->successful == true){
										audit(29,$fetchQuery['organization_id']);
										addTicket(2,$fetchQuery['organization_id'],'Logo Auto Rejected : '.$fetchQuery['tesco_style_ref'],'Design contains non Madeira colours.  Please use only Madeira colours.'); // set an alert for all golley slater users.
									}
								}
							}
						}

						// pROCESS AS IF UPLOADED.

					} else {
						if($digitizingOrderDetails->order->status == 'cancelled'){
							// Put an alert in as its cancelled?
							addTicket(3, $masterOrgDetail['organization_id'], 'eBOS order Cancelled!', 'The eBOS digitization order for '.$fetchQuery['tesco_style_ref'].' was cancelled.  Please upload files manually.', true, date('Y-m-d G:i:s',(time())), null, 3);
							echo 'eBOS order Cancelled!', 'The eBOS digitization order for '.$fetchQuery['tesco_style_ref'].' was cancelled.  Please upload files manually.'."\n";

							$updateQuery = dbq("update organization_logos
													set
															logo_digitized = 0,
															update_customer = 0,
                                           					complete_order = 1,
                                           					alert_user = 0

                                    WHERE organization_logo_id = :OLID",
								[
									':OLID' => $fetchQuery['organization_logo_id']
								]
							);

						}
						// order not complete
						//return (array)$digitizingOrderDetails;
					}


				} else {
					//return (array)$digitizingOrderDetails;
				}

				echo "\n";

            }
        }
		$this->cronlog(__METHOD__,__LINE__,'End');
    }

    public function createwelcomeorders()
    {

		die; // TODO remove when new welcome packs come in.

        // send out todays welcome packs.
        $welcomePackQuery = dbq("SELECT
									  o.organization_id,
                                      a.`entry_company`,
                                      a.`entry_street_address`,
                                      a.`entry_street_address_2`,
                                      a.`entry_city`,
                                      a.`entry_state`,
                                      a.`entry_postcode`,
                                      o.`organization_welcome_pack`,
                                      o.`school_URN`
                                    FROM
                                      organizations o
                                      JOIN addresses a
                                        ON a.`address_id` = o.`organization_default_address_id`
                                    WHERE
                                    o.organization_welcome_pack > 0
                                    AND
                                    o.`organization_live_date` IS NOT NULL"
            );

        while($welcomePack = dbf($welcomePackQuery))
        {
            // send welcome pack.
            if($welcomePack['organization_welcome_pack'] <> '' || $welcomePack['organization_welcome_pack'] <> NULL)
            {
                $qtypack1 = '';
                $qtypack2 = '';
                $i = 0;
                $a = $welcomePack['organization_welcome_pack'];
                do {
                    $i++;
                    if (($a % 2) == 0) {
                        $remainder = $a % 2;
                        $divident = ($a - $remainder) / 2;
                        $qtypack2 = $divident;
                        $a = $remainder;
                    } else if ($a == 1) {
                        $qtypack1 = $a;
                    } else {
                        $a = $a - 1;
                        $qtypack1 = 1;
                    }
                } while ($a > 1);

                $totalItem = $qtypack1 + $qtypack2;
                $line = $i;


                $orderid = 'WELC'.shorten(time()).$welcomePack['school_URN'];

                if ($qtypack1 != '') {
                    $quantity = $qtypack1;

                    $insertQuery = dbq("INSERT INTO ss_orders
                                        ( orderId,
                                          deliveryContact,
                                          deliveryAddress,
                                          deliveryTown,
                                          deliveryCounty,
                                          deliveryPostCode,                                         
                                          NumberofLines,
                                          NumberofItems,
										  LogoID1,
										  LogoPosition1,
                                          Date, 
                                          SKU,                                        
                                          Qty,
                                          LineSent
                                      ) VALUES (
                                          :ORDER_ID,
                                          :NAME,
                                          :ADDRESS,
                                          :TOWN,
                                          :COUNTY,
                                          :POSTCODE,                                          
                                          :LINE,
                                          :ITEM,
                                          'NO LOGO',
                                          'NO LOGO',
                                          NOW(),
                                          :SKU,
                                          :QUNTITY,
                                          '0'
                                        ) ",
                        [
                            ':ORDER_ID' => $orderid,
                            ':NAME' => $welcomePack['entry_company'],
                            ':ADDRESS' => $welcomePack['entry_street_address'] . ',' . $welcomePack['entry_street_address_2'],
                            ':TOWN' => $welcomePack['entry_city'],
                            ':COUNTY' => $welcomePack['entry_state'],
                            ':POSTCODE' => $welcomePack['entry_postcode'],
                            ':QUNTITY' => $quantity,
                            ':LINE' => $line,
                            ':SKU' => 'WELC200',
                            ':ITEM' => $totalItem
                        ]);

//                    $value = "";
//                    $value .= 'orderId,deliveryContact,deliveryAddress,deliveryTown,deliveryCounty,deliveryPostCode,deliveryPhone,deliveryEmail,NumberofLines,NumberofItems,Date,Notes,lineNo,SKU,Qty,LogoID1,LogoPosition1,LogoThread1,LogoID2,LogoPosition2,LogoThread2';
//
//                    $value .= "\n";
                }
                if ($qtypack2 != '')
                {
                    $quantity = $qtypack2;

                    $insertQuery = dbq("INSERT INTO ss_orders
                                        ( orderId,
                                          deliveryContact,
                                          deliveryAddress,
                                          deliveryTown,
                                          deliveryCounty,
                                          deliveryPostCode,                                         
                                          NumberofLines,
                                          NumberofItems,
										  LogoID1,
										  LogoPosition1,
                                          Date, 
                                          SKU,                                        
                                          Qty,
                                          LineSent )
                                  VALUES(
                                          :ORDER_ID,
                                          :NAME,
                                          :ADDRESS,
                                          :TOWN,
                                          :COUNTY,
                                          :POSTCODE,                                          
                                          :LINE,
                                          :ITEM,
                                          'NO LOGO',
                                          'NO LOGO',
                                          NOW(),
                                          :SKU,
                                          :QUNTITY,
                                          '0'
                                        ) ",
                        [
                            ':ORDER_ID' => $orderid,
                            ':NAME' => $welcomePack['entry_company'],
                            ':ADDRESS' => $welcomePack['entry_street_address'] . ',' . $welcomePack['entry_street_address_2'],
                            ':TOWN' => $welcomePack['entry_city'],
                            ':COUNTY' => $welcomePack['entry_state'],
                            ':POSTCODE' => $welcomePack['entry_postcode'],
                            ':QUNTITY' => $quantity,
                            ':LINE' => $line,
                            ':SKU' => 'WELC400',
                            ':ITEM' => $totalItem
                        ]);

                }
				// reset to 0.
				dbq("update organizations o set o.organization_welcome_pack = 0 where o.organization_id = :ORGID",
					[
						':ORGID' => $welcomePack['organization_id']
					]
				);

				// Add audit.
				//addTicket(10, $this->request['sample_request']['organization_id'], 'Sample Request.', $ticketmessage);
				audit(36,$welcomePack['organization_id'],$orderid);
            }
		}
    }

    public function create_edi_orders()
    {
        $lineQuery = dbq("SELECT orderId FROM `ss_orders` WHERE `LineSent` = '0' GROUP BY orderId");
		if(dbnr($lineQuery) > 0){
			$ss_conn_id = ftp_connect(IMS_FTP_HOST);
			if($ss_conn_id) {
				$ss_login_result = ftp_login($ss_conn_id, IMS_FTP_USER, IMS_FTP_PASS);

				if ($ss_login_result) {
					ftp_pasv($ss_conn_id, true);

					//$List = ftp_nlist($ss_conn_id,'/');
					//return $List;
				} else {
					return 'NO LOGIN SS FTP';
				}
			} else {

			}

			while($fetchLine = dbf($lineQuery))
			{
				$destination = './upload/';
				if(is_dir($destination) == false)
				{
					mkdir($destination);
				}
				$filename = $fetchLine['orderId'] . '.csv';

				if (is_dir($destination) == false) {
					mkdir($destination);
				}

				if (!$fp = fopen($destination . $filename, 'w+')) {
					return false;
				}

				$value = "";
				$value .= 'orderId,deliveryContact,deliveryAddress,deliveryTown,deliveryCounty,deliveryPostCode,deliveryPhone,deliveryEmail,NumberofLines,NumberofItems,Date,Notes,lineNo,SKU,Qty,LogoID1,LogoPosition1,LogoThread1,LogoID2,LogoPosition2,LogoThread2';

				$orderIdQuery = dbq("SELECT * FROM ss_orders WHERE orderId = :ORDERID AND `LineSent` = '0' ",
					[
						':ORDERID' => $fetchLine['orderId']
					]);
				while($fetchOrderId = dbf($orderIdQuery))
				{
					$updateLine = dbq("UPDATE ss_orders SET LineSent = '1' WHERE unique_id = :UNIQUE_ID",
						[
							':UNIQUE_ID' => $fetchOrderId['unique_id']
						]);
					$fetchOrderId = str_replace(',' , ' ' , $fetchOrderId);

					$value .= "\n";
					$value .= $fetchOrderId['orderId'] . ',' . $fetchOrderId['deliveryContact']. ',' . $fetchOrderId['deliveryAddress']  . ',' . $fetchOrderId['deliveryTown']. ',' . $fetchOrderId['deliveryCounty']. ',' . $fetchOrderId['deliveryPostCode']  . ',' . $fetchOrderId['deliveryPhone']. ',' . $fetchOrderId['deliveryEmail'] . ',' . $fetchOrderId['NumberofLines']  . ',' . $fetchOrderId['NumberofItems']. ',' . $fetchOrderId['Date']. ',' . $fetchOrderId['Notes'] . ',' . $fetchOrderId['unique_id']  . ',' . $fetchOrderId['SKU']. ',' . $fetchOrderId['Qty'] . ',' . $fetchOrderId['LogoID1']  . ',' . $fetchOrderId['LogoPosition1']. ',' . $fetchOrderId['LogoThread1'] . ',' . '' . ',' . ''. ',' . '' ;
				}
				$value .= "\n";
				fputs($fp, $value);
				fclose($fp);

				if(ftp_put($ss_conn_id,'/Tesco/DA/'.$filename,$destination . $filename,FTP_BINARY)){
					rename($destination . $filename,$destination . 'processed/'. $filename);
				}

			}
		}

		return true;
    }

	public function uploadpulsefiles(){

		$pulseUploadQuery = dbq("select * from pulse_upload pu join organization_logos ol on ol.tesco_style_ref = pu.pulse_upload_logo");

		include_once(DIR_CLASSES . 'eBOS_API.php');
		$eBOS_API = new eBOS_API(APIUser, APIPass);

		while($pulseUpload = dbf($pulseUploadQuery)){

			$bgColour = $pulseUpload['primary_background_colour'];

			$size = explode('x',$pulseUpload['size__mm_']);
			// upload logos to Pulse

			$LUcsv = 'Design Number,Name,Type,Width,Height,Stitch Count,Colours,Last modified,Threads,Notes'."\n";

			$LUcsv .= '"'.$pulseUpload['tesco_style_ref'].'",';
			$LUcsv .= '"'.$pulseUpload['school_name'].'",';
			$LUcsv .= '"Embroidery",';
			$LUcsv .= '"'.$size[0].'",';
			$LUcsv .= '"'.$size[1].'",';
			$LUcsv .= '"'.$pulseUpload['stitch_count'].'",';
			$LUcsv .= '"'.count(explode(',',$pulseUpload['thread_array'])).'",';
			$LUcsv .= '"'.date("c",$pulseUpload['logo_last_updated']).'",';
			$LUcsv .= '"'.$pulseUpload['thread_array'].'",';
			$LUcsv .= '""';

			$LUFileName = 'SampleLogos/'.$pulseUpload['tesco_style_ref'].'_LU.csv';

			file_put_contents($LUFileName,$LUcsv);

			$remotefile = '/Tesco/SampleLogos/'.$pulseUpload['tesco_style_ref'].'_LU.csv';
			$localfile = API_ROOT . $LUFileName;

			dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
				[
					':LOCALFILE' => $localfile,
					':REMOTEFILE' => $remotefile,
					':HOSTPREFIX' => ''
				]
			);

			$oldDesign = $eBOS_API->getDesignDetails($pulseUpload['ebos_id']);

			$DSTFileName = 'SampleLogos/'.$pulseUpload['tesco_style_ref'].'.dst';

			file_put_contents($DSTFileName,file_get_contents($oldDesign->design->dst->url));

			$remotefile = '/Tesco/SampleLogos/'.$pulseUpload['tesco_style_ref'].'.dst';
			$localfile = API_ROOT . $DSTFileName;

			dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
				[
					':LOCALFILE' => $localfile,
					':REMOTEFILE' => $remotefile,
					':HOSTPREFIX' => ''
				]
			);

			dbq("delete from pulse_upload where pulse_upload_id = :UPLOADID",
				[
					':UPLOADID' => $pulseUpload['pulse_upload_id']
				]
			);

		}

	}

	// Check if any logos have been updated on ebos directly.  Move files into Tesco Approved.
	public function checkebosupdates(){
		include(DIR_CLASSES . 'eBOS_API.php');

		$logoQuery = dbq("select * from organization_logos where logo_last_updated >= :MODTIME",
			[
				':MODTIME' => EBOS_LAST_MODIFIED - (60 * 5) // Allow 5 mins diff in server time
			]
		);

		$foundLogos = [];
		while($logos = dbf($logoQuery)){
			$foundLogos[$logos['tesco_style_ref']] = $logos['logo_last_updated'];
		}

		$eBOS_API = new eBOS_API(APIUser, APIPass);
		$designs = $eBOS_API->getDesigns('',1,1000,'true',date('Y-m-d G:i:s',EBOS_LAST_MODIFIED));

		$returnstrings = [];
		$latestDate = EBOS_LAST_MODIFIED;

		if(isset($designs->designs)){
			foreach($designs->designs as $design){

				$updateTime = strtotime($design->updated_at);
				$updateFF = false;
				if(isset($foundLogos[$design->number])){
					//$returnstrings[] = $design->number . ' - ' . $updateTime . ' > ' . $foundLogos[$design->number]. '?';
					if($updateTime > $foundLogos[$design->number]){
						// Needs an ebos update on FF-UES
						$updateFF = true;
					} else {
						// Probably is this that updated it.
					}
				} else {
					// needs an update from ebos.
					$updateFF = true;
					//$returnstrings[] = $design->number . ' - ' . $updateTime;
				}

				if($updateFF){
					if(stristr($design->number,'TMP')){
						// Not approved yet. nothing to do.
					} else {
						// Approved, Need to update files in Tesco Approved folder if approved.
						$designDetail = $eBOS_API->getDesignDetails($design->id);//'2017-01-19 00:00:00');

						$school_URN = @reset(explode('-',$design->number));

						$destination = './unapproved_logo_files/';

						if(!file_exists($destination.$school_URN)){
							mkdir($destination.$school_URN);
							chmod($destination.$school_URN,0777); // allow us to write to it from ftp.
						}

						if(file_put_contents($destination.$school_URN.'/'.$design->number.'.dst',file_get_contents($designDetail->design->dst->url))){
							// upload to Tesco/Approved Logos

							$remotefile = '/Tesco/Approved Schools/' . trim($school_URN) . '/' . $design->number.'.dst';
							$localfile = API_ROOT . 'unapproved_logo_files/' . trim($school_URN) . '/' . $design->number.'.dst';
							// error, add to queue to do another time
							dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
								[
									':LOCALFILE' => $localfile,
									':REMOTEFILE' => $remotefile,
									':HOSTPREFIX' => ''
								]
							);
							$returnstrings[] = $remotefile;

						}
					}

					dbq("update organization_logos set needs_ebos_update = 1 where tesco_style_ref = :STYLEREF",
						[
							':STYLEREF' => $design->number
						]
					);
				}

				if($latestDate < $updateTime){
					$latestDate = $updateTime;
				}
			}
		}

		$returnstrings[] = date('Y-m-d G:i:s',$latestDate);

		// update database.
		dbq("update configuration set config_value = :VALUE where config_key = :KEY",
			[
				':VALUE' => $latestDate,
				':KEY' => 'EBOS_LAST_MODIFIED'
			]
		);

		// check last updated against local?
		return $returnstrings;
	}

	public function addraf(){

		$fromDate = date("Y-m-d",strtotime('yesterday')).' 00:00:00';
		//$fromDate = "2017-03-04".' 00:00:00';
		$toDate = date("Y-m-d",strtotime('today')).' 00:00:00';

		$RAFQuery = dbq("SELECT o.organization_id, o.school_URN, o.organization_parent_id, o.organization_live_date, ro.organization_id as roid, ro.school_URN as rourn, ro.organization_parent_id as ropid, ro.organization_live_date as rold FROM organizations o JOIN organizations ro ON (ro.`organization_id` = o.`organization_raf_organization_id`) WHERE o.`organization_live_date` > :FROMDATE AND o.`organization_live_date` < :TODATE AND o.`organization_parent_id` IS NULL ",
			[
				':FROMDATE' => $fromDate,
				':TODATE' => $toDate
			]
		);

		$donations = [];
		while($RAF = dbf($RAFQuery)){
			// insert a donation if not a house that has gone live.

			if(is_null($RAF['organization_parent_id'])){

				$livetime = strtotime($RAF['organization_live_date']);
				//live school
				$orgdetail = getOrgDetails($RAF['organization_id']);

				dbq("insert into donations (
						organization_id,
						organization_urn,
						donation_name,
						donation_accounted_for,
						donation_amount,
						donation_new_balance,
						donation_category,
						donation_paid,
						donation_created_at,
						donation_display_date,
						donation_description
					)  values (
						:ORGID,
						:ORGURN,
						'tesco_donation',
						:ACCOUNTED,
						:AMOUNT,
						:BALANCE,
						5,
						0,
						:DATE,
						:DATEB,
						:DESC
					)",
					[
						':ORGID' => $RAF['organization_id'],
						':ORGURN' => $RAF['school_URN'],
						':ACCOUNTED' => date('Y-m-d H:i:s',strtotime('last day of this month',$livetime)),
						':AMOUNT' => '50',
						':BALANCE' => $orgdetail['organization_funds_balance'] + 50,
						':DATE' => $RAF['organization_live_date'],
						':DATEB' => $RAF['organization_live_date'],
						':DESC' => 'Extra donation during '.date("F Y",$livetime).' (RAF Bonus)'
					]
				);

				// update organization balance.
				dbq("update organizations set organization_funds_balance = :BALANCE, organization_funds = organization_funds + :THISDONATION where organization_id = :ORGID",
					[
						':BALANCE' => $orgdetail['organization_funds_balance'] + 50,
						':THISDONATION' => $orgdetail['organization_funds_balance'],
						':ORGID' => $RAF['organization_id']
					]
				);

				// Refering school
				$rorgdetail = getOrgDetails($RAF['roid']);

				dbq("insert into donations (
						organization_id,
						organization_urn,
						donation_name,
						donation_accounted_for,
						donation_amount,
						donation_new_balance,
						donation_category,
						donation_paid,
						donation_created_at,
						donation_display_date,
						donation_description
					)  values (
						:ORGID,
						:ORGURN,
						'tesco_donation',
						:ACCOUNTED,
						:AMOUNT,
						:BALANCE,
						5,
						0,
						:DATE,
						:DATEB,
						:DESC
					)",
					[
						':ORGID' => $RAF['roid'],
						':ORGURN' => $RAF['rourn'],
						':ACCOUNTED' => date('Y-m-d H:i:s',strtotime('last day of this month',$livetime)),
						':AMOUNT' => '50',
						':BALANCE' => $rorgdetail['organization_funds_balance'] + 50,
						':DATE' => $RAF['organization_live_date'],
						':DATEB' => $RAF['organization_live_date'],
						':DESC' => 'Extra donation during '.date("F Y",$livetime).' (RAF Bonus)'
					]
				);

				// update organization balance.
				dbq("update organizations set organization_funds_balance = :BALANCE, organization_funds = organization_funds + :THISDONATION where organization_id = :ORGID",
					[
						':BALANCE' => $rorgdetail['organization_funds_balance'] + 50,
						':THISDONATION' => $rorgdetail['organization_funds_balance'],
						':ORGID' => $RAF['roid']
					]
				);
			}

		}

		return $donations;

	}

	public function exportstyleeans(){

		$styleEANQuery = dbq("select product_colour_style_ref, product_variation_sku, product_variation_stock from product_colours pc left join products_sizes_to_products_colours psc on psc.product_colour_id = pc.product_colour_id where pc.product_colour_active = 1 ORDER BY pc.`product_colour_style_ref`,psc.`product_variation_sku`");

		$stylelist = [];

		while($styleEAN = dbf($styleEANQuery)){
			$stylecode = trim($styleEAN['product_colour_style_ref']);
			if(!isset($stylelist[$stylecode])){
				$stylelist[$stylecode] = [];
			}

			$stylelist[$stylecode][] = [
				'ean' => trim($styleEAN['product_variation_sku']),
				'qty' => $styleEAN['product_variation_stock']
			];
		}

		//$data = $stylelist;

		$data['data']['contents'] = 'parent_stylecode,parent_cat_id,product_gtin,product_cat_id'.PHP_EOL;
		// make it into a CSV
		foreach($stylelist as $stylecode => $styleDetail){
			foreach($styleDetail as $ean){
				$data['data']['contents'] .= $stylecode.',,'.$ean['ean'].',' . PHP_EOL;
			}

		}

		$data['data']['filename'] = 'SSStyleList-Export-'.date('Y-m-d_G-i-s').'.csv';

		// save locally.
		file_put_contents('reports/'.$data['data']['filename'],$data['data']['contents']);

		dbq("insert into ftp_queue (
                                  ftp_queue_local_file,
                                  ftp_queue_remote_file,
                                  ftp_queue_host_prefix
                              ) VALUES (
                                  :LOCALFILE,
                                  :REMOTEFILE,
                                  :HOSTPREFIX
                              )",
			[
				':LOCALFILE' =>  API_ROOT.'reports/'.$data['data']['filename'],
				':REMOTEFILE' => '/Tesco/Backfeed/OUT/'.$data['data']['filename'],
				':HOSTPREFIX' => ''
			]
		);
	}

	public function importstyleeans(){

		$conn_id = ftp_connect(FTP_HOST);

		if($conn_id){

			$login_result = ftp_login($conn_id, FTP_USER, FTP_PASS);

			if($login_result){
				ftp_pasv($conn_id, true);

				$stockfiles = ftp_nlist($conn_id,'/Tesco/Backfeed/IN/');

				dbq("DELETE from backfeed WHERE 1"); // empty table first.

				foreach($stockfiles as $filename){
					if(stristr($filename,'.csv')){
						// found it.
						//echo 'Found '.$filename;
						if(ftp_get($conn_id,'backfeed/'.basename($filename),$filename,FTP_ASCII)){



							// process it.
							$csvData = file_get_contents('backfeed/'.basename($filename));

							$lines = explode(PHP_EOL, $csvData);

							$headers = [];

							foreach ($lines as $line) {
								$stockline = str_getcsv($line,',');

								if(empty($headers)){
									$headers = array_flip($stockline);
									continue;
								} else {
									dbq('INSERT INTO backfeed (
											style_ref,
											gtin,
											cat_id,
											parent_cat_id)
										VALUE (
											:STYLEREF,
											:GTIN,
											:CATID,
											:PARENTCATID)',
										[
											':STYLEREF' => $stockline[$headers['parent_stylecode']],
											':GTIN' => $stockline[$headers['product_gtin']],
											':CATID' => $stockline[$headers['product_cat_id']],
											':PARENTCATID' => $stockline[$headers['parent_cat_id']]
										]
									);
								}
							}

							ftp_rename($conn_id,$filename,str_replace('/IN/','/IN/processed/',$filename));

						} else {
							//echo 'Error downloading';
						}
					}
				}

			} else {
				// error login in.
				debugMail("Back Feed: Could not log into FTP.");
			}
		}
	}


}