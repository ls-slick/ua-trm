<?php

/**
 * Rest object example
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class products extends restObject
{

    /**
     *
     * @param string $method
     * @param array $request
     * @param string $file
     */
    public function __construct($method, $request = null, $file = null)
    {
        parent::__construct($method, $request, $file);
    }

    public function getproduct()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if(!isset($this->request['pid'])){
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        $colourDetail = [
            'colour_id',
            'colour_name',
            'colour_rgb',
            'product_colour_style_ref',
            'product_colour_image_url',
            'product_colour_id',
        ];
        $productDetail = [
            'product_id',
            'product_name',
            'product_description',
            'product_size_text',
            'product_image',
            'product_url',
            'product_decorated',
            'product_category_id',
            'product_emblem_small',
            'product_new',
            'product_soon',
            'product_awaiting',
            'product_price',
            'product_ref',
            'product_alt_ref'
        ];
        $productQuery = dbq("SELECT * FROM products p
								JOIN product_colours pc using (product_id)
								JOIN colours using (colour_id)
								WHERE p.product_id = :PRODID
								AND p.product_active = 1
								and p.product_test_only = 0",[
                ':PRODID' => $this->request['pid']
            ]
        ); // dont want to see the test products

        $productArray = [];
        while ($product = dbf($productQuery)) {
            $colourParts = [];
            foreach($product as $key => $value){
                if(in_array($key,$productDetail)){
                    $productArray[$key] = $value;
                } if(in_array($key,$colourDetail)){
                    $colourParts[$key] = $value;
                }
            }
            $productArray['colours'][] = $colourParts;
        }

        return $productArray;

    }

    public function product()
    {

        // Title
        $data['product'] = [
            [
                'id' => 0,
                'title' => 'Product Name',
                'colours' => [
                    [
                        'colour_id' => 0,
                        'colour_name' => 'Black',
                        'colour_rgb' => '0,0,0',
                        'style' => 'AO4234523',
                        'image' => 'imageURL',
                        'sizes' => [
                            [
                                'id' => '12',
                                'name' => '2/3',
                                'SKU' => '5054269346565',
                                'price' => '6.00'
                            ],
                            [
                                'id' => '13',
                                'name' => '3/4',
                                'SKU' => '5054269346572',
                                'price' => '6.00'
                            ],
                            [
                                'id' => '14',
                                'name' => '4/5',
                                'SKU' => '5054269346589',
                                'price' => '6.00'
                            ]

                        ]
                    ], [
                        'colour_id' => 1,
                        'colour_name' => 'White',
                        'colour_rgb' => '255,255,255',
                        'style' => 'AO4234554',
                        'image' => 'imageURL',
                        'sizes' => [
                            [
                                'id' => '15',
                                'name' => '2/3',
                                'SKU' => '5054269347104',
                                'price' => '6.00'
                            ],
                            [
                                'id' => '16',
                                'name' => '3/4',
                                'SKU' => '5054269349252',
                                'price' => '6.00'
                            ],
                            [
                                'id' => '17',
                                'name' => '4/5',
                                'SKU' => '5054269349276',
                                'price' => '6.00'
                            ]

                        ]
                    ]
                ]
            ]
        ];

        return $data;
    }


    /* Embroidered */
    public function getusableproducts()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        // update it.
        $productQuery = dbq("SELECT * FROM products
								WHERE product_active = 1
								AND product_decorated = 1
								AND product_test_only = 0"); // dont want to see the test products

        $productArray = [];
        while ($product = dbf($productQuery)) {
            $product['product_image'] = str_replace("/small/","/medium/",$product['product_image']);

            $productArray[$product['product_id']] = $product;
            $productArray[$product['product_id']]['colours'] = [];
            // get colours here?
            $coloursQuery = dbq("select * from product_colours join colours using (colour_id) where product_id = :PRODID and product_colour_active = 1",[
                ':PRODID' => $product['product_id']
            ]);

            while ($colours = dbf($coloursQuery)) {
                //product_colour_image_url
                $colours['product_colour_image_url'] = str_replace("/small/","/medium/",$colours['product_colour_image_url']);
                $productArray[$product['product_id']]['colours'][] = $colours;
            }

        }
        $data['products'] = $productArray;

        return $data;

    }

    /* Embroidered */
    public function getusableplainproducts()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        // update it.
        $productQuery = dbq("SELECT * FROM products
								WHERE product_active = 1
								AND product_decorated = 0
								AND product_test_only = 0"); // dont want to see the test products

        $productArray = [];
        while ($product = dbf($productQuery)) {
            $product['product_image'] = str_replace("/small/","/medium/",$product['product_image']);

            $productArray[$product['product_id']] = $product;
            $productArray[$product['product_id']]['colours'] = [];
            // get colours here?
            $coloursQuery = dbq("select * from product_colours join colours using (colour_id) where product_id = :PRODID and product_colour_active = 1",[
                ':PRODID' => $product['product_id']
            ]);

            while ($colours = dbf($coloursQuery)) {
                //product_colour_image_url
                $colours['product_colour_image_url'] = str_replace("/small/","/medium/",$colours['product_colour_image_url']);
                $productArray[$product['product_id']]['colours'][] = $colours;
            }

        }
        $data['products'] = $productArray;

        return $data;

    }

    public function getproductoptions(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        // Make sure the org is for this user.
        if (!isset($this->request['requestData']) || empty($this->request['requestData'])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'type' => 200, // Not found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics' => [
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        foreach($this->request['requestData']['product'] as $productColourID){
            // get the sizes etc.
            $productColourQuery = dbq("select * from product_colours where product_colour_id = :PCID",
                [
                 ':PCID' => $productColourID
                ]
            );

            while($productColour = dbf($productColourQuery)){
                $data['data'][$productColourID] = $productColour;
                // get available sizes
                $sizeQuery = dbq("select * from products_sizes_to_products_colours ps2pc
                                    JOIN product_sizes ps on ps.product_size_id = ps2pc.product_size_id
                                    where ps2pc.products_sizes_to_colours_id = :PCID",[
                        ':PCID' => $productColourID
                    ]
                );
                $data['data'][$productColourID]['sizes'] = [];
                while($size = dbf($sizeQuery)){
                    $data['data'][$productColourID]['sizes'][] = $size;
                }

            }
        }

        return $data;
    }

    public function ribbon(){

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $getProductsQuery=dbq("select *
                        from products p where product_test_only = 0 and product_active = 1 and product_decorated = 1 order by RAND() limit 15");

        $pane = 0;
        while($getProducts = dbf($getProductsQuery)){
            $data['products'][$pane][$getProducts['product_id']] = $getProducts;

            if(sizeof($data['products'][$pane]) == 5){
                $pane++;
            }
        }

        return $data;
    }

}
