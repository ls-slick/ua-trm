<?php

/**
 * Rest object example
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class slicktools extends restObject
{

    /**
     * user data
     */
    public $id;
    public $name;
    public $lastName;
    public $login;

    /**
     *
     * @param string $method
     * @param array $request
     * @param string $file
     */
    public function __construct($method, $request = null, $file = null)
    {
        parent::__construct($method, $request, $file);
    }

    public function getlogodetail(){

        if (!$this->isMethodCorrect('POST')) {
            return $this->getResponse(500);
        }

        $data = [];

        if (!isset($this->request['logodetail']) || empty($this->request['logodetail'])) {
            $data['type'] = 403;
            $data['messages'][] = [
                'type' => 'danger',
                'message' => 'Cannot provide results, required arguments are missing.'
            ];
            return $data;
        }

        $data = [
            'pick_id' => $this->request['logodetail']['pick_id'],
            'logos' => ''
        ];

        //  $data['requested'] = $this->request;
        foreach($this->request['logodetail']['logos'] as $logodetail){

            // RETURN $logodetail['da_logos_ref'];
            $logoQuery = dbq("SELECT logo_url,logo_svg_url, primary_background_colour, thread_array, stitch_count, size__mm_ from organization_logos where tesco_style_ref = :LOGOREF AND logo_deleted = 0",
                [
                    ':LOGOREF' => $logodetail['da_logos_ref']
                ]
            );

            if(dbnr($logoQuery) > 0){
                $logo = dbf($logoQuery);
                $logo['thread_array'] = explode(',',$logo['thread_array']);
                foreach($logo as $key => $value){
                    $logodetail[$key] = $value;
                }

                $data['logos'][] = $logodetail;
            } else {
                // Probably a deleted logo....

                $logoQuery = dbq("SELECT logo_url,logo_svg_url, primary_background_colour, thread_array, stitch_count, size__mm_ from organization_logos where tesco_style_ref LIKE :LOGOREF order by logo_requested_date DESC",
                    [
                        ':LOGOREF' => $logodetail['da_logos_ref'].'%'
                    ]
                );

                if(dbnr($logoQuery) > 0){
                    $logo = dbf($logoQuery);
                    $logo['thread_array'] = explode(',',$logo['thread_array']);
                    foreach($logo as $key => $value){
                        $logodetail[$key] = $value;
                    }

                    $data['logos'][] = $logodetail;
                } else {

                    $logoQuery = dbq("SELECT logo_url,logo_svg_url, primary_background_colour, thread_array, stitch_count, size__mm_ from organization_logos where tesco_style_ref = :LOGOREF order by logo_requested_date DESC",
                        [
                            ':LOGOREF' => str_replace('-TMP','',$logodetail['da_logos_ref'])
                        ]
                    );

                    if(dbnr($logoQuery) > 0){
                        $logo = dbf($logoQuery);
                        $logo['thread_array'] = explode(',',$logo['thread_array']);
                        foreach($logo as $key => $value){
                            $logodetail[$key] = $value;
                        }

                        $data['logos'][] = $logodetail;
                    }
                }
            }

        }

        return $data;
    }

    function test(){
        $fields = [
            'logodetail' => [
                'logo' => [
                    'da_logos_ref' => 'ABC123-1A'
                ]
            ]
        ];

        $fieldString = http_build_query($fields);
        echo $fieldString;
        die;
    }

}