<?php

/**
 * Rest object example
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class stock extends restObject
{

    /**
     *
     * @param string $method
     * @param array $request
     * @param string $file
     */
    public function __construct($method, $request = null, $file = null)
    {
        parent::__construct($method, $request, $file);
    }

    public function sync()
    {
        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        // todo make this look at NAV
        $productQuery = dbq("SELECT * FROM products_sizes_to_products_colours"); // dont want to see the test products

        $productArray = [
            'count' => dbnr($productQuery),
            'skus' => [

            ]
        ];
        while ($product = dbf($productQuery)) {
            $productArray['skus'][] = [
                'product_sku' => $product['product_variation_sku'],
                'product_free_stock' => $product['product_variation_stock'],
                'product_master_sku' => $product['product_variation_gmo_sku']
            ];
        }

        return $productArray;
    }

}
