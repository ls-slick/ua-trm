<?php

/**
 * Rest object example 
 *
 * GNU General Public License (Version 2, June 1991)
 *
 * This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU
 * General Public License as published by the Free
 * Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class gmo extends restObject
{

    /**
     * user data
     */
    public $id;
    public $name;
    public $lastName;
    public $login;

    /**
     *
     * @param string $method
     * @param array $request
     * @param string $file
     */
    public function __construct($method, $request = null, $file = null)
    {
        parent::__construct($method, $request, $file);
    }

    private function putsftp($filename,$folder='LIVE'){
        // move them to the SFTP site.
        try
        {
            require(DIR_CLASSES.'phpseclib/Crypt/RSA.php');
            require(DIR_CLASSES.'phpseclib/Net/SSH2.php');
            require(DIR_CLASSES.'phpseclib/Net/SFTP.php');

            $includes = get_include_path();

            set_include_path($includes.PATH_SEPARATOR.FS_API_ROOT.DIR_CLASSES.'phpseclib/');

            $sftp = new Net_SFTP('85.13.238.176',22,2);

            $key = new Crypt_RSA();
            $key->setPassword('K4N8zyfF');
            $key->loadKey(file_get_contents('includes/keys/.ssh/Tesco_Slickstitch_SFTP_Key_New'));

            if (!$sftp->login('tescosftp', $key)) {
                return false;
            }

            $sftp->chdir("".$folder.'/OUTBOUND');

            $sftp->put(basename($filename),file_get_contents($filename));

            return true;
        }
        catch (Exception $e)
        {
           return false;
        }
    }

    public function testsssftp($folder='LIVE'){
        // move them to the SFTP site.
        try
        {
            require(DIR_CLASSES.'phpseclib/Crypt/RSA.php');
            require(DIR_CLASSES.'phpseclib/Net/SSH2.php');
            require(DIR_CLASSES.'phpseclib/Net/SFTP.php');

            $includes = get_include_path();

             echo $includes;

            set_include_path($includes.PATH_SEPARATOR.FS_API_ROOT.DIR_CLASSES.'phpseclib/');
            //set_include_path($includes.':'.FS_API_ROOT.DIR_CLASSES.'phpseclib');

            $includes = get_include_path();

            echo $includes;

            //var_dump(stream_resolve_include_path("Crypt/RC4.php"));

            define('NET_SSH2_LOGGING', 2);
            define('NET_SFTP_LOGGING', 2);
            //define('NET_SSH2_LOGGING', NET_SSH2_LOG_SIMPLE);

            $sftp = new Net_SFTP('85.13.238.176',22,2);
            //$sftp = new Net_SFTP('94.125.22.124',6022,2);

            $key = new Crypt_RSA();
            $key->setPassword('K4N8zyfF');
            $key->loadKey(file_get_contents('includes/keys/.ssh/Tesco_Slickstitch_SFTP_Key_New'));

            // set password?

            // Domain can be an IP too

            /*    if (!$sftp->login('svc-SlickStitch', $key)) {
                    exit('Login Failed');
                } else {
                    exit('Logged In');
                }*/

            if (!$sftp->login('tescosftp', $key)) {
            //if (!$sftp->login('root', 'g?BChAiGaEE9')) {
            //if (!$sftp->login('svc-SlickStitch', 'Yhs35Ew@rs')) {
                echo $sftp->exec('pwd');

                 //var_dump($sftp->getLog());
                 //var_dump($sftp->getErrors());

                 //var_dump($sftp->getSFTPLog());

                exit('Login Failed');
            }

            $files = $sftp->nlist();
            var_dump($files);
            //$sftp->mkdir('LIVE/OUTBOUND');
            //$sftp->mkdir('TEST');
            //$sftp->mkdir('TEST/INBOUND');
            //$sftp->mkdir('TEST/OUTBOUND');

            // make the files

            //


        }
        catch (Exception $e)
        {
            echo $e->getMessage() . "\n";
        }

    }


    public function testsftp(){

        // move them to the SFTP site.
        try
        {
            require(DIR_CLASSES.'phpseclib/Crypt/RSA.php');
            require(DIR_CLASSES.'phpseclib/Net/SSH2.php');
            require(DIR_CLASSES.'phpseclib/Net/SFTP.php');

            $includes = get_include_path();

           // echo $includes;

            set_include_path($includes.PATH_SEPARATOR.FS_API_ROOT.DIR_CLASSES.'phpseclib/');
            //set_include_path($includes.':'.FS_API_ROOT.DIR_CLASSES.'phpseclib');

            $includes = get_include_path();


            //echo $includes;

            //var_dump(stream_resolve_include_path("Crypt/RC4.php"));

            define('NET_SSH2_LOGGING', 2);
            define('NET_SFTP_LOGGING', 2);
            //define('NET_SSH2_LOGGING', NET_SSH2_LOG_SIMPLE);

            //$sftp = new Net_SFTP('sfg.global.tesco.org',6022,2); // Live
            $sftp = new Net_SFTP('sfg.dev.global.tesco.org',6022,2); // SIT

            $key = new Crypt_RSA();
           // $key->setPassword('K4N8zyfF');
           // $key->loadKey(file_get_contents('includes/keys/.ssh/Tesco_Slickstitch_SFTP_Key_New'));

            // set password?

            // Domain can be an IP too

                    /*    if (!$sftp->login('svc-SlickStitch', $key)) {
                            exit('Login Failed');
                        } else {
                            exit('Logged In');
                        }*/

            //if (!$sftp->login('tescosftp', $key)) {
           // if (!$sftp->login('svc-SlickStitch', 'Yue5A#fs23')) { // LIVE
           if (!$sftp->login('svc-SlickStitch', 'Yhs35Ew@rs')) { // SIT
                echo $sftp->exec('pwd');

               // var_dump($sftp->getLog());
               // var_dump($sftp->getErrors());

               // var_dump($sftp->getSFTPLog());

                exit('Login Failed');
            }

            $files = $sftp->nlist();
            var_dump($files);

            //


        }
        catch (Exception $e)
        {
            echo $e->getMessage() . "\n";
        }

    }

    public function exportbulk(){

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        $data = [
            'type' => 200, // OK found as standard
            'messages' => [], // list of messages.
            'data' => [], // Hold the data to be used on the page
            'metrics'=>[
                'time' => microtime()
            ]  // Hold flags and metrics required to progress app.
        ];

        $data['metrics']['andy'] = [];

        $bulkGMOQuery = dbq("select obo.*, o.school_URN, o.organization_name, o.organization_parent_organization_id
                                    from organization_bulk_orders obo
									join organizations o USING (organization_id)
									where organization_bulk_order_gmo_sent = 0");

        while ($bulkGMO = dbf($bulkGMOQuery)) {
            // create an xls or json object of this.
            $data['metrics']['andy'][] = $bulkGMO;
            $names = explode(' ', $bulkGMO['order_contact_name'], 2);

            if (sizeof($names) < 2) {
                $firstname = $names[0];
                $lastname = $names[0];
            } else {
                $firstname = $names[0];
                $lastname = $names[1];
            }

            //$bulkGMO['order_email_address'] = 'andy'.rand(1,10).'@slickstitch.com';  // test with random email

            $gmoOrder = [
                'order' => [
                    'orderHeader' => [
                        'externalOrderNumber' => $bulkGMO['organization_bulk_order_id'],
                        'businessEmail' => $bulkGMO['order_email_address'],
                        'businessReference' => "SlickStitch"
                    ],
                    'billingAddress' => [
                        'country' => "UK",
                        'address1' => $bulkGMO['order_delivery_address_1'],
                        'address2' => $bulkGMO['order_delivery_address_2'],
                        'postalCode' => $bulkGMO['order_delivery_postcode'],
                        'firstName' => $firstname,
                        'lastName' => $lastname,
                        'locality' => $bulkGMO['order_delivery_county'],
                        'title' => 'None',
                        'address3' => $bulkGMO['order_delivery_address_3'],
                        'city' => $bulkGMO['order_delivery_town'],
                        'buildingNameNumber' => '',
                        'daytimeNumber' => $bulkGMO['order_telephone'],
                        'eveningNumber' => $bulkGMO['order_telephone'],
                        'organisationName' => $bulkGMO['order_name'],
                        'phoneNumber' => $bulkGMO['order_telephone'],
                        'subBuildingNumber' => '',
                        'email' => $bulkGMO['order_email_address']
                    ],
                    'deliveryDetails' => [
                        [
                            'itemList' => [
                                // fill below.
                            ],
                            'deliveryAddress' => [
                                'country' => "UK",
                                'address1' => $bulkGMO['order_delivery_address_1'],
                                'address2' => $bulkGMO['order_delivery_address_2'],
                                'postalCode' => $bulkGMO['order_delivery_postcode'],
                                'firstName' => $firstname,
                                'lastName' => $lastname,
                                'locality' => $bulkGMO['order_delivery_county'],
                                'title' => 'None',
                                'address3' => $bulkGMO['order_delivery_address_3'],
                                'city' => $bulkGMO['order_delivery_town'],
                                'buildingNameNumber' => '',
                                'daytimeNumber' => $bulkGMO['order_telephone'],
                                'eveningNumber' => $bulkGMO['order_telephone'],
                                'organisationName' => $bulkGMO['order_name'],
                                'phoneNumber' => $bulkGMO['order_telephone'],
                                'subBuildingNumber' => '',
                                'email' => $bulkGMO['order_email_address']
                            ]
                        ]
                    ]
                ],
            ];

            $bulkOrderItemsQuery = dbq("select oboi.* from organization_bulk_order_items oboi
									where oboi.organization_bulk_order_id = :OBOID and oboi.product_removed = 0",
                [
                    ':OBOID' => $bulkGMO['organization_bulk_order_id']
                ]
            );

            // get items too.
            while($bulkOrderItems = dbf($bulkOrderItemsQuery)){
                $gmoOrder['order']['deliveryDetails'][0]['itemList'][] = [
                    'itemId' => $bulkOrderItems['product_gmo_cat_id'],
                    'quantity' => intval($bulkOrderItems['product_quantity']),
                    'schoolName' => $bulkGMO['order_name'],
                    'schoolId' => getMasterURN($bulkOrderItems['organization_urn']),
                    'logoRef' => trim($bulkOrderItems['products_logo_ref'])
                ];
            }

            // save json to file.
            $filename = 'BulkOrder'.$bulkGMO['organization_bulk_order_id'].'-'.shorten(time()).'.json';
            $data['metrics']['filename'] = $filename;
            // save it to a file..
            file_put_contents('bulkorders/'.$filename,json_encode($gmoOrder));

            // SFTP upload it.
            if($this->putsftp('bulkorders/'.$filename,'LIVE')){
                // update order to sent.
                dbq("UPDATE organization_bulk_orders set organization_bulk_order_gmo_sent = 1, organization_bulk_order_gmo_sent_datetime = NOW() where organization_bulk_order_id = :OBOID",
                    [
                        ':OBOID' => $bulkGMO['organization_bulk_order_id']
                    ]
                );

                $emailAddress = [
                    [
                        'Joanna.Osterman@uk.tesco.com' => 'Joanna.Osterman@uk.tesco.com',
                        'Emma.Rawson@uk.tesco.com' => 'Emma.Rawson@uk.tesco.com',
                        'Lyn.Smith@uk.tesco.com' => 'Lyn.Smith@uk.tesco.com',
                        'Karen.Brown@uk.tesco.com' => 'Karen.Brown@uk.tesco.com',
                        'Mark.McRae@uk.tesco.com' => 'Mark.McRae@uk.tesco.com',
                        'andy@slickstitch.com' => 'andy@slickstitch.com',
                        'ben.malcolm@uk.tesco.com' => 'ben.malcolm@uk.tesco.com',
                        'Megan.Bettles@uk.tesco.com' => 'Megan.Bettles@uk.tesco.com',
                        'Kathryn.James@uk.tesco.com' => 'Kathryn.James@uk.tesco.com',
                        'Ross.Williams@uk.tesco.com' => 'Ross.Williams@uk.tesco.com',
                        'Suprabhat.Sural@in.tesco.com' => 'Suprabhat.Sural@in.tesco.com',
                        'Rakesh.Pai@in.tesco.com' => 'Rakesh.Pai@in.tesco.com',
                        'Ben.Saunders@uk.tesco.com' => 'Ben.Saunders@uk.tesco.com',
                        'Chris.Stevenson@uk.tesco.com' => 'Chris.Stevenson@uk.tesco.com',
                        'jonathan.lonsdale@uk.tesco.com' => 'jonathan.lonsdale@uk.tesco.com'
                    ],
                ];

                $subject = 'New Bulk order sent to ATG';
                $title = $subject;
                $template = 'tesco-ff-uniforms';
                $tag = ['ff-new-bulk'];

                $template_content = array(
                    [
                        'name' => 'emailtitle',
                        'content' => $title
                    ],
                    [
                        'name' => 'main',
                        'content' => 'New test Bulk order has been sent to ATG '.$bulkGMO['organization_bulk_order_id']
                    ]
                );

                $attachmentArray[] = [
                    'type' => "text/csv",
                    'name' => basename($filename),
                    'content' => base64_encode(file_get_contents(API_ROOT . 'bulkorders/'.$filename))
                ];

                send_ues_mail($emailAddress, $subject, $template_content,$template,$tag,$attachmentArray);

            } else {
                // error uploading.  It will try again later.
            }


        }

        return $data;
    }

    // UPDATE organization_bulk_order_items obi JOIN products_sizes_to_products_colours ps ON (ps.`product_variation_sku` = obi.`product_ean`) SET obi.`product_gmo_cat_id` = ps.`product_variation_gmo_sku`


    /**
     * Lookup endpoint. Search registered schools.
     * @return array
     */

    public function lookupregistered()
    {

        if (!$this->isMethodCorrect('GET')) {
            return $this->getResponse(500);
        }

        if ((!isset($this->request['query']) || !isset($this->request['limit'])) || empty($this->request['query']) || empty($this->request['limit'])) {
            $this->setError([
                'message' => 'Cannot provide results, required arguments are missing.',
                'errorCode' => 'arguments-missing',
            ]);
            return $this->getResponse(500);
        }

        // split the query into individual words.
        $searchWords = explode(' ', $this->request['query']);
        $searchLimit = $this->request['limit'];
        $lookup = [];


        $weighting = "IF(
			`Name` LIKE '{$this->request['query']}%',  20, IF(`Name` LIKE '%{$this->request['query']}%', 10, 0)
      							)
      								+ IF(`Town` LIKE '%{$this->request['query']}%', 5,  0)
      								+ IF(`Postcode` LIKE '%{$this->request['query']}%', 1,  0)
    							";
        foreach ($searchWords as $index => $word) {
            if (!empty($weighting)) {
                $weighting .= ' + ';
            }

            $weighting .= "IF(
			`Name` LIKE '$word%',  20, IF(`Name` LIKE '%$word%', 10, 0)
      							)
      								+ IF(`Town` LIKE '%$word%', 5,  0)
      								+ IF(`Postcode` LIKE '%$word%', 1,  0)
    							";

            $lookup[':LOOKUP' . $index] = '%' . urldecode($word) . '%';
        }
        $weighting .= ' AS `weight`,';


        //foreach($searchWords as $index => $word){
        //	$lookup[':LOOKUP'.$index] = '%'.urldecode($word).'%';
        //}

// To search other fields add them to here.
        $matchFields = [
            'Name',
            'Address1',
            'Address2',
            'Town',
            'County',
            'Postcode',
            'URL',
            'Phone'
        ];

        $searchTerms = '';
        foreach ($lookup as $key => $value) {
            // Group each word into its own OR group.
            $searchTerms .= '(';
            foreach ($matchFields as $field) {
                $searchTerms .= "sd.`$field` LIKE $key OR ";
            }
            $searchTerms = substr($searchTerms, 0, -3);
            // link OR groups by AND.  each word must be there but could be in any field.
            $searchTerms .= ') AND ';
        }

        $searchTerms = substr($searchTerms, 0, -4);

        $searchSchoolQuery = dbq("select SQL_CALC_FOUND_ROWS
								$weighting
                               sd.`school_data_id`,
                               sd.`Name`,
                               sd.`Address1`,
                               sd.`Address2`,
                               sd.`Address3`,
                               sd.`Town`,
                               sd.`County`,
                               sd.`Area`,
                               sd.`Postcode`,
                               o.`organization_id`,
                               ss.`school_status_name`,
                               ol.`tesco_style_ref` as logo_ref,
                               ol.`primary_background_colour` as emblem_background,
                               o.organization_has_subunits,
                               o.organization_parent_id,
                               IF(ISNULL(o.organization_funds),0,o.organization_funds) as organization_funds,
                               IF(ISNULL(ol.`logo_url`),'images/emblems/example-emblem.png',ol.`logo_url`) as emblem_png,
                               IF(ISNULL(ol.`logo_svg_url`),'images/emblems/example-emblem.png',ol.`logo_svg_url`) as emblem_svg
                        from school_data sd
                        join school_statuses ss using (school_status_id)
                        left join organizations o using (school_data_id)
                        left join organization_units ou USING (organization_id)
                        left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id and ol.logo_approved = 1)
                        where ( $searchTerms )
                        AND sd.school_status_id = 3
                        AND o.organization_parent_id IS NULL
                        GROUP BY sd.school_data_id
                        ORDER BY `weight` DESC, sd.`Name` ASC
                        limit $searchLimit", $lookup);

        $data = [
            'count' => 0,
            'organizations' => [],
        ];

        $dataCount = 0;

        // need to split out emblems and address.

        $addressFields = [
            'Address1',
            'Address2',
            'Address3',
            'Town',
            'County',
            'Area',
            'Postcode'
        ];

        $emblemFields = [
            'emblem_png',
            'emblem_svg',
            'emblem_background',
        ];

        $doubleFields = [
            'organization_funds',
        ];

        $ignoreFields = [
            'weight',
            'organization_has_subunits',
            'organization_parent_id'
        ];

        while ($searchSchool = dbf($searchSchoolQuery)) {
            $searchSchool['emblem_png'] = 'http:' . $searchSchool['emblem_png'];
            foreach ($searchSchool as $key => $value) {
                if (!in_array($key, $ignoreFields)) {
                    if (in_array($key, $emblemFields)) {
                        $data['organizations'][$dataCount]['emblem'][str_replace('emblem_', '', strtolower($key))] = $value;
                    } else if (in_array($key, $addressFields)) {
                        $data['organizations'][$dataCount]['address'][strtolower($key)] = $value;
                    } else if (in_array($key, $doubleFields)) {
                        $data['organizations'][$dataCount][strtolower($key)] = sprintf("%0.2f", round($value, 2));
                    } else {
                        $data['organizations'][$dataCount][strtolower($key)] = $value;
                    }
                }
            }
            // If this is a house, need the others and the master too.

            // does it have houses.
            if ($searchSchool['organization_has_subunits']) {
                // has houses.
                $dataSubCount = 0;

                // add the main school to the houses.
                foreach ($searchSchool as $key => $value) {
                    if (!in_array($key, $ignoreFields)) {
                        if (in_array($key, $emblemFields)) {
                            $data['organizations'][$dataCount]['subunits'][$dataSubCount]['emblem'][str_replace('emblem_', '', strtolower($key))] = $value;
                        } else if (in_array($key, $addressFields)) {
                            $data['organizations'][$dataCount]['subunits'][$dataSubCount]['address'][strtolower($key)] = $value;
                        } else {
                            if (is_numeric($value) && $value == doubleval($value)) {
                                $value = round($value, 2);
                            }
                            $data['organizations'][$dataCount]['subunits'][$dataSubCount][strtolower($key)] = $value;
                        }
                    }
                }

                $dataSubCount++;

                $searchSubSchoolQuery = dbq("select SQL_CALC_FOUND_ROWS
                               sd.`school_data_id`,
                               sd.`Name`,
                               sd.`Address1`,
                               sd.`Address2`,
                               sd.`Address3`,
                               sd.`Town`,
                               sd.`County`,
                               sd.`Area`,
                               sd.`Postcode`,
                               o.`organization_id`,
                               ss.`school_status_name`,
                               ol.`tesco_style_ref` as logo_ref,
                               ol.`primary_background_colour` as emblem_background,
                               IF(ISNULL(o.organization_funds),0,o.organization_funds) as organization_funds,
                               IF(ISNULL(ol.`logo_url`),'images/emblems/example-emblem.png',ol.`logo_url`) as emblem_png,
                               IF(ISNULL(ol.`logo_svg_url`),'images/emblems/example-emblem.png',ol.`logo_svg_url`) as emblem_svg
                        from school_data sd
                        join school_statuses ss using (school_status_id)
                        left join organizations o using (school_data_id)
                        left join organization_units ou USING (organization_id)
                        left join organization_logos ol on (ol.organization_logo_id = o.organization_default_logo_id and ol.logo_approved = 1)
                        where sd.school_status_id = 3
                        AND o.organization_parent_id = :PARENTID
                        GROUP BY sd.school_data_id
                        ORDER BY o.organization_id DESC, nominations DESC", [':PARENTID' => $searchSchool['organization_id']]);

                while ($searchSubSchool = dbf($searchSubSchoolQuery)) {
                    $searchSchool['emblem_png'] = HTTP_ROOT . $searchSchool['emblem_png'];
                    foreach ($searchSubSchool as $key => $value) {
                        if (!in_array($key, $ignoreFields)) {
                            if (in_array($key, $emblemFields)) {
                                $data['organizations'][$dataCount]['subunits'][$dataSubCount]['emblem'][str_replace('emblem_', '', strtolower($key))] = $value;
                            } else if (in_array($key, $addressFields)) {
                                $data['organizations'][$dataCount]['subunits'][$dataSubCount]['address'][strtolower($key)] = $value;
                            } else {
                                if (is_numeric($value) && $value == doubleval($value)) {
                                    $value = round($value, 2);
                                }
                                $data['organizations'][$dataCount]['subunits'][$dataSubCount][strtolower($key)] = $value;
                            }
                        }
                    }
                    // If this is a house, need the others and the master too.
                    $dataSubCount++;
                }


            } else {
                $data['organizations'][$dataCount]['subunits'] = null;
            }
            $dataCount++;
        }

        $data['count'] = sizeof($data['organizations']);

        return $data;
    }

    public function trimskus(){
        $productSizesQuery = dbq("select products_sizes_to_colours_id, product_variation_gmo_sku from products_sizes_to_products_colours");
        while($productSizes = dbf($productSizesQuery)){

            dbq("update products_sizes_to_products_colours set product_variation_gmo_sku = :TRIMMEDSKU where products_sizes_to_colours_id = :PSTCID",
                [
                    ':TRIMMEDSKU' => trim($productSizes['product_variation_gmo_sku']),
                    ':PSTCID' => $productSizes['products_sizes_to_colours_id'],
                ]
            );
        }
    }


    public function checklisted(){
        ini_set('max_execution_time',7200);
        $productSizesQuery = dbq("select products_sizes_to_colours_id, product_variation_gmo_sku from products_sizes_to_products_colours");

        while($productSizes = dbf($productSizesQuery)){

            $listingData = @file_get_contents('https://www.tesco.com/direct/rest/content/catalog/sku/'.trim($productSizes['product_variation_gmo_sku']));
            //$stockData = @file_get_contents('https://www.tesco.com/direct/rest/inventory/sku/'.$productSizes['product_variation_gmo_sku'].'?format=standard');
            //$priceData = @file_get_contents('https://www.tesco.com/direct/rest/price/sku/'.$productSizes['product_variation_gmo_sku'].'?format=standard');

            $listed = false;
            $found = false;

            if($http_response_header[0] == 'HTTP/1.0 404 Not Found'){
                echo $productSizes['product_variation_gmo_sku']." Not Found \n";
            } else {
                $found = true;
               // echo $productSizes['product_variation_gmo_sku']." Found \n";
                $listingData = json_decode($listingData);

                foreach($listingData->links as $link){
                    if($link->type == 'listing' && $link->rel == 'listed'){
                        $listed = true;
                        break;
                    }
                }
            }

            dbq("update products_sizes_to_products_colours set product_variation_listed = :LISTED, product_variation_found = :FOUND where products_sizes_to_colours_id = :PSTCID",
                [
                    ':LISTED' => $listed ? 1 : 0,
                    ':FOUND' => $found ? 1 : 0,
                    ':PSTCID' => $productSizes['products_sizes_to_colours_id']
                ]
            );

        }
    }

    public function checkprices(){
        ini_set('max_execution_time',7200);
        $productSizesQuery = dbq("select products_sizes_to_colours_id, product_variation_gmo_sku,product_variation_price from products_sizes_to_products_colours where product_variation_listed = 1");

        while($productSizes = dbf($productSizesQuery)){

            $priceData = @file_get_contents('https://www.tesco.com/direct/rest/price/sku/'.trim($productSizes['product_variation_gmo_sku']).'?format=standard');

            if($http_response_header[0] == 'HTTP/1.0 404 Not Found'){
                echo $productSizes['product_variation_gmo_sku']." Not Found \n";
            } else {
                $found = true;
                echo $productSizes['product_variation_gmo_sku']." Found - Price : \n";
                $priceData = json_decode($priceData,true);

                if(isset($priceData['skus'][0]['price'])){
                   echo $productSizes['product_variation_gmo_sku']." Price : ".$priceData['skus'][0]['price']."\n";

                   dbq("update products_sizes_to_products_colours set product_variation_price = :PRICE where products_sizes_to_colours_id = :PSTCID",
                        [
                            ':PRICE' => $priceData['skus'][0]['price'],
                            ':PSTCID' => $productSizes['products_sizes_to_colours_id']
                        ]
                   );

                } else {
                    echo $productSizes['product_variation_gmo_sku']." ***NOPRICE***\n";
                }
            }
        }
        // Lastly update the price on the main product.

        $productQuery = dbq("SELECT p.`product_id`, p.`product_price`, MIN(ps2c.`product_variation_price`) as `new_price` FROM products p JOIN product_colours pc ON pc.`product_id` = p.`product_id` JOIN products_sizes_to_products_colours ps2c ON ps2c.`product_colour_id` = pc.`product_colour_id` AND ps2c.`product_variation_listed` = 1 WHERE p.product_active = 1 AND pc.`product_colour_active` = 1 GROUP BY p.`product_id`");

        while($product = dbf($productQuery)){
            // select the cheapest?
            dbq("update products p set p.product_price = :NEWPRICE where p.product_id = :PRODUCTID",
                [
                    ':NEWPRICE' => $product['new_price'],
                    ':PRODUCTID' => $product['product_id']
                ]
            );
        }
    }

    public function checkimages(){
        ini_set('max_execution_time',7200);
        $productColoursQuery = dbq("select * from product_colours where product_colour_id >= 324");

        while($productColours = dbf($productColoursQuery)){

            $productData = @file_get_contents('https://www.tesco.com/direct/rest/content/catalog/product/'.trim($productColours['product_colour_gmo_ref']).'');

            if($http_response_header[0] == 'HTTP/1.0 404 Not Found'){
                echo $productColours['product_colour_gmo_ref']." Not Found \n";
            } else {

                // echo $productSizes['product_variation_gmo_sku']." Found \n";
                $productData = json_decode($productData,true);

                if(isset($productData['mediaAssets']['defaultSku']['defaultImage']['src'])){

                    $image = file_get_contents('http:'.$productData['mediaAssets']['defaultSku']['defaultImage']['src']);

                    file_put_contents(FS_ROOT.'images/products/'.trim($productColours['product_colour_style_ref']).'.jpg',$image);

                        dbq("update product_colours set product_colour_image_url = :IMAGE where product_colour_id = :PCID",
                            [
                                ':IMAGE' => '//www.ff-ues.com/images/products/'.trim($productColours['product_colour_style_ref']).'.jpg',
                                ':PCID' => $productColours['product_colour_id']
                            ]
                        );


                }
            }
        }
    }

    public function bulkresponse(){
        $folder='LIVE';
        // move them to the SFTP site.
        try
        {
            require(DIR_CLASSES.'phpseclib/Crypt/RSA.php');
            require(DIR_CLASSES.'phpseclib/Net/SSH2.php');
            require(DIR_CLASSES.'phpseclib/Net/SFTP.php');

            $includes = get_include_path();

            set_include_path($includes.PATH_SEPARATOR.FS_API_ROOT.DIR_CLASSES.'phpseclib/');

            $sftp = new Net_SFTP('85.13.238.176',22,2);

            $key = new Crypt_RSA();
            $key->setPassword('K4N8zyfF');
            $key->loadKey(file_get_contents('includes/keys/.ssh/Tesco_Slickstitch_SFTP_Key_New'));

            if (!$sftp->login('tescosftp', $key)) {
                return false;
            }

            $sftp->chdir("".$folder.'/INBOUND');
            $files = $sftp->nlist();

            $responses = [];

            foreach($files as $file){
                if(stristr($file,'.json')){
                    $fileContent = $sftp->get($file);
                    $fileContentArray = json_decode($fileContent,true);
                    $sendmail = true;

                    $bulkOrderQuery = dbq("select * from organization_bulk_orders where organization_bulk_order_id = :ORDID",
                        [
                            ':ORDID' => $fileContentArray['externalOrderNumber']
                        ]
                    );

                    $bulkOrder = dbf($bulkOrderQuery);

                    $orgDetails = getOrgDetails(getMasterOrgID($bulkOrder['organization_id']));

                    if(stristr($file,'.err.json')){
                        // Error
                        // update the error reason.
                       // return $fileContentArray;

                        $updateOrder = dbq("update organization_bulk_orders obo set obo.organization_bulk_order_gmo_order_response = :ORDRESP, obo.organization_bulk_order_gmo_order_error = :GMOORDERERR where obo.organization_bulk_order_id = :OBOID and obo.organization_bulk_order_gmo_order_number IS NULL",
                            [
                                ':ORDRESP' => $fileContent,
                                ':GMOORDERERR' => implode('|',$fileContentArray['errorCodes']),
                                ':OBOID' => $fileContentArray['externalOrderNumber']
                            ]
                        );

                        // Check if this error updated.  Wont update if order already has an order ID.
                        if(dbnr($updateOrder) > 0){

                            $emailContent = "Bulk Order: ".$fileContentArray['externalOrderNumber']." failed to imported into ATG<br /><br />Reasons:<br />";

                            foreach($fileContentArray['errorCodes'] as $error){
                                $emailContent .= $error."<br />";
                            }

                            $subject = "Bulk Order: ".$fileContentArray['externalOrderNumber']." failed import into ATG";
                            $title = $orgDetails['organization_name']." (".$orgDetails['school_URN'].")";
                            $template = 'tesco-ff-uniforms';
                            $tag = ['ff-bulk-update'];

                        } else {
                            $sendmail = false;
                        }


                    } else {
                        // Success
                        // update the order id.
                        dbq("update organization_bulk_orders obo set obo.organization_bulk_order_gmo_order_response = :ORDRESP, obo.organization_bulk_order_gmo_order_number = :GMOORDERNUM, obo.organization_bulk_order_gmo_order_error = '' where obo.organization_bulk_order_id = :OBOID",
                            [
                                ':ORDRESP' => $fileContent,
                                ':GMOORDERNUM' => $fileContentArray['tescoOrderId'],
                                ':OBOID' => $fileContentArray['externalOrderNumber']
                            ]
                        );

                        $emailContent = "Bulk Order: ".$fileContentArray['externalOrderNumber']." has imported into ATG: ".$fileContentArray['tescoOrderId'];

                        $subject = "Bulk Order: ".$fileContentArray['externalOrderNumber']." = ATG: ".$fileContentArray['tescoOrderId'];
                        $title = $orgDetails['organization_name']." (".$orgDetails['school_URN'].")";
                        $template = 'tesco-ff-uniforms';
                        $tag = ['ff-bulk-update'];

                    }

                    if($sendmail){
                        $emailAddress = [
                            'Andy' => 'andy@slickstitch.com',
                            //'CSC' => 'csc.tlead@uk.tesco.com',
                            'Megan Bettles' => 'Megan.Bettles@uk.tesco.com'
                        ];

                        $message = $emailContent;

                        $template_content = array(
                            [
                                'name' => 'emailtitle',
                                'content' => $title
                            ],
                            [
                                'name' => 'main',
                                'content' => $message
                            ]
                        );

                        send_ues_mail($emailAddress, $subject, $template_content, $template, $tag);
                    }

                    // move file.
                    $sftp->chdir("read");
                    $sftp->put(basename($file),$fileContent);
                    $sftp->chdir("..");
                    $sftp->delete($file);

                   // die;

                }
            }

            return $responses;

        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }


    function findmissingchildren(){
        ini_set('max_execution_time',7200);

        $productSizesQuery = dbq("select products_size_name, product_size_id from product_sizes");
        $sizeArray = [];
        $allowedsizes = [
            'ONE SIZE',
            'XS',
            'S',
            'M',
            'L',
            'XL',
            'XXL',
            'XXXL',
            'XXXXL'
        ];

        while($productSizes = dbf($productSizesQuery)){
            if(!in_array(strtoupper($productSizes['products_size_name']),$allowedsizes)){
                $sizeArray[preg_replace("/[^0-9]/","",$productSizes['products_size_name'])] = $productSizes['product_size_id'];
            } else {
                $sizeArray[strtoupper($productSizes['products_size_name'])] = $productSizes['product_size_id'];
            }
        }

        //return $sizeArray;
        $productColoursQuery = dbq("select pc.product_colour_id, pc.product_colour_gmo_ref, pc.product_colour_style_ref from product_colours pc join products p on p.product_id = pc.product_id where p.product_decorated = 1 and p.product_active = 1");

        while($productColours = dbf($productColoursQuery)){

            // echo 'https://www.tesco.com/direct/rest/content/catalog/product/'.trim($productColours['product_colour_gmo_ref'])."\n";

            $listingData = @file_get_contents('https://www.tesco.com/direct/rest/content/catalog/product/'.trim($productColours['product_colour_gmo_ref']));

            if($http_response_header[0] == 'HTTP/1.0 404 Not Found'){
                echo $productColours['product_colour_gmo_ref']." - ".$productColours['product_colour_style_ref']." Not Found \n";
            } else {

                $listingData = json_decode($listingData);

                foreach($listingData->links as $link){
                    // loop through the child items.
                    if($link->type == 'sku' && $link->rel == 'childSku'){
                        // check we have a child to match.
                        $childQuery = dbq("select * from products_sizes_to_products_colours where product_variation_gmo_sku = :GMOSKU",
                            [
                                ':GMOSKU' => trim($link->id)
                            ]
                        );

                        if(dbnr($childQuery) < 1){
                            // MISSING!
                            if(!in_array(strtoupper($link->options->secondary),$allowedsizes)){
                                $sizecode = preg_replace("/[^0-9]/","",$link->options->secondary);
                            } else {
                                $sizecode = strtoupper($link->options->secondary);
                            }

                            echo "MISSING ".$link->id," SIZE ON : ".$listingData->displayName.' '.$link->options->primary.' '.$link->options->secondary."\n";
                            echo "COLOUR ID: ".$productColours['product_colour_id'] . " SIZE ID: ".$sizeArray[$sizecode]."\n";

                            if(isset($sizeArray[$sizecode])){
                                // check we dont have one that SHOULD be this one.
                                $childDoubleCheckQuery = dbq("select * from products_sizes_to_products_colours where product_colour_id = :COLURID and product_size_id = :SIZEID",
                                    [
                                        ':COLURID' => $productColours['product_colour_id'],
                                        ':SIZEID' => $sizeArray[$sizecode]
                                    ]
                                );

                                if(dbnr($childDoubleCheckQuery) > 0){
                                    //FOUND EQUIVELENT - Must have the wrong EAN?
                                    $childDoubleCheck = dbf($childDoubleCheckQuery);
                                    echo "FOUND ALTERNATIVE: ".$childDoubleCheck['product_variation_gmo_sku'].' '.$childDoubleCheck['product_variation_sku']."\n";

                                    //if(is_null($childDoubleCheck['product_variation_gmo_sku'])){
                                        // update it with what we found.
                                        dbq("update products_sizes_to_products_colours set product_variation_gmo_sku = :GMOSKU where product_colour_id = :COLURID and product_size_id = :SIZEID",
                                            [
                                                ':GMOSKU' => trim($link->id),
                                                ':COLURID' => $productColours['product_colour_id'],
                                                ':SIZEID' => $sizeArray[$sizecode]
                                            ]
                                        );
                                        echo "UPDATED ALTERNATIVE: ".$link->id.' '.$childDoubleCheck['product_variation_sku']."\n";
                                    //}
                                } else {
                                    // Doesnt exist in the database..  Should probably create it but can do that later.

                                    dbq("insert into products_sizes_to_products_colours (
                                          product_size_id,
                                          product_colour_id,
                                          product_variation_sku,
                                          product_variation_price,
                                          product_variation_gmo_sku,
                                          product_variation_stock,
                                          product_variation_listed,
                                          product_variation_found
                                        ) values (
                                          :PRODUCTSIZEID,
                                          :PRODUCTCOLOURID,
                                          :PRODUCTVARIATIONSKU,
                                          :PRODUCTVARIATIONPRICE,
                                          :PRODUCTVARIATIONGMOSKU,
                                          :PRODUCTVARIATIONSTOCK,
                                          :PRODUCTVARIATIONLISTED,
                                          :PRODUCTVARIATIONFOUND
                                        )",
                                        [
                                            ':PRODUCTSIZEID' => $sizeArray[$sizecode],
                                            ':PRODUCTCOLOURID' => $productColours['product_colour_id'],
                                            ':PRODUCTVARIATIONSKU' => '',
                                            ':PRODUCTVARIATIONPRICE' => $listingData->prices->toPrice, // assume higher price then fix later.
                                            ':PRODUCTVARIATIONGMOSKU' => $link->id,
                                            ':PRODUCTVARIATIONSTOCK' => 0,
                                            ':PRODUCTVARIATIONLISTED' => '1',
                                            ':PRODUCTVARIATIONFOUND' => '1'
                                        ]
                                    );
                                }
                                echo "ADDED NEW: ".$link->id.'-'.$link->options->secondary."\n";
                              //  die;
                            } else {
                                // weird size?  report it.
                                echo "***** NOT FOUND SIZE *******: ".$link->options->secondary."\n";
                            }
                        } else {
                            // Found it, nothing to do.
                        }
                    }
                }
            }
        }
    }

    public function testapi(){
      //  $this->putsftp('bulkorders/BulkOrder1354.json','TEST');
    }

}