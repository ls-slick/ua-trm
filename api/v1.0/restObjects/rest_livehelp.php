<?php

/**
 * Rest object example
 * 
 * GNU General Public License (Version 2, June 1991) 
 * 
 * This program is free software; you can redistribute 
 * it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, 
 * or (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 *
 * @author Rafał Przetakowski <rprzetakowski@pr-projektos.pl>
 */
class livehelp extends restObject {

	/**
	 * @param string $method
	 * @param array $request
	 * @param string $file
	 */

	public function __construct($method, $request = null, $file = null) {
		parent::__construct($method, $request, $file);
	}

	public function screenshot() {

		if (!$this->isMethodCorrect('POST')) {
			return $this->getResponse(500);
		}

		$data = [
			'type' => 200, // Not found as standard
			'messages' => [], // list of messages.
			'data' => [], // Hold the data to be used on the page
			'metrics'=>[
				'time' => microtime()
			]  // Hold flags and metrics required to progress app.
		];
		$filedata = $this->request['imageData'];
		$filedata = @end(explode('base64,',$filedata,2));
		$filedata = base64_decode($filedata);

		file_put_contents('screenshots/'.$_SESSION['session']['user']['user_id'].shorten(time()).'.png',$filedata);
		return $data;
	}


}
