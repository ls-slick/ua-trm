<?php

require_once('./mysqli.php');

require_once('./ftp_login_details.php');

require_once('./helper.funcs.php');





$conn_id = ftp_connect($ftp_login_details['ftp_server']);



$login_result = ftp_login($conn_id, $ftp_login_details['ftp_user'], $ftp_login_details['ftp_pass']);



ftp_pasv($conn_id, true);



$customer_list_array = array();

$customer_list_query = $db->query("select * from customers");



while ($row = mysqli_fetch_Assoc($customer_list_query))

{

	$customer_list_array[$row['id']] = $row['customer_ftp_folder_name'];

}



echo '<pre>'; // [test /]



foreach ($customer_list_array as $customer_id => $ftp_url)

{

	$var =  "{$ftp_url}/*.csv";



	$contents = ftp_nlist($conn_id, $var);



	// [test]



	if (count($contents) == 0) 

	{

		echo '[hm] nothing new to process for '.$ftp_url.'<br />';

	}

	

	// [/test]

	

	foreach ($contents as $csv_file)

	{

		echo $csv_file; // [test /]

		

		$web_server_saved_new_csv_file_name = './retrieved_csvs/'.basename($csv_file);

		

		if (ftp_get($conn_id, $web_server_saved_new_csv_file_name, $csv_file, FTP_ASCII))

		{

			$new_csv_file_name =  dirname($csv_file).'/PROCESSED/'.basename($csv_file);

			echo '$new_csv_file_name = '.$new_csv_file_name.'<br />';

	

			if (ftp_rename($conn_id, $csv_file, $new_csv_file_name))

			{

				echo "-- moved $csv_file to $new_csv_file_name <br />";

				echo '<br />';

				$csv_data = file_get_contents($web_server_saved_new_csv_file_name);

				echo $csv_data;

		

				$csv_data_parts = explode(',', $csv_data);

		

				echo '<br />';

		

				print_r($csv_data_parts);

		

				$order['order_reference']	 = mysqli_real_escape_string($db, str_replace('"', '', $csv_data_parts[0]));

				$order['customer_name']		 = mysqli_real_escape_string($db, str_replace('"', '', $csv_data_parts[1]));

				$order['customer_email']	 = mysqli_real_escape_string($db, str_replace('"', '', $csv_data_parts[2]));

				$order['quantity_ordered'] = mysqli_real_escape_string($db, str_replace('"', '', $csv_data_parts[3]));

																$d = mysqli_real_escape_string($db, str_replace('"', '', $csv_data_parts[4]));

		

				$order['date_created'] = $d[0].$d[1].$d[2].$d[3].'-'.$d[4].$d[5].'-'.$d[6].$d[7].' '.$d[8].$d[9].':'.$d[10].$d[11].':'.$d[12].$d[13];

		

				print_r($order);

		

				$name = ss_nl_get_customer_name_for_customer_id($db, $customer_id).' ';

				$lnid = sha1($csv_data);

				$insert_new_order = "insert into orders (date_created, customer_name, customer_email, customer_id, order_reference, quantity_ordered, lnid)

						     values	('{$order['date_created']}', '{$order['customer_name']}', '{$order['customer_email']}', '{$customer_id}', '{$order['order_reference']}', '{$order['quantity_ordered']}', '$lnid')";



				$db->query($insert_new_order);



				$order_id = $db->insert_id;

		                $order_ref = $order['order_reference'];

				for ($i = 1; $i <= $order['quantity_ordered']; $i++)

				{

					$insert_new_name_texts = $db->query("insert into names (date_created, name_text_id, order_id) values (NOW(), '$i', '$order_id')");

				}



				require_once 'mailchimp-mandrill-api-php-9f336b08ea14/mailchimp-mandrill-api-php-9f336b08ea14/src/Mandrill.php';



				try

				{

					$customer_namelabels_url = ss_nl_get_customer_namelabels_url_for_customer_id($db, $customer_id);

					$customer_namelabels_email = ss_nl_get_customer_namelabels_email_for_customer_id($db, $customer_id);

					$customer_email_text = ss_nl_get_customer_service_text_for_customer_id($db, $customer_id);
					
					$customer_template_name = ss_nl_get_customer_template_for_customer_id($db, $customer_id);
					
					$link_for_email = '<a href="'.$customer_namelabels_url.'/?lnid='.$lnid.'">'.$customer_namelabels_url.'/?lnid='.$lnid.'</a>';

					$mandrill = new Mandrill('PnT0naiEhIRaHBqLOj1MxQ');

	$template_content = array(
                                  array(
                                  'name' => 'ordernumber',
                                  'content' => $order_ref
                                  ),
                                  array (
                                  'name' => 'customername',
                                  'content' => $order['customer_name']
                                  ),
                                  array (
                                  'name' => 'nametapelink',
                                  'content' => $link_for_email
                                  )
         );


	$message = array(

			'subject' => 'Your Name Labels Order '.$order_ref.': Information Required',

			'from_email' => 'no-reply@ff-ues.com',

			'from_name' => 'F&F Uniform Embroidery Service',

			'to' => array(

					array(

							'email' => $order['customer_email'],

							'name' => $order['customer_name'],

							'type' => 'to'

					)

			),

			'important' => false,

			'track_opens' => null,

			'track_clicks' => false,

			'auto_text' => null,

			'auto_html' => null,

			'inline_css' => null,

			'url_strip_qs' => null,

			'preserve_recipients' => null,

			'view_content_link' => null,

			'bcc_address' => null,

			'tracking_domain' => null,

			'signing_domain' => 'ff-ues.com',

			'return_path_domain' => null,

			'merge' => true,

			'merge_language' => 'mailchimp',

			'global_merge_vars' => null,

			'merge_vars' => null,

			'metadata' => array('website' => 'www.ff-ues.com'),

			'recipient_metadata' => null,
			
			'tags' => array('ff-ues-sapphire')

	);

	$async = false;

	$ip_pool = 'Main Pool';

	//$send_at = '1999-01-01 12:34:56';

	$result = $mandrill->messages->sendTemplate($customer_template_name, $template_content, $message, $async, $ip_pool);//, $send_at);

	print_r($result);


			

					if ($result[0]['status'] == 'sent')

					{

						echo '[ok] mail sent<br />';

					}

				}

				catch (Mandrill_Error $e)

				{

					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();

					throw $e;

				}

			}

			else 

			{

					

			}

		}

	}

}



ftp_close($conn_id);



echo '</pre>';		