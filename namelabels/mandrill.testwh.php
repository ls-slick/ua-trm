<?php

require_once 'mailchimp-mandrill-api-php-9f336b08ea14/mailchimp-mandrill-api-php-9f336b08ea14/src/Mandrill.php'; //Not required with Composer

//$mandrill = new Mandrill('PnT0naiEhIRaHBqLOj1MxQ');



try {



	$mandrill = new Mandrill('PnT0naiEhIRaHBqLOj1MxQ');
	
	$template_name = 'tesco-ff-packing';
	
	$template_content = array(
                                  array(
                                  'name' => 'ordernumber',
                                  'content' => '5555555'
                                  ),
                                  array (
                                  'name' => 'customername',
                                  'content' => 'Andrew Lindsay'
                                  )
         );


	$message = array(

			'subject' => 'Your Order 5555555 - Test Email',

			'from_email' => 'no-reply@ff-ues.com',

			'from_name' => 'F&F Uniform Embroidery Service',

			'to' => array(

					array(

							'email' => 'andy@slickstitch.com',

							'name' => 'Andrew Lindsay',

							'type' => 'to'

					)

			),

			'important' => false,

			'track_opens' => null,

			'track_clicks' => false,

			'auto_text' => null,

			'auto_html' => null,

			'inline_css' => null,

			'url_strip_qs' => null,

			'preserve_recipients' => null,

			'view_content_link' => null,

			'bcc_address' => null,

			'tracking_domain' => null,

			'signing_domain' => 'ff-ues.com',

			'return_path_domain' => null,

			'merge' => true,

			'merge_language' => 'mailchimp',

			'global_merge_vars' => null,

			'merge_vars' => null,

			'metadata' => array('website' => 'www.ff-ues.com'),

			'recipient_metadata' => null,
			
			'tags' => array('ff-ues-sapphire')

	);

	$async = false;

	$ip_pool = 'Main Pool';

	//$send_at = '1999-01-01 12:34:56';

	$result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);//, $send_at);

	print_r($result);

	/*

	 Array

	 (

	 [0] => Array

	 (

	 [email] => recipient.email@example.com

	 [status] => sent

	 [reject_reason] => hard-bounce

	 [_id] => abc123abc123abc123abc123abc123

	 )



	 )

	*/

} catch(Mandrill_Error $e) {

	// Mandrill errors are thrown as exceptions

	echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();

	// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'

	throw $e;

}

?>