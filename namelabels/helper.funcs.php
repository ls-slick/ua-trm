<?php

function ss_nl_get_customer_name_for_customer_id ($db, $customer_id)

{

	return $db->query("select * from customers where id = '$customer_id' limit 1")->fetch_assoc()['customer_name'];

}

function ss_nl_get_customer_dir_for_customer_id ($db, $customer_id)

{

	return $db->query("select * from customers where id = '$customer_id' limit 1")->fetch_assoc()['customer_dir_name'];

}

function ss_nl_get_customer_template_for_customer_id ($db, $customer_id)

{

	return $db->query("select * from customers where id = '$customer_id' limit 1")->fetch_assoc()['customer_template_name'];

}


function ss_nl_get_customer_service_text_for_customer_id ($db, $customer_id)

{

	return $db->query("select * from customers where id = '$customer_id' limit 1")->fetch_assoc()['customer_email_service_description'];

}

function ss_nl_get_customer_namelabels_url_for_customer_id ($db, $customer_id)

{

	return $db->query("select * from customers where id = '$customer_id' limit 1")->fetch_assoc()['customer_namelabels_url'];

}



function ss_nl_get_customer_namelabels_email_for_customer_id ($db, $customer_id)

{

	return $db->query("select * from customers where id = '$customer_id' limit 1")->fetch_assoc()['customer_namelabels_email'];

}



function ss_nl_get_customer_ftp_folder_name_for_order_id($db, $order_id)

{

	$customer_id = $db->query("select * from orders where id = '$order_id' limit 1")->fetch_assoc()['customer_id'];

	

	return $db->query("select * from customers where id = '$customer_id' limit 1")->fetch_assoc()['customer_ftp_folder_name'];

}



function ss_nl_update_name_text_for_order($db, $order_id, $name_text, $name_text_id)

{

	$order_id = (int) mysqli_real_escape_string($db, $order_id);

	$name_text = mysqli_real_escape_string($db, $name_text);



	if ($db->query("update names set name_text = '$name_text' where order_id = '$order_id' and name_text_id = '$name_text_id' limit 1"))

	{

		return true;

	}	

	

	return false;

}



function ss_nl_get_name_texts($db, $order_id)

{

	$query = $db->query("select * from names where order_id = '$order_id' order by id asc");

	

	$name_texts = array();

	

	while ($row = $query->fetch_assoc())

	{

		$name_texts[] = array($row['name_text_id'] => $row['name_text']);

	}

	

	return $name_texts;

}



function good_lnid($db, $label_name_id)

{

	$label_name_id = mysqli_real_escape_string($db, $label_name_id);



	$query = "select * from orders where lnid = '$label_name_id'";

	

	if ($db->query($query)->num_rows == 1)

	{

		return true;

	}



	return false;

}



function get_info_for_lnid($db, $label_name_id)

{



	$label_name_id = mysqli_real_escape_string($db, $label_name_id);



	$query = $db->query("select * from orders where lnid = '$label_name_id'");



	while ($row = $query->fetch_assoc())

	{

		$info['customer_name'] = ss_nl_get_customer_name_for_customer_id($db, $row['customer_id']);
		
         	$info['customer_dir_name'] = ss_nl_get_customer_dir_for_customer_id($db, $row['customer_id']);

		$info['order_reference'] = $row['order_reference'];

		$info['order_complete'] = $row['order_complete'];

		$info['order_id'] = $row['id'];

		$info['quantity_ordered'] = $row['quantity_ordered'];
		
	}



	if (isset($info))

	{

		return $info;

	}



	return false;

}



function ss_nl_make_print_csv($db, $order_id)

{

	$csv_content = "STRING,QTY\n";

	$customer_id_query = "select * from orders where id = '$order_id'";

	$customer_id = $db->query($customer_id_query)->fetch_assoc()['customer_id'];

	$quantity = $db->query("select * from customers where id = '$customer_id'")->fetch_assoc()['customer_batch_multiple_value'];

	$names_query = $db->query("select * from names where order_id = '$order_id' order by id asc");

	

	while ($row = $names_query->fetch_assoc())

	{

		$csv_content .= "{$row['name_text']},$quantity\n";

	}

	

	return $csv_content;

}



function ss_nl_delete_print_csv($print_csv_name)

{

	require('./ftp_login_details.php');

	

	$conn_id = ftp_connect($ftp_login_details['ftp_server']);

	$login_result = ftp_login($conn_id, $ftp_login_details['ftp_user'], $ftp_login_details['ftp_pass']);

	ftp_pasv($conn_id, true);

	

	if (ftp_size($conn_id, $print_csv_name) != -1)

	{

		if (ftp_delete($conn_id, $print_csv_name))

		{

			return true;

		}

	}

	

	return false;

}



function ss_nl_write_print_csv($csv_name, $csv_content)

{

	require('./ftp_login_details.php');

	

	$conn_id = ftp_connect($ftp_login_details['ftp_server']);

	$login_result = ftp_login($conn_id, $ftp_login_details['ftp_user'], $ftp_login_details['ftp_pass']);

	ftp_pasv($conn_id, true);

	$local_file = str_replace('/', '_',$csv_name);

	

	file_put_contents($local_file, $csv_content);

	

	if (ftp_put($conn_id, $csv_name, $local_file, FTP_ASCII))

	{

		return true;

	}

	

	return false;

}





function ss_nl_order_complete($db, $order_id)

{

	require('./ftp_login_details.php');

	

	$conn_id = ftp_connect($ftp_login_details['ftp_server']);

	$login_result = ftp_login($conn_id, $ftp_login_details['ftp_user'], $ftp_login_details['ftp_pass']);

	ftp_pasv($conn_id, true);

	$order_info = $db->query("select * from orders where id = '$order_id'")->fetch_assoc();

	$print_csv_name = $order_info['print_csv_name'];

	

	if ((strlen($print_csv_name) > 0) && (ftp_size($conn_id, $print_csv_name) == -1))

	{		

			$db->query("update orders set order_complete = 1 where id = '$order_id'");

			

			return true;

	}

	

	return false;

}





function ss_nl_update_csv($db, $order_id)

{

	require('./ftp_login_details.php');

	

	$conn_id = ftp_connect($ftp_login_details['ftp_server']);

	$login_result = ftp_login($conn_id, $ftp_login_details['ftp_user'], $ftp_login_details['ftp_pass']);

	ftp_pasv($conn_id, true);

	$order_info = $db->query("select * from orders where id = '$order_id'")->fetch_assoc();

	$print_csv_name = $order_info['print_csv_name'];

	

	$order_reference = $order_info['order_reference'];



	if (strlen($print_csv_name) > 0)

	{

		if (ftp_size($conn_id, $print_csv_name) != -1)

		{

			if (ss_nl_delete_print_csv($print_csv_name) && ss_nl_write_print_csv($print_csv_name,  ss_nl_make_print_csv($db, $order_id)))

			{

				$query = "update orders set updated_at = NOW(), number_of_times_updated = number_of_times_updated + 1 where id = '$order_id'";

				$db->query($query);

			}		

		}

		else

		{

			$db->query("update orders set order_complete = 1 where id = '$order_id'");

		}

	}

	else

	{

		$csv_content = ss_nl_make_print_csv($db, $order_id);

		$print_csv_name_new = ss_nl_get_customer_ftp_folder_name_for_order_id($db, $order_id).'/PRINT/order_'.$order_reference.'.csv';

		$update_csv_name_query = "update orders set print_csv_name = '$print_csv_name_new' where id = '$order_id'";

		$update_csv_name = $db->query($update_csv_name_query);

		ss_nl_write_print_csv($print_csv_name_new, $csv_content);

	}

}