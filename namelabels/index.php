<?php

session_start();



require_once('./mysqli.php');

require_once('./helper.funcs.php');



if (!isset($_GET['lnid']))

{

	exit('no lnid');

}

else

{

	if (good_lnid($db, $_GET['lnid']))

	{

		$plural = false;

		$message = ''; // nothing yet.



		$_SESSION['lnid'] = $_GET['lnid'];

		$info = get_info_for_lnid($db, $_GET['lnid']);



		if (ss_nl_order_complete($db, $info['order_id']) === true)

		{

			$info = get_info_for_lnid($db, $_GET['lnid']);

		}



		if (($info['quantity_ordered']) > 1)

		{

			$plural = true;

		}



		$error = '<div class="alert alert-error">There was a problem saving your name labels. We have been notified of this error. '.date('H:i:s').'</div>';



		$no_error = '<div class="alert alert-success"><strong>Your chosen name'.(($plural == true) ? 's' : '').' ha'.(($plural == true) ? 've' : 's').' been saved.</strong><br />'.

		'You can update the text for your Name Labels at any time up until your order is processed.</div>';



		if (isset($_POST['submit']) && ($info['order_complete'] == 0))

		{

			$message = $no_error;

			$i = 0;



			$update_csv = true;



			foreach ($_POST['name'] as $name)

			{

				$i++;



				if (!ss_nl_update_name_text_for_order($db, $info['order_id'], $name, $i))

				{

					$message = $error;



					$error_message = $i.')'.print_r($db, true)."\r\n".print_r($info['order_id'], true)."\r\n".print_r($name, true)."\r\n";



					mail('alex@slickstitch.com', 'Problem - Slick Stitch Name Labels', $error_message);



					$update_csv = false;

				}

			}



			if ($update_csv === true)

			{

				ss_nl_update_csv($db, $info['order_id']);

			}

		}



		$name_texts =  ss_nl_get_name_texts($db, $info['order_id']);

	}

	else

	{

		exit('[hello]');

	}

}



$i = 0;

?>

<!DOCTYPE html>

<html>

<head>

<title><?=$info['customer_name']?></title>

<link rel="stylesheet" href="style.css" />

<link rel="stylesheet" href="<?=strtolower($info['customer_dir_name'])?>/style.css" />

</head>

<body>

<div id="container">

<div class="logo"><a href="./index.php?lnid=<?=$_SESSION['lnid']?>"><img src="<?=strtolower($info['customer_dir_name'])?>/logo.png" border="0" alt="<?=$info['customer_name']?>"/></a></div>

<div class="your-order-number">Your Order Number is <?=$info['order_reference']?></div>

<div class="container2">

<div class="gradient-bar"></div>

<div class="content">

<div class="first-message">UNIFORM</div>

<div class="second-message">Embroidery Service</div>

<div class="third-message">

<?=$message?>

<p>Thank you for your order!</p>

<p>You ordered <?=$info['quantity_ordered']; ?> batch<?=($plural == true) ? 'es' : ''?> of name labels.</p>

<p>Please type in the name<?=($plural == true) ? 's' : ''?> you want on your tapes. (Maximum 20 characters per name).</p>

</div>

<div class="form">

<form action="index.php?lnid=<?=$_SESSION['lnid']?>" method="post">

<fieldset>



<?php

$i = 0;



foreach ($name_texts as $name_text)

{

	$i++;

	

	echo '<p>'.

		'<label for="name'.$i.'">'.$i.'. </label>'.

		'<input type="text" id="name'.$i.'" name="name[]" placeholder="Type the name" class="textbox" maxlength="20" value="'.$name_text[$i].'" '; if ($info['order_complete'] == 1)	{ echo 'disabled="disabled"'; } echo '/>'.

		'</p>';

}

?>



<p>

<input type="submit" name="submit" value="SUBMIT" class="submit" <?= ($info['order_complete'] == true) ? 'disabled="disabled"' : ''?> />

</p>

<?= ($info['order_complete'] == true) ? '<strong>Your Name Labels have been Dispatched!</strong>' : '' ?>

</fieldset>

</form>

</div>

</div>

</div>

</div>

</body>

</html>